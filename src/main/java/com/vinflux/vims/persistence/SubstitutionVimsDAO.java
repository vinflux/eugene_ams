package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("substitutionVimsDAO")
public class SubstitutionVimsDAO extends VimsAbstractDAO {
	/**
	 * INSERT TVIEW_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.SubstitutionVimsDAO.insertTViewMstIcsb",parameterMap);
	}
	
	
	/**
	 * UPDATE TVIEW_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstIcsb(Map parameterMap) throws Exception {
		return update("com.vinflux.vims.persistence.SubstitutionVimsDAO.updateTViewMstIcsb",parameterMap);
	}
	

	/**
	 * DELETE TVIEW_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstIcsb(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.SubstitutionVimsDAO.deleteTViewMstIcsb",parameterMap);
	}
}
