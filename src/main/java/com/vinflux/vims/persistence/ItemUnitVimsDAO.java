package com.vinflux.vims.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("itemUnitVimsDAO")
public class ItemUnitVimsDAO extends VimsAbstractDAO{
	/**
	 * INSERT TVIEW_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemUnitVimsDAO.insertTViewMstIcut",parameterMap);
	}
	
	/**
	 * update TVIEW_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemUnitVimsDAO.updateTViewMstIcut",parameterMap);
	}

	/**
	 * DELETE TVIEW_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemUnitVimsDAO.deleteTViewMstIcut",parameterMap);
	}
	
	/**
	 * SELECT TVIEW_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstIcutxtype (Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.ItemUnitVimsDAO.selectTViewMstIcutxtype",parameterMap);
	}
	
	
	/**
	 * INSERT TVIEW_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstIcutxtype(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemUnitVimsDAO.insertTViewMstIcutxtype",parameterMap);
	}

	/**
	 * UPDATE TVIEW_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstIcutxtype(Map parameterMap) throws Exception {
		return update("com.vinflux.vims.persistence.ItemUnitVimsDAO.updateTViewMstIcutxtype",parameterMap);
	}

	/**
	 * DELETE TVIEW_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstIcutxtype(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.ItemUnitVimsDAO.deleteTViewMstIcutxtype",parameterMap);
	}
}
