package com.vinflux.vims.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("messageVimsDAO")
	
public class MessageVimsDAO extends VimsAbstractDAO {
	public int insertTVimsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.MessageVimsDAO.insertTViewMstMulaapmsgDt", parameterMap);
	}

	public int selectCountTVimsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.MessageVimsDAO.selectCountTViewMstMulaapmsgDt", parameterMap);
	}
	
	public int updateTVimsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.vims.persistence.MessageVimsDAO.updateTViewMstMulaapmsgDt", parameterMap);
	}
	
	public int deleteTVimsMstMulaapmsgDt(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.vims.persistence.MessageVimsDAO.deleteTViewMstMulaapmsgDt", parameterMap);
	}
	
	public List<Map> selectTviewIfAlertList(){
		return selectList("com.vinflux.vims.persistence.MessageVimsDAO.selectTviewIfAlertList", null);
	} 
	
	public int updateTviewIfAlertSENDYNBySeq(Map parameterMap) throws Exception {
		return update("com.vinflux.vims.persistence.MessageVimsDAO.updateTviewIfAlertSENDYNBySeq", parameterMap);
	}
	
	/**
	 * TVIEW_IF_ALERT sendyn bach 처리.
	 * @param List
	 * @return
	 * @throws Exception
	 */
	public void updateTviewIfAlertSENDYNBySeq(List<Map> list) throws Exception {

		updateExecuteBatch("com.vinflux.vims.persistence.MessageVimsDAO.updateTviewIfAlertSENDYNBySeq", list);
	}
	
	public int updateTviewIfAlertSENDYN(String insdate) throws Exception {
		return update("com.vinflux.vims.persistence.MessageVimsDAO.updateTviewIfAlertSENDYN", insdate);
	}
}