package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("ownerVimsDAO")
public class OwnerVimsDAO extends VimsAbstractDAO {
	
	
	public int insertTViewMstOw(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.OwnerVimsDAO.insertTViewMstOw",parameterMap);
	}

	public int updateTViewMstOw(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.OwnerVimsDAO.updateTViewMstOw",parameterMap);
	}
	
	public int deleteTViewMstOw(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.OwnerVimsDAO.deleteTViewMstOw",parameterMap);
	}
	
	
	
}