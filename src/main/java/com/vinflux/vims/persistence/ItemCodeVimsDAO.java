package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("itemCodeVimsDAO")
public class ItemCodeVimsDAO extends VimsAbstractDAO{
	/**
	 * INSERT TVIEW_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemCodeVimsDAO.insertTViewMstIc",parameterMap);
	}
	
	/**
	 * UPDATE TVIEW_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemCodeVimsDAO.updateTViewMstIc",parameterMap);
	}

	/**
	 * DELETE TVIEW_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemCodeVimsDAO.deleteTViewMstIc",parameterMap);
	}
}
