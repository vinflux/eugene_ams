package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("itemGroupVimsDAO")
public class ItemGroupVimsDAO extends VimsAbstractDAO {
	/**
	 * INSERT TVIEW_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemGroupVimsDAO.insertTViewMstIcgr",parameterMap);
	}
	
	/**
	 * update TVIEW_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemGroupVimsDAO.updateTViewMstIcgr",parameterMap);
	}

	/**
	 * DELETE TVIEW_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.ItemGroupVimsDAO.deleteTViewMstIcgr",parameterMap);
	}
}
