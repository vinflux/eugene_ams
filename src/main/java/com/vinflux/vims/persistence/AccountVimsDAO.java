package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("accountVimsDAO")
public class AccountVimsDAO extends VimsAbstractDAO {
	
	
	public int insertTViewMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AccountVimsDAO.insertTViewMstAc",parameterMap);
	}

	public int updateTViewMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AccountVimsDAO.updateTViewMstAc",parameterMap);
	}
	
	public int deleteTViewMstAc(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.AccountVimsDAO.deleteTViewMstAc",parameterMap);
	}
	
	
	
}