package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("adminCodeVimsDAO")
public class AdminCodeVimsDAO extends VimsAbstractDAO {

	public int mergeTViewAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AdminCodeVimsDAO.mergeTViewAdminCodeInfo",parameterMap);
	}

	public int deleteTViewAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.AdminCodeVimsDAO.deleteTViewAdminCodeInfo",parameterMap);
	}
	
	public int mergeTViewDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AdminCodeVimsDAO.mergeTViewDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTViewDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.AdminCodeVimsDAO.deleteTViewDetailAdminCodeInfo",parameterMap);
	}
	
	
	public int deleteByHDKeyTViewDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.AdminCodeVimsDAO.deleteByHDKeyTViewDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTViewAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.vims.persistence.AdminCodeVimsDAO.selectTViewAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTViewDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AdminCodeVimsDAO.insertTViewDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTViewAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.AdminCodeVimsDAO.insertTViewAdminCodeInfo",parameterMap);
	}
}