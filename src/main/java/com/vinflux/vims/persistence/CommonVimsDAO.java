package com.vinflux.vims.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("commonVimsDAO")
public class CommonVimsDAO extends VimsAbstractDAO {

	/**
	 * TMS Message Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectAppMessage",parameterMap);
	}	

	/**
	 * Master data select (CENTER) count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTViewMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstCtCount",parameterMap);
	}
	
	/** Master data select (CENTER) 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 **/
	public List selectTViewMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstCt",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTViewMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstOwCount",parameterMap);
	}
	
	/**
	 * Master data select (OWNER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstOw",parameterMap);
	}
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTViewMstVicdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstVicdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstVicdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstVicdDt",parameterMap);
	}
	
	/** Master data select count (ITEMCODE) **/
	public int selectTViewMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcCount",parameterMap);
	}

	/** Master data select (ITEMCODE) **/
	public List selectTViewMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIc",parameterMap);
	}
	
	/** Master data select count (ITEMCODE GROUP) **/
	public int selectTViewMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcgrCount",parameterMap);
	}
	
	/** Master data select (ITEMCODE GROUP) **/
	public List selectTViewMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcgr",parameterMap);
	}
	
	public int selectTViewMstIcsbCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcsbCount",parameterMap);
	}
	

	/** Master data select (SUBSTITUTION) **/
	public List selectTViewMstIcsb(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcsb",parameterMap);
	}
	
	/** Master data select (ITEMCODE UNIT) **/
	public List selectTViewMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcut",parameterMap);
	}

	/** Master data select count (ITEMCODE UNIT) **/
	public int selectTViewMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcutCount",parameterMap);
	}
	
	/**
	 * Master data select (ACCOUNT) Count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTViewMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstAcCount",parameterMap);
	}

	/**
	 * Master data select (ACCOUNT)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstAc",parameterMap);
	}
	
	/**
	 * Master data select (ITEM UNIT TYPE) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTViewMstIcutxtypeCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcutxtypeCount",parameterMap);
	}
	/**
	 * Master data select (ITEM UNIT TYPE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstIcutxtype",parameterMap);
	}
	
	/**
	 * Master data select (VIMS DT CODE) : TVIEW_MST_VICD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTViewMstVicdDtList() throws Exception {
		Map parameterMap = new HashMap();
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstVicdDtList", parameterMap);
	}
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectMyMenuGrouping",parameterMap);
	}
	
	public List selectTViewMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstUrdfus",parameterMap);
	}
	
	public List selectTViewMstVicdDtkeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstVicdDtkeyList", parameterMap);
	}	
	
	/**
	 * 데쉬보드 헤더 Count: TVIEW_MST_DASH_HD   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTViewMstDashHdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstDashHdCount",parameterMap);
	}
	
	/**
	 * 데쉬보드 헤더 :TVIEW_MST_DASH_HD
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstDashHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.vims.persistence.CommonVimsDAO.selectTViewMstDashHd",parameterMap);
	}	
	
}
