package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("commonAddrEntityVimsDAO")
public class CommonAddrEntityVimsDAO extends VimsAbstractDAO {

	public int addAddressEntitymaster(Map paramMap) throws Exception {
		return insert("com.vinflux.vims.persistence.CommonAddrEntityDAO.addAddressEntitymaster", paramMap);
	}
	
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.vims.persistence.CommonAddrEntityDAO.queryAddressEntitymaster", paramMap);
	}
	
	public int changeAddressEntitymaster(Map paramMap) throws Exception {
		return update("com.vinflux.vims.persistence.CommonAddrEntityDAO.changeAddressEntitymaster", paramMap);
	}
	
	public int deleteAddressEntitymaster(Map paramMap) throws Exception {    	    	
		return update("com.vinflux.vims.persistence.CommonAddrEntityDAO.deleteAddressEntitymaster", paramMap);
	}

}
