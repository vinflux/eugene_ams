package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("centerVimsDAO")
public class CenterVimsDAO extends VimsAbstractDAO {

	public int insertTViewMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.CenterVimsDAO.insertTViewMstCt",parameterMap);
	}

	public int updateTViewMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.vims.persistence.CenterVimsDAO.updateTViewMstCt",parameterMap);
	}
	
	public int deleteTViewMstCt(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.CenterVimsDAO.deleteTViewMstCt",parameterMap);
	}
}