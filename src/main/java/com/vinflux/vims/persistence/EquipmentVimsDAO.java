package com.vinflux.vims.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;


@Repository("equipmentVimsDAO")
public class EquipmentVimsDAO extends VimsAbstractDAO {
	/**
	 * SELECT TVIEW_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTViewMstEq (Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.vims.persistence.EquipmentVimsDAO.selectTViewMstEq",parameterMap);
	}
	
	/**
	 * INSERT TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTViewMstEq(Map parameterMap) throws Exception {		
		return insert("com.vinflux.vims.persistence.EquipmentVimsDAO.insertTViewMstEq",parameterMap);
	}

	/**
	 * UPDATE TVIEW_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTViewMstEq(Map parameterMap) throws Exception {
		return update("com.vinflux.vims.persistence.EquipmentVimsDAO.updateTViewMstEq",parameterMap);
	}

	/**
	 * DELETE TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTViewMstEq(Map parameterMap) throws Exception {
		return delete("com.vinflux.vims.persistence.EquipmentVimsDAO.deleteTViewMstEq",parameterMap);
	}
	
}
