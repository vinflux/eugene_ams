package com.vinflux.vims.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.VimsAbstractDAO;

@Repository("alertSettingDAO")
public class AlertSettingDAO extends VimsAbstractDAO {

	
	public Integer selectAlertSettingHdCount(Map parameterMap) {
		return selectForInt("com.vinflux.vims.persistence.AlertSettingDAO.selectAlertSettingHdCount", parameterMap);
	}

	public List selectAlertSettingHdList(Map parameterMap) {
		return selectList("com.vinflux.vims.persistence.AlertSettingDAO.selectAlertSettingHdList", parameterMap);
	}

	public void deleteAlertSettingHd(Map row) {
		update("com.vinflux.vims.persistence.AlertSettingDAO.deleteAlertSettingHd", row);
	}

	public void insertAlertSettingHd(Map map) {
		insert("com.vinflux.vims.persistence.AlertSettingDAO.insertAlertSettingHd", map);
	}

	public void updateAlertSettingHd(Map map) {
		update("com.vinflux.vims.persistence.AlertSettingDAO.updateAlertSettingHd", map);
	}

	public Map selectAlertSettingHd(Map paramMap) {
		return selectForMap("com.vinflux.vims.persistence.AlertSettingDAO.selectAlertSettingHd", paramMap);
	}
	
	public int checkAlertSetting(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.vims.persistence.AlertSettingDAO.checkAlertSetting", parameterMap);
    }
	
	public List selectCenterList(Map parameterMap) {
		return selectList("com.vinflux.vims.persistence.AlertSettingDAO.selectCenterList", parameterMap);
	}
	
	public List selectEventid(Map parameterMap) {
		return selectList("com.vinflux.vims.persistence.AlertSettingDAO.selectEventid", parameterMap);
	}

}