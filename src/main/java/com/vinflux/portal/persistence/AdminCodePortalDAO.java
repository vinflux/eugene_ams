package com.vinflux.portal.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PortalAbstractDAO;

@Repository("adminCodePortalDAO")
public class AdminCodePortalDAO extends PortalAbstractDAO {

	public int mergeTPortalAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.portal.persistence.AdminCodePortalDAO.mergeTPortalAdminCodeInfo",parameterMap);
	}

	public int deleteTPortalAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.portal.persistence.AdminCodePortalDAO.deleteTPortalAdminCodeInfo",parameterMap);
	}
	
	public int mergeTPortalDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.portal.persistence.AdminCodePortalDAO.mergeTPortalDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTPortalDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.portal.persistence.AdminCodePortalDAO.deleteTPortalDetailAdminCodeInfo",parameterMap);
	}
	
	
	public int deleteByHDKeyTPortalDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.portal.persistence.AdminCodePortalDAO.deleteByHDKeyTPortalDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTPortalAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.portal.persistence.AdminCodePortalDAO.selectTPortalAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTPortalDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.portal.persistence.AdminCodePortalDAO.insertTPortalDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTPortalAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.portal.persistence.AdminCodePortalDAO.insertTPortalAdminCodeInfo",parameterMap);
	}
	
	
}