package com.vinflux.portal.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PortalAbstractDAO;

@Repository("messagePortalDAO")
public class MessagePortalDAO extends PortalAbstractDAO {

	public int selectCountTPortalMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.MessagePortalDAO.selectCountTPortalMstMulaapmsgDt",parameterMap);
	}
	
	public int insertTPortalMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.portal.persistence.MessagePortalDAO.insertTPortalMstMulaapmsgDt",parameterMap);
	}

	public int updateTPortalMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.portal.persistence.MessagePortalDAO.updateTPortalMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTPortalMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.portal.persistence.MessagePortalDAO.deleteTPortalMstMulaapmsgDt",parameterMap);
	}
}