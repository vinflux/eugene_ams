package com.vinflux.portal.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PortalAbstractDAO;

@Repository("commonPortalDAO")
public class CommonPortalDAO extends PortalAbstractDAO {


	/**
	 * Message Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectAppMessage",parameterMap);
	}	

	/**
	 * code Select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTPortalMstVicdDtList (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstVicdDtList",parameterMap);
	}
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTViewMstPtcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTViewMstPtcdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTViewMstPtcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTViewMstPtcdDt",parameterMap);
	}	
	
	/**
	 * 직납거래처 테이블 Paging을 위한 Count: TPORTAL_MST_AC   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTtDcustTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTtDcustTCount",parameterMap);
	}
	
	/**
	 * 직납거래처 data select (Account) :TPORTAL_MST_AC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtDcustTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTtDcustTList",parameterMap);
	}
	
	/**
     * SELECT TPORTAL_MST_STORE
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTPortalMstStoreCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstStoreCount",parameterMap);
    }
    
    /**
     * SELECT TPORTAL_MST_STORE
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTPortalMstStore(Map parameterMap) throws Exception {
        return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstStore",parameterMap);
    }
    
    /**
     * SELECT TPORTAL_MST_DSTORE 거래처 도크조회
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTPortalMstDStoreDocCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstDStoreDocCount",parameterMap);
    }
    
    /**
     * SELECT TPORTAL_MST_DSTORE 거래처 도크조회
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTPortalMstDStoreDoc(Map parameterMap) throws Exception {
        return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstDStoreDoc",parameterMap);
    }
    
    
	public int selectTPortalMstMeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstMeCount",parameterMap);
	}
    
	public List selectTPortalMstMeAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstMeAll",parameterMap);
	}	
	
	public List selectTPortalMstMe(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPortalMstMe",parameterMap);
	}
	
	/**
	 * 사용자그룹
	 * @author jkc
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPtlInfoCompany(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectPtlInfoCompany",parameterMap);
	}

	public int selectTPtlMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstCtCount",parameterMap);
	}
		/** Master data select (USER GROUP) **/
	public List selectTPtlMstCt(Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstCt",parameterMap);
	}
	
	
	/**
	 * Master data select (USER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminPotalUserGroupCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ptl.persistence.PtlUserDAO.selectTAdminPotalUserGroupCount",parameterMap);
	}
		/** Master data select (USER GROUP) **/
	public List selectTAdminPotalUserGroupList(Map parameterMap) throws Exception { 
		return selectList("com.vinflux.ptl.persistence.PtlUserDAO.selectTAdminPotalUserGroupList",parameterMap);
	}

	
	public int selectTAdminMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTAdminMstOwCount",parameterMap);
	}                 
	
	
	/** Master data select (OWNER) **/
	public List selectTAdminMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTAdminMstOw",parameterMap);
	}                      

	
	/**
	 * SELECT TORDER_MST_STORE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTOrderMstStoreCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_STORE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTOrderMstStore",parameterMap);
	}
	
	/**
	 * SELECT 상품검색
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author jkc
	 */
	public int selectTOrderMstIcxCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTOrderMstIcxCtCount",parameterMap);
	}
	
	/**
	 * SELECT 상품검색
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author jkc
	 */
	public List selectTOrderMstIcxCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTOrderMstIcxCt",parameterMap);
	}
	
	
	public List selectTPtlMstIcgr (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIcgr",parameterMap);
	}

	public int selectTPtlMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIcgrCount",parameterMap);
	}
	
	public List selectPtlLogrpcd (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectPtlLogrpcd",parameterMap);
	}

	public int selectPtlLogrpcdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectPtlLogrpcdCount",parameterMap);
	}
	
	public List selectTPtlMstIcname (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIcname",parameterMap);
	}

	public int selectTPtlMstIcnameCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIcnameCount",parameterMap);
	}
	
	public List selectTPtlMstIc (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIc",parameterMap);
	}

	public int selectTPtlMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstIcCount",parameterMap);
	}
	
	public List selectPtlConfigList (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectPtlConfigList",parameterMap);
	}

	public int selectTPtlMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstAcCount",parameterMap);
	}
	
	public List selectTPtlMstAc (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTPtlMstAc",parameterMap);
	}

	/**
	 * 매입처검색
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author jkc
	 */
	public int selectTAdminMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.portal.persistence.CommonPortalDAO.selectTAdminMstAcCount",parameterMap);
	}
	public List selectTAdminMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.portal.persistence.CommonPortalDAO.selectTAdminMstAc",parameterMap);
	}
}