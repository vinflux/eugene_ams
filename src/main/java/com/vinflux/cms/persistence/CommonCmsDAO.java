package com.vinflux.cms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.CmsAbstractDAO;

@Repository("commonCmsDAO")
public class CommonCmsDAO extends CmsAbstractDAO {

	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectAppMessage",parameterMap);
	}	
	
	public List selectTCmsMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTCmsMstUrdfus",parameterMap);
	}
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTCmsSetCmcsCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.cms.persistence.CommonCmsDAO.selectTCmsSetCmcsCount",parameterMap);
	}
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTCmsSetCmcs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTCmsSetCmcs",parameterMap);
	}
	
	public int selectTCmsMstCmcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.cms.persistence.CommonCmsDAO.selectTCmsMstCmcdDtCount",parameterMap);
	}
	
	public List selectTCmsMstCmcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTCmsMstCmcdDt",parameterMap);
	}
	
	public int selectTitObjectMasterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.cms.persistence.CommonCmsDAO.selectTitObjectMasterCount",parameterMap);
	}
	
	public List selectTitObjectMaster(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTitObjectMaster",parameterMap);
	}	
	
	public int selectTitClassMasterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.cms.persistence.CommonCmsDAO.selectTitClassMasterCount",parameterMap);
	}
	
	public List selectTitClassMaster(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTitClassMaster",parameterMap);
	}	
	
	public List selectTitClassMasterList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectTitClassMasterList",parameterMap);
	}	
	
	public int selectFactorListCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.cms.persistence.CommonCmsDAO.selectFactorListCount",parameterMap);
	}
	
	public List selectFactorList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.cms.persistence.CommonCmsDAO.selectFactorList",parameterMap);
	}
	
}