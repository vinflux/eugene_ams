package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("ownerWmsDAO")
public class OwnerWmsDAO extends WmsAbstractDAO {
	
	public int insertTWorkMstOw(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.OwnerWmsDAO.insertTWorkMstOw",parameterMap);
	}
	
	public int updateTWorkMstOw(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.OwnerWmsDAO.updateTWorkMstOw",parameterMap);
	}
	
	public int deleteTWorkMstOw(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.OwnerWmsDAO.deleteTWorkMstOw",parameterMap);
	}
	
}
