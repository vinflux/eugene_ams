package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("dispshelfWmsDAO")
public class DispshelfWmsDAO extends WmsAbstractDAO {
	
	public int insertTWorkMstDispshelf(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.DispshelfWmsDAO.insertTWorkMstDispshelf",parameterMap);
	}
	
	public int updateTWorkMstDispshelf(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.DispshelfWmsDAO.updateTWorkMstDispshelf",parameterMap);
	}
	
	public int deleteTWorkMstDispshelf(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.DispshelfWmsDAO.deleteTWorkMstDispshelf",parameterMap);
	}
	
}
