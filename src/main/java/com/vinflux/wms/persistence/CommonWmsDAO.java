package com.vinflux.wms.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("commonWmsDAO")
public class CommonWmsDAO extends WmsAbstractDAO {

	/**
	 * SELECT TWORK_SET_CTCS 
	 * @param paramMap
	 * @return
	 */
    public List selectWmsConfigList(Map paramMap) {
    	return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectWmsConfigList", paramMap);
    }
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTWorkMstCtcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstCtcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDt",parameterMap);
	}
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTWorkMstCtcdDtMobileCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDtMobileCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstCtcdDtMobile(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDtMobile",parameterMap);
	}
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT (for SelectCommonCode : 290)
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTWorkMstCtcdCstDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdCstDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT (for SelectCommonCode : 290)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstCtcdCstDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdCstDt",parameterMap);
	}
	
	/**
	 * Master data select (MULTI LANGUAGE APPLICATION MESSAGE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstMulaapmsgDt",parameterMap);
	}
	
	/**
	 * Master data select (ACCOUNT) Count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAcCount",parameterMap);
	}

	/**
	 * Master data select (ACCOUNT)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAc",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstOwCount",parameterMap);
	}
	
	/**
	 * Master data select (OWNER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstOw",parameterMap);
	}

	/**
	 * Master data select (CENTER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtCount",parameterMap);
	}
	
	/**
	 * Master data select (CENTER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCt",parameterMap);
	}
	
	/**
	 * Master data select (CENTER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstToCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToCtCount",parameterMap);
	}
	
	/**
	 * Master data select (CENTER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstToCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToCt",parameterMap);
	}

	
	/**
	 * Master data select (ITEMCODE GROUP) Count TABLE : TWORK_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcgrCount",parameterMap);
	}
	
	
	/**
	 * Master data select (ITEMCODE GROUP) TABLE : TWORK_MST_ICGR 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcgr",parameterMap);
	}
	
	/**
	 * Master data count (ITEMCODE UNIT) 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcutCount",parameterMap);
	}

	/**
	 * Master data select (ITEMCODE UNIT) 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcut",parameterMap);
	}

	/**
	 * Master data select (ITEMCODE) COUNT TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIc",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) COUNT TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcCount2(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcCount2",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIc2(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIc2",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) COUNT TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcMobileCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcMobileCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) TABLE : TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcMobile(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcMobile",parameterMap);
	}

	/**
	 * Master data select (SUBSTITUTION) Count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selcetTWorkMstIcsbCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selcetTWorkMstIcsbCount",parameterMap);
	}
	
	/**
	 * Master data select (SUBSTITUTION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selcetTWorkMstIcsb(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selcetTWorkMstIcsb",parameterMap);
	}

	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkSetCtcsCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkSetCtcsCount",parameterMap);
	}

	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkSetCtcs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkSetCtcs",parameterMap);
	}

	/**
	 * Master data select (AREA)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstAr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAr",parameterMap);
	}

	/**
	 * Master data select (ZONE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstZn(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstZn",parameterMap);
	}
	
	/**
	 * Master data select (AREA)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstArCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstArCount",parameterMap);
	}
	
	/**
	 * Master data select (ZONE)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstZnCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstZnCount",parameterMap);
	}
	
	/**
	 * Master data select (LOCATION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstLc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLc",parameterMap);
	}
	
	/**
	 * Master data select (LOCATION)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstLcMobCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLcMobCount",parameterMap);
	}


	/**
	 * Master data select (LOCATION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstLcMob(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLcMob",parameterMap);
	}
	
	/**
	 * Master data select (LOCATION) ERPSTLC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstErpLoc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstErpLoc",parameterMap);
	}	
	

	/**
	 * Master data select (LOCATION)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstLcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLcCount",parameterMap);
	}

	/**
	 * WMS 입고관리>적치, 재고관리>로케이션이동, 재고관리>로케이션이동작업  그리드 TO_로케이션선택 팝업에서 사용 20181109(자동화 창고 조회 제외)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstLcNonAutoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLcNonAutoCount",parameterMap);
	}

	/**
	 * WMS 입고관리>적치, 재고관리>로케이션이동, 재고관리>로케이션이동작업  그리드 TO_로케이션선택 팝업에서 사용 20181109(자동화 창고 조회 제외)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstLcNonAuto(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLcNonAuto",parameterMap);
	}
	

	/**
	 * Master data select (ULC) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstUlcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstUlcCount",parameterMap);
	}	

	/**
	 * Master data select (ULC)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstUlc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstUlc",parameterMap);
	}


	/**
	 * Master data select (INVENTORY ATTRIBUTE) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkInfoIvatCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoIvatCount",parameterMap);
	}	
	
	/**
	 * Master data select (INVENTORY ATTRIBUTE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkInfoIvat(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoIvat",parameterMap);
	}
	
	
	/**
	 * Center List Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCenterSelect (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectCenterSelect",parameterMap);
	}

	/**
	 * WMS Message Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectAppMessage",parameterMap);
	}	
	
	/**
	 * WMS IcutXType Select count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstIcutxtypeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcutxtypeCount",parameterMap);
	}		

	/**
	 * WMS IcutXType Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcutxtype (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstIcutxtype",parameterMap);
	}

	/**
	 * Master data select (AREA)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkMstPiznCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstPiznCount",parameterMap);
	}
	
	/**
	 * Master data select (AREA)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstPizn(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstPizn",parameterMap);
	}
	
	/**
	 * Master data select (Wave)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkInfoWaHdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoWaHdCount",parameterMap);
	}
	
	/**
	 * Master data select (Wave)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkInfoWaHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoWaHd",parameterMap);
	}

	/**
	 * Master data select (Putaway location status)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPutawayLocationCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectPutawayLocationCount",parameterMap);
	}
	
	/**
	 * Master data select (Putaway location status)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPutawayLocation(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectPutawayLocation",parameterMap);
	}
	
	/**
	 * Master data select (WM Equipment) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstEqCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstEqCount",parameterMap);
	}

	/**
	 * Master data select (WM Equipment)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstEq(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstEq",parameterMap);
	}
	
	/**
	 * Waybillno  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectWaybillnoCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectWaybillnoCount",parameterMap);
	}

	/**
	 * Waybillno select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectWaybillno(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectWaybillno",parameterMap);
	}	
	
	/**
	 * Master data select (ADMIN DT CODE) :TWORK_MST_CTCD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTWorkMstCtcdDtList() throws Exception {
		Map parameterMap = new HashMap();
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDtList", parameterMap);
	}	

	/**
	 * Master data select Center Type (ADMIN DT CODE) :TWORK_MST_CTCD_DT 
	 * @return List
	 * @throws Exception
	 */
	public List selectTWorkMstCtcdDtCtkeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstCtcdDtCtkeyList", parameterMap);
	}
	
	/**
	 * Master data select (PO)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTworkInfoPoHdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoPoHdCount",parameterMap);
	}
	
	/**
	 * Master data select (PO)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTworkInfoPoHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoPoHd",parameterMap);
	}

	/**
	 * SELECT EquipmentList
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipmentList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectEquipmentList",parameterMap);
	}
	
	/**
	 * TWORK_MST_STG  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstStgCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstStgCount",parameterMap);
	}

	/**
	 * TWORK_MST_STG select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstStg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstStg",parameterMap);
	}

	/**
	 * 시스템 공지 사항 가지고 오는 구문 
	 * @return
	 * @throws Exception
	 */
	public Map selectBoardNotice() throws Exception {
		return selectForMap("com.vinflux.wms.persistence.CommonWmsDAO.selectBoardNotice",new HashMap());
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstStoreCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstStore",parameterMap);
	}

	/**
	 * SELECT 물품별 입수 수량
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectIcXIcuttypeXQtyCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectIcXIcuttypeXQtyCount",parameterMap);
	}
	
	/**
	 * SELECT 물품별 입수 수량
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcXIcuttypeXQty(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectIcXIcuttypeXQty",parameterMap);
	}
	
	/**
	 * SELECT 거래처별 유저
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstAcXUr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAcXUr",parameterMap);
	}

	/**
	 * SELECT 거래처별 유저 공통코드 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstAcXUrCommonCd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAcXUrCommonCd",parameterMap);
	}
	
	/**
	 * SELECT 거래처별 유저 - 공통코드 카운트
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTWorkMstAcXUrCommonCdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAcXUrCommonCdCount",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_SEIC_HD - SET 상품 COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectCountTworkMstSeIcHd(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectCountTworkMstSeIcHd",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_SEIC_HD - SET 상품
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectListTworkMstSeIcHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectListTworkMstSeIcHd",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_SEIC_HD - SET 상품 COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectCountTworkMstSeIcXLoggHd(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectCountTworkMstSeIcXLoggHd",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_SEIC_HD - SET 상품
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectListTworkMstSeIcXLoggHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectListTworkMstSeIcXLoggHd",parameterMap);
	}
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectMyMenuGrouping",parameterMap);
	}
	
	public List selectMyMenuGroupingWithLevel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectMyMenuGroupingWithLevel",parameterMap);
	}
	
	public List selectTWorkMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstUrdfus",parameterMap);
	}
	
	/**
	 * TWORK_MST_STG  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTworkMstCtxStCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkMstCtxStCount",parameterMap);
	}

	/**
	 * TWORK_MST_STG select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTworkMstCtxSt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkMstCtxSt",parameterMap);
	}
	
	/**
	 * TWORK_MST_LEGEND_HD  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTworkMstLegendCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLegendCount",parameterMap);
	}	
	
	/**
	 * TWORK_MST_LEGEND_HD select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTworkMstLegend(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstLegend",parameterMap);
	}
	
	/**
	 * TWORK_INFO_ANALY_HD  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTworkInfoAnalyCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoAnalyCount",parameterMap);
	}	
	
	/**
	 * TWORK_INFO_ANALY_HD select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTworkInfoAnaly(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoAnaly",parameterMap);
	}
	
	/**
	 * TWORK_INFO_STATUS_HD  count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTworkInfoStatusCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoStatusCount",parameterMap);
	}	
	
	/**
	 * TWORK_INFO_STATUS_HD select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTworkInfoStatus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkInfoStatus",parameterMap);
	}	
	
	/**
	 * Master data select (REQHDKEY)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkInfoReqCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoReqCount",parameterMap);
	}
	
	/**
	 * Master data select (REQHDKEY)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkInfoReqHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoReqHd",parameterMap);
	}
	
	/**
	 * Master data select (IBHDKEY)
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectTWorkIbHdkeyCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkIbHdkeyCount",parameterMap);
	}
	
	/**
	 * Master data select (IBHDKEY)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkInfoIbHdkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkInfoIbHdkey",parameterMap);
	}

	public int selectTWorkMstAcForRepCount(Map parameterMap) {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstAcForRepCount",parameterMap);
	}

	public List selectTWorkMstAcForRep(Map parameterMap) {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTworkMstAcForRep",parameterMap);
	}
	
	public int selectTWorkMstToLcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToLcCount",parameterMap);
	}
	
	
	public List selectTWorkMstToLc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToLc",parameterMap);
	}
	
	public int selectTWorkMstToLcBonCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToLcBonCount",parameterMap);
	}
		
	public List selectTWorkMstToLcBon(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.CommonWmsDAO.selectTWorkMstToLcBon",parameterMap);
	}
}