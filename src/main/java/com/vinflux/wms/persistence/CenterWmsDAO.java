package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("centerWmsDAO")
public class CenterWmsDAO extends WmsAbstractDAO {

	public Map selectTWorkMstCt(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.wms.persistence.CenterWmsDAO.selectTWorkMstCt",parameterMap);
	}
	
	public int insertTWorkMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.CenterWmsDAO.insertTWorkMstCt",parameterMap);
	}

	public int updateTWorkMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.CenterWmsDAO.updateTWorkMstCt",parameterMap);
	}
	
	public int deleteTWorkMstCt(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.CenterWmsDAO.deleteTWorkMstCt",parameterMap);
	}
}
