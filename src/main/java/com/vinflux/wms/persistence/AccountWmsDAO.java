package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("accountWmsDAO")
public class AccountWmsDAO extends WmsAbstractDAO {

	public Map selectTWorkMstAc(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.wms.persistence.AccountWmsDAO.selectTWorkMstAc",parameterMap);
	}
	
	public int selectPkCntTWorkMstAc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.AccountWmsDAO.selectPkCntTWorkMstAc",parameterMap);
	}
	
	public int insertTWorkMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.AccountWmsDAO.insertTWorkMstAc",parameterMap);
	}

	public int updateTWorkMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.AccountWmsDAO.updateTWorkMstAc",parameterMap);
	}
	
	public int deleteTWorkMstAc(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.AccountWmsDAO.deleteTWorkMstAc",parameterMap);
	}
	public int deleteTWorkMstAcAll(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.AccountWmsDAO.deleteTWorkMstAcAll",parameterMap);
	}
}
