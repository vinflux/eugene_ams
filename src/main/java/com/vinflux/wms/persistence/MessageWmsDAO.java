package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("messageWmsDAO")
public class MessageWmsDAO extends WmsAbstractDAO {
	
	public int insertTWorkMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.MessageWmsDAO.insertTWorkMstMulaapmsgDt", parameterMap);
	}

	public int selectCountTWorkMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.MessageWmsDAO.selectCountTWorkMstMulaapmsgDt", parameterMap);
	}
	
	public int updateTWorkMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.MessageWmsDAO.updateTWorkMstMulaapmsgDt", parameterMap);
	}
	
	public int deleteTWorkMstMulaapmsgDt(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.wms.persistence.MessageWmsDAO.deleteTWorkMstMulaapmsgDt", parameterMap);
	}
}