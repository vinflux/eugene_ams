package com.vinflux.wms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("equipmentWmsDAO")
public class EquipmentWmsDAO extends WmsAbstractDAO {

	
	
	/**
	 * SELECT TWORK_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstEq(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.EquipmentWmsDAO.selectTWorkMstEq",parameterMap);
	}
	
	
	/**
	 * DELETE TWORK_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstEq(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.EquipmentWmsDAO.deleteTWorkMstEq",parameterMap);
	}
	
	
	/**
	 * INSERT TWORK_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstEq(Map parameterMap) throws Exception {
		
		return insert("com.vinflux.wms.persistence.EquipmentWmsDAO.insertTWorkMstEq",parameterMap);
	}
	

	/**
	 * SELECT Count TWORK_MST_EQ
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTWorkMstEq(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.EquipmentWmsDAO.selectPkCntTWorkMstEq", parameterMap);
	} 
	
	/**
	 * UPDATE TWORK_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstEq(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.EquipmentWmsDAO.updateTWorkMstEq",parameterMap);
	}
	
	
	
}
