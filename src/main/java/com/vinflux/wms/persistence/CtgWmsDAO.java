package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("ctgWmsDAO")
public class CtgWmsDAO extends WmsAbstractDAO {
	
	public int insertTWorkMstCtg(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.CtgWmsDAO.insertTWorkMstCtg",parameterMap);
	}
	
	public int updateTWorkMstCtg(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.CtgWmsDAO.updateTWorkMstCtg",parameterMap);
	}
	
	public int deleteTWorkMstCtg(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.CtgWmsDAO.deleteTWorkMstCtg",parameterMap);
	}
	
}
