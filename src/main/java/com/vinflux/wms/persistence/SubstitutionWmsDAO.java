package com.vinflux.wms.persistence;

//import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("substitutionWmsDAO")
public class SubstitutionWmsDAO extends WmsAbstractDAO {
	/**
	 * SELECT Count TWORK_MST_ICSB
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectCountTWorkMstIcsb(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.SubstitutionWmsDAO.selectCountTWorkMstIcsb", parameterMap);
	}
	
	/**
	 * INSERT TWORK_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.SubstitutionWmsDAO.insertTWorkMstIcsb",parameterMap);
	} 
	
	/**
	 * UPDATE TWORK_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.SubstitutionWmsDAO.updateTWorkMstIcsb",parameterMap);
	}
	
	/**
	 * DELETE TWORK_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstIcsb(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.SubstitutionWmsDAO.deleteTWorkMstIcsb",parameterMap);
	}
	
	

	
	
}
