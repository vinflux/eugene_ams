package com.vinflux.wms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("itemUnitWmsDAO")
public class ItemUnitWmsDAO extends WmsAbstractDAO {
	
	/**
	 * DELETE TWORK_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstIcutxtype(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.ItemUnitWmsDAO.deleteTWorkMstIcutxtype",parameterMap);
	}
	
	/**
	 * DELETE TWORK_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstIcut(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.ItemUnitWmsDAO.deleteTWorkMstIcut",parameterMap);
	}
	
	/**
	 * UPDATE TWORK_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstIcutxtype(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.ItemUnitWmsDAO.updateTWorkMstIcutxtype",parameterMap);
	}
	
	/**
	 * UPDATE TWORK_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstIcut(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.ItemUnitWmsDAO.updateTWorkMstIcut",parameterMap);
	}
	
	/**
	 * INSERT TWORK_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.ItemUnitWmsDAO.insertTWorkMstIcut",parameterMap);
	}
	
	/**
	 * INSERT TWORK_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstIcutxtype(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.ItemUnitWmsDAO.insertTWorkMstIcutxtype",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.ItemUnitWmsDAO.selectTWorkMstIcutxtype",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTWorkMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.wms.persistence.ItemUnitWmsDAO.selectTWorkMstIcut",parameterMap);
	}
	/**
	 * SELECT COUNT TWORK_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTworkMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.ItemUnitWmsDAO.selectTworkMstIcutCount",parameterMap);
	}
	
}
