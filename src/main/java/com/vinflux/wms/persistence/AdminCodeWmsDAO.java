package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("adminCodeWmsDAO")
public class AdminCodeWmsDAO extends WmsAbstractDAO {

	public int mergeTWorkAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.AdminCodeWmsDAO.mergeTWorkAdminCodeInfo",parameterMap);
	}

	public int deleteTWorkAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.AdminCodeWmsDAO.deleteTWorkAdminCodeInfo",parameterMap);
	}
	
	public int mergeTWorkDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.AdminCodeWmsDAO.mergeTWorkDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTWorkDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.AdminCodeWmsDAO.deleteTWorkDetailAdminCodeInfo",parameterMap);
	}
	
	public int deleteByHDKeyTWorkDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.AdminCodeWmsDAO.deleteByHDKeyTWorkDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTWorkAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.wms.persistence.AdminCodeWmsDAO.selectTWorkAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTWorkDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.AdminCodeWmsDAO.insertTWorkDetailAdminCodeInfo",parameterMap);
	}
}