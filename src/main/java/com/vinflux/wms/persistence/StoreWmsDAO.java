package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("storeWmsDAO")
public class StoreWmsDAO extends WmsAbstractDAO {

	/**
	 * insert twork_mst_store
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstStore(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.wms.persistence.StoreWmsDAO.insertTWorkMstStore", parameterMap);
	}

	
	/**
	 * update twork_mst_store
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstStore(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.StoreWmsDAO.updateTWorkMstStore", parameterMap);
	}


	/**
	 * delete twork_mst_store
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstStore(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.wms.persistence.StoreWmsDAO.deleteTWorkMstStore", parameterMap);
	}
	

	/**
	 * INSERT twork_mst_storexct
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstStorexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.StoreWmsDAO.insertTWorkMstStorexct",parameterMap);
	}

	/**
	 * DELETE twork_mst_storexct
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstStorexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.StoreWmsDAO.deleteTWorkMstStorexct",parameterMap);
	}
	
	public int selectPkCntTWorkMstStorexct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.StoreWmsDAO.selectPkCntTWorkMstStorexct",parameterMap);
	}	
}