package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("itemGroupWmsDAO")
public class ItemGroupWmsDAO extends WmsAbstractDAO {

	public int deleteTWorkMstIcgr(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.ItemGroupWmsDAO.deleteTWorkMstIcgr",parameterMap);
	}
	
	public int insertTWorkMstIcgr(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.ItemGroupWmsDAO.insertTWorkMstIcgr",parameterMap);
	}
	
	public int updateTWorkMstIcgr(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.ItemGroupWmsDAO.updateTWorkMstIcgr",parameterMap);
	}
}
