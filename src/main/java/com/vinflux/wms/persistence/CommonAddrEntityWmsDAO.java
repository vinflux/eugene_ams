package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("commonAddrEntityWmsDAO")
public class CommonAddrEntityWmsDAO extends WmsAbstractDAO {

	public int addAddressEntitymaster(Map paramMap) throws Exception {
		return insert("com.vinflux.wms.persistence.CommonAddrEntityDAO.addAddressEntitymaster", paramMap);
	}
	
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.wms.persistence.CommonAddrEntityDAO.queryAddressEntitymaster", paramMap);
	}
	
	public int changeAddressEntitymaster(Map paramMap) throws Exception {
		return update("com.vinflux.wms.persistence.CommonAddrEntityDAO.changeAddressEntitymaster", paramMap);
	}
	
	public int deleteAddressEntitymaster(Map paramMap) throws Exception {    	    	
		return update("com.vinflux.wms.persistence.CommonAddrEntityDAO.deleteAddressEntitymaster", paramMap);
	}

}
