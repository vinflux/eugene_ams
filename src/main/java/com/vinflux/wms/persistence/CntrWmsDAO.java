package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("cntrWmsDAO")
public class CntrWmsDAO extends WmsAbstractDAO {
	
	public int insertTWorkMstCntr(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.CntrWmsDAO.insertTWorkMstCntr",parameterMap);
	}
	
	public int updateTWorkMstCntr(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.CntrWmsDAO.updateTWorkMstCntr",parameterMap);
	}
	
	public int deleteTWorkMstCntr(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.CntrWmsDAO.deleteTWorkMstCntr",parameterMap);
	}
	
}
