package com.vinflux.wms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("itemCodeWmsDAO")
public class ItemCodeWmsDAO extends WmsAbstractDAO {
	
	
	
	/**
	 * SELECT TWORK_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTWorkMstIc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.ItemCodeWmsDAO.selectPkCntTWorkMstIc",parameterMap);
	}
	
	/**
	 * SELECT TWORK_MST_ICGR
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTWorkMstIcgr(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.wms.persistence.ItemCodeWmsDAO.selectPkCntTWorkMstIcgr",parameterMap);
	}
	
	
	
	/**
	 * DELETE TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTWorkMstIc(Map parameterMap) throws Exception {
		return delete("com.vinflux.wms.persistence.ItemCodeWmsDAO.deleteTWorkMstIc",parameterMap);
	}
	
	/**
	 * INSERT TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTWorkMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.wms.persistence.ItemCodeWmsDAO.insertTWorkMstIc",parameterMap);
	}
	
	/**
	 * UPDATE TWORK_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTWorkMstIc(Map parameterMap) throws Exception {
		return update("com.vinflux.wms.persistence.ItemCodeWmsDAO.updateTWorkMstIc",parameterMap);
	}	
}
