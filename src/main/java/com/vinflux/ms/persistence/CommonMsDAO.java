//package com.vinflux.ms.persistence;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Repository;
//
//import com.vinflux.framework.db.MsAbstractDAO;
//
//@Repository("commonMsDAO")
//public class CommonMsDAO extends MsAbstractDAO {
//	
//	/**
//	 * 직납 doc번호 테이블 Paging을 위한 Count:    
//	 * @param parameterMap
//	 * @return int 
//	 * @throws Exception
//	 */
//	public int selectDocNoCount(Map parameterMap) throws Exception {
//		return selectForInt("com.vinflux.ms.persistence.CommonMsDAO.selectDocNoCount",parameterMap);
//	}
//	
//	/**
//	 * 직납 doc번호 data select :
//	 * @param parameterMap
//	 * @return
//	 * @throws Exception
//	 */
//	public List selectDocNoList(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.ms.persistence.CommonMsDAO.selectDocNoList",parameterMap);
//	}
//	
//	
//}
