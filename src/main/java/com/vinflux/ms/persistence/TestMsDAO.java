//package com.vinflux.ms.persistence;
//
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Repository;
//
//import com.vinflux.framework.db.MsAbstractDAO;
//
//@Repository("testMsDAO")
//public class TestMsDAO extends MsAbstractDAO {
//
//	/**
//	 * TEST : MSPO170MT
//	 * @param Map
//     * @return List
//     * @throws Exception 
//	 */
//	public List selectTestList(Map parameterMap) throws Exception{	
//		return selectList("com.vinflux.ms.persistence.TestMsDAO.selectTestList", parameterMap);
//	}
//}
