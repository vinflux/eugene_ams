package com.vinflux.pms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PmsAbstractDAO;

@Repository("commonPmsDAO")
public class CommonPmsDAO extends PmsAbstractDAO {

	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectAppMessage",parameterMap);
	}	

	public List selectTPmsMstVicdDtList (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstVicdDtList",parameterMap);
	}

	public int selectTPmsMstPtcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstPtcdDtCount",parameterMap);
	}

	public List selectTPmsMstPtcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstPtcdDt",parameterMap);
	}
	
	public List selectTPmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMulaapmsgDt",parameterMap);
	}
	
	/** Master data select (ACCOUNT) **/
	public int selectTPmsMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAcCount", parameterMap);
	}
	
	/** Master data select (ACCOUNT) **/
	public List selectTPmsMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAc",parameterMap);
	}
	
	/** Master data select (OWNER) **/
	public int selectTPmsMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstOwCount",parameterMap);
	}
	
	/** Master data select (OWNER) **/
	public List selectTPmsMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstOw",parameterMap);
	}
	
	/** Master data select (CENTER) **/
	public int selectTPmsMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstCtCount", parameterMap);
	}

	/** Master data select (CENTER) **/
	public List selectTPmsMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstCt",parameterMap);
	}
	
	/** Master data select count (ITEMCODE GROUP) **/
	public int selectTPmsMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcgrCount",parameterMap);
	}
	
	/** Master data select (ITEMCODE GROUP) **/
	public List selectTPmsMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcgr",parameterMap);
	}
	
	/** Master data select count (ITEMCODE UNIT) **/
	public int selectTPmsMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcutCount",parameterMap);
	}
	
	/** Master data select (ITEMCODE UNIT) **/
	public List selectTPmsMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcut",parameterMap);
	}	
	
	/** Master data select count (ITEMCODE) **/
	public int selectTPmsMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcCount",parameterMap);
	}

	/** Master data select (ITEMCODE) **/
	public List selectTPmsMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIc",parameterMap);
	}
	
	public int selectTPmsMstIcsbCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcsbCount",parameterMap);
	}
	

	/** Master data select (SUBSTITUTION) **/
	public List selectTPmsMstIcsb(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcsb",parameterMap);
	}
	
	/** Master data select (ITEM UNIT TYPE COUNT) **/
	public int selectTPmsMstIcutxtypeCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcutxtypeCount",parameterMap);
	}
	
	/** Master data select (ITEM UNIT TYPE) **/
	public List selectTPmsMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstIcutxtype",parameterMap);
	}
	
	/** Master data select (STORE COUNT) **/
	public int selectTPmsMstStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstStoreCount",parameterMap);
	}
	
	/** Master data select (STORE) **/
	public List selectTPmsMstStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstStore",parameterMap);
	}
	
	/** Master data select type lakey (ICOM DT CODE) : TPMS_MST_PTCD_DT **/
	public List selectTPmsMstPtcdDtLakeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstPtcdDtLakeyList", parameterMap);
	}
	
	public int selectTPmsMstSalesTypeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstSalesTypeCount",parameterMap);
	}

	public List selectTPmsMstSalesType(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstSalesType",parameterMap);
	}
	
	public int selectTPmsMstMnfrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMnfrCount",parameterMap);
	}

	public List selectTPmsMstMnfr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMnfr",parameterMap);
	}
	
	public int selectTPmsMstAudItemTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAudItemTCount",parameterMap);
	}

	public List selectTPmsMstAudItemT(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAudItemT",parameterMap);
	}
	
	public int selectTPmsMstMiorgInspCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMiorgInspCount",parameterMap);
	}

	public List selectTPmsMstMiorgInsp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMiorgInsp",parameterMap);
	}
	
	public int selectTPmsMstMiorgWgtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMiorgWgtCount",parameterMap);
	}

	public List selectTPmsMstMiorgWgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstMiorgWgt",parameterMap);
	}
	
	public int selectTPmsMstSafeIdxWgtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstSafeIdxWgtCount",parameterMap);
	}

	public List selectTPmsMstSafeIdxWgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstSafeIdxWgt",parameterMap);
	}
	
	public int selectTPmsMstAudItemCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAudItemCount",parameterMap);
	}

	public List selectTPmsMstAudItem(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstAudItem",parameterMap);
	}
	
	public int selectTPmsMstUrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstUrCount",parameterMap);
	}
	
	public List selectTPmsMstUr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstUr",parameterMap);
	}	
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectMyMenuGrouping",parameterMap);
	}
	
	public List selectTPmsMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.pms.persistence.CommonPmsDAO.selectTPmsMstUrdfus",parameterMap);
	}
	
	
}