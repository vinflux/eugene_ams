package com.vinflux.pms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PmsAbstractDAO;

@Repository("adminCodePmsDAO")
public class AdminCodePmsDAO extends PmsAbstractDAO {

	public int mergeTPmsAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.pms.persistence.AdminCodePmsDAO.mergeTPmsAdminCodeInfo",parameterMap);
	}

	public int deleteTPmsAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.pms.persistence.AdminCodePmsDAO.deleteTPmsAdminCodeInfo",parameterMap);
	}
	
	public int mergeTPmsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.pms.persistence.AdminCodePmsDAO.mergeTPmsDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTPmsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.pms.persistence.AdminCodePmsDAO.deleteTPmsDetailAdminCodeInfo",parameterMap);
	}
	
	
	public int deleteByHDKeyTPmsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.pms.persistence.AdminCodePmsDAO.deleteByHDKeyTPmsDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTPmsAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.pms.persistence.AdminCodePmsDAO.selectTPmsAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTPmsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.pms.persistence.AdminCodePmsDAO.insertTPmsDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTPmsAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.pms.persistence.AdminCodePmsDAO.insertTPmsAdminCodeInfo",parameterMap);
	}
}