package com.vinflux.pms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.PmsAbstractDAO;

@Repository("messagePmsDAO")
public class MessagePmsDAO extends PmsAbstractDAO {

	public int selectCountTPmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.pms.persistence.MessagePmsDAO.selectCountTPmsMstMulaapmsgDt",parameterMap);
	}
	
	public int insertTPmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.pms.persistence.MessagePmsDAO.insertTPmsMstMulaapmsgDt",parameterMap);
	}

	public int updateTPmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.pms.persistence.MessagePmsDAO.updateTPmsMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTPmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.pms.persistence.MessagePmsDAO.deleteTPmsMstMulaapmsgDt",parameterMap);
	}
}