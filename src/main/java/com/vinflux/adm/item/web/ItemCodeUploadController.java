package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.vinflux.adm.item.service.ItemCodeUploadService;
import com.vinflux.adm.item.service.ItemTrans;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.UnicodeUtil;
import com.vinflux.framework.web.CommandController;

@Controller("itemCodeUploadController")
@RemotingDestination
public class ItemCodeUploadController extends CommandController {

	@Resource(name = "itemCodeUploadService")
	private ItemCodeUploadService itemCodeUploadService;
	
	@Resource(name = "itemTrans")
	private ItemTrans itemTrans;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ItemCodeUploadController.class);
	
    /**
     * ADM > 물품 > 물품 업로드 > 물품 업로드 임시 테이블 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeUploadController/selectItemCodeUploadInfo.do")
    @ResponseBody   
    public CommandMap selectItemCodeUploadInfo (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeUploadMap = itemCodeUploadService.selectItemCodeUploadInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeUploadMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeUploadMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드
     * @param requestMap
     * @return 
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeUploadController/uploadItemCodeExcel.do")
    public void uploadItemCodeExcel (Map<String,Object> commandMap, HttpServletRequest request , HttpServletResponse response) throws Exception {
    	
    	CommandMap requestMap  = JsonUtil.jsonCovertCommandMap(UnicodeUtil.decode(MapUtil.getStr(commandMap, "commandParam")));
		Map paramMap = new HashMap();
		paramMap.put("userinfo", requestMap.getCommonUserInfo());
		
    	MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multiRequest.getFileMap();				

	    Iterator<Entry<String, MultipartFile>> itr = fileMap.entrySet().iterator();
		MultipartFile file;

		boolean transactionStatus = false; 
		int errorCode = 101;
		Map argMap = new HashMap();
		String ulhsKey = "";
	    
		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();

			file = entry.getValue();
			if (!"".equals(file.getOriginalFilename())) {

				if (file.getOriginalFilename().endsWith(".xls")
//						|| file.getOriginalFilename().endsWith(".xlsx")
//						|| file.getOriginalFilename().endsWith(".XLSX")
						|| file.getOriginalFilename().endsWith(".XLS")
					) {
					
					try {
						ulhsKey = itemCodeUploadService.uploadItemCodeExcel(paramMap, file.getInputStream() , file.getOriginalFilename());
						transactionStatus = true;
					} catch (MsgException me) {
						LOG.error( me );
						errorCode = me.getCode();
						argMap = me.getArgMap();
					} catch (Exception e) {
						LOG.error( e );
						errorCode = 101;
					}
					
				}else{
					errorCode = 102;
				}
			}
		}
	
		ExcelUtil.throwMessage(response, transactionStatus, argMap, errorCode, ulhsKey);
    } 
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드후 그리드 출력
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeUploadController/selectItemCodeTemp.do")
    @ResponseBody   
    public CommandMap selectItemCodeTemp(@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeTempMap = itemCodeUploadService.selectItemCodeTemp(paramMap);
    	Integer count = MapUtil.getInt(itemCodeTempMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeTempMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드후 저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeUploadController/saveItemCode.do")
    @ResponseBody   
    public CommandMap saveItemCode (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	List list = itemCodeUploadService.selectItemCodeTempAll(paramMap);
    	
    	LOG.debug("===============selectItemCodeTempAll=======================");
    	LOG.debug("selectItemCodeTempAll => " + list);
    	LOG.debug("================selectItemCodeTempAll======================");
    	
    	paramMap.put("lakey", requestMap.getCommonUserInfoMap().get("laKey"));
    	paramMap.put("owkey", requestMap.get("owkeym"));
    	
    	Map rtnMap = itemTrans.saveItemCodeUpload(paramMap , list);
    	
    	Integer count = MapUtil.getInt(rtnMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)rtnMap.get("list"));
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }   
    
    /**
     * ADM > 물품 > 물품 업로드 > 물품 업로드 임시테이블 엑셀 다운
     * @param requestMap
     * @return 
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeUploadController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = itemCodeUploadService.selectExcelDownItemCodeUpload(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				itemCodeUploadService.selectExcelDownItemCodeUpload(parameterMap, excelFormat),
				fileName, response);
    	
    }      
}