package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.CrossdockService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("crossdockController")
@RemotingDestination
public class CrossdockController extends CommandController {

	@Resource(name = "crossdockService")
	private CrossdockService crossdockService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(CrossdockController.class);
	
    /**
     * ADM > 상품 > 크로스도크 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/selectCrossdock.do")
    @ResponseBody   
    public CommandMap selectCrossdock (@RequestBody CommandMap requestMap) throws Exception {
    	    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
		LOG.debug("======================================");
		LOG.debug("crossdockController.selectCrossdock.paramMap => " + paramMap);
		LOG.debug("======================================");
    	
    	Map itemCodeMap = crossdockService.selectCrossdock(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 크로스도크 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/checkCrossdock.do")
    @ResponseBody   
    public CommandMap checkCrossdock (@RequestBody CommandMap requestMap) throws Exception {
    	    	
    	Map paramMap = requestMap.getMap("cdcMap");   	
    	crossdockService.checkCrossdock(paramMap);
    	
		LOG.debug("======================================");
		LOG.debug("crossdockController.checkCrossdock.paramMap => " + paramMap);
		LOG.debug("======================================");
		
    	return requestMap;
    }    

    /**
     * ADM > 상품 > 크로스도크 > 등록
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/insertCrossdock.do")
    @ResponseBody   
    public CommandMap insertCrossdock (@RequestBody CommandMap requestMap) throws Exception {
    	    	
    	Map paramMap = requestMap.getMap("cdcMap");
    	if(paramMap.get("tpendtm") != null && ((String)paramMap.get("tpendtm")).length() > 6){
    		paramMap.put("tpendtm", ((String)paramMap.get("tpendtm")).substring(0,6));
    	}    	
    	crossdockService.insertCrossdock(paramMap);
    	
    	LOG.debug("======================================");
		LOG.debug("crossdockController.insertCrossdock.paramMap => " + paramMap);
		LOG.debug("======================================");
    	    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 크로스도크 > 수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/updateCrossdock.do")
    @ResponseBody   
    public CommandMap updateCrossdock (@RequestBody CommandMap requestMap) throws Exception {
    	    	
    	Map paramMap = requestMap.getMap("cdcMap");
    	if(paramMap.get("tpendtm") != null && ((String)paramMap.get("tpendtm")).length() > 6){
    		paramMap.put("tpendtm", ((String)paramMap.get("tpendtm")).substring(0,6));
    	}    	
    	crossdockService.updateCrossdock(paramMap);
    	
    	LOG.debug("======================================");
		LOG.debug("crossdockController.updateCrossdock.paramMap => " + paramMap);
		LOG.debug("======================================");
    	    	
    	return requestMap;
    }  
    
    /**
     * ADM > 상품 > 크로스도크 > 삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/deleteCrossdock.do")
    @ResponseBody   
    public CommandMap deleteCrossdock (@RequestBody CommandMap requestMap) throws Exception {
    	    	
    	Map paramMap = requestMap.getParamMap();
      	List list = requestMap.getList("list");
    	crossdockService.deleteCrossdock(list);
    	
    	LOG.debug("======================================");
		LOG.debug("crossdockController.deleteCrossdock.paramMap => " + paramMap);
		LOG.debug("======================================");
    	    	
    	return requestMap;
    }    
    
    
    /**
     * ADM > 상품 > 크로스도크 > 엑셀다운
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/crossdockController/excelDown.do")
    @ResponseBody   
    public void selectExcelCrossdock (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	
    	
    	LOG.debug("======================================");
		LOG.debug("crossdockController.selectExcelCrossdock.param => " + param);
		LOG.debug("======================================");

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = crossdockService.selectExcelDownDic(paramMap);

    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				crossdockService.selectExcelCrossdock(parameterMap, excelFormat),
				fileName, response);
    }
}
