package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.vinflux.adm.item.service.ItemTrans;
import com.vinflux.adm.item.service.SubstitutionUploadService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.UnicodeUtil;
import com.vinflux.framework.web.CommandController;

@Controller("substitutionUploadController")
@RemotingDestination
public class SubstitutionUploadController extends CommandController {

	@Resource(name = "substitutionUploadService")
	private SubstitutionUploadService substitutionUploadService;
	
	@Resource(name = "itemTrans")
	private ItemTrans itemTrans;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(SubstitutionUploadController.class);
	
    /**
     * ADM > 물품 > 물품 업로드 > 물품 업로드 임시 테이블 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionUploadController/selectSubstitutionUploadInfo.do")
    @ResponseBody   
    public CommandMap selectSubstitutionUploadInfo (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.selectSubstitutionUploadInfo.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map substitutionUploadMap = substitutionUploadService.selectSubstitutionUploadInfo(paramMap);
    	Integer count = MapUtil.getInt(substitutionUploadMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)substitutionUploadMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.selectSubstitutionUploadInfo.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드
     * @param requestMap
     * @return 
     * @throws Exception
     */
    @RequestMapping(value="/substitutionUploadController/uploadSubstitutionExcel.do")
    public void uploadSubstitutionExcel (Map<String,Object> commandMap, HttpServletRequest request , HttpServletResponse response) throws Exception {

    	CommandMap requestMap  = JsonUtil.jsonCovertCommandMap(UnicodeUtil.decode(MapUtil.getStr(commandMap, "commandParam")));
		Map paramMap = new HashMap();
		paramMap.put("userinfo", requestMap.getCommonUserInfo());
		
    	MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multiRequest.getFileMap();				

	    Iterator<Entry<String, MultipartFile>> itr = fileMap.entrySet().iterator();
		MultipartFile file;

		boolean transactionStatus = false; 
		int errorCode = 101;
		Map argMap = new HashMap();
		String ulhsKey = "";
	    
		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();

			file = entry.getValue();
			if (!"".equals(file.getOriginalFilename())) {

				if (file.getOriginalFilename().endsWith(".xls")
//						|| file.getOriginalFilename().endsWith(".xlsx")
//						|| file.getOriginalFilename().endsWith(".XLSX")
						|| file.getOriginalFilename().endsWith(".XLS")
					) {
					
					try {
						ulhsKey = substitutionUploadService.uploadSubstitutionExcel(paramMap, file.getInputStream() , file.getOriginalFilename());
						transactionStatus = true;
					} catch (MsgException me) {
						LOG.error("", me);
						errorCode = me.getCode();
						argMap = me.getArgMap();
					} catch (Exception e) {
						
						LOG.error("", e);
						errorCode = 101;
					}
					
				}else{
					errorCode = 102;
				}
			}
		}
		ExcelUtil.throwMessage(response, transactionStatus, argMap, errorCode, ulhsKey);
    } 
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드후 그리드 출력
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionUploadController/selectSubstitutionTemp.do")
    @ResponseBody   
    public CommandMap selectSubstitutionTemp(@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.selectSubstitutionTemp.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	
    	Map param = requestMap.getParamMap();
    	param.putAll(param);    	
    	
    	Map substitutuonTempMap = substitutionUploadService.selectSubstitutionTemp(param);
    	Integer count = MapUtil.getInt(substitutuonTempMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)substitutuonTempMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.selectSubstitutionTemp.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 업로드 > 엑셀 업로드후 저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionUploadController/saveSubstitution.do")
    @ResponseBody   
    public CommandMap saveSubstitution (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.saveSubstitution.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	List list = substitutionUploadService.selectSubstitutionTempAll(paramMap);
    	paramMap.put("lakey", requestMap.getCommonUserInfoMap().get("laKey"));
    	
    	Map rtnMap = itemTrans.saveSubstitutionUpload(paramMap , list);
    	
    	Integer count = MapUtil.getInt(rtnMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)rtnMap.get("list"));
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.saveSubstitution.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }   
    
    /**
     * ADM > 물품 > 물품 업로드 > 물품 업로드 임시테이블 엑셀 다운
     * @param requestMap
     * @return 
     * @throws Exception
     */
    @RequestMapping(value="/substitutionUploadController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("SubstitutionUploadController.excelDown.requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = substitutionUploadService.selectExcelDownSubstitutionUpload(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(substitutionUploadService
				.selectExcelDownSubstitutionUpload(parameterMap, excelFormat), fileName,
				response);
    }      
}