package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.ItemCodeService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.web.CommandController;

@Controller("itemCodeController")
@RemotingDestination
public class ItemCodeController extends CommandController {

	@Resource(name = "itemCodeService")
	private ItemCodeService itemCodeService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ItemCodeController.class);
	
    /**
     * ADM > 상품 > 상품 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/selectItemCodeInfo.do")
    @ResponseBody   
    public CommandMap selectItemCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param); 
    	/* 대소구분없이 변경되도록 수정 2018-10-23*/
    	String sScond = (String)paramMap.get("SEARCH_CONDITION");
    	sScond = StringUtil.replace(sScond, "#{ICKEY)}", "UPPER(#{ICKEY)})");
    	sScond = StringUtil.replace(sScond, "#{ICNAME)}", "UPPER(#{ICNAME)})");
    	paramMap.put("SEARCH_CONDITION", sScond);
    	
    	Map itemCodeMap = itemCodeService.selectItemCodeInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 상품 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/selectItemCode.do")
    @ResponseBody   
    public CommandMap selectItemCode (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map map = itemCodeService.selectItemCode(requestMap.getParamMap());
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    /**
     * ADM > 상품 > 상품 > wms 가용재고 chec
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/selectItemCodeAvqtyCheck.do")
    @ResponseBody   
    public CommandMap selectItemCodeAvqtyCheck (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemCodeAvqtyCheck_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	
    	requestMap.addList("rtnGrid", itemCodeService.selectItemCodeAvqtyCheck(paramMap));
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemCodeAvqtyCheck_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * 화주변경 일괄저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/saveOwItemCode.do")
    @ResponseBody   
    public CommandMap saveOwItemCode (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	itemCodeService.insertOwItemCode(requestMap);
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 상품 > 상품 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/saveItemCode.do")
    @ResponseBody   
    public CommandMap saveItemCode (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	
    	if ("INSERT".equals(workType) ) {
    		itemCodeService.insertItemCode(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		itemCodeService.updateItemCode(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		Map resultMap = itemCodeService.deleteItemCode(requestMap);
    		List resultList = new ArrayList();
    		resultList.add(resultMap);
    		requestMap.addList("resultData", resultList);
    	} else if ("SCM".equals(workType) ) {
	    	List list = requestMap.getList("SCM_LIST");
	    	String scm_workType = (String)paramMap.get("scm_workType");
	    		    	
	    	for (int i=0 ;list.size() > i ; i++  ) {
	    		Map row = (Map)list.get(i);
	    		row.put("scmyn", scm_workType);
	    		itemCodeService.updateSCM(row);
	    	}
	    }
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 상품 > 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/selectItemCodeCenter.do")
    @ResponseBody   
    public CommandMap selectItemCodeCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	requestMap.addList("rtnGrid1", itemCodeService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", itemCodeService.selectTAdminMstIcxct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 상품 > 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/saveItemCodeCenter.do")
    @ResponseBody   
    public CommandMap saveItemCodeCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	itemCodeService.saveItemCodeCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    /**
     * ADM > 상품 > 상품 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemCodeController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	Map userMap = (Map)requestMap.getCommon("USER_INFO");
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);
    	parameterMap.put("lakey", (String)userMap.get("laKey"));
    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = itemCodeService.selectExcelDownItemCode(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
    	ExcelWriteHandler.downloadExcelFile(
    			itemCodeService.selectExcelDownItemCode(parameterMap, excelFormat),
				fileName, response);
    }    
    
    
    public CommandMap validCntrCd(CommandMap requesMap) throws Exception{
    	Map paramMap = requesMap.getParamMap();
    	List icxctList = requesMap.getList("SAVE_LIST");
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + paramMap.toString());
    	LOG.debug("IcxctList => " + icxctList.toString());
    	LOG.debug("======================================");
    	
    	boolean result = itemCodeService.validCntrCd(paramMap, icxctList);
    	Map resultMap = new HashMap();
    	List resultList = new ArrayList();
    	System.out.println("result::    " + result);
    	if(result){
    		resultMap.put("result","true");
    	}else{
    		resultMap.put("result","false");
    	}
    	
    	System.out.println("result::    " + resultMap.toString());
    	
    	resultList.add(resultMap);
    	requesMap.addList("resultData", resultList);
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requesMap.toString());
    	LOG.debug("======================================");
    	
    	return requesMap;
    }
    
}