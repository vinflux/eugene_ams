package com.vinflux.adm.item.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.SetItemCodeService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("setItemCodeController")
@RemotingDestination
public class SetItemCodeController extends CommandController {
	
	@Resource(name="setItemCodeService")
	private SetItemCodeService setItemCodeService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(SetItemCodeController.class);

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/selectSetItemCodeHd.do")
    @ResponseBody   
    public CommandMap selectSetItemCodeHd (@RequestBody CommandMap requestMap) throws Exception {

		Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);    	

		Map setItemCode = setItemCodeService.selectSetItemCodeHd(paramMap);
    	Integer count = MapUtil.getInt(setItemCode, "cnt");

    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)setItemCode.get("list"));
    	}

   		return requestMap;
    }
    /**
     * AMS > 상품관리 > 물류 유통가공 상품 > 수정
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/selectSetItemCodeSpec.do")
    @ResponseBody   
    public CommandMap selectSetItemCodeSpec (@RequestBody CommandMap requestMap) throws Exception {
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	Map map = setItemCodeService.selectSetItemCodeSpec(paramMap);
    	List list = new ArrayList();
    	list.add(map);
		requestMap.addList("rtnGrid", list);
		
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/selectSetItemCodeDt.do")
    @ResponseBody   
    public CommandMap selectSetItemCodeDt (@RequestBody CommandMap requestMap) throws Exception {

    	Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);    	

		Map setItemCode = setItemCodeService.selectSetItemCodeDt(param);
		Integer count = MapUtil.getInt(setItemCode, "cnt");

    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)setItemCode.get("list"));
    	}
    	
    	LOG.debug("======================================");
	   	LOG.debug("requestMap => " + requestMap);
	   	LOG.debug("count=> " + count);
	   	LOG.debug("======================================");
	   	
    	
	   return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/checkSetItemCodeHdInfo.do")
    @ResponseBody   
    public CommandMap checkSetItemCodeHdInfo (@RequestBody CommandMap requestMap) throws Exception {

     	LOG.debug("======================================");
 	   	LOG.debug("requsetMap => " + requestMap);
 	   	LOG.debug("requsetMap orcd_hdkey VALUE=> " + requestMap.getParamMap().get("seickey"));
 	   	LOG.debug("======================================");
 	   	
 	   	Map param = requestMap.getParamMap();
 	   	
 	   	int count = setItemCodeService.selectSetItemCodeHdCount(param);

	   	if (count == 0 ) {
	   		requestMap.putParam("isDuplicate", "false");
	   	}
	   	else{
	   		requestMap.putParam("isDuplicate", "true");
	   	}
	   	    	
	   	LOG.debug("======================================");
	   	LOG.debug("resultMap Size => " + Integer.toString(count));
	   	LOG.debug("resultMap check adcd_hdkey => " + requestMap.getParam("isDuplicate"));
	   	LOG.debug("======================================");
	   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/checkSetItemCodeDtInfo.do")
    @ResponseBody   
    public CommandMap checkSetItemCodeDtInfo (@RequestBody CommandMap requestMap) throws Exception {

     	LOG.debug("======================================");
 	   	LOG.debug("requsetMap => " + requestMap);
 	   	LOG.debug("requsetMap orcd_hdkey VALUE=> " + requestMap.getParamMap().get("seickey"));
 	   	LOG.debug("======================================");
 	   	
 	   	Map param = requestMap.getParamMap();

	   	int count = setItemCodeService.selectSetItemCodeDtCount(param);

	   	if (count == 0 ) {
	   		requestMap.putParam("isDuplicate", "false");
	   	}
	   	else{
	   		requestMap.putParam("isDuplicate", "true");
	   	}
	   	    	
	   	LOG.debug("======================================");
	   	LOG.debug("resultMap Size => " + Integer.toString(count));
	   	LOG.debug("resultMap check adcd_hdkey => " + requestMap.getParam("isDuplicate"));
	   	LOG.debug("======================================");
	   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 생성
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/insertSetItemCodeHd.do")
    @ResponseBody   
    public CommandMap insertSetItemCodeHd (@RequestBody CommandMap requestMap) throws Exception {

	   	LOG.debug("======================================");
	   	LOG.debug("requsetMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	setItemCodeService.insertSetItemCodeHd(requestMap.getMap("setItemCodeMap"));

	   	LOG.debug("======================================");
	   	LOG.debug("resultMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 생성
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/insertSetItemCodeDt.do")
    @ResponseBody   
    public CommandMap insertSetItemCodeDt (@RequestBody CommandMap requestMap) throws Exception {

	   	LOG.debug("======================================");
	   	LOG.debug("requsetMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	setItemCodeService.insertSetItemCodeDt(requestMap.getMap("setItemCodeMap"));

	   	LOG.debug("======================================");
	   	LOG.debug("resultMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 갱신
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/updateSetItemCodeHd.do")
    @ResponseBody   
    public CommandMap updateSetItemCodeHd (@RequestBody CommandMap requestMap) throws Exception {

	   	LOG.debug("======================================");
	   	LOG.debug("requsetMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	setItemCodeService.updateSetItemCodeHd(requestMap.getMap("setItemCodeMap"));

	   	LOG.debug("======================================");
	   	LOG.debug("resultMap => " + requestMap);
	   	LOG.debug("======================================");
   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 갱신
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/updateSetItemCodeDt.do")
    @ResponseBody   
    public CommandMap updateSetItemCodeDt (@RequestBody CommandMap requestMap) throws Exception {

	   	LOG.debug("======================================");
	   	LOG.debug("requsetMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
	   	setItemCodeService.updateSetItemCodeDt(requestMap.getMap("setItemCodeMap"));

	   	LOG.debug("======================================");
	   	LOG.debug("resultMap => " + requestMap);
	   	LOG.debug("======================================");
   	
	   	return requestMap;
    }

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 삭제
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/deleteSetItemCodeHd.do")
    @ResponseBody   
    public CommandMap deleteSetItemCodeHd (@RequestBody CommandMap requestMap) throws Exception {

       	LOG.debug("======================================");
       	LOG.debug("requsetMap => " + requestMap);
       	LOG.debug("======================================");
       	
       	setItemCodeService.deleteSetItemCodeHd(requestMap);
    		
       	return requestMap;
    }	

    /**
     * ICOM > 주문 마스터 관리 > SET 물품 > 삭제
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/deleteSetItemCodeDt.do")
    @ResponseBody   
    public CommandMap deleteSetItemCodeDt (@RequestBody CommandMap requestMap) throws Exception {

        LOG.debug("======================================");
       	LOG.debug("requsetMap => " + requestMap);
       	LOG.debug("======================================");
       	
       	setItemCodeService.deleteSetItemCodeDt(requestMap);

       	return requestMap;
    }

    /**
 	 * ICOM > 주문 마스터 관리 > 주문 물품 맵핑 >  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/excelDownHD.do")
    @ResponseBody   
    public void excelDownHD (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = setItemCodeService.selectSetItemCodeExcelHD(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(setItemCodeService
				.selectSetItemCodeExcelHD(parameterMap, excelFormat), fileName,
				response);
    }   

    /**
 	 * ICOM > 주문 마스터 관리 > 주문 물품 맵핑 >  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/setItemCodeController/excelDownDtl.do")
    @ResponseBody   
    public void excelDownDtl (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = setItemCodeService.selectSetItemCodeExcelDT(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(setItemCodeService
				.selectSetItemCodeExcelDT(parameterMap, excelFormat), fileName,
				response);
    }   
  
}
