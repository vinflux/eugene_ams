package com.vinflux.adm.item.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.MixShipItemImpossService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("mixShipItemImpossController")
@RemotingDestination
public class MixShipItemImpossController extends CommandController {

	@Resource(name="mixShipItemImpossService")
	private MixShipItemImpossService mixShipItemImpossService;
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(MixShipItemImpossController.class);
		
	
	 /**
     * AMS > 상품 관리 > 혼적불가상품 > 헤더 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
	
	@RequestMapping(value="mixShipItemImpossController/selectMixShipItemImpossHd.do")
	@ResponseBody
	public CommandMap selectMixShipItemImpossHd (@RequestBody CommandMap requestMap) throws Exception {
		
		Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);    	
		
		Map mixShipItemImposs = mixShipItemImpossService.selectMixShipItemImpossHd(paramMap);
    	Integer count = MapUtil.getInt(mixShipItemImposs, "cnt");

    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)mixShipItemImposs.get("list"));
    	}

   		return requestMap;
	}


    /**
     * AMS > 상품 관리 > 혼적불가상품 > 디테일 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/mixShipItemImpossController/selectMixShipItemImpossDt.do")
    @ResponseBody   
    public CommandMap selectMixShipItemImpossDt (@RequestBody CommandMap requestMap) throws Exception {

    	Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);    	

		Map setItemCode = mixShipItemImpossService.selectMixShipItemImpossDt(param);
		Integer count = MapUtil.getInt(setItemCode, "cnt");

    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)setItemCode.get("list"));
    	}
    	
    	LOG.debug("======================================");
	   	LOG.debug("requestMap => " + requestMap);
	   	LOG.debug("======================================");
	   	
    	
	   return requestMap;
    }

    
    /**
     * AMS > 상품 관리 > 혼적불가상품 > 헤더 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/mixShipItemImpossController/saveMixShipItemImposs.do")
    @ResponseBody   
    public CommandMap saveMixShipItemImposs (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = requestMap.getList("INPUT_LIST");
    	List returnList = new ArrayList();
			mixShipItemImpossService.saveMixShipItemImposs(list);
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	return requestMap;
    }  
	
    /**
     * AMS > 상품 관리 > 혼적불가상품 > 디테일 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    
    @RequestMapping(value="/mixShipItemImpossController/saveDetailMixShipItemImposs.do")
    @ResponseBody   
    public CommandMap saveDetailMixShipItemImposs (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = requestMap.getList("INPUT_LIST");
    	Map param = requestMap.getParamMap();
    	List returnList = new ArrayList();
    	
    	
    	mixShipItemImpossService.saveDetailMixShipItemImposs(list);
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }      

    /**
   	 * AMS > 상품 관리 > 혼적불가상품 > 헤더 엑셀다운로드
       * @param requestMap
       * @return
       * @throws Exception
       */
      @RequestMapping(value="/mixShipItemImpossController/excelDownMixShipItemImposs.do")
      @ResponseBody   
      public void excelDownMixShipItemImposs (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
      	
      	// JSON parsing 
      	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
      	
      	// Search condition setting 
      	Map paramMap = requestMap.getSearchQuery();
      	Map param = requestMap.getParamMap();
      	paramMap.putAll(param);    	

      	// excel parameter setting 
      	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
      	String fileName = (String)param.get("EXCEL_FILENAME");
      	
      	ExcelWriteHandler.downloadExcelFile(
      			mixShipItemImpossService.excelDownMixShipItemImposs(paramMap, excelFormat),
      			fileName, response);
      }   

      /**
   	 * AMS > 상품 관리 > 혼적불가상품 > 디테일 엑셀다운로드
       * @param requestMap
       * @return
       * @throws Exception
       */
    	@RequestMapping(value="/mixShipItemImpossController/excelDownDetailMixShipItemImposs.do")
      @ResponseBody   
      public void excelDownDetailMixShipItemImposs (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
      	
      	// JSON parsing 
      	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
      	
      	// Search condition setting 
      	Map paramMap = requestMap.getSearchQuery();
      	Map param = requestMap.getParamMap();
      	paramMap.putAll(param);    	

      	// excel parameter setting 
      	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
      	String fileName = (String)param.get("EXCEL_FILENAME");
      	
      	ExcelWriteHandler.downloadExcelFile(
      			mixShipItemImpossService.excelDownDetailMixShipItemImposs(paramMap, excelFormat),
      			fileName, response);
      }   
}
