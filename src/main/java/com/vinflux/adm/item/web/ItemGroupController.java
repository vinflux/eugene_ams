package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.ItemGroupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("itemGroupController")
@RemotingDestination
public class ItemGroupController extends CommandController {

	@Resource(name = "itemGroupService")
	private ItemGroupService itemGroupService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ItemGroupController.class);
	
    /**
     * ADM > 물품 > 물품 그룹 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/selectItemGroupInfo.do")
    @ResponseBody   
    public CommandMap selectItemGroupInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = itemGroupService.selectItemGroupInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
	
    /**
     * ADM > 물품 > 물품 그룹 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/selectItemGroupCode.do")
    @ResponseBody   
    public CommandMap selectItemGroupCode (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	requestMap.addList("rtnGrid", itemGroupService.selectItemGroupCode(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 그룹 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/selectItemGroup.do")
    @ResponseBody   
    public CommandMap selectItemGroup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map map = itemGroupService.selectItemGroup(requestMap.getParamMap());
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * 화주변경 일괄저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/saveOwItemGroup.do")
    @ResponseBody   
    public CommandMap saveOwItemGroup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	itemGroupService.insertOwItemGroup(requestMap);
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 물품 > 물품 그룹 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/saveItemGroup.do")
    @ResponseBody   
    public CommandMap saveItemGroup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		itemGroupService.insertItemGroup(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		itemGroupService.updateItemGroup(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			itemGroupService.deleteItemGroup(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 그룹 > 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/selectItemGroupCenter.do")
    @ResponseBody   
    public CommandMap selectItemGroupCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	requestMap.addList("rtnGrid1", itemGroupService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", itemGroupService.selectTAdminMstIcgrxct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 그룹 > 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/saveItemGroupCenter.do")
    @ResponseBody   
    public CommandMap saveItemGroupCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	itemGroupService.saveItemGroupCenter(requestMap.getParamMap() , requestMap.getList("list"));

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 물품 > 물품 그룹 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemGroupController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = itemGroupService.selectExcelDownItemGroup(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				itemGroupService.selectExcelDownItemGroup(parameterMap, excelFormat), fileName,
				response);
    }      
    
}
