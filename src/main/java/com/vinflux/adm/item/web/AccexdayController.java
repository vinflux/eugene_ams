package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.AccexdayService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("accexdayController")
@RemotingDestination
public class AccexdayController extends CommandController {

	@Resource(name = "accexdayService")
	private AccexdayService accexdayService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccexdayController.class);
	
    /**
     * ADM > 상품관리 > 체화예정재고 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accexdayController/selectAccexday.do")
    @ResponseBody   
    public CommandMap selectAccexday (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);  
    	if("IC".equals((String)param.get("tabType"))){
    		requestMap.putParam("accexday_flag", (String)paramMap.get("ACCEXDAY_FLAG"));
    	}
    	
    	Map itemCodeMap = accexdayService.selectAccexday(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * ADM > 상품관리 > 체화예정재고 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accexdayController/updateAccexday.do")
    @ResponseBody   
    public CommandMap updateAccexday (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
//		String result = "";
//		String message = "";
		
//    	Map paramMap = requestMap.getParamMap();
    	accexdayService.updateAccexday(requestMap);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품관리 > 체화예정재고 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accexdayController/excelDown.do")
    @ResponseBody   
    public void selectExcelAccexday (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = accexdayService.selectExcelDownDic(paramMap);

    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				accexdayService.selectExcelAccexday(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * ADM > 상품관리 > 체화예정재고 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accexdayController/selectAccexdayPop.do")
    @ResponseBody   
    public CommandMap selectAccexdayPop (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = accexdayService.selectAccexdayPop(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("CTG_LEVEL_LIST", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
}
