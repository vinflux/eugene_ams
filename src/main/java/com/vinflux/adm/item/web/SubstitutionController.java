package com.vinflux.adm.item.web;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.SubstitutionService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.web.CommandController;

@Controller("substitutionController")
@RemotingDestination
public class SubstitutionController extends CommandController {

	@Resource(name = "substitutionService")
	private SubstitutionService substitutionService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(SubstitutionController.class);
    
    /**
	 * ADM > 물품 > 물품 대체 코드 > PK 체크
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/substitutionController/selectPkCountSubstitution.do")
    @RemotingInclude // BDS
    @ResponseBody  
	public CommandMap selectPkCountSubstitution (@RequestBody CommandMap requestMap) throws Exception {
    	Map paramMap = requestMap.getParamMap(); 
    	int cnt = substitutionService.selectPkCountSubstitution(paramMap);
    	requestMap.putParam("CNT", Integer.toString(cnt));
		return requestMap;
    }
    
    /**
     * ADM > 물품  > 물품대체코드 > 상단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/selectListSubstitution.do")
    @ResponseBody   
    public CommandMap selectListSubstitution (@RequestBody CommandMap requestMap) throws Exception {

    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	//건수 가져오기
    	int cnt = substitutionService.selectPkCountSubstitution(paramMap);
    	List list = substitutionService.selectListSubstitution(paramMap);

    	if (cnt > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(cnt));
    		requestMap.addList("rtnGrid", list);
    	}

    	return requestMap;
    }
    
    /**
     * ADM > 물품  > 물품대체코드 > 물품 대체코드 PK검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/selectPkSubstitution.do")
    @ResponseBody   
    public CommandMap selectPkSubstitution (@RequestBody CommandMap requestMap) throws Exception {
     
    	Map paramMap = requestMap.getParamMap();
    	
    	Map substitutionMap = this.substitutionService.selectPkSubstitution(paramMap);
    	requestMap.putMap("substitutionMap", substitutionMap);

    	return requestMap;
    }
    
    /**
     * ADM > 물품  > 물품대체코드 > 하단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/selectSubstitutionCenter.do")
    @ResponseBody   
    public CommandMap selectSubstitutionCenter (@RequestBody CommandMap requestMap) throws Exception {
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
 
    	requestMap.addList("rtnGrid1", this.substitutionService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", this.substitutionService.selectTAdminMstIcsbxct(paramMap));

    	return requestMap;
    }
    
    /**
     * ADM > 물품  > 물품대체코드 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/saveSubstitution.do")
    @ResponseBody   
    public CommandMap saveSubstitution (@RequestBody CommandMap requestMap) throws Exception {

    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) { // 등록
    		substitutionService.insertSubstitution(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) { // 수정
    		substitutionService.updateSubstitution(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) { // 삭제
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			substitutionService.deleteSubstitution(row);
    		}
    	}

    	return requestMap;
    }
    
    /**
     * ADM > 물품  > 물품대체코드 > 하단  저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/saveSubstitutionCenter.do")
    @ResponseBody   
    public CommandMap saveSubstitutionCenter (@RequestBody CommandMap requestMap) throws Exception {    	
    	this.substitutionService.saveSubstitutionCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	return requestMap;
    }    

    /**
     * ADM > 물품  > 물품대체코드 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/substitutionController/selectListSubstitutionExcel.do")
    @ResponseBody   
    public void selectListSubstitutionExcel (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = substitutionService.selectListSubstitutionExcel(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(substitutionService
				.selectListSubstitutionExcel(parameterMap, excelFormat),
				fileName, response);
    }      
    
}
