package com.vinflux.adm.item.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.item.service.ItemUnitService;
import com.vinflux.adm.master.service.CenterService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("itemUnitController")
@RemotingDestination
public class ItemUnitController extends CommandController {

	@Resource(name = "itemUnitService")
	private ItemUnitService itemUnitService;
	
	@Resource(name = "centerService")
	private CenterService centerService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ItemUnitController.class);
	
    /**
     * ADM > 물품 > 물품 단위 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/selectItemUnitInfo.do")
    @ResponseBody   
    public CommandMap selectItemUnitInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnitInfo.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = itemUnitService.selectItemUnitInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnitInfo.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 단위 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/selectItemUnit.do")
    @ResponseBody   
    public CommandMap selectItemUnit (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnit.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = itemUnitService.selectItemUnitType(requestMap.getParamMap());
    	if (list == null || list.isEmpty() ) {
    		requestMap.putMap("rtnMap", null);	
    	} else { 
    		requestMap.putMap("rtnMap", (Map)list.get(0));
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnit.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 단위 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/saveItemUnit.do")
    @ResponseBody   
    public CommandMap saveItemUnit (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("saveItemUnit.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		itemUnitService.insertItemUnit(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		itemUnitService.updateItemUnit(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		itemUnitService.deleteItemUnit(requestMap);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 단위 > 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/selectItemUnitCenter.do")
    @ResponseBody   
    public CommandMap selectItemUnitCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnitCenter.requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	
    	requestMap.addList("rtnGrid1", centerService.getTAdminMstCtList(paramMap));
    	requestMap.addList("rtnGrid2", itemUnitService.selectTAdminMstIcutxtypexct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("selectItemUnitCenter.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 물품 > 물품 단위 > 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/saveItemUnitCenter.do")
    @ResponseBody   
    public CommandMap saveItemUnitCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("saveItemUnitCenter.requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	itemUnitService.saveItemUnitCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("saveItemUnitCenter.resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 물품 > 물품 단위 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/itemUnitController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("excelDown.requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	 
    	// db selsect
//    	List excelList = itemUnitService.selectExcelDownItemUnit(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				itemUnitService.selectExcelDownItemUnit(parameterMap, excelFormat), fileName,
				response);
    }      
    
}
