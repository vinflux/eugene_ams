package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.StoreevService;
import com.vinflux.adm.persistence.StoreevDAO;


@Service("storeevService")
public class StoreevServiceImpl implements StoreevService {
	
	@Resource(name="storeevDAO")
	private StoreevDAO storeevDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectStoreevInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storeevDAO.selectStoreevCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeevDAO.selectStoreev(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectExcelDownStoreev (Map parameterMap) throws Exception {
    	return storeevDAO.selectExcelDownStoreev(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownStoreev(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return storeevDAO.selectExcelDownStoreev(parameterMap, excelFormat);
	}
}