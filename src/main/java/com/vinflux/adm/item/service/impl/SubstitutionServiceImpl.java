package com.vinflux.adm.item.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.item.service.SubstitutionService;
import com.vinflux.adm.persistence.SubstitutionDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.tms.persistence.SubstitutionTmsDAO;
import com.vinflux.wms.persistence.SubstitutionWmsDAO;

@Service("substitutionService")
public class SubstitutionServiceImpl implements SubstitutionService {
	
	
	@Resource(name="substitutionDAO")
	private SubstitutionDAO substitutionDAO;
	
	@Resource(name="substitutionWmsDAO")
	private SubstitutionWmsDAO substitutionWmsDAO;	
	
//	@Resource(name="substitutionIcomDAO")
//	private SubstitutionIcomDAO substitutionIcomDAO;
	
	@Resource(name="substitutionTmsDAO")
	private SubstitutionTmsDAO substitutionTmsDAO;
	
//	@Resource(name="substitutionVimsDAO")
//	private SubstitutionVimsDAO substitutionVimsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;	
	
	protected static final Log LOG = LogFactory.getLog(SubstitutionServiceImpl.class);
	
	@Override
	public int selectPkCountSubstitution(Map parameterMap) throws Exception {
		return this.substitutionDAO.selectPkCountSubstitution(parameterMap);
	}
	
	@Override
	public List selectListSubstitution(Map parameterMap) throws Exception {
		return this.substitutionDAO.selectListSubstitution(parameterMap);
	}
	
	@Override
	public Map selectPkSubstitution(Map parameterMap) throws Exception {
		return this.substitutionDAO.selectPkSubstitution(parameterMap);
	}
	
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return substitutionDAO.selectTAdminMstCt(parameterMap);
	}
	
	
	@Override
	public List selectTAdminMstIcsbxct(Map parameterMap) throws Exception {
		return substitutionDAO.selectTAdminMstIcsbxct(parameterMap);
	}
	
	@Override
	public void insertSubstitution (Map parameterMap) throws Exception {
		
		//상품대체코드가 존재하는지 확인
		int count = this.selectPkCountSubstitution(parameterMap);
		
		//count가 0보다크면 전체갯수 + udf1의 값에 +1을 세팅
		if (count > 0) {
			this.substitutionDAO.updateTAdminMstIcsb(parameterMap);
		} else {
			this.substitutionDAO.insertTAdminMstIcsb(parameterMap);
		}
		
		int tTcount = substitutionTmsDAO.selectPkCountTtMstIcsb(parameterMap);
		
		if(tTcount>0){		
			substitutionTmsDAO.updateTTMstIcsb(parameterMap);
		}else{
			if( "YES".equals(this.commonService.getAdmConfigVal("USETMS")) ) {
				this.substitutionTmsDAO.insertTTMstIcsb(parameterMap);
			}
		}

	}
	
	@Override
	public void updateSubstitution (Map parameterMap) throws Exception {
		
		int count = this.selectPkCountSubstitution(parameterMap);
		if (count < 1) {
			throw new MsgException(10003); //선택된 데이터가 존재하지 않아 수정할 수 없습니다.
		} else {
			
			this.substitutionDAO.updateTAdminMstIcsb(parameterMap);
			
			int mocnt = 0;
			
			/* 10.21 OMS 삭제작업
			if( "YES".equals(this.commonService.getAdmConfigVal("USEICOM")) ) {
				mocnt = this.substitutionIcomDAO.updateTOrderMstIcsb(parameterMap);
				if(mocnt==0){
					this.substitutionIcomDAO.insertTOrderMstIcsb(parameterMap);
				}
			}*/
			
			if( "YES".equals(this.commonService.getAdmConfigVal("USETMS")) ) {
				mocnt = this.substitutionTmsDAO.updateTTMstIcsb(parameterMap);
				if(mocnt==0){
					this.substitutionTmsDAO.insertTTMstIcsb(parameterMap);
				}
			}
			
			/*
			if( "YES".equals(this.commonService.getAdmConfigVal("USEVIMS")) ) {
				mocnt = this.substitutionVimsDAO.updateTViewMstIcsb(parameterMap);
				if(mocnt==0){
					this.substitutionVimsDAO.insertTViewMstIcsb(parameterMap);
				}
			}
			*/
			
			if( "YES".equals(this.commonService.getAdmConfigVal("USEWM")) ) {
				int wmsCount = this.substitutionWmsDAO.selectCountTWorkMstIcsb(parameterMap);
				if (wmsCount > 0) {
					this.substitutionWmsDAO.updateTWorkMstIcsb(parameterMap);
				} 
			}
		}
	}

	@Override
	public void deleteSubstitution (Map parameterMap) throws Exception {
		
		//wms
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			substitutionWmsDAO.deleteTWorkMstIcsb(parameterMap);
		}
		
		//icom
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			substitutionIcomDAO.deleteTOrderMstIcsb(parameterMap);
		}*/
		
		//tms
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			substitutionTmsDAO.deleteTTMstIcsb(parameterMap);
		}
		
		//vims
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			substitutionVimsDAO.deleteTViewMstIcsb(parameterMap);
		}*/
		
		//admin(TADMIN_MST_ICSBXCT)
		substitutionDAO.deleteTAdminMstIcsbxct(parameterMap);
		//admin(TADMIN_MST_ICSB)
		substitutionDAO.deleteSubstitution(parameterMap);
		
	}
	
	@Override
	public void saveSubstitutionCenter(Map parameterMap ,  List parameterList) throws Exception {
		
		LOG.debug("parameterMap");
		LOG.debug(parameterMap);
		LOG.debug("parameterMap");
		
		LOG.debug("parameterList");
		LOG.debug(parameterList);
		LOG.debug("parameterList");

		//선택된 물품별 대체코드 마스터 데이타 가져오기
		//List itemSubstitutionList = this.selectListSubstitution(parameterMap);
		Map itemSubstitutionMaster = this.selectPkSubstitution(parameterMap);
		
		LOG.debug("itemSubstitutionMaster");
		LOG.debug(itemSubstitutionMaster);
		LOG.debug("itemSubstitutionMaster");
		
		//DB저장센터
		List icsbxctCenterList = substitutionDAO.selectTAdminMstIcsbxct(parameterMap); //DB에 저장되어 있는 하단 우측 센터별 물품 대체코드 리스트

			
		//화면에서 넘어온 센터 리스트
		if( parameterList!=null && !parameterList.isEmpty() ) {
				
			boolean insert = false;
				
			for (int i=0; parameterList.size()>i; i++) { //하단 우측에 표시된 센터별 물품 대체코드 리스트											
					
				insert = true;
				Map itemSubstitutionCenter = (Map) parameterList.get(i);	//화면
				String masterCenter = (String) itemSubstitutionCenter.get("ctkey");			
					
				for(int j=0; icsbxctCenterList.size()>j; j++) {	//DB에 저장되어 있는 하단 우측 센터별 물품 대체코드 리스트							
							
					Map icsbxctCenterMap = (Map)icsbxctCenterList.get(j); 		//DB			
					String icsbxceCenter = (String) icsbxctCenterMap.get("ctkey");
							
					if (masterCenter.equals(icsbxceCenter)) {				
						insert = false;
						break;
					}		
				}					
					
				//추가분 입력
				if( insert ){
					
					itemSubstitutionMaster.put("ctkey", masterCenter);
					
					int icxCtCnt = substitutionDAO.selectPkCountIcxCt(itemSubstitutionMaster);
					if (icxCtCnt == 0) {
						Map argMap = new HashMap();				// NOPMD - 내부 처리를 위한 생성 [ sw.yoo ]
						argMap.put("CTKEY", masterCenter);
						throw new MsgException(10021, argMap); // [ctkey]에서 유효하지 않은 물품 코드 입니다
					}
							
					int icxIcutxTypexCtCnt = substitutionDAO.selectPkCountIcuttypexIcxCt(itemSubstitutionMaster);
					if (icxIcutxTypexCtCnt == 0) {
						Map argMap = new HashMap();				// NOPMD - 내부 처리를 위한 생성 [ sw.yoo ]
						argMap.put("CTKEY", masterCenter);
						throw new MsgException(10022, argMap); // [ctkey]에서 유효하지 않은 물품 단위 유형 입니다
					}
						
					//admin 입력	
					//parameterMap.put("ctkey", masterCenter);
					int icsbCnt = substitutionDAO.selectPkCountTadminMstIcsbxct (itemSubstitutionMaster);
					if(icsbCnt>0){
						substitutionDAO.updateTAdminMstIcsbxct(itemSubstitutionMaster);
					}else{
						substitutionDAO.insertTAdminMstIcsbxct(itemSubstitutionMaster);
					}
					
					int icsbTTCnt = substitutionTmsDAO.selectPkCountTtMstIcsbxct(itemSubstitutionMaster);
					if(icsbTTCnt>0){
						substitutionTmsDAO.updateTTMstIcsbxct(parameterMap);
					}else{
						substitutionTmsDAO.insertTTMstIcsbxct(itemSubstitutionMaster);	
					}
											
					
					//wms 입력
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {	
						int icsbWmCnt = substitutionWmsDAO.selectCountTWorkMstIcsb(itemSubstitutionMaster);
						if(icsbWmCnt>0){
							substitutionWmsDAO.updateTWorkMstIcsb(itemSubstitutionMaster);
						}else{
							substitutionWmsDAO.insertTWorkMstIcsb(itemSubstitutionMaster);	
						}
						
					}
				}	
			}
		}
				
				
		//삭제
		if( icsbxctCenterList!=null && !icsbxctCenterList.isEmpty()){	
				
			boolean delete = false;
			for (int i=0; icsbxctCenterList.size()>i; i++)
			{				
				Map icsbxctCenterMap = (Map)icsbxctCenterList.get(i); 
											
				delete = true;	
				for(int j=0; parameterList.size()>j; j++){	
								
					Map itemSubstitutionCenter = (Map) parameterList.get(j);
									
					String masterCenter = (String) itemSubstitutionCenter.get("ctkey");
					String icsbxceCenter = (String) icsbxctCenterMap.get("ctkey");
								
					if (masterCenter.equals(icsbxceCenter)) {				
		
						delete = false;
						break;
					}			
								
				}
					
				//삭제		
				if( delete ){
						
					
					parameterMap.put("ctkey", icsbxctCenterMap.get("ctkey"));
					
					//wms 삭제
					substitutionWmsDAO.deleteTWorkMstIcsb(parameterMap);
					
					//admin 삭제
					substitutionDAO.deleteTAdminMstIcsbxct(parameterMap);		
					
					//admin 삭제
					substitutionTmsDAO.deleteTTMstIcsbxct(parameterMap);	
								
				}						
			}	
		}
	}
	
	@Override
	public void saveSubstitutionCenter(Map parameterMap) throws Exception {
		int icxCtCnt = substitutionDAO.selectPkCountIcxCt(parameterMap);
		
		if (icxCtCnt == 0) {
			Map argMap = new HashMap();
			argMap.put("CTKEY", MapUtil.getStr(parameterMap, "ctkey"));
			throw new MsgException(10021, argMap); // [ctkey]에서 유효하지 않은 물품 코드 입니다
		}
				
		//int icxIcutxTypexCtCnt = substitutionDAO.selectPkCountIcuttypexIcxCt(parameterMap);
		
		//if (icxIcutxTypexCtCnt == 0) {
		//	Map argMap = new HashMap();
		//	argMap.put("CTKEY", MapUtil.getStr(parameterMap, "ctkey"));
		//	throw new MsgException(10022, argMap); // [ctkey]에서 유효하지 않은 물품 단위 유형 입니다
		//}
		
		//상품대체별센터 체크
		int icsbxCtCnt = substitutionDAO.selectPkCountTadminMstIcsbxct(parameterMap);		// NOPMD - 내부 로직 처리를 위해 변수 생성 [ sw.yoo ]
		
		//parameterMap.put("udf1", icsbxCtCnt+1);
			
		substitutionDAO.insertTAdminMstIcsbxct(parameterMap);
			
		//if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
		substitutionWmsDAO.insertTWorkMstIcsb(parameterMap);
			
		substitutionTmsDAO.insertTTMstxCtIcsb(parameterMap);
		//}
		
	}

	@Override
	public List selectListSubstitutionExcel(Map parameterMap) throws Exception {
		return substitutionDAO.selectListSubstitutionExcel(parameterMap);
	}
	
	@Override
	public Workbook selectListSubstitutionExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return substitutionDAO.selectListSubstitutionExcel(parameterMap, excelFormat);
	}
}