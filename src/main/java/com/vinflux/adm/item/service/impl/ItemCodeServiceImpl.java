package com.vinflux.adm.item.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.item.service.ItemCodeService;
import com.vinflux.adm.item.service.SubstitutionService;
import com.vinflux.adm.persistence.ItemCodeDAO;
import com.vinflux.adm.persistence.ItemGroupDAO;
import com.vinflux.adm.persistence.ItemUnitDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.tms.persistence.ItemCodeTmsDAO;
import com.vinflux.wms.persistence.ItemCodeWmsDAO;

@Service("itemCodeService")
public class ItemCodeServiceImpl implements ItemCodeService {
	
	@Resource(name="substitutionService")
	private SubstitutionService substitutionService;
	
	@Resource(name="itemCodeDAO")
	private ItemCodeDAO itemCodeDAO;

	@Resource(name="itemCodeWmsDAO")
	private ItemCodeWmsDAO itemCodeWmsDAO;
	
	@Resource(name="itemGroupDAO")
	private ItemGroupDAO itemGroupDAO;
	
	@Resource(name="itemUnitDAO")
	private ItemUnitDAO itemUnitDAO;
	
	@Resource(name="mstIcGenService")
	private IdGenService mstIcGenService;


//	@Resource(name="itemCodeIcomDAO")
//	private ItemCodeIcomDAO itemCodeIcomDAO;
	
//	@Resource(name="itemCodeVimsDAO")
//	private ItemCodeVimsDAO itemCodeVimsDAO;
	
	@Resource(name="itemCodeTmsDAO")
	private ItemCodeTmsDAO itemCodeTmsDAO;
	
//	@Resource(name="substitutionDAO")
//	private SubstitutionDAO substitutionDAO;
	
//	@Resource(name="substitutionWmsDAO")
//	private SubstitutionWmsDAO substitutionWmsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	protected static final Log LOG = LogFactory.getLog(ItemCodeServiceImpl.class);

	@Override
	public Map selectItemCodeInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = itemCodeDAO.selectItemCodeCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemCodeDAO.selectItemCode(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectItemCodeAvqtyCheck(Map parameterMap) throws Exception {
		return itemCodeDAO.selectItemCodeAvqtyCheck(parameterMap);
	}
	
	/** **/
	@Override
	public Map selectItemCode(Map parameterMap) throws Exception {
    	return itemCodeDAO.selectPkItemCode(parameterMap);
	}	

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#insertOwAccount(java.util.Map)
	 */
	@Override
	public void insertOwItemCode (CommandMap requestMap) throws Exception {
    	List list = requestMap.getList("list");
    	List returnList = new ArrayList();
    	Map paramMap = requestMap.getParamMap();
    	String owkey = (String) paramMap.get("owkey");
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			Map resultMap = selectItemCode(inputMap);
    			insertOwItemCode (resultMap, owkey);
    		}
    	}
//    	System.out.println("aaaa".substring(0, 1000));
	}

	/** 화주변경 저장 **/
	public void insertOwItemCode (Map parameterMap, String newOwkey) throws Exception {
		LOG.debug("insertItemCode.parameterMap ==> " + parameterMap.toString());
		String icutkey = (String)parameterMap.get("icutkey");
		String oldOwkey = (String)parameterMap.get("owkey");
		Map paramMap = new HashMap();
		paramMap.put("owkey", newOwkey);
		paramMap.put("icutkey", icutkey);
		
		int cnt = itemCodeDAO.selectTAdminMstIcutCount(paramMap);
		if(cnt == 0) {
			paramMap = new HashMap();
			paramMap.put("owkey", oldOwkey);
			paramMap.put("icutkey", icutkey);
			Map resultMap = itemCodeDAO.selectTAdminMstIcut(paramMap);
			resultMap.put("owkey", newOwkey);
			itemCodeDAO.insertTAdminMstIcut(resultMap);
		}

		paramMap = new HashMap();
		paramMap.put("owkey", newOwkey);
		paramMap.put("icutkey", icutkey);
		cnt = itemCodeDAO.selectTAdminMstIcutxtypeCount(paramMap);
		if(cnt == 0) {
			paramMap = new HashMap();
			paramMap.put("owkey", oldOwkey);
			paramMap.put("icutkey", icutkey);
			List list = itemCodeDAO.selectTAdminMstIcutxtype(paramMap);
			for(int i = 0; list.size() > i; i++) {
				Map resultMap = (Map)list.get(i);
				resultMap.put("owkey", newOwkey);
				itemCodeDAO.insertTAdminMstIcut(resultMap);
			}
		}
		// 변경되는 화주 정보 셋팅
		parameterMap.put("owkey", newOwkey);
		
		// ADMIN
		itemCodeDAO.insertTAdminMstIc(parameterMap);
		
		String ctkey2 = (String) parameterMap.get("ctkey");
		if(ctkey2 == null || "".equals(ctkey2) || ctkey2.isEmpty()){
			//지정된 상품단단위코드로 내려간 센터와 지정된 상품그룹코드로 내려간 센터가 같아야 매핑된다.
			List icgr_ctkeyList = itemCodeDAO.selecticgrCtkey(parameterMap);
			List icut_ctkeyList = itemCodeDAO.selecticutCtkey(parameterMap);
			Boolean equelCt = icgr_ctkeyList.equals(icut_ctkeyList);
			while(equelCt){
				// AMS, TMS, WMS --센터 맵핑
				String loggrpcd = (String) parameterMap.get("udf2");
				if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
					List chkList = itemCodeDAO.selectTAdminMstCt(parameterMap);
					if (chkList != null && !chkList.isEmpty() ) {
					
						for (int i=0 ;chkList.size() > i ; i++ ) { 
							Map chkRow = (Map)chkList.get(i);
							String ctkey = (String)chkRow.get("ctkey");
							parameterMap.put("ctkey", ctkey);
							parameterMap.put("dckey", "1");
							parameterMap.put("loggrpcd", loggrpcd);
							// ADMIN
							itemCodeDAO.insertTAdminMstIcxct(parameterMap);
							
							// ADMINS
							itemCodeDAO.insertTAdminMstCtxacxic(parameterMap);
							
							// TMS
							itemCodeTmsDAO.insertTTMstIcxct(parameterMap);
							
							// WMS 
							if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
								String icgrkey = (String) parameterMap.get("icgrkey");
								itemCodeWmsDAO.insertTWorkMstIc(parameterMap);
							}
							equelCt= false;
						}
					}		
				}
			}
		}	
	}
	
	/** **/
	@Override
	public void insertItemCode (Map parameterMap) throws Exception {
		LOG.debug("insertItemCode.parameterMap ==> " + parameterMap.toString());
		// 사용자 정보 초기화
		parameterMap.put("inserturkey", "");
		parameterMap.put("updateurkey", "");
		
		String ickey = (String)parameterMap.get("ickey");
		String owkey = (String)parameterMap.get("owkey");
		
		if("".equals(ickey) && ickey.isEmpty()){
			ickey = mstIcGenService.getNextStringId();	
			parameterMap.put("ickey", owkey+ickey);
		}

		// ADMIN
		itemCodeDAO.insertTAdminMstIc(parameterMap);
		
		// ICOM
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			itemCodeIcomDAO.insertTOrderMstIc(parameterMap);
		}*/
		
		// VIMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			itemCodeVimsDAO.insertTViewMstIc(parameterMap);
		}*/
		
		// TMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			itemCodeTmsDAO.insertTTMstIc(parameterMap);
		}
		
		// WMS INSERT
		/*if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = itemCodeDAO.selectTAdminMstIcxct(parameterMap);
        	if (list != null && !list.isEmpty()) {
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			this.itemCodeWmsDAO.insertTWorkMstIc(parameterMap);
        		}
        	}
		}*/
		
		String ctkey2 = (String) parameterMap.get("ctkey");
		if(ctkey2 == null || "".equals(ctkey2) || ctkey2.isEmpty()){
			//지정된 상품단단위코드로 내려간 센터와 지정된 상품그룹코드로 내려간 센터가 같아야 매핑된다.
			List icgr_ctkeyList = itemCodeDAO.selecticgrCtkey(parameterMap);
			List icut_ctkeyList = itemCodeDAO.selecticutCtkey(parameterMap);
			Boolean equelCt = icgr_ctkeyList.equals(icut_ctkeyList);
			while(equelCt){
				// AMS, TMS, WMS --센터 맵핑
					String loggrpcd = (String) parameterMap.get("udf2");
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
						List chkList = itemCodeDAO.selectTAdminMstCt(parameterMap);
						if (chkList != null && !chkList.isEmpty() ) {
						
							for (int i=0 ;chkList.size() > i ; i++ ) { 
								Map chkRow = (Map)chkList.get(i);
								String ctkey = (String)chkRow.get("ctkey");
								parameterMap.put("ctkey", ctkey);
								parameterMap.put("dckey", "1");
								parameterMap.put("loggrpcd", loggrpcd);
								// ADMIN
								itemCodeDAO.insertTAdminMstIcxct(parameterMap);
								
								// ADMINS
								itemCodeDAO.insertTAdminMstCtxacxic(parameterMap);
								
								// TMS
								itemCodeTmsDAO.insertTTMstIcxct(parameterMap);
								
								// WMS 
								if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
									String icgrkey = (String) parameterMap.get("icgrkey");
									itemCodeWmsDAO.insertTWorkMstIc(parameterMap);
								}
								equelCt= false;
							}
						}		
					}
			}
		}	
		
	}
	
	
	/** **/
	@Override
	public void updateItemCode (Map parameterMap) throws Exception {
		
		int count = 0;
		
		// ICOM
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = itemCodeIcomDAO.updateTOrderMstIc(parameterMap);
			if(count==0){
				itemCodeIcomDAO.insertTOrderMstIc(parameterMap);
			}
		}*/
		
		// VIMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			count = itemCodeVimsDAO.updateTViewMstIc(parameterMap);
			if(count==0){
				itemCodeVimsDAO.insertTViewMstIc(parameterMap);
			}
		}*/
		/*List icgr_ctkeyList = itemCodeDAO.selecticgrCtkey(parameterMap);
		List chkList = itemCodeDAO.selectTAdminMstIcxct(parameterMap);
		Boolean equelCt = icgr_ctkeyList.contains(chkList);
		System.out.println("++++++++++++++++++++equelCt : "+equelCt);
		if(!equelCt){
			throw new MsgException(11109);
		}*/
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			itemCodeWmsDAO.updateTWorkMstIc(parameterMap);
		}
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			count = itemCodeTmsDAO.updateTTMstIc(parameterMap);
			itemCodeTmsDAO.updateTTMstIcxct(parameterMap);
			if(count==0){
				itemCodeTmsDAO.insertTTMstIc(parameterMap);
			}
		}
		
		// ADM
		itemCodeDAO.updateTAdminMstIc(parameterMap); 
		itemCodeDAO.updateTAdminMstIcxct(parameterMap);
		itemCodeDAO.updateTAdminMstCtxacxic(parameterMap);  // ADMIN(TADMIN_MST_CTXACXIC)
		
	}

	/** **/
	@Override
	public Map deleteItemCode (CommandMap parameterMap) throws Exception {
		Map resultMap = new HashMap();
//		boolean flag = true;
		
		List list = parameterMap.getList("DELETE_LIST");
		
//		try {
			for (int i=0 ;list.size() > i ; i++  ) {
				Map dataMap = (Map)list.get(i);
			
				// WMS 
				if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
					itemCodeWmsDAO.deleteTWorkMstIc(dataMap);
				}
		
				// ICOM
				/* 10.21 OMS 삭제작업
				if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
					itemCodeIcomDAO.deleteTOrderMstIc(parameterMap);
				}*/
				
				// VIMS
				/*
				if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
					itemCodeVimsDAO.deleteTViewMstIc(parameterMap);
				}*/
				
				// TMS
				if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
					itemCodeTmsDAO.deleteTTMstIcxct(dataMap);
					itemCodeTmsDAO.deleteTTMstIc(dataMap);
				}
				
				//ICSB DELETE
				substitutionService.deleteSubstitution(dataMap);
				
				// ADMIN
				itemCodeDAO.deleteTAdminMstCtxacxic(dataMap);  // ADMIN(TADMIN_MST_CTXACXIC)
				itemCodeDAO.deleteTAdminMstIcxct(dataMap);
				itemCodeDAO.deleteTAdminMstIc(dataMap);
			}
			resultMap.put("flag", "true");
//		} catch (Exception e){
//			resultMap.put("flag", "false");
//			return resultMap;
//		}
		return resultMap;
	}
	
	/** **/
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return itemCodeDAO.selectTAdminMstCt(parameterMap);
	}
	
	/** **/
	@Override
	public List selectTAdminMstIcxct(Map parameterMap) throws Exception {
		return itemCodeDAO.selectTAdminMstIcxct(parameterMap);
	}

	/** **/
	@Override
	public void saveItemCodeCenter(Map parameterMap ,  List parameterList) throws Exception {
		int count = 0;
		// ### DELETE LOGIC ### //
		String ackey = (String) parameterMap.get("ackey");
		
		// ## SELECT ## 
		List chkList = itemCodeDAO.selectTAdminMstIcxct(parameterMap);
		
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
				String chkCtkey = (String)chkRow.get("ctkey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);
						String ctkey = (String)row.get("ctkey");
						if (chkCtkey.equals(ctkey)) {
							chkDelete = false;
							break;
						}
					}
				}

				// ## DELETE ##
				if (chkDelete) {
					//if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
					//	substitutionWmsDAO.deleteTWorkMstIcsb(chkRow);
					///}
					//substitutionDAO.deleteTAdminMstIcsbxct(chkRow);
					chkRow.put("delyn", "Y");
					// WMS 
					//if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
					itemCodeWmsDAO.deleteTWorkMstIc(chkRow);
					//}
					// TMS
					itemCodeTmsDAO.deleteTTMstIcxct(chkRow);
					
					// ADMIN(TADMIN_MST_CTXACXIC)
					itemCodeDAO.deleteTAdminMstCtxacxic(chkRow);
					
					// ADMIN
					itemCodeDAO.deleteTAdminMstIcxct(chkRow);
					
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		boolean chkInsert2 = false;
		int listct = 0;
		boolean equl = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			Map argMap = new HashMap();
			
			
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				listct=0;
				chkInsert = true;
				Map row = (Map)parameterList.get(i);
				String ctkey = (String)row.get("ctkey"); //list에서 가지고온 ctkey 
				
				//check된 ctkey가 icgrkey에 내려간 ctkey에 존재하는지 여부판단
				String icgrkey = (String)row.get("icgrkey");
				parameterMap.put("icgrkey", icgrkey);
				List icgr_ctkeyList = itemCodeDAO.selecticgrCtkey(parameterMap);
				
				for(int k=0;k<icgr_ctkeyList.size();k++){
					Map map = (HashMap)icgr_ctkeyList.get(k);
					chkInsert2=map.containsValue(ctkey);
					
					if(chkInsert2){
						listct ++;
					}
				}
				
				if(listct < 1){
					throw new MsgException(11109); 
				}
				//
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);
						String chkCtkey = (String)chkRow.get("ctkey");
						if (chkCtkey.equals(ctkey)) {
							chkInsert = false;
							break;
						}
					}					
				}
				// ## INSERT ##
				if (chkInsert) {
//					Map argMap = new HashMap();
					argMap.clear();
					
					//21050627 김종민
					if( ("null").equals(ackey) || ("").equals(ackey) ){
						throw new MsgException(20149, argMap);
						//거래처코드는 필수입력입니다.
					}
					
//					int icutCount = itemCodeDAO.selectCountTAdminMstIcutxct(row);
//					
//					if(icutCount == 0) {
//						argMap.put("ICUTKEY", MapUtil.getStr(row, "icutkey"));
//						argMap.put("UOM", MapUtil.getStr(row, "uom"));
//						argMap.put("CTKEY", MapUtil.getStr(row, "ctkey"));
//						throw new MsgException(10025, argMap);
//					}
					
					row.put("ackey", ackey);
					// ADMIN
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						int amCnt = itemCodeDAO.selectPkCntTadminMstIcxct(row);
						if (amCnt > 0) {
							row.put("delyn", "N");
							itemCodeDAO.deleteTAdminMstIcxct(row);
						} else {
							itemCodeDAO.insertTAdminMstIcxct(row);
						}
					}
					
					// ADMIN
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						int amCnt = itemCodeDAO.selectPkCntTadminMstCtxacxic(row);
						if (amCnt > 0) {
							row.put("delyn", "N");
							itemCodeDAO.deleteTAdminMstCtxacxic(row);
						} else {
							itemCodeDAO.insertTAdminMstCtxacxic(row);
						}
					}
					// TMS
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						int amCnt = itemCodeTmsDAO.selectPkCntTTMstIc(row);
						if (amCnt > 0) {
							row.put("delyn", "N");
							itemCodeTmsDAO.deleteTTMstIcxct(row);
						} else {
							itemCodeTmsDAO.insertTTMstIcxct(row);
						}
					}
					
					//itemCodeDAO.insertTAdminMstIcxct(row);
					
					// ADMIN
					//itemCodeDAO.insertTAdminMstCtxacxic(row);
					
					// TMS
					//itemCodeTmsDAO.insertTTMstIcxct(row);
					
					// WMS 
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						row.put("delyn", "N");
						count = itemCodeWmsDAO.updateTWorkMstIc(row);
						if(count==0){
							itemCodeWmsDAO.insertTWorkMstIc(row);
						}
					}
					
				} 
			}			
		}
	}
	
	@Override
	public void saveItemCodeCenter(Map parameterMap) throws Exception {
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			int cntWorkMstIc = itemCodeWmsDAO.selectPkCntTWorkMstIc(parameterMap);
			if ( cntWorkMstIc == 0) {
				int icgrChkCnt = itemCodeWmsDAO.selectPkCntTWorkMstIcgr(parameterMap);
				
				if(icgrChkCnt < 1) {
					Map argMap = new HashMap();
					argMap.put("CTKEY", MapUtil.getStr(parameterMap, "ctkey"));
					argMap.put("ICGRKEY", MapUtil.getStr(parameterMap, "icgrkey"));
					throw new MsgException(10014, argMap);
				} else {
					itemCodeWmsDAO.insertTWorkMstIc(parameterMap);
				}
			}
		}
		
		if (itemCodeDAO.selectPkCntTadminMstIcxct(parameterMap) == 0) {
			itemCodeDAO.insertTAdminMstIcxct(parameterMap);
		}
	}

	@Override
	public void saveItemCodeCenterxAc(Map parameterMap) throws Exception {
		itemCodeDAO.insertTAdminMstCtxacxic(parameterMap);
	}	
	
	@Override
	public List selectExcelDownItemCode(Map parameterMap) throws Exception {
		return itemCodeDAO.selectExcelDownItemCode(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownItemCode(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemCodeDAO.selectExcelDownItemCode(parameterMap, excelFormat);
	}

	@Override
	public void updateSCM(Map row) throws Exception {
		itemCodeDAO.updateSCM(row);
		
	}

	@Override
	public boolean validCntrCd(Map paramMap, List icxctList) throws Exception {
		boolean rsltBln = true;
		boolean rsltBln2 = true;
		List icgrxctList = itemGroupDAO.selectTAdminMstIcgrxct(paramMap);
		List IcutxtypexctList = itemUnitDAO.selectTAdminMstIcutxtypexct(paramMap);
		
		LOG.debug("icxctList====>  " + icxctList.toString());
		LOG.debug("icgrxctList====>  " + icgrxctList.toString());
		LOG.debug("IcutxtypexctList====>  " + IcutxtypexctList.toString());
		
			//기준은 icxctList
			for(int i = 0; i < icxctList.size(); i++){
				Map icxctMap = (Map)icxctList.get(i);
				String ctkey = (String)icxctMap.get("ctkey");
				
				for(int j = 0; j < icgrxctList.size(); j++){
					Map icgrxctMap = (Map)icgrxctList.get(i);
					if (!ctkey.equals((String)icgrxctMap.get("ctkey"))) {
						if(j == icgrxctList.size()-1 ){
							rsltBln = false;
						}
					}
				}
				
				if(rsltBln) {
					for(int j = 0; j < IcutxtypexctList.size(); j++){
						Map IcutxtypexctMap = (Map)IcutxtypexctList.get(i);
						if (!ctkey.equals((String)IcutxtypexctMap.get("ctkey"))) {
							if(j == IcutxtypexctList.size()-1 ){
								rsltBln2 = false;
							}
						}
					}
				}
			}
			LOG.debug("rsltBln===> " + rsltBln + ",rsltBln2===> " + rsltBln2);
		if(rsltBln && rsltBln2) {
			return  true;
		}else {
			return  false;
		}
	}
}