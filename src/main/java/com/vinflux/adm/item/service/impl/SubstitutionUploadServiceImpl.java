package com.vinflux.adm.item.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.SubstitutionService;
import com.vinflux.adm.item.service.SubstitutionUploadService;
import com.vinflux.adm.persistence.SubstitutionUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("substitutionUploadService")
public class SubstitutionUploadServiceImpl implements SubstitutionUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="substitutionService")
	private SubstitutionService substitutionService;
	
	@Resource(name="substitutionUploadDAO")
	private SubstitutionUploadDAO substitutionUploadDAO;
	
	protected static final Log LOG = LogFactory.getLog(SubstitutionUploadServiceImpl.class);
	
	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"ctkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"owkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"ickey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"sbkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"icuttype" 			,"" },	
	};	

	@Override
	public Map selectSubstitutionUploadInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = substitutionUploadDAO.selectSubstitutionUploadCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = substitutionUploadDAO.selectSubstitutionUpload(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	
	@Override
	public Map selectSubstitutionTemp(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = substitutionUploadDAO.selectSubstitutionUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = substitutionUploadDAO.selectSubstitutionTemp(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectSubstitutionTempAll(Map parameterMap) throws Exception {
		return substitutionUploadDAO.selectSubstitutionTempAll(parameterMap);
	}
	    
    @Override
	public String uploadSubstitutionExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
		
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");//////수정
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);

			//ULHSKEY
	    	substitutionUploadDAO.insertTadminTempIcsbup(row);

		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		substitutionUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }

	@Override
	public List selectExcelDownSubstitutionUpload(Map parameterMap) throws Exception {
		return substitutionUploadDAO.selectExcelDownSubstitutionUpload(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownSubstitutionUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return substitutionUploadDAO.selectExcelDownSubstitutionUpload(parameterMap, excelFormat);
	}
	
	@Override
	public void saveSubstitution (Map parameterMap, Map ctMap) throws Exception {
//		Set ctSet = ctMap.keySet();
//    	Iterator ctIrt = ctSet.iterator();
    	
    	String ctKey = MapUtil.getStr(parameterMap, "ctkey");
    	
    	if(this.selectCountIcxOw(parameterMap) == 0) {
    		throw new MsgException(10019); //유효하지 않은 상품 코드 입니다
		}
    	
    	if(this.selectCountIcuttypexIcutxIc(parameterMap) == 0) {
    		throw new MsgException(10020); //유효하지 않은 싱품 단위 유형입니다
		}
    	
    	int count = this.substitutionService.selectPkCountSubstitution(parameterMap);
    	
		
    	//if (count == 0) {
		parameterMap.put("udf1", count+1);
		
		//상품대체코드 insert
		this.substitutionService.insertSubstitution(parameterMap);
    	//}
    	
    	/*
    	else {
    		
    		LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    		LOG.debug("saveSubstitution-상품대체코드 update=>"+count);
    		LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    		
    		Map updateMap = new HashMap();
    		updateMap.putAll(parameterMap);
    		updateMap.put("ctkey", "");
    		this.substitutionService.updateSubstitution(updateMap);
    	}
    	*/
    	
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
    	/*
		if("".equals(ctKey) || ctKey == null) {
			
			LOG.debug("@@@@@@@@@@@@@@@@@@@@ 111 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    		LOG.debug("saveSubstitution-상품대체코드 ctKey=>"+ctKey);
    		LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			
			while(ctIrt.hasNext()) {
				String eCtKey = (String)ctIrt.next();
				parameterMap.put("ctkey", eCtKey);

				this.substitutionService.saveSubstitutionCenter(parameterMap);
			}
		} else {
		*/	
		if ( !ctMap.containsKey(ctKey) ) {
			throw new MsgException(10013); //유효하지 않은 센터 코드 입니다.
		}
		
		this.substitutionService.saveSubstitutionCenter(parameterMap);
		//}
		
	}
	
	private int selectCountIcxOw(Map parameterMap) throws Exception {
		return this.substitutionUploadDAO.selectCountIcxOw(parameterMap);
	}
	
	private int selectCountIcuttypexIcutxIc(Map parameterMap) throws Exception {
		return this.substitutionUploadDAO.selectCountIcuttypexIcutxIc(parameterMap);
	}
	
	@Override
	public void updateTadminTempIcsbup(Map parameterMap) throws Exception {
		this.substitutionUploadDAO.updateTadminTempIcsbup(parameterMap);
	}
}