package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;


public interface ItemUnitService {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectItemUnitInfo (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectItemUnitCount(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemUnitType (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertItemUnit (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateItemUnit (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteItemUnit (CommandMap parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcutxtypexct (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @param parameterList
	 * @throws Exception
	 */
	public void saveItemUnitCenter (Map parameterMap,List parameterList) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @param 
	 * @throws Exception
	 */
	public void saveItemUnitCenter(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownItemUnit (Map parameterMap) throws Exception;

	public Workbook selectExcelDownItemUnit(Map parameterMap,
			List<Map> excelFormat) throws Exception;
	
}
