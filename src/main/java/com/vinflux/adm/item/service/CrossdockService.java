package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface CrossdockService {
	
	/** **/
	public Map selectCrossdock (Map parameterMap) throws Exception;
	
	
	public void checkCrossdock (Map parameterMap) throws Exception;
		
	/** **/
	public void insertCrossdock (Map parameterMap) throws Exception;
	
	
	public void updateCrossdock (Map parameterMap) throws Exception;	
	
	
	public void deleteCrossdock (List list) throws Exception;
	
	/** **/
	public Workbook selectExcelCrossdock(Map parameterMap, List<Map> excelFormat) throws Exception;
	
}
