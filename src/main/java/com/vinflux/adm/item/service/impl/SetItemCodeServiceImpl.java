package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.SetItemCodeService;
import com.vinflux.adm.persistence.SetItemCodeDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.wms.persistence.SetItemCodeWmsDAO;

@Service("setItemCodeService")
public class SetItemCodeServiceImpl implements SetItemCodeService {

	@Resource(name="setItemCodeDAO")
	private SetItemCodeDAO setItemCodeDAO;
	
	@Resource(name="seicDtKeyIdGenService")
	private IdGenService seicDtKeyIdGenService;

//	@Resource(name="setItemCodeIcomDAO")
//	private SetItemCodeIcomDAO setItemCodeIcomDAO;
	
	@Resource(name="setItemCodeWmsDAO")
	private SetItemCodeWmsDAO setItemCodeWmsDAO;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(SetItemCodeServiceImpl.class);

	@Override
	public Map selectSetItemCodeHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		Integer count = this.selectSetItemCodeHdCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

    	if (count > 0 ) {     		
    		List list = setItemCodeDAO.selectSetItemCodeHd(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}

    	return resultMap;
	}

	@Override
	public int selectSetItemCodeHdCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = setItemCodeDAO.selectSetItemCodeHdCount(parameterMap);
				
		return cnt;
	}

	@Override
	public Map selectSetItemCodeDt(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		Integer count = this.selectSetItemCodeDtCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

    	if (count > 0 ) {     		
    		List list = setItemCodeDAO.selectSetItemCodeDt(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		
    	return resultMap;
	}

	@Override
	public int selectSetItemCodeDtCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = setItemCodeDAO.selectSetItemCodeDtCount(parameterMap);
				
		return cnt;
	}
	

	@Override
	public int checkSetItemCodeDtInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = setItemCodeDAO.checkSetItemCodeDtInfo(parameterMap);
		
		return cnt;
	}

	@Override
	public void insertSetItemCodeHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = this.selectSetItemCodeHdCount(parameterMap);

		if (cnt > 0) {
			throw new MsgException(30018);
		} else {
			
			setItemCodeDAO.insertSetItemCodeHd(parameterMap);
			/* 10.21 OMS 삭제작업
			setItemCodeIcomDAO.insertSetItemCodeHd(parameterMap);
			*/
			
			// WMS INSERT
			List list = setItemCodeDAO.selectCtkeyHd(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			setItemCodeWmsDAO.insertSetItemCodeHd(ctKeyMap);
        		}
        	}
			
		}
		
	}

	@Override
	public void insertSetItemCodeDt(Map parameterMap) throws Exception {
		//마스터 헤더키
    	String seicHdkey = MapUtil.getStr(parameterMap, "seickey");
    	//마스터 디테일키
		String seicDtKey = seicDtKeyIdGenService.getNextStringIdByKey(seicHdkey);
		parameterMap.put("seiclinenum", seicDtKey);
		
		int cnt = setItemCodeDAO.checkSetItemCodeDtInfo(parameterMap);
		
		if (cnt > 0) {
			throw new MsgException(30018);
		} else {
			setItemCodeDAO.insertSetItemCodeDt(parameterMap);
			/* 10.21 OMS 삭제작업
			setItemCodeIcomDAO.insertSetItemCodeDt(parameterMap);
			*/
			
			// WMS INSERT
			List list = setItemCodeDAO.selectCtkeyDt(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			setItemCodeWmsDAO.insertSetItemCodeDt(ctKeyMap);
        		}
        	}

		}
	}

	@Override
	public void updateSetItemCodeHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = this.selectSetItemCodeHdCount(parameterMap);
		
		if (cnt <=0) {
			throw new MsgException(30002);
		} else {
			setItemCodeDAO.updateSetItemCodeHd(parameterMap);
			/* 10.21 OMS 삭제작업
			setItemCodeIcomDAO.updateSetItemCodeHd(parameterMap);
			*/
			setItemCodeWmsDAO.updateSetItemCodeHd(parameterMap);

		}
	}

	@Override
	public void updateSetItemCodeDt(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = setItemCodeDAO.checkSetItemCodeDtInfo(parameterMap);
		
		if (cnt <= 0) {
			throw new MsgException(30002);
		} else {
			setItemCodeDAO.updateSetItemCodeDt(parameterMap);
			/* 10.21 OMS 삭제작업
			setItemCodeIcomDAO.updateSetItemCodeDt(parameterMap);
			*/
			setItemCodeWmsDAO.updateSetItemCodeDt(parameterMap);
		}
	}

	@Override
	public void deleteSetItemCodeHd(CommandMap parameterMap) throws Exception {
		
        List list = parameterMap.getList("DELETE_LIST");
       	int size = list.size();
       	for (int i=0 ;size > i ; i++ ) {
       		Map map = (Map)list.get(i);
       		
       		map.remove("ickey");
			int cnt = setItemCodeDAO.checkSetItemCodeDtInfo(map);
			if (cnt > 0) {
				setItemCodeWmsDAO.deleteSetItemCodeDt(map);
				/* 10.21 OMS 삭제작업
				setItemCodeIcomDAO.deleteSetItemCodeDt(map);
				*/
				setItemCodeDAO.deleteSetItemCodeDt(map);
			}
			
			int cnt2 = this.selectSetItemCodeHdCount(map);
			
			if (cnt2 <= 0) {
				throw new MsgException(30002);
			} else {
				setItemCodeDAO.deleteSetItemCodeHd(map);
				/* 10.21 OMS 삭제작업
				setItemCodeIcomDAO.deleteSetItemCodeHd(map);
				*/
				setItemCodeWmsDAO.deleteSetItemCodeHd(map);
			}
       	}
	}

	@Override
	public void deleteSetItemCodeDt(CommandMap parameterMap) throws Exception {
		
        List list = parameterMap.getList("DELETE_LIST");
       	int size = list.size();
       	for (int i=0 ;size > i ; i++ ) {
       		Map map = (Map)list.get(i);
       		
   			int cnt = setItemCodeDAO.checkSetItemCodeDtInfo(map);
			if (cnt <= 0) {
				throw new MsgException(30002);
			} else {
				setItemCodeWmsDAO.deleteSetItemCodeDt(map);
				/* 10.21 OMS 삭제작업
				setItemCodeIcomDAO.deleteSetItemCodeDt(parameterMap);
				*/
				setItemCodeDAO.deleteSetItemCodeDt(map);
			}
		}
	}

	@Override
	public List selectSetItemCodeExcelHD(Map parameterMap) throws Exception {
		return setItemCodeDAO.selectSetItemCodeExcelHD(parameterMap);
	}
	
	@Override
	public Workbook selectSetItemCodeExcelHD(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return setItemCodeDAO.selectSetItemCodeExcelHD(parameterMap,excelFormat);
	}

	@Override
	public List selectSetItemCodeExcelDT(Map parameterMap) throws Exception {
		return setItemCodeDAO.selectSetItemCodeExcelDT(parameterMap);
	}
	
	@Override
	public Workbook selectSetItemCodeExcelDT(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return setItemCodeDAO.selectSetItemCodeExcelDT(parameterMap, excelFormat);
	}

	@Override
	public Map selectSetItemCodeSpec(Map paramMap) throws Exception {
		return setItemCodeDAO.selectSetItemCodeSpec(paramMap);
	}
}