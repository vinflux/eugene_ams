package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface DicService {
	
	/** **/
	public Map selectDicInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectDic (Map parameterMap) throws Exception;
	
	/** **/
	public int checkDicCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertDic (Map parameterMap) throws Exception;
	
	/** **/
	public void updateDic (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteDic (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownDic (Map parameterMap) throws Exception;

	public Workbook selectExcelDownDic(Map parameterMap, List<Map> excelFormat) throws Exception;
	
}
