package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface SubstitutionService {
	//헤더 그리드 카운트
	public int selectPkCountSubstitution(Map parameterMap) throws Exception;
	
	//헤더 그리드 조회
	public List selectListSubstitution (Map parameterMap) throws Exception;
	
	public Map selectPkSubstitution(Map parameterMap) throws Exception;
	
	public void insertSubstitution (Map parameterMap) throws Exception;
	
	public void updateSubstitution (Map parameterMap) throws Exception;
	
	//헤더 그리드 삭제
	public void deleteSubstitution (Map parameterMap) throws Exception;
	
	//센터 조회
	public List selectTAdminMstCt(Map parameterMap) throws Exception;
	
	//물품별 센터 조회
	public List selectTAdminMstIcsbxct (Map parameterMap) throws Exception;
	
	//하단 저장
	public void saveSubstitutionCenter (Map parameterMap,List parameterList) throws Exception;
	
	public void saveSubstitutionCenter (Map parameterMap) throws Exception;
	
	//헤더 그리드 엑셀
	public List selectListSubstitutionExcel (Map parameterMap) throws Exception;

	public Workbook selectListSubstitutionExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;
}
