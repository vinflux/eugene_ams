package com.vinflux.adm.item.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.ItemCodeUnitUploadService;
import com.vinflux.adm.item.service.ItemUnitService;
import com.vinflux.adm.persistence.ItemCodeUnitUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("itemCodeUnitUploadService")
public class ItemCodeUnitUploadServiceImpl implements ItemCodeUnitUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="itemUnitService")
	private ItemUnitService itemUnitService;
	
	@Resource(name="itemCodeUnitUploadDAO")
	private ItemCodeUnitUploadDAO itemCodeUnitUploadDAO;
	
	protected static final Log LOG = LogFactory.getLog(ItemCodeUnitUploadServiceImpl.class);

	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"owkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"icutkey" 			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"uom" 				,"" },
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"10"		,"Y" ,"icutorder"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"Y" ,"icutqty" 			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"ickey" 				,"" }
	};	

	@Override
	public Map selectManageList(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = itemCodeUnitUploadDAO.selectCountTadminTempIcutup(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemCodeUnitUploadDAO.selectManageList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	
	@Override
	public Map selectUploadList(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = itemCodeUnitUploadDAO.selectCountTadminTempIcutup(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemCodeUnitUploadDAO.selectUploadList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectListTadminTempIcutup(Map parameterMap) throws Exception {
		return itemCodeUnitUploadDAO.selectListTadminTempIcutup(parameterMap);
	}
	    
    @Override
	public String uploadItemCodeUnit (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
    	
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
		
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);
	    	row.put("ctkey", "100");
			//ULHSKEY
	    	itemCodeUnitUploadDAO.insertTadminTempIcutup(row);
		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		itemCodeUnitUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }
    
    @Override
	public void updateTadminTempIcutup(Map parameterMap) throws Exception {
    	itemCodeUnitUploadDAO.updateTadminTempIcutup(parameterMap);
	}
    
    @Override
	public void saveItemCodeUnit(Map parameterMap, Map ctMap) throws Exception {
    	LOG.debug("saveItemCodeUnit Start!!!!!!!!!!!!!!");
    	
    	Set ctSet = ctMap.keySet();
    	Iterator ctIrt = ctSet.iterator();
    	
    	int icutChkFlag = 0;
    	String ctKey = MapUtil.getStr(parameterMap, "ctkey");
    	
		MapUtil.copyParam(parameterMap, "icuttype", parameterMap, "uom");
		
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
		if(ctKey == null || "".equals(ctKey)) {
			icutChkFlag = itemCodeUnitUploadDAO.selectItemIcutCount(parameterMap);
			
			if (icutChkFlag < 1) {
				itemUnitService.insertItemUnit(parameterMap);
				while(ctIrt.hasNext()) {
					String eCtKey = (String)ctIrt.next();
					parameterMap.put("ctkey", eCtKey);

					itemUnitService.saveItemUnitCenter(parameterMap);
				}
			} else {
				itemUnitService.updateItemUnit(parameterMap);
				while(ctIrt.hasNext()) {
					String eCtKey = (String)ctIrt.next();
					parameterMap.put("ctkey", eCtKey);

					itemUnitService.saveItemUnitCenter(parameterMap);
				}
			}
		} else {
			if ( !ctMap.containsKey(ctKey) ) {
				throw new MsgException(10013); //존재하지 않는 센터코드 입니다.
			}

			icutChkFlag = itemCodeUnitUploadDAO.selectItemIcutCount(parameterMap);

			if (icutChkFlag < 1) {
				itemUnitService.insertItemUnit(parameterMap);
				itemUnitService.saveItemUnitCenter(parameterMap);
			} else {
				itemUnitService.updateItemUnit(parameterMap);
				itemUnitService.saveItemUnitCenter(parameterMap);
			}
		}
    }

	@Override
	public List selectManageListExcel(Map parameterMap) throws Exception {
		return itemCodeUnitUploadDAO.selectManageListExcel(parameterMap);
	}
	
	@Override
	public Workbook selectManageListExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemCodeUnitUploadDAO.selectManageListExcel(parameterMap, excelFormat);
	}


	@Override
	public int selectItemUnitCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
}