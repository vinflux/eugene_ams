package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;


public interface ItemCodeService {
	
	/** **/
	public Map selectItemCodeInfo (Map parameterMap) throws Exception;

	/** **/
	public Map selectItemCode (Map parameterMap) throws Exception;
	
	/** **/
	public List selectItemCodeAvqtyCheck (Map parameterMap) throws Exception;
	/** 화주변경 저장 */
	public void insertOwItemCode (CommandMap requestMap) throws Exception;
	/** **/
	public void insertItemCode (Map parameterMap) throws Exception;
	
	/** **/
	public void updateItemCode (Map parameterMap) throws Exception;
	
	/** **/
	public Map deleteItemCode (CommandMap parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMstCt(Map parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMstIcxct (Map parameterMap) throws Exception;
	
	/** **/
	public void saveItemCodeCenter (Map parameterMap,List parameterList) throws Exception;
	

	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void saveItemCodeCenter(Map parameterMap) throws Exception;
	
	/** **/
	public void saveItemCodeCenterxAc (Map parameterMap) throws Exception;		
	
	/** **/
	public List selectExcelDownItemCode (Map parameterMap) throws Exception;

	public Workbook selectExcelDownItemCode(Map parameterMap,
			List<Map> excelFormat)throws Exception;

	public void updateSCM(Map row) throws Exception;

	public boolean validCntrCd(Map paramMap, List paramList) throws Exception;
	
}
