package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.DicService;
import com.vinflux.adm.persistence.DicDAO;
//import com.vinflux.icom.persistence.DicIcomDAO;

@Service("dicService")
public class DicServiceImpl implements DicService {
	
	@Resource(name="dicDAO")
	private DicDAO dicDAO;
	
	//@Resource(name="dicIcomDAO")
	//private DicIcomDAO dicIcomDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectDicInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = dicDAO.selectDicCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = dicDAO.selectDic(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectDic (Map parameterMap) throws Exception {
		return dicDAO.selectPkDic(parameterMap);
	}
	
	@Override
	public int checkDicCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return dicDAO.checkDicCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.DicService#insertDic(java.util.Map)
	 */
	@Override
	public void insertDic (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.dicDAO.insertTAdminMstDic(parameterMap);
		
		/* 10.21 OMS 삭제작업
		// ICOM INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.dicIcomDAO.insertTOrderMstDic(parameterMap);	
		}*/
		
	}
	
	/** **/
	@Override
	public void updateDic (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.dicDAO.updateTAdminMstDic(parameterMap);
		
//		int count = 0;
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//count = this.dicIcomDAO.updateTOrderMstDic(parameterMap);
			if(count==0){
				//this.dicIcomDAO.updateTOrderMstDic(parameterMap);
			}
		}*/
		
	}

	/** **/
	@Override
	public void deleteDic (Map parameterMap) throws Exception {
		// ICOM DELETE
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.dicIcomDAO.deleteTOrderMstDic(parameterMap);
		}*/
		
		// ADMIN DELETE
		this.dicDAO.deleteTAdminMstDic(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownDic (Map parameterMap) throws Exception {
    	return dicDAO.selectExcelDownDic(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDic(Map parameterMap, List<Map> excelFormat) throws Exception {
		return dicDAO.selectExcelDownDic(parameterMap, excelFormat);
	}
}