package com.vinflux.adm.item.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface ItemCodeUnitUploadService {
	/**
     * 아이템 단위 코드 업로드 관리 탭 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectManageList (Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 업로드 업로드 탭 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectUploadList(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 임시 테이블 조회 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectListTadminTempIcutup(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadItemCodeUnit (Map parameterMap, InputStream excelFis , String fileName) throws Exception;
	
	/**
     * 아이템 단위 코드 임시 테이블 수정
     * @param Map parameterMap
     * @return
     * @throws Exception
     */
	public void updateTadminTempIcutup(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 저장
     * @param Map parameterMap, Iterator pItr, Map ctMap
     * @return
     * @throws Exception
     */
	public void saveItemCodeUnit(Map parameterMap, Map ctMap) throws Exception;
	
	/**
     * 아이템 단위 코드  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectManageListExcel (Map parameterMap) throws Exception;

	public Workbook selectManageListExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectItemUnitCount(Map parameterMap) throws Exception;
	
}
