package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.MixShipItemImpossService;
import com.vinflux.adm.persistence.MixShipItemImpossDAO;
import com.vinflux.framework.exception.MsgException;

@Service("mixShipItemImpossService")
public class MixShipItemImpossServiceImpl implements MixShipItemImpossService {

	@Resource(name="mixShipItemImpossDAO")
	private MixShipItemImpossDAO mixShipItemImpossDAO;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MixShipItemImpossServiceImpl.class);

	@Override
	public Map selectMixShipItemImpossHd(Map parameterMap) throws Exception {
		
		Map resultMap = new HashMap();
		
		Integer count = this.selectMixShipItemImpossHdCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		
		if(count > 0) {
			List list = mixShipItemImpossDAO.selectMixShipItemImpossHd(parameterMap);
			if( list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}else {
			List list = new ArrayList();
			resultMap.put("list", list);
			
		}
		
		return resultMap;
	}

	@Override
	public int selectMixShipItemImpossHdCount(Map parameterMap) throws Exception {
		
		int cnt = mixShipItemImpossDAO.selectMixShipItemImpossHdCount(parameterMap);
		
		return cnt;
	}

	@Override
	public Map selectMixShipItemImpossDt(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();

		Integer count = this.selectMixShipItemImpossDtCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

    	if (count > 0 ) {     		
    		List list = mixShipItemImpossDAO.selectMixShipItemImpossDt(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		
    	return resultMap;
	}

	@Override
	public int selectMixShipItemImpossDtCount(Map parameterMap) throws Exception {
		
		int cnt = mixShipItemImpossDAO.selectMixShipItemImpossDtCount(parameterMap);
		return cnt;
	}
	
	
	/**
     * SET 혼적상품불가 헤더 추가,수정,삭제 후 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@Override
	public void saveMixShipItemImposs(List parameterList)throws Exception {
		Map map;
		for(int i = 0 ; i < parameterList.size() ; i++){
			Map mixShipItemImposeMap = (Map)parameterList.get(i);
			int cnt = mixShipItemImpossDAO.checkmixShipItemImposso(mixShipItemImposeMap);
			
			if ("C".equals(mixShipItemImposeMap.get("STATUS")) ) {
				if(cnt > 0){
					throw new MsgException(30018);
				}else {
					mixShipItemImpossDAO.insertMixShipItemImposInfo(mixShipItemImposeMap);
				}
			}else if ("U".equals(mixShipItemImposeMap.get("STATUS")) ) {
				mixShipItemImpossDAO.updateMixShipItemImposInfo(mixShipItemImposeMap);
			}else if ("D".equals(mixShipItemImposeMap.get("STATUS")) ) {
				mixShipItemImpossDAO.deleteMixShipItemImposInfo(mixShipItemImposeMap);
			}
		}
	
	}
	
	/**
     * SET 혼적상품불가 디테일 추가,수정,삭제 후 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@Override
	public void saveDetailMixShipItemImposs(List parameterList)throws Exception {
		for(int i = 0 ; i < parameterList.size() ; i++){
			Map mixShipItemImposeMap = (Map)parameterList.get(i);
			int cnt = mixShipItemImpossDAO.checkDetailmixShipItemImposso(mixShipItemImposeMap);
			if ("C".equals(mixShipItemImposeMap.get("STATUS")) ) {
				if(cnt > 0){
					throw new MsgException(30018);
				}else {
					mixShipItemImpossDAO.insertDetailMixShipItemImposInfo(mixShipItemImposeMap);
				}
			}else if ("U".equals(mixShipItemImposeMap.get("STATUS")) ) {
				mixShipItemImpossDAO.updateDetailMixShipItemImposInfo(mixShipItemImposeMap);
			}else if ("D".equals(mixShipItemImposeMap.get("STATUS")) ) {
				mixShipItemImpossDAO.deleteDetailMixShipItemImposInfo(mixShipItemImposeMap);
			}
		}
	
	}
	
	/**
     * SET 혼적상품불가 헤어 엑셀 다운
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List excelDownMixShipItemImposs(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return this.mixShipItemImpossDAO.excelDownMixShipItemImposs(parameterMap);
	}
	
	public Workbook excelDownMixShipItemImposs(Map parameterMap, List<Map> excelFormat) {
		return mixShipItemImpossDAO.excelDownMixShipItemImposs(parameterMap, excelFormat);
	}
	
	/**
     * SET 혼적상품불가 디테일 엑셀 다운
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List excelDownDetailMixShipItemImposs(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return this.mixShipItemImpossDAO.excelDownDetailMixShipItemImposs(parameterMap);
	}
	
	public Workbook excelDownDetailMixShipItemImposs(Map parameterMap, List<Map> excelFormat) {
		return mixShipItemImpossDAO.excelDownDetailMixShipItemImposs(parameterMap, excelFormat);
	}

	

}
