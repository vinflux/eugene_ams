package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface StoreevxicService {
	
	/** **/
	public Map selectStoreevxicInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownStoreevxic (Map parameterMap) throws Exception;

	public Workbook selectExcelDownStoreevxic(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	
	/** **/
	public Map selectStoreevxicInfo2 (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownStoreevxic2 (Map parameterMap) throws Exception;

	public Workbook selectExcelDownStoreevxic2(Map parameterMap,
			List<Map> excelFormat)throws Exception;	
	
}
