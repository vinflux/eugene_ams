package com.vinflux.adm.item.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface ItemCodeUploadService {
	/**
     * 아이템 코드 업로드 임시 테이블 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectItemCodeUploadInfo (Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 업로드 항목 조회 with paging
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectItemCodeTemp(Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 업로드 항목 조회 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectItemCodeTempAll(Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadItemCodeExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception;
	
	/**
     * 아이템 코드 임시 저장 테이블 수정
     * @param Map parameterMap
     * @return
     * @throws Exception
     */
	public void updateTadminTempIcup(Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 저장
     * @param Map parameterMap, Iterator pItr, Map ctMap
     * @return
     * @throws Exception
     */
	public void saveItemCode(Map parameterMap, Map ctMap) throws Exception;
	
	/**
     * 아이템 코드  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownItemCodeUpload (Map parameterMap) throws Exception;

	public Workbook selectExcelDownItemCodeUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
