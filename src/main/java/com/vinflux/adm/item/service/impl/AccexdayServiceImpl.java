package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.AccexdayService;
import com.vinflux.adm.persistence.AccexdayDAO;
import com.vinflux.framework.collections.CommandMap;

@Service("accexdayService")
public class AccexdayServiceImpl implements AccexdayService {
	
	@Resource(name="accexdayDAO")
	private AccexdayDAO accexdayDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectAccexday (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		String tabType = (String)parameterMap.get("tabType");
		
		if("CTG".equals(tabType)) {
			
	    	Integer count = accexdayDAO.selectAccexdayCount(parameterMap);
	    	resultMap.put("cnt", Integer.toString(count));
	    	if (count > 0 ) {     		
	        	List list = accexdayDAO.selectAccexday(parameterMap);
	        	if (list != null && !list.isEmpty()) { 
	        		resultMap.put("list", list);	
	        	}
	    	} else {
	    		List list = new ArrayList();
	    		resultMap.put("list", list);
	    	}
	    	
		} else if("IC".equals(tabType)) {
			
			Integer count = accexdayDAO.selectAccexdayIcCount(parameterMap);
	    	resultMap.put("cnt", Integer.toString(count));
	    	if (count > 0 ) {     		
	        	List list = accexdayDAO.selectAccexdayIc(parameterMap);
	        	if (list != null && !list.isEmpty()) { 
	        		resultMap.put("list", list);	
	        	}
	    	} else {
	    		List list = new ArrayList();
	    		resultMap.put("list", list);
	    	}
		}
		
		return resultMap;
	}

	/** **/
	@Override
	public void updateAccexday(CommandMap requestMap) throws Exception {
		Map param = requestMap.getParamMap();
		List list = requestMap.getList("list");
		
		if("CTG".equals((String)param.get("tabType"))) {
			for(int i=0; i<list.size(); i++){
				Map paramMap = (Map)list.get(i);
				
				if("C".equals(paramMap.get("STATUS")) || "U".equals(paramMap.get("STATUS"))) 
				{
					//채화예정재고 카테고리 존재 여부
					int cnt = this.accexdayDAO.selectAccexdayCtegoryCount(paramMap);
					if(cnt > 0) {
						this.accexdayDAO.updateAccexday(paramMap);
					} else {
						this.accexdayDAO.insertAccexday(paramMap);
					}
				} 
				else if("D".equals(paramMap.get("STATUS"))) 
				{
					this.accexdayDAO.deleteAccexday(paramMap);
				}
				
				this.accexdayDAO.updateAccexdayCtg(paramMap);
			}
		}
		else if("IC".equals((String)param.get("tabType"))) {
			for(int i=0; i<list.size(); i++){
				this.accexdayDAO.updateAccexdayIc((Map)list.get(i));
			}
		}
	}
	
	@Override
	public Workbook selectExcelAccexday(Map parameterMap, List<Map> excelFormat) throws Exception {
		if("CTG".equals((String)parameterMap.get("tabType"))) {
			return accexdayDAO.selectExcelAccexdayCtg(parameterMap, excelFormat);
		}
		else { return accexdayDAO.selectExcelAccexdayIc(parameterMap, excelFormat); }
	}
	
	/** **/
	@Override
	public Map selectAccexdayPop (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		
		String tabType = (String)parameterMap.get("tabType");
		
		if("CTG".equals(tabType)) {
	    	Integer count = accexdayDAO.selectAccexdayPopCount(parameterMap);
	    	resultMap.put("cnt", Integer.toString(count));
	    	if (count > 0 ) {     		
	        	List list = accexdayDAO.selectAccexdayPop(parameterMap);
	        	if (list != null && !list.isEmpty()) { 
	        		resultMap.put("list", list);	
	        	}
	    	} else {
	    		List list = new ArrayList();
	    		resultMap.put("list", list);
	    	}
		}
		else if("IC".equals(tabType)) {
			Integer count = accexdayDAO.selectAccexdayIcPopCount(parameterMap);
	    	resultMap.put("cnt", Integer.toString(count));
	    	if (count > 0 ) {     		
	        	List list = accexdayDAO.selectAccexdayIcPop(parameterMap);
	        	if (list != null && !list.isEmpty()) { 
	        		resultMap.put("list", list);	
	        	}
	    	} else {
	    		List list = new ArrayList();
	    		resultMap.put("list", list);
	    	}
		}
		return resultMap;
	}
}