package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;

public interface SetItemCodeService {

    /**
     * SET 물품 헤더 정보 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectSetItemCodeHd(Map parameterMap) throws Exception;

    /**
     * SET 물품 헤더 정보 count
     * @param Map parameterMap
     * @return int
     * @throws Exception
     */
	public int selectSetItemCodeHdCount(Map parameterMap) throws Exception;

    /**
     * SET 물품 상세 정보 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectSetItemCodeDt(Map parameterMap) throws Exception;

    /**
     * SET 물품 정보 중복 확인
     * @param Map parameterMap
     * @return int
     * @throws Exception
     */
	public int selectSetItemCodeDtCount(Map parameterMap) throws Exception;
	
	
	public int checkSetItemCodeDtInfo(Map parameterMap) throws Exception;

    /**
     * SET 물품 헤더 정보 생성
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void insertSetItemCodeHd(Map parameterMap) throws Exception;

    /**
     * SET 물품 상세 정보 생성
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void insertSetItemCodeDt(Map parameterMap) throws Exception;

    /**
     * SET 물품 헤더 정보 갱신
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void updateSetItemCodeHd(Map parameterMap) throws Exception;

    /**
     * SET 물품 상세 정보 갱신
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void updateSetItemCodeDt(Map parameterMap) throws Exception;

    /**
     * SET 물품 헤더 정보 삭제
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void deleteSetItemCodeHd(CommandMap parameterMap) throws Exception;

    /**
     * SET 물품 상세 정보 삭제
     * @param Map parameterMap
     * @return void
     * @throws Exception
     */
	public void deleteSetItemCodeDt(CommandMap parameterMap) throws Exception;

	/**
	 * 주문 물품 맵핑 엑셀 다운 
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectSetItemCodeExcelHD(Map parameterMap) throws Exception ;

	public Workbook selectSetItemCodeExcelHD(Map parameterMap,
			List<Map> excelFormat)throws Exception ;
	
	/**
	 * 주문 물품 맵핑 엑셀 다운 
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectSetItemCodeExcelDT(Map parameterMap) throws Exception ;

	public Workbook selectSetItemCodeExcelDT(Map parameterMap,
			List<Map> excelFormat)throws Exception ;

	public Map selectSetItemCodeSpec(Map paramMap)throws Exception;

}
