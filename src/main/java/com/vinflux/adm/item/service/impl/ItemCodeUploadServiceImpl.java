package com.vinflux.adm.item.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.ItemCodeService;
import com.vinflux.adm.item.service.ItemCodeUploadService;
import com.vinflux.adm.item.service.ItemGroupService;
import com.vinflux.adm.item.service.ItemUnitService;
import com.vinflux.adm.persistence.ItemCodeUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("itemCodeUploadService")
public class ItemCodeUploadServiceImpl implements ItemCodeUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="itemUnitService")
	private ItemUnitService itemUnitService;
	
	@Resource(name="itemGroupService")
	private ItemGroupService itemGroupService;	
	
	@Resource(name="itemCodeService")
	private ItemCodeService itemCodeService;
	
	@Resource(name="itemCodeUploadDAO")
	private ItemCodeUploadDAO itemCodeUploadDAO;
	

	@Resource(name="mstIcGenService")
	private IdGenService mstIcGenService;

	protected static final Log LOG = LogFactory.getLog(ItemCodeUploadServiceImpl.class);
	
//	@Resource(name="txAdmManager")
//	private DataSourceTransactionManager txAdmManager;
	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"ctkey" 				,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"owkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"icgrkey" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"600"		,"Y" ,"icname" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"ictype" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"ctgkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"icutkey" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"uom" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"ackey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"shelflifeindicator" 	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"shelflifecodetype" 	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"Y" ,"lo_non_stock_type" 	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"Y" ,"foodyn" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"Y" ,"operstatype" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"udf2" 				,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"25"		,"N" ,"boxprice" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"25"		,"N" ,"bottleprice" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"N" ,"boxyn" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"N" ,"btlyn" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"width" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"length" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"height" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"weight" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM			    ,"25"		,"N" ,"volume" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"widthpi" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"lengthpi" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"heightpi" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"spec" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"maxordqty" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM			    ,"25"		,"N" ,"minordqty" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"N" ,"taxyn" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"serialinputtype" 	,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"10"		,"N" ,"shelflife" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"releasedays" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"rfidbox" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"rfidbottle" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25"		,"N" ,"ordqty" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"brandcode" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"01"		,"N" ,"returnor" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"shelfday" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"40"		,"N" ,"stc_apply_grade_cd" 	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"accexday" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"chittype" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"boxickey" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"btlickey" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"ordstartdate" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"ordstopdate" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"regdate" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"closingdate" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf1" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf3" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf4" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"N" ,"udf5" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf6" 				,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"ickey" 				,"" }
		
	};	

	@Override
	public Map selectItemCodeUploadInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = itemCodeUploadDAO.selectItemCodeUploadCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemCodeUploadDAO.selectItemCodeUpload(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	
	@Override
	public Map selectItemCodeTemp(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = itemCodeUploadDAO.selectItemCodeUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemCodeUploadDAO.selectItemCodeTemp(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectItemCodeTempAll(Map parameterMap) throws Exception {
		return itemCodeUploadDAO.selectItemCodeTempAll(parameterMap);
	}
	    
    @Override
	public String uploadItemCodeExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
		
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);
	    	//row.put("ctkey", "100");
	    	
	    	String ickey = (String) row.get("ickey");
	    	String owkey = (String) row.get("owkey");
			
			if("".equals(ickey) && ickey.isEmpty()){
				ickey = mstIcGenService.getNextStringId();	
				row.put("ickey", owkey+ickey);
			}

			//ULHSKEY
	    	itemCodeUploadDAO.insertTadminTempIcup(row);
		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		itemCodeUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }
    
    @Override
	public void updateTadminTempIcup(Map parameterMap) throws Exception {
    	itemCodeUploadDAO.updateTadminTempIcup(parameterMap);
	}
    
    @Override
	public void saveItemCode(Map parameterMap, Map ctMap) throws Exception {
    	
    	Set ctSet = ctMap.keySet();
    	Iterator ctIrt = ctSet.iterator();
    	
    	int icutChkFlag = 0;
    	int icgrChkFlag = 0;
    	int acChkFlag = 0;
		int txFlag = 0;
    	String ctKey = MapUtil.getStr(parameterMap, "ctkey");
    	
		MapUtil.copyParam(parameterMap, "icuttype", parameterMap, "uom");
		
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
		if(ctKey == null || "".equals(ctKey)) {
			/* 2차 개발때 주석 해제 - 2017.11.06 : ksh			
			icutChkFlag = itemUnitService.selectItemUnitCount(parameterMap);
			if (icutChkFlag < 1) {
				itemUnitService.insertItemUnit(parameterMap);
			}

			icgrChkFlag = itemGroupService.selectItemGroupExcelUploadCount(parameterMap);

			if (icgrChkFlag < 1) {
				itemGroupService.insertItemGroup(parameterMap);
			}	*/		
			
			txFlag = itemCodeUploadDAO.selectPkCntTadminMstIc(parameterMap);
			if (txFlag == 0) {
				itemCodeService.insertItemCode(parameterMap);

			} else {
				itemCodeService.updateItemCode(parameterMap);
			}
			
			while(ctIrt.hasNext()) {
				String eCtKey = (String)ctIrt.next();
				parameterMap.put("ctkey", eCtKey);
				/* 2차 개발때 주석 해제 - 2017.11.06 : ksh
				itemUnitService.saveItemUnitCenter(parameterMap);
				itemGroupService.saveItemGroupCenter(parameterMap);*/
				itemCodeService.saveItemCodeCenter(parameterMap);
				
				String ackey = (String)parameterMap.get("ackey");
				
				if(ackey != null && !"".equals(ackey)){
					//AC MASTER - ACMASTER 없으면 저장, 있으면 패스 1차
					acChkFlag = itemCodeUploadDAO.selectPkCntTadminMstAc(parameterMap);
					if(acChkFlag > 0){
						itemCodeService.saveItemCodeCenterxAc(parameterMap);
					}
				}				
				
			}
		} else {
			if ( !ctMap.containsKey(ctKey) ) {
				throw new MsgException(10013); //존재하지 않는 센터코드 입니다.
			}
			
			// OW MASTER - OWMASTER 없으면 저장, 있으면 패스 2차
			
			/* 2차 개발때 주석 해제 - 2017.11.06 : ksh
			icutChkFlag = itemUnitService.selectItemUnitCount(parameterMap);

			if (icutChkFlag < 1) {
				itemUnitService.insertItemUnit(parameterMap);
			}
			
			itemUnitService.saveItemUnitCenter(parameterMap);*/

			// 상품그룹 체크 후 저장 - ICGR 없으면 저장, 있으면 패스 1차
			/* 2차 개발때 주석 해제 - 2017.11.06 : ksh	
			icgrChkFlag = itemGroupService.selectItemGroupExcelUploadCount(parameterMap);

			if (icgrChkFlag < 1) {
				itemGroupService.insertItemGroup(parameterMap);
			}
			
			itemGroupService.saveItemGroupCenter(parameterMap);	*/		
			
			txFlag = itemCodeUploadDAO.selectPkCntTadminMstIc(parameterMap);

			if (txFlag == 0) {
				itemCodeService.insertItemCode(parameterMap);
				
			} else {
				itemCodeService.updateItemCode(parameterMap);
			}
			// CT MASTER - CTMASTER 없으면 저장, 있으면 패스 2차
			itemCodeService.saveItemCodeCenter(parameterMap);
			
			String ackey = (String)parameterMap.get("ackey");
			
			if(ackey != null && !"".equals(ackey)){
				//AC MASTER - ACMASTER 없으면 저장, 있으면 패스 1차
				acChkFlag = itemCodeUploadDAO.selectPkCntTadminMstAc(parameterMap);
				if(acChkFlag > 0){
					itemCodeService.saveItemCodeCenterxAc(parameterMap);
				}
			}
		}
    }

	@Override
	public List selectExcelDownItemCodeUpload(Map parameterMap) throws Exception {
		return itemCodeUploadDAO.selectExcelDownItemCodeUpload(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownItemCodeUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemCodeUploadDAO.selectExcelDownItemCodeUpload(parameterMap, excelFormat);
	}
}