package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;

public interface MixShipItemImpossService {

    /**
     * SET 혼적상품불가 헤더 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMixShipItemImpossHd(Map parameterMap) throws Exception;

	 /**
     * SET 혼적상품불가  헤더 count
     * @param Map parameterMap
     * @return int
     * @throws Exception
     */
	public int selectMixShipItemImpossHdCount(Map parameterMap) throws Exception;
	
	/**
     * SET 혼적상품불가  상세 정보 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMixShipItemImpossDt(Map parameterMap) throws Exception;

	/**
     * SET 혼적상품불가  상세 정보  count
     * @param Map parameterMap
     * @return int
     * @throws Exception
     */
	public int selectMixShipItemImpossDtCount(Map parameterMap) throws Exception;
	
	 /**
     * 혼적상품불가 헤더 정보 생성,갱신,삭네
     * @param List parameterMap
     * @return void
     * @throws Exception
     */	
	public void saveMixShipItemImposs(List parameterMap)throws Exception;
	
   
	
	/**
     * 혼적상품불가 상세 정보 생성,갱신,삭제
     * @param List parameterMap
     * @return void
     * @throws Exception
     */	
	public void saveDetailMixShipItemImposs(List parameterMap) throws Exception;


	/**
	 * 주문 기준 코드 엑셀 다운 
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List excelDownMixShipItemImposs(Map parameterMap) throws Exception ;

	public Workbook excelDownMixShipItemImposs(Map paramMap, List<Map> excelFormat);
	
	
	/**
	 * 주문 기준 코드 엑셀 다운 
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List excelDownDetailMixShipItemImposs(Map parameterMap) throws Exception ;
	
	public Workbook excelDownDetailMixShipItemImposs(Map paramMap, List<Map> excelFormat);


}
