package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface StoreevService {
	
	/** **/
	public Map selectStoreevInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownStoreev (Map parameterMap) throws Exception;

	public Workbook selectExcelDownStoreev(Map parameterMap,
			List<Map> excelFormat) throws Exception;
	
}
