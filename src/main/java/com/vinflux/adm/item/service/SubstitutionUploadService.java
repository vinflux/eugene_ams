package com.vinflux.adm.item.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface SubstitutionUploadService {
	/**
     * 아이템 코드 업로드 임시 테이블 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectSubstitutionUploadInfo (Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 업로드 항목 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectSubstitutionTemp(Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 업로드 항목 조회 without paging
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectSubstitutionTempAll(Map parameterMap) throws Exception;
	
	/**
     * 아이템 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadSubstitutionExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception;

	/**
     * 아이템 코드  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownSubstitutionUpload (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownSubstitutionUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception;
	
	/**
     * 물품 대체코드 저장
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public void saveSubstitution (Map parameterMap, Map ctMap) throws Exception;
	
	public void updateTadminTempIcsbup(Map parameterMap) throws Exception;

}
