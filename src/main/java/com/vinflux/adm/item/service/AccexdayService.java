package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;

public interface AccexdayService {
	
	/** **/
	public Map selectAccexday (Map parameterMap) throws Exception;
	
	/** **/
	public void updateAccexday (CommandMap requestMap) throws Exception;
	
	/** **/
	public Workbook selectExcelAccexday(Map parameterMap, List<Map> excelFormat) throws Exception;
	
	/** **/
	public Map selectAccexdayPop (Map parameterMap) throws Exception;
	
	
}
