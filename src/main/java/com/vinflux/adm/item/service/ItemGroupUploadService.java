package com.vinflux.adm.item.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface ItemGroupUploadService {
	
	/**
     * 아이템 단위 코드 업로드 관리 탭 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public int selectCountTAdminTempIcgrUp(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 업로드 관리 탭 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectListItemGroupUploadTabManage(Map parameterMap) throws Exception ;
	
	/**
     * 아이템 단위 코드 업로드 관리 탭 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectListItemGroupUploadTabUpload(Map parameterMap) throws Exception ;
	
	/**
     * 아이템 단위 코드 임시 테이블 조회 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectListTAdminTempIcgrUp(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadItemGroupList (Map parameterMap, InputStream excelFis , String fileName) throws Exception;
	
	/**
     * 아이템 단위 코드 임시 테이블 수정
     * @param Map parameterMap
     * @return
     * @throws Exception
     */
	public void updateTAdminTempIcgrUp(Map parameterMap) throws Exception;
	
	/**
     * 아이템 단위 코드 저장
     * @param Map parameterMap, Iterator pItr, Map ctMap
     * @return
     * @throws Exception
     */
	public void saveItemGroup(Map parameterMap, Map ctMap) throws Exception;
	
	/**
     * 아이템 단위 코드  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelItemGroupUploadTabManage (Map parameterMap) throws Exception;

	public Workbook selectExcelItemGroupUploadTabManage(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
