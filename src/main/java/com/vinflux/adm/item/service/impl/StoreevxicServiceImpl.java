package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.StoreevxicService;
import com.vinflux.adm.persistence.StoreevxicDAO;


@Service("storeevxicService")
public class StoreevxicServiceImpl implements StoreevxicService {
	
	@Resource(name="storeevxicDAO")
	private StoreevxicDAO storeevxicDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	@Override
	public Map selectStoreevxicInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storeevxicDAO.selectStoreevxicCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeevxicDAO.selectStoreevxic(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	public List selectExcelDownStoreevxic(Map parameterMap) throws Exception {
		return storeevxicDAO.selectExcelDownStoreevxic(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownStoreevxic(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return storeevxicDAO.selectExcelDownStoreevxic(parameterMap, excelFormat);
	}
	
	
	
	@Override
	public Map selectStoreevxicInfo2(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storeevxicDAO.selectStoreevxicCount2(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeevxicDAO.selectStoreevxic2(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	public List selectExcelDownStoreevxic2(Map parameterMap) throws Exception {
		return storeevxicDAO.selectExcelDownStoreevxic2(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownStoreevxic2(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return storeevxicDAO.selectExcelDownStoreevxic2(parameterMap, excelFormat);
	}	
}