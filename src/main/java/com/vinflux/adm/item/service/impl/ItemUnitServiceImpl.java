package com.vinflux.adm.item.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.item.service.ItemUnitService;
import com.vinflux.adm.persistence.CenterDAO;
import com.vinflux.adm.persistence.ItemUnitDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.tms.persistence.ItemUnitTmsDAO;
import com.vinflux.vims.persistence.ItemUnitVimsDAO;
import com.vinflux.wms.persistence.ItemUnitWmsDAO;

@Service("itemUnitService")
public class ItemUnitServiceImpl implements ItemUnitService {
	
	@Resource(name="itemUnitDAO")
	private ItemUnitDAO itemUnitDAO;
	
	@Resource(name="itemUnitWmsDAO")
	private ItemUnitWmsDAO itemUnitWmsDAO;
	
	@Resource(name="itemUnitTmsDAO")
	private ItemUnitTmsDAO itemUnitTmsDAO;
	
	@Resource(name="centerDAO")
	private CenterDAO centerDAO;
	
//	@Resource(name="itemUnitIcomDAO")
//	private ItemUnitIcomDAO itemUnitIcomDAO;
	
	@Resource(name="itemUnitVimsDAO")
	private ItemUnitVimsDAO itemUnitVimsDAO;
	
//	@Resource(name="txAdmManager")
//	private DataSourceTransactionManager txAdmManager;
	
	protected static final Log LOG = LogFactory.getLog(ItemUnitServiceImpl.class);
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;	
	
	@Override
	public Map selectItemUnitInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = this.selectItemUnitCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemUnitDAO.selectItemUnit(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public int selectItemUnitCount(Map parameterMap) throws Exception {
		return itemUnitDAO.selectItemUnitCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#selectItemUnit(java.util.Map)
	 */
	@Override
	public List selectItemUnitType(Map parameterMap) throws Exception {
    	return itemUnitDAO.selectItemUnitType(parameterMap);
	}	

	/**
	 * ICUTORDER CHECK 
	 * @param parameterMap
	 * @throws Exception
	 */
	private void checkIcutOrder (Map parameterMap ) throws Exception {
		BigDecimal icutorder = new BigDecimal(parameterMap.get("icutorder").toString());

		String icuttype = MapUtil.getStr(parameterMap,"icuttype" );
		
		if (icutorder != null ) {
			Map queryMap = new HashMap();
			queryMap.put("icutkey", parameterMap.get("icutkey"));
			queryMap.put("owkey", parameterMap.get("owkey"));
			
			List list = itemUnitDAO.selectItemUnitType(queryMap);
			if (list != null && !list.isEmpty() ) {
				for (int i=0 ;list.size() > i ; i++ ) {
					Map map = (Map)list.get(i);
					BigDecimal queryIcutorder = (BigDecimal)map.get("icutorder");
					String queryIcuttype = (String)map.get("icuttype");
					if (!queryIcuttype.equals(icuttype) && icutorder.equals(queryIcutorder) ) { 
						throw new MsgException(10017);
					}
				}
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#insertItemUnit(java.util.Map)
	 */
	@Override
	public void insertItemUnit (Map parameterMap) throws Exception {
		
		/*
		 * 하단 로직은 상품단위 추가시 TADMIN_MST_ICUT 무결성 제약조건에 무조건 위배됨. 16.07.20  
		 * 
		// order 중복 체크 
		this.checkIcutOrder(parameterMap);
		List list = itemUnitDAO.selectTAdminMstIcut(parameterMap);
		
		if (list == null || list.isEmpty()  ) {
		    // INSERT TADMIN_MST_ICUT
			itemUnitDAO.insertTAdminMstIcut(parameterMap);
			// INSERT TADMIN_MST_ICUTXTYPE 
			itemUnitDAO.insertTAdminMstIcutxtype(parameterMap);
	        if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				itemUnitTmsDAO.insertTTMstIcut(parameterMap);
			}
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				itemUnitTmsDAO.insertTTMstIcutxtype(parameterMap);
			} 
			
			 10.21 OMS 삭제작업
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				itemUnitIcomDAO.insertTOrderMstIcut(parameterMap);
			}
			
			
			if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
				itemUnitVimsDAO.insertTViewMstIcut(parameterMap);
			}
			
			 10.21 OMS 삭제작업
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				itemUnitIcomDAO.insertTOrderMstIcutxtype(parameterMap);
			}
			
		}*/
		
		
	    /*
	     * 상품마스터 > 상품단위 > 상품단위 추가 로직 수정 16.07.20 tw.yun
	     * */
		// order 중복 체크 
		this.checkIcutOrder(parameterMap);
		
		List icutList = itemUnitDAO.selectTAdminMstIcut(parameterMap);		
		if (icutList == null || icutList.isEmpty()) {
			 // INSERT TADMIN_MST_ICUT
			itemUnitDAO.insertTAdminMstIcut(parameterMap);
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				itemUnitTmsDAO.insertTTMstIcut(parameterMap);
			}
		}
		
		List icutTypeList = itemUnitDAO.selectTAdminMstIcutxtype(parameterMap);
		if (icutTypeList == null || icutTypeList.isEmpty()) {
			// INSERT TADMIN_MST_ICUTXTYPE 
			itemUnitDAO.insertTAdminMstIcutxtype(parameterMap);	 
		     if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
					itemUnitTmsDAO.insertTTMstIcutxtype(parameterMap);
				}
				
		}
		//AMS, TMS, WMS 센터매핑
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
				List chkList = centerDAO.getTAdminMstCtList(parameterMap);
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int i=0 ;chkList.size() > i ; i++ ) { 
						
							Map chkRow = (Map)chkList.get(i);
							String ctkey = (String)chkRow.get("ctkey");
							parameterMap.put("ctkey", ctkey);
							
							itemUnitDAO.insertTAdminMstIcutxtypexct(parameterMap);
							List selectTWorkMstIcutList = itemUnitWmsDAO.selectTWorkMstIcut(parameterMap);
							if (selectTWorkMstIcutList == null || selectTWorkMstIcutList.isEmpty() ) { 
								itemUnitWmsDAO.insertTWorkMstIcut(parameterMap);							
							}
							itemUnitWmsDAO.insertTWorkMstIcutxtype(parameterMap);
							
							List typelist = itemUnitTmsDAO.selectTTMstIcutxtype(parameterMap);
							if(typelist==null || typelist.isEmpty()) {
								itemUnitTmsDAO.insertTTMstIcutxtype(parameterMap);		
							}
							itemUnitTmsDAO.insertTTMstIcutxtypexct(parameterMap);
						}
						
					}
				}
				
			}
		
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#updateItemUnit(java.util.Map)
	 */
	@Override
	public void updateItemUnit (Map parameterMap) throws Exception {

		// order 중복 체크 
		this.checkIcutOrder(parameterMap);
		
		int icutxtypecnt = 0, icutcnt = 0;
		
		/* 10.21 OMS 삭제작업
		// ICOM
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			icutcnt = itemUnitIcomDAO.updateTOrderMstIcut(parameterMap);
			if(icutcnt==0){
				itemUnitIcomDAO.insertTOrderMstIcut(parameterMap);
			}
			
			icutxtypecnt = itemUnitIcomDAO.updateTOrderMstIcutxtype(parameterMap);
			if(icutxtypecnt==0){
				itemUnitIcomDAO.insertTOrderMstIcutxtype(parameterMap);
			}
		}*/
		
		// VIMS
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			icutcnt = itemUnitVimsDAO.updateTViewMstIcut(parameterMap);
			if(icutcnt==0){
				itemUnitVimsDAO.insertTViewMstIcut(parameterMap);
			}
			
			icutxtypecnt = itemUnitVimsDAO.updateTViewMstIcutxtype(parameterMap);
			if(icutxtypecnt==0){
				itemUnitVimsDAO.insertTViewMstIcutxtype(parameterMap);
			}
		}*/
		
		// VMS 바라보는 코드 WINFOOD 미사용으로 주석처리
		/*if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			icutcnt = itemUnitVimsDAO.updateTViewMstIcut(parameterMap);
			if(icutcnt==0){
				itemUnitVimsDAO.insertTViewMstIcut(parameterMap);
			}
			
			icutxtypecnt = itemUnitVimsDAO.updateTViewMstIcutxtype(parameterMap);
			if(icutxtypecnt==0){
				itemUnitVimsDAO.insertTViewMstIcutxtype(parameterMap);
			}
		}*/
		
		
		// WMS 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			itemUnitWmsDAO.updateTWorkMstIcut(parameterMap);
			itemUnitWmsDAO.updateTWorkMstIcutxtype(parameterMap);
		}
		
		// TMS 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			itemUnitTmsDAO.updateTTMstIcut(parameterMap);
			itemUnitTmsDAO.updateTTMstIcutxtype(parameterMap);
		}

		
		// ADMIN
		itemUnitDAO.updateTAdminMstIcut(parameterMap);
		itemUnitDAO.updateTAdminMstIcutxtype(parameterMap);
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#deleteItemUnit(java.util.Map)
	 */
	@Override
	public void deleteItemUnit (CommandMap parameterMap) throws Exception {
		
		List deleteList = parameterMap.getList("DELETE_LIST");
		for (int i=0 ;deleteList.size() > i ; i++  ) {
			Map rowMap = (Map)deleteList.get(i);
		
			Map selectMap = new HashMap();
			selectMap.put("icutkey", rowMap.get("icutkey"));
			selectMap.put("owkey", rowMap.get("owkey"));
			
			/*************** WMS ***************/ 
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				// 창고의 물품 그룹 마스터(M.TWORK_MST_ICUTXTYPE) 테이블
				itemUnitWmsDAO.deleteTWorkMstIcutxtype(rowMap);
				
				//List list = itemUnitWmsDAO.selectTWorkMstIcutxtype(selectMap);
				//if (list == null || list.isEmpty() ) { 
					// ICOM.TORDER_MST_ICUTXTYPE
				//	itemUnitWmsDAO.deleteTWorkMstIcut(rowMap);
				//}
			}
			
	
			/*************** ICOM ***************/
			/* 10.21 OMS 삭제작업
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				// ICOM.TORDER_MST_ICUT
				itemUnitIcomDAO.deleteTOrderMstIcutxtype(parameterMap);
				List list = itemUnitIcomDAO.selectTOrderMstIcutxtype(selectMap);
				if (list == null || list.size()< 1) { 
					// ICOM.TORDER_MST_ICUTXTYPE
					itemUnitIcomDAO.deleteTOrderMstIcut(parameterMap);
				}
			}*/
			
			/*************** TMS ***************/
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				// TMS.TT_MST_ICUT
				itemUnitTmsDAO.deleteTTMstIcutxtypexct(rowMap);
				itemUnitTmsDAO.deleteTTMstIcutxtype(rowMap);
				//itemUnitTmsDAO.deleteTTMstIcut(rowMap);
				//List list = itemUnitTmsDAO.selectTTMstIcutxtype(selectMap);
				//if (list == null || list.isEmpty() ) { 
					// TMS.TT_MST_ICUTXTYPE
					//itemUnitTmsDAO.deleteTTMstIcut(rowMap);
				//}
			}
			
			/*************** VIMS ***************/
			/*
			if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
				// VIMS.TVIEW_MST_ICUT
				itemUnitVimsDAO.deleteTViewMstIcutxtype(parameterMap);
				List list = itemUnitVimsDAO.selectTViewMstIcutxtype(selectMap);
				if (list == null || list.size()< 1) { 
					// ICOM.TVIEW_MST_ICUTXTYPE
					itemUnitVimsDAO.deleteTViewMstIcut(parameterMap);
				}
			}*/
	
			
			/*************** ADMIN ***************/
			//	 관리자의 물품 그룹 마스터(ADMIN.TADMIN_MST_ICUTXCT) 테이블
			itemUnitDAO.deleteTAdminMstIcutxtypexct(rowMap);
			
			// 관리자의 물품 그룹별 센터 마스터(ADMIN.TADMIN_MST_ICUTXCT)  테이블
			itemUnitDAO.deleteTAdminMstIcutxtype(rowMap);
//			List list = itemUnitDAO.selectTAdminMstIcutxtype(selectMap);
//			if (list == null || list.isEmpty() ) { 
//				//	 관리자의 물품 그룹 마스터(ADMIN.TADMIN_MST_ICGR) 테이블
//				itemUnitDAO.deleteTAdminMstIcut(rowMap);
//			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#selectTAdminMstIcutxct(java.util.Map)
	 */
	@Override
	public List selectTAdminMstIcutxtypexct(Map parameterMap) throws Exception {
		return itemUnitDAO.selectTAdminMstIcutxtypexct(parameterMap);
	}


	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#saveItemUnitCenter(java.util.Map, java.util.List)
	 */
	@Override
	public void saveItemUnitCenter(Map parameterMap ,  List parameterList) throws Exception {

		// ### DELETE LOGIC ### //
		
		// ## SELECT ## 
		List chkList = itemUnitDAO.selectTAdminMstIcutxtypexct(parameterMap);
		boolean chkDelete = true;
		
		
		Map selectMap = new HashMap();
		// ### COMPARE LOGIC ### //		
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
				String chkCtkey = (String)chkRow.get("ctkey");
				String chkOwkey = (String)chkRow.get("owkey");				
				String chkIcutkey = (String)chkRow.get("icutkey");
				String chkIcutType = (String)chkRow.get("icuttype");
				
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);
						String ctkey = (String)row.get("ctkey");
						String owkey = (String)row.get("owkey");
						String icutkey = (String)row.get("icutkey");
						String icuttype = (String)row.get("icuttype");
						if (chkCtkey.equals(ctkey)
							&& chkOwkey.equals(owkey)
							&& icutkey.equals(chkIcutkey) 
							&& chkIcutType.equals(icuttype)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					// ## WMS DETAIL DELETE ##
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						itemUnitWmsDAO.deleteTWorkMstIcutxtype(chkRow);
						
//						Map selectMap = new HashMap();		// clear처리로 변경			
						selectMap.clear();
						
						selectMap.put("owkey", MapUtil.getStr(chkRow, "owkey"));
						selectMap.put("ctkey", MapUtil.getStr(chkRow, "ctkey"));
						selectMap.put("icutkey", MapUtil.getStr(chkRow, "icutkey"));
						List selectTWorkMstIcutxtypeList = itemUnitWmsDAO.selectTWorkMstIcutxtype(selectMap);
						selectMap.clear();
						if (selectTWorkMstIcutxtypeList == null || selectTWorkMstIcutxtypeList.isEmpty() ) {
							
							LOG.debug("chkRow =  " + chkRow);
							
							try{
								itemUnitWmsDAO.deleteTWorkMstIcut(chkRow);
							}catch (DataAccessException e3) {
								throw new MsgException(20123);
							}
						}		
					}	
					
					// ## ADMIN DELETE ## 
					itemUnitDAO.deleteTAdminMstIcutxtypexct(chkRow);
					
					// ## TMS DELETE ## 
					itemUnitTmsDAO.deleteTTMstIcutxtypexct(chkRow);
					
				} 
			}			
		}
		
		// ### COMPARE LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String owkey = (String)row.get("owkey");
				String icutkey = (String)row.get("icutkey");
				String ctkey = (String)row.get("ctkey");
				String icuttype = (String)row.get("icuttype");

				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);
						String chkOwkey = (String)chkRow.get("owkey");
						String chkIcutkey = (String)chkRow.get("icutkey");
						String chkCtkey = (String)chkRow.get("ctkey");
						String chkIcutType = (String)chkRow.get("icuttype");

						if (icutkey.equals(chkIcutkey) 
							&& chkCtkey.equals(ctkey)
							&& chkIcutType.equals(icuttype)
							&& chkOwkey.equals(owkey)
							) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					// ## ADMIN INSERT ##
					itemUnitDAO.insertTAdminMstIcutxtypexct(row);
					
					LOG.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
					LOG.debug("insertTTMstIcutxtypexct.row=>"+row);
					LOG.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
					
					// ##  WMS INSRET ## 
					List list = itemUnitDAO.selectItemUnitType(row);
					int size = list.size();
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						for(int j=0 ;size > j ; j++) {
							Map dataRow  = (Map)list.get(j);
							dataRow.put("ctkey", ctkey);
							
							List selectTWorkMstIcutList = itemUnitWmsDAO.selectTWorkMstIcut(dataRow);
							if (selectTWorkMstIcutList == null || selectTWorkMstIcutList.isEmpty() ) { 
								itemUnitWmsDAO.insertTWorkMstIcut(dataRow);							
							}
							itemUnitWmsDAO.insertTWorkMstIcutxtype(dataRow);
							
						}
					}
					
					////////////2016-06-01  syy
					// ## TMS INSERT ##
					for(int j=0 ;size > j ; j++) {
						Map dataRow  = (Map)list.get(j);
						LOG.debug("SYY========"+dataRow);
						List typelist = itemUnitTmsDAO.selectTTMstIcutxtype(row);
						if(typelist==null || typelist.isEmpty()) {
							itemUnitTmsDAO.insertTTMstIcutxtype(dataRow);		
						}
					}
					itemUnitTmsDAO.insertTTMstIcutxtypexct(row);
					//////////2016-06-01  syy
				} 
			}			
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#saveItemUnitCenter(java.util.Map, java.util.List)
	 */
	@Override
	public void saveItemUnitCenter(Map parameterMap) throws Exception {
		if(itemUnitDAO.selectTAdminMstIcutxtypexctCount(parameterMap) < 1) {
			itemUnitDAO.insertTAdminMstIcutxtypexct(parameterMap);
			
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				if(itemUnitWmsDAO.selectTworkMstIcutCount(parameterMap) < 1) {
					itemUnitWmsDAO.insertTWorkMstIcut(parameterMap);							
				}
				itemUnitWmsDAO.insertTWorkMstIcutxtype(parameterMap);
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.item.service.ItemUnitService#selectExcelDownItemUnit(java.util.Map)
	 */
	@Override
	public List selectExcelDownItemUnit(Map parameterMap) throws Exception {
		return itemUnitDAO.selectExcelDownItemUnit(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownItemUnit(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemUnitDAO.selectExcelDownItemUnit(parameterMap, excelFormat);
	}
}