package com.vinflux.adm.item.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.item.service.ItemCodeUnitUploadService;
import com.vinflux.adm.item.service.ItemCodeUploadService;
import com.vinflux.adm.item.service.ItemGroupUploadService;
import com.vinflux.adm.item.service.ItemTrans;
import com.vinflux.adm.item.service.SubstitutionUploadService;
//import com.vinflux.adm.persistence.ItemGroupDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Service("itemTrans")
public class ItemTransImpl implements ItemTrans {
	
//	@Resource(name="itemGroupDAO")
//	private ItemGroupDAO itemGroupDAO;
	
	@Resource(name="itemCodeUploadService")
	private ItemCodeUploadService itemCodeUploadService;
	
	@Resource(name="itemCodeUnitUploadService")
	private ItemCodeUnitUploadService itemCodeUnitUploadService;
	
	@Resource(name="substitutionUploadService")
	private SubstitutionUploadService substitutionUploadService;
	
	@Resource(name="itemGroupUploadService")
	private ItemGroupUploadService itemGroupUploadService;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ItemTransImpl.class);
	
    //상품 업로드
	@Override
	public Map saveItemCodeUpload(Map parameterMap ,  List parameterList) throws Exception {
		
//    	if (parameterList == null  || parameterList.size() < 1 ) {
		if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104); // update data가 올바르지 않습니다. 확인 부탁드립니다.
    	}
    	
		int parameterListSize = parameterList.size();
    	
    	String ulhsKey = (String)parameterMap.get("ulhskey");
    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
    	String ackey = null;
		String owKey = (String)parameterMap.get("owkey");
		String shelfLifeIndicator = null;			// NOPMD
		String shelfLifeCodeType = null;			// NOPMD
		
		CommandController cc = new CommandController();
    	Map msgMap = new HashMap();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    	getMapParam.put("owkey", owKey);
    	
    	//account list
    	getMapParam.put("searchid", "40");
    	Map acMap = commonService.getAdmMasternCodeMap(getMapParam); 
    	LOG.debug("acMap:" + acMap.toString());
    	//owner list
    	getMapParam.put("searchid", "50");
    	Map owMap = commonService.getAdmMasternCodeMap(getMapParam); 
    	LOG.debug("owMap:" + owMap.toString());
    	//center list
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	LOG.debug("ctMap:" + ctMap.toString());
    	//expiration date check Y/N list
    	getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "YESORNO");
//    	Map shelfLifeIndicatorMap = commonService.getAdmMasternCodeMap(getMapParam);		// 처리 로직이 주석처리되어있음.
    	
    	//expiration date check type list
    	getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "SHELFLIFECODETYPE");
//    	Map shelfLifeCodeTypeMap = commonService.getAdmMasternCodeMap(getMapParam);			// 처리 로직이 주석처리되어있음.
    	
    	
		for (int i = 0; i<parameterListSize; i++) {
			
			Map icMap = (Map) parameterList.get(i);
			try {
				String udf2 = (String) icMap.get("udf2");
				icMap.put("loggrpcd", udf2);
				icMap.put("dckey", "1");
				icMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
				this.itemCodeUploadService.updateTadminTempIcup(icMap);
				
				ackey = MapUtil.getStr(icMap, "ackey");
				owKey = MapUtil.getStr(icMap, "owkey");
				shelfLifeIndicator = MapUtil.getStr(icMap, "shelflifeindicator");
				shelfLifeCodeType = MapUtil.getStr(icMap, "shelflifecodetype");
				
				LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				LOG.debug("ItemTransImpl.saveItemCodeUpload.owKey=>"+owKey);
				LOG.debug("ItemTransImpl.saveItemCodeUpload.owMap=>"+owMap);
				LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				
				
				if ( !owMap.containsKey(owKey) ) {	
					throw new MsgException(10006); // 유효하지 않은 화주 코드 입니다.
				}
				
				//Map chkMap = itemGroupDAO.selectPkItemGroup(icMap);
				
				LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				LOG.debug("ItemTransImpl.saveItemCodeUpload.icMap=>"+icMap);
				//LOG.debug("ItemTransImpl.saveItemCodeUpload.chkMap=>"+chkMap);
				LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				
				//if ( chkMap == null ) {
				//	throw new MsgException(10008); //유효하지 않은 물품 그룹 코드 이거나 , 물품 그룹 유형입니다.
				//}
				
				if( ackey != null && !acMap.containsKey(ackey) ) { // 거래쳐 코드가 입럭되었을때
					throw new MsgException(10018); // 유효하지 않은 거래처 코드 입니다.
				}
				
				/*
				if( shelfLifeIndicator != null ) { // 유통기한 체크 여부가 입력 되었을 때
					if (!shelfLifeIndicatorMap.containsKey(shelfLifeIndicator)) { 
						// Y나 N이 입력 되었는지 체크하고
						throw new MsgException(10009); // 유통기한 체크 여부에는 'Y'나 'N' 만 입력 가능합니다.
					} 
					
					if( shelfLifeCodeType == null || "".equals(shelfLifeCodeType )) { 
						//유통기한 체크 유형이 입력 되었는지 체크한다.					
						throw new MsgException(10010); //유통기한 체크 유형을 입력해 주세요.
					} else {
						if ( !shelfLifeCodeTypeMap.containsKey(shelfLifeCodeType) ) { 
							// 입력되었다면 유통기한 체크 유형이 올바르게 입력 되었는지 확인한다.
							throw new MsgException(10011); // 유효하지 않은 유통기한 체크 유형입니다.
						}
					}
				} else { // 유통기한 체크 여부가 입력 되지 않았다면
					if( shelfLifeCodeType != null ) { 
						//유통기한 체크 유형이 입력 되었는지 체크한다.					
						throw new MsgException(10012); // 유통기간 체크 여부가 입력 후, 유통기간 체크 유형을 입력하세요.
					}
				}
				*/
				
				itemCodeUploadService.saveItemCode(icMap, ctMap);
				
				icMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_SUCCESS);				
				icMap.put("ifmessage", "Success");	
			}catch( DataAccessException dae ){
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icMap.put("ifmessage", ifMessage);
				icMap.put("ulhskey", ulhsKey);
				
			}catch( SQLException se ){
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icMap.put("ifmessage", ifMessage);
				icMap.put("ulhskey", ulhsKey);
				
			}catch( MsgException me ){
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap",me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icMap.put("ifmessage", ifMessage);
				icMap.put("ulhskey", ulhsKey);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icMap.put("ifmessage", ifMessage);
				icMap.put("ulhskey", ulhsKey);

			} finally {
				this.itemCodeUploadService.updateTadminTempIcup(icMap);
			}
		}
    	
    	// Select 
    	Map rtnMap = itemCodeUploadService.selectItemCodeUploadInfo(parameterMap);
    	
		return rtnMap;
    }
	
	//상품단위업로드에서 저장버튼 클릭시
	@Override
	public Map saveItemCodeUnitUpload(Map parameterMap ,  List parameterList) throws Exception {
		
//    	if (parameterList == null  || parameterList.size() < 1 ) {
		if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104); // update data가 올바르지 않습니다. 확인 부탁드립니다.
    	}
		int parameterListSize = parameterList.size();
    	
    	String ulhsKey = (String)parameterMap.get("ulhskey");
    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
		
		CommandController cc = new CommandController();
    	Map msgMap = new HashMap();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    	
    	//센터 리스트 가져오는 부분
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
		for (int i = 0; i<parameterListSize; i++) {
			
			Map icutMap = (Map) parameterList.get(i);
			
			//icutMap.put("ulhskey", ulhsKey);
			try {
				icutMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
				//TADMIN_TEMP_ICUTUP에 인터페이스 메시지 UPDATE
				itemCodeUnitUploadService.updateTadminTempIcutup(icutMap);
				
				itemCodeUnitUploadService.saveItemCodeUnit(icutMap, ctMap);
				
				icutMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_SUCCESS);				
				icutMap.put("ifmessage", "Success");

			}catch( DataAccessException dae ){
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icutMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icutMap.put("ifmessage", ifMessage);
				icutMap.put("ulhskey", ulhsKey);
				
			}catch( SQLException se ){
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icutMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icutMap.put("ifmessage", ifMessage);
				icutMap.put("ulhskey", ulhsKey);
				
			}catch( MsgException me ){
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap",me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icutMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icutMap.put("ifmessage", ifMessage);
				icutMap.put("ulhskey", ulhsKey);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icutMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icutMap.put("ifmessage", ifMessage);
				icutMap.put("ulhskey", ulhsKey);

				//itemCodeUnitUploadService.updateTadminTempIcutup(icutMap);
			} finally {
				itemCodeUnitUploadService.updateTadminTempIcutup(icutMap);
			}
		}
    	
    	// Select 
    	Map rtnMap = itemCodeUnitUploadService.selectManageList(parameterMap);
    	
		return rtnMap;
    }	
	
	
	//상품대체업로드에서 저장버튼 클릭시
	@Override
	public Map saveSubstitutionUpload(Map parameterMap ,  List parameterList) throws Exception {
//    	if (parameterList == null  || parameterList.size() < 1 ) {
		if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104); // update data가 올바르지 않습니다. 확인 부탁드립니다.
    	}   

    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
    	
    	CommandController cc = new CommandController();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    
    	//owner list
    	getMapParam.put("searchid", "50");
    	Map owMap = commonService.getAdmMasternCodeMap(getMapParam); 
    	
    	//center list
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
    	int size = parameterList.size();
    	String owKey = null;
    	
        // Save 
    	Map msgMap = new HashMap();
		for (int i=0 ;size > i ; i++ ) { 	
			Map icsbMap = (Map)parameterList.get(i);

			try {
				
				icsbMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
				this.substitutionUploadService.updateTadminTempIcsbup(icsbMap);
				
				owKey = (String)icsbMap.get("owkey");
				
				if ( !owMap.containsKey(owKey) ) {
					throw new MsgException(10006); //유효하지 않은 화주 코드 입니다.
				}

				this.substitutionUploadService.saveSubstitution(icsbMap, ctMap);

				icsbMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_SUCCESS);
				icsbMap.put("ifmessage", "Success");

			} catch( DataAccessException dae ){
				msgMap.clear();
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icsbMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icsbMap.put("ifmessage", ifMessage);
				
			}catch( SQLException se ){
				msgMap.clear();
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icsbMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icsbMap.put("ifmessage", ifMessage);
			}catch( MsgException me ){
				msgMap.clear();
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap",me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icsbMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icsbMap.put("ifmessage", ifMessage);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				icsbMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icsbMap.put("ifmessage", ifMessage);
				
			} finally {
				this.substitutionUploadService.updateTadminTempIcsbup(icsbMap);
			}
		}    

    	// Select 
		Map rtnMap = this.substitutionUploadService.selectSubstitutionUploadInfo(parameterMap);
    	
		return rtnMap;
    }
	
	@Override
	public Map saveItemGroupList(Map parameterMap ,  List parameterList) throws Exception {
		
//    	if (parameterList == null  || parameterList.size() < 1 ) {
		if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104); // update data가 올바르지 않습니다. 확인 부탁드립니다.
    	}

		int parameterListSize = parameterList.size();
    	
    	
    	String ulhsKey = (String)parameterMap.get("ulhskey");
    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
		
		CommandController cc = new CommandController();
    	Map msgMap = new HashMap();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    	
    	//center list
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
    	//ICGRTYPE list
    	getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "ICGRTYPE");
    	Map icgrTypeMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
		for (int i = 0; i<parameterListSize; i++) {
			
			Map icgrMap = (Map) parameterList.get(i);
			
			try {
	
				icgrMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
				itemGroupUploadService.updateTAdminTempIcgrUp(icgrMap);
				
				String icgrType = MapUtil.getStr(icgrMap, "icgrtype");
				
				if(!icgrTypeMap.containsKey(icgrType)) {
					throw new MsgException(10024); // 유효 하지 않은 물품 그룹 유형입니다.
				}
				
				itemGroupUploadService.saveItemGroup(icgrMap, ctMap);
				
				icgrMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_SUCCESS);				
				icgrMap.put("ifmessage", "Success");

			} catch( DataAccessException dae ){
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}
				icgrMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icgrMap.put("ifmessage", ifMessage);
				icgrMap.put("ulhskey", ulhsKey);
				
			}catch( SQLException se){
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}
				icgrMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icgrMap.put("ifmessage", ifMessage);
				icgrMap.put("ulhskey", ulhsKey);
				
			}catch( MsgException me ){
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap", me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}
				icgrMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icgrMap.put("ifmessage", ifMessage);
				icgrMap.put("ulhskey", ulhsKey);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e);
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}
				icgrMap.put("ifstatus",EnvConstant.INTF_STATUS_CODE_FAIL);
				icgrMap.put("ifmessage", ifMessage);
				icgrMap.put("ulhskey", ulhsKey);

			} finally {
				itemGroupUploadService.updateTAdminTempIcgrUp(icgrMap);
			}
		}
    	
    	// Select
		Map rtnMap = new HashMap();
		Integer count = this.itemGroupUploadService.selectCountTAdminTempIcgrUp(parameterMap);
		rtnMap.put("cnt", Integer.toString(count));

    	if (count > 0 ) {     		
    		List itemGroupList = this.itemGroupUploadService.selectListItemGroupUploadTabManage(parameterMap);
    		rtnMap.put("list", itemGroupList);
    	} else {
    		rtnMap.put("list", new ArrayList());
    	}
    	
		return rtnMap;
    }	
}
