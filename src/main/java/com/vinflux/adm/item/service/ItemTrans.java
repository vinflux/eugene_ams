package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

public interface ItemTrans {
	
	/**
     * 아이템 코드 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map saveItemCodeUpload(Map parameterMap ,  List parameterList) throws Exception;
	
	
	/**
     * 아이템 코드 단위 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map saveItemCodeUnitUpload(Map parameterMap ,  List parameterList) throws Exception;
	
	/**
     * 아이템 대체 코드 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map saveSubstitutionUpload(Map parameterMap ,  List parameterList) throws Exception;
	
	/**
     * 아이템 그룹 코드 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map saveItemGroupList(Map parameterMap ,  List parameterList) throws Exception;
}
