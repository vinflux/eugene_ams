package com.vinflux.adm.item.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.item.service.ItemGroupService;
import com.vinflux.adm.persistence.ItemGroupDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.tms.persistence.ItemGroupTmsDAO;
import com.vinflux.wms.persistence.ItemGroupWmsDAO;

@Service("itemGroupService")
public class ItemGroupServiceImpl implements ItemGroupService {
	
	@Resource(name="itemGroupDAO")
	private ItemGroupDAO itemGroupDAO;
	
//	@Resource(name="itemGroupIcomDAO")
//	private ItemGroupIcomDAO itemGroupIcomDAO;
	
	@Resource(name="itemGroupWmsDAO")
	private ItemGroupWmsDAO itemGroupWmsDAO;
	
	@Resource(name="itemGroupTmsDAO")
	private ItemGroupTmsDAO itemGroupTmsDAO;
	
//	@Resource(name="itemGroupVimsDAO")
//	private ItemGroupVimsDAO itemGroupVimsDAO;
	
	@Resource(name="txAdmManager")
	private DataSourceTransactionManager txAdmManager;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Override
	public Map selectItemGroupInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = itemGroupDAO.selectItemGroupCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = itemGroupDAO.selectItemGroup(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	/** **/
	@Override
	public List selectItemGroupCode(Map parameterMap) throws Exception {
		return itemGroupDAO.selectItemGroupCode(parameterMap);
	}
	
	/** **/
	@Override
	public Map selectItemGroup(Map parameterMap) throws Exception {
    	return itemGroupDAO.selectPkItemGroup(parameterMap);
	}	
	
	/** **/
	@Override
	public int selectCountTAdminMstIcgr(Map parameterMap) throws Exception {
    	return itemGroupDAO.selectCountTAdminMstIcgr(parameterMap);
	}	
	
	/** **/
	@Override
	public int selectCountTAdminMstIcgrXCt(Map parameterMap) throws Exception {
    	return itemGroupDAO.selectCountTAdminMstIcgrXCt(parameterMap);
	}	

	@Override
	public void insertOwItemGroup (CommandMap requestMap) throws Exception {
		// 화면에서 넘견 화주변경 대상 데이터
    	List list = requestMap.getList("list");
    	// 로긴한 사용자의 센터 정보
    	Map userInfo = requestMap.getCommonUserInfoMap();
    	// 화면에서 넘겨준 변경할 화주 정보
    	Map paramMap = requestMap.getParamMap();
    	String owkey = (String) paramMap.get("owkey");
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			Map resultMap = selectItemGroup(inputMap);
    			resultMap.put("owkey", owkey);
    			resultMap.put("inserturkey", "");
    			resultMap.put("updateurkey", "");
    			resultMap.put("ctkey", userInfo.get("ctKey"));
    			insertItemGroup (resultMap);
    		}
    	}
//    	System.out.println("aaaa".substring(0, 1000));
	}
	
	/** **/
	@Override
	public void insertItemGroup (Map parameterMap) throws Exception {
		// 이미 저장된 내역 여부 조회
		int cnt = itemGroupDAO.selectCountTAdminMstIcgrXCt(parameterMap);
		if(cnt > 0) {
			throw new MsgException(20006);
		}

		// 저장 
		itemGroupDAO.insertTAdminMstIcgr(parameterMap);
		
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			itemGroupIcomDAO.insertTOrderMstIcgr(parameterMap);
		}*/
		
		//if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			//itemGroupVimsDAO.insertTViewMstIcgr(parameterMap);
		//}
		
		// TMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			itemGroupTmsDAO.insertTTMstIcgr(parameterMap);
		}
		
		// WMS INSERT
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = itemGroupDAO.selectItemGroupCt(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			
        			this.itemGroupWmsDAO.insertTWorkMstIcgr(parameterMap);
        		}
        	}
		}*/
		//AMS, TMS, WMS 센터매핑
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
			List chkList = itemGroupDAO.selectTAdminMstCt(parameterMap);
			
			if (chkList != null && !chkList.isEmpty() ) {
				for (int i=0 ;chkList.size() > i ; i++ ) { 
					
						Map chkRow = (Map)chkList.get(i);
						String ctkey = (String)chkRow.get("ctkey");
						parameterMap.put("ctkey", ctkey);
						
						itemGroupDAO.insertTAdminMstIcgrxct(parameterMap);
						itemGroupTmsDAO.insertTTMstIcgrxct(parameterMap);
						itemGroupWmsDAO.insertTWorkMstIcgr(parameterMap);
					}
				}
			}
		
	}
	
	
	/** **/
	@Override
	public void updateItemGroup (Map parameterMap) throws Exception {
		
		itemGroupDAO.updateTAdminMstIcgr(parameterMap);	
		
		int count = 0;
		
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {			
			count = itemGroupIcomDAO.updateTOrderMstIcgr(parameterMap);
			if(count==0){
				itemGroupIcomDAO.insertTOrderMstIcgr(parameterMap);
			}
		}*/
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			count = itemGroupTmsDAO.updateTTMstIcgr(parameterMap);
			if(count==0){
				itemGroupTmsDAO.insertTTMstIcgr(parameterMap);
			}
		}
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			itemGroupWmsDAO.updateTWorkMstIcgr(parameterMap);	
		}
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			count = itemGroupVimsDAO.updateTViewMstIcgr(parameterMap);
			if(count==0){
				itemGroupVimsDAO.insertTViewMstIcgr(parameterMap);
			}
		}*/
	}
	

	/** **/
	@Override
	public void deleteItemGroup (Map parameterMap) throws Exception {

		// 창고의 물품 그룹 마스터(WM.TWORK_MST_ICGR) 테이블
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			itemGroupWmsDAO.deleteTWorkMstIcgr(parameterMap);
		}
		
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			itemGroupIcomDAO.deleteTOrderMstIcgr(parameterMap);
		}*/
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			itemGroupTmsDAO.deleteTTMstIcgr(parameterMap);
		}
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			itemGroupVimsDAO.deleteTViewMstIcgr(parameterMap);
		}*/
		

		// ADMIN == Transaction Check 
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		TransactionStatus status = txAdmManager.getTransaction(def);
		
		try {
			// 관리자의 물품 그룹별 센터 마스터(ADMIN.TADMIN_MST_ICGRXCT)  테이블
			itemGroupDAO.deleteTAdminMstIcgrxct(parameterMap);
			
			// 관리자의 물품 그룹 마스터(ADMIN.TADMIN_MST_ICGR) 테이블
			itemGroupDAO.deleteTAdminMstIcgr(parameterMap);
			
		} catch (Exception e) {
			txAdmManager.rollback(status);
			throw new MsgException(10004);
		} finally { 
			if(!status.isCompleted()) {
				txAdmManager.commit(status);
			}			
		}
	}
	
	/** **/
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return itemGroupDAO.selectTAdminMstCt(parameterMap);
	}
	
	/** **/
	@Override
	public List selectTAdminMstIcgrxct(Map parameterMap) throws Exception {
		return itemGroupDAO.selectTAdminMstIcgrxct(parameterMap);
	}

	/** **/
	@Override
	public void saveItemGroupCenter(Map parameterMap ,  List parameterList) throws Exception {

		// ### DELETE LOGIC ### //
		
		// ## SELECT ## 
		List chkList = itemGroupDAO.selectTAdminMstIcgrxct(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkOwkey = (String)chkRow.get("owkey");
				String chkIcgrtype = (String)chkRow.get("icgrtype");
				String chkIcgrkey = (String)chkRow.get("icgrkey");
				String chkCtkey = (String)chkRow.get("ctkey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);
						String owkey = (String)row.get("owkey");
						String icgrtype = (String)row.get("icgrtype");
						String icgrkey = (String)row.get("icgrkey");
						String ctkey = (String)row.get("ctkey");
						if (owkey.equals(chkOwkey) && icgrtype.equals(chkIcgrtype) && icgrkey.equals(chkIcgrkey) && chkCtkey.equals(ctkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					// ## DELETE ##
					// WM.TWORK_MST_MULAAPMSG_DT DELETE (WMS Table 생성 전이라 아직 개발을 안함.)
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						itemGroupWmsDAO.deleteTWorkMstIcgr(chkRow);
					}
					//TMS
					itemGroupTmsDAO.deleteTTMstIcgrxct(chkRow);
					// ADMIN.TADMIN_MST_MULAAPMSGXCT DELETE
					itemGroupDAO.deleteTAdminMstIcgrxct(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String owkey = (String)row.get("owkey");
				String icgrtype = (String)row.get("icgrtype");
				String icgrkey = (String)row.get("icgrkey");
				String ctkey = (String)row.get("ctkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkOwkey = (String)chkRow.get("owkey");
						String chkIcgrtype = (String)chkRow.get("icgrtype");
						String chkIcgrkey = (String)chkRow.get("icgrkey");
						String chkCtkey = (String)chkRow.get("ctkey");

						
						if (owkey.equals(chkOwkey) && icgrtype.equals(chkIcgrtype) && icgrkey.equals(chkIcgrkey) && chkCtkey.equals(ctkey)) {
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					// ## INSERT ##
					itemGroupDAO.insertTAdminMstIcgrxct(row);
					
					// ## INSERT ##
					itemGroupTmsDAO.insertTTMstIcgrxct(row);

					// ## INSERT ##
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						itemGroupWmsDAO.insertTWorkMstIcgr(row);
					}
					
				} 
			}			
		}
	}
	
	@Override
	public void saveItemGroupCenter(Map parameterMap) throws Exception {
		if(this.itemGroupDAO.selectCountTAdminMstIcgrXCt(parameterMap) < 1) {
			itemGroupDAO.insertTAdminMstIcgrxct(parameterMap);
			
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				itemGroupWmsDAO.insertTWorkMstIcgr(parameterMap);
			}
		}
	}
	
	@Override
	public List selectExcelDownItemGroup(Map parameterMap) throws Exception {
		return itemGroupDAO.selectExcelDownItemGroup(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownItemGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemGroupDAO.selectExcelDownItemGroup(parameterMap, excelFormat);
	}
	
	@Override
	public int selectItemGroupExcelUploadCount(Map parameterMap) throws Exception {
		return itemGroupDAO.selectItemGroupExcelUploadCount(parameterMap);
	}	
}