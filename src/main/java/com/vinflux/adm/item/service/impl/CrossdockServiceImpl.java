package com.vinflux.adm.item.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.CrossdockService;
import com.vinflux.adm.persistence.CrossdockDAO;
import com.vinflux.framework.exception.MsgException;

@Service("crossdockService")
public class CrossdockServiceImpl implements CrossdockService {
	
	@Resource(name="crossdockDAO")
	private CrossdockDAO crossdockDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectCrossdock (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = crossdockDAO.selectCrossdockCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = crossdockDAO.selectCrossdock(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public void checkCrossdock(Map parameterMap) throws Exception {
		Integer count = crossdockDAO.checkCrossdock(parameterMap);
		if(count > 0){
			throw new MsgException(10023);
		}
	}	
	
	/** **/
	@Override
	public void insertCrossdock(Map parameterMap) throws Exception {
		this.crossdockDAO.insertCrossdock(parameterMap);
	}
	
	@Override
	public void updateCrossdock(Map parameterMap) throws Exception {
		this.crossdockDAO.updateCrossdock(parameterMap);
	}	
	
	@Override
	public void deleteCrossdock(List paramList) throws Exception {
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);			
			this.crossdockDAO.deleteCrossdock(paramMap);
		}	
	}		
	
	@Override
	public Workbook selectExcelCrossdock(Map parameterMap, List<Map> excelFormat) throws Exception {
		return crossdockDAO.selectExcelCrossdock(parameterMap, excelFormat);
	}
}