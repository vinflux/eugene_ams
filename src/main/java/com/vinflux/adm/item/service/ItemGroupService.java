package com.vinflux.adm.item.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;


public interface ItemGroupService {
	
	/** **/
	public Map selectItemGroupInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectItemGroupCode (Map parameterMap) throws Exception;

	/** **/
	public Map selectItemGroup (Map parameterMap) throws Exception;
	
	public int selectCountTAdminMstIcgr(Map parameterMap) throws Exception;
	
	/** **/
	public int selectCountTAdminMstIcgrXCt(Map parameterMap) throws Exception;
	
	/** 화주변경 저장 **/
	public void insertOwItemGroup (CommandMap requestMap) throws Exception;
	
	/** **/
	public void insertItemGroup (Map parameterMap) throws Exception;
	
	/** **/
	public void updateItemGroup (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteItemGroup (Map parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMstCt(Map parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMstIcgrxct (Map parameterMap) throws Exception;
	
	/** **/
	public void saveItemGroupCenter (Map parameterMap,List parameterList) throws Exception;
	
	public void saveItemGroupCenter(Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownItemGroup (Map parameterMap) throws Exception;

	public Workbook selectExcelDownItemGroup(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/** **/
	public int selectItemGroupExcelUploadCount(Map parameterMap) throws Exception;	
	
}
