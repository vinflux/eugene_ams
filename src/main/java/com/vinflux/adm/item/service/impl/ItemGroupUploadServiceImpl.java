package com.vinflux.adm.item.service.impl;


import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.item.service.ItemGroupService;
import com.vinflux.adm.item.service.ItemGroupUploadService;
import com.vinflux.adm.persistence.ItemGroupUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("itemGroupUploadService")
public class ItemGroupUploadServiceImpl implements ItemGroupUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="itemGroupService")
	private ItemGroupService itemGroupService;
	
	@Resource(name="itemGroupUploadDAO")
	private ItemGroupUploadDAO itemGroupUploadDAO;
	
	protected static final Log LOG = LogFactory.getLog(ItemGroupUploadServiceImpl.class);

	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"owkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"icgrkey" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"icgrtype" 			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"icgrname" 			,"" },
	};	
	
	@Override
	public int selectCountTAdminTempIcgrUp(Map parameterMap) throws Exception {
//    	int count = itemGroupUploadDAO.selectCountTAdminTempIcgrUp(parameterMap);
//		return count;
		return itemGroupUploadDAO.selectCountTAdminTempIcgrUp(parameterMap);
	}

	@Override
	public List selectListItemGroupUploadTabManage(Map parameterMap) throws Exception { 		
//        List list = itemGroupUploadDAO.selectListItemGroupUploadTabManage(parameterMap);
//		return list;
		return itemGroupUploadDAO.selectListItemGroupUploadTabManage(parameterMap);
	}
	
	@Override
	public List selectListItemGroupUploadTabUpload(Map parameterMap) throws Exception { 		
//        List list = itemGroupUploadDAO.selectListItemGroupUploadTabUpload(parameterMap);
//		return list;
		return itemGroupUploadDAO.selectListItemGroupUploadTabUpload(parameterMap);
	}
	
	@Override
	public List selectListTAdminTempIcgrUp(Map parameterMap) throws Exception { 		
//        List list = itemGroupUploadDAO.selectListTAdminTempIcgrUp(parameterMap);
//		return list;
		return itemGroupUploadDAO.selectListTAdminTempIcgrUp(parameterMap);
	}
	
	    
    @Override
	public String uploadItemGroupList (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
		
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);
	    	row.put("ctkey", "100");

			//ULHSKEY
	    	itemGroupUploadDAO.insertTAdminTempIcgrUp(row);
		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		itemGroupUploadDAO.insertTAdminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }
    
    @Override
	public void updateTAdminTempIcgrUp(Map parameterMap) throws Exception {
    	itemGroupUploadDAO.updateTAdminTempIcgrUp(parameterMap);
	}
    
    @Override
	public void saveItemGroup(Map parameterMap, Map ctMap) throws Exception {
    	LOG.debug("saveItemGroup Start!!!!!!!!!!!!!!");
    	
    	Set ctSet = ctMap.keySet();
    	Iterator ctIrt = ctSet.iterator();
    	
    	int icgrChkFlag = 0;
    	String ctKey = MapUtil.getStr(parameterMap, "ctkey");
		
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
		if(ctKey == null || "".equals(ctKey)) {
			icgrChkFlag = this.itemGroupService.selectCountTAdminMstIcgr(parameterMap);
			if (icgrChkFlag < 1) {
				this.itemGroupService.insertItemGroup(parameterMap);
				while(ctIrt.hasNext()) {
					String eCtKey = (String)ctIrt.next();
					parameterMap.put("ctkey", eCtKey);

					this.itemGroupService.saveItemGroupCenter(parameterMap);
				}
			} else {
				this.itemGroupService.updateItemGroup(parameterMap);
				while(ctIrt.hasNext()) {
					String eCtKey = (String)ctIrt.next();
					parameterMap.put("ctkey", eCtKey);

					this.itemGroupService.saveItemGroupCenter(parameterMap);
				}
			}
		} else {
			if ( !ctMap.containsKey(ctKey) ) {
				throw new MsgException(10013); //존재하지 않는 센터코드 입니다.
			}

			icgrChkFlag = this.itemGroupService.selectCountTAdminMstIcgr(parameterMap);

			if (icgrChkFlag < 1) {
				this.itemGroupService.insertItemGroup(parameterMap);
				this.itemGroupService.saveItemGroupCenter(parameterMap);
			} else {
				this.itemGroupService.updateItemGroup(parameterMap);
				this.itemGroupService.saveItemGroupCenter(parameterMap);
			}
		}
    }

	@Override
	public List selectExcelItemGroupUploadTabManage(Map parameterMap) throws Exception {
		return itemGroupUploadDAO.selectExcelItemGroupUploadTabManage(parameterMap);
	}
	
	@Override
	public Workbook selectExcelItemGroupUploadTabManage(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return itemGroupUploadDAO.selectExcelItemGroupUploadTabManage(parameterMap, excelFormat);
	}
}