package com.vinflux.adm.role.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.role.service.UsobByRoleService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("usobByRoleController")

@RemotingDestination
public class UsobByRoleController extends CommandController {

	@Resource(name = "usobByRoleService")
	private UsobByRoleService usobByRoleService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UsobByRoleController.class);
	
    /**
     * ADM > 권한 관리 > Role별 사용자 화면 오브젝트 > 상단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/usobByRoleController/selectUserScreenByRole.do")
    @ResponseBody   
    public CommandMap selectUserScreenByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map usByRoleMap = usobByRoleService.selectUserScreenByRole(paramMap);
    	Integer count = MapUtil.getInt(usByRoleMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)usByRoleMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 사용자 화면 오브젝트 > 하단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/usobByRoleController/selectUsobByRole.do")
    @ResponseBody   
    public CommandMap selectUsobByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	requestMap.addList("rtnGrid1", usobByRoleService.selectTAdminMstAdcdDt(paramMap));
    	requestMap.addList("rtnGrid2", usobByRoleService.selectUsobByRole(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 사용자 화면 오브젝트 > 하단 그리드 저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/usobByRoleController/saveUsobByRole.do")
    @ResponseBody   
    public CommandMap saveUsobByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	usobByRoleService.saveUsobByRole(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 권한관리 > Role별 사용자 화면 오브젝트 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/usobByRoleController/excelDownUsobByRole.do")
    @ResponseBody   
    public void excelDownUsobByRole (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = usobByRoleService.selectExcelDownUsobByRole(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(usobByRoleService
				.selectExcelDownUsobByRole(parameterMap, excelFormat),
				fileName, response);
    }     
    
    /**
     * ADM > 권한관리 > Role별 사용자 화면 오브젝트 > Detail 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/usobByRoleController/excelDownDtlUsobByRole.do")
    @ResponseBody  
    public void excelDownDtlUsobByRole (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = usobByRoleService.selectExcelDownDtlUsobByRole(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				usobByRoleService.selectExcelDownDtlUsobByRole(parameterMap,excelFormat),
				fileName, response);
    }  
}


