package com.vinflux.adm.role.web;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.role.service.UserByRoleAdmService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.web.CommandController;

@Controller("userByRoleAdmController")
@RemotingDestination
public class UserByRoleAdmController extends CommandController {

	@Resource(name = "userByRoleAdmService")
	private UserByRoleAdmService userByRoleAdmService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserByRoleAdmController.class);
	
    @RemotingInclude // BDS
	@RequestMapping(value="/userByRoleAdmController/selectUserRole.do", method=RequestMethod.POST)
    @ResponseBody
	public CommandMap selectUserRole(@RequestBody CommandMap requestMap) throws Exception {

    	// get Param OBJ
    	Map param = requestMap.getParamMap();

    	LOG.debug("======================================");
    	LOG.debug(" role param => " + param);
    	LOG.debug("======================================");
    	
    	Map appRoleMap    	   = userByRoleAdmService.selectAppKeyRole(param);
    	Map whRoleMap     	   = userByRoleAdmService.selectCenterKeyRole(param);
    	Map ownerRoleMap  	   = userByRoleAdmService.selectOwnerKeyRole(param);
    	Map menuObjectRoleMap  = userByRoleAdmService.selectMenuObjectKeyRole(param);

		requestMap.addList("rtnAppRole"		   , (ArrayList)appRoleMap.get("appKeyRole"));
		requestMap.addList("rtnWHRole" 		   , (ArrayList)whRoleMap.get("whKeyRole"));
		requestMap.addList("rtnOwnerRole" 	   , (ArrayList)ownerRoleMap.get("ownerKeyRole"));
		requestMap.addList("rtnMenuObjectRole" , (ArrayList)menuObjectRoleMap.get("menuObjectKeyRole"));

    	LOG.debug("======================================");
    	LOG.debug(" role resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    @RemotingInclude // BDS
 	@RequestMapping(value="/userByRoleAdmController/mSelectUserRole.do", method=RequestMethod.POST)
     @ResponseBody
 	public CommandMap mSelectUserRole(@RequestBody CommandMap requestMap) throws Exception {

     	// get Param OBJ
     	Map param = requestMap.getParamMap();

     	Map appRoleMap    	   = userByRoleAdmService.selectAppKeyRole(param);
     	Map whRoleMap     	   = userByRoleAdmService.selectCenterKeyRole(param);
     	Map ownerRoleMap  	   = userByRoleAdmService.selectOwnerKeyRole(param);

 		requestMap.addList("rtnAppRole"		   , (ArrayList)appRoleMap.get("appKeyRole"));
 		requestMap.addList("rtnWHRole" 		   , (ArrayList)whRoleMap.get("whKeyRole"));
 		requestMap.addList("rtnOwnerRole" 	   , (ArrayList)ownerRoleMap.get("ownerKeyRole"));

     	LOG.debug("======================================");
     	LOG.debug(" role resultMap => " + requestMap);
     	LOG.debug("======================================");
     	
     	return requestMap;
     }
    
    
    @RemotingInclude // BDS
 	@RequestMapping(value="/userByRoleAdmController/mSelectUserMenuRole.do", method=RequestMethod.POST)
     @ResponseBody
 	public CommandMap mSelectUserMenuRole(@RequestBody CommandMap requestMap) throws Exception {

     	// get Param OBJ
     	Map param = requestMap.getParamMap();

     	Map menuObjectRoleMap  = userByRoleAdmService.selectMenuObjectKeyRole(param);

 		requestMap.addList("rtnMenuObjectRole" , (ArrayList)menuObjectRoleMap.get("menuObjectKeyRole"));

     	LOG.debug("======================================");
     	LOG.debug(" role resultMap => " + requestMap);
     	LOG.debug("======================================");
     	
     	return requestMap;
     }
}