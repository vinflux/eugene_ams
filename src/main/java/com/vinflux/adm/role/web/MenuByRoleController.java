package com.vinflux.adm.role.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.role.service.MenuByRoleService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("menuByRoleController")

@RemotingDestination
public class MenuByRoleController extends CommandController {

	@Resource(name = "menuByRoleService")
	private MenuByRoleService menuByRoleService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MenuByRoleController.class);
	
    /**
     * ADM > 권한 관리 > Role별 메뉴 > 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/menuByRoleController/selectApplicationByRole.do")
    @ResponseBody   
    public CommandMap selectApplicationByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map apByRoleMap = menuByRoleService.selectApplicationByRole(paramMap);
    	Integer count = MapUtil.getInt(apByRoleMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)apByRoleMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 메뉴 > menu list select
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/menuByRoleController/selectMenuByRoleNexa.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectMenuByRoleNexa(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	List list = menuByRoleService.selectMenuByRoleNexa(paramMap);
    	
    	requestMap.addList("menu_LIST", list);
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 메뉴 > menu list select
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/menuByRoleController/selectMenuByRole.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectMenuByRole(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	Map map = menuByRoleService.selectMenuByRole(paramMap);
    	
    	requestMap.putMap("menuStructure", map);
    	return requestMap;
    }

    /**
     * ADM > 권한 관리 > Role별 메뉴 > 수정
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/menuByRoleController/updateMenuByRoll.do")
    @ResponseBody   
    public CommandMap updateMenuByRoll (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
		List list = requestMap.getList("list");
	
		menuByRoleService.deleteTAdminMstRoxmegt(paramMap);
		
		for (int i=0 ;list.size() > i ; i++  ) {
			Map row = (Map)list.get(i);
			if ("0".equals( MapUtil.getStr(row, "mekey") )) {
				continue;
			}
			menuByRoleService.insertTAdminMstRoxmegt(row);
		}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    } 
}
