package com.vinflux.adm.role.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.role.service.CenterByRoleService;
import com.vinflux.adm.role.service.RoleService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("centerByRoleController")

@RemotingDestination
public class CenterByRoleController extends CommandController {

	@Resource(name = "centerByRoleService")
	private CenterByRoleService centerByRoleService;
	
	@Resource(name = "roleService")
	private RoleService roleService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(CenterByRoleController.class);
	
    /**
     * ADM > 권한 관리 > Role별 센터 > 상단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/selectRole.do")
    @ResponseBody   
    public CommandMap selectRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map roleMap = roleService.selectRole(paramMap);
    	Integer count = MapUtil.getInt(roleMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)roleMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 센터 > POP UP 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/selectForUpdateTAdminMstRo.do")
    @ResponseBody  
    public CommandMap selectForUpdateTAdminMstRo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");   	
    	
    	Map map = roleService.selectForUpdateTAdminMstRo(requestMap.getParamMap());
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 센터 > 하단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/selectCenterByRole.do")
    @ResponseBody   
    public CommandMap selectCenterByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	requestMap.addList("rtnGrid1", centerByRoleService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", centerByRoleService.selectCenterByRole(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > Role별 센터 > 하단 그리드 저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/saveCenterByRole.do")
    @ResponseBody   
    public CommandMap saveCenterByRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	centerByRoleService.saveCenterByRole(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    

	/**
	 * ADM > 권한 관리 > Role별 센터 > 추가, 수정, 삭제
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/centerByRoleController/saveRoleList.do")
	@ResponseBody
	public CommandMap saveRoleList(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				roleService.insertRole(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				roleService.updateRole(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				roleService.deleteRole(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    /**
     * ADM > 권한 관리 > Role별 센터 > 추가, 수정, 삭제
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/saveRole.do")
    @ResponseBody   
    public CommandMap saveRole (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		roleService.insertRole(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		roleService.updateRole(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			roleService.deleteRole(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    } 
    
    /**
     * ADM > 권한관리 > Role별 센터 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerByRoleController/excelDownRole.do")
    @ResponseBody   
    public void excelDownRole (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = roleService.selectExcelDownRole(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				roleService.selectExcelDownRole(parameterMap, excelFormat),
				fileName, response);
    }     
}
