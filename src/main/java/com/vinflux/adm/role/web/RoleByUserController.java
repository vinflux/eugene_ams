package com.vinflux.adm.role.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.role.service.RoleByUserService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("roleByUserController")

@RemotingDestination
public class RoleByUserController extends CommandController {

	@Resource(name = "roleByUserService")
	private RoleByUserService roleByUserService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(RoleByUserController.class);
	
    /**
     * ADM > 권한 관리 > 사용자별 Role > 상단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/roleByUserController/selectUser.do")
    @ResponseBody   
    public CommandMap selectUser (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map userMap = roleByUserService.selectUser(paramMap);
    	Integer count = MapUtil.getInt(userMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)userMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > 사용자별 Role > 하단 그리드 검색
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/roleByUserController/selectRoleByUser.do")
    @ResponseBody   
    public CommandMap selectRoleByUser (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	requestMap.addList("rtnGrid1", roleByUserService.selectTAdminMstRo(paramMap));
    	requestMap.addList("rtnGrid2", roleByUserService.selectRoleByUser(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 권한 관리 > 사용자별 Role > 하단 그리드 저장
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/roleByUserController/saveRoleByUser.do")
    @ResponseBody   
    public CommandMap saveRoleByUser (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	roleByUserService.saveRoleByUser(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 권한관리 > 사용자별 Role > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/roleByUserController/excelDownRoleByUser.do")
    @ResponseBody   
    public void excelDownRoleByUser (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = roleByUserService.selectExcelDownRoleByUser(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(roleByUserService
				.selectExcelDownRoleByUser(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * ADM > 권한관리 > 사용자별 Role > Detail 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/roleByUserController/excelDownDtlRoleByUser.do")
    @ResponseBody  
    public void excelDownDtlRoleByUser (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = roleByUserService.selectExcelDownDtlRoleByUser(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
    	
		ExcelWriteHandler.downloadExcelFile(roleByUserService
				.selectExcelDownDtlRoleByUser(parameterMap, excelFormat),
				fileName, response);
    }
}


