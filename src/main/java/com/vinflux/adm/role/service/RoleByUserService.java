package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface RoleByUserService {
	
	/**
     * 사용자 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectUser (Map parameterMap) throws Exception;
	
	/**
     * Role 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstRo (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectRoleByUser (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자 정보 저장
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void saveRoleByUser(Map parameterMap ,  List parameterList) throws Exception;
	
	/**
     * 엑셀 다운로드를 위한 사용자 정보 검색
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public List selectExcelDownRoleByUser(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownRoleByUser(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	/**
     * 엑셀 다운로드를 위한 Role별 사용자 정보 검색
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public List selectExcelDownDtlRoleByUser(Map parameterMap) throws Exception;

	public Workbook selectExcelDownDtlRoleByUser(Map parameterMap,
			List<Map> excelFormat)throws Exception;

}
