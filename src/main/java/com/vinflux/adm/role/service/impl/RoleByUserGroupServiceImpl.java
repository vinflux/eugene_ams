package com.vinflux.adm.role.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.RoleByUserGroupDAO;
import com.vinflux.adm.role.service.RoleByUserGroupService;

@Service("roleByUserGroupService")
public class RoleByUserGroupServiceImpl implements RoleByUserGroupService {
	
	protected static final Log LOG = LogFactory.getLog(RoleByUserGroupServiceImpl.class);
	
	@Resource(name="roleByUserGroupDAO")
	private RoleByUserGroupDAO roleByUserGroupDAO;
	
	@Override
	public Map selectUserGroup(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = roleByUserGroupDAO.selectUserGroupCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = roleByUserGroupDAO.selectUserGroup(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectTAdminMstRo (Map parameterMap) throws Exception {
		return roleByUserGroupDAO.selectTAdminMstRo(parameterMap);
	}
	
	@Override
	public List selectRoleByUserGroup (Map parameterMap) throws Exception {
		return roleByUserGroupDAO.selectRoleByUserGroup(parameterMap);
	}
	
	@Override
	public void saveRoleByUserGroup(Map parameterMap ,  List parameterList) throws Exception {

		// ## SELECT ## 
		List chkList = roleByUserGroupDAO.selectTAdminMstUrgrxrogt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkUrgrkey = (String)chkRow.get("urgrkey");
				String chkRokey = (String)chkRow.get("rokey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						
						String urgrkey = (String)row.get("urgrkey");
						String rokey = (String)row.get("rokey");

						if (rokey.equals(chkRokey) && urgrkey.equals(chkUrgrkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					roleByUserGroupDAO.deleteTAdminMstUrgrxrogt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String urgrkey = (String)row.get("urgrkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkUrgrkey = (String)chkRow.get("urgrkey");
						
						if (chkRokey.equals(rokey) && chkUrgrkey.equals(urgrkey)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					roleByUserGroupDAO.insertTAdminMstUrgrxrogt(row);
				} 
			}			
		}
	}
	
	@Override
	public List selectExcelDownRoleByUserGroup(Map parameterMap) throws Exception {
		return roleByUserGroupDAO.selectExcelDownRoleByUserGroup(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return roleByUserGroupDAO.selectExcelDownRoleByUserGroup(parameterMap, excelFormat);
	}
	
/*	@Override
	public List selectExcelDownDtlRoleByUserGroup(Map parameterMap) throws Exception{
		return roleByUserGroupDAO.selectRoleByUserGroup(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDtlRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return roleByUserGroupDAO.selectRoleByUserGroup(parameterMap, excelFormat);
	}*/
}