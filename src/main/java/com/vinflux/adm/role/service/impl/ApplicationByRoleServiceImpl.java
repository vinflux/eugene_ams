package com.vinflux.adm.role.service.impl;


import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import com.vinflux.adm.persistence.ApplicationByRoleDAO;
import com.vinflux.adm.role.service.ApplicationByRoleService;

@Service("applicationByRoleService")
public class ApplicationByRoleServiceImpl implements ApplicationByRoleService {
	
	protected static final Log LOG = LogFactory.getLog(ApplicationByRoleServiceImpl.class);
	
	@Resource(name="applicationByRoleDAO")
	private ApplicationByRoleDAO applicationByRoleDAO;
	
	/**
     * 어플레케이션 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@Override
	public List selectTAdminMstAp (Map parameterMap) throws Exception {
		return applicationByRoleDAO.selectTAdminMstAp(parameterMap);
	}
	
	/**
     * Role별 어플레케이션 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@Override
	public List selectApplicationByRole (Map parameterMap) throws Exception {
		return applicationByRoleDAO.selectApplicationByRole(parameterMap);
	}
	
	/**
     * Role별 어플레케이션 정보 저장
     * @param Map parameterMap, List parameterList
     * @return 
     * @throws Exception
     */
	@Override
	public void saveApplicationByRole(Map parameterMap ,  List parameterList) throws Exception {

		// ## SELECT ## 
		List chkList = applicationByRoleDAO.selectTAdminMstRoxapgt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkRokey = (String)chkRow.get("rokey");
				String chkApkey = (String)chkRow.get("apkey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						String rokey = (String)row.get("rokey");
						String apkey = (String)row.get("apkey");

						if (rokey.equals(chkRokey) && apkey.equals(chkApkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					applicationByRoleDAO.deleteTAdminMstRoxapgt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String apkey = (String)row.get("apkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkApkey = (String)chkRow.get("apkey");
						
						if (chkRokey.equals(rokey) && chkApkey.equals(apkey)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					applicationByRoleDAO.insertTAdminMstRoxapgt(row);
				} 
			}			
		}
	}
}