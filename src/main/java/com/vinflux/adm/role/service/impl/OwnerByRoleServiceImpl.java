package com.vinflux.adm.role.service.impl;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.OwnerByRoleDAO;
import com.vinflux.adm.role.service.OwnerByRoleService;

@Service("ownerByRoleService")
public class OwnerByRoleServiceImpl implements OwnerByRoleService {
	
	protected static final Log LOG = LogFactory.getLog(OwnerByRoleServiceImpl.class);
	
	@Resource(name="ownerByRoleDAO")
	private OwnerByRoleDAO ownerByRoleDAO;
	
	@Override
	public List selectTAdminMstOw (Map parameterMap) throws Exception {
		return ownerByRoleDAO.selectTAdminMstOw(parameterMap);
	}

	@Override
	public List selectOwnerByRole (Map parameterMap) throws Exception {
		return ownerByRoleDAO.selectOwnerByRole(parameterMap);
	}

	@Override
	public void saveOwnerByRole(Map parameterMap ,  List parameterList) throws Exception {

		// ## SELECT ## 
		List chkList = ownerByRoleDAO.selectTAdminMstRoxowgt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkRokey = (String)chkRow.get("rokey");
				String chkOwkey = (String)chkRow.get("owkey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						String rokey = (String)row.get("rokey");
						String owkey = (String)row.get("owkey");

						if (rokey.equals(chkRokey) && owkey.equals(chkOwkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					ownerByRoleDAO.deleteTAdminMstRoxowgt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String owkey = (String)row.get("owkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkOwkey = (String)chkRow.get("owkey");
						
						if (chkRokey.equals(rokey) && chkOwkey.equals(owkey)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					ownerByRoleDAO.insertTAdminMstRoxowgt(row);
				} 
			}			
		}
	}
}