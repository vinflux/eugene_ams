package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface RoleByUserGroupService {
	/**
     * 사용자그룹 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectUserGroup (Map parameterMap) throws Exception;
	
	/**
     * Role 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstRo (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자그룹 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectRoleByUserGroup (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자그룹 정보 저장
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void saveRoleByUserGroup(Map parameterMap ,  List parameterList) throws Exception;
	
	/**
     * 엑셀 다운로드를 위한 사용자그룹 정보 검색
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public List selectExcelDownRoleByUserGroup(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
     * 엑셀 다운로드를 위한 Role별 사용자그룹 정보 검색
     * @param parameterMap
     * @return 
     * @throws Exception
     */
/*	public List selectExcelDownDtlRoleByUserGroup(Map parameterMap) throws Exception;

	public Workbook selectExcelDownDtlRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat)throws Exception;*/

	
}
