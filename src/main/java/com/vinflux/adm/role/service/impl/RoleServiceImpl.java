package com.vinflux.adm.role.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.ApplicationByRoleDAO;
import com.vinflux.adm.persistence.CenterByRoleDAO;
import com.vinflux.adm.persistence.MenuByRoleDAO;
import com.vinflux.adm.persistence.OwnerByRoleDAO;
import com.vinflux.adm.persistence.RoleByUserDAO;
import com.vinflux.adm.persistence.RoleByUserGroupDAO;
import com.vinflux.adm.persistence.RoleDAO;
import com.vinflux.adm.persistence.UsobByRoleDAO;
import com.vinflux.adm.role.service.RoleService;
import com.vinflux.framework.idgen.IdGenService;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	
	protected static final Log LOG = LogFactory.getLog(RoleServiceImpl.class);
	
	@Resource(name="roleDAO")
	private RoleDAO roleDAO;
	
	@Resource(name="applicationByRoleDAO")
	private ApplicationByRoleDAO applicationByRoleDAO;
	
	@Resource(name="centerByRoleDAO")
	private CenterByRoleDAO centerByRoleDAO;

	@Resource(name="ownerByRoleDAO")
	private OwnerByRoleDAO ownerByRoleDAO;
	
	@Resource(name="menuByRoleDAO")
	private MenuByRoleDAO menuByRoleDAO;
	
	@Resource(name="usobByRoleDAO")
	private UsobByRoleDAO usobByRoleDAO;
	
	@Resource(name="roleByUserGroupDAO")
	private RoleByUserGroupDAO roleByUserGroupDAO;
	
	@Resource(name="roleByUserDAO")
	private RoleByUserDAO roleByUserDAO;
	
	@Resource(name="mstRoGenService")
	private IdGenService mstRoGenService;
	
	@Override
	public Map selectRole(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = roleDAO.selectRoleCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = roleDAO.selectRole(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public Map selectForUpdateTAdminMstRo (Map parameterMap) throws Exception {
		return roleDAO.selectForUpdateTAdminMstRo(parameterMap);
	}
	
	@Override
	public void insertRole (Map parameterMap) throws Exception {
		String rokey = mstRoGenService.getNextStringId();	
		parameterMap.put("rokey", rokey);
		roleDAO.insertTAdminMstRo(parameterMap);	
	}
	
	@Override
	public void updateRole (Map parameterMap) throws Exception {
		roleDAO.updateTAdminMstRo(parameterMap);	
	}

	@Override
	public void deleteRole (Map parameterMap) throws Exception {
		List applicaionByRoleList = applicationByRoleDAO.selectTAdminMstRoxapgt(parameterMap);
		if (applicaionByRoleList != null && !applicaionByRoleList.isEmpty() ) { 
			for (int i=0 ;applicaionByRoleList.size() > i; i ++ ) {
				Map row = (Map)applicaionByRoleList.get(i);
				// TADMIN_MST_ROXAPGT DELETE
				applicationByRoleDAO.deleteTAdminMstRoxapgt(row);
			}
		}
		
		List centerByRoleList = centerByRoleDAO.selectTAdminMstRoxctgt(parameterMap);
		if (centerByRoleList != null && !centerByRoleList.isEmpty() ) { 
			for (int i=0; centerByRoleList.size() > i; i ++ ) {
				Map row = (Map)centerByRoleList.get(i);
				// TADMIN_MST_ROXCTGT DELETE
				centerByRoleDAO.deleteTAdminMstRoxctgt(row);
			}
		}
		
		List ownerByRoleList = ownerByRoleDAO.selectTAdminMstRoxowgt(parameterMap);
		if ( ownerByRoleList != null && !ownerByRoleList.isEmpty() ) {
			for (int i=0; ownerByRoleList.size() > i; i ++ ) {
				Map row = (Map)ownerByRoleList.get(i);
				// TADMIN_MST_ROXOWGT DELETE
				ownerByRoleDAO.deleteTAdminMstRoxowgt(row);
			}
		}
	
		List menuByRoleList = menuByRoleDAO.selectTAdminMstRoxmegt(parameterMap);
		if (menuByRoleList != null && !menuByRoleList.isEmpty() ) { 
			for (int i=0; menuByRoleList.size() > i; i ++ ) {
				Map row = (Map)menuByRoleList.get(i);
				// TADMIN_MST_ROXMEGT DELETE
				menuByRoleDAO.deleteTAdminMstRoxmegt(row);
			}
		}
		
		List usobByRoleList = usobByRoleDAO.selectTAdminMstRoxusobgt(parameterMap);
		if (usobByRoleList != null && !usobByRoleList.isEmpty() ) { 
			for (int i=0; usobByRoleList.size() > i; i ++ ) {
				Map row = (Map)usobByRoleList.get(i);
				// TADMIN_MST_ROXUSOBGT DELETE
				usobByRoleDAO.deleteTAdminMstRoxusobgt(row);
			}
		}
		
		List roleByUserGroupList = roleByUserGroupDAO.selectTAdminMstUrgrxrogt(parameterMap);
		if (roleByUserGroupList != null && !roleByUserGroupList.isEmpty() ) { 
			for (int i=0; roleByUserGroupList.size() > i; i ++ ) {
				Map row = (Map)roleByUserGroupList.get(i);
				// TADMIN_MST_URGRXROGT DELETE
				roleByUserGroupDAO.deleteTAdminMstUrgrxrogt(row);
			}
		}
		
		List roleByUserList = roleByUserDAO.selectTAdminMstUrxrogt(parameterMap);
		if ( roleByUserList != null && !roleByUserList.isEmpty() ) { 
			for (int i=0; roleByUserList.size() > i; i ++ ) {
				Map row = (Map)roleByUserList.get(i);
				// TADMIN_MST_URXROGT DELETE
				roleByUserDAO.deleteTAdminMstUrxrogt(row);
			}
		}
		
		roleDAO.deleteTAdminMstRo(parameterMap);
	}
	
	@Override
	public List selectExcelDownRole(Map parameterMap) throws Exception {
		return roleDAO.selectExcelDownRole(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownRole(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return roleDAO.selectExcelDownRole(parameterMap, excelFormat);
	}
}
