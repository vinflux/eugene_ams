package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;


public interface MenuByRoleService {
	/**
     * Role별 어플리케이션 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectApplicationByRole (Map parameterMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMenuByRole(Map paramMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectMenuByRoleNexa(Map paramMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 삭제
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void deleteTAdminMstRoxmegt (Map parameterMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 추가
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void insertTAdminMstRoxmegt (Map parameterMap) throws Exception;
}
