package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;


public interface OwnerByRoleService {
	/**
     * 화주 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstOw (Map parameterMap) throws Exception;
	
	/**
     * ROLE별 화주 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectOwnerByRole (Map parameterMap) throws Exception;
	
	/**
     * ROLE별 화주 정보 저장
     * @param Map parameterMap, List parameterList
     * @return 
     * @throws Exception
     */
	public void saveOwnerByRole(Map parameterMap ,  List parameterList) throws Exception;
}
