package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface RoleService {
	/**
     * Role 정보 검색
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectRole (Map parameterMap) throws Exception;
	
	/**
     * Role 수정 정보 검색
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectForUpdateTAdminMstRo (Map parameterMap) throws Exception;
	
	/**
     * Role 정보 추가
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	public void insertRole(Map parameterMap) throws Exception;
	
	/**
     * Role 정보 수정
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	public void updateRole(Map parameterMap) throws Exception;
	
	/**
     * Role 정보 삭제
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	public void deleteRole(Map parameterMap) throws Exception;
	
	/**
     * 엑셀 다운로드를 위한 Role 정보 검색
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	public List selectExcelDownRole(Map parameterMap) throws Exception;

	public Workbook selectExcelDownRole(Map parameterMap, List<Map> excelFormat)throws Exception;
}
