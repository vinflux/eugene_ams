package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface UsobByRoleService {
	/**
     * Role별 사용자화면 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectUserScreenByRole (Map parameterMap) throws Exception;
	
	/**
     * 사용자화면 오브젝트 코드 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstAdcdDt (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자화면 오브젝트 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectUsobByRole (Map parameterMap) throws Exception;
	
	/**
     * Role별 사용자화면 오브젝트 정보 저장 
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void saveUsobByRole(Map parameterMap ,  List parameterList) throws Exception;
	
	/**
     * 엑셀다운로드를 위한 Role별 사용자화면 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownUsobByRole(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUsobByRole(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
     * 엑셀다운로드를 위한 Role별 사용자화면 오브젝트 정보 검색
     * @param parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownDtlUsobByRole(Map parameterMap) throws Exception;

	public Workbook selectExcelDownDtlUsobByRole(Map parameterMap,
			List<Map> excelFormat)throws Exception;

}
