package com.vinflux.adm.role.service;

import java.util.Map;

public interface UserByRoleAdmService {
	
	public Map selectAppKeyRole(Map parameterMap) throws Exception;
	
	public Map selectOwnerKeyRole(Map parameterMap) throws Exception;
	
	public Map selectCenterKeyRole(Map parameterMap) throws Exception;
	
	public Map selectMenuObjectKeyRole(Map parameterMap) throws Exception;	
}
