package com.vinflux.adm.role.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.RoleByUserDAO;
import com.vinflux.adm.role.service.RoleByUserService;

@Service("roleByUserService")
public class RoleByUserServiceImpl implements RoleByUserService {
	
	protected static final Log LOG = LogFactory.getLog(RoleByUserServiceImpl.class);
	
	@Resource(name="roleByUserDAO")
	private RoleByUserDAO roleByUserDAO;

	@Override
	public Map selectUser(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = roleByUserDAO.selectUserCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = roleByUserDAO.selectUser(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectTAdminMstRo (Map parameterMap) throws Exception {
		return roleByUserDAO.selectTAdminMstRo(parameterMap);
	}

	@Override
	public List selectRoleByUser (Map parameterMap) throws Exception {
		return roleByUserDAO.selectRoleByUser(parameterMap);
	}

	@Override
	public void saveRoleByUser(Map parameterMap ,  List parameterList) throws Exception {
		// ## SELECT ## 
		List chkList = roleByUserDAO.selectTAdminMstUrxrogt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkUrkey = (String)chkRow.get("urkey");
				String chkRokey = (String)chkRow.get("rokey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						
						String urkey = (String)row.get("urkey");
						String rokey = (String)row.get("rokey");

						if (rokey.equals(chkRokey) && urkey.equals(chkUrkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					roleByUserDAO.deleteTAdminMstUrxrogt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String urkey = (String)row.get("urkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkUrkey = (String)chkRow.get("urkey");
						
						if (chkRokey.equals(rokey) && chkUrkey.equals(urkey)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					roleByUserDAO.insertTAdminMstUrxrogt(row);
				} 
			}			
		}
	}

	@Override
	public List selectExcelDownRoleByUser(Map parameterMap) throws Exception {
		return roleByUserDAO.selectExcelDownRoleByUser(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownRoleByUser(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return roleByUserDAO.selectExcelDownRoleByUser(parameterMap, excelFormat);
	}
	
	@Override
	public List selectExcelDownDtlRoleByUser(Map parameterMap) throws Exception{
		return roleByUserDAO.selectExcelDownDtlRoleByUser(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDtlRoleByUser(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return roleByUserDAO.selectExcelDownDtlRoleByUser(parameterMap, excelFormat);
	}
}