package com.vinflux.adm.role.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.UsobByRoleDAO;
import com.vinflux.adm.role.service.UsobByRoleService;

@Service("usobByRoleService")
public class UsobByRoleServiceImpl implements UsobByRoleService {
	
	protected static final Log LOG = LogFactory.getLog(UsobByRoleServiceImpl.class);
	
	@Resource(name="usobByRoleDAO")
	private UsobByRoleDAO usobByRoleDAO;
	
	@Override
	public Map selectUserScreenByRole(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = usobByRoleDAO.selectUserScreenByRoleCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = usobByRoleDAO.selectUserScreenByRole(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectTAdminMstAdcdDt (Map parameterMap) throws Exception {
		return usobByRoleDAO.selectTAdminMstAdcdDt(parameterMap);
	}
	
	@Override
	public List selectUsobByRole (Map parameterMap) throws Exception {
		return usobByRoleDAO.selectUsobByRole(parameterMap);
	}
	
	@Override
	public void saveUsobByRole(Map parameterMap ,  List parameterList) throws Exception {

		// ## SELECT ## 
		List chkList = usobByRoleDAO.selectTAdminMstRoxusobgt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkRokey = (String)chkRow.get("rokey");
				String chkApkey = (String)chkRow.get("apkey");
				String chkUskey = (String)chkRow.get("uskey");
				String chkUsobType = (String)chkRow.get("usobtype");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						String rokey = (String)row.get("rokey");
						String apkey = (String)row.get("apkey");
						String uskey = (String)row.get("uskey");
						String usobtype = (String)row.get("adcd_dtkey");

						if (rokey.equals(chkRokey) && apkey.equals(chkApkey) && uskey.equals(chkUskey) && usobtype.equals(chkUsobType)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					usobByRoleDAO.deleteTAdminMstRoxusobgt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String apkey = (String)row.get("apkey");
				String uskey = (String)row.get("uskey");
				String usobtype = (String)row.get("adcd_dtkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkApkey = (String)chkRow.get("apkey");
						String chkUskey = (String)chkRow.get("uskey");
						String chkUsobType = (String)chkRow.get("usobtype");
						
						if (chkRokey.equals(rokey) && chkApkey.equals(apkey) && chkUskey.equals(uskey) && chkUsobType.equals(usobtype)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					usobByRoleDAO.insertTAdminMstRoxusobgt(row);
				} 
			}			
		}
	}
	
	@Override
	public List selectExcelDownUsobByRole(Map parameterMap) throws Exception {
		return usobByRoleDAO.selectExcelDownUsobByRole(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownUsobByRole(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return usobByRoleDAO.selectExcelDownUsobByRole(parameterMap,excelFormat);
	}
	
	@Override
	public List selectExcelDownDtlUsobByRole(Map parameterMap) throws Exception{
		return usobByRoleDAO.selectUsobByRole(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDtlUsobByRole(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return usobByRoleDAO.selectUsobByRole(parameterMap, excelFormat);
	}
}