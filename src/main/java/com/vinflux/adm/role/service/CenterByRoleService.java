package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;


public interface CenterByRoleService {
	/**
     * Center 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstCt (Map parameterMap) throws Exception;
	
	/**
     * Role별 Center 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectCenterByRole (Map parameterMap) throws Exception;

	/**
     * Role별 Center 정보 저장
     * @param Map parameterMap, List parameterList
     * @return 
     * @throws Exception
     */
	public void saveCenterByRole(Map parameterMap ,  List parameterList) throws Exception;
}
