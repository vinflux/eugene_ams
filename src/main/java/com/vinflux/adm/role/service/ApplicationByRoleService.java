package com.vinflux.adm.role.service;

import java.util.List;
import java.util.Map;


public interface ApplicationByRoleService {
	/**
     * 어플레케이션 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectTAdminMstAp (Map parameterMap) throws Exception;
	
	/**
     * Role별 어플레케이션 정보 검색
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectApplicationByRole (Map parameterMap) throws Exception;
	
	/**
     * Role별 어플레케이션 정보 저장
     * @param Map parameterMap, List parameterList
     * @return 
     * @throws Exception
     */
	public void saveApplicationByRole(Map parameterMap ,  List parameterList) throws Exception;
}
