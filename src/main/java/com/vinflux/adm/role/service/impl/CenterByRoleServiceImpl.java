package com.vinflux.adm.role.service.impl;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.CenterByRoleDAO;
import com.vinflux.adm.role.service.CenterByRoleService;

@Service("centerByRoleService")
public class CenterByRoleServiceImpl implements CenterByRoleService {
	
	protected static final Log LOG = LogFactory.getLog(CenterByRoleServiceImpl.class);
	
	@Resource(name="centerByRoleDAO")
	private CenterByRoleDAO centerByRoleDAO;

	@Override
	public List selectTAdminMstCt (Map parameterMap) throws Exception {
		return centerByRoleDAO.selectTAdminMstCt(parameterMap);
	}

	@Override
	public List selectCenterByRole (Map parameterMap) throws Exception {
		return centerByRoleDAO.selectCenterByRole(parameterMap);
	}

	@Override
	public void saveCenterByRole(Map parameterMap ,  List parameterList) throws Exception {
		// ## SELECT ## 
		List chkList = centerByRoleDAO.selectTAdminMstRoxctgt(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty() ) {
			for (int i=0 ;chkList.size() > i ; i++ ) { 
				chkDelete = true;
				
				Map chkRow = (Map)chkList.get(i);
								
				String chkRokey = (String)chkRow.get("rokey");
				String chkCtkey = (String)chkRow.get("ctkey");
				
				if (parameterList != null && !parameterList.isEmpty() ) {
					for (int j=0 ;parameterList.size() > j; j++ ) {
						Map row = (Map)parameterList.get(j);

						String rokey = (String)row.get("rokey");
						String ctkey = (String)row.get("ctkey");

						if (rokey.equals(chkRokey) && ctkey.equals(chkCtkey)) {
							chkDelete = false;
							break;
						}
					}
				}
				
				if (chkDelete) {
					centerByRoleDAO.deleteTAdminMstRoxctgt(chkRow);
				} 
			}			
		}
		
		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty() ) {
			for (int i=0 ;parameterList.size() > i ; i++ ) { 
				
				chkInsert = true;
				
				Map row = (Map)parameterList.get(i);
				
				String rokey = (String)row.get("rokey");
				String ctkey = (String)row.get("ctkey");
				
				if (chkList != null && !chkList.isEmpty() ) {
					for (int j=0 ;chkList.size() > j; j++ ) {
						Map chkRow = (Map)chkList.get(j);

						String chkRokey = (String)chkRow.get("rokey");
						String chkCtkey = (String)chkRow.get("ctkey");
						
						if (chkRokey.equals(rokey) && chkCtkey.equals(ctkey)) 
						{
							chkInsert = false;
							break;
						}
					}					
				}
				
				if (chkInsert) {
					centerByRoleDAO.insertTAdminMstRoxctgt(row);
				} 
			}			
		}
	}
}