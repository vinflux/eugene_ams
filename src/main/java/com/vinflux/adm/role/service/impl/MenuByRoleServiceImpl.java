package com.vinflux.adm.role.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.MenuByRoleDAO;
import com.vinflux.adm.role.service.MenuByRoleService;
import com.vinflux.framework.util.MapUtil;

@Service("menuByRoleService")
public class MenuByRoleServiceImpl implements MenuByRoleService {
	
	protected static final Log LOG = LogFactory.getLog(MenuByRoleServiceImpl.class);
	
	@Resource(name="menuByRoleDAO")
	private MenuByRoleDAO menuByRoleDAO;

	@Override
	public Map selectApplicationByRole(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = menuByRoleDAO.selectApplicationByRoleCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = menuByRoleDAO.selectApplicationByRole(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}

	@Override
	public List selectMenuByRoleNexa(Map paramMap) throws Exception {
		return menuByRoleDAO.selectMenuByRoleNexa(paramMap);
	}		

	@Override
	public Map selectMenuByRole(Map paramMap) throws Exception {
		List list = menuByRoleDAO.selectMenuByRole(paramMap);
		
		if ( list == null || list.isEmpty() ){ 
			 return new HashMap();
		}

		int listSize = list.size();

    	// root Setting 가상루트
    	String rootMekey = "0";
    	Map menuMap = new HashMap();
		menuMap.put("mekey", rootMekey);
		menuMap.put("mename", MapUtil.getStr(paramMap, "apkey"));
		menuMap.put("isleaf", "N");
		menuMap.put("leaf", "false");
		menuMap.put("checked", true);
		menuMap.put("children", new ArrayList());

    	// 1Depth Setting
    	for (int i=0 ;listSize > i ; i++ ) {
    		Map row = (Map)list.get(i);
    		String uppermekey = (String)row.get("uppermekey");
    		if (rootMekey.equals(uppermekey) ) {
    			List rootChildrenList = (List)menuMap.get("children");
    			rootChildrenList.add(row);
    		}
			String isleaf = (String)row.get("isleaf");
			if ("N".equals(isleaf) ) { 
				row.put("children", new ArrayList());			// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
			}
    	}
    	
    	// 2Depth Setting
    	List rootChildrenList = (List)menuMap.get("children");
    	for (int i=0 ;rootChildrenList.size()> i ; i++ ) {
    		Map depth1Map = (Map)rootChildrenList.get(i);
    		
    		String meKey = (String)depth1Map.get("mekey");
        	for (int j=0 ;listSize > j ; j++ ) {
        		Map row = (Map)list.get(j);
        		String uppermekey = (String)row.get("uppermekey");
        		if (meKey.equals(uppermekey) ) {
        			List childrenList = (List)depth1Map.get("children");
        			childrenList.add(row);
        			String isleaf = (String)row.get("isleaf");
        			if ("N".equals(isleaf) ) { 
        				row.put("children", new ArrayList());		// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
        			}
        		}
        	}
    	}
    	
    	// 3Depth Setting
    	for (int i=0 ;rootChildrenList.size()> i ; i++ ) {
    		
    		Map depth1Row = (Map)rootChildrenList.get(i);
    		List depth2List  = (List) depth1Row.get("children");
    		
    		if (depth2List != null && !depth2List.isEmpty() ) { 
    			
    			int depth2ListSize = depth2List.size();
    			for (int j=0; j<depth2ListSize; j++ ) {
    				
        			Map depth2Row = (Map)depth2List.get(j);
        			String depth2RowMekey = (String)depth2Row.get("mekey");
        			String depth2RowIsleaf = (String)depth2Row.get("isleaf");
        			
        			if ("Y".equals(depth2RowIsleaf) ) { 
        				continue;
        			} 
        			
        			for (int k=0 ; k<listSize; k++ ) {
        	    		Map row = (Map)list.get(k);
        	    		String uppermekey = (String)row.get("uppermekey");
        	    		if (uppermekey.equals(depth2RowMekey)) {
        	    			List childrenList = (List)depth2Row.get("children");
        	    			childrenList.add(row);
        	    			
        	    			String isleaf = (String)row.get("isleaf");
                			if ("N".equals(isleaf) ) { 
                				row.put("children", new ArrayList());		// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
                			}
        	    		}
        			}
        		}
    		}
    	}
    	
    	// 4Depth Setting
    	for (int i=0 ;rootChildrenList.size()> i ; i++ ) {
    		
    		Map depth1Row = (Map)rootChildrenList.get(i);
    		List depth2List  = (List) depth1Row.get("children");
    		
    		if (depth2List != null && !depth2List.isEmpty() ) { 
    			
    			int depth2ListSize = depth2List.size();
    			for (int j=0; j<depth2ListSize; j++ ) {
    				
        			Map depth2Row = (Map)depth2List.get(j);
        			List depth3List = (List) depth2Row.get("children");
        			
        			if (depth3List != null && !depth3List.isEmpty() ) { 
        				
        				for(int k=0; k< depth3List.size(); k++) {
        					
        					Map depth3Row = (Map)depth3List.get(k);
        					String depth3RowMekey = (String)depth3Row.get("mekey");
                			String depth3RowIsleaf = (String)depth3Row.get("isleaf");
                			
                			if ("Y".equals(depth3RowIsleaf) ) { 
                				continue;
                			} 
                			
                			for (int z=0; z<listSize; z++ ) {
                				
                	    		Map row = (Map)list.get(z);
                	    		String uppermekey = (String)row.get("uppermekey");
                	    		if (uppermekey.equals(depth3RowMekey)) {
                	    			List childrenList = (List)depth3Row.get("children");
                	    			childrenList.add(row);
                	    		}
                			}
        				}
        			}
        		}
    		}
    	}
    	
		return menuMap;
	}

	@Override
	public void deleteTAdminMstRoxmegt (Map parameterMap) throws Exception {
		menuByRoleDAO.deleteTAdminMstRoxmegt(parameterMap);	
	}

	@Override
	public void insertTAdminMstRoxmegt (Map parameterMap) throws Exception {
		menuByRoleDAO.insertTAdminMstRoxmegt(parameterMap);	
	}
}