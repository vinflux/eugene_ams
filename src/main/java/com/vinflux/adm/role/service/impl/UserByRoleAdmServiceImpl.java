package com.vinflux.adm.role.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.UserByRoleAdmDAO;
import com.vinflux.adm.role.service.UserByRoleAdmService;

@Service("userByRoleAdmService")
public class UserByRoleAdmServiceImpl implements UserByRoleAdmService{


	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserByRoleAdmServiceImpl.class); 
    
	@Resource(name="userByRoleAdmDAO")
	private UserByRoleAdmDAO userByRoleAdmDAO;

	@Override
	public Map selectAppKeyRole(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		resultMap.put("appKeyRole", userByRoleAdmDAO.selectAppKeyRole(parameterMap));
		
		return resultMap;
	}

	@Override
	public Map selectOwnerKeyRole(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		resultMap.put("ownerKeyRole", userByRoleAdmDAO.selectOwnerKeyRole(parameterMap));
		
		return resultMap;
	}

	@Override
	public Map selectCenterKeyRole(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		resultMap.put("whKeyRole", userByRoleAdmDAO.selectCenterKeyRole(parameterMap));
		
		return resultMap;
	}

	@Override
	public Map selectMenuObjectKeyRole(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		resultMap.put("menuObjectKeyRole", userByRoleAdmDAO.selectMenuObjectKeyRole(parameterMap));
		
		return resultMap;
	}

}
