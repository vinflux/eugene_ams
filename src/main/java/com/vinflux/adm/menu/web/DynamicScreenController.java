package com.vinflux.adm.menu.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.menu.service.DynamicScreenService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("dynamicScreenController")
@RemotingDestination
public class DynamicScreenController extends CommandController {

	@Resource(name = "dynamicScreenService")
	private DynamicScreenService dynamicScreenService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(DynamicScreenController.class);
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/dynamicScreenController/selectDynamicScreenInitConfig.do")
    @ResponseBody   
    public CommandMap selectDynamicScreenInitConfig (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	Map resultMap = dynamicScreenService.selectDynamicScreenInitConfig(paramMap);
    	
    	requestMap.putMap("DATA_MAP", (Map)resultMap.get("DATA_MAP"));
    	requestMap.addList("DATA_LIST", (List)resultMap.get("DATA_LIST"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/dynamicScreenController/selectDynamicScreenGetData.do")
    @ResponseBody   
    public CommandMap selectDynamicScreenGetData (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	//라니안 CommandMap List에 세팅 명명규칙 어긋남
    	//List dyscCol = requestMap.getList("DYSC_COL");
    	
    	Map resultMap = dynamicScreenService.selectDynamicSql(paramMap);
    	Integer count = MapUtil.getInt(resultMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)resultMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/dynamicScreenController/selectDynamicScreenGetData2.do")
    @ResponseBody   
    public CommandMap selectDynamicScreenGetData2 (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	//라니안 CommandMap List에 세팅 명명규칙 어긋남
    	//List dyscCol = requestMap.getList("DYSC_COL");
    	
    	Map resultMap = dynamicScreenService.selectDynamicSql2(paramMap);
    	Integer count = MapUtil.getInt(resultMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)resultMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
	 * ADMIN > 메뉴관리 > 동적화면 마스터 그리드 조회
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/selectDynamicScreen.do")
	@ResponseBody
	public CommandMap selectDynamicScreen(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);

		Map itemCodeMap = dynamicScreenService.selectDynamicScreen(paramMap);
		Integer count = MapUtil.getInt(itemCodeMap, "cnt");
		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList) itemCodeMap.get("list"));
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
    
	/**
	 * ADMIN > 메뉴관리 > 동적화면 디테일그리드 조회
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/selectDynamicScreenDetail.do")
	@ResponseBody
	public CommandMap selectDynamicScreenDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap(selectDynamicScreenDetail" + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);

		requestMap.addList("rtnGrid2", dynamicScreenService.selectDynamicScreenDetail(paramMap));

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 입력
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/insertDynamicScreen.do")
	@ResponseBody
	public CommandMap insertDynamicScreen(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map param = requestMap.getParamMap();

		dynamicScreenService.insertDynamicScreen(param);

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 수정
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/updateDynamicScreen.do")
	@ResponseBody
	public CommandMap updateDynamicScreen(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getParamMap();

		dynamicScreenService.updateDynamicScreen(paramMap);

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 동적화면 상세그리드 입력
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/insertDynamicScreenDetail.do")
	@ResponseBody
	public CommandMap insertDynamicScreenDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap(insertDynamicScreenDetail" + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getMap("FORM_DATA");

		dynamicScreenService.insertDynamicScreenDetail(paramMap);

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 동적화면 상세그리드 수정
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/updateDynamicScreenDetail.do")
	@ResponseBody
	public CommandMap updateDynamicScreenDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getMap("FORM_DATA");

		dynamicScreenService.updateDynamicScreenDetail(paramMap);

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 동적화면 마스터그리드 삭제
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/deleteDynamicScreen.do")
	@ResponseBody
	public CommandMap deleteDynamicScreen(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DELETE_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			dynamicScreenService.deleteDynamicScreen(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
 
	/**
	 * ADMIN > 메뉴관리 > 동적화면 상세그리드 삭제
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/deleteDynamicScreenDetail.do")
	@ResponseBody
	public CommandMap deleteDynamicScreenDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DELETE_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			dynamicScreenService.deleteDynamicScreenDetail(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
	/**
	 * ADMIN > 메뉴관리 > 동적화면 마스터그리드 엑셀다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/excelDownDynamicScreen.do")
	@ResponseBody
	public void excelDownDynamicScreen(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = menuMgtService.selectSearchConditionExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler.downloadExcelFile(dynamicScreenService
				.selectDynamicScreenExcel(parameterMap, excelFormat),
				fileName, response);
	}
	
	/**
	 * ADMIN > 메뉴관리 > 동적화면 디테일그리드 엑셀다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dynamicScreenController/excelDownDynamicScreenDetail.do")
	@ResponseBody
	public void excelDownSearchConditionDetail(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = menuMgtService.selectSearchConditionDetailExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler.downloadExcelFile(dynamicScreenService
				.selectDynamicScreenDetailExcel(parameterMap, excelFormat),
				fileName, response);
	}
	
}
