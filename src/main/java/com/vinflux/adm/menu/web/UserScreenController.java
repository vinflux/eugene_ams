package com.vinflux.adm.menu.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.menu.service.UserScreenService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("userScreenController")
@RemotingDestination
public class UserScreenController extends CommandController {

	@Resource(name = "userScreenService")
	private UserScreenService userScreenService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserScreenController.class);
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userScreenController/selectUserScreen.do")
    @ResponseBody   
    public CommandMap selectUserScreen (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = userScreenService.selectUserScreenInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

	/**
	 * ADMIN > 메뉴관리 > 사용자화면 > 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/userScreenController/saveUserScreenList.do")
	@ResponseBody
	public CommandMap saveUserScreenList(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
   					userScreenService.insertUserScreen(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				userScreenService.updateUserScreen(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				userScreenService.deleteUserScreen(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > popup > 저장/수정/삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userScreenController/saveUserScreen.do")
    @ResponseBody   
    public CommandMap saveUserScreen (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();	
    	String workType = (String)paramMap.get("workType");

    	// insert
    	if ("INSERT".equals(workType) ) {
        	Map formData = requestMap.getMap("FORM_DATA");
    		userScreenService.insertUserScreen(formData);
   		// update 
    	} else if ("UPDATE".equals(workType) ) {
        	Map formData = requestMap.getMap("FORM_DATA");

    		userScreenService.updateUserScreen(formData);
    	// delete 
    	} else  if ("DELETE".equals(workType) ) {
        	List dataList = requestMap.getList("DATA_LIST");
        	int size = dataList.size();
        	for (int i=0 ; size > i ; i++) { 
        		Map map = (Map)dataList.get(i);
        		userScreenService.deleteUserScreen(map);	
        	}
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 업데이트시 단건 pk 조회 하는 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userScreenController/selectPkUserScreen.do")
    @ResponseBody   
    public CommandMap selectPkUserScreen (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	requestMap.putMap("FORM_DATA", userScreenService.selectPkUserScreen(requestMap.getParamMap()));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 사용자 화면 > 엑셀다운  
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userScreenController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = userScreenService.selectExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				userScreenService.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }
}
