package com.vinflux.adm.menu.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.menu.service.MenuService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("menuController")
@RemotingDestination
public class MenuController extends CommandController {

	@Resource(name = "menuService")
	private MenuService menuService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MenuController.class);
    
    /**
     * ADMIN > 메뉴관리 > 메뉴 > 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/menuController/selectMenu.do")
    @ResponseBody   
    public CommandMap selectMenu (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = menuService.selectMenuInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

	/**
	 * ADMIN > 메뉴관리 > 메뉴 > popup > 저장/수정
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/menuController/saveMenuList.do")
	@ResponseBody
	public CommandMap saveMenuList(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				menuService.insertMenu(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				menuService.updateMenu(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				menuService.deleteMenu(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    /**
     * ADMIN > 메뉴관리 > 메뉴 > popup > 저장/수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/menuController/saveMenu.do")
    @ResponseBody   
    public CommandMap saveMenu (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();	
    	String workType = (String)paramMap.get("workType");
    	
    	// insert
    	if ("INSERT".equals(workType) ) {
    		Map formData = requestMap.getMap("FORM_DATA");
    		menuService.insertMenu(formData);
   		// update 
    	} else if ("UPDATE".equals(workType) ) {
    		Map formData = requestMap.getMap("FORM_DATA");
    		menuService.updateMenu(formData);
    	// delete 
    	} else  {
    		List deleteList = requestMap.getList("DELETE_LIST");
    		int deleteListSize = deleteList.size();
    		for (int i=0 ;deleteListSize > i ; i++ ) {
    			Map map = (Map)deleteList.get(i);
    			menuService.deleteMenu(map);
    		}
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 메뉴 > 업데이트시 단건 조회 구문.
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/menuController/selectPkMenu.do")
    @ResponseBody   
    public CommandMap selectPkMenu (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = menuService.selectPkMenu(requestMap.getParamMap());
    	if (list != null && !list.isEmpty() ) {
    		requestMap.putMap("FORM_DATA", (Map)list.get(0));	
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADMIN > 메뉴관리 > 메뉴 > 엑셀다운  
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/menuController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	
    	Map commonMap = requestMap.getCommonMap();
    	Map userInfoMap = (Map)commonMap.get("USER_INFO");
    
    	parameterMap.putAll(param);   
    	parameterMap.putAll(userInfoMap);
    	
    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = menuService.selectExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				menuService.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }    
}
