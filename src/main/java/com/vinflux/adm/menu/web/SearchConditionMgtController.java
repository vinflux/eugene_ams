package com.vinflux.adm.menu.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.menu.service.MenuMgtService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("searchConditionMgtController")
@RemotingDestination
public class SearchConditionMgtController extends CommandController {

	@Resource(name = "menuMgtService")
	private MenuMgtService menuMgtService;

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(SearchConditionMgtController.class);

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터 그리드 조회
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/selectSearchCondition.do")
	@ResponseBody
	public CommandMap selectSearchCondition(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		String mekey = "";
		String mename = "";
		
		List searchList = requestMap.getSearchList();
		for (int i=0 ;searchList.size() > i ; i++ ) {
			Map row = (Map)searchList.get(i);
			String dbColoum = MapUtil.getStr(row, "dbColoum");
			if ("MEKEY".equals(dbColoum) || "MENAME".equals(dbColoum)  ) {
				String value = MapUtil.getStr(row, "value");
				searchList.remove(i);
				i=0;
				if ("MEKEY".equals(dbColoum) ) {
					mekey = value;
				}
				if ("MENAME".equals(dbColoum) ) {
					mename = value;
				}
			}
		}
		
		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);
		
		if (mekey != null && !mekey.isEmpty() ) {
			paramMap.put("MEKEY", mekey);
		}
		if (mename != null && !mename.isEmpty() ) {
			paramMap.put("MENAME", mename);
		}

		Map returnMap = menuMgtService.selectSearchCondition(paramMap);
		Integer count = MapUtil.getInt(returnMap, "cnt");
		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList) returnMap.get("list"));
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 디테일그리드 조회
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/selectSearchConditionDetail.do")
	@ResponseBody
	public CommandMap selectSearchConditionDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap(selectSearchConditionDetail" + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);
		
		Map returnMap = menuMgtService.selectSearchConditionDetail(paramMap);
		Integer count = MapUtil.getInt(returnMap, "cnt");
		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid2", (ArrayList) returnMap.get("list"));
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/saveSearchCondition.do")
	@ResponseBody
	public CommandMap saveSearchCondition(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	Map param = requestMap.getParamMap();
      	
      	if ("ALL".equals(param.get("chkAll")) ) {
        	Map paramMap = requestMap.getSearchQuery();
        	paramMap.putAll(param);
        	list = menuMgtService.selectSearchConditionExcel(paramMap);
      	}
      	
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
					menuMgtService.insertSearchCondition(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				menuMgtService.updateSearchCondition(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) || "ALL".equals(param.get("chkAll"))  ) {
    				menuMgtService.deleteSearchCondition(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 디테일그리드 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/saveSearchConditionDetail.do")
	@ResponseBody
	public CommandMap saveSearchConditionDetail(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
//    			if (DataSet.ROW_TYPE_NAME_INSERTED.equals(inputMap.get("rowTypeName")) ) {
//					menuMgtService.insertSearchConditionDetail(inputMap);
//    			}else if (DataSet.ROW_TYPE_NAME_UPDATED.equals(inputMap.get("rowTypeName")) ) {
//    				menuMgtService.updateSearchConditionDetail(inputMap);
//    			}else if (DataSet.ROW_TYPE_NAME_DELETED.equals(inputMap.get("rowTypeName")) ) {
//    				menuMgtService.deleteSearchConditionDetail(inputMap);
//    			}
    			if ("C".equals(inputMap.get("STATUS")) ) {
					menuMgtService.insertSearchConditionDetail(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				menuMgtService.updateSearchConditionDetail(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				menuMgtService.deleteSearchConditionDetail(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 입력
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/insertSearchCondition.do")
	@ResponseBody
	public CommandMap insertSearchCondition(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map param = requestMap.getParamMap();

		menuMgtService.insertSearchCondition(param);

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 상세그리드 입력
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/insertSearchConditionDetail.do")
	@ResponseBody
	public CommandMap insertSearchConditionDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap(insertSearchConditionDetail" + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getMap("FORM_DATA");

		menuMgtService.insertSearchConditionDetail(paramMap);

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 수정
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/updateSearchCondition.do")
	@ResponseBody
	public CommandMap updateSearchCondition(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getParamMap();

		menuMgtService.updateSearchCondition(paramMap);

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 상세그리드 수정
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/updateSearchConditionDetail.do")
	@ResponseBody
	public CommandMap updateSearchConditionDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getMap("FORM_DATA");

		menuMgtService.updateSearchConditionDetail(paramMap);

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 삭제
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/deleteSearchCondition.do")
	@ResponseBody
	public CommandMap deleteSearchCondition(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DELETE_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			menuMgtService.deleteSearchCondition(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 삭제
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/deleteSearchConditionDetail.do")
	@ResponseBody
	public CommandMap deleteSearchConditionDetail(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DELETE_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			menuMgtService.deleteSearchConditionDetail(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 엑셀다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/excelDownSearchCondition.do")
	@ResponseBody
	public void excelDownSearchCondition(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = menuMgtService.selectSearchConditionExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler.downloadExcelFile(menuMgtService
				.selectSearchConditionExcel(parameterMap, excelFormat),
				fileName, response);
	}

	/**
	 * ADMIN > 메뉴관리 > 사용자화면검색조건 마스터그리드 엑셀다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/searchConditionMgtController/excelDownSearchConditionDetail.do")
	@ResponseBody
	public void excelDownSearchConditionDetail(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = menuMgtService.selectSearchConditionDetailExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler.downloadExcelFile(menuMgtService
				.selectSearchConditionDetailExcel(parameterMap, excelFormat),
				fileName, response);
	}

}
