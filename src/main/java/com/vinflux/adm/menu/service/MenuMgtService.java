package com.vinflux.adm.menu.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface MenuMgtService {
	
	//검색조건 리스트
	public Map selectSearchCondition (Map parameterMap) throws Exception;
	
	//검색조건 디테일 리스트
	public Map selectSearchConditionDetail (Map parameterMap) throws Exception;
	
	//검색조건 디테일 중복검사
	public int selectSearchConditionDetailCount(Map parameterMap) throws Exception;
	
	//검색조건 입력
	public void insertSearchCondition (Map parameterMap) throws Exception;
	
	//검색조건 디테일 입력
	public void insertSearchConditionDetail (Map parameterMap) throws Exception;
	
	//검색조건 업데이트
	public void updateSearchCondition (Map parameterMap) throws Exception;
	
	//검색조건 디테일 업데이트
	public void updateSearchConditionDetail (Map parameterMap) throws Exception;
	
	//검색조건 삭제
	public void deleteSearchCondition (Map parameterMap) throws Exception;
	
	//검색조건 디테일 삭제
	public void deleteSearchConditionDetail (Map parameterMap) throws Exception;
	
	//검색조건 리스트 엑셀
	public List selectSearchConditionExcel(Map parameterMap) throws Exception;
	
	public Workbook selectSearchConditionExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	//검색조건 디테일 리스트 엑셀
	public List selectSearchConditionDetailExcel(Map parameterMap) throws Exception;

	public Workbook selectSearchConditionDetailExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;

	


}
