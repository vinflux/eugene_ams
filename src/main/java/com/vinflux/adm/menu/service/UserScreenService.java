package com.vinflux.adm.menu.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface UserScreenService {
	
	/**
	 * SELECT LIST TADMIN_MST_URSC 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectUserScreenInfo (Map parameterMap) throws Exception;
	
	/**
	 *  INSERT TADMIN_MST_URSC
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertUserScreen (Map parameterMap) throws Exception;
	
	/**
	 * UPDATE TADMIN_MST_URSC
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateUserScreen (Map parameterMap) throws Exception;
	
	/**
	 * DELETE TADMIN_MST_URSC
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteUserScreen (Map parameterMap) throws Exception;
	
	/**
	 * SELECT PK TADMIN_MST_URSC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectPkUserScreen (Map parameterMap) throws Exception;
	
	/**
	 * DATA EXCEL DOWN
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown (Map parameterMap) throws Exception;

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;
}
