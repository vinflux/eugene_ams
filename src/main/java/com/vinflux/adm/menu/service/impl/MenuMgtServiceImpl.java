package com.vinflux.adm.menu.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.menu.service.MenuMgtService;
import com.vinflux.adm.persistence.MenuMgtDAO;
import com.vinflux.framework.exception.MsgException;

@Service("menuMgtService")
public class MenuMgtServiceImpl implements MenuMgtService {

	@Resource(name = "menuMgtDAO")
	private MenuMgtDAO menuMgtDAO;

	@Override
	public Map selectSearchCondition(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = menuMgtDAO.selectSearchConditionCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		if (count > 0) {
			List list = menuMgtDAO.selectSearchCondition(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}
		else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}

	@Override
	public Map selectSearchConditionDetail(Map parameterMap) throws Exception {

		Map resultMap = new HashMap();
		Integer count = menuMgtDAO.selectSearchConditionDetailCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		if (count > 0) {
			List list = menuMgtDAO.selectSearchConditionDetail(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}
		else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;		

	}

	@Override
	public int selectSearchConditionDetailCount(Map parameterMap) throws Exception {
		return menuMgtDAO.selectSearchConditionDetailCount(parameterMap);
	}

	@Override
	public void insertSearchCondition(Map parameterMap) throws Exception {

		this.menuMgtDAO.insertSearchCondition(parameterMap);

	}

	@Override
	public void insertSearchConditionDetail(Map parameterMap) throws Exception {

		this.menuMgtDAO.insertSearchConditionDetail(parameterMap);
	}

	@Override
	public void updateSearchCondition(Map parameterMap) throws Exception {
		int cnt = menuMgtDAO.selectSearchConditionCount(parameterMap);
		if (cnt > 0) {
			this.menuMgtDAO.updateSearchCondition(parameterMap);
		}
		else {
			throw new MsgException("수정하려는  코드가 존재 하지 않습니다.");
		}
	}

	@Override
	public void updateSearchConditionDetail(Map parameterMap) throws Exception {
		int cnt = menuMgtDAO.selectSearchConditionDetailCount(parameterMap);
		if (cnt > 0) {
			this.menuMgtDAO.updateSearchConditionDetail(parameterMap);
		}
		else {
			throw new MsgException("수정하려는  코드가 존재 하지 않습니다.");
		}
	}

	@Override
	public void deleteSearchCondition(Map parameterMap) throws Exception {
		//DETAIL DELETE  
		menuMgtDAO.deleteSearchConditionDetail(parameterMap);
		// HEADER DELETE 
		menuMgtDAO.deleteSearchCondition(parameterMap);
	}

	@Override
	public void deleteSearchConditionDetail(Map parameterMap) throws Exception {
		int cnt = menuMgtDAO.selectSearchConditionDetailCount(parameterMap);
		if (cnt > 0) {
			this.menuMgtDAO.deleteSearchConditionDetail(parameterMap);
		}
		else {
			throw new MsgException("삭제하려는 코드가 존재 하지 않습니다.");
		}
	}

	@Override
	public List selectSearchConditionExcel(Map parameterMap) throws Exception {
		return menuMgtDAO.selectSearchConditionExcel(parameterMap);
	}
	
	@Override
	public Workbook selectSearchConditionExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return menuMgtDAO.selectSearchConditionExcel(parameterMap, excelFormat);
	}

	@Override
	public List selectSearchConditionDetailExcel(Map parameterMap) throws Exception {
		return menuMgtDAO.selectSearchConditionDetailExcel(parameterMap);
	}

	@Override
	public Workbook selectSearchConditionDetailExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return menuMgtDAO.selectSearchConditionDetailExcel(parameterMap,excelFormat);
	}
}
