package com.vinflux.adm.menu.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface MenuService {
	
	/**
	 * SELECT LIST TADMIN_MST_ME 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectMenuInfo (Map parameterMap) throws Exception;
	
	/**
	 *  INSERT TADMIN_MST_ME
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertMenu (Map parameterMap) throws Exception;
	
	/**
	 * UPDATE TADMIN_MST_ME
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateMenu (Map parameterMap) throws Exception;
	
	/**
	 * DELETE TADMIN_MST_ME
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteMenu (Map parameterMap) throws Exception;
	
	/**
	 * SELECT PK TADMIN_MST_ME
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPkMenu (Map parameterMap) throws Exception;
	
	
	/**
	 * SELECT EXCEL DOWN 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown (Map parameterMap) throws Exception;

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;
}
