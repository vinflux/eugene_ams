package com.vinflux.adm.menu.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.menu.service.MenuService;
import com.vinflux.adm.persistence.MenuDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;

@Service("menuService")
public class MenuServiceImpl implements MenuService {
	
	@Resource(name="menuDAO")
	private MenuDAO menuDAO;

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.MenuService#selectMenuInfo(java.util.Map)
	 */
	@Override
	public Map selectMenuInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = menuDAO.selectMenuCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = menuDAO.selectMenu(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.MenuService#insertMenu(java.util.Map)
	 */
	@Override
	public void insertMenu (Map parameterMap) throws Exception {

		// meorder 중복 체크 
		String meorder = (String)parameterMap.get("meorder");
		String uppermekey = (String) parameterMap.get("uppermekey");
		String eqtype = MapUtil.getStr(parameterMap, "eqtype");

		// 메뉴 순서 중복 체크 
		meOrderDuplicatiionCheck(meorder, uppermekey, null, eqtype );

		// data insert
		menuDAO.insertTAdminMstMe(parameterMap);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.MenuService#updateMenu(java.util.Map)
	 */
	@Override
	public void updateMenu (Map parameterMap) throws Exception { 
		// meorder 중복 체크 
		String meorder = (String)parameterMap.get("meorder");
		String uppermekey = (String) parameterMap.get("uppermekey");
		String mekey = (String) parameterMap.get("mekey");
		String eqtype = MapUtil.getStr(parameterMap, "eqtype");
		
		meOrderDuplicatiionCheck(meorder, uppermekey , mekey, eqtype);

		// data update
		if (menuDAO.updateTAdminMstMe(parameterMap) < 1 ) {
			throw new MsgException(1001, "에러가 났어요");
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.MenuService#deleteMenu(java.util.Map)
	 */
	@Override
	public void deleteMenu (Map parameterMap) throws Exception {
		// data delete
		menuDAO.deleteTAdminMstMe(parameterMap); 
	}
	/**
	 * meorder(메뉴 순서) 중복 체크 
	 * @param meorder
	 * @param uppermekey
	 */
	private void meOrderDuplicatiionCheck (String meorder , String uppermekey , String mekey, String eqtype ) throws Exception {
		Map selectMap = new HashMap();
		selectMap.put("uppermekey", uppermekey);
		selectMap.put("eqtype", eqtype);
		List list = menuDAO.selectTAdminMstMe(selectMap);
		if (list != null && !list.isEmpty() &&
			 !"0".equals(uppermekey) ) { // 최상위 루트 메뉴는 모두 meorder가 10이기 때문에 중복 체크를 하지 않는다.
			for (int i=0 ;list.size() > i; i++ ) {
				Map map = (Map)list.get(i);
				if ( mekey != null && mekey.equals(map.get("mekey")) ) {
					continue;
				}
				
				BigDecimal bigMeorderChk = (BigDecimal)map.get("meorder");
				if (bigMeorderChk != null && meorder != null && 
					meorder.equals(bigMeorderChk.toString())){
						throw new MsgException(10026);
						//Map errArg = new HashMap();
						//errArg.put("field", "meorder");
						//errArg.put("value", meorder);
						//throw new MsgException(1001, "에러가 났어요", errArg);
						
				}
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.MenuService#selectPkMenu(java.util.Map)
	 */
	@Override
	public List selectPkMenu(Map parameterMap) throws Exception {
		return menuDAO.selectTAdminMstMe(parameterMap);
	}

	
	@Override
	public List selectExcelDown(Map parameterMap) throws Exception {
		return menuDAO.selectExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return menuDAO.selectExcelDown(parameterMap, excelFormat);
	}
}