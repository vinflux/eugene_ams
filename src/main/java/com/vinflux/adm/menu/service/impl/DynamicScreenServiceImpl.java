package com.vinflux.adm.menu.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.menu.service.DynamicScreenService;
import com.vinflux.adm.persistence.DynamicScreenDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;

@Service("dynamicScreenService")
public class DynamicScreenServiceImpl implements DynamicScreenService {
	
	@Resource(name="dynamicScreenDAO")
	private DynamicScreenDAO dynamicScreenDAO;
	
	@Override
	public Map selectDynamicScreenInitConfig(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Map map = dynamicScreenDAO.selectDynamicScreenInitConfig(parameterMap);
    	parameterMap.putAll(map);
    	List list = dynamicScreenDAO.selectDynamicScreenInitConfigDetail(parameterMap);
    	
    	resultMap.put("DATA_MAP", map);
    	resultMap.put("DATA_LIST", list);
    	
    	return resultMap;
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map selectDynamicSql (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = dynamicScreenDAO.selectDynamicSqlCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = dynamicScreenDAO.selectDynamicSql(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
        	
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map selectDynamicSql2 (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = dynamicScreenDAO.selectDynamicSqlCount2(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = dynamicScreenDAO.selectDynamicSql2(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
        	
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public Map selectDynamicScreen(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = dynamicScreenDAO.selectDynamicScreenCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		if (count > 0) {
			List list = dynamicScreenDAO.selectDynamicScreen(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}
		else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}
	
	@Override
	public List selectDynamicScreenDetail(Map parameterMap) throws Exception {

		List list = dynamicScreenDAO.selectDynamicScreenDetail(parameterMap);

		return list;

	}
	
	@Override
	public int selectDynamicScreenDetailCount(Map parameterMap) throws Exception {
		return dynamicScreenDAO.selectDynamicScreenDetailCount(parameterMap);
	}
	
	
	@Override
	public void insertDynamicScreen(Map parameterMap) throws Exception {
		// data insert
		dynamicScreenDAO.insertDynamicScreen(parameterMap);
	}	
	
	@Override
	public void updateDynamicScreen(Map parameterMap) throws Exception {
		int cnt = dynamicScreenDAO.selectDynamicScreenCount(parameterMap);
		if (cnt > 0) {
			this.dynamicScreenDAO.updateDynamicScreen(parameterMap);
		}
		else {
			throw new MsgException("수정하려는  코드가 존재 하지 않습니다.");
		}
	}
	
	@Override
	public void deleteDynamicScreen(Map parameterMap) throws Exception {
		//DETAIL DELETE  
		dynamicScreenDAO.deleteDynamicScreenDetail(parameterMap);
		// HEADER DELETE 
		dynamicScreenDAO.deleteDynamicScreen(parameterMap);
	}
	
	@Override
	public void deleteDynamicScreenDetail(Map parameterMap) throws Exception {
		int cnt = dynamicScreenDAO.selectDynamicScreenDetailCount(parameterMap);
		if (cnt > 0) {
			this.dynamicScreenDAO.deleteDynamicScreenDetail(parameterMap);
		}
		else {
			throw new MsgException("삭제하려는 코드가 존재 하지 않습니다.");
		}
	}
	
	@Override
	public void insertDynamicScreenDetail(Map parameterMap) throws Exception {

		this.dynamicScreenDAO.insertDynamicScreenDetail(parameterMap);
	}
	
	
	@Override
	public void updateDynamicScreenDetail(Map parameterMap) throws Exception {
		int cnt = dynamicScreenDAO.selectDynamicScreenDetailCount(parameterMap);
		if (cnt > 0) {
			this.dynamicScreenDAO.updateDynamicScreenDetail(parameterMap);
		}
		else {
			throw new MsgException("수정하려는  코드가 존재 하지 않습니다.");
		}
	}
	
	@Override
	public List selectDynamicScreenExcel(Map parameterMap) throws Exception {
		return dynamicScreenDAO.selectDynamicScreenExcel(parameterMap);
	}
	
	@Override
	public Workbook selectDynamicScreenExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return dynamicScreenDAO.selectDynamicScreenExcel(parameterMap, excelFormat);
	}
	
	@Override
	public List selectDynamicScreenDetailExcel(Map parameterMap) throws Exception {
		return dynamicScreenDAO.selectDynamicScreenDetailExcel(parameterMap);
	}
	
	@Override
	public Workbook selectDynamicScreenDetailExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return dynamicScreenDAO.selectDynamicScreenDetailExcel(parameterMap,excelFormat);
	}

	
	
}