package com.vinflux.adm.menu.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.menu.service.UserScreenService;
import com.vinflux.adm.persistence.UserScreenDAO;

@Service("userScreenService")
public class UserScreenServiceImpl implements UserScreenService {
	
	@Resource(name="userScreenDAO")
	private UserScreenDAO userScreenDAO;

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#selectUserScreenInfo(java.util.Map)
	 */
	@Override
	public Map selectUserScreenInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = userScreenDAO.selectUserScreenCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = userScreenDAO.selectUserScreen(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#insertUserScreen(java.util.Map)
	 */
	@Override
	public void insertUserScreen (Map parameterMap) throws Exception {
		// data insert
		userScreenDAO.insertTAdminMstUrsc(parameterMap);
	}	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#updateUserScreen(java.util.Map)
	 */
	@Override
	public void updateUserScreen (Map parameterMap) throws Exception { 
		userScreenDAO.updateTAdminMstUrsc(parameterMap);
	}
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#deleteUserScreen(java.util.Map)
	 */
	@Override
	public void deleteUserScreen (Map parameterMap) throws Exception {
		// data delete
		userScreenDAO.deleteTAdminMstUrsc(parameterMap); 
	}
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#selectPkUserScreen(java.util.Map)
	 */
	@Override
	public Map selectPkUserScreen(Map parameterMap) throws Exception {
		return userScreenDAO.selectTAdminMstUrsc(parameterMap);	
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.menu.service.UserScreenService#selectExcelDown(java.util.Map)
	 */
	@Override
	public List selectExcelDown(Map parameterMap) throws Exception {
		return userScreenDAO.selectExcelDown(parameterMap);	
	}
	
	@Override
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return userScreenDAO.selectExcelDown(parameterMap, excelFormat);	
	}
}