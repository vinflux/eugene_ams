package com.vinflux.adm.menu.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface DynamicScreenService {
	
	/**
	 * 동적 화면 get config 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectDynamicScreenInitConfig (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectDynamicSql (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectDynamicSql2 (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 헤더 조회 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectDynamicScreen (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 디테일 조회 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectDynamicScreenDetail (Map parameterMap) throws Exception;
	
	
	public int selectDynamicScreenDetailCount(Map parameterMap) throws Exception;
	
	/**
	 *  동적 화면 헤더 추가
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertDynamicScreen (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 헤더 수정
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateDynamicScreen (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 헤더 삭제
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteDynamicScreen (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 디테일 삭제
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteDynamicScreenDetail (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 디테일 입력
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertDynamicScreenDetail (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 디테일 수정
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateDynamicScreenDetail (Map parameterMap) throws Exception;
	
	/**
	 * 동적 화면 엑셀다운
	 * @param parameterMap
	 * @throws Exception
	 */
	public List selectDynamicScreenExcel(Map parameterMap) throws Exception;
	
	public Workbook selectDynamicScreenExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
	 * 동적 화면 엑셀다운
	 * @param parameterMap
	 * @throws Exception
	 */
	public List selectDynamicScreenDetailExcel(Map parameterMap) throws Exception;

	public Workbook selectDynamicScreenDetailExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	
	
}
