package com.vinflux.adm.session.service;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public interface LoginService {
	
	/**
	 * LOGIN
	 * @param vo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public Map actionLogin(LoginVO vo, HttpServletRequest request) throws Exception;
	
	/**
	 * LOGOUT
	 * @param vo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public void actionLogout(LoginVO vo, HttpServletRequest request) throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map amfLogin() throws Exception;
}
