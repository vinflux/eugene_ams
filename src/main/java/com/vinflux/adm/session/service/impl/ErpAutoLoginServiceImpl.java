package com.vinflux.adm.session.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.DrvrService;
import com.vinflux.adm.persistence.CommonDAO;
import com.vinflux.adm.persistence.UserByRoleAdmDAO;
import com.vinflux.adm.session.service.ErpAutoLoginService;
import com.vinflux.adm.session.service.LoginVO;
import com.vinflux.adm.system.service.UserHistoryService;
import com.vinflux.adm.user.service.UserService;
import com.vinflux.framework.common.EnvProperties;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.session.LoginManager;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.DateUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.ObjectUtil;
import com.vinflux.framework.util.StringUtil;

/**
 * 일반 로그인을 처리하는 비즈니스 구현 클래스
 * @author 연구개발팀 최순봉
 * @since 2013.05.21
 * @version 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  2013.05.21  최순봉          최초 생성 
 *  </pre>
 */

@Service("erpAutoLoginService")
public class ErpAutoLoginServiceImpl implements ErpAutoLoginService {
    
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ErpAutoLoginServiceImpl.class);
    
    @Resource(name="userService")
    private UserService userService;
    
    @Resource(name="drvrService")
    private DrvrService drvrService;
    
	@Resource(name="userByRoleAdmDAO")
    private UserByRoleAdmDAO userByRoleAdmDAO;
	
	@Resource(name="commonDAO")
    private CommonDAO commonDAO;
	
	@Resource(name = "userHistoryService")
	private UserHistoryService userHistoryService;
	
	private static final String POPUP_FINISH_DAY = EnvProperties.getProperty("Portal.PopupFinishDay");
	
	/**
	 * LOGIN
	 * @param vo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map actionLogin(LoginVO vo, HttpServletRequest request) throws Exception{
		
		Map returnMap = new HashMap();
		Map paramMap = new HashMap();
		// ERP로그인시 비밀번호를 가지고 오지 않음
		String chgPw = "PASS";
		LoginVO loginVO;		
		//LoginVO acloginVO; // lee 추가
		// acloginVO 어디다 쓰는지????
		
		try {
			
			String ipAddress = vo.getIpAddress();
			String uniqueAddr = vo.getUniqueAddr();
			if ( StringUtil.isEmpty(ipAddress) ) {
				//ipAddress = request.getRemoteAddr();
				ipAddress = uniqueAddr;
			}
			
			loginVO = userByRoleAdmDAO.actionLogin(vo);		
			//ID 인증 완료.
			if (loginVO != null && loginVO.getUrKey() != null && !loginVO.getUrKey().equals("")) {
				//PW 6개월 체크
				chgPw = loginVO.getChgPw();
				
				// 유효기간 만료 체크 
				int urCnt = userByRoleAdmDAO.actionLoginCount(vo);
				if(urCnt == 1) {
					returnMap.put("urkey", vo.getUrKey());
					returnMap.put("loginStatus", "URKEY_EXPIERD");
					return returnMap;
				}

				// pw 에러 횟수 체크
				int maxLoginFail = 0;				
				paramMap.put("sckey", "MAXLOGINFAILCOUNT");
				paramMap.put("urkey", loginVO.getUrKey());
				List tempList = commonDAO.selectSystemConfigList(paramMap);
				if (tempList != null  && !tempList.isEmpty() ) { 
					Map tempMap = (Map)tempList.get(0);
					maxLoginFail = MapUtil.getInt(tempMap, "value1");
				}
				
				if( "Y".equals( loginVO.getQuiescenceyn()) ) { 
					returnMap.put("loginStatus", "QUIESCENCE_Y");
					return returnMap;
				}
				//라니안 로그인횟수초과시 로그인 실패처리
				if (loginVO.getLoginFailCount()+1 > maxLoginFail) {
					returnMap.put("loginStatus", "URKEY_LOCK");
					returnMap.put("urkey", loginVO.getUrKey());
				} else {
					boolean appChk = true;
					paramMap.put("urKey", loginVO.getUrKey());
					List appList = userByRoleAdmDAO.selectAppKeyRoleAuth(paramMap);
					if (appList != null && !appList.isEmpty()) {
						appChk = false;
						for (int i=0 ; appList.size() > i ; i++ ) {
							Map row = (Map)appList.get(i);
							String appKey = MapUtil.getStr(row, "appKey");
							
							if (vo.getAppkey().equals(appKey)) { 
								appChk = true;
							}
						}
					}
					if (!appChk && !"ADMIN".equals(vo.getAppkey())) { 
						returnMap.put("loginStatus", "APP AUTH FAIL");
						return returnMap;
					}
					
					// LOGIN 권한 체크 기능 
					if (loginVO.getCtKey() == null || loginVO.getCtKey().isEmpty() ) {
						List centerList = userByRoleAdmDAO.selectCenterKeyRole(paramMap);
						if (centerList != null && !centerList.isEmpty() ) { 
							Map row = (Map)centerList.get(0);
							String centerKey = (String)row.get("centerKey");
							String centerName = (String)row.get("centerName");
							loginVO.setCtKey(centerKey);
							loginVO.setCtKey_desc(centerName);
						}
					} else {
						/**
						 * 기본 센터 코드가 권한에 들어가 있지 않으면 권한의 첫번재 센터로 변경한다. 
						 */
						List centerList = userByRoleAdmDAO.selectCenterKeyRole(paramMap);
						if (centerList != null && !centerList.isEmpty() ) {
							
							boolean chk = false;
							for (int i=0 ; centerList.size() > i ; i++ ) {
								Map row  = (Map)centerList.get(i);
								String centerKey = (String)row.get("centerKey");
								if (loginVO.getCtKey().equals(centerKey)) {
									chk = true;
									break;
								}
							}
							if (!chk) { 
								Map row = (Map)centerList.get(0);
								String centerKey = (String)row.get("centerKey");
								String centerName = (String)row.get("centerName");
								loginVO.setCtKey(centerKey);
								loginVO.setCtKey_desc(centerName);
							}
						} 
						
						// GMT 사용여부 조회
						Map paramgmtMap = new HashMap();
						paramgmtMap.put("sckey", "GMTUSEYN");
						List gmtList = commonDAO.selectSystemConfigList(paramgmtMap);
						String gmtUseYn = "N"; // Defalut 'N'
						
						if(gmtList != null && !gmtList.isEmpty()){
						  Map row = (Map)gmtList.get(0);
						  gmtUseYn = (String)row.get("value1");
						}
						
						SessionVO sessionVO = new SessionVO();
						sessionVO.setUrKey(loginVO.getUrKey());
						sessionVO.setUrName(loginVO.getUrName());
						sessionVO.setUrGrKey(loginVO.getUrGrKey());
						sessionVO.setLaKey(vo.getLaKey());	//client 에서 넘오온 데이터로 다국어 설정.
						sessionVO.setDst(loginVO.getDst());
						sessionVO.setUtcOffset(loginVO.getUtcOffset());
						sessionVO.setCtKey(loginVO.getCtKey());
						sessionVO.setCtKey_desc(loginVO.getCtKey_desc());
						sessionVO.setOwkeym(loginVO.getOwkeym());
						sessionVO.setDivcd(loginVO.getDivcd());
						sessionVO.setLoggrpcd(loginVO.getLoggrpcd());
						sessionVO.setRstcdiv(loginVO.getRstcdiv());
						sessionVO.setRstclgst(loginVO.getRstclgst());
						sessionVO.setUsrtyp(loginVO.getUsrtyp());
						sessionVO.setRep_ackey(loginVO.getRep_ackey());
						sessionVO.setIcgrkey(loginVO.getIcgrkey());
						sessionVO.setGmtUseYn(gmtUseYn);
		 
						boolean sessionChk = false ;
						SessionVO sessionVOChk = new SessionVO();
						try {
							sessionVOChk = SessionManager.getSessionVO(request);	
						} catch (MsgException e) {
							if (e.getCode() == 9998) {
								sessionChk = true;
							}
						}
						
						String sVOChklaKey = "";
						String sVOlaKey = "";
						if(sessionVOChk.getLaKey() != null){
							sVOChklaKey = sessionVOChk.getLaKey();
						}
						if(sessionVO.getLaKey() != null){
							sVOlaKey = sessionVO.getLaKey();
						}
						// session 중복 체크  
						if (!sessionChk) {
							if (!sessionVOChk.getUrKey().equals(sessionVO.getUrKey())
									|| !sVOChklaKey.equals(sVOlaKey)
								){
								returnMap.put("loginStatus", "SESSION FAIL");
								this.actionLogout(loginVO, request);
							} else {
								
								Map historyMap = new HashMap();
								historyMap.put("ipaddress", ipAddress);
								historyMap.put("urkey", loginVO.getUrKey());
								historyMap.put("logindate", DateUtil.getGmtDateTime(""));
								historyMap.put("logoutdate", DateUtil.getGmtDateTime(""));
								historyMap.put("insertdate", DateUtil.getGmtDateTime(""));
								
								historyMap = this.userHistoryService.insertUserHistoryHd(historyMap);
								
								String urliloh_key = (String)historyMap.get("urliloh_key");
								sessionVO.setUrliloh_key(urliloh_key);
								
								SessionManager.setUserInfo(request, sessionVO, "Y");
								returnMap = ObjectUtil.convertObjectToMap(sessionVO);
								returnMap.put("loginStatus", "SUCCESS");
								
								String currentUniqueAddr = loginVO.getUniqueAddr();
								
								LOG.debug("uniqueAddr === > " + uniqueAddr);
								LOG.debug("currentUniqueAddr === > " + currentUniqueAddr);
								
								if (StringUtil.isEmpty(currentUniqueAddr)) {
									returnMap.put("duplLog", "N");
								} else {
									if ( StringUtil.equals(currentUniqueAddr, uniqueAddr) ){
										returnMap.put("duplLog", "N");
									} else {
										returnMap.put("duplLog", "Y");
										LOG.debug("login Duplication!!!");
									}
								}

								loginVO.setUniqueAddr(uniqueAddr);						
								userByRoleAdmDAO.updateUniqueAddr(loginVO);

								Map sessionMap = new HashMap();
								sessionMap.put("urkey", vo.getUrKey());
								sessionMap.put("eqtype", vo.getEqtype());
								sessionMap.put("pushid", vo.getPushid());
								this.userService.setUserXEq(sessionMap);
								if ("TMS".equals(vo.getAppkey())) { 
									this.drvrService.updateDrvr(sessionMap);
								}
							}
						} else {
							
							Map historyMap = new HashMap();
							historyMap.put("ipaddress", ipAddress);
							historyMap.put("urkey", loginVO.getUrKey());
							historyMap.put("logindate", DateUtil.getGmtDateTime(""));
							historyMap.put("logoutdate", DateUtil.getGmtDateTime(""));
							historyMap.put("insertdate", DateUtil.getGmtDateTime(""));
							historyMap = this.userHistoryService.insertUserHistoryHd(historyMap);
							
							String urliloh_key = (String)historyMap.get("urliloh_key");
							sessionVO.setUrliloh_key(urliloh_key);
							
							SessionManager.setUserInfo(request, sessionVO, "Y");
							returnMap = ObjectUtil.convertObjectToMap(sessionVO);
							
							returnMap.put("ptl_cd_company", loginVO.getPtl_cd_company()  );
							returnMap.put("loginStatus", "SUCCESS");
							
							//특정일까지 비밀번호 변경요구 팝업 노출
							LOG.debug("POPUP_FINISH_DAY : " + POPUP_FINISH_DAY);
							if(POPUP_FINISH_DAY != null && 
								POPUP_FINISH_DAY.length() == 8){
									SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
									Date itDay = sdf.parse(POPUP_FINISH_DAY);
									Date targetDay = new Date(itDay.getTime()+ (1000 * 60 * 60 * 24));
									Date today = new Date();
									if(today.compareTo(targetDay) > 0){
										returnMap.put("must_change_pw", "N");	
									}else{
										returnMap.put("must_change_pw", "Y");
									}
							}

							String currentUniqueAddr = loginVO.getUniqueAddr();
							
							LOG.debug("uniqueAddr === > " + uniqueAddr);
							LOG.debug("currentUniqueAddr === > " + currentUniqueAddr);
							
							if (StringUtil.isEmpty(currentUniqueAddr)) {
								returnMap.put("duplLog", "N");
							} else {
								if ( StringUtil.equals(currentUniqueAddr, uniqueAddr) ){
									returnMap.put("duplLog", "N");
								} else {
									returnMap.put("duplLog", "Y");
									LOG.debug("login Duplication!!!");
								}
							}
							
							loginVO.setUniqueAddr(uniqueAddr);
							userByRoleAdmDAO.updateUniqueAddr(loginVO);
						}
						
						userService.actionInitUrpwInit(paramMap);
						int expireDateCnt = userService.checkExpireDateInfo(paramMap);
						if( expireDateCnt == 1 ) {
							returnMap.put("expireDateStatus", "EXPIRED");
						} else {
							returnMap.put("expireDateStatus", "VALID");
							
							Map sessionMap = new HashMap();
							sessionMap.put("urkey", vo.getUrKey());
							sessionMap.put("eqtype", vo.getEqtype());
							sessionMap.put("pushid", vo.getPushid());
							this.userService.setUserXEq(sessionMap);
							if ("TMS".equals(vo.getAppkey())) { 
								this.drvrService.updateDrvr(sessionMap);
							}
						}
					}
				}
			} else {
				returnMap.put("loginStatus", "ID_FAIL");
			}

		} catch (Exception e){
			LOG.error("", e);
			returnMap.put("loginStatus", "ID_FAIL");
		}
		
		LOG.debug("returnMap ===> ");
		LOG.debug(returnMap);
		returnMap.put("chgPw",chgPw);
		return returnMap;
	}
	
	
	/**
	 * LOGOUT
	 * @param vo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public void actionLogout(LoginVO vo, HttpServletRequest request) throws Exception{
		
		//사용자 이력 남기기.
		SessionVO sessionVO = SessionManager.getSessionVO(request);
		String urliloh_key = sessionVO.getUrliloh_key();
		Map historyMap = new HashMap();
		historyMap.put("urliloh_key", urliloh_key);
		historyMap.put("logoutdate", DateUtil.getGmtDateTime(""));
		this.userHistoryService.updateUserHistoryHd(historyMap);
		
		// When logout event occurs, 
		// if (session ip address = ur info ip address ) then ur info ip address set to NULL
		// else nothing to do.
		// Begin
		String storeUniqueAddr = userByRoleAdmDAO.selectIpaddrxUser(vo);
		String sessUniqueAddr = request.getRemoteAddr();
		
		LOG.debug("storeUniqueAddr == > " + storeUniqueAddr);
		LOG.debug("sessUniqueAddr == > " + sessUniqueAddr);

		if(StringUtil.equals(storeUniqueAddr, sessUniqueAddr)) {
			LOG.debug("Unique IP Address Set to NULL!!!");
			vo.setUniqueAddr("");
			userByRoleAdmDAO.updateUniqueAddr(vo);
		}
		// End
		
		HttpSession session = request.getSession();
		if(session != null) {
			session.invalidate();
		}
		
		LoginManager loginManager = LoginManager.getInstance();
		loginManager.removeSession(vo.getUrKey());
		
		//this.removeModuleSession(sessionVO);
		
	}
		
}
