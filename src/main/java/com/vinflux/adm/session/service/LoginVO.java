package com.vinflux.adm.session.service;

import java.io.Serializable;

public class LoginVO implements Serializable {

	private static final long serialVersionUID = 3409508727368314576L;

	private String urKey;
	private String urPw;
	private String urName;
	private String urGrKey;
	private String laKey;
	private String dst;
	private String utcOffset;
	private String ctKey;
	private String ctKey_desc;
	private String owkeym;
	private String closingdate;
	private int loginFailCount;
	private String divcd;
	private String loggrpcd;
	private String usrtyp;
	private String rstcdiv;
	private String rstclgst;	
	private String ipAddress;
	private String uniqueAddr;
	private String appkey;
	private String eqtype;
	private String acType;
	private String pushid;
	private String rep_ackey;
	private String icgrkey;
	private String quiescenceyn; 
	private String ptl_cd_company; 
	private String chgPw; 
 
 
	public String getPtl_cd_company() {
		return ptl_cd_company;
	}
	public void setPtl_cd_company(String ptl_cd_company) {
		this.ptl_cd_company = ptl_cd_company;
	}
	public String getPushid() {
		return pushid;
	}
	public void setPushid(String pushid) {
		this.pushid = pushid;
	}

	public String getEqtype() {
		return eqtype;
	}
	public void setEqtype(String eqtype) {
		this.eqtype = eqtype;
	}
	public String getAppkey() {
		return appkey;
	}
	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}
	public String getOwkeym() {
		return owkeym;
	}
	public void setOwkeym(String owkeym) {
		this.owkeym = owkeym;
	}
	public String getUrKey() {
		return urKey;
	}
	public void setUrKey(String urKey) {
		this.urKey = urKey;
	}
	public String getUrPw() {
		return urPw;
	}
	public void setUrPw(String urPw) {
		this.urPw = urPw;
	}
	public String getUrName() {
		return urName;
	}
	public void setUrName(String urName) {
		this.urName = urName;
	}
	public String getUrGrKey() {
		return urGrKey;
	}
	public void setUrGrKey(String urGrKey) {
		this.urGrKey = urGrKey;
	}
	public String getLaKey() {
		return laKey;
	}
	public void setLaKey(String laKey) {
		this.laKey = laKey;
	}	
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getUtcOffset() {
		return utcOffset;
	}
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}
	public String getCtKey() {
		return ctKey;
	}
	public void setCtKey(String ctKey) {
		this.ctKey = ctKey;
	}
	public String getCtKey_desc() {
		return ctKey_desc;
	}
	public void setCtKey_desc(String ctKey_desc) {
		this.ctKey_desc = ctKey_desc;
	}
	public String getClosingdate() {
		return closingdate;
	}
	public void setClosingdate(String closingdate) {
		this.closingdate = closingdate;
	}

	public int getLoginFailCount() {
		return loginFailCount;
	}
	public void setLoginFailCount(int loginFailCount) {
		this.loginFailCount = loginFailCount;
	}
	public String getDivcd() {
		return divcd;
	}
	public void setDivcd(String divcd) {
		this.divcd = divcd;
	}
	public String getLoggrpcd() {
		return loggrpcd;
	}
	public void setLoggrpcd(String loggrpcd) {
		this.loggrpcd = loggrpcd;
	}
	public String getUsrtyp() {
		return usrtyp;
	}
	public void setUsrtyp(String usrtyp) {
		this.usrtyp = usrtyp;
	}
	public String getRstcdiv() {
		return rstcdiv;
	}
	public void setRstcdiv(String rstcdiv) {
		this.rstcdiv = rstcdiv;
	}
	public String getRstclgst() {
		return rstclgst;
	}
	public void setRstclgst(String rstclgst) {
		this.rstclgst = rstclgst;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getUniqueAddr() {
		return uniqueAddr;
	}
	
	public void setUniqueAddr(String uniqueAddr) {
		this.uniqueAddr = uniqueAddr;
	}
	
	public String getAcType() {
		return acType;
	}
	public void setAcType(String acType) {
		this.acType = acType;
	}
	
	public String getRep_ackey() {
		return rep_ackey;
	}
	
	public void setRep_ackey(String rep_ackey) {
		this.rep_ackey = rep_ackey;
	}
	
	public String getIcgrkey() {
		return icgrkey;
	}
	
	public void setIcgrkey(String icgrkey) {
		this.icgrkey = icgrkey;
	}
	public String getQuiescenceyn() {
		return quiescenceyn;
	}
	public void setQuiescenceyn(String quiescenceyn) {
		this.quiescenceyn = quiescenceyn;
	}
	public String getChgPw() {
		return chgPw;
	}
	public void setChgPw(String chgPw) {
		this.chgPw = chgPw;
	}
}