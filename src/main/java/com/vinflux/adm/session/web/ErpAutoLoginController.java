package com.vinflux.adm.session.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.session.service.ErpAutoLoginService;
import com.vinflux.adm.session.service.LoginVO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.common.EnvProperties;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.ObjectUtil;
import com.vinflux.framework.web.CommandController;

@Controller("erpAutoLoginController")
@RemotingDestination
public class ErpAutoLoginController extends CommandController {
	
    /** LoginService */
	@Resource(name = "erpAutoLoginService")
    private ErpAutoLoginService erpAutoLoginService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ErpAutoLoginController.class);
    
    /**
     * login
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/erpAutoLoginController/actionLogin.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap actionLogin(@RequestBody CommandMap requestMap) throws Exception  {
    	CommandMap returnMap = new CommandMap();
    	Map param = requestMap.getParamMap();
    	// get Param OBJ
    	String ipAddress = (String)requestMap.get("IPADDRESS");
    	// ERP에서 보내준 로그인 사용자 ID
    	String urKey = (String)param.get("urKey");
    	// ERP에서 보내준 인증키
    	String autoAccessToken = (String)param.get("autoAccessToken");
    	LoginVO loginVO = new LoginVO();
    	loginVO.setUrKey(urKey);
    	loginVO.setLaKey("KOR");
    	loginVO.setLoginFailCount(0);
    	loginVO.setAppkey("ADMIN");
    	loginVO.setEqtype("10");

    	loginVO.setIpAddress(ipAddress);
    	
    	// get HttpServletRequest
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	String uniqueAddr = request.getRemoteAddr();
    	loginVO.setUniqueAddr(uniqueAddr);
    	
    	// Global 프로퍼티에 셋팅된 ERP 호출 URL : ERP 인증키 확인
		String getAccessUrl = EnvProperties.getProperty("Globals.ERP.GetAccessUrl");											

    	// ERP 전송 파라미터 세팅
        Map<String,Object> paramsMap = new LinkedHashMap(); 
        paramsMap.put("cId", "MEATLOAN");
        paramsMap.put("uId", urKey);
        paramsMap.put("AccessToken", autoAccessToken);
        
        // ERP 결과 값 관리
        Map resultMap = new HashMap<String, String>(); 
        
        // ERP 호출 : 1->토큰인증, 2->토큰삭제
    	resultMap = erpWebService(getAccessUrl, paramsMap, "1");

    	// ERP 호출 결과 값
        String statusCd = (String)resultMap.get("statusCd");		// 1->성공, 0:실패
        String accessToken = (String)resultMap.get("accessToken");	// ERP 토큰 셋팅
        
        // 1->성공, 0:실패, 토큰 값이 호출한 토큰과 같은경우 성공
    	if("1".equals(statusCd) && accessToken.length() > 0 && accessToken.equals(autoAccessToken)) {
    		// 성공시에 인증기 삭제요청
			String expireAccessUrl = EnvProperties.getProperty("Globals.ERP.ExpireAccessUrl");
    		paramsMap = new LinkedHashMap(); // 파라미터 세팅
            paramsMap.put("cId", "MEATLOAN");
            paramsMap.put("uId", urKey);
            // 1->토큰인증, 2->토큰삭제
            resultMap = erpWebService(expireAccessUrl, paramsMap, "2");
            statusCd = (String)resultMap.get("statusCd"); // 1->성공, 0:실패
            // 인증키 삭제여부 확인 1->성공, 0:실패
            if(!"1".equals(statusCd)) {
//            	System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDD1 : 삭제 실패");
    			throw new MsgException(0); 
            }
    	} else {
//        	System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDD2 : 인증실패  statusCd : "+statusCd+", accessToken : "+accessToken+"=="+autoAccessToken);
			throw new MsgException(0); 
    	}

    	Map loginMap = erpAutoLoginService.actionLogin(loginVO, request);
    	
    	if ( "SUCCESS".equals(loginMap.get("loginStatus")) ) {
    		returnMap.putMap("USER_INFO", loginMap);;
    	} else {
    		returnMap.setMessageCode("99990");
    		returnMap.setErrCode(EnvConstant.COMMAND_ERROR_CODE_FAIL);
    		LOG.debug("###Login Fail!! Status=>"+loginMap.get("loginStatus"));
    		
        	if ("PW_FAIL".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("패스워드를 확인해 주세요"); // 메세지 한글변경요청으로 변경 2018.10.08
        		//returnMap.setMessage("Password you entered do not match.\n(Max Login fail count : " + loginMap.get("loginFailCnt") + " / " + loginMap.get("maxLoginFail") + ")");
        	} else if ("ID_FAIL".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("존재하지 않는 아이디입니다.");
        		//returnMap.setMessage("Username you entered not exists.");
        	} else if ("MAX_PW_FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("ID is expired because you entered your password incorrectly more than " + loginMap.get("maxLoginFail") +" times.");
        		returnMap.setMessage("아이디가 잠겼습니다.(비밀번호 오류 " + loginMap.get("maxLoginFail") +"회 초과)");
        	} else if ("URKEY_EXPIERD".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("ID(" + loginMap.get("urkey") + ")의 사용기간이 만료되었습니다.");
        		//returnMap.setMessage("ID(" + loginMap.get("urkey") + ") is expired.");
        	} else if ("URKEY_LOCK".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("ID(" + loginMap.get("urkey") + ") is locked.");
        		returnMap.setMessage("아이디(" + loginMap.get("urkey") + ")가 잠겼습니다.");
        	} else if ("SESSION FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("[SESSION CRASH] Logout proceed.");
        		returnMap.setMessage("[SESSION 만료] 로그아웃됩니다.");
        	} else if ("APP AUTH FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("[Permission denied] User does not have application authority.");
        		returnMap.setMessage("[권한 오류] 접근 권한이 없습니다.");
        	} else if ("QUIESCENCE_Y".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("휴면계정 처리된 ID 입니다.");
        	} else {
        		//returnMap.setMessage("LOGIN FAIL");
        		returnMap.setMessage("로그인 실패");  
        	}
    	}
    	return returnMap;
    }
    
    /**
     * logout 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/erpAutoLoginController/actionLogout.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap actionLogout(@RequestBody CommandMap requestMap) throws Exception  {
    	
    	// get Param OBJ
    	Map param = requestMap.getCommonUserInfoMap();
    	
    	LoginVO loginVO = new LoginVO();
    	loginVO = (LoginVO)ObjectUtil.convertMapToObject(param, loginVO);
    	
    	// get HttpServletRequest
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	
    	erpAutoLoginService.actionLogout(loginVO, request);
    	
    	return requestMap;
    }
    
    /**
     * connUrl : 호출 url, paramMap : 쥬고 받는 전문, operType : 1->토큰인증, 2->토큰삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    public Map erpWebService(String connUrl, Map<String,Object> paramMap, String operType) throws Exception  {
    	Map resultMap = new HashMap<String, String>();
    	Map accessTokenMap = new HashMap<String, String>();
    	// statusCd : 1->성공, 0:실패
    	String statusCd = "0";
    	String accessToken = "";
    	String createDt = "";
		try {
	    	
	    	URL url = new URL(connUrl); // 호출할 url
	  
	        StringBuilder postData = new StringBuilder();
	        for(Map.Entry<String,Object> param : paramMap.entrySet()) {
	            if(postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	 
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        conn.setDoOutput(true);
	        conn.getOutputStream().write(postDataBytes); // POST 호출
	 
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	 
	        String inputLine;
	        while((inputLine = in.readLine()) != null) { // response 출력
		        // json 파싱
		        JSONParser jsonParser = new JSONParser();
		        JSONObject jsonObject = (JSONObject) jsonParser.parse(inputLine);
//		        System.out.println("ERP Result Data : "+jsonObject.toString());
		        // 전문 호출시만 톸큰 정보 확인
		        if("1".equals(operType)) {
			        Map returnAccKey = (Map)jsonObject.get("UserAccessToken");
			        accessToken = (String)returnAccKey.get("AccessToken");
			        createDt = (String)jsonObject.get("CreateDt");
		        	if(accessToken == null) {
		        		accessToken = "";
		        	}
		        }
		        statusCd = (String)jsonObject.get("StatusCd");
		        if(statusCd == null) {
		        	resultMap.put("statusCd", "0");
		        } else {
		        	resultMap.put("statusCd", statusCd);
			        // 전문 호출시만 톸큰 정보 확인
			        if("1".equals(operType)) {
			        	resultMap.put("accessToken", accessToken);
			        	resultMap.put("createDt", createDt);
			        }
		        }
	        }
	        in.close();
	    } catch(MalformedURLException e) { 
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch(Exception e) {
	    	e.printStackTrace();
	    } 
    	return resultMap;
    }
}