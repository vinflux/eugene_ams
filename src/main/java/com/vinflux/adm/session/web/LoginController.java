package com.vinflux.adm.session.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.session.service.LoginService;
import com.vinflux.adm.session.service.LoginVO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.util.CipherUtil;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.ObjectUtil;
import com.vinflux.framework.web.CommandController;

@Controller("loginController")
@RemotingDestination
public class LoginController extends CommandController {
	
    /** LoginService */
	@Resource(name = "loginService")
    private LoginService loginService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(LoginController.class);
    
    /**
     * login
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/loginController/actionLogin.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap actionLogin(@RequestBody CommandMap requestMap) throws Exception  {

    	CommandMap returnMap = new CommandMap();

    	// get Param OBJ
    	Map param = requestMap.getParamMap();
    	String ipAddress = (String)requestMap.getCommon("IPADDRESS");
    	String appkey = (String)requestMap.getCommon("APKEY");
    	
    	if("ADM".equals(appkey)) {
    		// 롤별 어플리케이션 권한때문에 임시 방편으로 처리함
    		// 영향도 0으로 파악됨
    		appkey = "ADMIN";
    	} else if ("ICOM".equals(appkey)) {
    		appkey = "ICOM";
    	} else if ("TMS".equals(appkey)) {
    		appkey = "TMS";
    	} else if ("WMS".equals(appkey)) {
    		appkey = "WM";
    	} else if ("VIMS".equals(appkey)) {
    		appkey = "VIMS";
    	} else if ("CMS".equals(appkey)) {
    		appkey = "CMS";    	
    	} else if ("KPI".equals(appkey)) {
    		appkey = "KPI";    	
    	} else if ("PTL".equals(appkey)) {
    		appkey = "PTL";    	
    	}
    	
    	LoginVO loginVO = new LoginVO();
    	loginVO = (LoginVO)ObjectUtil.convertMapToObject(param, loginVO);
    	loginVO.setIpAddress(ipAddress);
    	loginVO.setAppkey(appkey);
    	
    	//비밀번호 암호화.
    	String encryptPw = CipherUtil.encryptPassword(MapUtil.getStr(param, "urPw"));
    	loginVO.setUrPw(encryptPw);
    	
    	// get HttpServletRequest
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	String uniqueAddr = request.getRemoteAddr();
    	loginVO.setUniqueAddr(uniqueAddr);
    	
    	
    	Map loginMap = loginService.actionLogin(loginVO, request);
    	
    	if ( "SUCCESS".equals(loginMap.get("loginStatus")) ) {
    		returnMap.putMap("USER_INFO", loginMap);
    	} else {
    		returnMap.setMessageCode("99990");
    		returnMap.setErrCode(EnvConstant.COMMAND_ERROR_CODE_FAIL);
    		LOG.debug("###Login Fail!! Status=>"+loginMap.get("loginStatus"));
    		
        	if ("PW_FAIL".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("패스워드를 확인해 주세요"); // 메세지 한글변경요청으로 변경 2018.10.08
        		//returnMap.setMessage("Password you entered do not match.\n(Max Login fail count : " + loginMap.get("loginFailCnt") + " / " + loginMap.get("maxLoginFail") + ")");
        	} else if ("ID_FAIL".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("존재하지 않는 아이디입니다.");
        		//returnMap.setMessage("Username you entered not exists.");
        	} else if ("MAX_PW_FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("ID is expired because you entered your password incorrectly more than " + loginMap.get("maxLoginFail") +" times.");
        		returnMap.setMessage("아이디가 잠겼습니다.(비밀번호 오류 " + loginMap.get("maxLoginFail") +"회 초과)");
        	} else if ("URKEY_EXPIERD".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("ID(" + loginMap.get("urkey") + ")의 사용기간이 만료되었습니다.");
        		//returnMap.setMessage("ID(" + loginMap.get("urkey") + ") is expired.");
        	} else if ("URKEY_LOCK".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("ID(" + loginMap.get("urkey") + ") is locked.");
        		returnMap.setMessage("아이디(" + loginMap.get("urkey") + ")가 잠겼습니다.");
        	} else if ("SESSION FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("[SESSION CRASH] Logout proceed.");
        		returnMap.setMessage("[SESSION 만료] 로그아웃됩니다.");
        	} else if ("APP AUTH FAIL".equals(loginMap.get("loginStatus"))) {
        		//returnMap.setMessage("[Permission denied] User does not have application authority.");
        		returnMap.setMessage("[권한 오류] 접근 권한이 없습니다.");
        	} else if ("QUIESCENCE_Y".equals(loginMap.get("loginStatus"))) {
        		returnMap.setMessage("휴면계정 처리된 ID 입니다.");
        	} else {
        		//returnMap.setMessage("LOGIN FAIL");
        		returnMap.setMessage("로그인 실패");  
        	}
    	}
    	    	
    	LOG.debug("### returnMap END=>"+ returnMap);	
    	return returnMap;
    }
    
    /**
     * logout 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/loginController/actionLogout.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap actionLogout(@RequestBody CommandMap requestMap) throws Exception  {
    	
    	// get Param OBJ
    	Map param = requestMap.getCommonUserInfoMap();
    	
    	LoginVO loginVO = new LoginVO();
    	loginVO = (LoginVO)ObjectUtil.convertMapToObject(param, loginVO);
    	
    	// get HttpServletRequest
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	
    	loginService.actionLogout(loginVO, request);
    	
    	return requestMap;
    }

}