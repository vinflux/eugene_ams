package com.vinflux.adm.addr.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.addr.service.AddrService;
import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.persistence.AddrDAO;

@Service("addrService")
public class AddrServiceImpl implements AddrService {

	@Resource(name="addrDAO")
	private AddrDAO addrDAO;

	@Resource(name = "entityAddrService")
	private EntityAddrService entityAddrService;

	@Override
	public Map selectAddr(Map paramMap) throws Exception {
		return addrDAO.selectAddr(paramMap);
	}

	@Override
	public void insertAddr(Map paramMap) throws Exception {
		int intAddrId = entityAddrService.getAddrId();
		paramMap.put("addrid", intAddrId);
		entityAddrService.addAddressEntitymaster(paramMap);
	}

	@Override
	public void updateAddr(Map paramMap) throws Exception {
		entityAddrService.changeAddressEntitymaster(paramMap);
	}
	
	/** **/
	@Override
	public Map selectAddrInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = addrDAO.selectAddrInfoCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = addrDAO.selectAddrInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	/** **/
	@Override
	public Map selectAddrInfoDetail (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = addrDAO.selectAddrInfoDetailCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = addrDAO.selectAddrInfoDetail(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}	
	
	/** **/
	@Override
	public List selectExcelDownAddr (Map parameterMap) throws Exception {
    	return addrDAO.selectExcelDownAddr(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownAddr(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return addrDAO.selectExcelDownAddr(parameterMap, excelFormat);
	}
	
	@Override
	public void deleteAddr(Map paramMap) throws Exception {
		entityAddrService.deleteAddressEntitymaster(paramMap);
	}
	
	@Override
	public Workbook selectExcelDownAddrDetail(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return addrDAO.selectExcelDownAddrDetail(parameterMap, excelFormat);
	}	

}
