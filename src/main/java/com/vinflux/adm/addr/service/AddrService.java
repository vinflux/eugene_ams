package com.vinflux.adm.addr.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface AddrService {
	
	public Map selectAddr (Map paramMap) throws Exception;
	
	public void insertAddr (Map paramMap) throws Exception;

	public void updateAddr (Map paramMap) throws Exception;
	
	public void deleteAddr (Map paramMap) throws Exception;
	
	public Map selectAddrInfo (Map paramMap) throws Exception;
	
	public Map selectAddrInfoDetail (Map paramMap) throws Exception;
	
	public List selectExcelDownAddr (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownAddr(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	public Workbook selectExcelDownAddrDetail(Map parameterMap, List<Map> excelFormat)throws Exception;
}
