package com.vinflux.adm.addr.web;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.addr.service.AddrService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("addrController")
@RemotingDestination
public class AddrController extends CommandController {
	@Resource(name = "addrService")
	private AddrService addrService;

//	@Resource(name="admYYCommonIdGenService")
//	private TableIdGenService admYYCommonIdGenService;
 
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(AddrController.class);

	/**
	 *
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/addrController/selectAddr.do")
	@ResponseBody   
	public CommandMap selectAddr (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("selectAddr requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);

//		String shpgLocType = paramMap.get("loctype").toString();

		Map resultMap = addrService.selectAddr(paramMap);
		 
		/*
		admYYCommonIdGenService.setKeyName("TADMIN_MST_ADDR_T");
		String strAddrId = admYYCommonIdGenService.getNextStringId();
		resultMap.put("addrid", strAddrId);
		*/
		
//		if("frm".equals(shpgLocType)) {
//			resultMap.put("newfrmshpgaddrid", strAddrId);
//		} else if("to".equals(shpgLocType)) {
//			resultMap.put("newtoshpgaddrid", strAddrId);
//		}
		
		requestMap.putMap("FORM_DATA", resultMap);
		
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/addrController/saveAddr.do")
	@ResponseBody   
	public CommandMap saveAddr (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	Map param = requestMap.getParamMap();
      	
      	if ("ALL".equals(param.get("chkAll")) ) {
        	Map paramMap = requestMap.getSearchQuery();
        	paramMap.putAll(param);
        	list = addrService.selectExcelDownAddr(paramMap);
      	}
      	
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				addrService.insertAddr(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				addrService.updateAddr(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				addrService.deleteAddr(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
	/**
     * ADM > 마스터 > 주소 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/addrController/selectAddrInfo.do")
    @ResponseBody   
    public CommandMap selectAddrInfo(@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map addrCodeMap = addrService.selectAddrInfo(paramMap);
    	Integer count = MapUtil.getInt(addrCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)addrCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
	/**
     * ADM > 마스터 > 주소 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/addrController/selectAddrInfoDetail.do")
    @ResponseBody   
    public CommandMap selectAddrInfoDetail(@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map addrCodeMap = addrService.selectAddrInfoDetail(paramMap);
    	Integer count = MapUtil.getInt(addrCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)addrCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 마스터 > 주소 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/addrController/excelDownAddr.do")
    @ResponseBody   
    public void excelDownAddr (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = addrService.selectExcelDownAddr(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				addrService.selectExcelDownAddr(parameterMap, excelFormat),
				fileName, response);
    }
    
    @RequestMapping(value="/addrController/excelDownAddrDetail.do")
    @ResponseBody   
    public void excelDownAddrDetail (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
		ExcelWriteHandler.downloadExcelFile(
				addrService.selectExcelDownAddrDetail(parameterMap, excelFormat),
				fileName, response);
    }     
	
}
