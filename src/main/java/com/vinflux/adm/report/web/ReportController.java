package com.vinflux.adm.report.web;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.util.UbiUtil;
import com.vinflux.framework.web.CommandController;

@Controller("reportController")
@RemotingDestination
public class ReportController extends CommandController {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ReportController.class);
    
    
    /**
     * print
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/reportController/ubiReport.do")
    public String ubiReport(Map<String,Object> commandMap, ModelMap model) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("commandMap => " + commandMap);
    	LOG.debug("======================================");
    	
    	String udsName = (String)commandMap.get("udsName");
    	// JSON parsing 
    	String reportParam = (String)commandMap.get("reportParam");
    	LOG.debug("reportParam => " + reportParam);    	
    	
    	if ( StringUtil.isNotEmpty(reportParam) ) {
        	CommandMap requestMap  = JsonUtil.jsonCovertCommandMap(reportParam);
        	Map paramMap = requestMap.getParamMap();
        	
        	LOG.debug("requsetMap => " + requestMap);       	
        	
        	/**
        	 * exctType
        	 *· TYPE2 : Frame으로 UbiViewer를 로드시킵니다.
    		 *· TYPE3 : 미리보기없이 전체 페이지를 한 장씩 직접 프린트로 출력을 내보냅니다.
    		 *· TYPE4 : UbiViewer를 브라우저에 포함시켜 로드시킵니다.
    		 *· TYPE5 : 미리보기없이 프린트 설정 대화상자를 띄워 프린트로 출력을 내보냅니다. 
    		 * plugin 방식은 위 TYPE3,TYPE5 가 동작하지 않음.
    		 * 이에 direct print 시  TYPE4 선택 후 별도 javascript function 실행.
        	 */
        	String printType = MapUtil.getStr(paramMap, "printType");
        	String execType = null;
        	
        	if ( "direct".equals(printType) ) {	//direct print.
        		execType = "TYPE4";
        	} else {	//preview.
        		execType = "TYPE4";
        	}
        	     	
        	String appUrl = EnvConstant.APP_URL_ADM;
        	
        	String jrfDir = appUrl + EnvConstant.UBIREPORT_URL;
        	String jrfName = MapUtil.getStr(paramMap, "jrfName");    	
        	
//        	jrfName = "ubi_sample.jrf";
//        	strStream = "abcd"; 
        	
        	LOG.debug("printType => " + printType);
        	LOG.debug("jrfName => " + jrfName);
        	
        	String title = MapUtil.getStr(paramMap, "title");       	       	        
        	        	
        	//전송 파라미터로 사용 되기 때문 &연산자가 있으면 안됨.
        	reportParam = StringUtil.replace(reportParam, "&quot;", "\"");
        	        	
        	//getArg() 사용 되는 argument 생성.
        	String strArg = UbiUtil.makeUbiStrArgument(udsName, reportParam);
        	
        	model.addAttribute("title", title);
        	model.addAttribute("appUrl", appUrl);    
        	model.addAttribute("jrfDir", jrfDir);
        	model.addAttribute("jrfName", jrfName);
        	model.addAttribute("strArg", strArg);
        	model.addAttribute("execType", execType);
        	model.addAttribute("printType", printType);
    	}
    	    	
    	return "ubireport/ubiReport";
    }
    
    /**
     * print
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/reportController/dataTest.do")
    public void dataTest(Map<String,Object> commandMap, ModelMap model, HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("commandMap => " + commandMap);
    	LOG.debug("======================================");

    	String udc = (String)commandMap.get("uds_def_columns");
    	LOG.debug("udc => " + udc);
    	
    	String cnt = (String)commandMap.get("cnt");
    	if( cnt == null || cnt.equals("") ){
    		cnt = "10";
    	}

    	int icnt = Integer.parseInt(cnt);

    	StringBuffer data = new StringBuffer("col1^tcol2^tcol3^t^n");
    	for(int i = 0; i < icnt; i++) {
    		data.append("data1_").append( i ).append( "^tdata2_").append( i ).append("^tdata3_").append( i ).append("^t^n");
    	}
    	
		response.setContentType("text/html");
		String LANG_ENCODING = "UTF-8";
	    OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), LANG_ENCODING);
	    PrintWriter out = new PrintWriter (writer);
        out.println(data.toString());
	    out.close();	
    	
    }

}
