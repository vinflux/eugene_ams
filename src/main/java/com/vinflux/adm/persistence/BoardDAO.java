package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("boardDAO")
public class BoardDAO extends AdmAbstractDAO  {
	
	
	public List selectNoticeInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNoticeInfoList", parameterMap);
	}
	
	public List selectFileList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectFileList", parameterMap);
	}
	
	public List selectNoticeGrInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNoticeGrInfoList", parameterMap);
	}
	
	public List selectNoticeUrGrInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNoticeUrGrInfoList", parameterMap);
	}
	
	public List selectNoticeInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNoticeInfo", parameterMap);
	}
		
	public int selectNoticeCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.BoardDAO.selectNoticeCount", parameterMap);
	}
	
	public String selectMsgNo() {
		return selectForStr("com.vinflux.adm.persistence.BoardDAO.selectMsgNo", null);
	}
	
	public int insertNoticeInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.insertNoticeInfo", parameterMap);
	}
	
	public int insertNoticeReppleTarget(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.insertNoticeReppleTarget", parameterMap);
	}
	
	public int updateNoticeRepple(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.updateNoticeRepple", parameterMap);
	}
	public int deleteNoticeRepple(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.deleteNoticeRepple", parameterMap);
	}
		
	public int insertFileInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.insertFileInfo", parameterMap);
	}
	
	public int deleteFileInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.deleteFileInfo", parameterMap);
	}
	
	public int deleteFileAll(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.deleteFileAll", parameterMap);
	}
	
	public int insertNoticeTargetInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.insertNoticeTargetInfo", parameterMap);
	}
	
	public int updateNoticeInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.BoardDAO.updateNoticeInfo", parameterMap);
	}
	
	
	public int deleteNoticeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.BoardDAO.deleteNoticeInfo", parameterMap);
	}
	
	public int deleteNoticeTargetInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.BoardDAO.deleteNoticeTargetInfo", parameterMap);
	}
	
	public int selectDeleteNoticeCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.BoardDAO.selectDeleteNoticeCount", parameterMap);
	}
	
	
	
	public List selectNewsInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNewsInfoList", parameterMap);
	}
		
	public List selectNewsInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectNewsInfo", parameterMap);
	}
		
	public int selectNewsCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.BoardDAO.selectNewsCount", parameterMap);
	}
	
	public int insertNewsInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.BoardDAO.insertNewsInfo", parameterMap);
	}
		
	public int updateNewsInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.BoardDAO.updateNewsInfo", parameterMap);
	}
	
	
	public int deleteNewsInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.BoardDAO.deleteNewsInfo", parameterMap);
	}
	
	public int selectDeleteNewsCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.BoardDAO.selectDeleteNewsCount", parameterMap);
	}
	
	/**
	 * select excel list
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownNotice(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.BoardDAO.selectExcelDownNotice", parameterMap);
	}
	
	public Workbook selectExcelDownNotice(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.BoardDAO.selectExcelDownNotice", parameterMap, handler);
		return handler.getWorkbook();
	}
}
