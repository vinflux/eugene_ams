package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("carrierDAO")
public class CarrierDAO extends AdmAbstractDAO {

	public int selectCarrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CarrierDAO.selectCarrCount", parameterMap);
	} 
	
	public List selectCarr(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.CarrierDAO.selectCarr", parameterMap);
	}
	
	public Map selectPkCarr(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.CarrierDAO.selectPkCarr", parameterMap);
	}

	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CarrierDAO.selectExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CarrierDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/* ubi report sample code. **/
	public List selectCarrCdReport(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CarrierDAO.selectCarrCdReport", parameterMap);
	}

	public int checkValidCarrList(List<String> parameterList) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CarrierDAO.checkValidCarrList", parameterList);
	}
	
	public int approveCarr(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.CarrierDAO.approveCarr", parameterMap);
	}

}
