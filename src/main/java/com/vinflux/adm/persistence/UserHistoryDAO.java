package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("userHistoryDAO")
public class UserHistoryDAO extends AdmAbstractDAO {
	
	/**
	 * 사용자 이력 조회 코드 row count : TADMIN_INFO_URLILOH
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectUserHistoryCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UserHistoryDAO.selectUserHistoryCount", parameterMap);
	} 
	
	/**
	 * 사용자 이력 조회 코드 검색 : TADMIN_INFO_URLILOH, TADMIN_MST_UR
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectUserHistory(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserHistoryDAO.selectUserHistory", parameterMap);
	}
	
	/**
	 * 사용자 이력 조회 코드 검색 : TADMIN_INFO_URCH
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectUserHistoryDtl(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserHistoryDAO.selectUserHistoryDtl", parameterMap);
	}
	
	/**
	 * 사용자 이력 조회 코드 검색 : TADMIN_INFO_URCH
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectUserHistoryDtlCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UserHistoryDAO.selectUserHistoryDtlCount", parameterMap);
	}	
	
	/**
	 * 사용자 이력 조회 코드 검색(헤더 엑셀) : TADMIN_INFO_URLILOH, TADMIN_MST_UR
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistory(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserHistoryDAO.selectExcelDownUserHistory", parameterMap);
	}
	
	public Workbook selectExcelDownUserHistory(Map parameterMap,
			List<Map> excelFormat)throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UserHistoryDAO.selectExcelDownUserHistory", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 사용자 이력 조회 코드 검색(상세 엑셀) : TADMIN_INFO_URCH
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistoryDtl(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserHistoryDAO.selectExcelDownUserHistoryDtl", parameterMap);
	}
	
	public Workbook selectExcelDownUserHistoryDtl(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UserHistoryDAO.selectExcelDownUserHistoryDtl", parameterMap,handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 사용자 이력 조회 정보 추가 : TADMIN_INFO_URLILOH
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int insertTAdminInfoUrliloh(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.UserHistoryDAO.insertTAdminInfoUrliloh", parameterMap);
	}
	
	/**
	 * 사용자 이력 조회 정보 수정 : TADMIN_INFO_URLILOH
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int updateTAdminInfoUrliloh(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.UserHistoryDAO.updateTAdminInfoUrliloh", parameterMap);
	}

	

		
}