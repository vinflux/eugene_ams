package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("crossdockDAO")
public class CrossdockDAO extends AdmAbstractDAO {
	
	public int selectCrossdockCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CrossdockDAO.selectCrossdockCount", parameterMap);
	}
	
	public List selectCrossdock(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CrossdockDAO.selectCrossdock", parameterMap);
	}

	public int checkCrossdock(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CrossdockDAO.checkCrossdock", parameterMap);
	}	
	
	public int insertCrossdock(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.CrossdockDAO.insertCrossdock", parameterMap);
	}
	
	public int updateCrossdock(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CrossdockDAO.updateCrossdock", parameterMap);
	}	
	
	public int deleteCrossdock(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CrossdockDAO.deleteCrossdock", parameterMap);
	}
	
	public Workbook selectExcelCrossdock(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CrossdockDAO.selectExcelCrossdock", parameterMap, handler);
		return handler.getWorkbook();
	}
}