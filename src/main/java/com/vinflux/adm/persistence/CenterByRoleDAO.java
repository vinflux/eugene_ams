package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("centerByRoleDAO")
public class CenterByRoleDAO extends AdmAbstractDAO {
	/**
	 * Center 검색 : TADMIN_MST_CT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterByRoleDAO.selectTAdminMstCt", parameterMap);
	}
	
	/**
	 * Role별 Center 검색 : TADMIN_MST_CT, TADMIN_MST_ROXCTGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectCenterByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterByRoleDAO.selectCenterByRole", parameterMap);
	}
	
	/**
	 * Center 기준 정보 검색 : TADMIN_MST_CT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRoxctgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterByRoleDAO.selectTAdminMstRoxctgt", parameterMap);
	}
	
	/**
	 * Role별 Center data insert : TADMIN_MST_CT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstRoxctgt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.CenterByRoleDAO.insertTAdminMstRoxctgt", parameterMap);
	}
	
	/**
	 * Role별 Center data delete : TADMIN_MST_CT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstRoxctgt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.CenterByRoleDAO.deleteTAdminMstRoxctgt", parameterMap);
	}
}