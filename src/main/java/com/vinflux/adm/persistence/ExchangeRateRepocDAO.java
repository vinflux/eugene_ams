package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("exchangeRateRepocDAO")
public class ExchangeRateRepocDAO extends AdmAbstractDAO {

	public int selectExchangeRateRepoCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ExchangeRateRepocDAO.selectExchangeRateRepoCount", parameterMap);
	}
	
	public List selectExchangeRateRepoList(Map parameterMap){
		return selectList("com.vinflux.adm.persistence.ExchangeRateRepocDAO.selectExchangeRateRepoList", parameterMap);
	}
	
	/**
	 * ADM > 환율정보 > 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ExchangeRateRepocDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ExchangeRateRepocDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}
