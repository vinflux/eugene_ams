package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("icOrdStopDAO")
public class IcOrdStopDAO extends AdmAbstractDAO {
	
	//--------------- 센터 발주정지  ---------------------------------------------------------------------
	/**
	 * 센터 발주정지 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectIcOrdStopCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopCount", parameterMap);
	} 
	 
	/**
	 * 센터 발주정지 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStop(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStop", parameterMap);
	}

	/**
	 * 센터 발주정지 Excel Down 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopExcelDown", parameterMap);
	}
	
	public Workbook selectIcOrdStopExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

	public int updateIcOrdStop(Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.updateIcOrdStop", parameterMap);
	}
	
	public int mergeIcOrdStop(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.mergeIcOrdStop", parameterMap);
	}
	
	public int updateIcOrdStopXct(Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.updateIcOrdStopXct", parameterMap);
	}
	
	public int deleteIcOrdStop(Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.deleteIcOrdStop", parameterMap);
	}	
	
	
	/**
	 * 발주정지상품 신청 확인 SCM용
	 */
	public List selectPoStopConfirm(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectPoStopConfirm", parameterMap);
	}
	
	public List selectPoStopConfirmExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectPoStopConfirmExcelDown", parameterMap);
	}
	
	public Workbook selectPoStopConfirmExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectPoStopConfirmExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public int selectPoStopConfirmCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcOrdStopDAO.selectPoStopConfirmCount", parameterMap);
	} 
	
	public int updatePoStopConfirm(Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.updatePoStopConfirm", parameterMap);
	}
	
	
	
	
	
	
	
	
	//--------------- 거래처 발주정지 해제  ---------------------------------------------------------------------

	
	/**
	 * 센터 발주정지 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopXct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopXct", parameterMap);
	}
	
	public Workbook selectIcOrdStopXct(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopXct", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 거래처 발주정지 해제 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectIcOrdStopRemoveCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopRemoveCount", parameterMap);
	} 
	 
	/**
	 * 거래처 발주정지 해제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopRemove(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopRemove", parameterMap);
	}

	/**
	 * 거래처 발주정지 해제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopRemoveExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopRemoveExcelDown", parameterMap);
	}
	
	public Workbook selectIcOrdStopRemoveExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopRemoveExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 거래처 발주정지 해제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopXctForRemove(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopXctForRemove", parameterMap);
	}
	
	public Workbook selectIcOrdStopXctForRemove(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopXctForRemove", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public int mergeIcOrdStopRemove(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.IcOrdStopDAO.mergeIcOrdStopRemove", parameterMap);
	}
	//--------------- 점포 발주정지 대상 현황 조회  ---------------------------------------------------------------------
	
	/**
	 * 점포 발주정지 대상 현황 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectIcOrdStopStatusCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopStatusCount", parameterMap);
	} 

	/**
	 * 점포 발주정지 대상 현황 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopStatus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopStatus", parameterMap);
	}
	
	/**
	 * Excel Download
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectIcOrdStopStatusExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopStatusExcelDown", parameterMap);
	}

	public Workbook selectIcOrdStopStatusExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcOrdStopDAO.selectIcOrdStopStatusExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

}