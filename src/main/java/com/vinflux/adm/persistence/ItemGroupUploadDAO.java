package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemGroupUploadDAO")
public class ItemGroupUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 아이템 코드 임시 테이블 row count : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectCountTAdminTempIcgrUp(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectCountTAdminTempIcgrUp", parameterMap);
	} 
	 
	/**
	 * 아이템 코드 임시 테이블 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectListItemGroupUploadTabManage(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectListItemGroupUploadTabManage", parameterMap);
	}
	
	/**
	 * 아이템 코드 임시 테이블 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectListItemGroupUploadTabUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectListItemGroupUploadTabUpload", parameterMap);
	}

	
	/**
	 * 저장을 위한 업로드 내역 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectListTAdminTempIcgrUp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectListTAdminTempIcgrUp", parameterMap);
	}
	
	/**
	 * 물품 코드 insert to temp table : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminTempIcgrUp (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemGroupUploadDAO.insertTAdminTempIcgrUp", parameterMap);
	}
	
	/**
	 * 물품 코드 저장 전,후 I/F status 변경 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTAdminTempIcgrUp (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.ItemGroupUploadDAO.updateTAdminTempIcgrUp", parameterMap);
	}
	
	/**
	 * 물품 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.ItemGroupUploadDAO.insertTAdminInfoUlhs", parameterMap);
	}

	/**
	 * 물픔 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return Lisst
	 * @throws Exception
	 */
	public List selectExcelItemGroupUploadTabManage(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectExcelItemGroupUploadTabManage",parameterMap);
	}

	public Workbook selectExcelItemGroupUploadTabManage(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemGroupUploadDAO.selectExcelItemGroupUploadTabManage",parameterMap, handler);
		return handler.getWorkbook();
	}
}