package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("ERPStmntAcDAO")
public class ERPStmntAcDAO extends AdmAbstractDAO {

	public List selectERPStmntAcList(Map parameterMap){
		return selectList("com.vinflux.adm.persistence.ERPStmntAcDAO.selectERPStmntAcList", parameterMap);
	}
	
	public int selectERPStmntAcCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ERPStmntAcDAO.selectERPStmntAcCount", parameterMap);
	}
	
	public List selectExcelERPStmntAc(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ERPStmntAcDAO.selectExcelERPStmntAc", parameterMap);
	}
	
	public Workbook selectExcelERPStmntAc(Map paramMap, List<Map> excelFormat) {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ERPStmntAcDAO.selectExcelERPStmntAc", paramMap, handler);
		return handler.getWorkbook();
	}
	
	public List selectERPStmntAcDt(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ERPStmntAcDAO.selectERPStmntAcDt", parameterMap);
	}
	
}
