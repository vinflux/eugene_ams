package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;


import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("ownerByRoleDAO")
public class OwnerByRoleDAO extends AdmAbstractDAO {
	/**
	 * 화주 검색 : TADMIN_MST_OW
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.OwnerByRoleDAO.selectTAdminMstOw", parameterMap);
	}
	
	/**
	 * Role별 화주 검색 : TADMIN_MST_OW, TADMIN_MST_ROXOWGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectOwnerByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.OwnerByRoleDAO.selectOwnerByRole", parameterMap);
	}
	
	/**
	 * Role별 화주 기준정보 검색 : TADMIN_MST_ROXOWGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRoxowgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.OwnerByRoleDAO.selectTAdminMstRoxowgt", parameterMap);
	}
	
	/**
	 * Role별 화주 data insert : TADMIN_MST_ROXOWGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstRoxowgt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.OwnerByRoleDAO.insertTAdminMstRoxowgt", parameterMap);
	}
	
	/**
	 * Role별 화주 data delete : TADMIN_MST_ROXOWGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstRoxowgt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.OwnerByRoleDAO.deleteTAdminMstRoxowgt", parameterMap);
	}
}