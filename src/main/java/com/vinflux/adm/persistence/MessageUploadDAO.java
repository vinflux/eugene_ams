package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("messageUploadDAO")
public class MessageUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 메시지 임시 테이블 row count : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectMessageUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageUploadDAO.selectMessageUploadCount", parameterMap);
	} 
	 
	/**
	 * 메시지 임시 테이블 조회 : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMessageUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageUploadDAO.selectMessageUpload", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectMessageTemp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageUploadDAO.selectMessageTemp", parameterMap);
	}
	
	/**
	 * 저장을 위한 업로드 내역 조회 : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectMessageTempAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageUploadDAO.selectMessageTempAll", parameterMap);
	}
	
	/**
	 * 메시지 insert to temp table : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempMsgup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.MessageUploadDAO.insertTadminTempMsgup", parameterMap);
	}
	
	/**
	 * 메시지 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.MessageUploadDAO.insertTadminInfoUlhs", parameterMap);
	}
	
	/**
	 * 메시지 업로드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_MULAAPMSG_UP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownMessageUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageUploadDAO.selectExcelDownMessageUpload",parameterMap);
	}

	public Workbook selectExcelDownMessageUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MessageUploadDAO.selectExcelDownMessageUpload",parameterMap, handler);
		return handler.getWorkbook();
	}
	
}