package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("addrDAO")
public class AddrDAO extends AdmAbstractDAO {

	public Map selectAddr(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.AddrDAO.selectAddr", paramMap);
	}

	public int selectAddrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AddrDAO.selectAddrCount", parameterMap);
	} 
	 
	public List selectAddrList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AddrDAO.selectAddrList", parameterMap);
	}
	
	public int selectAddrInfoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AddrDAO.selectAddrInfoCount", parameterMap);
	} 
	
	public List selectAddrInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AddrDAO.selectAddrInfo", parameterMap);
	}
	
	public int selectAddrInfoDetailCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AddrDAO.selectAddrInfoDetailCount", parameterMap);
	} 
	
	public List selectAddrInfoDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AddrDAO.selectAddrInfoDetail", parameterMap);
	}	
	
	public List selectExcelDownAddr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AddrDAO.selectExcelDownAddr", parameterMap);
	}
	
	public List selectExcelDownAddrDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AddrDAO.selectExcelDownAddrDetail", parameterMap);
	}

	public Workbook selectExcelDownAddr(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AddrDAO.selectExcelDownAddr", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public Workbook selectExcelDownAddrDetail(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AddrDAO.selectExcelDownAddrDetail", parameterMap, handler);
		return handler.getWorkbook();
	}	
}