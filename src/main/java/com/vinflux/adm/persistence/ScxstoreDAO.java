package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("scxstoreDAO")
public class ScxstoreDAO extends AdmAbstractDAO {
	
	public int selectScxstoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ScxstoreDAO.selectScxstoreCount", parameterMap);
	}
	
	public int checkScxstoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ScxstoreDAO.checkScxstoreCount", parameterMap);
	}
	 
	public List selectScxstore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ScxstoreDAO.selectScxstore", parameterMap);
	}
	
	public List selectPkScxstore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ScxstoreDAO.selectPkScxstore", parameterMap);
	}
	
	public int insertTAdminMstScxstore(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.ScxstoreDAO.insertTAdminMstScxstore", parameterMap);
	}

	public int updateTAdminMstScxstore(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.ScxstoreDAO.updateTAdminMstScxstore", parameterMap);
	}
	
	public int deleteTAdminMstScxstore(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.ScxstoreDAO.deleteTAdminMstScxstore", parameterMap);
	}	

	public List selectExcelDownScxstore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ScxstoreDAO.selectExcelDownScxstore", parameterMap);
	}
	
	public Workbook selectExcelDownScxstore(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ScxstoreDAO.selectExcelDownScxstore", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select total Store Key. count     
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int searchStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ScxstoreDAO.searchStoreCount", parameterMap);
	} 
	
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List searchStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ScxstoreDAO.searchStore", parameterMap);
	}
	
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}

	
}