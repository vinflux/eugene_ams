package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("messageDAO")
public class MessageDAO extends AdmAbstractDAO {
	
	/** **/
	public int selectMessageCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectMessageCount", parameterMap);
	} 
	 
	/** **/
	public List selectMessage(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageDAO.selectMessage", parameterMap);
	}

	/** **/
	public Map selectPkMessage(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.MessageDAO.selectPkMessage", parameterMap);
	}
	
	/** **/
	public List selectMessageByApKeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageDAO.selectMessageByApKeyList", parameterMap);
	}

	public Map selectTAdminMstMulaapmsgHd(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.MessageDAO.selectTAdminMstMulaapmsgHd", parameterMap);
	}

	/** **/
	public int insertTAdminMstMulaapmsgHd (Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTAdminMstMulaapmsgHd", parameterMap);
	}

	public int selectTAdminMstMulaapmsgDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectTAdminMstMulaapmsgDtCount", parameterMap);
	}
	
	/** **/
	public int insertTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTAdminMstMulaapmsgDt", parameterMap);
	}
	
	/** **/
	public int updateTAdminMstMulaapmsgHd(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTAdminMstMulaapmsgHd", parameterMap);
	}

	/** **/
	public int updateTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTAdminMstMulaapmsgDt", parameterMap);
	}

	/** **/
	public int deleteTAdminMstMulaapmsgHd(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTAdminMstMulaapmsgHd", parameterMap);
	}

	/** **/
	public int deleteTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTAdminMstMulaapmsgDt", parameterMap);
	}
	
	/** **/
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageDAO.selectTAdminMstCt",parameterMap);
	}
	
	/** **/
	public List selectTAdminMsgMulappmsgxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageDAO.selectTAdminMsgMulappmsgxct",parameterMap);
	}
	
	/** **/
	public int insertTAdminMsgMulappmsgxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTAdminMsgMulappmsgxct",parameterMap);
	}

	/** **/
	public int deleteTAdminMsgMulappmsgxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTAdminMsgMulappmsgxct",parameterMap);
	}

	/** **/
	public List selectExcelDownMessage(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MessageDAO.selectExcelDownMessage", parameterMap);
	}

	public Workbook selectExcelDownMessage(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat); 
		getSqlSession().select("com.vinflux.adm.persistence.MessageDAO.selectExcelDownMessage", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/** **/
	public int updateTadminTempMulaapmsgUp(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTadminTempMulaapmsgUp", parameterMap);
	}	
	
	
	public int insertTMdmMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTMdmMstMulaapmsgDt", parameterMap);
	}
	
	public int insertTCmsMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTCmsMstMulaapmsgDt", parameterMap);
	}
	
	public int insertTFisMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTFisMstMulaapmsgDt", parameterMap);
	}
	
	public int insertTIbsMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.MessageDAO.insertTIbsMstMulaapmsgDt", parameterMap);
	}
	
	
	
	public int selectCountTMdmMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectCountTMdmMstMulaapmsgDt",parameterMap);
	}

	public int updateTMdmMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTMdmMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTMdmMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTMdmMstMulaapmsgDt",parameterMap);
	}
	
	public int selectCountTCmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectCountTCmsMstMulaapmsgDt",parameterMap);
	}

	public int updateTCmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTCmsMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTCmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTCmsMstMulaapmsgDt",parameterMap);
	}
	
	public int selectCountTFisMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectCountTFisMstMulaapmsgDt",parameterMap);
	}

	public int updateTFisMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTFisMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTFisMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTFisMstMulaapmsgDt",parameterMap);
	}
	
	public int selectCountTIbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MessageDAO.selectCountTIbsMstMulaapmsgDt",parameterMap);
	}

	public int updateTIbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MessageDAO.updateTIbsMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTIbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MessageDAO.deleteTIbsMstMulaapmsgDt",parameterMap);
	}
	
}