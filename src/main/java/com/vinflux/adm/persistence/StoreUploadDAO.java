package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("storeUploadDAO")
public class StoreUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 출고처 코드 임시 테이블 row count : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectStoretUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreUploadDAO.selectStoretUploadCount", parameterMap);
	} 
	 
	/**
	 * 출고처 코드 임시 테이블 조회 : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectStoreUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreUploadDAO.selectStoreUpload", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_STOREUP with paging
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectStoreTemp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreUploadDAO.selectStoreTemp", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectStoreTempAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreUploadDAO.selectStoreTempAll", parameterMap);
	}
	
	/**
	 * 출고처 코드 PK 조회 : TADMIN_MST_AC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstStore(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreUploadDAO.selectPkCntTadminMstStore", parameterMap);
	}
	
	/**
	 * 출고처 코드 PK 조회 : TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstStorexCt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreUploadDAO.selectPkCntTadminMstStorexCt", parameterMap);
	}
	
	/**
	 * 출고처 코드 insert to temp table : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempStoreup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.StoreUploadDAO.insertTadminTempStoreup", parameterMap);
	}
	
	/**
	 * 출고처 코드 저장 전,후 I/F status 변경 : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminTempStoreup (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.StoreUploadDAO.updateTadminTempStoreup", parameterMap);
	}
	
	/**
	 * 출고처 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.StoreUploadDAO.insertTadminInfoUlhs", parameterMap);
	}

	/**
	 * 출고처 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_STOREUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownStoreUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreUploadDAO.selectExcelDownStoreUpload",parameterMap);
	}

	public Workbook selectExcelDownStoreUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.StoreUploadDAO.selectExcelDownStoreUpload",parameterMap, handler);
		return handler.getWorkbook();
	}
}