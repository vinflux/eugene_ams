package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("equipmentDAO")
public class EquipmentDAO extends AdmAbstractDAO {
	
	@Resource(name="eqKeyIdGenService")
	private IdGenService eqKeyIdGenService;
	
	
	

	/**
	 * SELECT COUNT TADMIN_MST_EQ 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstEqCnt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.EquipmentDAO.selectTAdminMstEqCnt", parameterMap);
	} 
	 
	/**
	 * SELECT LIST TADMIN_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipment(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EquipmentDAO.selectEquipment", parameterMap);
	}
	
	/**
	 * SELECT PK TADMIN_MST_EQ
	 * @param parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectPkTAdminMstEq(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.EquipmentDAO.selectPkTAdminMstEq", parameterMap);
	}
	
	/**
	 * SELECT TADMIN_MST_CT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EquipmentDAO.selectTAdminMstCt",parameterMap);
	}
	
	/**
	 * SELECT TADMIN_MST_EQXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstEqxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EquipmentDAO.selectTAdminMstEqxct",parameterMap);
	}

	/**
	 * SELECT TADMIN_MST_EQXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectPkCntTadminMstEqxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.EquipmentDAO.selectPkCntTadminMstEqxct", parameterMap);
	} 
	
	/**
	 * INSERT TADMIN_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertEquipment (Map parameterMap) throws Exception {	
		
    	//키 발급
		String eqKey = eqKeyIdGenService.getNextStringId();	
    	
		parameterMap.put("eqkey", eqKey);
		
		return insert("com.vinflux.adm.persistence.EquipmentDAO.insertEquipment", parameterMap);
	}
	
	/**
	 * INSERT TADMIN_MST_EQXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstEqxct (Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.EquipmentDAO.insertTAdminMstEqxct", parameterMap);
	}
	
	/**
	 * UPDATE TADMIN_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateEquipment(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.EquipmentDAO.updateEquipment", parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteEquipment(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.EquipmentDAO.deleteEquipment", parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_EQXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstEqxct(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.EquipmentDAO.deleteTAdminMstEqxct", parameterMap);
	}

	/**
	 * SELECT TADMIN_MST_EQ EXCEL
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipmentExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EquipmentDAO.selectEquipmentExcel", parameterMap);
	}

	public Workbook selectEquipmentExcel(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.EquipmentDAO.selectEquipmentExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}