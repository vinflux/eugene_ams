package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;


@Repository("systemConfigSetupDAO")
public class SystemConfigSetupDAO extends AdmAbstractDAO {
	
	/**
	 * 관리자 환경 설정 코드 row count : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectSystemConfigSetupCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SystemConfigSetupDAO.selectSystemConfigSetupCount", parameterMap);
	} 
	
	/**
	 * 관리자 환경 설정 코드 검색 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectSystemConfigSetup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SystemConfigSetupDAO.selectSystemConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 pk 검색 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map selectPkSystemConfigSetup(Map parameterMap) throws Exception {
		return (Map)selectByPk("com.vinflux.adm.persistence.SystemConfigSetupDAO.selectPkSystemConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 추가 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int insertSystemConfigSetup(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.SystemConfigSetupDAO.insertSystemConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 수정 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int updateSystemConfigSetup(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.SystemConfigSetupDAO.updateSystemConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 삭제 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int deleteSystemConfigSetup(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.SystemConfigSetupDAO.deleteSystemConfigSetup", parameterMap);
	}	
	
	/**
	 * 관리자 환경 설정 코드 검색(엑셀) : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownSystemConfigSetup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SystemConfigSetupDAO.selectExcelDownSystemConfigSetup", parameterMap);
	}

	public Workbook selectExcelDownSystemConfigSetup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.SystemConfigSetupDAO.selectExcelDownSystemConfigSetup", parameterMap, handler);
		return handler.getWorkbook();
	}
}