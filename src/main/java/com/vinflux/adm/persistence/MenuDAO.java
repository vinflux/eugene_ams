package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("menuDAO")
public class MenuDAO extends AdmAbstractDAO {
	
	@Resource(name="mstMeIdGenService")
	private IdGenService mstMeIdGenService;
	
	
	public int selectMenuCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MenuDAO.selectMenuCount", parameterMap);
	} 
	 
	public List selectMenu(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuDAO.selectMenu", parameterMap);
	} 

	public List selectTAdminMstMe(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuDAO.selectTAdminMstMe", parameterMap);
	}
	
	public int insertTAdminMstMe(Map parameterMap) throws Exception {
		String mekey = mstMeIdGenService.getNextStringId();	
		parameterMap.put("mekey", mekey);
		
		return insert("com.vinflux.adm.persistence.MenuDAO.insertTAdminMstMe", parameterMap);
	}

	public int updateTAdminMstMe(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MenuDAO.updateTAdminMstMe", parameterMap);
	}

	public int deleteTAdminMstMe(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MenuDAO.deleteTAdminMstMe", parameterMap);
	}
	
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuDAO.selectExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat); 
		getSqlSession().select("com.vinflux.adm.persistence.MenuDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

}