package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("myGridHeaderFormatDAO")
public class MyGridHeaderFormatDAO extends AdmAbstractDAO {
	
	
	/**
	 * ADMIN adm 그리드 개인화 select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUrdfus (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.adm.persistence.MyGridHeaderFormatDAO.selectTAdminMstUrdfus",parameterMap);
	}
	
	
	
	/**
	 * ADMIN adm 그리드 개인화 insert
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstUrdfus (Map paramMap) throws Exception { 
		
		return insert("com.vinflux.adm.persistence.MyGridHeaderFormatDAO.insertTAdminMstUrdfus",paramMap);
	}

	
	
	/**
	 * ADMIN adm 그리드 개인화 delete
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstUrdfus (Map parameterMap) throws Exception { 
		return delete("com.vinflux.adm.persistence.MyGridHeaderFormatDAO.deleteTAdminMstUrdfus",parameterMap);
	}
	
	
	
}