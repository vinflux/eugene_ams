package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;

@Repository("pushDAO")
public class PushDAO extends AdmAbstractDAO {
	
	@Resource(name="mstPushHdIdGenService")
	private IdGenService mstPushHdIdGenService;
	
	@Resource(name="mstPushDtIdGenService")
	private IdGenService mstPushDtIdGenService;
	
	/**
	 * 푸시 해더 카운트 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectPushInfoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PushDAO.selectPushInfoCount", parameterMap);
	} 
	 
	/**
	 * 푸시 해더 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPushInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PushDAO.selectPushInfo", parameterMap);
	}

	/**
	 * 푸시 디테일 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectDetailPushInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PushDAO.selectDetailPushInfo", parameterMap);
	}
	
	/**
	 * 푸시 삭제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void deletePush(Map parameterMap) throws Exception {
		delete("com.vinflux.adm.persistence.PushDAO.deletePush", parameterMap);
	}
	
	/**
	 * 푸시 데이터체크
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkPush(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PushDAO.checkPush", parameterMap);
	}
	
	/**
	 * 푸시 디테일 삭제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void deletePushDtl(Map parameterMap) throws Exception {
		delete("com.vinflux.adm.persistence.PushDAO.deletePushDtl", parameterMap);
	}
	
	/**
	 * 푸시 등록
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertPushInfo(Map parameterMap) throws Exception {    	    
		String pushhdkey = mstPushHdIdGenService.getNextStringId();	
		parameterMap.put("pushhdkey", pushhdkey);
		return insert("com.vinflux.adm.persistence.PushDAO.insertPushInfo", parameterMap);
	}
	
	/**
	 * 푸시 수정
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updatePushInfo(Map parameterMap) throws Exception {    	    
		return update("com.vinflux.adm.persistence.PushDAO.updatePushInfo", parameterMap);
	}
	
	/**
	 * 푸시 사용자그룹조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectUserListByUserGroup(Map parameterMap) throws Exception {
		return this.selectList("com.vinflux.adm.persistence.PushDAO.selectUserListByUserGroup", parameterMap);
	}
	
	/**
	 * 푸시 사용자조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public String selectUser(Map parameterMap) {
		return this.selectForStr("com.vinflux.adm.persistence.PushDAO.selectUser", parameterMap);
	}
	
	/**
	 * 푸시 발송등록
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertPush(Map parameterMap) throws Exception {
		String pushdtkey = mstPushDtIdGenService.getNextStringId();
		parameterMap.put("pushdtkey", pushdtkey);
		return this.insert("com.vinflux.adm.persistence.PushDAO.insertPush", parameterMap);
	}
	
	/**
	 * 푸시 재발송
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updatePushStatus(Map parameterMap) throws Exception {
		return this.update("com.vinflux.adm.persistence.PushDAO.updatePushStatus", parameterMap);
	}

	/**
	 * 푸시 발송 후 헤더 발송상태변경
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updatePushHd(Map parameterMap) throws Exception {
		return this.update("com.vinflux.adm.persistence.PushDAO.updatePushHd", parameterMap);
	}

	
	

}