package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("accexdayDAO")
public class AccexdayDAO extends AdmAbstractDAO {
	
	public int selectAccexdayCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayCount", parameterMap);
	}
	
	public int selectAccexdayIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayIcCount", parameterMap);
	}
	
	public List selectAccexday(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccexdayDAO.selectAccexday", parameterMap);
	}
	
	public List selectAccexdayIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayIc", parameterMap);
	}
	
	public int selectAccexdayCtegoryCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayCtegoryCount", parameterMap);
	}
	
	public int insertAccexday(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.AccexdayDAO.insertAccexday", parameterMap);
	}
	public int updateAccexday(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AccexdayDAO.updateAccexday", parameterMap);
	}
	public int deleteAccexday(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AccexdayDAO.deleteAccexday", parameterMap);
	}
	
	public int selectAccexdayCtgCount(Map parameterMap) throws Exception {		    
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayCtgCount", parameterMap);
	}
	
	public int updateAccexdayCtg(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AccexdayDAO.updateAccexdayCtg", parameterMap);
	}
	public int updateAccexdayIc(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AccexdayDAO.updateAccexdayIc", parameterMap);
	}

	public Workbook selectExcelAccexdayCtg(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AccexdayDAO.selectExcelAccexdayCtg", parameterMap, handler);
		return handler.getWorkbook();
	}
	public Workbook selectExcelAccexdayIc(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AccexdayDAO.selectExcelAccexdayIc", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public int selectAccexdayPopCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayPopCount", parameterMap);
	}
	
	public List selectAccexdayPop(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayPop", parameterMap);
	}
	
	public int selectAccexdayIcPopCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayIcPopCount", parameterMap);
	}
	
	public List selectAccexdayIcPop(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccexdayDAO.selectAccexdayIcPop", parameterMap);
	}
}