package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("ptlUserDAO")
public class PtlUserDAO extends AdmAbstractDAO   {

 
	
	/**
	 * ADM > 사용자 > 포탈사용자관리  
	 * @param parameterMap
	 * @return
	 */
	public List selectPtlUserInfoList(Map parameterMap) {
		return selectList( "com.vinflux.ptl.persistence.PtlUserDAO.selectPtlUserInfoList", parameterMap);
	}
	 
 
	
	/**
	 * ADM > 사용자 > ADMIN 사용자 수정 - 휴면해제.
	 * @param parameterMap
	 * @return
	 */
	public int updateAdminMstUr(Map parameterMap) {
		return update("com.vinflux.ptl.persistence.PtlUserDAO.updateAdminMstUr", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 포탈사용자 승인, 휴면 처리. 
	 * @param parameterMap
	 * @return
	 */
	public int updatePtlUserInfo(Map parameterMap) {
		return update("com.vinflux.ptl.persistence.PtlUserDAO.updatePtlUserInfo", parameterMap);
	}
 
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.ptl.persistence.PtlUserDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
}