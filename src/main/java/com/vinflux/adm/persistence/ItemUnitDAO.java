package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemUnitDAO")
public class ItemUnitDAO extends AdmAbstractDAO {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectItemUnitCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemUnitDAO.selectItemUnitCount", parameterMap);
	} 

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemUnit(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectItemUnit", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemUnitType(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectItemUnitType", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectTAdminMstIcut", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcut(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.ItemUnitDAO.insertTAdminMstIcut", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcutxtype ( Map parameterMap) throws Exception { 
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectTAdminMstIcutxtype", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcutxtype(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.ItemUnitDAO.insertTAdminMstIcutxtype", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstIcutxtype(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.ItemUnitDAO.updateTAdminMstIcutxtype", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcutxtypexctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemUnitDAO.selectTAdminMstIcutxtypexctCount", parameterMap);
	} 
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstIcut(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.ItemUnitDAO.updateTAdminMstIcut", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstMulaapmsgHd(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.ItemUnitDAO.updateTAdminMstMulaapmsgHd", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.ItemUnitDAO.updateTAdminMstMulaapmsgDt", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectTAdminMstCt",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcutxtypexct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectTAdminMstIcutxtypexct",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstIcutxtype(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemUnitDAO.deleteTAdminMstIcutxtype",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcutxtypexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemUnitDAO.insertTAdminMstIcutxtypexct",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstIcutxtypexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemUnitDAO.deleteTAdminMstIcutxtypexct",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstIcut(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemUnitDAO.deleteTAdminMstIcut",parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownItemUnit(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemUnitDAO.selectExcelDownItemUnit", parameterMap);
	}

	public Workbook selectExcelDownItemUnit(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemUnitDAO.selectExcelDownItemUnit", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}