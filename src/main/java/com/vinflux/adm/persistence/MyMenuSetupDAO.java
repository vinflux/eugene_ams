package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("myMenuSetupDAO")
public class MyMenuSetupDAO extends AdmAbstractDAO {
	/**
	 * Role별 메뉴 정보검색(for tree) : TADMIN_MST_ME, TADMIN_MST_URSC, TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMyMenuNexa(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.MyMenuSetupDAO.selectMyMenuNexa", paramMap);
    }
	
	/**
	 * Role별 메뉴 정보검색(for tree) : TADMIN_MST_ME, TADMIN_MST_URSC, TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMyMenu(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.MyMenuSetupDAO.selectMyMenu", paramMap);
    }
	
	/**
	 * TWORK_MST_URDFME
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMyMenuGrouping(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.MyMenuSetupDAO.selectMyMenuGrouping", paramMap);
    }
	
	public List selectMyMenuGroupingWithLevel(Map paramMap) {
		return selectList("com.vinflux.adm.persistence.MyMenuSetupDAO.selectMyMenuGroupingWithLevel", paramMap);
	}
	
	public List selectMyMenuGroupingLevel(Map paramMap) {
		return selectList("com.vinflux.adm.persistence.MyMenuSetupDAO.selectMyMenuGroupingLevel", paramMap);
	}
	
	/**
	 * Role별 메뉴 정보추가 : TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTworkMstUrdfme(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.MyMenuSetupDAO.insertTworkMstUrdfme", parameterMap);
	}
	
	
	/**
	 * Role별 메뉴 정보삭제 : TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTworkMstUrdfme(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MyMenuSetupDAO.deleteTworkMstUrdfme", parameterMap);
	}
}