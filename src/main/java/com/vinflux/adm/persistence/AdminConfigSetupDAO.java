package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("adminConfigSetupDAO")
public class AdminConfigSetupDAO extends AdmAbstractDAO {
	
	/**
	 * 관리자 환경 설정 코드 row count : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectAdminConfigSetupCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectAdminConfigSetupCount", parameterMap);
	} 
	
	/**
	 * 관리자 환경 설정 코드 검색 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectAdminConfigSetup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectAdminConfigSetup", parameterMap);
	}
	

	/**
	 * 관리자 환경 설정 코드 검색 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectAllAdminConfigSetup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectAllAdminConfigSetup", parameterMap);
	}
	
	
	
	/**
	 * 관리자 환경 설정 코드 pk 검색 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map selectPkAdminConfigSetup(Map parameterMap) throws Exception {
		return (Map)selectByPk("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectPkAdminConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 추가 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int insertAdminConfigSetup(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.AdminConfigSetupDAO.insertAdminConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 수정 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int updateAdminConfigSetup(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AdminConfigSetupDAO.updateAdminConfigSetup", parameterMap);
	}
	
	/**
	 * 관리자 환경 설정 코드 삭제 : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int deleteAdminConfigSetup(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.AdminConfigSetupDAO.deleteAdminConfigSetup", parameterMap);
	}	
	
	/**
	 * 관리자 환경 설정 코드 검색(엑셀) : TADMIN_SET_ADCS
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownAdminConfigSetup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectExcelDownAdminConfigSetup", parameterMap);
	}

	public Workbook selectExcelDownAdminConfigSetup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AdminConfigSetupDAO.selectExcelDownAdminConfigSetup", parameterMap, handler);
		return handler.getWorkbook();
	}
}