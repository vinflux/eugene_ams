package com.vinflux.adm.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("commonAddrEntityDAO")
public class CommonAddrEntityDAO extends AdmAbstractDAO {

	public int addAddressEntitymaster(Map paramMap) throws Exception {
		return insert("com.vinflux.adm.persistence.CommonAddrEntityDAO.addAddressEntitymaster", paramMap);
	}
	
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.CommonAddrEntityDAO.queryAddressEntitymaster", paramMap);
	}
	
	public int changeAddressEntitymaster(Map paramMap) throws Exception {
		return update("com.vinflux.adm.persistence.CommonAddrEntityDAO.changeAddressEntitymaster", paramMap);
	}
	
	public int deleteAddressEntitymaster(Map paramMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.CommonAddrEntityDAO.deleteAddressEntitymaster", paramMap);
	}
	
	public int getAddrId() throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonAddrEntityDAO.getAddrId", null);
	}

}
