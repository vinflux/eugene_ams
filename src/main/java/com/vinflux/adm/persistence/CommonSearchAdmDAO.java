/**
 *
 */
package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

/**
 * @author goldbug
 *
 */
@Repository("commonSearchAdmDAO")
public class CommonSearchAdmDAO extends AdmAbstractDAO {

	/**
	 * Search Type 20 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtMstCtCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchAdmDAO.selectTtMstCtCount", paramMap);
	}

	/**
	 * Search Type 20 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtMstCtList(Map paramMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.PopupSearchAdmDAO.selectTtMstCtList", paramMap);
	}

	/**
	 * Search Type 40 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTAdminMstUrCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.PopupSearchAdmDAO.selectTAdminMstUrCount", paramMap);
	}

	/**
	 * Search Type 40 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTAdminMstUrList(Map paramMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.PopupSearchAdmDAO.selectTAdminMstUrList", paramMap);
	}


}
