package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("uploadColumnManagementDAO")
public class UploadColumnManagementDAO extends AdmAbstractDAO {

	@Resource(name="updfHdKeyIdGenService")
	private IdGenService updfHdKeyIdGenService;
	
	@Resource(name="updfDtKeyIdGenService")
	private IdGenService updfDtKeyIdGenService;	
	
	
	public int selectUploadColumnHeaderInfoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHeaderInfoCount", parameterMap);
	} 
	 
	public List selectUploadColumnHeaderInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHeaderInfo", parameterMap);
	}

	public List selectUploadColumnDetailInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnDetailInfo", parameterMap);
	}
	
	public List selectUploadColumnHDExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHDExcel", parameterMap);
	}

	public int selectUploadColumnHDForInsertDuplicateCheck(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHDForInsertDuplicateCheck", parameterMap);
	}
	
	public int insertUploadColunmHD(Map parameterMap) throws Exception {		    	
    	//키 발급
		String updfHdKey = updfHdKeyIdGenService.getNextStringId();	
    	
		parameterMap.put("updf_hdkey", updfHdKey);
		return insert("com.vinflux.adm.persistence.UploadColumnManagementDAO.insertUploadColunmHD", parameterMap);
	}

	public int selectUploadColumnHDForUpdateDuplicateCheck(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHDForUpdateDuplicateCheck", parameterMap);
	}	
	
	public int updateUploadColunmHD(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.UploadColumnManagementDAO.updateUploadColunmHD", parameterMap);
	}
	
	
	public int deleteUploadColunmHD(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.UploadColumnManagementDAO.deleteUploadColunmHD", parameterMap);
	}	

	public int insertUploadColunmDT(Map parameterMap) throws Exception {		    	
    	//마스터 헤더키
    	String updfHdkey = (String) parameterMap.get("updf_hdkey");
    	//마스터 디테일키
		String updfDtKey = updfDtKeyIdGenService.getNextStringIdByKey(updfHdkey);
    	
		parameterMap.put("updf_dtkey", updfDtKey); 
		return insert("com.vinflux.adm.persistence.UploadColumnManagementDAO.insertUploadColunmDT", parameterMap);
	}

	public int updateUploadColunmDT(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.UploadColumnManagementDAO.updateUploadColunmDT", parameterMap);
	}	
	
	public int deleteUploadColunmDT(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.UploadColumnManagementDAO.deleteUploadColunmDT", parameterMap);
	}
	
	public Workbook selectUploadColumnHDExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UploadColumnManagementDAO.selectUploadColumnHDExcel", parameterMap, handler);
		return handler.getWorkbook();
	}	
	
	public Workbook excelDownSearchConditionDetail(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UploadColumnManagementDAO.excelDownSearchConditionDetail", parameterMap, handler);
		return handler.getWorkbook();
	}	

}