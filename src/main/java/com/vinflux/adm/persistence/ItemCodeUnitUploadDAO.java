package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemCodeUnitUploadDAO")
public class ItemCodeUnitUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 아이템 코드 임시 테이블 row count : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectCountTadminTempIcutup(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectCountTadminTempIcutup", parameterMap);
	} 
	 
	/**
	 * 아이템 코드 임시 테이블 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectManageList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectManageList", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectUploadList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectUploadList", parameterMap);
	}
	
	/**
	 * 저장을 위한 업로드 내역 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectListTadminTempIcutup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectListTadminTempIcutup", parameterMap);
	}
	
	/**
	 * 물품 코드 insert to temp table : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempIcutup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.insertTadminTempIcutup", parameterMap);
	}
	
	/**
	 * 물품 코드 저장 전,후 I/F status 변경 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminTempIcutup (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.updateTadminTempIcutup", parameterMap);
	}
	
	/**
	 * 물품 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.insertTadminInfoUlhs", parameterMap);
	}

	/**
	 * 물픔 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_ICUTUP
	 * @param parameterMap
	 * @return Lisst
	 * @throws Exception
	 */
	public List selectManageListExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectManageListExcel",parameterMap);
	}

	public Workbook selectManageListExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectManageListExcel",parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public int selectItemIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeUnitUploadDAO.selectItemIcutCount", parameterMap);
	} 
}