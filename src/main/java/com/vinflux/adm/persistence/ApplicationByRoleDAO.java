package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("applicationByRoleDAO")
public class ApplicationByRoleDAO extends AdmAbstractDAO {
	/**
	 * Application 검색 : TADMIN_MST_AP
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstAp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ApplicationByRoleDAO.selectTAdminMstAp", parameterMap);
	}
	
	/**
	 * Role별 Application 검색 : TADMIN_MST_ROXAPGT, TADMIN_MST_AP
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectApplicationByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ApplicationByRoleDAO.selectApplicationByRole", parameterMap);
	}
	
	/**
	 * Role별 Application 기준정보 검색 : TADMIN_MST_ROXAPGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRoxapgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ApplicationByRoleDAO.selectTAdminMstRoxapgt", parameterMap);
	}
	
	/**
	 * Role별 Application data insert : TADMIN_MST_ROXAPGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public int insertTAdminMstRoxapgt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ApplicationByRoleDAO.insertTAdminMstRoxapgt", parameterMap);
	}
	
	/**
	 * Role별 Application data delete : TADMIN_MST_ROXAPGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	*/
	public int deleteTAdminMstRoxapgt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ApplicationByRoleDAO.deleteTAdminMstRoxapgt", parameterMap);
	}
}