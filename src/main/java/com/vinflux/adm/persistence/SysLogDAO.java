package com.vinflux.adm.persistence;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.common.SysLogVO;
import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("sysLogDAO")
public class SysLogDAO extends AdmAbstractDAO {

	/**
	 * INSERT TADMIN_INFO_SYSLOG
	 * @param SysLogVO
	 * @return
	 * @throws Exception
	 */
	public int insertSysLog(SysLogVO vo) throws Exception {
		return insert("com.vinflux.adm.persistence.SysLogDAO.insertSysLog",vo);
	}

}