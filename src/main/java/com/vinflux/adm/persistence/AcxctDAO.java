package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("acxctDAO")
public class AcxctDAO extends AdmAbstractDAO {
	
	public int selectAcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AcxctDAO.selectAcxctCount", parameterMap);
	}
	
	public int selectDetailAcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AcxctDAO.selectDetailAcxctCount", parameterMap);
	}
	
	public int checkAcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AcxctDAO.checkAcxctCount", parameterMap);
	}
	 
	public List selectAcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectAcxct", parameterMap);
	}
	
	public List selectDetailAcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectDetailAcxct", parameterMap);
	}
	
	public List selectPkAcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectPkAcxct", parameterMap);
	}
	
	public int insertTAdminMstAcxct(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.AcxctDAO.insertTAdminMstAcxct", parameterMap);
	}

	public int updateTAdminMstAcxct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AcxctDAO.updateTAdminMstAcxct", parameterMap);
	}

	public int updateTWorkMstAcxct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.AcxctDAO.updateTWorkMstAcxct", parameterMap);
	}
	
	public int deleteTAdminMstAcxct(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.AcxctDAO.deleteTAdminMstAcxct", parameterMap);
	}	

//	public List selectHerderExcelDownAcxct(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectHerderExcelDownAcxct", parameterMap);
//	}
	
	public Workbook selectExcelDownHerderAcxct(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat); 
		getSqlSession().select("com.vinflux.adm.persistence.AcxctDAO.selectExcelDownHerderAcxct", parameterMap, handler);
		return handler.getWorkbook();
	}

	public List selectExcelDownDetailAcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectExcelDownDetailAcxct", parameterMap);
	}
	
	public Workbook selectExcelDownDetailAcxct(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AcxctDAO.selectExcelDownDetailAcxct", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	
	public Workbook selectExcelDownAllAcxct(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat); 
		getSqlSession().select("com.vinflux.adm.persistence.AcxctDAO.selectExcelDownAllAcxct", parameterMap, handler);
		return handler.getWorkbook();
	}	
	
//	public List selectExcelDownDetailAcxct(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.adm.persistence.AcxctDAO.selectExcelDownDetailAcxct", parameterMap);
//	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}


}