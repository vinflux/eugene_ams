package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("storexctDAO")
public class StorexctDAO extends AdmAbstractDAO {
	
	public int selectStorexctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StorexctDAO.selectStorexctCount", parameterMap);
	}
	
	public int checkStorexctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StorexctDAO.checkStorexctCount", parameterMap);
	}
	 
	public List selectStorexct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StorexctDAO.selectStorexct", parameterMap);
	}
	
	public List selectPkStorexct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StorexctDAO.selectPkStorexct", parameterMap);
	}
	
	public int insertTAdminMstStorexct(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.StorexctDAO.insertTAdminMstStorexct", parameterMap);
	}

	public int updateTAdminMstStorexct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.StorexctDAO.updateTAdminMstStorexct", parameterMap);
	}
	
	public int deleteTAdminMstStorexct(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.StorexctDAO.deleteTAdminMstStorexct", parameterMap);
	}	

	public List selectExcelDownStorexct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StorexctDAO.selectExcelDownStorexct", parameterMap);
	}
	
	public Workbook selectExcelDownStorexct(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.StorexctDAO.selectExcelDownStorexct", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StorexctDAO.selectCtkey", parameterMap);
	}

}