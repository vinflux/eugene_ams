package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("substitutionDAO")
public class SubstitutionDAO extends AdmAbstractDAO {
	

	public int selectPkCountSubstitution(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionDAO.selectPkCountSubstitution", parameterMap);
	} 
	 

	public List selectListSubstitution(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionDAO.selectListSubstitution", parameterMap);
	}
	
	public Map selectPkSubstitution(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.SubstitutionDAO.selectPkSubstitution", parameterMap);
	}
	
	//센터 조회
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionDAO.selectTAdminMstCt",parameterMap);
	}
	
	//물품대체코드 별 센터
	public List selectTAdminMstIcsbxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionDAO.selectTAdminMstIcsbxct",parameterMap);
	}

	
	public int selectPkCountTadminMstIcsbxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionDAO.selectPkCountTadminMstIcsbxct", parameterMap);
	} 
	

	public int insertTAdminMstIcsb (Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.SubstitutionDAO.insertTAdminMstIcsb", parameterMap);
	}
	
	public int insertTAdminMstIcsbxct (Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.SubstitutionDAO.insertTAdminMstIcsbxct", parameterMap);
	}
	

	public int updateTAdminMstIcsb(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.SubstitutionDAO.updateTAdminMstIcsb", parameterMap);
	}


	//물품대체코드 마스터 그리드 삭제
	public int deleteSubstitution(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.SubstitutionDAO.deleteSubstitution", parameterMap);
	}

	//물품대체코드 센터별 그리드 삭제
	public int deleteTAdminMstIcsbxct(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.SubstitutionDAO.deleteTAdminMstIcsbxct", parameterMap);
	}

	public List selectListSubstitutionExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionDAO.selectListSubstitutionExcel", parameterMap);
	}
	
	public Workbook selectListSubstitutionExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.SubstitutionDAO.selectListSubstitutionExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public int selectPkCountIcxCt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionDAO.selectPkCountIcxCt", parameterMap);
	} 
	
	public int selectPkCountIcuttypexIcxCt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionDAO.selectPkCountIcuttypexIcxCt", parameterMap);
	}
	
	public int updateTAdminMstIcsbxct(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.SubstitutionDAO.updateTAdminMstIcsbxct", parameterMap);
	}

}