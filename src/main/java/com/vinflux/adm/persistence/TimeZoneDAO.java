package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("timeZoneDAO")
public class TimeZoneDAO extends AdmAbstractDAO {

	/**
	* 표준시간대코드 헤더 정보 조회 : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectTimezoneHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.TimeZoneDAO.selectTimezoneHd", parameterMap);
	}

	/**
	* 표준시간대코드 헤더 정보 count : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int selectTimezoneCountHd(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.TimeZoneDAO.selectTimezoneCountHd", parameterMap);
	}

	/**
	* 표준시간대코드 상세 조회 : TORDER_MST_ORCD_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectTimezoneDtl(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.TimeZoneDAO.selectTimezoneDtl", parameterMap);
	}
	
	/**
	* 표준시간대코드 상세 정보 count : TADMIN_MST_TZ_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int selectTimezoneCountDtl(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.TimeZoneDAO.selectTimezoneCountDtl", parameterMap);
	}	

	/**
	* 표준시간대코드 중복 확인 : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkTimezoneInfoHd(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.TimeZoneDAO.checkTimezoneInfoHd", parameterMap);
	}

	/**
	* 표준시간대코드 중복 확인 : TORDER_MST_ORCD_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkTimezoneInfoDtl(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.TimeZoneDAO.checkTimezoneInfoDtl", parameterMap);
	}

	/**
	* 표준시간대코드 헤더 생성 : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertTimezoneInfoHd(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.TimeZoneDAO.insertTimezoneInfoHd", parameterMap);
	}

	/**
	* 표준시간대코드 상세 생성 : TORDER_MST_ORCD_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertTimezoneInfoDtl(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.TimeZoneDAO.insertTimezoneInfoDtl", parameterMap);
	}

	/**
	* 표준시간대코드 헤더 정보 갱신 : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateTimezoneInfoHd(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.TimeZoneDAO.updateTimezoneInfoHd", parameterMap);
	}

	/**
	* 표준시간대코드 상세 정보 갱신 : TORDER_MST_ORCD_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateTimezoneInfoDtl(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.TimeZoneDAO.updateTimezoneInfoDtl", parameterMap);
	}

	/**
	* 표준시간대코드 헤더 정보 삭제 : TADMIN_MST_TZ_HD
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public void deleteTimezoneInfoHd(Map parameterMap) throws Exception {
		delete("com.vinflux.adm.persistence.TimeZoneDAO.deleteTimezoneInfoHd", parameterMap);
	}

	/**
	* 표준시간대코드 상세 정보 삭제 : TORDER_MST_ORCD_DT
	* {@link com.vinflux.adm.master.service.impl.TimeZoneServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public void deleteTimezoneInfoDtl(Map parameterMap) throws Exception {
		delete("com.vinflux.adm.persistence.TimeZoneDAO.deleteTimezoneInfoDtl", parameterMap);
	}
	
	/**
	 * 표준시간대 상단 엑셀 다운
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTimzoneHdExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.TimeZoneDAO.selectTimzoneHdExcel", parameterMap);
	}

	public Workbook selectTimzoneHdExcel(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.TimeZoneDAO.selectTimzoneHdExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 표준시간대 하단 엑셀 다운
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTimzoneDtlExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.TimeZoneDAO.selectTimzoneHdDtlExcel", parameterMap);
	}

	public Workbook selectTimzoneDtlExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.TimeZoneDAO.selectTimzoneHdDtlExcel", parameterMap, handler);
		return handler.getWorkbook();
	}

}
