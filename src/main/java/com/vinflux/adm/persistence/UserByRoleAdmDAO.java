package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.vinflux.adm.session.service.LoginVO;
import com.vinflux.framework.db.AdmAbstractDAO;


/**
 * 일반 로그  클래스
 * @author 연구 개발팀 최순봉
 * @since 2013.05.21
 * @version 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  2013.05.21  최순봉          최초 생성 
 *  </pre>
 */

@Repository("userByRoleAdmDAO")
public class UserByRoleAdmDAO extends AdmAbstractDAO {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserByRoleAdmDAO.class);
    
    /**
     * 2013.05.21
	 * urkey,urpw 이용한 로그인을 처리한다
	 * @param vo LoginVO
	 * @return LoginVO
	 * @exception Exception
	 */
    public LoginVO actionLogin(LoginVO vo) {
    	return (LoginVO)selectByPk("com.vinflux.adm.persistence.UserByRoleAdmDAO.actionLogin", vo);
    }
    
    /**
     * 2014.10.16
	 * 거래처 체크
	 * @param vo LoginVO
	 * @return int
	 * @exception Exception
	 */
    public int actionLoginAcCount(LoginVO vo) {
    	return selectForInt("com.vinflux.adm.persistence.UserByRoleAdmDAO.actionLoginAcCount", vo);
    }
    
    /**
     * 2014.10.16
	 * portal 거래처 로그인 시 정보를 조회한다.
	 * @param vo LoginVO
	 * @return LoginVO
	 * @exception Exception
	 */
    public LoginVO actionLoginAc(LoginVO vo) {
    	return (LoginVO)selectByPk("com.vinflux.adm.persistence.UserByRoleAdmDAO.actionLoginAc", vo);
    }
    
    public String selectIpaddrxUser(LoginVO vo) {
    	return selectForStr("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectIpaddrxUser", vo);
    }
    
    /**
     * 2014.02.04
	 * user 정보  count
	 * @param vo LoginVO
	 * @return int
	 * @exception Exception
	 */
    public int actionLoginCount(LoginVO vo) {
    	return selectForInt("com.vinflux.adm.persistence.UserByRoleAdmDAO.actionLoginCount", vo);
    }
    
    /**
     * 2014.09.05
	 * 로그인 성공후 ip를 업데이트 한다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public int updateUniqueAddr(LoginVO vo) throws Exception {
		return update("com.vinflux.adm.persistence.UserByRoleAdmDAO.updateUniqueAddr", vo);
	}

    /**
     * 2013.08.27
	 * user별 사용할 수 있는 application key를 가져온다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public List selectAppKeyRole(Map parameterMap) throws Exception {		
		List list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectAppKeyRole", parameterMap);
//		if ( list==null || list.isEmpty() ){
//			list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectAppKeyRoleAll", parameterMap);
//		}
		return list;
	}

    /**
	 * user별 사용할 수 있는 application key를 가져온다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public List selectAppKeyRoleAuth(Map parameterMap) throws Exception {		
		return selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectAppKeyRoleAuth", parameterMap);
	}	
	
    /**
     * 2013.08.27
	 * user별 사용할 수 있는 Owner key를 가져온다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public List selectOwnerKeyRole(Map parameterMap) throws Exception {
		List list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectOwnerKeyRole", parameterMap);
		if ( list==null || list.isEmpty() ){
			list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectOwnerKeyRoleAll", parameterMap);
		}
		return list;
	}

    /**
     * 2013.08.27
	 * user별 사용할 수 있는 Center key를 가져온다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public List selectCenterKeyRole(Map parameterMap) throws Exception {
		List list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectCenterKeyRole", parameterMap);
		
		//권한이 없을 경우 전체 검색.
		if ( list==null || list.isEmpty() ) {
			list = selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectCenterKeyRoleAll", parameterMap);
		}		
		return list;
	}

    /**
     * 2013.08.27
	 * user별 사용할 수 있는 Menu Object key를 가져온다.
	 * @param HashMap
	 * @return List
	 * @exception Exception
	 */
	public List selectMenuObjectKeyRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserByRoleAdmDAO.selectMenuObjectKeyRole", parameterMap);
	}
}
