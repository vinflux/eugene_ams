package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("dicDAO")
public class DicDAO extends AdmAbstractDAO {
	
	public int selectDicCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DicDAO.selectDicCount", parameterMap);
	}
	
	public int checkDicCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DicDAO.checkDicCount", parameterMap);
	}
	 
	public List selectDic(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DicDAO.selectDic", parameterMap);
	}
	
	public List selectPkDic(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DicDAO.selectPkDic", parameterMap);
	}
	
	public int insertTAdminMstDic(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.DicDAO.insertTAdminMstDic", parameterMap);
	}

	public int updateTAdminMstDic(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.DicDAO.updateTAdminMstDic", parameterMap);
	}
	
	public int deleteTAdminMstDic(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.DicDAO.deleteTAdminMstDic", parameterMap);
	}	

	public List selectExcelDownDic(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DicDAO.selectExcelDownDic", parameterMap);
	}
	
	public Workbook selectExcelDownDic(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.DicDAO.selectExcelDownDic", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}
}