package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("ownerDAO")
public class OwnerDAO extends AdmAbstractDAO {
	
	public int selectOwnerCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.OwnerDAO.selectOwnerCount", parameterMap);
	}
	
	public int checkOwnerCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.OwnerDAO.checkOwnerCount", parameterMap);
	}
	 
	public List selectOwner(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.OwnerDAO.selectOwner", parameterMap);
	}

	public Map selectPkOwner(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.OwnerDAO.selectPkOwner", parameterMap);
	}
	
	public int insertTAdminMstOw(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.OwnerDAO.insertTAdminMstOw", parameterMap);
	}

	public int updateTAdminMstOw(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.OwnerDAO.updateTAdminMstOw", parameterMap);
	}
	
	public int deleteTAdminMstOw(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.OwnerDAO.deleteTAdminMstOw", parameterMap);
	}	

	public List selectExcelDownOwner(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.OwnerDAO.selectExcelDownOwner", parameterMap);
	}

	public Workbook selectExcelDownOwner(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.OwnerDAO.selectExcelDownOwner", parameterMap, handler);
		return handler.getWorkbook();
	}
}