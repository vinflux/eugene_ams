package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("userScreenDAO")
public class UserScreenDAO extends AdmAbstractDAO {
	
	@Resource(name="mstUrscGenService")
	private IdGenService mstUrscGenService;
	
	public int selectUserScreenCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UserScreenDAO.selectUserScreenCount", parameterMap);
	} 
	 
	public List selectUserScreen(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserScreenDAO.selectUserScreen", parameterMap);
	} 
	
	public Map selectTAdminMstUrsc(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.UserScreenDAO.selectTAdminMstUrsc", parameterMap);
	} 

	public int insertTAdminMstUrsc(Map parameterMap) throws Exception {
		String uskey = mstUrscGenService.getNextStringId();
		parameterMap.put("uskey", uskey);
		return insert("com.vinflux.adm.persistence.UserScreenDAO.insertTAdminMstUrsc", parameterMap);
	}

	public int updateTAdminMstUrsc(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.UserScreenDAO.updateTAdminMstUrsc", parameterMap);
	}
	
	public int deleteTAdminMstUrsc(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.UserScreenDAO.deleteTAdminMstUrsc", parameterMap);
	}

	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserScreenDAO.selectExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UserScreenDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}