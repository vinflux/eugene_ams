package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("storeDAO")
public class StoreDAO extends AdmAbstractDAO {
	
	/**
	 * select total count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreDAO.selectStoreCount", parameterMap);
	} 
	 
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreDAO.selectStore", parameterMap);
	}

	/**
	 * select pk
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectPkStore(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.StoreDAO.selectPkStore", parameterMap);
	}
	/**
	 * select pk
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCeckStore(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.StoreDAO.selectCeckStore", parameterMap);
	}
	

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectTAdminMstStore(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.StoreDAO.selectTAdminMstStore", parameterMap);
	}

	/**
	 * insert tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstStore(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.StoreDAO.insertTAdminMstStore", parameterMap);
	}
	public int mergeTtShpgLocT(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.StoreDAO.mergeTtShpgLocT", parameterMap);
	}
	
	/**
	 * update tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstStore(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.StoreDAO.updateTAdminMstStore", parameterMap);
	}


	/**
	 * delete tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstStore(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.StoreDAO.deleteTAdminMstStore", parameterMap);
	}
	
	/**
	 * center select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreDAO.selectTAdminMstCt",parameterMap);
	}
	
	/**
	 * loggrpcd select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAdminMstLogg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreDAO.selectAdminMstLogg",parameterMap);
	}
	
	
	/**
	 * SELECT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstStorexct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreDAO.selectPkCntTadminMstStorexct",parameterMap);
	}
	
	
	/**
	 * SELECT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstStorexct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreDAO.selectTAdminMstStorexct",parameterMap);
	}
	
	/**
	 * INSERT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstStorexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.StoreDAO.insertTAdminMstStorexct",parameterMap);
	}
	
	/**
	 * update tadmin_mst_storexct
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstStorexct(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.StoreDAO.updateTAdminMstStorexct", parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstStorexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.StoreDAO.deleteTAdminMstStorexct",parameterMap);
	}

	/**
	 * select excel list
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreDAO.selectExcelDownStore", parameterMap);
	}

	public Workbook selectExcelDownStore(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.StoreDAO.selectExcelDownStore", parameterMap, handler);
		return handler.getWorkbook();
	}
}