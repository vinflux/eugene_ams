package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("accountUploadDAO")
public class AccountUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 거래처 코드 임시 테이블 row count : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectAccountUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountUploadDAO.selectAccountUploadCount", parameterMap);
	} 
	 
	/**
	 * 거래처 코드 임시 테이블 조회 : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectAccountUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountUploadDAO.selectAccountUpload", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_ACUP with paging
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectAccountTemp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountUploadDAO.selectAccountTemp", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectAccountTempAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountUploadDAO.selectAccountTempAll", parameterMap);
	}
	
	/**
	 * 거래처 코드 PK 조회 : TADMIN_MST_AC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstAc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountUploadDAO.selectPkCntTadminMstAc", parameterMap);
	}
	
	/**
	 * 거래처 코드 PK 조회 : TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstAcxCt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountUploadDAO.selectPkCntTadminMstAcxCt", parameterMap);
	}
	
	/**
	 * 거래처 코드 insert to temp table : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempAcup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.AccountUploadDAO.insertTadminTempAcup", parameterMap);
	}
	
	/**
	 * 거래처 코드 저장 전,후 I/F status 변경 : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminTempAcup (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.AccountUploadDAO.updateTadminTempAcup", parameterMap);
	}
	
	/**
	 * 거래처 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.AccountUploadDAO.insertTadminInfoUlhs", parameterMap);
	}

	/**
	 * 거래처 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_ACUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownAccountUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountUploadDAO.selectExcelDownAccountUpload",parameterMap);
	}

	public Workbook selectExcelDownAccountUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AccountUploadDAO.selectExcelDownAccountUpload",parameterMap, handler);
		return handler.getWorkbook();
	}
}