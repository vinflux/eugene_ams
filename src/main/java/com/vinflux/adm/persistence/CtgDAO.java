package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("ctgDAO")
public class CtgDAO extends AdmAbstractDAO {
	
	public int selectCtgCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CtgDAO.selectCtgCount", parameterMap);
	}
	
	public int checkCtgCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CtgDAO.checkCtgCount", parameterMap);
	}
	 
	public List selectCtg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CtgDAO.selectCtg", parameterMap);
	}
	
	public List selectPkCtg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CtgDAO.selectPkCtg", parameterMap);
	}

	public Map selectPkCtgMap(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.CtgDAO.selectPkCtg", parameterMap);
	}
	
	public int insertTAdminMstCtg(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.CtgDAO.insertTAdminMstCtg", parameterMap);
	}

	public int updateTAdminMstCtg(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CtgDAO.updateTAdminMstCtg", parameterMap);
	}
	
	public int deleteTAdminMstCtg(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.CtgDAO.deleteTAdminMstCtg", parameterMap);
	}	

	public List selectExcelDownCtg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CtgDAO.selectExcelDownCtg", parameterMap);
	}
	
	public Workbook selectExcelDownCtg(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CtgDAO.selectExcelDownCtg", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select total Ctg Key. count        
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int searchCtgCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CtgDAO.searchCtgCount", parameterMap);
	} 
	
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List searchCtg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CtgDAO.searchCtg", parameterMap);
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}

	
}