package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("substitutionUploadDAO")
public class SubstitutionUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 아이템 코드 임시 테이블 row count : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectSubstitutionUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectSubstitutionUploadCount", parameterMap);
	} 
	 
	/**
	 * 아이템 코드 임시 테이블 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectSubstitutionUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectSubstitutionUpload", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectSubstitutionTemp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectSubstitutionTemp", parameterMap);
	}
	
	/**
	 * 저장을 위한 업로드 내역 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectSubstitutionTempAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectSubstitutionTempAll", parameterMap);
	}
	
	/**
	 * 상품 코드 insert to temp table : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempIcsbup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.SubstitutionUploadDAO.insertTadminTempIcsbup", parameterMap);
	}
	
	/**
	 * 상품 코드 갱신 : TADMIN_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminMstIcsb (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.SubstitutionUploadDAO.updateTadminMstIcsb", parameterMap);
	}
	
	/**
	 * 상품 코드 insert : TADMIN_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminMstIcsb (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.SubstitutionUploadDAO.insertTadminMstIcsb", parameterMap);
	}
	
	/**
	 * 센터별 상품 코드 insert : TADMIN_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminMstIcsbxct (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.SubstitutionUploadDAO.insertTadminMstIcsbxct", parameterMap);
	}
	
	/**
	 * 상품 코드 저장 전/후 IF status 변경 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminTempIcsbup (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.SubstitutionUploadDAO.updateTadminTempIcsbup", parameterMap);
	}
	
	/**
	 * 상품 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.SubstitutionUploadDAO.insertTadminInfoUlhs", parameterMap);
	}

	/**
	 * 물픔 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return Lisst
	 * @throws Exception
	 */
	public List selectExcelDownSubstitutionUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectExcelDownSubstitutionUpload",parameterMap);
	}
	
	public Workbook selectExcelDownSubstitutionUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectExcelDownSubstitutionUpload",parameterMap,handler);
		return handler.getWorkbook();
	}
	
	public int selectCountIcxOw(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectCountIcxOw", parameterMap);
	} 
	
	public int selectCountIcuttypexIcutxIc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SubstitutionUploadDAO.selectCountIcuttypexIcutxIc", parameterMap);
	}


}