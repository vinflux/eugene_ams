package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemCodeDAO")
public class ItemCodeDAO extends AdmAbstractDAO {
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectItemCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectItemCodeCount", parameterMap);
	} 
	 
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectItemCode", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemCodeAvqtyCheck(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectItemCodeAvqtyCheck", parameterMap);
	}
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectPkItemCode(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.ItemCodeDAO.selectPkItemCode", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectPkCntTadminMstIcxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectPkCntTadminMstIcxct", parameterMap);
	} 
	
	/**
	 * TADMIN_MST_IC INSRET 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIc (Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.insertTAdminMstIc", parameterMap);
	}

	/**
	 * TADMIN_MST_IC UPDATE 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstIc (Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.updateTAdminMstIc", parameterMap);
	}
	/**
	 * TADMIN_MST_ICXCT UPDATE 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstIcxct (Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.updateTAdminMstIcxct", parameterMap);
	}
	
	/**
	 * TADMIN_MST_CTXACXIC UPDATE 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstCtxacxic (Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.updateTAdminMstCtxacxic", parameterMap);
	}
	
	/**
	 * TADMIN_MST_IC DELETE 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstIc (Map parameterMap) throws Exception { 
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.deleteTAdminMstIc", parameterMap);
	}
	
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstCt",parameterMap);
	}
	
	public List selecticgrCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selecticgrCtkey",parameterMap);
	}
	
	public List selecticutCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selecticutCtkey",parameterMap);
	}
	
	public int selectCountTAdminMstIcutxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectCountTAdminMstIcutxct",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstIcxct",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstIcxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemCodeDAO.deleteTAdminMstIcxct",parameterMap);
	}
	
	public int selectPkCntTadminMstCtxacxic(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectPkCntTadminMstCtxacxic",parameterMap);
	}
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstCtxacxic(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemCodeDAO.deleteTAdminMstCtxacxic",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.insertTAdminMstIcxct",parameterMap);
	}
		
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstCtxacxic(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.insertTAdminMstCtxacxic",parameterMap);
	}
	
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownItemCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectExcelDownItemCode",parameterMap);
	}

	public Workbook selectExcelDownItemCode(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemCodeDAO.selectExcelDownItemCode",parameterMap, handler);
		return handler.getWorkbook();
	}

	public int updateSCM(Map row) throws Exception  {
		return update("com.vinflux.adm.persistence.ItemCodeDAO.updateSCM", row);
	}
	
	
	
	/**
	 * 화주변경 저장 : 상품단위 코드 존재여부 확인
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstIcutCount",parameterMap);
	}

	/**
	 * 화주변경 저장 : 기존 상품 코드 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectTAdminMstIcut(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstIcut",parameterMap);
	}
	
	/**
	 * 화주변경 저장 : 상품단위 코드 저장
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.insertTAdminMstIcut",parameterMap);
	}

	/**
	 * 화주변경 저장 : 상품단위 코드 존재여부 확인
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcutxtypeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstIcutxtypeCount",parameterMap);
	}

	/**
	 * 화주변경 저장 : 기존 상품단위 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeDAO.selectTAdminMstIcutxtype",parameterMap);
	}
	
	/**
	 * 화주변경 저장 : 상품단위 코드 저장
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstIcutxtype(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeDAO.insertTAdminMstIcutxtype",parameterMap);
	}	
}