package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("calendarDAO")
public class CalendarDAO extends AdmAbstractDAO {
	
	public int selectCalendarCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CalendarDAO.selectCalendarCount", parameterMap);
	} 
	 
	public List selectCalendar(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CalendarDAO.selectCalendar", parameterMap);
	}

	public Map selectPkCalendar(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.CalendarDAO.selectPkCalendar", parameterMap);
	}
	
	public int insertCalendar(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.CalendarDAO.insertCalendar", parameterMap);
	}

	public int updateCalendar(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CalendarDAO.updateCalendar", parameterMap);
	}
	
	public int deleteCalendar(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.CalendarDAO.deleteCalendar", parameterMap);
	}	

	public List selectExcelDownCalendar(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CalendarDAO.selectExcelDownCalendar", parameterMap);
	}

	public Workbook selectExcelDownCalendar(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CalendarDAO.selectExcelDownCalendar", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}