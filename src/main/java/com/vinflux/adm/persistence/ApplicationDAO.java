package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("applicationDAO")
public class ApplicationDAO extends AdmAbstractDAO  {
	
	public List selectApplicationInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ApplicationDAO.selectApplicationInfoList", parameterMap);
	}
	
	public List selectApplicationInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ApplicationDAO.selectApplicationInfo", parameterMap);
	}
	
	public int selectApplicationCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ApplicationDAO.selectApplicationCount", parameterMap);
	}
	
	public int checkApplicationInfo(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ApplicationDAO.checkApplicationInfo", parameterMap);
	}
	
	public int insertApplicationInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.ApplicationDAO.insertApplicationInfo", parameterMap);
	}
	
	
	public int updateApplicationInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.ApplicationDAO.updateApplicationInfo", parameterMap);
	}
	
	public int deleteApplicationInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ApplicationDAO.deleteApplicationInfo", parameterMap);
	}
	
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ApplicationDAO.selectExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ApplicationDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}
