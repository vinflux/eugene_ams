package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("icxctxacDAO")
public class IcxctxacDAO extends AdmAbstractDAO {
	
	public int selectIcxctxacCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcxctxacDAO.selectIcxctxacCount", parameterMap);
	}
	
	public int checkIcxctxacCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcxctxacDAO.checkIcxctxacCount", parameterMap);
	}
	 
	public List selectIcxctxac(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctxacDAO.selectIcxctxac", parameterMap);
	}
	
	public List selectPkIcxctxac(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctxacDAO.selectPkIcxctxac", parameterMap);
	}
	
	public int insertTAdminMstIcxctxac(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.IcxctxacDAO.insertTAdminMstIcxctxac", parameterMap);
	}

	public int updateTAdminMstIcxctxac(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.IcxctxacDAO.updateTAdminMstIcxctxac", parameterMap);
	}

	public int updateTAdminMstIcxctxacAll(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.IcxctxacDAO.updateTAdminMstIcxctxacAll", parameterMap);
	}	
	
	public int deleteTAdminMstIcxctxac(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.IcxctxacDAO.deleteTAdminMstIcxctxac", parameterMap);
	}	

	public List selectExcelDownIcxctxac(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctxacDAO.selectExcelDownIcxctxac", parameterMap);
	}
	
	public Workbook selectExcelDownIcxctxac(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcxctxacDAO.selectExcelDownIcxctxac", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}

}