package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemCodeUploadDAO")
public class ItemCodeUploadDAO extends AdmAbstractDAO {
	
	/**
	 * 아이템 코드 임시 테이블 row count : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectItemCodeUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectItemCodeUploadCount", parameterMap);
	} 
	 
	/**
	 * 아이템 코드 임시 테이블 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectItemCodeUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectItemCodeUpload", parameterMap);
	}
	
	/**
	 * 엑셀 업로드 내역 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemCodeTemp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectItemCodeTemp", parameterMap);
	}
	
	/**
	 * 저장을 위한 업로드 내역 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemCodeTempAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectItemCodeTempAll", parameterMap);
	}
	
	/**
	 * 물품 코드 PK 조회 : TADMIN_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstIc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectPkCntTadminMstIc", parameterMap);
	}
	
	/**
	 * 물품 코드 PK 조회 : TADMIN_MST_IC
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstAc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectPkCntTadminMstAc", parameterMap);
	}	
	
	/**
	 * 물품 코드 insert to temp table : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminTempIcup (Map parameterMap ) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemCodeUploadDAO.insertTadminTempIcup", parameterMap);
	}
	
	/**
	 * 물품 코드 저장 전,후 I/F status 변경 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTadminTempIcup (Map parameterMap ) throws Exception {
		return update("com.vinflux.adm.persistence.ItemCodeUploadDAO.updateTadminTempIcup", parameterMap);
	}
	
	/**
	 * 물품 코드 업로드 내역 정보 insert : TADMIN_INFO_ULHS
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTadminInfoUlhs(Map parameterMap) throws Exception {
		// param setting 
		return insert("com.vinflux.adm.persistence.ItemCodeUploadDAO.insertTadminInfoUlhs", parameterMap);
	}

	/**
	 * 물픔 코드 임시 테이블 엑셀 다운로드를 위한 조회 : TADMIN_TEMP_ICUP
	 * @param parameterMap
	 * @return Lisst
	 * @throws Exception
	 */
	public List selectExcelDownItemCodeUpload(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectExcelDownItemCodeUpload",parameterMap);
	}

	public Workbook selectExcelDownItemCodeUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemCodeUploadDAO.selectExcelDownItemCodeUpload",parameterMap, handler);
		return handler.getWorkbook();
	}
}