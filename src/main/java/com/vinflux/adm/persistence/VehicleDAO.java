package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("vehicleDAO")
public class VehicleDAO extends AdmAbstractDAO {

	public List selectEqmt(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.VehicleDAO.selectEqmt", parameterMap);
	}
	
	public int selectEqmtCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.VehicleDAO.selectEqmtCount", parameterMap);
	}
	
	public List selectEqmtList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.VehicleDAO.selectEqmtList", parameterMap);
	}
	
	public int insertEqmt(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.VehicleDAO.insertEqmt", parameterMap);
	}
	
	public int updateEqmt(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.VehicleDAO.updateEqmt", parameterMap);
	}
	
	public int deleteEqmt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.VehicleDAO.deleteEqmt", parameterMap);
	}

	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.VehicleDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.VehicleDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

	public int checkValidEqmtList(List<String> parameterList) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.VehicleDAO.checkValidEqmtList", parameterList);
	}
	
	
	public Integer selectEqmtDtlCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.VehicleDAO.selectEqmtDtlCount", parameterMap);
	}

	public List selectEqmtDtlList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.VehicleDAO.selectEqmtDtlList", parameterMap);
	}

	public int insertEqmtDtl(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.VehicleDAO.insertEqmtDtl", parameterMap);
	}
	
	public int deleteEqmtDtl(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.VehicleDAO.deleteEqmtDtl", parameterMap);
	}

	public List selectEqmtDtl(Map paramMap) {
		return selectList("com.vinflux.adm.persistence.VehicleDAO.selectEqmtDtl", paramMap);
	}

	public int updateEqmtDtl(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.VehicleDAO.updateEqmtDtl", parameterMap);
	}
	
	public int approveEqmt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.VehicleDAO.approveEqmt", parameterMap);
	}

	
	
}
