package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("storeevxicDAO")
public class StoreevxicDAO extends AdmAbstractDAO {
	
	public int selectStoreevxicCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreevxicDAO.selectStoreevxicCount", parameterMap);
	}
	 
	public List selectStoreevxic(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevxicDAO.selectStoreevxic", parameterMap);
	}

	public List selectExcelDownStoreevxic(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevxicDAO.selectExcelDownStoreevxic", parameterMap);
	}

	public Workbook selectExcelDownStoreevxic(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.StoreevxicDAO.selectExcelDownStoreevxic", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	
	public int selectStoreevxicCount2(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreevxicDAO.selectStoreevxicCount2", parameterMap);
	}
	 
	public List selectStoreevxic2(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevxicDAO.selectStoreevxic2", parameterMap);
	}

	public List selectExcelDownStoreevxic2(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevxicDAO.selectExcelDownStoreevxic2", parameterMap);
	}

	public Workbook selectExcelDownStoreevxic2(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.StoreevxicDAO.selectExcelDownStoreevxic2", parameterMap, handler);
		return handler.getWorkbook();
	}	
}