/**
 *
 */
package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

/**
 * @author goldbug
 *
 */
@Repository("commonSearchDAO")
public class CommonSearchDAO extends AdmAbstractDAO {

	/**
	 * Search Type 10 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtMstTmcdDtCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtMstTmcdDtCount", paramMap);
	}

	/**
	 * Search Type 10 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtMstTmcdDtList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtMstTmcdDtList", paramMap);
	}

	/**
	 * Search Type 20 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtMstCtCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtMstCtCount", paramMap);
	}

	/**
	 * Search Type 20 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtMstCtList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtMstCtList", paramMap);
	}

	/**
	 * Search Type 30 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtCustTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCustTCount", paramMap);
	}

	/**
	 * Search Type 30 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtCustTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCustTList", paramMap);
	}

	/**
	 * Search Type 40 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTAdminMstUrCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTAdminMstUrCount", paramMap);
	}

	/**
	 * Search Type 40 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTAdminMstUrList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTAdminMstUrList", paramMap);
	}

	/**
	 * Search Type 50 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtPlanTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtPlanTCount", paramMap);
	}

	/**
	 * Search Type 50 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtPlanTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtPlanTList", paramMap);
	}

	/**
	 * Search Type 60 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtCarrTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCarrTCount", paramMap);
	}

	/**
	 * Search Type 60 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtCarrTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCarrTList", paramMap);
	}

	/**
	 * Search Type 70 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtItmGrpTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtItmGrpTCount", paramMap);
	}

	/**
	 * Search Type 70 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtItmGrpTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtItmGrpTList", paramMap);
	}

	/**
	 * Search Type 80 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtItmMstTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtItmMstTCount", paramMap);
	}

	/**
	 * Search Type 80 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtItmMstTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtItmMstTList", paramMap);
	}

	/**
	 * Search Type 90 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtShpgLocTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtShpgLocTCount", paramMap);
	}

	/**
	 * Search Type 90 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtShpgLocTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtShpgLocTList", paramMap);
	}

	/**
	 * Search Type 100 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtChrgTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtChrgTCount", paramMap);
	}

	/**
	 * Search Type 100 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtChrgTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtChrgTList", paramMap);
	}

	/**
	 * Search Type 110 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtEqmtTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtEqmtTCount", paramMap);
	}

	/**
	 * Search Type 110 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtEqmtTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtEqmtTList", paramMap);
	}

	/**
	 * Search Type 120 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtZoneTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtZoneTCount", paramMap);
	}

	/**
	 * Search Type 120 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtZoneTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtZoneTList", paramMap);
	}

	/**
	 * Search Type 120 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtServiceTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtServiceTCount", paramMap);
	}

	/**
	 * Search Type 130 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtServiceTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtServiceTList", paramMap);
	}

	/**
	 * Search Type 140 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtRtgrpHdTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtRtgrpHdTCount", paramMap);
	}

	/**
	 * Search Type 140 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtRtgrpHdTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtRtgrpHdTList", paramMap);
	}

	/**
	 * Search Type 150 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtRtMstHdTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtRtMstHdTCount", paramMap);
	}

	/**
	 * Search Type 150 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtRtMstHdTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtRtMstHdTList", paramMap);
	}
	
	   /**
     * Search Type 160 Count
     * @param paramMap
     * @return int
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public int selectTtDrvrTCount(Map paramMap) throws Exception {
        return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtDrvrTCount", paramMap);
    }

    /**
     * Search Type 160 List
     * @param paramMap
     * @return List
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public List selectTtDrvrTList(Map paramMap) throws Exception {
        return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtDrvrTList", paramMap);
    }

    /**
     * Search Type 170 Count
     * @param paramMap
     * @return int
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
	public Integer selectTtCncyTCount(Map paramMap) {
    	 return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCncyTCount", paramMap);
	}

    /**
     * Search Type 170 List
     * @param paramMap
     * @return List
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
	public List selectTtCncyTList(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtCncyTList", paramMap);
	}

	/**
	 * Search Type 180 Count
	 * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int selectTtOwTCount(Map paramMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PopupSearchDAO.selectTtOwTCount", paramMap);
	}

	/**
	 * Search Type 180 List
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List selectTtOwTList(Map paramMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PopupSearchDAO.selectTtOwTList", paramMap);
	}
		
}
