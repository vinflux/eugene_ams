package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("mixShipItemImpossDAO")
public class MixShipItemImpossDAO extends AdmAbstractDAO {

	/**
	* SET 혼적상품불가 조회 : TADMIN_MST_NOMIX_HD
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectMixShipItemImpossHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MixShipItemImpossDAO.selectMixShipItemImpossHd",parameterMap);
	}
	
	/**
	 * SET 혼적상품불가 count : TADMIN_MST_NOMIX_HD
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectMixShipItemImpossHdCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.MixShipItemImpossDAO.selectMixShipItemImpossHdCount", parameterMap);
	}

	/**
	* SET 혼적상품불가 조회 : TADMIN_MST_NOMIX_DT
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectMixShipItemImpossDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MixShipItemImpossDAO.selectMixShipItemImpossDt",parameterMap);
	}
	
	/**
	 * SET 혼적상품불가 count : TADMIN_MST_NOMIX_DT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectMixShipItemImpossDtCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.MixShipItemImpossDAO.selectMixShipItemImpossDtCount", parameterMap);
	}

	/**
	* 혼적상품불가 헤더 생성 : TADMIN_MST_NOMIX_HD
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertMixShipItemImposInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.MixShipItemImpossDAO.insertMixShipItemImposInfo", parameterMap);
	}

	/**
	 * 혼적상품불가 헤더 정보 갱신 : TADMIN_MST_NOMIX_HD
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateMixShipItemImposInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.MixShipItemImpossDAO.updateMixShipItemImposInfo", parameterMap);
	}
	
	/**
	 * 혼적상품불가 헤더 정보 삭제 : TADMIN_MST_NOMIX_HD
	 * @param Map parameterMap
	 * @return void
	 * @throws Exception
	 */
	public void deleteMixShipItemImposInfo(Map parameterMap) throws Exception {
		delete("com.vinflux.adm.persistence.MixShipItemImpossDAO.deleteMixShipItemImposInfo", parameterMap);
	}
	
	/**
	* 혼적상품불가 그룹명 중복 확인 : TADMIN_MST_NOMIX_HD
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkmixShipItemImposso(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MixShipItemImpossDAO.checkmixShipItemImposso",parameterMap);
	}
	
	
	/**
	* 혼적상품불가 상품코드 중복 확인 : TADMIN_MST_NOMIX_DT
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkDetailmixShipItemImposso(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MixShipItemImpossDAO.checkDetailmixShipItemImposso",parameterMap);
	}
	
	
	/**
	* 혼적상품불가 상세 생성 : TADMIN_MST_NOMIX_DT
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertDetailMixShipItemImposInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.MixShipItemImpossDAO.insertDetailMixShipItemImposInfo", parameterMap);
	}


	/**
	* 혼적상품불가 상세 정보 갱신 : TADMIN_MST_NOMIX_DT
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateDetailMixShipItemImposInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.MixShipItemImpossDAO.updateDetailMixShipItemImposInfo", parameterMap);
	}


	/**
	* 혼적상품불가 상세 정보 삭제 : TADMIN_MST_NOMIX_DT
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int deleteDetailMixShipItemImposInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MixShipItemImpossDAO.deleteDetailMixShipItemImposInfo", parameterMap);
	}

	/**
	* 혼적상품불가 헤더 엑셀다운 
	* @param Map parameterMap
	* @return Workbook
	* @throws Exception
	*/
	public List excelDownMixShipItemImposs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MixShipItemImpossDAO.excelDownMixShipItemImposs",parameterMap);
	}
	
	public Workbook excelDownMixShipItemImposs(Map paramMap, List<Map> excelFormat) {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MixShipItemImpossDAO.excelDownMixShipItemImposs", paramMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	* 혼적상품불가 상세 정보 엑셀다운 
	* @param Map parameterMap
	* @return Workbook
	* @throws Exception
	*/
	public List excelDownDetailMixShipItemImposs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MixShipItemImpossDAO.excelDownDetailMixShipItemImposs",parameterMap);
	}
	
	public Workbook excelDownDetailMixShipItemImposs(Map paramMap, List<Map> excelFormat) {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MixShipItemImpossDAO.excelDownDetailMixShipItemImposs", paramMap, handler);
		return handler.getWorkbook();
	}
}
