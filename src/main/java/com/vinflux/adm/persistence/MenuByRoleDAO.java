package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("menuByRoleDAO")
public class MenuByRoleDAO extends AdmAbstractDAO {
	/**
	 * Role별 어플리케이션 count : TADMIN_MST_RO, TADMIN_MST_ROXAPGT, TADMIN_MST_AP
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectApplicationByRoleCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MenuByRoleDAO.selectApplicationByRoleCount", parameterMap);
	} 
	
	/**
	 * Role별 어플리케이션 정보검색 : TADMIN_MST_RO, TADMIN_MST_ROXAPGT, TADMIN_MST_AP
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectApplicationByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuByRoleDAO.selectApplicationByRole", parameterMap);
	}
	
	/**
	 * Role별 메뉴 정보검색(for tree) : TADMIN_MST_ME, TADMIN_MST_URSC, TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMenuByRoleNexa(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.MenuByRoleDAO.selectMenuByRoleNexa", paramMap);
    }
	
	/**
	 * Role별 메뉴 정보검색(for tree) : TADMIN_MST_ME, TADMIN_MST_URSC, TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectMenuByRole(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.MenuByRoleDAO.selectMenuByRole", paramMap);
    }
	
	/**
	 * Role별 메뉴 정보검색 : TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRoxmegt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuByRoleDAO.selectTAdminMstRoxmegt", parameterMap);
	}
	
	/**
	 * Role별 메뉴 정보추가 : TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstRoxmegt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.MenuByRoleDAO.insertTAdminMstRoxmegt", parameterMap);
	}
	
	/**
	 * Role별 메뉴 정보삭제 : TADMIN_MST_ROXMEGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstRoxmegt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MenuByRoleDAO.deleteTAdminMstRoxmegt", parameterMap);
	}
}