package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("icxctDAO")
public class IcxctDAO extends AdmAbstractDAO {
	
	public int selectIcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcxctDAO.selectIcxctCount", parameterMap);
	}
	
	public int checkIcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.IcxctDAO.checkIcxctCount", parameterMap);
	}
	 
	public List selectIcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctDAO.selectIcxct", parameterMap);
	}
	
	public List selectPkIcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctDAO.selectPkIcxct", parameterMap);
	}
	
	public int insertTAdminMstIcxct(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.IcxctDAO.insertTAdminMstIcxct", parameterMap);
	}

	public int updateTAdminMstIcxct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.IcxctDAO.updateTAdminMstIcxct", parameterMap);
	}
	
	public int updateAllIcxct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.IcxctDAO.updateAllIcxct", parameterMap);
	}	
	
	public int updateChkIcxct(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.IcxctDAO.updateChkcxct", parameterMap);
	}	
	
	public int deleteTAdminMstIcxct(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.IcxctDAO.deleteTAdminMstIcxct", parameterMap);
	}	

	public List selectExcelDownIcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.IcxctDAO.selectExcelDownIcxct", parameterMap);
	}
	
	public Workbook selectExcelDownIcxct(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.IcxctDAO.selectExcelDownIcxct", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}

}