package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("apDAO")
public class ApDAO extends AdmAbstractDAO   {

	/**
	 * ADM >
	 * @param parameterMap
	 * @return
	 */
	public List selectApInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ApDAO.selectApInfoList", parameterMap);
	}
	
	/**
	 * ADM > 
	 * @param parameterMap
	 * @return
	 */
	public int selectApCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ApDAO.selectApCount", parameterMap);
	}

	/**
	 * ADM > 
	 * @param parameterMap
	 * @return
	 */
	public int insertAp(Map parameterMap) {
		return insert("com.vinflux.adm.persistence.ApDAO.insert", parameterMap);
	}
	
	/**
	 * ADM > 
	 * @param parameterMap
	 * @return
	 */
	public int updateAp(Map parameterMap) {
		return update("com.vinflux.adm.persistence.ApDAO.update", parameterMap);
	}
	
	/**
	 * ADM > 
	 * @param parameterMap
	 * @return
	 */
	public int deleteAp(Map parameterMap) {
		return delete("com.vinflux.adm.persistence.ApDAO.delete", parameterMap);
	}
	
	/**
	 * ADM >엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ApDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ApDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
}