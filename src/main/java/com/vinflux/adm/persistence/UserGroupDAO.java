package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("userGroupDAO")
public class UserGroupDAO extends AdmAbstractDAO   {

	public List selectUserGroupInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.UserGroupDAO.selectUserGroupInfoList", parameterMap);
	}
	
	public List selectUserGroupInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.UserGroupDAO.selectUserGroupInfo", parameterMap);
	}
	
	public int selectUserGroupCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.UserGroupDAO.selectUserGroupCount", parameterMap);
	}
	
	public int insertUserGroupInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.UserGroupDAO.insertUserGroupInfo", parameterMap);
	}
	
	
	public int updateUserGroupInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.UserGroupDAO.updateUserGroupInfo", parameterMap);
	}
	
	public int deleteUserGroupInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.UserGroupDAO.deleteUserGroupInfo", parameterMap);
	}
	
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserGroupDAO.selectExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UserGroupDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

}
