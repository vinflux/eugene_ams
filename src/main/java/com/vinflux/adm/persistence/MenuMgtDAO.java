package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("menuMgtDAO")
public class MenuMgtDAO extends AdmAbstractDAO {
	
	
	@Resource(name="usccHdKeyIdGenService")
	private IdGenService usccHdKeyIdGenService;
	
	@Resource(name="usccDtKeyIdGenService")
	private IdGenService usccDtKeyIdGenService;
	
	
	public int selectSearchConditionCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionCount", parameterMap);
	} 
	 
	public List selectSearchCondition(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchCondition", parameterMap);
	}
	
	public int selectSearchConditionDetailCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionDetailCount", parameterMap);
	} 
	 
	public List selectSearchConditionDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionDetail", parameterMap);
	}
	
	public int insertSearchCondition(Map parameterMap) throws Exception {		    	
    	//키 발급
		String usscHdKey = usccHdKeyIdGenService.getNextStringId();	
    	
		parameterMap.put("ussc_hdkey", usscHdKey);
		return insert("com.vinflux.adm.persistence.MenuMgtDAO.insertSearchCondition", parameterMap);
	}
	
	public int insertSearchConditionDetail(Map parameterMap) throws Exception {
		    	
    	//마스터 헤더키
    	String usscHdkey = (String) parameterMap.get("ussc_hdkey");
    	//마스터 디테일키
		String usccDtKey = usccDtKeyIdGenService.getNextStringIdByKey(usscHdkey);
    	
		parameterMap.put("ussc_dtkey", usccDtKey);    	   	
		return insert("com.vinflux.adm.persistence.MenuMgtDAO.insertSearchConditionDetail", parameterMap);
	}

	public int updateSearchCondition(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.MenuMgtDAO.updateSearchCondition", parameterMap);
	}
	
	public int updateSearchConditionDetail(Map parameterMap) throws Exception {		
		return update("com.vinflux.adm.persistence.MenuMgtDAO.updateSearchConditionDetail", parameterMap);
	}
	
	
	public int deleteSearchCondition(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.MenuMgtDAO.deleteSearchCondition", parameterMap);
	}	
	
	public int deleteSearchConditionDetail(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.MenuMgtDAO.deleteSearchConditionDetail", parameterMap);
	}

	public List selectSearchConditionExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionExcel", parameterMap);
	}
	
	public Workbook selectSearchConditionExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public List selectSearchConditionDetailExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionDetailExcel", parameterMap);
	}

	public Workbook selectSearchConditionDetailExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MenuMgtDAO.selectSearchConditionDetailExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
}