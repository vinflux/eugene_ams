package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("userDAO")
public class UserDAO extends AdmAbstractDAO   {

	/**
	 * ADM > 사용자 > 사용자그룹 > 조회 
	 * @param parameterMap
	 * @return
	 */
	public List selectUserInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.UserDAO.selectUserInfoList", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 1건 조회 
	 * @param parameterMap
	 * @return
	 */
	public Map selectPkTAdminMstUr(Map parameterMap) {
		return selectForMap("com.vinflux.adm.persistence.UserDAO.selectPkTAdminMstUr", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 조회 건수 
	 * @param parameterMap
	 * @return
	 */
	public int selectUserCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.UserDAO.selectUserCount", parameterMap);
	}

	public int selectUserInfoChkInsert(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.UserDAO.selectUserInfoChkInsert", parameterMap);
	}
	
	public int selectUserInfoChkUpdate(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.UserDAO.selectUserInfoChkUpdate", parameterMap);
	}	
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 등록 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertUserInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.UserDAO.insertUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 수정 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateUserInfo(Map parameterMap) throws Exception {    	   
		return update("com.vinflux.adm.persistence.UserDAO.updateUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 실패 건수 update
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateMaxLoginFailCount(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.UserDAO.updateMaxLoginFailCount", parameterMap);
	}
	
	/**
	 * 로그인 비밀 번호 실패 횟수 초기화 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateLoginFailInit(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.UserDAO.updateLoginFailInit", parameterMap);
	}

	/**
	 * 사용자 암호 수정 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateUserPw(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.UserDAO.updateUserPw", parameterMap);
	}
	
	
	
	/**
	 * select count 'whether expiredate is over or not!!'
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkExpireDateInfo(Map parameterMap) throws Exception {    	    	
		return selectForInt("com.vinflux.adm.persistence.UserDAO.checkExpireDateInfo", parameterMap);
	}
	
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 삭제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteUserInfo(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.UserDAO.deleteUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UserDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UserDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

	
	/**
	 * ADM > 사용자 > 사용자그룹 > 삭제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstUrXEq(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.UserDAO.deleteTAdminMstUrXEq", parameterMap);
	}

	/**
	 * INSERT TADMIN_MST_URXEQ 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstUrXEq(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.UserDAO.insertTAdminMstUrXEq", parameterMap);
	}

	/**
	 * SELECT COUNT TADMIN_MST_URXEQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrXEqCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UserDAO.selectTAdminMstUrXEqCount", parameterMap);
	}

	/**
	 * UPDATE INSERT TADMIN_MST_URXEQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstUrXEq(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.UserDAO.updateTAdminMstUrXEq", parameterMap);
	}

	
}