package com.vinflux.adm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;

@Repository("commonDAO")
public class CommonDAO extends AdmAbstractDAO {

	/**
	 * SELECT TADMIN_SET_ADCS
	 * @param paramMap
	 * @return
	 */
    public List selectAdminConfigList(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectAdminConfigList", paramMap);
    }
    
    /**
	 * SELECT TADMIN_SET_SC 
	 * @param paramMap
	 * @return
	 */
    public List selectSystemConfigList(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectSystemConfigList", paramMap);
    }
	
	/** Module menu select **/
    public List selectAppMessage(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectAppMessage", paramMap);
    }

	/** Module menu select **/
    public List selectModuleMenu(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectModuleMenu", paramMap);
    }

    /** Module menu select **/
    public List selectModuleMenuNexacro(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectModuleMenuNexacro", paramMap);
    }

	/** Module menu select By Role **/
    public List selectModuleMenuByRole(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectModuleMenuByRole", paramMap);
    }
    
    /** Module menu select By Role **/
    public List selectModuleMenuByRoleNexacro(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectModuleMenuByRoleNexacro", paramMap);
    }

	/** Count menu Role **/
    public int selectModuleMenuByRoleCount(Map paramMap) {
    	return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectModuleMenuByRoleCount", paramMap);
    }

    /** Search Condition select **/
    public List selectUserSearchCondtion(Map paramMap) {
    	return selectList("com.vinflux.adm.persistence.CommonDAO.selectUserSearchCondtion", paramMap);
    }
	
    /** Common code select **/
	public List selectCommonCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectCommonCode", parameterMap);
	}
	
	
	/**
	 * Master data select (APPLICATION) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstApCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstApCount",parameterMap);
	}
	
	/**
	 * Master data select (APPLICATION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAp",parameterMap);
	}
	
	/**
	 * Master data select (APPLICATION) COUNTAll - WINFOOD 160627.twyun
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstApCountAll(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstApCountAll",parameterMap);
	}
	
	/**
	 * Master data select (APPLICATION) All - WINFOOD 160627.twyun
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstApAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstApAll",parameterMap);
	}
	
	
	/**
	 * Master data select (ADMIN DT CODE) :TADMIN_MST_ADCD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstAdcdDtList() throws Exception {
		Map parameterMap = new HashMap();
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdDtList", parameterMap);
	}

	/**
	 * Master data select (ADMIN DT CODE) :TADMIN_MST_ADCD_DT (Lakey)
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstAdcdDtLakeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdDtLakeyList", parameterMap);
	}
	

	/**
	 * Master data select (ADMIN CODE) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstAdcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdDtCount",parameterMap);
	}


	/**
	 * Master data select (ADMIN CODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAdcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdDt",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) COUNT (for SelectCommonCode :290)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstAdcdCstDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdCstDtCount",parameterMap);
	}


	/**
	 * Master data select (ADMIN CODE) (for SelectCommonCode :290)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAdcdCstDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAdcdCstDt",parameterMap);
	}

	/**
	 * Master data select (MULTI LANGUAGE APPLICATION MESSAGE) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstMulAapMsgDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstMulAapMsgDtCount",parameterMap);
	}
	
	/**
	 * Master data select (MULTI LANGUAGE APPLICATION MESSAGE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstMulAapMsgDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstMulAapMsgDt",parameterMap);
	}

	
	/**
	 * Master data select (ACCOUNT) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAcCount",parameterMap);
	}
	
	/**
	 * Master data select (ACCOUNT)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAc",parameterMap);
	}

	

	public int selectTAdminMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstOwCount",parameterMap);
	}
	
	
	/** Master data select (OWNER) **/
	public List selectTAdminMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstOw",parameterMap);
	}

	
	/**
	 * Master data select (CENTER) count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstCtCount",parameterMap);
	}
	
	/**
	 * Master data select (CENTER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstCt",parameterMap);
	}

	/** Master data select (USER GROUP) **/
	public List selectTAdminMstUrgr(Map parameterMap) throws Exception { 
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrgr",parameterMap);
	}
	/**
	 * Master data select (USER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrgrCount",parameterMap);
	}
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrCount",parameterMap);
	}
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUr",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE GROUP)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcgrCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcgrCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE GROUP) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcgr",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE UNIT) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcutCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcutCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE UNIT)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcut",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcCount",parameterMap);
	}
	
	public int selectTAdminMstIcCount2(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcCount2",parameterMap);
	}

	/**
	 * Master data select (ITEMCODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIc",parameterMap);
	}
	
	public List selectTAdminMstIc2(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIc2",parameterMap);
	}
	
	public List selectTAdminMstIc3(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIc3",parameterMap);
	}

	public List selectTAdminMstIc4(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIc4",parameterMap);
	}

	/**
	 * Master data select (ITEMCODE) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcCountPerAC(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcCountPerAC",parameterMap);
	}

	/**
	 * Master data select (ITEMCODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcPerAC(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcPerAC",parameterMap);
	}
	/**
	 * Master data select (SUBSTITUTION) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcsbCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcsbCount",parameterMap);
	}
	
	/**
	 * Master data select (SUBSTITUTION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcsb(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcsb",parameterMap);
	}

	/**
	 * Master data select (USER SCREEN SEARCH CONDITION) 채ㅕㅜㅅ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUsscHdCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUsscHdCount",parameterMap);
	}
	
	/**
	 * Master data select (USER SCREEN SEARCH CONDITION)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUsscHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUsscHd",parameterMap);
	}

	/**
	 * Master data select (USER SCREEN) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrscCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrscCount",parameterMap);
	}

	/**
	 *  Master data select (USER SCREEN)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUrsc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrsc",parameterMap);
	}
	
	/**
	 * Master data select (USER SCREEN) VMS COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrscVmsCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrscVmsCount",parameterMap);
	}
	
	/**
	 *  Master data select (USER SCREEN) VMS
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUrscVms(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrscVms",parameterMap);
	}	
	
	/**
	 * Master data select (MENU) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstMeCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstMeCount",parameterMap);
	}

	/**
	 * Master data select (MENU)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstMe(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstMe",parameterMap);
	}


	/**
	 * Master data select (ROLE) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstRoCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstRoCount",parameterMap);
	}

	
	/**
	 * Master data select (ROLE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstRo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstRo",parameterMap);
	}

	/**
	 * Master data select (SYSTEM CONFIG) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminSetAdScCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminSetAdScCount",parameterMap);
	}

	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminSetAdSc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminSetAdSc",parameterMap);
	}
	
	
	/**
	 * Master data select (SYSTEM CONFIG) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminSetScCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminSetScCount",parameterMap);
	}

	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminSetSc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminSetSc",parameterMap);
	}
	
	
	/**
	 * Master data select (TIME ZONE) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstTzCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstTzCount",parameterMap);
	}

	/**
	 * Master data select (TIME ZONE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstTz(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstTz",parameterMap);
	}

	/**
	 * Master data select (UOM TYPE) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstIcutxtypeCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcutxtypeCount",parameterMap);
	}

	/**
	 * Master data select (UOM TYPE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcutxtype",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN Equipment) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstEqCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstEqCount",parameterMap);
	}

	/**
	 * Master data select (ADMIN Equipment)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstEq(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstEq",parameterMap);
	}
	
	/**
	 * ADM MulLang Select, TADMIN_MST_MULAAPMSG_DT
	 * @param parameterMap
	 * @return String
	 * @throws Exception
	 */
	public String getAdmMulLangMsg(Map parameterMap) throws Exception {
		return selectForStr("com.vinflux.adm.persistence.CommonDAO.getAdmMulLangMsg",parameterMap);
	}
	
	/**
	 * SELECT EquipmentList
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipmentList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectEquipmentList",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectStoreCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectStoreList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectStoreList",parameterMap);
	}
	
	
	/**
	 * Master data select (TADMIN_MST_AC) COUNT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstAcForRepCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAcForRepCount",parameterMap);
	}
	
	
	/**
	 * Master data select (TADMIN_MST_AC)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAcForRep(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAcForRep",parameterMap);
	}
	

	/**
	 * 유저별 푸쉬 아이디 조회  COUNT 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrXEqCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrXEqCount",parameterMap);
	}
	
	
	/**
	 * 유저별 푸쉬 아이디 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUrXEq(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstUrXEq",parameterMap);
	}
	
	/**
	 * 사용자 정보를 조회 
	 * @param sckey
	 * @return
	 * @throws Exception
	 */
	public List<HashMap<String, String>>  selectUserInfoList() throws Exception{	
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectUserInfoList", new HashMap());
	}
	
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstPortalUrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstPortalUrCount",parameterMap);
	}
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstPortalUr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstPortalUr",parameterMap);
	}	
	
	/** Master data select (SeIc) **/
	public int selectTOrderMstSeIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTOrderMstSeIcCount",parameterMap);
	}
	
	/** Master data select (SeIc) **/
	public List selectTOrderMstSeIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTOrderMstSeIc",parameterMap);
	}
	
	/**
	 * 어플리케이션 count, 조회 
	 * @param sckey
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstApListCount() throws Exception {
		Map param = null;
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstApListCount", param);
	}
	
	public List<HashMap<String, String>>  selectTadminMstApList() throws Exception{	
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTadminMstApList", new HashMap());
	}
	
	/**
	 * 업로드 정의 테이블 row count : TADMIN_MST_UPDF_HD, TADMIN_MST_UPDF_DT
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectGridCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectGridCount", parameterMap);
	}
	
	/**
	 * 업로드 정의 테이블 row count : TADMIN_MST_UPDF_HD, TADMIN_MST_UPDF_DT
	 * @param parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectGridColumns(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectGridColumns", parameterMap);
	}
	
	public List selectTAdminMstAddr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAddr",parameterMap);
	}
	
	public int selectTAdminMstAddrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstAddrCount",parameterMap);
	}
		
	public List selectTAdminMstCntry(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstCntry",parameterMap);
	}
	
	public int selectTAdminMstCntryCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstCntryCount",parameterMap);
	}

	public List selectTAdminMstZipcd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstZipcd",parameterMap);
	}
	
	public int selectTAdminMstZipcdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstZipcdCount",parameterMap);
	}

	public Integer selectTAdminMstIcInICOMCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcInICOMCount",parameterMap);
	}

	public List selectTAdminMstIcInICOM(Map parameterMap) throws Exception{
		return selectList("com.vinflux.adm.persistence.CommonDAO.selectTAdminMstIcInICOM",parameterMap);
	}	
	
}