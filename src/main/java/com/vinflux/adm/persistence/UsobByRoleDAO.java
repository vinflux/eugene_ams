package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("usobByRoleDAO")
public class UsobByRoleDAO extends AdmAbstractDAO {
	/**
	 * Role별 사용자화면 count : TADMIN_MST_ROXMEGT, TADMIN_MST_RO, TADMIN_MST_ROXAPGT, TADMIN_MST_AP, TADMIN_MST_ME, TADMIN_MST_URSC
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectUserScreenByRoleCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.UsobByRoleDAO.selectUserScreenByRoleCount", parameterMap);
	} 
	
	/**
	 * Role별 사용자화면 정보검색 : TADMIN_MST_ROXMEGT, TADMIN_MST_RO, TADMIN_MST_ROXAPGT, TADMIN_MST_AP, TADMIN_MST_ME, TADMIN_MST_URSC
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectUserScreenByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UsobByRoleDAO.selectUserScreenByRole", parameterMap);
	}
	
	/**
	 * 사용자화면 오브젝트 코드정보검색 : TADMIN_MST_ROXUSOBGT, TADMIN_MST_ADCD_DT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstAdcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UsobByRoleDAO.selectTAdminMstAdcdDt", parameterMap);
	}
	
	/**
	 * Role별 사용자화면오브젝트 정보검색: TADMIN_MST_ROXUSOBGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRoxusobgt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UsobByRoleDAO.selectTAdminMstRoxusobgt", parameterMap);
	}
	
	/**
	 * Role별 사용자화면 오브젝트 화면 정보검색: TADMIN_MST_ROXUSOBGT, TADMIN_MST_ADCD_DT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectUsobByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UsobByRoleDAO.selectUsobByRole", parameterMap);
	}
	
	public Workbook selectUsobByRole(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UsobByRoleDAO.selectUsobByRole", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * Role별 사용자화면오브젝트 정보추가: TADMIN_MST_ROXUSOBGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstRoxusobgt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.UsobByRoleDAO.insertTAdminMstRoxusobgt", parameterMap);
	}
	
	/**
	 * Role별 사용자화면오브젝트 정보삭제: TADMIN_MST_ROXUSOBGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstRoxusobgt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.UsobByRoleDAO.deleteTAdminMstRoxusobgt", parameterMap);
	}
	
	/**
	 * 엑셀다운로드를 위한 Role별 사용자화면 정보검색 : TADMIN_MST_ROXMEGT, TADMIN_MST_RO, TADMIN_MST_ROXAPGT, TADMIN_MST_AP, TADMIN_MST_ME, TADMIN_MST_URSC
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownUsobByRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.UsobByRoleDAO.selectExcelDownUsobByRole", parameterMap);
	}

	public Workbook selectExcelDownUsobByRole(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.UsobByRoleDAO.selectExcelDownUsobByRole", parameterMap, handler);
		return handler.getWorkbook();
	}

	
}