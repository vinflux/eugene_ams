package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("centerDAO")
public class CenterDAO extends AdmAbstractDAO {
	
	public int selectCenterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectCenterCount", parameterMap);
	} 
	 
	public List selectCenter(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterDAO.selectCenter", parameterMap);
	}

	public List selectPkCenter(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterDAO.selectPkCenter", parameterMap);
	}
	
	public int insertCenter(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.CenterDAO.insertCenter", parameterMap);
	}

	public int updateCenter(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CenterDAO.updateCenter", parameterMap);
	}
	
	public int mergeTtShpgLocT(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CenterDAO.mergeTtShpgLocT", parameterMap);
	}
	
	public int deleteCenter(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.CenterDAO.deleteCenter", parameterMap);
	}	

	public List selectExcelDownCenter(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterDAO.selectExcelDownCenter", parameterMap);
	}
	
	public Workbook selectExcelDownCenter(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CenterDAO.selectExcelDownCenter", parameterMap, handler);
		return handler.getWorkbook();
	}

	public int selectTAdminMstIcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstIcxctCount", parameterMap);
	}
	public int selectTAdminMstIcgrxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstIcgrxctCount", parameterMap);
	}
	public int selectTAdminMsticutxtypexctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMsticutxtypexctCount", parameterMap);
	}
	public int selectTAdminMstMulaapmsgxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstMulaapmsgxctCount", parameterMap);
	}
	public int selectTAdminMstAcxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstAcxctCount", parameterMap);
	}
	public int selectTAdminMstEqxctCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstEqxctCount", parameterMap);
	}
	public int selectTAdminMstRoxctgtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CenterDAO.selectTAdminMstRoxctgtCount", parameterMap);
	}
	
	public List getTAdminMstCtList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CenterDAO.getTAdminMstCtList", parameterMap);
	}

	
	
}