package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("roleDAO")
public class RoleDAO extends AdmAbstractDAO{
	/**
	 * Role row count 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectRoleCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.RoleDAO.selectRoleCount", parameterMap);
	} 
	
	/**
	 * Role 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleDAO.selectRole", parameterMap);
	}
	
	/**
	 * Role Pop-Up 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectForUpdateTAdminMstRo(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.RoleDAO.selectForUpdateTAdminMstRo", parameterMap);
	}
	
	
	/**
	 * Role data insert : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstRo(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.RoleDAO.insertTAdminMstRo", parameterMap);
	}	
	
	/**
	 * Role data update : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int updateTAdminMstRo(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.RoleDAO.updateTAdminMstRo", parameterMap);
	}	
	
	/**
	 * Role data delete : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstRo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.RoleDAO.deleteTAdminMstRo", parameterMap);
	}
	
	/**
	 * 엑셀 다운로드를 위한 Role 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownRole(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleDAO.selectExcelDownRole", parameterMap);
	}

	public Workbook selectExcelDownRole(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.RoleDAO.selectExcelDownRole", parameterMap, handler);
		return handler.getWorkbook();
	}
}
