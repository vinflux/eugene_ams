package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("eqmtDAO")
public class EqmtDAO extends TmsAbstractDAO {

	public List selectEqmt(Map parameterMap) {
		return selectList("com.vinflux.tms.persistence.EqmtDAO.selectEqmt", parameterMap);
	}
	
	public int selectEqmtCount(Map parameterMap) {
		return selectForInt("com.vinflux.tms.persistence.EqmtDAO.selectEqmtCount", parameterMap);
	}
	
	public List selectEqmtList(Map parameterMap) {
		return selectList("com.vinflux.tms.persistence.EqmtDAO.selectEqmtList", parameterMap);
	}
	
	public int insertEqmt(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.tms.persistence.EqmtDAO.insertEqmt", parameterMap);
	}
	
	public int updateEqmt(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.tms.persistence.EqmtDAO.updateEqmt", parameterMap);
	}
	
	public int deleteEqmt(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.EqmtDAO.deleteEqmt", parameterMap);
	}

	public List selectExcelDown(Map parameterMap) {
		return selectList("com.vinflux.tms.persistence.EqmtDAO.selectExcelDown", parameterMap);
	}


	
	public Integer selectEqmtDtlCount(Map parameterMap) {
		return selectForInt("com.vinflux.tms.persistence.EqmtDAO.selectEqmtDtlCount", parameterMap);
	}

	public List selectEqmtDtlList(Map parameterMap) {
		return selectList("com.vinflux.tms.persistence.EqmtDAO.selectEqmtDtlList", parameterMap);
	}

	public int insertEqmtDtl(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.tms.persistence.EqmtDAO.insertEqmtDtl", parameterMap);
	}
	
	public int deleteEqmtDtl(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.EqmtDAO.deleteEqmtDtl", parameterMap);
	}

	public List selectEqmtDtl(Map paramMap) {
		return selectList("com.vinflux.tms.persistence.EqmtDAO.selectEqmtDtl", paramMap);
	}

	public int updateEqmtDtl(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.tms.persistence.EqmtDAO.updateEqmtDtl", parameterMap);
	}
}
