package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("roleByUserDAO")
public class RoleByUserDAO extends AdmAbstractDAO {
	/**
	 * 사용자 count : TADMIN_MST_UR
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectUserCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.RoleByUserDAO.selectUserCount", parameterMap);
	} 
	
	/**
	 * 사용자 정보 검색 : TADMIN_MST_UR
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectUser(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectUser", parameterMap);
	}
	
	/**
	 * Role 정보 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectTAdminMstRo", parameterMap);
	}
	
	/**
	 * 사용자별 Role 정보 검색 based on TADMIN_MST_RO  : TADMIN_MST_URXROGT, TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectRoleByUser(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectRoleByUser", parameterMap);
	}
	
	
	public List selectExcelDownDtlRoleByUser(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectExcelDownDtlRoleByUser", parameterMap);
	}
	
	public Workbook selectExcelDownDtlRoleByUser(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.RoleByUserDAO.selectExcelDownDtlRoleByUser", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 사용자별 Role 정보 검색 : TADMIN_MST_URXROGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstUrxrogt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectTAdminMstUrxrogt", parameterMap);
	}
	
	/**
	 * 사용자별 Role 정보 추가 : TADMIN_MST_URXROGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstUrxrogt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.RoleByUserDAO.insertTAdminMstUrxrogt", parameterMap);
	}
	
	/**
	 * 사용자별 Role 정보 삭제 : TADMIN_MST_URXROGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstUrxrogt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.RoleByUserDAO.deleteTAdminMstUrxrogt", parameterMap);
	}
	
	/**
	 * 엑셀다운로드를 사용자 정보 검색 : TADMIN_MST_URXROGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownRoleByUser(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserDAO.selectExcelDownRoleByUser", parameterMap);
	}

	public Workbook selectExcelDownRoleByUser(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.RoleByUserDAO.selectExcelDownRoleByUser", parameterMap, handler);
		return handler.getWorkbook();
	}
	
}