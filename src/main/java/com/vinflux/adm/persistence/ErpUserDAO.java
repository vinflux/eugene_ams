package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("erpUserDAO")
public class ErpUserDAO extends AdmAbstractDAO   {

	/**
	 * ADM > 사용자 > ERP사용자관리 > 조회 
	 * @param parameterMap
	 * @return
	 */
	public List selectErpUserInfoList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.ErpUserDAO.selectErpUserInfoList", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > ERP사용자관리  > 조회 건수 
	 * @param parameterMap
	 * @return
	 */
	public int selectErpUserCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ErpUserDAO.selectErpUserCount", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > ERP사용자관리  > 사용자 등록시 중복체크 
	 * @param parameterMap
	 * @return
	 */
	public int selectErpUserCheckCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.ErpUserDAO.selectErpUserCheckCount", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > ERP사용자관리  > 사용자신규등롣 
	 * @param parameterMap
	 * @return
	 */
	public int insertErpUserInfo(Map parameterMap) {
		return insert("com.vinflux.adm.persistence.ErpUserDAO.insertErpUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > ERP사용자관리  > 사용자 등록 
	 * @param parameterMap
	 * @return
	 */
	public int updateErpUserInfo(Map parameterMap) {
		return update("com.vinflux.adm.persistence.ErpUserDAO.updateErpUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > ERP사용자관리  > 사용자 등록취소 
	 * @param parameterMap
	 * @return
	 */
	public int deleteErpUserInfo(Map parameterMap) {
		return delete("com.vinflux.adm.persistence.ErpUserDAO.deleteErpUserInfo", parameterMap);
	}
	
	/**
	 * ADM > 사용자 > 사용자그룹 > 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ErpUserDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ErpUserDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
}