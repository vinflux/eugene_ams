package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("itemGroupDAO")
public class ItemGroupDAO extends AdmAbstractDAO {
	
	/** **/
	public int selectItemGroupCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemGroupDAO.selectItemGroupCount", parameterMap);
	} 
	
	/** **/
	public int selectCountTAdminMstIcgr(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemGroupDAO.selectCountTAdminMstIcgr", parameterMap);
	}
	
	/** **/
	public int selectCountTAdminMstIcgrXCt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemGroupDAO.selectCountTAdminMstIcgrXCt", parameterMap);
	} 
	 
	/** **/
	public List selectItemGroup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectItemGroup", parameterMap);
	}

	/** **/
	public Map selectPkItemGroup(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.ItemGroupDAO.selectPkItemGroup", parameterMap);
	}
	
	/** **/
	public List selectItemGroupCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectItemGroupCode",parameterMap);
	}

	/** **/
	public int insertTAdminMstIcgr(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.ItemGroupDAO.insertTAdminMstIcgr", parameterMap);
	}
	
	public int updateTAdminMstIcgr(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.ItemGroupDAO.updateTAdminMstIcgr", parameterMap);
	}

	/** **/
	public int insertTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {    	
		return insert("com.vinflux.adm.persistence.ItemGroupDAO.insertTAdminMstMulaapmsgDt", parameterMap);
	}
	
	/** **/
	public int updateTAdminMstMulaapmsgHd(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.ItemGroupDAO.updateTAdminMstMulaapmsgHd", parameterMap);
	}

	/** **/
	public int updateTAdminMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.ItemGroupDAO.updateTAdminMstMulaapmsgDt", parameterMap);
	}
	
	/** **/
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectTAdminMstCt",parameterMap);
	}
	
	/**  **/
	public List selectTAdminMstIcgrxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectTAdminMstIcgrxct",parameterMap);
	}
	
	/** **/
	public int insertTAdminMstIcgrxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.ItemGroupDAO.insertTAdminMstIcgrxct",parameterMap);
	}

	/** **/
	public int deleteTAdminMstIcgrxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemGroupDAO.deleteTAdminMstIcgrxct",parameterMap);
	}
	
	public int deleteTAdminMstIcgr(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.ItemGroupDAO.deleteTAdminMstIcgr",parameterMap);
	}

	/** **/
	public List selectExcelDownItemGroup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectExcelDownItemGroup", parameterMap);
	}
	
	public Workbook selectExcelDownItemGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.ItemGroupDAO.selectExcelDownItemGroup", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectItemGroupCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.ItemGroupDAO.selectItemGroupCt", parameterMap);
	}
	
	/** **/
	public int selectItemGroupExcelUploadCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.ItemGroupDAO.selectItemGroupExcelUploadCount", parameterMap);
	} 	

}