package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("masterAdminCodeDAO")
public class MasterAdminCodeDAO extends AdmAbstractDAO {

	public List selectAdminCodeInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectAdminCodeInfo", parameterMap);
	}
	
	public List selectAdminCode(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectAdminCode", parameterMap);
	}
	
	public List selectDetailAdminCodeInfo(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectDetailAdminCodeInfo", parameterMap);
	}
	
	public int selectAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectAdminCodeCountInfo", parameterMap);
	}
	
	public int selectDetailAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectDetailAdminCodeCountInfo", parameterMap);
	}

	
	public int insertAdminCodeInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.MasterAdminCodeDAO.insertAdminCodeInfo", parameterMap);
	}
	
	public int insertAdminCodeInfoIF(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.MasterAdminCodeDAO.insertAdminCodeInfoIF", parameterMap);
	}
	
	public int updateAdminCodeInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.MasterAdminCodeDAO.updateAdminCodeInfo", parameterMap);
	}
	
	public int deleteAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MasterAdminCodeDAO.deleteAdminCodeInfo", parameterMap);
	}
	
	
	
	public int insertDetailAdminCodeInfo(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.MasterAdminCodeDAO.insertDetailAdminCodeInfo", parameterMap);
	}
	
	
	public int updateDetailAdminCodeInfo(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.MasterAdminCodeDAO.updateDetailAdminCodeInfo", parameterMap);
	}
	
	
	public int deleteDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MasterAdminCodeDAO.deleteItemDetailAdminCodeInfo", parameterMap);
	}
	
	public int insertDetailAdminCodeInfoIF(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.MasterAdminCodeDAO.insertDetailAdminCodeInfoIF", parameterMap);
	}
	
	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectAdminCodeInfoExcelDown", parameterMap);
	}

	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectAdminCodeInfoExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public List selectExcelDownDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectDetailAdminCodeInfoExcel", parameterMap);
	}

	public Workbook selectExcelDownDetail(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.MasterAdminCodeDAO.selectDetailAdminCodeInfoExcel", parameterMap, handler);
		return handler.getWorkbook();
	}

}
