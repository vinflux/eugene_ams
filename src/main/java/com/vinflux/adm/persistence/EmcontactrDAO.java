package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("emcontactrDAO")
public class EmcontactrDAO extends AdmAbstractDAO   {

	public List selectEmcontactrList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.EmcontactrDAO.selectEmcontactrList", parameterMap);
	}

	public int selectEmcontactrCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.EmcontactrDAO.selectEmcontactrCount", parameterMap);
	}	

	public int updateEmcontactr(Map parameterMap) throws Exception {    	   
		return update("com.vinflux.adm.persistence.EmcontactrDAO.updateEmcontactr", parameterMap);
	}
	
	public List selectExcelDownEmcontactr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EmcontactrDAO.selectExcelDownEmcontactr", parameterMap);
	}
	
	public Workbook selectExcelDownEmcontactr(Map parameterMap,
			List<Map> excelFormat)throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.EmcontactrDAO.selectExcelDownEmcontactr", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTAdminMstUrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.EmcontactrDAO.selectTAdminMstUrCount",parameterMap);
	}
	
	/**
	 * Master data select (USER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstUr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.EmcontactrDAO.selectTAdminMstUr",parameterMap);
	}

}
