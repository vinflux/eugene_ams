package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("storeevDAO")
public class StoreevDAO extends AdmAbstractDAO {
	
	public int selectStoreevCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.StoreevDAO.selectStoreevCount", parameterMap);
	}
	 
	public List selectStoreev(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevDAO.selectStoreev", parameterMap);
	}

	public List selectExcelDownStoreev(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.StoreevDAO.selectExcelDownStoreev", parameterMap);
	}

	public Workbook selectExcelDownStoreev(Map parameterMap,
			List<Map> excelFormat)  throws Exception {
		ExcelWriteHandler handler= new ExcelWriteHandler(excelFormat);  
		getSqlSession().select("com.vinflux.adm.persistence.StoreevDAO.selectExcelDownStoreev", parameterMap, handler);
		return handler.getWorkbook();
	}
}