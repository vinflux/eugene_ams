package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("setItemCodeDAO")
public class SetItemCodeDAO extends AdmAbstractDAO {

	/**
	* SET 물품 헤더 정보 조회 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeHd",parameterMap);
	}
	
	/**
	* SET 물품 헤더 정보 조회 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeExcelHD(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeExcelHD",parameterMap);
	}
	
	public Workbook selectSetItemCodeExcelHD(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeExcelHD",parameterMap,handler);
		return handler.getWorkbook();
	}

	/**
	* SET 물품 헤더 정보 count : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int selectSetItemCodeHdCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeHdCount", parameterMap);
	}
	
	public int selectSetItemCodeDtCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeDtCount", parameterMap);
	}
	
	/**
	* SET 물품  정보 count : TADMIN_MST_IC
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int selectItemCodeCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.adm.persistence.SetItemCodeDAO.selectItemCodeCount", parameterMap);
	}

	/**
	* SET 물품 상세 정보 조회 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeDt",parameterMap);
	}
	
	/**
	* SET 물품 상세 정보 조회 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeExcelDT(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeExcelDT",parameterMap);
	}
	
	public Workbook selectSetItemCodeExcelDT(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeExcelDT",parameterMap, handler);
		return handler.getWorkbook();
	}

	/**
	* SET 물품 정보 중복 확인 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public int checkSetItemCodeHdInfo(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SetItemCodeDAO.checkSetItemCodeHdInfo",parameterMap);
	}

	/**
	* SET 물품 정보 중복 확인 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkSetItemCodeDtInfo(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.SetItemCodeDAO.checkSetItemCodeDtInfo",parameterMap);
	}

	/**
	* SET 물품 헤더 정보 생성 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertSetItemCodeHd(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.SetItemCodeDAO.insertSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 생성 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertSetItemCodeDt(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.adm.persistence.SetItemCodeDAO.insertSetItemCodeDt", parameterMap);
	}

	/**
	* SET 물품 헤더 정보 갱신 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateSetItemCodeHd(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.SetItemCodeDAO.updateSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 갱신 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateSetItemCodeDt(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.SetItemCodeDAO.updateSetItemCodeDt", parameterMap);
	}

	/**
	* SET 물품 헤더 정보 삭제 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int deleteSetItemCodeHd(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.SetItemCodeDAO.deleteSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 삭제 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.adm.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int deleteSetItemCodeDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.SetItemCodeDAO.deleteSetItemCodeDt", parameterMap);
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data (Hd)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkeyHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectCtkeyHd", parameterMap);
	}
	
	/**
	 * select WM interface를 위한 센터코드 조회 data (Dt)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkeyDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.SetItemCodeDAO.selectCtkeyDt", parameterMap);
	}

	public Map selectSetItemCodeSpec(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.SetItemCodeDAO.selectSetItemCodeSpec", parameterMap);
	}

	

	

}
