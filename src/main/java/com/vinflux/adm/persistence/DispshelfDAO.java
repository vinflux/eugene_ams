package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("dispshelfDAO")
public class DispshelfDAO extends AdmAbstractDAO {
	
	public int selectDispshelfCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DispshelfDAO.selectDispshelfCount", parameterMap);
	}
	
	public int checkDispshelfCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DispshelfDAO.checkDispshelfCount", parameterMap);
	}
	 
	public List selectDispshelf(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectDispshelf", parameterMap);
	}
	
	public List selectPkDispshelf(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectPkDispshelf", parameterMap);
	}
	
	public int insertTAdminMstDispshelf(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.DispshelfDAO.insertTAdminMstDispshelf", parameterMap);
	}

	public int updateTAdminMstDispshelf(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.DispshelfDAO.updateTAdminMstDispshelf", parameterMap);
	}
	
	public int deleteTAdminMstDispshelf(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.DispshelfDAO.deleteTAdminMstDispshelf", parameterMap);
	}	

	public List selectExcelDownDispshelf(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectExcelDownDispshelf", parameterMap);
	}
	
	public Workbook selectExcelDownDispshelf(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.DispshelfDAO.selectExcelDownDispshelf", parameterMap,handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select total Dispshelf Key. count     
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int searchDispshelfCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DispshelfDAO.searchDispshelfCount", parameterMap);
	} 
	
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List searchDispshelf(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.searchDispshelf", parameterMap);
	}
	
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}

	
	
}