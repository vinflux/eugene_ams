package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("roleByUserGroupDAO")
public class RoleByUserGroupDAO extends AdmAbstractDAO {
    /**
	 * 사용자그룹 count : TADMIN_MST_URGR
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectUserGroupCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectUserGroupCount", parameterMap);
	} 
	
	/**
	 * 사용자그룹 정보 검색 : TADMIN_MST_UR
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectUserGroup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectUserGroup", parameterMap);
	}
	
	/**
	 * Role 정보 검색 : TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstRo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectTAdminMstRo", parameterMap);
	}
	
	/**
	 * 사용자그룹별 Role 정보 검색 based on TADMIN_MST_RO  : TADMIN_MST_URGRXROGT, TADMIN_MST_RO
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectRoleByUserGroup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectRoleByUserGroup", parameterMap);
	}
	
	public Workbook selectRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectRoleByUserGroup", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * 사용자그룹별 Role 정보 검색 : TADMIN_MST_URGRXROGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectTAdminMstUrgrxrogt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectTAdminMstUrgrxrogt", parameterMap);
	}
	
	/**
	 * 사용자그룹별 Role 정보 추가 : TADMIN_MST_URGRXROGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int insertTAdminMstUrgrxrogt(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.RoleByUserGroupDAO.insertTAdminMstUrgrxrogt", parameterMap);
	}
	
	/**
	 * 사용자그룹별 Role 정보 삭제 : TADMIN_MST_URGRXROGT
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTAdminMstUrgrxrogt(Map parameterMap) throws Exception {
		return delete("com.vinflux.adm.persistence.RoleByUserGroupDAO.deleteTAdminMstUrgrxrogt", parameterMap);
	}
	
	/**
	 * 엑셀다운로드를 사용자그룹 정보 검색 : TADMIN_MST_URGRXROGT
	 * @param Map parameterMap
	 * @return List
	 * @throws Exception
	 */
	public List selectExcelDownRoleByUserGroup(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectExcelDownRoleByUserGroup", parameterMap);
	}

	public Workbook selectExcelDownRoleByUserGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.RoleByUserGroupDAO.selectExcelDownRoleByUserGroup", parameterMap,handler);
		return handler.getWorkbook();
	}

	
}