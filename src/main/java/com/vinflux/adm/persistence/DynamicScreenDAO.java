package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;





import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;


import com.vinflux.framework.util.ExcelWriteHandler;

import edu.umd.cs.findbugs.gui.ConsoleLogger;

@Repository("dynamicScreenDAO")
public class DynamicScreenDAO extends AdmAbstractDAO {
	
	@Resource(name="dyccHdKeyIdGenService")
	private IdGenService dyccHdKeyIdGenService;
	
	@Resource(name="dyccDtKeyIdGenService")
	private IdGenService dyccDtKeyIdGenService;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map  selectDynamicScreenInitConfig(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenInitConfig", parameterMap);
	} 

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectDynamicScreenInitConfigDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenInitConfigDetail", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectDynamicSqlCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicSqlCount", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectDynamicSql(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicSql", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectDynamicSqlCount2(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicSqlCount2", parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectDynamicSql2(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicSql2", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectDynamicSqlSummary(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicSqlSummary", parameterMap);
	}

	public List  selectDynamicScreen(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreen", parameterMap);
	} 

	public List selectDynamicScreenDetail(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenDetail", parameterMap);
	}

	public int selectDynamicScreenCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenCount", parameterMap);
	}
	
	public int selectDynamicScreenDetailCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenDetailCount", parameterMap);
	}
	
	public int insertDynamicScreen(Map parameterMap) throws Exception {	
		//키 발급
		String prefix = (String) parameterMap.get("apkey");
		String wapkey;
		prefix = prefix.substring(0,2);
		
		if (prefix.equals("AD")){
			prefix="AM_";
		}else if (prefix.equals("IC")){
			prefix="OM_";
		}else if (prefix.equals("VI")){
			prefix="VM_";
		}else if (prefix.equals("WM")){
			prefix="WM_";
			wapkey="WMS";//apkey가 WM인 경우 다이나믹화면 생성시 에러가 발생하여 WMS로 입력 되도록 수정
			parameterMap.put("apkey", wapkey);
		}else if (prefix.equals("PM")){
			prefix="PM_";
		}
	
				
		String dyscHdKey = prefix + dyccHdKeyIdGenService.getNextStringId();	
    	
		parameterMap.put("dysc_hdkey", dyscHdKey);
		
		return insert("com.vinflux.adm.persistence.DynamicScreenDAO.insertDynamicScreen", parameterMap);
	}
	
	public int updateDynamicScreen(Map parameterMap) throws Exception {
		
		return update("com.vinflux.adm.persistence.DynamicScreenDAO.updateDynamicScreen", parameterMap);
	}
	
	public int insertDynamicScreenDetail(Map parameterMap) throws Exception {
    	
    	//마스터 헤더키
    	String dyscHdKey = (String) parameterMap.get("dysc_hdkey");
    	
    	//마스터 디테일키
		String dyscDtKey = dyccDtKeyIdGenService.getNextStringIdByKey(dyscHdKey);
    	
		parameterMap.put("dysc_dtkey", dyscDtKey);    	   	
		return insert("com.vinflux.adm.persistence.DynamicScreenDAO.insertDynamicScreenDetail", parameterMap);
	}
	
	public int updateDynamicScreenDetail(Map parameterMap) throws Exception {		
		return update("com.vinflux.adm.persistence.DynamicScreenDAO.updateDynamicScreenDetail", parameterMap);
	}
	
	public int deleteDynamicScreen(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.DynamicScreenDAO.deleteDynamicScreen", parameterMap);
	}	
	
	public int deleteDynamicScreenDetail(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.adm.persistence.DynamicScreenDAO.deleteDynamicScreenDetail", parameterMap);
	}
	
	public List selectDynamicScreenExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenExcel", parameterMap);
	}
	
	public Workbook selectDynamicScreenExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	public List selectDynamicScreenDetailExcel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenDetailExcel", parameterMap);
	}

	public Workbook selectDynamicScreenDetailExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.DynamicScreenDAO.selectDynamicScreenDetailExcel", parameterMap, handler);
		return handler.getWorkbook();
	}
}