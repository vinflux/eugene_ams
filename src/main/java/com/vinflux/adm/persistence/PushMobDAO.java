package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.idgen.IdGenService;

@Repository("pushMobDAO")
public class PushMobDAO extends AdmAbstractDAO {
	
	@Resource(name="mstPushHdIdGenService")
	private IdGenService mstPushHdIdGenService;
	
	@Resource(name="mstPushDtIdGenService")
	private IdGenService mstPushDtIdGenService;
	
	/**
	 * 푸시 해더 카운트 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectPushInfoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.PushMobDAO.selectPushInfoCount", parameterMap);
	} 
	 
	/**
	 * 푸시 해더 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPushInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.PushMobDAO.selectPushInfo", parameterMap);
	}

	
	

}