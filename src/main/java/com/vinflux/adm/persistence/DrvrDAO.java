package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("drvrDAO")
public class DrvrDAO extends AdmAbstractDAO {

	public List selectDrvr(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.DrvrDAO.selectDrvr", parameterMap);
	}

	public int selectDrvrCount(Map parameterMap) {
		return selectForInt("com.vinflux.adm.persistence.DrvrDAO.selectDrvrCount", parameterMap);
	}

	public List selectDrvrList(Map parameterMap) {
		return selectList("com.vinflux.adm.persistence.DrvrDAO.selectDrvrList", parameterMap);
	}

	public int updateDrvr(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.DrvrDAO.updateDrvr", parameterMap);
	}
	
	public int updateDrvrRegId(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.DrvrDAO.updateDrvrRegId", parameterMap);
	}	

	public List selectExcelDown(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DrvrDAO.selectExcelDown", parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.DrvrDAO.selectExcelDown", parameterMap, handler);
		return handler.getWorkbook();
	}

	public int checkValidDrvrList(List<String> parameterList) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.DrvrDAO.checkValidDrvrList", parameterList);
	}
	
	public int approveDrvr(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.DrvrDAO.approveDrvr", parameterMap);
	}

}
