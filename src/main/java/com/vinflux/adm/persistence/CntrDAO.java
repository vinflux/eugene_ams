package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("cntrDAO")
public class CntrDAO extends AdmAbstractDAO {
	
	public int selectCntrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CntrDAO.selectCntrCount", parameterMap);
	}
	
	public int checkCntrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CntrDAO.checkCntrCount", parameterMap);
	}
	 
	public List selectCntr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CntrDAO.selectCntr", parameterMap);
	}
	
	public List selectPkCntr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CntrDAO.selectPkCntr", parameterMap);
	}
	
	public int insertTAdminMstCntr(Map parameterMap) throws Exception {		
		return insert("com.vinflux.adm.persistence.CntrDAO.insertTAdminMstCntr", parameterMap);
	}

	public int updateTAdminMstCntr(Map parameterMap) throws Exception {		    
		return update("com.vinflux.adm.persistence.CntrDAO.updateTAdminMstCntr", parameterMap);
	}
	
	public int deleteTAdminMstCntr(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.CntrDAO.deleteTAdminMstCntr", parameterMap);
	}	

	public List selectExcelDownCntr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CntrDAO.selectExcelDownCntr", parameterMap);
	}
	
	public Workbook selectExcelDownCntr(Map parameterMap, List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.CntrDAO.selectExcelDownCntr", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	
	/**
	 * select WM interface를 위한 센터코드 조회 data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectCtkey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.DispshelfDAO.selectCtkey", parameterMap);
	}
	
	/**
	 * Master data select (Cntr 검색)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int searchCntrKeySearchInfoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.CntrDAO.searchCntrKeySearchInfoCount",parameterMap);
	}
	
	/**
	 * Master data select (Cntr 검색)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List searchCntrKeySearchInfo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.CntrDAO.searchCntrKeySearchInfo",parameterMap);
	}

	
}