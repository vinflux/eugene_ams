package com.vinflux.adm.persistence;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;
import com.vinflux.framework.util.ExcelWriteHandler;

@Repository("accountDAO")
public class AccountDAO extends AdmAbstractDAO {
	
	/**
	 * select total count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectAccountCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountDAO.selectAccountCount", parameterMap);
	} 
	 
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAccount(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountDAO.selectAccount", parameterMap);
	}

	/**
	 * select pk
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectPkAccount(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.AccountDAO.selectPkAccount", parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectTAdminMstAc(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.adm.persistence.AccountDAO.selectTAdminMstAc", parameterMap);
	}

	/**
	 * insert tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstAc(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.AccountDAO.insertTAdminMstAc", parameterMap);
	}

	public int mergeTtShpgLocT(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.adm.persistence.AccountDAO.mergeTtShpgLocT", parameterMap);
	}
	
	/**
	 * update tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTAdminMstAc(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.AccountDAO.updateTAdminMstAc", parameterMap);
	}


	/**
	 * delete tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstAc(Map parameterMap) throws Exception {    	    	
		return update("com.vinflux.adm.persistence.AccountDAO.deleteTAdminMstAc", parameterMap);
	}
	
	/**
	 * center select 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountDAO.selectTAdminMstCt",parameterMap);
	}
	
	/**
	 * SELECT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectPkCntTadminMstAcxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountDAO.selectPkCntTadminMstAcxct",parameterMap);
	}
	
	
	/**
	 * SELECT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAcxct(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountDAO.selectTAdminMstAcxct",parameterMap);
	}
	
	/**
	 * INSERT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTAdminMstAcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.adm.persistence.AccountDAO.insertTAdminMstAcxct",parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTAdminMstAcxct(Map parameterMap) throws Exception {
		return update("com.vinflux.adm.persistence.AccountDAO.deleteTAdminMstAcxct",parameterMap);
	}
	
	public int selectPkCntMstAcxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountDAO.selectPkCntMstAcxct",parameterMap);
	}
	/**
	 * select excel list
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownAccount(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountDAO.selectExcelDownAccount", parameterMap);
	}
	
	public Workbook selectExcelDownAccount(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		ExcelWriteHandler handler = new ExcelWriteHandler(excelFormat);
		getSqlSession().select("com.vinflux.adm.persistence.AccountDAO.selectExcelDownAccount", parameterMap, handler);
		return handler.getWorkbook();
	}
	
	/**
	 * select total Rep. count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectRepAccountCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.adm.persistence.AccountDAO.selectRepAccountCount", parameterMap);
	} 
	
	/**
	 * select paging data
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectRepAccount(Map parameterMap) throws Exception {
		return selectList("com.vinflux.adm.persistence.AccountDAO.selectRepAccount", parameterMap);
	}

}