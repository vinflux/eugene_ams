package com.vinflux.adm.common.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.persistence.CommonAddrEntityDAO;
import com.vinflux.icom.persistence.CommonAddrEntityIcomDAO;
import com.vinflux.tms.persistence.CommonAddrEntityTmsDAO;
import com.vinflux.vims.persistence.CommonAddrEntityVimsDAO;
import com.vinflux.wms.persistence.CommonAddrEntityWmsDAO;

@Service("entityAddrService")
public class EntityAddrServiceImpl implements EntityAddrService {
	
	@Resource(name = "commonAddrEntityDAO")
	private CommonAddrEntityDAO commonAddrEntityDAO;
	
	@Resource(name = "commonAddrEntityWmsDAO")
	private CommonAddrEntityWmsDAO commonAddrEntityWmsDAO;

	@Resource(name = "commonAddrEntityIcomDAO")
	private CommonAddrEntityIcomDAO commonAddrEntityIcomDAO;

	@Resource(name = "commonAddrEntityTmsDAO")
	private CommonAddrEntityTmsDAO commonAddrEntityTmsDAO;
	
	@Resource(name = "commonAddrEntityVimsDAO")
	private CommonAddrEntityVimsDAO commonAddrEntityVimsDAO;
	
	@Override
	public int getAddrId() throws Exception {
    	return commonAddrEntityDAO.getAddrId();
	}
		
	@Override
	public boolean addAddressEntitymaster(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntAdd = commonAddrEntityDAO.addAddressEntitymaster(paramMap);
			if ( cntAdd > 0) {
				result = true;
			}
		}
		
		return result;
	}

	@Override
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		paramMap.put("ADDRSTTCD", "1");
		paramMap.put("DELYN", "N");
		return commonAddrEntityDAO.queryAddressEntitymaster(paramMap);
	}

	@Override
	public boolean changeAddressEntitymaster(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			if (commonAddrEntityDAO.changeAddressEntitymaster(paramMap) > 0) {
				result = true;
			} else {
				
				result = addAddressEntitymaster(paramMap);
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAddressEntitymaster(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntDel = commonAddrEntityDAO.deleteAddressEntitymaster(paramMap);
			if ( cntDel > 0) {
				result = true;
			}
		}
		
		return result;
	}

	@Override
	public boolean addAddressEntitymasterWms(Map paramMap) throws Exception {
		boolean result = false;

		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntAdd = commonAddrEntityWmsDAO.addAddressEntitymaster(paramMap);
			if ( cntAdd > 0) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean changeAddressEntitymasterWms(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			if (commonAddrEntityWmsDAO.changeAddressEntitymaster(paramMap) > 0) {
				result = true;
			} else {
				result = addAddressEntitymasterWms(paramMap);
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAddressEntitymasterWms(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntDel = commonAddrEntityWmsDAO.deleteAddressEntitymaster(paramMap);
			if ( cntDel > 0) {
				result = true;
			}
		}
		
		return result;
	}

	@Override
	public boolean addAddressEntitymasterIcom(Map paramMap) throws Exception {
		boolean result = false;

		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntAdd = commonAddrEntityIcomDAO.addAddressEntitymaster(paramMap);
			if ( cntAdd > 0) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean changeAddressEntitymasterIcom(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			if (commonAddrEntityIcomDAO.changeAddressEntitymaster(paramMap) > 0) {
				result = true;
			} else {
				result = addAddressEntitymasterIcom(paramMap);
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAddressEntitymasterIcom(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntDel = commonAddrEntityIcomDAO.deleteAddressEntitymaster(paramMap);
			if ( cntDel > 0) {
				result = true;
			}
		}
		
		return result;
	}

	@Override
	public boolean addAddressEntitymasterTms(Map paramMap) throws Exception {
		boolean result = false;

		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntAdd = commonAddrEntityTmsDAO.addAddressEntitymaster(paramMap);
			if ( cntAdd > 0) {
				result = true;
			} 
		}
		return result;
	}

	@Override
	public boolean changeAddressEntitymasterTms(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			if (commonAddrEntityTmsDAO.changeAddressEntitymaster(paramMap) > 0) {
				result = true;
			} else {
				result = addAddressEntitymasterTms(paramMap);
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAddressEntitymasterTms(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntDel = commonAddrEntityTmsDAO.deleteAddressEntitymaster(paramMap);
			if ( cntDel > 0) {
				result = true;
			}
		}
		
		return result;
	}

	@Override
	public boolean addAddressEntitymasterVims(Map paramMap) throws Exception {
		boolean result = false;

		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			int cntAdd = commonAddrEntityVimsDAO.addAddressEntitymaster(paramMap);
			if ( cntAdd > 0) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean changeAddressEntitymasterVims(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()) {
			if (commonAddrEntityVimsDAO.changeAddressEntitymaster(paramMap) > 0) {
				result = true;
			} else {
				result = addAddressEntitymasterVims(paramMap);
			}
		}
		
		return result;
	}

	@Override
	public boolean deleteAddressEntitymasterVims(Map paramMap) throws Exception {
		boolean result = false;
		
		if (!"".equals(paramMap.get("addrid")) && !paramMap.isEmpty()){
			int cntDel = commonAddrEntityVimsDAO.deleteAddressEntitymaster(paramMap);
			if( cntDel > 0) {
				result = true;
			}
		}
		
		return result;
	}

}
