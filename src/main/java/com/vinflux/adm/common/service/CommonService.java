package com.vinflux.adm.common.service;

import java.util.List;
import java.util.Map;

public interface CommonService {

	/**
	 * 창고 변경 
	 * @param paramMap
	 * @throws Exception
	 */
	public void changeCenter (Map paramMap ) throws Exception; 
	
	/**
	 * module system config select
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectSystemConfigList (Map paramMap ) throws Exception;
	
	
	/**
	 * ADM module system config select
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectAdmSystemConfigList (Map paramMap ) throws Exception;
	
	
	/**
	 * 각 모듈에서  Config가 변경시 Admin 모듈 메모리 변경을 위한 구문
	 * @param String appCode , List list
	 * @return
	 * @throws Exception
	 */
	public void setConfigSetup(String appCode , List list) throws Exception;
	
	/**
	 * 각 모듈에서  공통 코드 변경 시 Admin 모듈 메모리 변경을 위한 구문
	 * @param String appCode , List list
	 * @return
	 * @throws Exception
	 */
	public void setCommonCodeSetup(String appCode , List list) throws Exception;

	/**
	 * module menu list select
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage(Map paramMap) throws Exception;

	/**
	 * module menu list select 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public Map selectModuleMenu(Map paramMap) throws Exception;

	public List selectModuleMenuNexacro(Map paramMap) throws Exception;

	/**
	 * menu search condition select
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public Map selectUserSearchCondtion (Map paramMap) throws Exception;

	/**
	 * popup common code select 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCommonCode (Map paramMap ) throws Exception;
	
	/**
	 * center select
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectCenterSelect (Map paramMap ) throws Exception;
	
	/**
	 * SELECT EQUIPMENT LIST 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipmentList(Map paramMap) throws Exception;
	
	
	/**
	 * convert Master & Code List to Map
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map getAdmMasternCodeMap(Map paramMap) throws Exception;
	
	
	/**
	 * 공지사항 저장 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public Map setNoticeData(Map paramMap) throws Exception;
	
	/**
	 * ADM 다국어 정보 select
	 * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	public String getAdmMulLangMsg(Map paramMap) throws Exception;
	
	/**
	 * 시스템 환경 설정의 value1 추출.
	 * @param cskey
	 * @return String
	 * @throws Exception
	 */	
	public String getAdmConfigVal(String cskey) throws Exception;
	
	/**
	 * 각 모듈 별 Code Table List 전송 
	 * @throws Exception
	 */
	public void otherModuleAdmSysConfigSetting() throws Exception;
	
	
	public Map checkDupLogin(Map param) throws Exception;
	
	
	/**
	 * SELECT selectNewBbcnMekey LIST 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectNewBbcnMekey(Map paramMap) throws Exception;
	
	
	
	public List selectMyMenuGrouping(Map paramMap) throws Exception ;
	
	
	public List selectMyGridHeaderFormat(Map paramMap) throws Exception ;
	
	/**
     * 업로드 팝업 메뉴 유형에 따른 컬럼 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectGridInfo(Map parameterMap) throws Exception;	
	
	/**
     * 우편번호 조회 공통 팝업
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectZipCode(Map parameterMap) throws Exception;
	
}