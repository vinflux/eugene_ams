package com.vinflux.adm.common.service;

import java.util.Map;

public interface EntityAddrService {
	public int getAddrId () throws Exception;
	
	public boolean addAddressEntitymaster(Map paramMap) throws Exception;

	public Map queryAddressEntitymaster(Map paramMap) throws Exception;

	public boolean changeAddressEntitymaster(Map paramMap) throws Exception;

	public boolean deleteAddressEntitymaster(Map paramMap) throws Exception;
	
	
	public boolean addAddressEntitymasterWms(Map paramMap) throws Exception;

	public boolean changeAddressEntitymasterWms(Map paramMap) throws Exception;

	public boolean deleteAddressEntitymasterWms(Map paramMap) throws Exception;

	
	public boolean addAddressEntitymasterIcom(Map paramMap) throws Exception;

	public boolean changeAddressEntitymasterIcom(Map paramMap) throws Exception;

	public boolean deleteAddressEntitymasterIcom(Map paramMap) throws Exception;

	
	public boolean addAddressEntitymasterTms(Map paramMap) throws Exception;

	public boolean changeAddressEntitymasterTms(Map paramMap) throws Exception;

	public boolean deleteAddressEntitymasterTms(Map paramMap) throws Exception;
	
	
	public boolean addAddressEntitymasterVims(Map paramMap) throws Exception;

	public boolean changeAddressEntitymasterVims(Map paramMap) throws Exception;

	public boolean deleteAddressEntitymasterVims(Map paramMap) throws Exception;	
	
}