package com.vinflux.adm.common.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.StaticManager;
import com.vinflux.adm.persistence.CommonDAO;
import com.vinflux.framework.common.AdmSysCfgStaticHashtable;
import com.vinflux.framework.common.CodeStaticHashtable;
import com.vinflux.framework.common.IcomCodeStaticHashtable;
import com.vinflux.framework.common.PmsCodeStaticHashtable;
import com.vinflux.framework.common.PortalCodeStaticHashtable;
import com.vinflux.framework.common.UrMstStaticHashtable;
import com.vinflux.framework.common.VimsCodeStaticHashtable;
import com.vinflux.framework.common.WmsCodeStaticHashtable;
import com.vinflux.framework.inf.persistence.IntfMappingDAO;
import com.vinflux.framework.inf.util.InfMappingStaticHashMap;
import com.vinflux.icom.persistence.CommonIcomDAO;
import com.vinflux.pms.persistence.CommonPmsDAO;
import com.vinflux.portal.persistence.CommonPortalDAO;
import com.vinflux.vims.persistence.CommonVimsDAO;
import com.vinflux.wms.persistence.CommonWmsDAO;

@Service("staticManager")
public class StaticManagerImpl implements StaticManager {

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(StaticManagerImpl.class);

	@Resource(name = "commonDAO")
	private CommonDAO commonDAO;
	
	@Resource(name = "commonIcomDAO")
	private CommonIcomDAO commonIcomDAO;

	@Resource(name = "commonVimsDAO")
	private CommonVimsDAO commonVimsDAO;
	
	@Resource(name = "commonWmsDAO")
	private CommonWmsDAO commonWmsDAO;
	
	@Resource(name = "commonPortalDAO")
	private CommonPortalDAO commonPortalDAO;

	@Resource(name = "commonPmsDAO")
	private CommonPmsDAO commonPmsDAO;
	
	/**
	 * interface mapping DAO
	 */
	@Resource(name = "intfMappingDAO")
	private IntfMappingDAO intfMappingDAO;


    /** commonService */
	@Resource(name = "commonService")
    private CommonService commonService;
	
	/**
	 * Mapping Table 초기화
	 * 1. Mapping 대상 테이블을 DB에서 가져옴
	 * 2. In Memory상에 해당 테이블을 로딩시킴
	 */
	@Override
	@PostConstruct
	public void init() {
		try {

			List<HashMap<String, String>> userInfoList = this.commonDAO.selectUserInfoList();
			UrMstStaticHashtable.setData(userInfoList);
			
			Integer cnt = this.commonDAO.selectTAdminMstApListCount();
			List<HashMap<String, String>> apList = null;
			
			if(cnt > 0){
				apList = this.commonDAO.selectTadminMstApList();
			}
			
			Map<String, String> apListMap = new HashMap<String, String>();
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("apkey", "ADMIN");
			
			int size = apList.size();
			for(int i=0 ; i<size ; i++){
				apListMap = (Map)apList.get(i);
				String apkey = (String) apListMap.get("apkey");
					
				if ("ADMIN".equals(apkey)) {	
					//ADMIN code Table
					List<HashMap<String, String>> list = this.commonDAO.selectTAdminMstAdcdDtList();
					CodeStaticHashtable.setData(list);
					
					//ADM system config
					List<HashMap<String, String>> sysList = this.commonService.selectSystemConfigList(paramMap);
					AdmSysCfgStaticHashtable.setData(sysList);
				}
				else if("ICOM".equals(apkey)){
					//ICOM code Table
					List<HashMap<String, String>> icomList = this.commonIcomDAO.selectTOrderMstOrcdDtList();
					IcomCodeStaticHashtable.setData(icomList);
				}
				else if("VIMS".equals(apkey)){
					//VIMS code Table
//					List<HashMap<String, String>> vimsList = this.commonVimsDAO.selectTViewMstVicdDtList();
//					VimsCodeStaticHashtable.setData(vimsList);
				}
				else if("WM".equals(apkey)){	
					//WMS code Table
					List<HashMap<String, String>> wmsList = this.commonWmsDAO.selectTWorkMstCtcdDtList();
					WmsCodeStaticHashtable.setData(wmsList);
				}
				else if("PORTAL".equals(apkey)){
					//PORTAL SYSTEM CONFIG 
					List<HashMap<String, String>> portalList = this.commonPortalDAO.selectTPortalMstVicdDtList(paramMap);
					PortalCodeStaticHashtable.setData(portalList);
				}
				else if("PMS".equals(apkey)){
					// PMS SYSTEM CONFIG 
					List<HashMap<String, String>> pmsList = this.commonPmsDAO.selectTPmsMstVicdDtList(paramMap);
					PmsCodeStaticHashtable.setData(pmsList);
				}
				else{
					;
				}	
//				// Interface Test Data Setting
//				List<Map<String, String>> intfValidList = this.intfMappingDAO.selectMappingData(paramMap);
//				InfMappingStaticHashMap.setData(intfValidList);
			}
			
			LOG.debug("### WMS CTCD SIZE=>" + WmsCodeStaticHashtable.getDataSize());
			this.getMemorySize();
	        
		}
		catch (SQLException e) {
			LOG.error("Mapping Table을 초기화 하지 못하였습니다.", e);
		}
		catch (Exception e) {
			LOG.error("Mapping Table을 초기화 하지 못하였습니다.", e);
		}
	}

	@Override
	public void reload() {
		init();
	}
	
	public void getMemorySize() {
		int mb = 1024*1024;
        
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
         
        //System.out.println("##### Heap utilization statistics [MB] #####");
        LOG.debug("##### Heap utilization statistics [MB] #####");
         
        //Print used memory
//        System.out.println("Used Memory:"
//            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
        LOG.debug("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
 
        //Print free memory
//        System.out.println("Free Memory:"
//            + runtime.freeMemory() / mb);
        LOG.debug("Free Memory:"
                + runtime.freeMemory() / mb);
        
        //Print total available memory
        //System.out.println("Total Memory:" + runtime.totalMemory() / mb);
        LOG.debug("Total Memory:" + runtime.totalMemory() / mb);
 
        //Print Maximum available memory
        //System.out.println("Max Memory:" + runtime.maxMemory() / mb);
        LOG.debug("Max Memory:" + runtime.maxMemory() / mb);
	}
}
