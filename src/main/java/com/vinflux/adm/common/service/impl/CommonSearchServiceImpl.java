/**
 *
 */
package com.vinflux.adm.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonSearchService;
import com.vinflux.adm.persistence.CommonSearchAdmDAO;
import com.vinflux.adm.persistence.CommonSearchDAO;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;

/**
 * @author goldbug
 *
 */
@Service("commonSearchService")
public class CommonSearchServiceImpl implements CommonSearchService {

    /** LOG */
    protected static final Log LOG = LogFactory.getLog(CommonServiceImpl.class);

    protected final static int COMMONCODE    =  10;
    //protected final int LOGCD       =  20; 센터 키 기존에 사용하던 이름 변경
    protected final static int CTKEY         =  20;
    protected final static int CUSTCD        =  30;
    protected final static int URKEY         =  40;
    protected final static int PLANID        =  50;
    protected final static int CARRCD        =  60;
    protected final static int ITEMGRPCD     =  70;
    protected final static int ITEMCD        =  80;
    protected final static int SHPGLOC       =  90;
    protected final static int CHARGECD      = 100;
    protected final static int EQUIPTYPCD    = 110;
    protected final static int RGNCD          = 120;
    protected final static int SERVICECD     = 130;
    protected final static int RTGRP         = 140;
    protected final static int ROUT          = 150;
    protected final static int DRVR          = 160;
    protected final static int CNCYTYP       = 170;
    protected final static int OWKEY        = 180;

    @Resource(name="commonSearchDAO")
    private CommonSearchDAO commonSearchDAO;

    @Resource(name="commonSearchAdmDAO")
    private CommonSearchAdmDAO commonSearchAdmDAO;
    //CURRTYP
    
    /**
     * selectPopupSearch
     *
     * SEARCH_TYPE :    10    NAME : COMMONCODE                   refTable : TT_MST_TMCD_HD
     * SEARCH_TYPE :    20    NAME : CTKEY		                  refTable : TT_MST_CT
     * SEARCH_TYPE :    30    NAME : CUSTCD                       refTable : TT_CUST_T
     * SEARCH_TYPE :    40    NAME : URKEY                        refTable : TADMIN_MST_UR
     * SEARCH_TYPE :    50    NAME : PLANID                       refTable : TT_PLAN_T
     * SEARCH_TYPE :    60    NAME : CARRCD                       refTable : TT_CARR_T
     * SEARCH_TYPE :    70    NAME : ITEMGRPCD                    refTable : TT_ITEM_GRP_T
     * SEARCH_TYPE :    80    NAME : ITEMCD                       refTable : TT_ITEM_MST_T
     * SEARCH_TYPE :    90    NAME : SHPGLOC                      refTable : TT_SHPG_LOC_T
     * SEARCH_TYPE :   100    NAME : CHARGECD                     refTable : TT_CHRG_T
     * SEARCH_TYPE :   110    NAME : EQUIPTYPCD                   refTable : TT_EQUIP_T
     * SEARCH_TYPE :   120    NAME : RGNCD                        refTable : TT_REGION_T
     * SEARCH_TYPE :   130    NAME : SERVICECD                    refTable : TT_SRVC_T
     * SEARCH_TYPE :   140    NAME : RTGRP                        refTable : TT_RT_GRP_HD
     * SEARCH_TYPE :   150    NAME : ROUT                         refTable : TT_RT_MST_HD
     * SEARCH_TYPE :   160    NAME : DRVR                         refTable : TT_DRVR_T
     * SEARCH_TYPE :   170    NAME : CNCY                         refTable : TT_CURCNCY_T
     * SEARCH_TYPE :   180    NAME : OWKEY                        refTable : TT_MST_OW
     */
    @Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
    public Map selectCommonSearch (Map paramMap ) throws Exception {
        String searchid = (String)paramMap.get("searchid");

        HttpServletRequest req = HttpUtil.getCurrentRequest();
        SessionVO sessionVo = SessionManager.getSessionVO(req);
        String lakey = sessionVo.getLaKey();
        paramMap.put("lakey", lakey);
        String type = (String)paramMap.get("type");

        List rtnList = null;
        Integer count = 0;

        Map rtnMap = new HashMap();
        switch (Integer.parseInt(searchid)) {
        case COMMONCODE: // 10
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtMstTmcdDtCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtMstTmcdDtList(paramMap);
            rtnMap.put("FIELD", "ADCD_DTKEY;ADCD_DTNAME;ADCD_DTVALUE");
            break;

        case CTKEY: // 20
            if (!"combo".equals(type)) {
//                count = commonSearchAdmDAO.selectTtMstCtCount(paramMap);
                count = commonSearchDAO.selectTtMstCtCount(paramMap);
            }
//            rtnList = commonSearchAdmDAO.selectTtMstCtList(paramMap);
            rtnList = commonSearchDAO.selectTtMstCtList(paramMap);
            rtnMap.put("FIELD", "CTKEY;CTNM;CTTYPE");
            break;

        case CUSTCD: // 30
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtCustTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtCustTList(paramMap);
            rtnMap.put("FIELD", "CUSTCD;CUSTNM");
            break;

        case URKEY: // 40
            if (!"combo".equals(type)) {
                count = commonSearchAdmDAO.selectTAdminMstUrCount(paramMap);
//                count = commonSearchDAO.selectTAdminMstUrCount(paramMap);
            }
            rtnList = commonSearchAdmDAO.selectTAdminMstUrList(paramMap);
//            rtnList = commonSearchDAO.selectTAdminMstUrList(paramMap);
            rtnMap.put("FIELD", "URKEY;URNM");
            break;

        case PLANID: // 50
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtPlanTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtPlanTList(paramMap);
            rtnMap.put("FIELD", "PLANID;PLANDESC");
            break;

        case CARRCD: // 60
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtCarrTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtCarrTList(paramMap);
            rtnMap.put("FIELD", "CARRCD;CARRNM");
            break;

        case ITEMGRPCD: // 70
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtItmGrpTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtItmGrpTList(paramMap);
            rtnMap.put("FIELD", "ITEMGRPCD;ITEMGRPDESC");
            break;

        case ITEMCD: // 80
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtItmMstTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtItmMstTList(paramMap);
            rtnMap.put("FIELD", "ICKEY; ICNAME");
            break;

        case SHPGLOC: // 90
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtShpgLocTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtShpgLocTList(paramMap);
            rtnMap.put("FIELD", "SHPGLOCID;SHPGLOCCD;SHPGLOCNM;ADDRID");
            break;

        case CHARGECD: // 100
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtChrgTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtChrgTList(paramMap);
            rtnMap.put("FIELD", "CHARGECD;CHARGEDESC");
            break;

        case EQUIPTYPCD: // 110
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtEqmtTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtEqmtTList(paramMap);
            rtnMap.put("FIELD", "EQUIPTYPCD;EQUIPTYPDESC");
            break;

        case RGNCD: // 120
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtZoneTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtZoneTList(paramMap);
            rtnMap.put("FIELD", "RGNCD;RGNDESC");
            break;

        case SERVICECD: // 130
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtServiceTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtServiceTList(paramMap);
            rtnMap.put("FIELD", "SERVICECD;SERVICEDESC");
            break;

        case RTGRP: // 140
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtRtgrpHdTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtRtgrpHdTList(paramMap);
            rtnMap.put("FIELD", "RTGRPCD;RTGRPDESC");
            break;

        case ROUT: // 150
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtRtMstHdTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtRtMstHdTList(paramMap);
            rtnMap.put("FIELD", "RTSYSID;ROUTNO;ROUTDESC");
            break;

        case DRVR: // 160
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtDrvrTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtDrvrTList(paramMap);
            rtnMap.put("FIELD", "DRVNO;DRVNM;MOBILENO");
            break;
            
        case CNCYTYP: // 170
        	if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtCncyTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtCncyTList(paramMap);
            rtnMap.put("FIELD", "CURRTYP;CURRCD;CURRDESC");
            break;
            
        case OWKEY: // 180
            if (!"combo".equals(type)) {
                count = commonSearchDAO.selectTtOwTCount(paramMap);
            }
            rtnList = commonSearchDAO.selectTtOwTList(paramMap);
            rtnMap.put("FIELD", "CUSTCD;CUSTNM");
            break;

        default:
            rtnList = new ArrayList();
            break;
        }

        rtnMap.put("list", rtnList);
        rtnMap.put("count", Integer.toString(count));

        return rtnMap;
    }

}
