package com.vinflux.adm.common.service;

public interface StaticManager {
	public void init();
	public void reload();
}
