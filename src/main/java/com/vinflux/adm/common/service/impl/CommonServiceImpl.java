package com.vinflux.adm.common.service.impl;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.persistence.CommonDAO;
import com.vinflux.adm.persistence.MyGridHeaderFormatDAO;
import com.vinflux.adm.persistence.MyMenuSetupDAO;
import com.vinflux.adm.persistence.UserByRoleAdmDAO;
import com.vinflux.adm.session.service.LoginVO;
import com.vinflux.bbs.persistence.CommonBbsDAO;
import com.vinflux.cms.persistence.CommonCmsDAO;
import com.vinflux.fis.persistence.CommonFisDAO;
import com.vinflux.framework.client.amf.AmfClient;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.AdmSysCfgStaticHashtable;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.common.IcomCodeStaticHashtable;
import com.vinflux.framework.common.NoticeCodeStaticHashtable;
import com.vinflux.framework.common.WmsCodeStaticHashtable;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.ibs.persistence.CommonIbsDAO;
import com.vinflux.icom.persistence.CommonIcomDAO;
import com.vinflux.kpi.persistence.CommonKpiDAO;
import com.vinflux.mdm.persistence.CommonMdmDAO;
import com.vinflux.pms.persistence.CommonPmsDAO;
import com.vinflux.portal.persistence.CommonPortalDAO;
import com.vinflux.tms.persistence.CommonTmsDAO;
import com.vinflux.vims.persistence.CommonVimsDAO;
import com.vinflux.wms.persistence.CommonWmsDAO;

@Service("commonService")
@SuppressWarnings({"rawtypes","unchecked"})
public class CommonServiceImpl implements CommonService {

	@Resource(name = "commonDAO")
	private CommonDAO commonDAO;

	@Resource(name = "commonWmsDAO")
	private CommonWmsDAO commonWmsDAO;

	@Resource(name = "commonIcomDAO")
	private CommonIcomDAO commonIcomDAO;

	@Resource(name = "commonTmsDAO")
	private CommonTmsDAO commonTmsDAO;

	@Resource(name = "commonVimsDAO")
	private CommonVimsDAO commonVimsDAO;

	@Resource(name = "commonKpiDAO")
	private CommonKpiDAO commonKpiDAO;

	@Resource(name = "commonPortalDAO")
	private CommonPortalDAO commonPortalDAO;

	@Resource(name = "commonPmsDAO")
	private CommonPmsDAO commonPmsDAO;
	
	@Resource(name = "commonBbsDAO")
	private CommonBbsDAO commonBbsDAO;
	
	@Resource(name = "commonMdmDAO")
	private CommonMdmDAO commonMdmDAO;
	
	@Resource(name = "commonCmsDAO")
	private CommonCmsDAO commonCmsDAO;
	
	@Resource(name = "commonIbsDAO")
	private CommonIbsDAO commonIbsDAO;
	
	@Resource(name = "commonFisDAO")
	private CommonFisDAO commonFisDAO;
	
//	@Resource(name = "commonMsDAO")
//	private CommonMsDAO commonMsDAO;

	@Resource(name="userByRoleAdmDAO")
    private UserByRoleAdmDAO userByRoleAdmDAO;
	
	@Resource(name="myMenuSetupDAO")
	private MyMenuSetupDAO myMenuSetupDAO;
	
	@Resource(name="myGridHeaderFormatDAO")
	private MyGridHeaderFormatDAO myGridHeaderFormatDAO;
	
	@Resource(name = "commonPortalDAO")
	private CommonPortalDAO commonPtlDAO;
	
    @Resource(name="amfClient")
    private AmfClient amfClient;
    
	protected static final Log LOG = LogFactory.getLog(CommonServiceImpl.class);

	/**
	 * 창고 변경
	 */
	@Override
	public void changeCenter(Map paramMap) throws Exception {

		String urkey = MapUtil.getStr(paramMap, "urkey");
		String ctkey = MapUtil.getStr(paramMap, "ctkey");
		String ctKey_desc = MapUtil.getStr(paramMap, "ctkey_desc");

		LOG.debug("#### WMS Session Center change ####");
		LOG.debug("Center => " + ctkey);
		LOG.debug("Center Name => " + ctKey_desc);

		// TODO Auto-generated method stub
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req, urkey);

		sessionVO.setCtKey(ctkey);
		sessionVO.setCtKey_desc(ctKey_desc);

		SessionManager.setUserInfo(req, sessionVO, "Y");
	}

	/**
	 * 공지사항 저장
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map setNoticeData(Map paramMap) throws Exception {
		//Notice setting 
		Map noticeMap = this.commonWmsDAO.selectBoardNotice();
		if (noticeMap != null) {
//			String quest_content = MapUtil.getStr(noticeMap, "quest_content");
			String qa_title = MapUtil.getStr(noticeMap, "qa_title");
			String updatedate = MapUtil.getStr(noticeMap, "updatedate");
			NoticeCodeStaticHashtable.setData(qa_title, updatedate);
		}
		else {
			NoticeCodeStaticHashtable.setData("", "");
		}
		return paramMap;
	}

	/**
	 * module system config select
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectSystemConfigList(Map paramMap) throws Exception {
		List list = new ArrayList();
		String apkey = (String) paramMap.get("apkey");
		// ADMIN

		if ("ADMIN".equals(apkey)) {
			list = commonDAO.selectAdminConfigList(paramMap);
			// WMS 
		}
		else if ("WM".equals(apkey)) {
			list = commonWmsDAO.selectWmsConfigList(paramMap);
			// ICOM 
		}
		else if ("ICOM".equals(apkey)) {
			list = commonIcomDAO.selectIcomConfigList(paramMap);
		}
		else if ("TMS".equals(apkey)) {
            list = commonTmsDAO.selectTmsConfigList(paramMap);
        }
		else if ("PTL".equals(apkey)) {
			list = commonPtlDAO.selectPtlConfigList(paramMap);
		}
		else if ("IBS".equals(apkey)) {
			list = commonIbsDAO.selectIbsConfigList(paramMap);
			// ICOM 
		}
		List sysList = commonDAO.selectSystemConfigList(paramMap);

		for (int i = 0; i < sysList.size(); i++) {
			list.add(sysList.get(i));
		}

		return list;
	}
	
	
	
	@Override
	public List selectAdmSystemConfigList(Map paramMap) throws Exception {

		List list = commonDAO.selectAdminConfigList(paramMap);
		
		return list;
	
	}	
		

	/**
	 * 각 모듈에서 Config가 변경시 Admin 모듈 메모리 변경을 위한 구문
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void setConfigSetup(String appCode, List list) throws Exception {
		if (EnvConstant.APP_WMS.equals(appCode)) {
			//
			LOG.debug("APP WMS");
		}
		else if (EnvConstant.APP_ICOM.equals(appCode)) {
			//
			LOG.debug("APP ICOM");
		}
	}

	/**
	 * 각 모듈에서 Code 변경시 Admin 모듈 메모리 변경을 위한 구문
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void setCommonCodeSetup(String appCode, List list) throws Exception {
		if (EnvConstant.APP_ICOM.equals(appCode)) {
			IcomCodeStaticHashtable.icomMappingTable.clear();
			IcomCodeStaticHashtable.setData(list);
		}
		else if (EnvConstant.APP_WMS.equals(appCode)) {
			WmsCodeStaticHashtable.wmsMappingTable.clear();
			WmsCodeStaticHashtable.setData(list);
		}
	}

	@Override
	public List selectAppMessage(Map paramMap) throws Exception {
		List list = new ArrayList();
		String activeApp = (String) paramMap.get("activeApp");
		String ctkey = (String) paramMap.get("ctkey");
		// ADMIN
		if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
			list = commonDAO.selectAppMessage(paramMap);

			// WMS 
		}
		else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {

			if (ctkey == null || "".equals(ctkey)) {
				List centerList = commonWmsDAO.selectCenterSelect(paramMap);
				if (centerList != null && !centerList.isEmpty() ) {
					Map map = (Map) centerList.get(0);
					paramMap.put("ctkey", map.get("ctkey"));
				}
			}
			list = commonWmsDAO.selectAppMessage(paramMap);

			// ICOM 
		}
		else if ("ICOM".equals(activeApp)) {
			list = commonIcomDAO.selectAppMessage(paramMap);
		}
		else if ("TMS".equals(activeApp)) {
			list = commonTmsDAO.selectAppMessage(paramMap);
		}
		else if ("VIMS".equals(activeApp)) {
			list = commonVimsDAO.selectAppMessage(paramMap);
		}
		else if ("KPI".equals(activeApp)) {
			list = commonKpiDAO.selectAppMessage(paramMap);
		} 
		else if ("PORTAL".equals(activeApp) || "PTL".equals(activeApp)) {
			list = commonPortalDAO.selectAppMessage(paramMap);
		}
		else if ("PMS".equals(activeApp)) {
			list = commonPmsDAO.selectAppMessage(paramMap);
		}
		else if ("BBS".equals(activeApp)) {
			list = commonBbsDAO.selectAppMessage(paramMap);
		}
		else if ("MDM".equals(activeApp)) {
			list = commonMdmDAO.selectAppMessage(paramMap);
		}
		else if ("FIS".equals(activeApp)) {
			list = commonFisDAO.selectAppMessage(paramMap);
		}
		else if ("CMS".equals(activeApp)) {
			list = commonCmsDAO.selectAppMessage(paramMap);
		}
		else if ("IBS".equals(activeApp)) {
			list = commonIbsDAO.selectAppMessage(paramMap);
		}
		return list;
		
	}

	/**
	 * Module menu list select & make json structure
	 */
	@Override
	public Map selectModuleMenu(Map paramMap) throws Exception {

	    List list = new ArrayList();
		int cntRole = -1;

		// Count role first.
		cntRole = commonDAO.selectModuleMenuByRoleCount(paramMap);

		// select menu based on role count.
		if (cntRole <= 0) {
			list = commonDAO.selectModuleMenu(paramMap);
		}
		else {
			list = commonDAO.selectModuleMenuByRole(paramMap);
		}

		// NULL RETURN 
		if (list == null || list.isEmpty()) { return new HashMap(); }

		int listSize = list.size();
		Map menuMap = new HashMap();
		// root Setting 
		for (int i = 0; listSize > i; i++) {
			Map row = (Map) list.get(i);
			String uppermekey = (String) row.get("uppermekey");
			if ("0".equals(uppermekey)) {
				menuMap.putAll(row);
				String isleaf = (String) row.get("isleaf");
				if ("N".equals(isleaf)) {
					menuMap.put("children", new ArrayList());	// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
				}
				break;
			}
		}

		String mekey = (String) menuMap.get("mekey");

		// 1Depth Setting
		for (int i = 0; listSize > i; i++) {
			Map row = (Map) list.get(i);
			String uppermekey = (String) row.get("uppermekey");
			if (mekey.equals(uppermekey)) {
				List rootChildrenList = (List) menuMap.get("children");
				rootChildrenList.add(row);
			}
			String isleaf = (String) row.get("isleaf");
			if ("N".equals(isleaf)) {
				row.put("children", new ArrayList());		// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
			}
		}

		// 2Depth Setting
		List rootChildrenList = (List) menuMap.get("children");
		for (int i = 0; rootChildrenList.size() > i; i++) {
			Map depth1Map = (Map) rootChildrenList.get(i);

			String meKey = (String) depth1Map.get("mekey");
			for (int j = 0; listSize > j; j++) {
				Map row = (Map) list.get(j);
				String uppermekey = (String) row.get("uppermekey");
				if (meKey.equals(uppermekey)) {
					List childrenList = (List) depth1Map.get("children");
					childrenList.add(row);
					String isleaf = (String) row.get("isleaf");
					if ("N".equals(isleaf)) {
						row.put("children", new ArrayList());		// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
					}
				}
			}
		}

		// 3Depth Setting
		for (int i = 0; rootChildrenList.size() > i; i++) {
			Map depth1Map = (Map) rootChildrenList.get(i);

			List depth2List = (List) depth1Map.get("children");
			if (depth2List != null && !depth2List.isEmpty() ) {
				for (int j = 0; depth2List.size() > j; j++) {
					Map depth2Row = (Map) depth2List.get(j);
					String depth2Mekey = (String) depth2Row.get("mekey");

					String isleaf = (String) depth2Row.get("isleaf");
					if ("Y".equals(isleaf)) {
						continue;
					}

					for (int z = 0; listSize > z; z++) {
						Map row = (Map) list.get(z);
						String uppermekey = (String) row.get("uppermekey");
						if (uppermekey.equals(depth2Mekey)) {
							List childrenList = (List) depth2Row.get("children");
							childrenList.add(row);
						}
					}
				}
			}
		}

		return menuMap;
	}

	@Override
	public List selectModuleMenuNexacro(Map paramMap) throws Exception {

	    List list = new ArrayList();
		int cntRole = -1;
		
		String eqtype = (String) paramMap.get("eqtype");
		if(StringUtil.equals(eqtype, "30")){
			list = commonDAO.selectModuleMenuNexacro(paramMap);
		}else{
			list = commonDAO.selectModuleMenuByRoleNexacro(paramMap);
		}
/*		// Count role first.
		cntRole = commonDAO.selectModuleMenuByRoleCount(paramMap);

		// select menu based on role count.
		if (cntRole <= 0) {
			list = commonDAO.selectModuleMenuNexacro(paramMap);
		}
		else {
			list = commonDAO.selectModuleMenuByRoleNexacro(paramMap);
		}*/

		return list;
	}
	
	/**
	 * menu search condition select
	 */

	@Override
	public Map selectUserSearchCondtion(Map paramMap) throws Exception {
		Map returnMap = new HashMap();
		String activeApp = (String) paramMap.get("activeApp");
		// get SearchCondition 
		List list = commonDAO.selectUserSearchCondtion(paramMap);
		returnMap.put("SRCH_CONDITION", list);

		// get SearchCondition2
		paramMap.put("tabidx", "tab2");
		// get SearchCondition2 
		List list2 = commonDAO.selectUserSearchCondtion(paramMap);
		returnMap.put("SRCH_CONDITION2", list2);
		LOG.debug("paramMap:::"+paramMap);
		
		// GET COMBO CODE LIST
		Map searchMap = new HashMap();
		searchMap.put("lakey", MapUtil.getStr(paramMap, "lakey"));

		List codeList;
		if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp) || "PTL".equals(activeApp) || "KPI".equals(activeApp) ) {
			codeList = commonDAO.selectTAdminMstAdcdDtLakeyList(searchMap);
		}
		else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
			String ctkey = MapUtil.getStr(paramMap, "ctkey");
			searchMap.put("ctkey", ctkey);
			codeList = commonWmsDAO.selectTWorkMstCtcdDtCtkeyList(searchMap);
		}
		else if ("TMS".equals(activeApp)) {
			codeList = commonTmsDAO.selectTmMstTmcdDtList(searchMap);
		}
		else if ("PMS".equals(activeApp)) {
			codeList = commonPmsDAO.selectTPmsMstPtcdDtLakeyList(searchMap);
		}else if ("VIMS".equals(activeApp)) {
			codeList = commonVimsDAO.selectTViewMstVicdDtkeyList(searchMap);
		}else if ("CMS".equals(activeApp)) {
			codeList = commonCmsDAO.selectTitClassMasterList(searchMap);
		}else if ("IBS".equals(activeApp)) {
			codeList = commonIbsDAO.selectTitClassMasterList(searchMap);
//		}else if ("KPI".equals(activeApp)) {
//			codeList = commonKpiDAO.selectTKpiMstKpcdDtkeyList(searchMap);
		}else {
			codeList = commonIcomDAO.selectTOrderMstOrcdDtLakeyList(searchMap);
		}

		returnMap.put("CODE_LIST", codeList);
		return returnMap;
	}

	/**
	 * Get Search Condition combo data
	 * 
	 * @param list private void getSearchConditionComoboData (List list , String activeApp ) throws Exception { if (list
	 *            != null && list.size() > 0 ) { int size = list.size(); for (int i=0 ; size > i ; i++ ) { Map map =
	 *            (Map)list.get(i); String ussc_dttype = (String)map.get("ussc_dttype"); if
	 *            ("COMBOBOX".equals(ussc_dttype)) { String searchId = (String)map.get("searchid"); String usscDtKey =
	 *            (String)map.get("ussc_dtkey"); String ctkey = (String)map.get("ctkey"); String [] searchpopupidArr =
	 *            searchId.split(";"); if (searchpopupidArr.length == 1 ) { Map comboParamMap = new HashMap();
	 *            comboParamMap.put("searchid", searchId); comboParamMap.put("activeApp", activeApp);
	 *            comboParamMap.put("type", "combo"); Map comboRtnMap = selectCommonCode(comboParamMap);
	 *            map.put("COMBO_"+usscDtKey, comboRtnMap.get("list")); } else if (searchpopupidArr.length > 1 ) {
	 *            String id = searchpopupidArr[0]; String hdkey = searchpopupidArr[1]; Map comboParamMap = new
	 *            HashMap(); comboParamMap.put("searchid", id); if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp) ) {
	 *            comboParamMap.put("adcd_hdkey", hdkey); } else if ("ICOM".equals(activeApp) ) {
	 *            comboParamMap.put("orcd_hdkey", hdkey); } else if ("WM".equals(activeApp) || "WMS".equals(activeApp)){
	 *            comboParamMap.put("ctcd_hdkey", hdkey); } comboParamMap.put("activeApp", activeApp);
	 *            comboParamMap.put("type", "combo"); Map comboRtnMap = selectCommonCode(comboParamMap);
	 *            map.put("COMBO_"+usscDtKey, comboRtnMap.get("list")); } } } } }
	 */

	/**
	 * selectPopupCommonCode 공통 코드 select ADCD_DTKEY : 10 NAME : APPLICATION refTable : TADMIN_MS_AP ADCD_DTKEY : 20
	 * NAME : ADMIN CODE refTable : TADMIN_MST_ADCD_HD ADCD_DTKEY : 30 NAME : MULTI LANGUAGE APPLICATION MESSAGE
	 * refTable : TADMIN_MST_MULAAPMSG_HD ADCD_DTKEY : 40 NAME : ACCOUNT refTable : TADMIN_MST_AC ADCD_DTKEY : 50 NAME :
	 * OWNER refTable : TADMIN_MST_OW ADCD_DTKEY : 60 NAME : CENTER refTable : TADMIN_MST_CT ADCD_DTKEY : 70 NAME : USER
	 * GROUP refTable : TADMIN_MST_URGR ADCD_DTKEY : 80 NAME : USER refTable : TADMIN_MST_UR ADCD_DTKEY : 90 NAME :
	 * ITEMCODE GROUP refTable : TADMIN_MST_ICGR ADCD_DTKEY : 100 NAME : ITEMCODE UNIT refTable : TADMIN_MST_ICUT
	 * ADCD_DTKEY : 110 NAME : ITEMCODE refTable : TADMIN_MST_IC ADCD_DTKEY : 120 NAME : SUBSTITUTION refTable :
	 * TADMIN_MST_ICSB ADCD_DTKEY : 130 NAME : USER SCREEN SEARCH CONDITION refTable : TADMIN_MST_USSC_HD ADCD_DTKEY :
	 * 140 NAME : USER SCREEN refTable : TADMIN_MST_URSC ADCD_DTKEY : 150 NAME : MENU refTable : TADMIN_MST_ME
	 * ADCD_DTKEY : 160 NAME : ROLE refTable : TADMIN_MST_RO ADCD_DTKEY : 170 NAME : SYSTEM CONFIG refTable :
	 * TADMIN_SET_SC ADCD_DTKEY : 180 NAME : TIME ZONE refTable : TADMIN_MST_TZ ADCD_DTKEY : 190 NAME : AREA refTable :
	 * TWORK_MST_AR ADCD_DTKEY : 200 NAME : LOCATION refTable : TWORK_MST_LC ADCD_DTKEY : 210 NAME : ULC refTable :
	 * TWORK_MST_ULC ADCD_DTKEY : 220 NAME : INVENTORY ATTRIBUTE refTable : TWORK_INFO_IVAT
	 */
	@Override
	public Map selectCommonCode(Map paramMap) throws Exception {
		String searchid = (String) paramMap.get("searchid");
		String activeApp = (String) paramMap.get("activeApp");
		LOG.debug("searchid : " +searchid);
		LOG.debug("activeApp : " +activeApp);
		String device = "";
		if(paramMap.get("device") != null){
			device = (String) paramMap.get("device");
		}

		// lakey setting 
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVo = SessionManager.getSessionVO(req);
		String lakey = sessionVo.getLaKey();
		paramMap.put("lakey", lakey);
		String type = (String) paramMap.get("type");

		List rtnList = null;
		Integer count = 0;

		Map rtnMap = new HashMap();
		
		switch (Integer.parseInt(searchid)) {
			case 10:
				count = commonDAO.selectTAdminMstApCount(paramMap);
				rtnList = commonDAO.selectTAdminMstAp(paramMap);
				rtnMap.put("FIELD", "APKEY;APNAME");
				break;
		    
				//WINFOOD 용 어플리케이션 관리 부분 - WINFOOD 160627.twyun
			case 11:
				count = commonDAO.selectTAdminMstApCountAll(paramMap);
				rtnList = commonDAO.selectTAdminMstApAll(paramMap);
				rtnMap.put("FIELD", "APKEY;APNAME");
				break;

			case 20:
				//ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)|| "PTL".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonDAO.selectTAdminMstAdcdDtCount(paramMap);
					}
					rtnList = commonDAO.selectTAdminMstAdcdDt(paramMap);
					rtnMap.put("FIELD", "ADCD_DTKEY;ADCD_DTNAME;ADCD_DTVALUE;ADCD_HDKEY");
					// ICOM
				}
				else if ("ICOM".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIcomDAO.selectTOrderMstOrcdDtCount(paramMap);
					}
					rtnList = commonIcomDAO.selectTOrderMstOrcdDt(paramMap);
					rtnMap.put("FIELD", "ORCD_DTKEY;ORCD_DTNAME;ORCD_DTVALUE;ORCD_HDKEY");
					
					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					
					// 2015.12.22  모바일일 경우 분기 처리
					if( "MOBILE".equals(device)){
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstCtcdDtMobileCount(paramMap);
						}
						rtnList = commonWmsDAO.selectTWorkMstCtcdDtMobile(paramMap);
						rtnMap.put("FIELD", "CTCD_DTKEY;CTCD_DTNAME;COMMENTS");						
					} else {
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstCtcdDtCount(paramMap);
						}
						
						rtnList = commonWmsDAO.selectTWorkMstCtcdDt(paramMap);
						rtnMap.put("FIELD", "CTCD_DTKEY;CTCD_DTNAME;CTCD_DTVALUE;COMMENTS");
					}
					
					// TMS
					// 2014.01.01 TMS는 activeApp code를 TMS 로 사용하고 있는데 TM 으로 들어가 있어서 TMS로 수정
				}
				else if ("TMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonTmsDAO.selectTtMstTmcdDtCount(paramMap);
					}
					rtnList = commonTmsDAO.selectTtMstTmcdDt(paramMap);
					rtnMap.put("FIELD","TMCD_DTKEY;TMCD_DTNAME;TMCD_DTVALUE;COMMENTS");	
				}
				else if ("VIMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonVimsDAO.selectTViewMstVicdDtCount(paramMap);
					}
					rtnList = commonVimsDAO.selectTViewMstVicdDt(paramMap);
					rtnMap.put("FIELD", "VICD_HDKEY;VICD_DTKEY;VICD_DTNAME;VICD_DTVALUE");
				} 
				else if ("PORTAL".equals(activeApp)) {//PORTAL
					if (!"combo".equals(type)) {
						count = commonPortalDAO.selectTViewMstPtcdDtCount(paramMap);
					}
					rtnList = commonPortalDAO.selectTViewMstPtcdDt(paramMap);
					rtnMap.put("FIELD", "PTCD_DTKEY;PTCD_DTNAME;PTCD_DTVALUE");
				}
				else if ("PMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonPmsDAO.selectTPmsMstPtcdDtCount(paramMap);
					}
					rtnList = commonPmsDAO.selectTPmsMstPtcdDt(paramMap);
					rtnMap.put("FIELD", "PTCD_DTKEY;PTCD_DTNAME;PTCD_DTVALUE");
				}
				else if ("BBS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonBbsDAO.selectTBbsMstBbcdDtCount(paramMap);
					}
					rtnList = commonBbsDAO.selectTBbsMstBbcdDt(paramMap);
					rtnMap.put("FIELD", "BBCD_DTKEY;BBCD_DTNAME;BBCD_DTVALUE");
				}
				else if ("CMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonCmsDAO.selectTCmsMstCmcdDtCount(paramMap);
					}
					rtnList = commonCmsDAO.selectTCmsMstCmcdDt(paramMap);
					rtnMap.put("FIELD", "CMCD_HDKEY;CMCD_DTKEY;CMCD_DTNAME;CMCD_DTVALUE");
				}
				else if ("IBS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIbsDAO.selectTIbsMstIbcdDtCount(paramMap);
					}
					rtnList = commonIbsDAO.selectTIbsMstIbcdDt(paramMap);
					rtnMap.put("FIELD", "IBCD_HDKEY;IBCD_DTKEY;IBCD_DTNAME;IBCD_DTVALUE");
				}
				else if ("MDM".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonMdmDAO.selectTMdmMstMdcdDtCount(paramMap);
					}
					rtnList = commonMdmDAO.selectTMdmMstMdcdDt(paramMap);
					rtnMap.put("FIELD", "MDCD_HDKEY;MDCD_DTKEY;MDCD_DTNAME;MDCD_DTVALUE");
				}
				else if ("FIS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonFisDAO.selectTFisMstFicdDtCount(paramMap);
					}
					rtnList = commonFisDAO.selectTFisMstFicdDt(paramMap);
					rtnMap.put("FIELD", "FICD_HDKEY;FICD_DTKEY;FICD_DTNAME;FICD_DTVALUE");
				}
				
				
				break;
				
			case 30:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstMulAapMsgDtCount(paramMap);
					rtnList = commonDAO.selectTAdminMstMulAapMsgDt(paramMap);
					rtnMap.put("FIELD", "MULAAPMSG_HDKEY;APKEY;LAKEY;MSGTYPE;DISPLAYMESSAGE");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					rtnList = commonIcomDAO.selectTOrderMstMulaapmsgDt(paramMap);
					rtnMap.put("FIELD", "MULAAPMSG_HDKEY;LAKEY;MSGTYPE;DISPLAYMESSAGE");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					rtnList = commonWmsDAO.selectTWorkMstMulaapmsgDt(paramMap);
					rtnMap.put("FIELD", "CTKEY;MULAAPMSG_HDKEY;LAKEY;MSGTYPE;DISPLAYMESSAGE");
				}
				else if ("PMS".equals(activeApp)) {
					rtnList = commonPmsDAO.selectTPmsMstMulaapmsgDt(paramMap);
					rtnMap.put("FIELD", "MULAAPMSG_HDKEY;LAKEY;MSGTYPE;DISPLAYMESSAGE");
				}

				break;
				
			case 40:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)){
					count = commonDAO.selectTAdminMstAcCount(paramMap);
					rtnList = commonDAO.selectTAdminMstAc(paramMap);
					//rtnMap.put("FIELD", "PTN_ACKEY;PTN_ACNAME;PTN_ACTYPE");
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					if(paramMap.get("owkey") != null && "".equals(paramMap.get("owkey"))) {
						String[] owkey = ((String)paramMap.get("owkey")).split(",");
						List owlist = new ArrayList();
						
						if(owkey.length > 1) {
							for(int i=0; i< owkey.length; i++) {
								owlist.add((String)owkey[i]);
							}
							paramMap.remove("owkey");
							paramMap.put("owkeylist", owlist);
						}
					}
					count = commonIcomDAO.selectTOrderMstAcCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstAc(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)){
					count = commonWmsDAO.selectTWorkMstAcCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstAc(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE;LOGGRPCD;DCKEY");

					// VIMS
				}
				else if ("PTL".equals(activeApp)){
					count = commonPtlDAO.selectTAdminMstAcCount(paramMap);
					rtnList = commonPtlDAO.selectTAdminMstAc(paramMap);
					//rtnMap.put("FIELD", "PTN_ACKEY;PTN_ACNAME;PTN_ACTYPE");
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}
				else if ("VIMS".equals(activeApp)) {
					count = commonVimsDAO.selectTViewMstAcCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstAc(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}
			    // TMS
				else if ("TMS".equals(activeApp)) {
	                count = commonTmsDAO.selectTMMstAcCount(paramMap);
	                rtnList = commonTmsDAO.selectTMMstAc(paramMap);
	                rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
	            }
				else if ("PMS".equals(activeApp)) {					
					count = commonPmsDAO.selectTPmsMstAcCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstAc(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
	            }
				else if ("KPI".equals(activeApp)) {					
					count = commonKpiDAO.selectTKpiMstAcCount(paramMap);
					rtnList = commonKpiDAO.selectTKpiMstAc(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}

				break;
			case 44:
				// ADMIN
				if (  "PTL".equals(activeApp) ) {  
					count   = commonPortalDAO.selectTPtlMstAcCount(paramMap);
					rtnList = commonPortalDAO.selectTPtlMstAc(paramMap);
					//rtnMap.put("FIELD", "PTN_ACKEY;PTN_ACNAME;PTN_ACTYPE"); 
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
					// ICOM 
				}
	            break;
			case 50:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)|| "PTL".equals(activeApp)) {
					count = commonDAO.selectTAdminMstOwCount(paramMap);
					rtnList = commonDAO.selectTAdminMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp) ) {
					count = commonIcomDAO.selectTOrderMstOwCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstOwCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");

					// TMS
				}
				else if ("TMS".equals(activeApp)) {
					count = commonTmsDAO.selectTTMstOwCount(paramMap);
					rtnList = commonTmsDAO.selectTTMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");

					// VIMS 2014.05.21 jk
				}
				else if ("VIMS".equals(activeApp)) {
					count = commonVimsDAO.selectTViewMstOwCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
				}
				else if ("KPI".equals(activeApp)) {
					count = commonKpiDAO.selectTKpiMstOwCount(paramMap);
					rtnList = commonKpiDAO.selectTKpiMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
				}
				else if ("PMS".equals(activeApp)) {					
					count = commonPmsDAO.selectTPmsMstOwCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstOw(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
				}
				
				break;
				
			case 60:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstCtCount(paramMap);
					rtnList = commonDAO.selectTAdminMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}else 	if ( "PTL".equals(activeApp) ) {
					count = this.commonPtlDAO.selectTAdminMstOwCount(paramMap);
					rtnList = commonPtlDAO.selectTAdminMstOw(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				else if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstCtCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");

					// VIMS
				}
				else if ("VIMS".equals(activeApp)) {
					count = commonVimsDAO.selectTViewMstCtCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");

					// TMS
				}
				else if ("KPI".equals(activeApp)) {
					count = commonKpiDAO.selectTKpiMstCtCount(paramMap);
					rtnList = commonKpiDAO.selectTKpiMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");

					// KPI
				}
				else if ("TMS".equals(activeApp)) {
					count = commonTmsDAO.selectTTMstCtCount(paramMap);
					rtnList = commonTmsDAO.selectTTMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				else if ("PMS".equals(activeApp)) {			
					count = commonPmsDAO.selectTPmsMstCtCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				else if ("PORTAL".equals(activeApp) || "PTL".equals(activeApp)) {					
					count = commonDAO.selectTAdminMstCtCount(paramMap);
					rtnList = commonDAO.selectTAdminMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				else if ("BBS".equals(activeApp)) {					
					count = commonDAO.selectTAdminMstCtCount(paramMap);
					rtnList = commonDAO.selectTAdminMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				else if ("KPI".equals(activeApp)) {
					count = commonDAO.selectTAdminMstCtCount(paramMap);
					rtnList = commonDAO.selectTAdminMstCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");
				}
				break;
				
			case 70:
				count = commonDAO.selectTAdminMstUrgrCount(paramMap);
				rtnList = commonDAO.selectTAdminMstUrgr(paramMap);
				rtnMap.put("FIELD", "URGRKEY;URGRNAME");
				break;
				
			case 80:
				if ("PMS".equals(activeApp)) {			
					count = commonPmsDAO.selectTPmsMstUrCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstUr(paramMap);
					rtnMap.put("FIELD", "URKEY;URNAME");
				}else{
					count = commonDAO.selectTAdminMstUrCount(paramMap);
					rtnList = commonDAO.selectTAdminMstUr(paramMap);
					rtnMap.put("FIELD", "URKEY;URNAME");					
				}
				break;
				
			case 90:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstIcgrCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;ICGRTYPE;OWKEY");
					// ICOM 
				}
				else if ("PTL".equals(activeApp)) {
					count = commonPtlDAO.selectTPtlMstIcgrCount(paramMap);
					rtnList = commonPtlDAO.selectTPtlMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;ICGRTYPE;OWKEY");

					// WMS
				}				
				else if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstIcgrCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;ICGRTYPE;OWKEY");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {

					count = commonWmsDAO.selectTWorkMstIcgrCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;ICGRTYPE;OWKEY");

					// VIMS - 2014.05.23 jk
				}
				else if ("VIMS".equals(activeApp)) {

					count = commonVimsDAO.selectTViewMstIcgrCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;OWKEY");
				}
				else if ("KPI".equals(activeApp)) {

					count = commonKpiDAO.selectTKpiMstIcgrCount(paramMap);
					rtnList = commonKpiDAO.selectTKpiMstIcgr(paramMap);
					rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;OWKEY");
				}
                else if ("PMS".equals(activeApp)) {                 
                    /*count = commonPmsDAO.selectTPmsMstIcgrCount(paramMap);
                    rtnList = commonPmsDAO.selectTPmsMstIcgr(paramMap);*/
                	count = commonDAO.selectTAdminMstIcgrCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIcgr(paramMap);
                    rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;ICGRTYPE;OWKEY");
                }
                
				else if ("TMS".equals(activeApp)) {

                    count = commonTmsDAO.selectTTMstIcgrCount(paramMap);
                    rtnList = commonTmsDAO.selectTTMstIcgr(paramMap);
                    rtnMap.put("FIELD", "ICGRKEY;ICGRNAME;OWKEY");
                }
				break;
				
			case 100:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstIcutCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIcut(paramMap);
					rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY;OWKEY");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstIcutCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcut(paramMap);
					rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY;OWKEY");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstIcutCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstIcut(paramMap);
					rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY;OWKEY");

					// VIMS
				}
				else if ("VIMS".equals(activeApp)) {
					count = commonVimsDAO.selectTViewMstIcutCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstIcut(paramMap);
					rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY");
				}
				else if ("PMS".equals(activeApp)) {					
					count = commonPmsDAO.selectTPmsMstIcutCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstIcut(paramMap);
					rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY;OWKEY");
				}
 
				break;
				
			case 108:
				// ICOM 기타출고주문(일반), 폐기출고주문(일반)
				if ("ICOM".equals(activeApp)) {
					//count = commonDAO.selectTAdminMstIcCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIc3(paramMap);
					System.out.println("selectTAdminMstIc3_paramMap:::"+paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWNAME;UOM;PRODUCTLOT;ATTRIBUTE3;HOQTY;TTQTY");
				}
				
				break;
				
			case 109:
				// ICOM 폐기출고주문(보세)
				if ("ICOM".equals(activeApp)) {
					//count = commonDAO.selectTAdminMstIcCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIc4(paramMap);
					System.out.println("selectTAdminMstIc4_paramMap:::"+paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWNAME;UOM;PRODUCTLOT;ATTRIBUTE3;HOQTY;TTQTY");
				}
				
				break;
				
			case 110:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstIcCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;LOGGRPCD");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstIcCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE;LOGGRPCD");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					
					// 2015.12.22  모바일일 경우 분기 처리
					if( "MOBILE".equals(device)){
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstIcMobileCount(paramMap);
						}
						rtnList = commonWmsDAO.selectTWorkMstIcMobile(paramMap);
						// 재고조사 시 상품추가에 사용을 하기위해 상저온 코드가 필요함. field7번으로 추가. 2017.04.27 고제영
						rtnMap.put("FIELD", "ICKEY;ICNAME;ICUTKEY;UOM;SPEC;ICGRKEY;LOGGRPCD");
					} else {
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstIcCount(paramMap);
						}
						rtnList = commonWmsDAO.selectTWorkMstIc(paramMap);
						rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE;LOGGRPCD;SPEC;ICGRKEY;ICGRNAME");
					}

					// VIMS - 2014.05.23 jk
				}
				else if ("VIMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonVimsDAO.selectTViewMstIcCount(paramMap);
					}
					rtnList = commonVimsDAO.selectTViewMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE");
				}
				else if ("KPI".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonKpiDAO.selectTKpiMstIcCount(paramMap);
					}
					rtnList = commonKpiDAO.selectTKpiMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE");
				}
				else if ("PMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonPmsDAO.selectTPmsMstIcCount(paramMap);
					}
					rtnList = commonPmsDAO.selectTPmsMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE");
				}
				else if ("PTL".equals(activeApp)) {
					/*paramMap.put("rep_ackey", sessionVo.getRep_ackey());
					count = commonDAO.selectTAdminMstIcCountPerAC(paramMap);
					rtnList = commonDAO.selectTAdminMstIcPerAC(paramMap);*/
					count = commonPtlDAO.selectTPtlMstIcCount(paramMap);
					rtnList = commonPtlDAO.selectTPtlMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME");
				}

				break; // refTable : TADMIN_MST_IC
				
				//WMS - OBDT에 상품 검색 팝업에서 모든 상품 조회 되도록 수정 
			case 111:
				if ("ICOM".equals(activeApp)|| "OMS".equals(activeApp)) {
					// OMS 상품X가용재고 팝업
					count = commonIcomDAO.selectTOrderMstIcAvqtyCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcAvqty(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;LOGGRPCD;AVQTY;TTQTY");

				} 
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					
					// 2015.12.22  모바일일 경우 분기 처리
					if( "MOBILE".equals(device)){
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstIcMobileCount(paramMap);
						}
						rtnList = commonWmsDAO.selectTWorkMstIcMobile(paramMap);
						rtnMap.put("FIELD", "ICKEY;ICNAME;ICUTKEY;UOM;SPEC;ICGRKEY");
					} else {
						if (!"combo".equals(type)) {
							count = commonWmsDAO.selectTWorkMstIcCount2(paramMap);
						}
						rtnList = commonWmsDAO.selectTWorkMstIc2(paramMap);
						rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;SALEPRICE;LOGGRPCD;SPEC;ICGRKEY");
					}
					// VIMS - 2014.05.23 jk
				}	
				break; // refTable : TADMIN_MST_IC
				
			case 112:
				//OMS 상품 검색
				if("ICOM".equals(activeApp) || "OMS".equals(activeApp)){
					count = commonDAO.selectTAdminMstIcInICOMCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIcInICOM(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;DELYN;CLOSINGDATE");
				}
				break;
				
			case 120:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstIcsbCount(paramMap);
					rtnList = commonDAO.selectTAdminMstIcsb(paramMap);
					rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstIcsbCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcsb(paramMap);
					rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selcetTWorkMstIcsbCount(paramMap);
					rtnList = commonWmsDAO.selcetTWorkMstIcsb(paramMap);
					rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");

					// VIMS - 2014.05.23 jk
				}
				else if ("VIMS".equals(activeApp)) {
					count = commonVimsDAO.selectTViewMstIcsbCount(paramMap);
					rtnList = commonVimsDAO.selectTViewMstIcsb(paramMap);
					rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");
				}
				else if ("PMS".equals(activeApp)) {
                    count = commonPmsDAO.selectTPmsMstIcsbCount(paramMap);
                    rtnList = commonPmsDAO.selectTPmsMstIcsb(paramMap);
                    rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");
                }
				else if ("TMS".equals(activeApp)) {
                    count = commonTmsDAO.selectTTMstIcsbCount(paramMap);
                    rtnList = commonTmsDAO.selectTTMstIcsb(paramMap);
                    rtnMap.put("FIELD", "SBKEY;ICKEY;OWKEY;ICUTTYPE");
                }

				break; // refTable : TADMIN_MST_ICSB
				
			case 130:
				count = commonDAO.selectTAdminMstUsscHdCount(paramMap);
				rtnList = commonDAO.selectTAdminMstUsscHd(paramMap);
				rtnMap.put("FIELD", "USSC_HDKEY;USSC_HDNAME");
				break; // refTable : TADMIN_MST_USSC_DT
			case 140:
				if("VIMS".equals(activeApp) || "VMS".equals(activeApp)){
					count = commonDAO.selectTAdminMstUrscVmsCount(paramMap);
					rtnList = commonDAO.selectTAdminMstUrscVms(paramMap);
					rtnMap.put("FIELD", "USKEY;USNAME;APKEY;USSC_HDKEY");	
				}else{
					count = commonDAO.selectTAdminMstUrscCount(paramMap);
					rtnList = commonDAO.selectTAdminMstUrsc(paramMap);
					rtnMap.put("FIELD", "USKEY;USNAME;APKEY;USSC_HDKEY");
				}
				break; // refTable : TADMIN_MST_URSC
			case 150:
				count = commonDAO.selectTAdminMstMeCount(paramMap);
				rtnList = commonDAO.selectTAdminMstMe(paramMap);
				rtnMap.put("FIELD", "MEKEY;MENAMECODE;APKEY;EQTYPE;MENAME");
				break; // refTable : TADMIN_MST_ME
			case 160:
				count = commonDAO.selectTAdminMstRoCount(paramMap);
				rtnList = commonDAO.selectTAdminMstRo(paramMap);
				rtnMap.put("FIELD", "ROKEY;RONAME");
				break; // refTable : TADMIN_MST_RO
			case 170:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminSetAdScCount(paramMap);
					rtnList = commonDAO.selectTAdminSetAdSc(paramMap);
					rtnMap.put("FIELD", "ADCSKEY;ADCSNAME");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					rtnList = commonIcomDAO.selectTOrderSetOrcs(paramMap);
					rtnMap.put("FIELD", "ORCSKEY;ORCSNAME");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonWmsDAO.selectTWorkSetCtcsCount(paramMap);
					}
					rtnList = commonWmsDAO.selectTWorkSetCtcs(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTCSKEY;CTCSNAME");
				}
				else if("IBS".equals(activeApp)){
			       count = commonIbsDAO.selectTIbsSetIbcsCount(paramMap);	
			       rtnList = commonIbsDAO.selectTIbsSetIbcs(paramMap);
			       rtnMap.put("FIELD", "IBCSKEY;IBCSNAME");
				}
				else if("MDM".equals(activeApp)){
					count = commonMdmDAO.selectTMdmSetMdcsCount(paramMap);	
				    rtnList = commonMdmDAO.selectTMdmSetMdcs(paramMap);
				    rtnMap.put("FIELD", "MDCSKEY;MDCSNAME");	
				}
				else if("CMS".equals(activeApp)){
					count = commonCmsDAO.selectTCmsSetCmcsCount(paramMap);	
				    rtnList = commonCmsDAO.selectTCmsSetCmcs(paramMap);
				    rtnMap.put("FIELD", "CMCSKEY;CMCSNAME");	
				}
				break; // refTable : TADMIN_SET_SC
			case 171:
					count = commonDAO.selectTAdminSetScCount(paramMap);
					rtnList = commonDAO.selectTAdminSetSc(paramMap);
					rtnMap.put("FIELD", "SCKEY;SCNAME;DEFAULTVALUE;VALUE1;VALUE2");
				break; // refTable : TADMIN_SET_SC				
			case 180:
				// ADMIN
				count = commonDAO.selectTAdminMstTzCount(paramMap);
				rtnList = commonDAO.selectTAdminMstTz(paramMap);
				rtnMap.put("FIELD", "TZKEY;UTC_OFFSET");
				break; // refTable : TADMIN_MST_TZ			
			case 181:
				count = commonWmsDAO.selectTWorkMstZnCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstZn(paramMap);
				rtnMap.put("FIELD", "ZNKEY;ZNNAME");
				break; // refTable : TWORK_MST_ZN
			case 190:
				count = commonWmsDAO.selectTWorkMstArCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstAr(paramMap);
				rtnMap.put("FIELD", "ARKEY;ARNAME;LOGGRPCD");
				break; // refTable : TWORK_MST_AR
			case 200:
				count = commonWmsDAO.selectTWorkMstLcCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstLc(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;LCHOLDTYPE;ERPSTLC;ARKEY;EMPTY_LOCATION_YN");
				break; // refTable : TWORK_MST_LC
			case 201:
				count = commonWmsDAO.selectTWorkMstLcMobCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstLcMob(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;LCHOLDTYPE;ERPSTLC;ARKEY");
				break; // refTable : TWORK_MST_LC Mobile
			case 202:
				// WMS 입고관리>적치, 재고관리>로케이션이동, 재고관리>로케이션이동작업  그리드 TO_로케이션선택 팝업에서 사용 20181109
				count = commonWmsDAO.selectTWorkMstLcNonAutoCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstLcNonAuto(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;LCHOLDTYPE;ERPSTLC;ARKEY;EMPTY_LOCATION_YN");
				break; // refTable : TWORK_MST_LC
			case 210:
				count = commonWmsDAO.selectTWorkMstUlcCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstUlc(paramMap);
				rtnMap.put("FIELD", "ULCKEY;ULCTYPE");
				break; // refTable : TWORK_MST_ULC
			case 220:
				count = commonWmsDAO.selectTWorkInfoIvatCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkInfoIvat(paramMap);
				rtnMap.put("FIELD", "IVATKEY;OWKEY;ICKEY;PRODUCTDATE;EXPIREDATE;IBCONFIRMDATE;PRODUCTLOT;IVATSTATUS");
				break; // refTable : TWORK_INFO_IVAT
			case 230:
				count = commonIcomDAO.selectTOrderMstZcCount(paramMap);
				rtnList = commonIcomDAO.selectTOrderMstZc(paramMap);
				rtnMap.put("FIELD", "ZCKEY;CITY;STATE;ADDRESS1;DLCOMPANY");
				break;
			case 240:
				count = commonWmsDAO.selectTWorkInfoWaHdCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkInfoWaHd(paramMap);
				rtnMap.put("FIELD", "WAHDKEY;OWKEY;WAHDNAME;WKSTATUS;WAHDDATE");
				break;
			case 250:
				count = commonWmsDAO.selectPutawayLocationCount(paramMap);
				rtnList = commonWmsDAO.selectPutawayLocation(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;QA_STATUS;ARKEY");
				break;
				
			case 270:
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonDAO.selectTAdminMstIcutxtypeCount(paramMap);
					}
					rtnList = commonDAO.selectTAdminMstIcutxtype(paramMap);
					rtnMap.put("FIELD", "ICUTTYPE;ICUTKEY;ICUTQTY;ICUTORDER");
					// ICOM 
				}
				else if ("ICOM".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIcomDAO.selectTOrderMstIcutxtypeCount(paramMap);
					}
					rtnList = commonIcomDAO.selectTOrderMstIcutxtype(paramMap);
					rtnMap.put("FIELD", "ICUTTYPE;ICUTQTY");

					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonWmsDAO.selectTWorkMstIcutxtypeCount(paramMap);
					}
					rtnList = commonWmsDAO.selectTWorkMstIcutxtype(paramMap);
					rtnMap.put("FIELD", "ICUTTYPE;ICUTQTY");
					break;

					// VIMS 
				}
				else if ("VIMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonVimsDAO.selectTViewMstIcutxtypeCount(paramMap);
					}
					rtnList = commonVimsDAO.selectTViewMstIcutxtype(paramMap);
					rtnMap.put("FIELD", "ICUTTYPE;ICUTQTY");

					LOG.debug("rtnList ==>" + rtnList);
					// WMS
				}
				else if ("PMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonPmsDAO.selectTPmsMstIcutxtypeCount(paramMap);
					}
					rtnList = commonPmsDAO.selectTPmsMstIcutxtype(paramMap);
					rtnMap.put("FIELD", "ICUTTYPE;ICUTQTY");
				}
				
				break;

			case 280:
				/*
				count =commonIcomDAO.selectTOrderMstIcutxtypeCount(paramMap);
				rtnList = commonIcomDAO.selectTOrderMstIcutxtype(paramMap);
				rtnMap.put("FIELD", "ICUTKEY;ICUTTYPE;ICUTQTY");
				break; 
				*/
				count = commonWmsDAO.selectTWorkMstPiznCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstPizn(paramMap);
				rtnMap.put("FIELD", "PIZNKEY;PIZNNAME");
				break;

			/* Young.Park */
			case 290:
				//ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonDAO.selectTAdminMstAdcdCstDtCount(paramMap);
					}
					rtnList = commonDAO.selectTAdminMstAdcdCstDt(paramMap);
					rtnMap.put("FIELD", "ADCD_HDKEY;ADCD_DTKEY;ADCD_DTNAME;ADCD_DTVALUE");
					// ICOM
				}
				else if ("ICOM".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIcomDAO.selectTOrderMstOrcdCstDtCount(paramMap);
					}
					rtnList = commonIcomDAO.selectTOrderMstOrcdCstDt(paramMap);
					rtnMap.put("FIELD", "ORCD_HDKEY;ORCD_DTKEY;ORCD_DTNAME;ORCD_DTVALUE");
					// WMS
				}
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonWmsDAO.selectTWorkMstCtcdCstDtCount(paramMap);
					}
					rtnList = commonWmsDAO.selectTWorkMstCtcdCstDt(paramMap);
					rtnMap.put("FIELD", "CTCD_HDKEY;CTCD_DTKEY;CTCD_DTNAME;CTCD_DTVALUE");
				}
				else if ("KPI".equals(activeApp)) {
					count = commonKpiDAO.selectTKpiMstOrdCdDtCount(paramMap);
					rtnList = commonKpiDAO.selectTKpiMstOrdCdDt(paramMap);
					rtnMap.put("FIELD", "KPCD_HDKEY;KPCD_DTKEY;KPCD_DTNAME;KPCD_DTVALUE");
				}
				break;

			case 300:
				count = commonWmsDAO.selectTWorkMstLcCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstErpLoc(paramMap);
				rtnMap.put("FIELD", "ERPSTLC;LCKEY;LCTYPE;LCHOLDTYPE;ARKEY");
				break;

			case 310: //장비.ehkim	

				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstEqCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstEq(paramMap);
				}
				else {
					if ("ICOM".equals(activeApp)) {
						paramMap.put("apkey", activeApp);

					}
					else if ("VIMS".equals(activeApp)) {
						paramMap.put("apkey", activeApp);
					}
					else if ("PMS".equals(activeApp)) {
						paramMap.put("apkey", activeApp);
					}

					count = commonDAO.selectTAdminMstEqCount(paramMap);
					rtnList = commonDAO.selectTAdminMstEq(paramMap);
				}

				rtnMap.put("FIELD", "EQKEY;EQTYPE");

				break;

			case 320: //물품매핑코드.swbae	

				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstIcmpCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcmp(paramMap);
				}
				rtnMap.put("FIELD", "ICMPKEY;OWKEY");
				break;

			case 330:
				count = commonWmsDAO.selectWaybillnoCount(paramMap);
				rtnList = commonWmsDAO.selectWaybillno(paramMap);
				rtnMap.put("FIELD", "FIRSTWAYBILLNO;UDF1;OWKEY;WAYBILLNO;ULCKEY;INSERTDATE;INSERTURKEY");
				break;

			case 350: //발주공통코드.swbae	

				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTworkInfoPoHdCount(paramMap);
					rtnList = commonWmsDAO.selectTworkInfoPoHd(paramMap);
				}
				rtnMap.put("FIELD", "POHDKEY;POHDDATE;DELIVERYDATE;OWKEY;ACKEY;ACNAME");
				break;

			case 360:
				count = commonWmsDAO.selectTWorkMstStgCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstStg(paramMap);
				rtnMap.put("FIELD", "STGKEY;STGDESC");
				break; // refTable : TWORK_MST_STG
				
			case 370:
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTworkMstCtxStCount(paramMap);
					rtnList = commonWmsDAO.selectTworkMstCtxSt(paramMap);
				}
				rtnMap.put("FIELD", "STKEY;STNAME");
				break;		
			
				
				
			case 380:
			case 390:
			case 400:
			case 410:
			case 420:
				
			case 710:
				// AMS
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtgCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtg(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");					
				}
				// OMS
				else if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtgCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtg(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");
				}
				// WMS
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					LOG.debug("WMS");
				}
				break;
			case 720:
				// AMS
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)  || "PTL".equals(activeApp)) {
					count = commonDAO.selectStoreCount(paramMap);
					rtnList = commonDAO.selectStoreList(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
				}
				// OMS
				else if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstStoreCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstStore(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
				}
				// WMS
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstStoreCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstStore(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME;DCKEY");
				}
				// TMS
                else if ("TM".equals(activeApp) || "TMS".equals(activeApp)) {
                    count = commonTmsDAO.selectTMMstStoreCount(paramMap);
                    rtnList = commonTmsDAO.selectTMMstStore(paramMap);
                    rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
                }
				// PORTAL
                else if ("PORTAL".equals(activeApp)) {
                    count = commonPortalDAO.selectTPortalMstStoreCount(paramMap);
                    rtnList = commonPortalDAO.selectTPortalMstStore(paramMap);
                    rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
                    
                }
                else if ("PMS".equals(activeApp)) {					
					count = commonPmsDAO.selectTPmsMstStoreCount(paramMap);					
					rtnList = commonPmsDAO.selectTPmsMstStore(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
					
				}
				break;
			case 730:  //
				if ("PTL".equals(activeApp)  ) {
					count = this.commonPtlDAO.selectTOrderMstStoreCount(paramMap);
					rtnList = commonPtlDAO.selectTOrderMstStore(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME");
				} 
				break;
			case 740:
				// AMS
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstAddrCount(paramMap);
					rtnList = commonDAO.selectTAdminMstAddr(paramMap);
					rtnMap.put("FIELD", "ADDRID;CTYNM;STNM;UNIT;ZIPCD");
				} 
				// OMS
				else if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstAddrTCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstAddrT(paramMap);
					rtnMap.put("FIELD", "ADDRID;CTYNM;STNM;UNIT;ZIPCD");
				}
				// WMS
				else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					LOG.debug("WMS");
				}
				break;			
			case 750: // ICKEY + ICNAME + ICUTKEY + UOM + ICUTQTY + LOGGRPCD
				// WMS
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectIcXIcuttypeXQtyCount(paramMap);
					rtnList = commonWmsDAO.selectIcXIcuttypeXQty(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;ICUTKEY;ICUTTYPE;ICUTQTY;LOGGRPCD;OWKEY;ACKEY;ICGRKEY;ICGRTYPE");
				}else if ("PTL".equals(activeApp) || "POTAL".equals(activeApp)) {
					count = commonPtlDAO.selectPtlLogrpcdCount(paramMap);
					rtnList = commonPtlDAO.selectPtlLogrpcd(paramMap);
					rtnMap.put("FIELD", "ADCD_DTKEY;ADCD_DTNAME");
				}
				break;
			case 760:
				// AMS
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTOrderMstSeIcCount(paramMap);
					rtnList = commonDAO.selectTOrderMstSeIc(paramMap);
					rtnMap.put("FIELD", "SEICKEY;OWKEY;ICKEY;ICNAME;ICUTKEY");  
				} 
				// OMS
				else if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstSeIcCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstSeIc(paramMap);
					rtnMap.put("FIELD", "SEICKEY;ICKEY;ICNAME;OWKEY;UOM;ICUTKEY;CLOSINGDATE");
					
				} else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					if("Y".equals(paramMap.get("loggrpcdyn"))){
						count = commonWmsDAO.selectCountTworkMstSeIcXLoggHd(paramMap);
						rtnList = commonWmsDAO.selectListTworkMstSeIcXLoggHd(paramMap);
						rtnMap.put("FIELD", "SEICKEY;ICKEY;ICNAME;ICUTKEY;OWKEY");
					}else{
						count = commonWmsDAO.selectCountTworkMstSeIcHd(paramMap);
						rtnList = commonWmsDAO.selectListTworkMstSeIcHd(paramMap);
						rtnMap.put("FIELD", "SEICKEY;ICKEY;ICNAME;ICUTKEY;CLOSINGDATE;OWKEY");
					}
				}

				break;
			case 770:
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp) || "PORTAL".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstPogrpCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstPogrp(paramMap);
					rtnMap.put("FIELD", "POGRPKEY;POGRPNAME;OWKEY");
				}
				break;
			case 780:				
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)   ) {
					count = commonIcomDAO.selectTOrderMstIcxCtCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstIcxCt(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;CTKEY;ICUTKEY;UOM;CTNAME;COSTPRICE;ICGRKEY;SPEC;LOGGRPCD;DCKEY");
				}
				if ("PTL".equals(activeApp)    ) {
					count = commonPortalDAO.selectTOrderMstIcxCtCount(paramMap);
					rtnList = commonPortalDAO.selectTOrderMstIcxCt(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;CTKEY;ICUTKEY;UOM;CTNAME;COSTPRICE;ICGRKEY;SPEC;LOGGRPCD;DCKEY");
				}
				
				break;
			case 790:
				if (!"combo".equals(type)) {
					count = commonDAO.selectTAdminMstUrXEqCount(paramMap);
				}
				rtnList = commonDAO.selectTAdminMstUrXEq(paramMap);
				rtnMap.put("FIELD", "URKEY;URNAME;EQTYPE;PUSHID");
					
				break;
			case 800:				
				// OMS
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstStorexCtCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstStorexCt(paramMap);
					rtnMap.put("FIELD", "STORE_KEY;STORE_NAME;CTKEY");
				}
				
				break;
				
			case 810:				
				// OMS
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIcomDAO.selectTOrderMstOrTypeReasonCount(paramMap);
					}
					rtnList = commonIcomDAO.selectTOrderMstOrTypeReason(paramMap);
					rtnMap.put("FIELD", "ORREASONCD;ORREASONNAME");
				}
				
				break;

			case 820:
				
				// WMS
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstAcXUrCommonCdCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstAcXUrCommonCd(paramMap);
					rtnMap.put("FIELD", "URKEY;ACKEY;ACNAME;ICGRKEY;ICGRTYPE;ICGRNAME");
				} else
				// OMS
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstAcXUrCommonCdCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstAcXUrCommonCd(paramMap);
					rtnMap.put("FIELD", "URKEY;ACKEY;ACNAME;ICGRKEY;ICGRTYPE;ICGRNAME");
				}
				break;
				
			case 830://20150121 추가 
				
				// PORTAL
                if ("PORTAL".equals(activeApp)) {
    				count = commonDAO.selectTAdminMstPortalUrCount(paramMap);
    				rtnList = commonDAO.selectTAdminMstPortalUr(paramMap);
    				rtnMap.put("FIELD", "URKEY;URNAME");
                }
				break;			
			case 840:
				// ADMIN
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstToCtCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstToCt(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME;CTTYPE");

					// VIMS
				}
				break;
			case 850:
				// OMS
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstPromotionIcCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstPromotionIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;COSTPRICE;ICGRKEY;SPEC;LOGGRPCD;DCKEY");
				}
				break;
			case 910:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)
					|| "ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					paramMap.put("ctglevel", "1");
					count = commonIcomDAO.selectTOrderMstCtgCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtg(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");					
				}
				break;
			case 920:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)
					|| "ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					paramMap.put("ctglevel", "2");
					count = commonIcomDAO.selectTOrderMstCtgCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtg(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");					
				}
				break;	
			case 930:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)
					|| "ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					paramMap.put("ctglevel", "3");
					count = commonIcomDAO.selectTOrderMstCtgCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtg(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");					
				}
				break;					
			// TMS
			case 1030:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtCustTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtCustTList(paramMap);
				rtnMap.put("FIELD", "OWKEY;OWNAME");
				break;
            case 1040:
                if (!"combo".equals(type)) {
                    count = commonTmsDAO.selectTtSmEstmTCount(paramMap);
                }
                rtnList = commonTmsDAO.selectTtSmEstmTList(paramMap);
                rtnMap.put("FIELD", "ESTMDESC;FRMDATE;TODATE");
                break;
            case 1041:
                if (!"combo".equals(type)) {
                    count = commonTmsDAO.selectTtSmEstmHdCount(paramMap);
                }
                rtnList = commonTmsDAO.selectTtSmEstmHdList(paramMap);
                rtnMap.put("FIELD", "QUESTION");
                break;
			case 1050:
				if ("TMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonTmsDAO.selectTtPlanTCount(paramMap);
					}
					rtnList = commonTmsDAO.selectTtPlanTList(paramMap);
					rtnMap.put("FIELD", "PLANID;PLANDESC");
				}
				break;

			case 1060:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtCarrTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtCarrTList(paramMap);
				rtnMap.put("FIELD", "CARRCD;CARRNM");
				break;

			case 1070:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtItmGrpTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtItmGrpTList(paramMap);
				rtnMap.put("FIELD", "ITMGRPCD;ITMGRPDESC");
				break;

			case 1080:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtItmMstTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtItmMstTList(paramMap);
				rtnMap.put("FIELD", "ITMCD;ITMDESC");
				break;

			case 1090:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtCurcncyTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtCurcncyTList(paramMap);
				rtnMap.put("FIELD", "CURCNCYTYP;CURCNCYCD;CURCNCYDESC");
				//			System.err.println("=====currency Search=====");
				break;

			case 1140:
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtShpglocTCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtShpglocTList(paramMap);
				rtnMap.put("FIELD", "SHPGLOCNM;SHPGLOCCD");
				break;

			case 1100:
				paramMap.put("rep_ackey", sessionVo.getRep_ackey());
				if (!"combo".equals(type)) {
					count = commonPortalDAO.selectTtDcustTCount(paramMap);
				}
				rtnList = commonPortalDAO.selectTtDcustTList(paramMap);
				rtnMap.put("FIELD", "ACKEY;ACNAME");
				//			System.err.println("=====Dcust Search=====");
				break;
				
			case 1110:	
				if (!"combo".equals(type)) {
					count = commonTmsDAO.selectTtMstEqmtTypCdCount(paramMap);
				}
				rtnList = commonTmsDAO.selectTtMstEqmtTypCd(paramMap);
				rtnMap.put("FIELD", "EQUIPTYPCD;EQUIPTYPDESC;DCKEY");
				break;
			case 1120:	//DOC구분
				// PORTAL
                if ("PORTAL".equals(activeApp)) {
                    count = commonPortalDAO.selectTPortalMstDStoreDocCount(paramMap);
                    rtnList = commonPortalDAO.selectTPortalMstDStoreDoc(paramMap);
                    rtnMap.put("FIELD", "DOCKKEY;DOCKNAME");
                }
				break;
			case 1130:	//사용자
				// PORTAL
				paramMap.put("rep_ackey", sessionVo.getRep_ackey());
                if ("PORTAL".equals(activeApp) || "AMS".equals(activeApp) ) {
                    count = commonPortalDAO.selectTtDcustTCount(paramMap);
                    rtnList = commonPortalDAO.selectTtDcustTList(paramMap);
                    rtnMap.put("FIELD", "ACKEY;ACNAME");
                }
                break;
			case 1136:	//포탈 그룹.
				// PORTAL
 
	 
				count  = commonPortalDAO.selectTAdminPotalUserGroupCount(paramMap);
				rtnList = commonPortalDAO.selectTAdminPotalUserGroupList(paramMap);
				rtnMap.put("FIELD", "URGRKEY;URGRNAME");
				break; 
	 			
			case 1200:
				//ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstAcForRepCount(paramMap);
					rtnList = commonDAO.selectTAdminMstAcForRep(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}
				if ("WMS".equals(activeApp) || "WM".equals(activeApp)) {
					count = commonWmsDAO.selectTWorkMstAcForRepCount(paramMap);
					rtnList = commonWmsDAO.selectTWorkMstAcForRep(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}
				if ("ICOM".equals(activeApp) || "OMS".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstAcForRepCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstAcForRep(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE;OWKEY");
				}
				if ("PORTAL".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstAcForRepCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstAcForRep(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME;ACTYPE");
				}
				break;
			case 1210://영업유형(대)
				if ("PMS".equals(activeApp)) {
					paramMap.put("salestypelevel", "1");
					count = commonPmsDAO.selectTPmsMstSalesTypeCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstSalesType(paramMap);
					rtnMap.put("FIELD", "SALESTYPEKEY;SALESTYPENAME");
				}
				break;
			case 1220://영업유형(중)
				if ("PMS".equals(activeApp)) {
					paramMap.put("salestypelevel", "2");
					count = commonPmsDAO.selectTPmsMstSalesTypeCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstSalesType(paramMap);
					rtnMap.put("FIELD", "SALESTYPEKEY;SALESTYPENAME");
				}
				break;
			case 1230://영업유형(소)
				if ("PMS".equals(activeApp)) {
					paramMap.put("salestypelevel", "3");
					count = commonPmsDAO.selectTPmsMstSalesTypeCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstSalesType(paramMap);
					rtnMap.put("FIELD", "SALESTYPEKEY;SALESTYPENAME");
				}
				break;
			case 1240://제조업체
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstMnfrCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstMnfr(paramMap);
					rtnMap.put("FIELD", "MNFR;MNFRNAME");
				}
				break;
			case 1250://미생물검사항목마스터
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstMiorgInspCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstMiorgInsp(paramMap);
					rtnMap.put("FIELD", "INSPKEY;INSPNAME;INSPKIND");
				}
				break;
			case 1260://미생물가중치마스터
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstMiorgWgtCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstMiorgWgt(paramMap);
					rtnMap.put("FIELD", "MIORGWGTKEY;MIORGWGTNAME;WGTVALUE");
				}
				break;
			case 1270://PB안전지수가중치마스터
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstSafeIdxWgtCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstSafeIdxWgt(paramMap);
					rtnMap.put("FIELD", "SAFEIDXKEY;SAFEIDXNAME;WGTVALUE");
				}
				break;
			case 1280://심사항목마스터(대)
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstAudItemCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstAudItem(paramMap);
					rtnMap.put("FIELD", "AUDITEMKEY;AUDITEMNAME");
				}
				break;
			case 1290://게시판목록

				if ("BBS".equals(activeApp)) {
					count = commonBbsDAO.selectTBbsMstMeCount(paramMap);
					if(count > 0){
						rtnList = commonBbsDAO.selectTBbsMstMe(paramMap);
					}else{
						rtnList = commonBbsDAO.selectTBbsMstMeAll(paramMap);
					}
					rtnMap.put("FIELD", "MENAME;MEKEY");
				} else if("PORTAL".equals(activeApp)) {					
					count = commonPortalDAO.selectTPortalMstMeCount(paramMap);
					if(count > 0){
						rtnList = commonPortalDAO.selectTPortalMstMe(paramMap);
					}else{
						rtnList = commonPortalDAO.selectTPortalMstMeAll(paramMap);
					}					
					rtnMap.put("FIELD", "MENAME;MEKEY");
				}
				break;
			case 1310://비밀글 지정용 - 거래처
				if ("BBS".equals(activeApp)) {
					String uskey = (String)paramMap.get("uskey");
					if( "US00007570".equals(uskey) || "US00007580".equals( uskey ) || "US00007590".equals(uskey) || "US00007600".equals(uskey)){
						paramMap.put("bbs_schType", "CUSTOMER");	
					}else{
						paramMap.put("bbs_schType", "CENTER");	
					}					
					//rtnList = commonBbsDAO.selectTBbsMstMe(paramMap);
					count = commonDAO.selectTAdminMstUrCount(paramMap);
					rtnList = commonDAO.selectTAdminMstUr(paramMap);
					rtnMap.put("FIELD", "URKEY;URNAME;URGRKEY;URGRNAME");	
				}
				break;	
			case 1320://심사항목마스터
				if ("PMS".equals(activeApp)) {
					count = commonPmsDAO.selectTPmsMstAudItemTCount(paramMap);
					rtnList = commonPmsDAO.selectTPmsMstAudItemT(paramMap);
					rtnMap.put("FIELD", "AUDITEMKEY;AUDITEMNAME");
				}
				break;
			case 1330://범례 헤더
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTworkMstLegendCount(paramMap);
					rtnList = commonWmsDAO.selectTworkMstLegend(paramMap);
					rtnMap.put("FIELD", "LGHDKEY;LGHDNAME");
				}
				break;
			case 1340://3D분석 헤더
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTworkInfoAnalyCount(paramMap);
					rtnList = commonWmsDAO.selectTworkInfoAnaly(paramMap);
					rtnMap.put("FIELD", "ANALY_HDKEY;ANALY_TITLE");
				}
				break;
			case 1350://3D현황 헤더
				if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
					count = commonWmsDAO.selectTworkInfoStatusCount(paramMap);
					rtnList = commonWmsDAO.selectTworkInfoStatus(paramMap);
					rtnMap.put("FIELD", "ANALY_HDKEY;ANALY_TITLE");
				}
				break;
			case 1360://데쉬보드 헤더
				if ("VIMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonVimsDAO.selectTViewMstDashHdCount(paramMap);
					}
					rtnList = commonVimsDAO.selectTViewMstDashHd(paramMap);
					rtnMap.put("FIELD", "DASH_HDKEY;DASHDESCRIPTION");
				}
				break;
			case 1370: // 일괄발주 화주별 상품코드 조회
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectBatchOrderMstIcCount(paramMap);
					rtnList = commonIcomDAO.selectBatchOrderMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;ACKEY;ACNAME;CTKEY;OWKEY");
				}
			   break;
			case 1380: // 국가코드 조회
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstCntryCount(paramMap);
					rtnList = commonDAO.selectTAdminMstCntry(paramMap);
					rtnMap.put("FIELD", "CNTRYCD;CNTRYNM;ADCD_DTORDER;ADCD_DTVALUE");
				}
			   break;
			case 1390: // 우편번호 조회
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstZipcdCount(paramMap);
					rtnList = commonDAO.selectTAdminMstZipcd(paramMap);
					rtnMap.put("FIELD", "ZIPCD;STACD;CTYNM;CNTRYNM");
				}
			   break;
			case 1400: // 초도발주 화주별 상품코드 조회
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectNewProductMstIcCount(paramMap);
					rtnList = commonIcomDAO.selectNewProductMstIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;ACKEY;ACNAME;OWKEY");
				}
			   break;
			case 1410: // 초도발주 화주별 유사상품코드 조회시 사용(LIKE search 제외)
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectNewProductMstSimIcCount(paramMap);
					rtnList = commonIcomDAO.selectNewProductMstSimIc(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;ACKEY;ACNAME;OWKEY");
				}
			   break; 
			case 1420: // 상품 대분류 조회
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtgHlevelCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtgHlevel(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");
				}
			   break;   
			case 1430: // 상품 중분류 조회
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtgMlevelCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtgMlevel(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");
				}
			   break;  
			case 1440: // 상품 소분류 조회
				if ("ICOM".equals(activeApp)) {
					count = commonIcomDAO.selectTOrderMstCtgBlevelCount(paramMap);
					rtnList = commonIcomDAO.selectTOrderMstCtgBlevel(paramMap);
					rtnMap.put("FIELD", "CTGKEY;CTGLEVEL;CTGNAME");
				}
			   break;   
			case 1450: // OMS 재고유형 변경지시
				if ("ICOM".equals(activeApp)|| "OMS".equals(activeApp)) {
					if (!"combo".equals(type)) {
						count = commonIcomDAO.selectTOrderMstIvChangeCount(paramMap);
					}
					
					rtnList = commonIcomDAO.selectTOrderMstIvChange(paramMap);
					rtnMap.put("FIELD", "ORCD_DTKEY;ORCD_DTNAME");
				}
			   break; 
			case 1460: // Object Popup
				if("IBS".equals(activeApp)|| "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectTitObjectMasterCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectTitObjectMaster(paramMap);
					rtnMap.put("FIELD", "OBJECT_CODE;OBJECT_NAME;OBJECT_SID");
				}
				
				break;			
			case 1470: // Class Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectTitClassMasterCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectTitClassMaster(paramMap);
					rtnMap.put("FIELD", "CODE_CODE;CODE_NAME;CODE_SID");
				}
				
				break;
				
			case 1480: // Factor Popup
				if("IBS".equals(activeApp)|| "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectFactorListCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectFactorList(paramMap);
					rtnMap.put("FIELD", "CODE_CODE;CODE_NAME;CODE_SID");
				}
				
				break;
				
			case 1490: // Service Popup
				if("IBS".equals(activeApp)|| "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectTitServiceMasterCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectTitSerivceMaster(paramMap);
					rtnMap.put("FIELD", "SERVICE_CODE;SERVICE_NAME;SERVICE_SID");
				}
				
				break;
				
			case 1500: // MainFactor Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectMainFactorCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectMainFactor(paramMap);
					rtnMap.put("FIELD", "FACTOR_CODE;FACTOR_NAME;FACTOR_SID");
				}
				
				break;
				
			case 1510: // FactorValue Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
									
					rtnList = commonIbsDAO.selectFactorValue(paramMap);
					rtnMap.put("FIELD", "FACTOR_VALUE_CODE;FACTOR_VALUE_NAME;FACTOR_VALUE_SID");
				}
				break;
			
			case 1520: // LCC Code Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectLccCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectLccCode(paramMap);
					rtnMap.put("FIELD", "LCC_CODE;LCC_NAME");
				}
				break;
				
			case 1530: // VAT Code Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectVATCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectVATCode(paramMap);
					rtnMap.put("FIELD", "TAX_CODE;TAX_NAME");
				}
				break;
				
			case 1540: // 정산 거래처  Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectBillPartCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectBillPartCode(paramMap);
					rtnMap.put("FIELD", "CD_PARTNER;LN_PARTNER");
				}
				break;
				
			case 1550: // Table User Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectTableUserCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectTableUser(paramMap);
					rtnMap.put("FIELD", "USER_NAME");
				}
				break;
				
			case 1560: // Table Name Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					
					rtnList = commonIbsDAO.selectTableName(paramMap);
					rtnMap.put("FIELD", "TABLE_NAME;COMMENTS");
				}
				break;
				
			case 1570: // Source Table Code Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectSourceTableCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectSourceTable(paramMap);
					rtnMap.put("FIELD", "TABLE_NAME;COMMENTS");
				}
				break;
			
			case 1580: // Key Column Name Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectKeyColumnNameCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectKeyColumnName(paramMap);
					rtnMap.put("FIELD", "COLUMN_NAME;COMMENTS");
				}
				break;
				
			case 1590: // FactorValue Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){		
					String Query = commonIbsDAO.selectFactorDataQuery(paramMap);
					paramMap.put("FACTOR_DATA_QUERY", Query);
					
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectFactorDataQueryResultCount(paramMap);
					}
					rtnList = commonIbsDAO.selectFactorDataQueryResult(paramMap);
					rtnMap.put("FIELD", "CODE_SID;CODE_CODE;CODE_CODE1;CODE_CODE2;CODE_CODE3;CODE_CODE4;CODE_CODE5;CODE_CODE6;CODE_CODE7;CODE_CODE8;CODE_NAME");
				}
				break;
				
			case 1600: // 거래처  Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp) || "AMS".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectCustomerCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectCustomerCode(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
				}
				break;
				
			case 1610: // Key Column Name Popup
				if("PTL".equals(activeApp)){
					count = commonPtlDAO.selectTPtlMstIcnameCount(paramMap);
					rtnList = commonPtlDAO.selectTPtlMstIcname(paramMap);
					rtnMap.put("FIELD", "ICNAME;ICKEY");
				}
				break;	
			
			case 1620: // 물류그룹 Popup
				if("IBS".equals(activeApp)){
					String ctkey = sessionVo.getCtKey();
					paramMap.put("ctkey", ctkey);	
					paramMap.put("ctcd_hdkey", paramMap.get("adcd_hdkey"));
					if (!"combo".equals(type)) {
						count = commonWmsDAO.selectTWorkMstCtcdDtCount(paramMap);
					}
					
					rtnList = commonWmsDAO.selectTWorkMstCtcdDt(paramMap);
					rtnMap.put("FIELD", "CTCD_DTKEY;CTCD_DTNAME;CTCD_DTVALUE;DESCRIPTION");
				}
				break;	
				
			case 1630: // 센터코드 Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectCenterCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectCenterCode(paramMap);
					rtnMap.put("FIELD", "CTKEY;CTNAME");
				}
				break;		
				
			case 1640: // 요청 헤더 코드 Popup
				if("WMS".equals(activeApp) || "WM".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonWmsDAO.selectTWorkInfoReqCount(paramMap);
					}
					rtnList = commonWmsDAO.selectTWorkInfoReqHd(paramMap);
					rtnMap.put("FIELD", "REQHDKEY;OWKEY;REQUESTDATE;REQHDTYPE;ACKEY;ACNAME;STORE_KEY;STORE_NAME");
				}
				break;
			
			case 1650: // 화주사 Popup
				if("IBS".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectOwkeyCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectOwkeyCode(paramMap);
					rtnMap.put("FIELD", "OWKEY;OWNAME");
				}
				break;
				
			case 1660: // 계약 Popup
				if("IBS".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectContractNoCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectContractNo(paramMap);
					rtnMap.put("FIELD", "CONTRACT_NO;CONTRACT_NM;COMPANY_CD;COMPANY_NM;START_DT;END_DT;CMS_REMARK");
				}
				break;
				
			case 1670: // 운송사 Popup
				if("IBS".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectCarrierCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectCarrierCode(paramMap);
					rtnMap.put("FIELD", "CARRCD;CARRNM");
				}
				break;

			case 1680:
				// ADMIN
				if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
					count = commonDAO.selectTAdminMstIcCount2(paramMap);
					rtnList = commonDAO.selectTAdminMstIc2(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;OWKEY;ICUTKEY;UOM;LOGGRPCD");
				}
				break;
			case 1690: // ICKEY Popup
				if("IBS".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectIckeyCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectIckeyCode(paramMap);
					rtnMap.put("FIELD", "ICKEY;ICNAME;SPEC;PER");
				}
				break;
			case 1700: // 은행  Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectBankCodeCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectBankCode(paramMap);
					rtnMap.put("FIELD", "ACCOUNT_OWNER_NAME;ACCOUNT_BANK_CODE;ACCOUNT_BANK_NAME;ACCOUNT_NUMBER");
				}
				break;
			case 1705:
				count = commonWmsDAO.selectTWorkMstToLcCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstToLc(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;LCHOLDTYPE");
				break; // refTable : TWORK_MST_AR
			case 1710:
				count = commonWmsDAO.selectTWorkMstToLcBonCount(paramMap);
				rtnList = commonWmsDAO.selectTWorkMstToLcBon(paramMap);
				rtnMap.put("FIELD", "LCKEY;LCTYPE;LCHOLDTYPE");
				break; // refTable : TWORK_MST_AR
			case 1720: // 은행  Popup
				if("IBS".equals(activeApp) || "PTL".equals(activeApp)){
					if(!"combo".equals(type)){
						count = commonIbsDAO.selectAckeyCount(paramMap);
					}
					
					rtnList = commonIbsDAO.selectAckeyCode(paramMap);
					rtnMap.put("FIELD", "ACKEY;ACNAME");
				}
				break;
			default:
				rtnList = new ArrayList();
				break;
		}

		rtnMap.put("list", rtnList);
		rtnMap.put("count", Integer.toString(count));
		return rtnMap;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.common.service.CommonService#selectCenterSelect(java.util.Map)
	 */
	@Override
	public List selectCenterSelect(Map paramMap) throws Exception {
		//return commonWmsDAO.selectCenterSelect(paramMap);
		return commonWmsDAO.selectCenterSelect(paramMap);
	}

	/**
	 * SELECT EQUIPMENT LIST
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectEquipmentList(Map paramMap) throws Exception {
		List list = new ArrayList();
		String activeApp = (String) paramMap.get("activeApp");
//		String ctkey = (String) paramMap.get("ctkey");
		if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
			list = commonDAO.selectEquipmentList(paramMap);
		}
		else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
			list = commonWmsDAO.selectEquipmentList(paramMap);
		}
		else if ("ICOM".equals(activeApp)) {
			list = commonIcomDAO.selectEquipmentList(paramMap);
		}
		else if ("TMS".equals(activeApp)) {
			list = new ArrayList();
		}
		else if ("VIMS".equals(activeApp)) {
			list = new ArrayList();
		}
		else if ("KPI".equals(activeApp)) {
			list = new ArrayList();
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * ExcelUpload 에서 유효성 체크를 위해 사용함
	 */
	@Override
	public Map getAdmMasternCodeMap(Map paramMap) throws Exception {
		paramMap.put("activeApp", "ADMIN");
		paramMap.put("type", "combo");

		Map rtnMap = new HashMap();
		List tempList = (List) this.selectCommonCode(paramMap).get("list");
		for (int i = 0; i < tempList.size(); i++) {
			Map tempMap = (Map) tempList.get(i);
			rtnMap.put(MapUtil.getStr(tempMap, "field1"), "0");
		}
		
		return rtnMap;
	}

	@Override
	public String getAdmMulLangMsg(Map paramMap) throws Exception {
		Map argMap = (Map) paramMap.get("argmap");
		String msg = commonDAO.getAdmMulLangMsg(paramMap);
		String key = null;
		String regex = null;
		String replacement = null;
		if (argMap != null) {
			Set tmpSet = argMap.keySet();
			Iterator tmpIrt = tmpSet.iterator();

			while (tmpIrt.hasNext()) {
				key = tmpIrt.next().toString();
				regex = "#\\{" + key + "\\}";
				replacement = "[" + MapUtil.getStr(argMap, key) + "]";
				msg = msg.replaceAll(regex, replacement);
			}
		}
		return msg;
	}

	/**
	 * 시스템 환경 설정의 value1 추출.
	 * 
	 * @param cskey
	 * @return String
	 * @throws Exception
	 */
	@Override
	public String getAdmConfigVal(String cskey) throws Exception {

		/*
		 * Static Object 에서 정보 추출.
		 */
		Map map = AdmSysCfgStaticHashtable.getData(cskey);

		//메모리 상의 데이터가 없을 경우 DB 접속 하여 다시 한번 데이터 추출.
		if (map == null) {
			//ADM system config
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("apkey", "ADMIN");
			List<HashMap<String, String>> sysList = this.selectSystemConfigList(paramMap);
			AdmSysCfgStaticHashtable.setData(sysList);
		}

		String rtnVal = MapUtil.getStr(map, "value1");
		if (StringUtil.isEmpty(rtnVal)) {
			Map<String, Object> argMap = new HashMap<String, Object>();
			argMap.put("ADCSKEY", cskey);
			throw  new MsgException(112, argMap); //ADMIN 환경설정 코드 '#{ADCSKEY}' 가 존재 하지 않습니다.
		}

		return rtnVal;
	}
	
	/**
	 * 각 모듈 별 Code Table List 전송 
	 * @throws Exception
	 */
	@Override
	public void otherModuleAdmSysConfigSetting() throws Exception { 
		//  각  모듈 Config Amf Setting 
		CommandMap requestMap = new CommandMap();

		requestMap.putCommon("BEANID", "commonController");
		requestMap.putCommon("METHODNM", "setAdmSysConfigSetup");
		
		if ( "YES".equals(this.getAdmConfigVal("USEWM")) ) {
			requestMap = this.amfClient.execute(requestMap, EnvConstant.APP_WMS);
		}
		if ( "YES".equals(this.getAdmConfigVal("USEICOM")) ) {
			requestMap = this.amfClient.execute(requestMap, EnvConstant.APP_ICOM);
		}
	}
	
	@Override
	public Map checkDupLogin(Map param) throws Exception { 
		//  각  모듈 Config Amf Setting 
		Map rtnMap = new HashMap();
		LoginVO vo = new LoginVO();
		
		LOG.debug(MapUtil.getStr(param, "urkey"));
		
		vo.setUrKey(MapUtil.getStr(param, "urkey"));
		
		String storeUniqueAddr = userByRoleAdmDAO.selectIpaddrxUser(vo);
		String sessUniqueAddr = MapUtil.getStr(param, "sessUniqueAddr");
		
		if(StringUtil.isEmpty(storeUniqueAddr)) {
			rtnMap.put("duplLog", "N");
			vo.setUniqueAddr(sessUniqueAddr);
			userByRoleAdmDAO.updateUniqueAddr(vo);
		} else {
			if(StringUtil.equals(sessUniqueAddr, storeUniqueAddr)) {
				rtnMap.put("duplLog", "N");
			} else {
				rtnMap.put("duplLog", "Y");
			}
		}
		
		
		return rtnMap;
	}
	
	
	
	@Override
	public List selectNewBbcnMekey(Map paramMap) throws Exception {
		
		List list = new ArrayList();
		
		list = commonBbsDAO.selectNewBbcnMekey(paramMap);
		
		return list;
	}
	
	
	@Override
	public List selectMyMenuGrouping(Map paramMap) throws Exception {
		
		List list = new ArrayList();
		String activeApp = (String) paramMap.get("activeApp");

		if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
			//list = commonDAO.selectMyMenuGrouping(paramMap);
			//list =  myMenuSetupDAO.selectMyMenuGrouping(paramMap);
			list =  myMenuSetupDAO.selectMyMenuGroupingWithLevel(paramMap);
		}
		else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
			list = commonWmsDAO.selectMyMenuGroupingWithLevel(paramMap);
		}
		else if ("ICOM".equals(activeApp)) {
			list = commonIcomDAO.selectMyMenuGrouping(paramMap);
		}
		else if ("TMS".equals(activeApp)) {
			list = commonTmsDAO.selectMyMenuGrouping(paramMap);
		}
		else if ("VIMS".equals(activeApp)) {
			list = commonVimsDAO.selectMyMenuGrouping(paramMap);
		}
		else if ("KPI".equals(activeApp)) {
			list = commonKpiDAO.selectMyMenuGrouping(paramMap);
		}
		else if ("PMS".equals(activeApp)) {
			list = commonPmsDAO.selectMyMenuGrouping(paramMap);
		}
		
		
		return list;
		
	}
	
	
	@Override
	public List selectMyGridHeaderFormat(Map paramMap) throws Exception {
		
		List list = new ArrayList();
		String activeApp = (String) paramMap.get("activeApp");

		if ("ADMIN".equals(activeApp) || "ADM".equals(activeApp) || "AMS".equals(activeApp)) {
			list =  myGridHeaderFormatDAO.selectTAdminMstUrdfus(paramMap);
		}
		else if ("WM".equals(activeApp) || "WMS".equals(activeApp)) {
			list = commonWmsDAO.selectTWorkMstUrdfus(paramMap);
		}
		else if ("ICOM".equals(activeApp)) {
			list = commonIcomDAO.selectTOrderMstUrdfus(paramMap);
		}
		else if ("TMS".equals(activeApp)) {
			list = commonTmsDAO.selectTtMstUrdfus(paramMap);
		}
		else if ("VIMS".equals(activeApp)) {
			list = commonVimsDAO.selectTViewMstUrdfus(paramMap);
		}
		else if ("KPI".equals(activeApp)) {
			list = commonKpiDAO.selectTKpiMstUrdfus(paramMap);
		}
		else if ("PMS".equals(activeApp)) {
			list = commonPmsDAO.selectTPmsMstUrdfus(paramMap);
		} 
		else if ("IBS".equals(activeApp)) {
			list = commonIbsDAO.selectTIbsMstUrdfus(paramMap);
		}
		else if ("CMS".equals(activeApp)) {
			list = commonCmsDAO.selectTCmsMstUrdfus(paramMap);
		}
		else if ("FIS".equals(activeApp)) {
			list = commonFisDAO.selectTFisMstUrdfus(paramMap);
		}
		else if ("MDM".equals(activeApp)) {
			list = commonMdmDAO.selectTMdmMstUrdfus(paramMap);
		}
		
		return list;
		
	}

	@Override
	public Map selectGridInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = commonDAO.selectGridCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));

    	if (count > 0 ) {     		
        	List listTab1 = commonDAO.selectGridColumns(parameterMap);
        	if (listTab1 != null && !listTab1.isEmpty()) { 
        		List listTab2 = new ArrayList();
        		int lastIdx = 0;
        		
        		for(int i=0; i<listTab1.size(); i++) {
        			Map row = (Map)listTab1.get(i);
        			String dataindex = MapUtil.getStr(row, "columnname").toLowerCase();
        			row.put("dataindex",dataindex);
        			listTab2.add(row);
        			lastIdx = i;
        		}
        		
        		resultMap.put("list", listTab1);
        		
        		Map tempMap = new HashMap();
        		
        		tempMap.put("updf_dtkey", lastIdx);
        		tempMap.put("columnname", "IFSTATUS");
        		tempMap.put("columndesc", "IFSTATUS");
        		tempMap.put("dataindex", "ifstatus_desc");
        		tempMap.put("columntype", "S");
        		
        		listTab2.add(tempMap);
        		
        		tempMap = new HashMap();
        		tempMap.put("updf_dtkey", lastIdx+1);
        		tempMap.put("columnname", "IFMESSAGE");
        		tempMap.put("columndesc", "IFMESSAGE");
        		tempMap.put("dataindex", "ifmessage");
        		tempMap.put("columntype", "S");
        		
        		listTab2.add(tempMap);
        		
        		tempMap = new HashMap();
        		tempMap.put("updf_dtkey", lastIdx+2);
        		tempMap.put("columnname", "INSERTDATE");
        		tempMap.put("columndesc", "INSERTDATE");
        		tempMap.put("dataindex", "insertdate");
        		tempMap.put("columntype", "D");
        		
        		listTab2.add(tempMap);
        		
        		tempMap = new HashMap();
        		tempMap.put("updf_dtkey", lastIdx+3);
        		tempMap.put("columnname", "INSERTURKEY");
        		tempMap.put("columndesc", "INSERTURKEY");
        		tempMap.put("dataindex", "inserturkey");
        		tempMap.put("columntype", "S");
        		
        		listTab2.add(tempMap);
        		
        		tempMap = new HashMap();
        		tempMap.put("updf_dtkey", lastIdx+4);
        		tempMap.put("columnname", "ULHSKEY");
        		tempMap.put("columndesc", "ULHSKEY");
        		tempMap.put("dataindex", "ulhskey");
        		tempMap.put("columntype", "S");
        		
        		listTab2.add(tempMap);
        		
        		resultMap.put("list2", listTab2);

        	}
        	
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    		resultMap.put("list2", list);
    	}

		return resultMap;
	}	
	
 	//* 공공데이타포털(http://www.data.go.kr) 오픈 API 이용
    
    // 서비스명 : 통합검색 5자리 우편번호 조회서비스
    // 새 우편번호(2015-08-01부터) 오픈 API 주소
    // http://openapi.epost.go.kr/postal/retrieveNewAdressAreaCdSearchAllService/retrieveNewAdressAreaCdSearchAllService/getNewAddressListAreaCdSearchAll
    
    // [in] s : 검색어 (도로명주소[도로명/건물명] 또는 지5번주소[동/읍/면/리])
    // [in] p : 읽어올 페이지(1부터), l : 한 페이지당 출력할 목록 수(최대 50까지)
    // [out] v[i*3 +0]=우편번호, v[i*3 +1]=도로명주소, v[i*3 +2]=지번주소, v.Count/3=표시할 목록 수
    // [out] n[0]=검색한 전체 목록(우편번호) 개수, n[1]=읽어온 페이지(1부터)
    // 반환값 : 에러메시지, null == OK
	
	@Override
	public Map selectZipCode(Map parameterMap) throws Exception {
		
		 Map resultMap = new HashMap();
		 HttpURLConnection con = null;
         String s = (String)parameterMap.get("search");
         String s1 = "";
         if(s.indexOf(" ") > 0) {
        	 s1 = s.replace(" ","");
         }
         String currentPage = (String)parameterMap.get("currentPage");
         String pagingLimit = (String)parameterMap.get("pagingLimit");
         int l = 0;
         if(Integer.parseInt(pagingLimit) < 50) {
        	 l = Integer.parseInt(pagingLimit);
         } else {
        	 l = 50;
         }
         int p = Integer.parseInt(currentPage); 
         List v = new ArrayList();
         int[] n = new int[2] ;
         try
         {
         	URL url = new URL(
         	"http://openapi.epost.go.kr/postal/retrieveNewAdressAreaCdSearchAllService/retrieveNewAdressAreaCdSearchAllService/getNewAddressListAreaCdSearchAll"
             + "?ServiceKey=aLlkCp3T9GFWRu9QV84g93QSn0iIWS3irDJz6ART7ZFhmGuErzMOQty0X%2FRngQAUzSV7bgoHLSOSbUFJV7VbgQ%3D%3D" // 서비스키
             + "&countPerPage=" + l // 페이지당 출력될 개수를 지정(최대 50)
             + "&currentPage=" + p // 출력될 페이지 번호
             + "&srchwrd=" + URLEncoder.encode(s,"UTF-8") // 검색어
             );
             con = (HttpURLConnection) url.openConnection();
             con.setRequestProperty("Accept-language", "ko");
             
             DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
             DocumentBuilder bd = fac.newDocumentBuilder();
             Document doc = bd.parse(con.getInputStream());
             
             boolean bOk = false; // <successYN>Y</successYN> 획득 여부
             s = null; // 에러 메시지
             String nn;
             Node nd;
             NodeList ns = doc.getElementsByTagName("cmmMsgHeader");
             int i =0;
             if (ns.getLength() > 0) {
	                for (nd = ns.item(0).getFirstChild(); nd != null; nd = nd.getNextSibling()){
	                    nn = nd.getNodeName();
	                    if (!bOk){
	                        if (nn.equals("successYN")){ // 성공 여부
	                            if (nd.getTextContent().equals("Y")){
	                            	bOk = true; // 검색 성공
	                            } 
	                        }else if (nn.equals("errMsg")){ // 에러 메시지
	                            s = nd.getTextContent();
	                        }
	                    }else {
	                        if (nn.equals("totalCount")) { // 전체 검색수
	                            n[0] = Integer.parseInt(nd.getTextContent());
	                        }
	                        else if (nn.equals("currentPage")){ // 현재 페이지 번호
	                            n[1] = Integer.parseInt(nd.getTextContent());
	                        }
	                    }
	                }
             }
             if (bOk)
             {
                 ns = doc.getElementsByTagName("newAddressListAreaCdSearchAll");
                 for (p = 0; p < ns.getLength(); p++){
                 	Map zipMap = new HashMap();
                 	for (nd = ns.item(p).getFirstChild(); nd != null; nd = nd.getNextSibling()){
                 		zipMap.put(nd.getNodeName(), nd.getTextContent());
	                }
                 	v.add(zipMap);
                 }
                 resultMap.put("rtnGrid", v);
             }
             if (s == null) { // OK!
                 if (v.size() < 1)
                     s = "검색결과가 없습니다.";
             }
         }catch(Exception e){
             s = e.getMessage();
         }
         
         if (con != null){
         	con.disconnect();
         }
         	resultMap.put("cnt", n[0]);
         	resultMap.put("currentpage", n[1]);
			return resultMap;
	}	
	
	
}
