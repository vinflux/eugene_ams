/**
 * 
 */
package com.vinflux.adm.common.service;

import java.util.Map;

/**
 * @author goldbug
 *
 */
public interface CommonSearchService {
	
	/**
	 * tms common search 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCommonSearch (Map paramMap ) throws Exception;

}
