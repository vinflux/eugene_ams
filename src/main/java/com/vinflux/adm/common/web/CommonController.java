package com.vinflux.adm.common.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nexacro.spring.util.NexacroUtil;
import com.vinflux.adm.common.service.CommonSearchService;
import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.StaticManager;
import com.vinflux.adm.role.service.UserByRoleAdmService;
import com.vinflux.adm.system.service.MyGridHeaderFormatService;
import com.vinflux.adm.system.service.MyMenuSetupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.common.NoticeCodeStaticHashtable;
import com.vinflux.framework.common.push.service.MobileMessageService;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.web.CommandController;
import com.vinflux.wms.persistence.CommonWmsDAO;

import edu.emory.mathcs.backport.java.util.Arrays;


@Controller("commonController")
@RemotingDestination
public class CommonController extends CommandController {

	/** commonSearchService */
	@Resource(name = "commonSearchService")
    private CommonSearchService commonSearchService;
	
    /** commonService */
	@Resource(name = "commonService")
    private CommonService commonService;
	
    /** commonService */
	@Resource(name = "commonWmsDAO")
    private CommonWmsDAO commonWmsDAO;
	
	@Resource(name = "myGridHeaderFormatService")
    private MyGridHeaderFormatService myGridHeaderFormatService;
	
	
	@Resource(name = "userByRoleAdmService")
	private UserByRoleAdmService userByRoleAdmService;
	
	@Resource(name = "myMenuSetupService")
	private MyMenuSetupService myMenuSetupService;
	
	@Resource(name = "staticManager")
	private StaticManager staticManager;	
	
	@Resource(name = "mobileMessageService")
	private MobileMessageService mobileMessageService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(CommonController.class);

    /**
     * JSESSION ID INIT ADM 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/getJessionId.do")
    @ResponseBody   
    public CommandMap getJessionId(@RequestBody CommandMap requestMap) throws Exception{
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	requestMap.putParam("JSESSIONID", request.getSession().getId());
    	return requestMap;
    }   
    
    /**
     * get Notice 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/getNotice.do")
    @ResponseBody   
    public CommandMap getNotice(@RequestBody CommandMap requestMap) throws Exception{
    	requestMap.putMap("NOTICE_MAP", NoticeCodeStaticHashtable.getData());
    	return requestMap;
    }    
    
    @RequestMapping(value="/commonController/checkDupLogin.do")
    @ResponseBody   
    public CommandMap checkDupLogin(@RequestBody CommandMap requestMap) throws Exception{
    	
    	Map param = requestMap.getParamMap();
    	String urKey = (String)requestMap.getCommonUserInfoMap().get("urKey");
    	param.put("urkey", urKey);
    	
    	HttpServletRequest request = HttpUtil.getCurrentRequest();
    	param.put("sessUniqueAddr", request.getRemoteAddr());
    	
    	Map map  = commonService.checkDupLogin(param);
		
    	requestMap.putMap("DUPLLOG_MAP", map);
    	requestMap.setErrCode("0000");
    	return requestMap;
    } 

    /**
     * get Notice 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/setNoticeData.do")
    @ResponseBody   
    public CommandMap setNoticeData(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getParamMap();
    	commonService.setNoticeData(paramMap);
    	return requestMap;
    }    
    
    /**
     * APP/창고 별로 System config 가지고 오는 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/changeCenter.do")
    @ResponseBody   
    public CommandMap changeCenter(@RequestBody CommandMap requestMap) throws Exception{
    	LOG.debug("======================================");
    	LOG.debug("requestMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = new HashMap();
    	paramMap.put("urkey", requestMap.getCommonUserInfo("urKey"));
    	paramMap.put("ctkey", requestMap.getParam("ctkey"));
    	paramMap.put("ctkey_desc", requestMap.getParam("ctkey_desc"));
    	
    	commonService.changeCenter(paramMap);
    	return requestMap;
    }
    
    
    /**
     * APP/창고 별로 System config 가지고 오는 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectSystemConfigList.do")
    @ResponseBody   
    public CommandMap selectSystemConfigList(@RequestBody CommandMap requestMap) throws Exception{
    	
    	LOG.debug("======================================");
    	LOG.debug("requestMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	
    	List list = commonService.selectSystemConfigList(requestMap.getParamMap());
    	requestMap.addList("SYSTEM_CONFIG", list);

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	
    	return requestMap;
    }
    
    /**
     * APP 별로 다국어를 가지고 오는 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectAppMessage.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectAppMessage(@RequestBody CommandMap requestMap) throws Exception{
    	    	
    	Map paramMap = requestMap.getParamMap();

    	// Active App Setting 
    	String activeApp = (String)requestMap.getCommon("ACTIVE_APP");
    	paramMap.put("activeApp", activeApp);
    	String ctkey = (String)paramMap.get("ctkey");
    	if (ctkey == null || "".equals(ctkey) ) { 
        	Map commonMap = requestMap.getCommonMap();
        	Map userInfoMap = (Map)commonMap.get("USER_INFO");
        	String userInfoCtkey = (String)userInfoMap.get("ctKey");
        	paramMap.put("ctkey", userInfoCtkey);
    	}
    	
    	paramMap.put("msgtype" , "TEXT");
    	requestMap.addList("appMessageTxt", commonService.selectAppMessage(paramMap));
    	paramMap.put("msgtype" , "MSG");
    	requestMap.addList("appMessageMsg", commonService.selectAppMessage(paramMap));    	
    	
    	return requestMap;
    }
    
    /**
     * module menu list select
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectModuleMenu.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectModuleMenu(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	Map map = commonService.selectModuleMenu(paramMap);
    	
    	requestMap.putMap("menuStructure", map);
    	return requestMap;
    }

    @RequestMapping(value="/commonController/selectModuleMenuNexacro.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectModuleMenuNexacro(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	List list = commonService.selectModuleMenuNexacro(paramMap);
    	requestMap.addList("MENU", list);
    	return requestMap;
    }
    
    /**
     * menu search condition select
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectUserSearchCondtion.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectUserSearchCondtion(@RequestBody CommandMap requestMap) throws Exception{
    	
    	Map paramMap = requestMap.getParamMap();
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	paramMap.put("activeApp", requestMap.getCommon("ACTIVE_APP"));
    	
    	Map map = commonService.selectUserSearchCondtion(paramMap);
    	
    	requestMap.addList("SRCH_CONDITION", (List)map.get("SRCH_CONDITION"));
    	requestMap.addList("SRCH_CONDITION2", (List)map.get("SRCH_CONDITION2"));
    	requestMap.addList("CODE_LIST", (List)map.get("CODE_LIST"));
    	
    	return requestMap;
    }    
    
	/**
	 * 공통 검색 Popup 을 위한 마스터  Code를 가지고 오는 구문 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value="/commonController/selectCommonCode.do")
    @ResponseBody   
    public CommandMap selectCommonCode (@RequestBody CommandMap requestMap) throws Exception  {
    	
    	LOG.debug("======================================");
    	LOG.debug("requestMap => " + requestMap);
    	LOG.debug("======================================");
    	
		Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);    	
		
    	// Active App Setting 
    	String device = (String)requestMap.getCommon("DEVICE");
    	String activeApp =  (String)requestMap.getCommon("ACTIVE_APP");
    	
    	paramMap.put("activeApp", activeApp);
    	paramMap.put("device", device);
    	
    	List OWKEY_SELECTION_VALUE = requestMap.getList("OWKEY_SELECTION_LIST");
    	if (OWKEY_SELECTION_VALUE != null && !OWKEY_SELECTION_VALUE.isEmpty()) {
    		paramMap.put("OWKEY_SELECTION_VALUE", OWKEY_SELECTION_VALUE);
    	}
    	
    	List ICKEY_SELECTION_VALUE = requestMap.getList("ICKEY_SELECTION_VALUE");
    	if (ICKEY_SELECTION_VALUE != null && !ICKEY_SELECTION_VALUE.isEmpty()) {
    		paramMap.put("ICKEY_SELECTION_VALUE", ICKEY_SELECTION_VALUE);
    	}
    	
    	List CTKEY_SELECTION_VALUE = requestMap.getList("CTKEY_SELECTION_LIST");
    	if (CTKEY_SELECTION_VALUE != null && !CTKEY_SELECTION_VALUE.isEmpty()) {
    		paramMap.put("CTKEY_SELECTION_VALUE", CTKEY_SELECTION_VALUE);
    	}
    	
   	    List APKEY_SELECTION_LIST = requestMap.getList("APKEY_SELECTION_LIST");
    	if (APKEY_SELECTION_LIST != null && !APKEY_SELECTION_LIST.isEmpty()) {
    		paramMap.put("APKEY_SELECTION_LIST", APKEY_SELECTION_LIST);
    	}
    	

    	String loggrpcd = requestMap.getCommonUserInfo("loggrpcd");
    	if (loggrpcd != null && !"".equals(loggrpcd) ) {
//    		List<String> loggrpcdList = new ArrayList<String>();
//    		String loggrpcdArr[] = loggrpcd.split(",");
//    		for (int i=0 ; loggrpcdArr.length > i ; i++ ) {
//    			loggrpcdList.add(loggrpcdArr[i]);
//    		}
    		
    		String loggrpcdArr[] = loggrpcd.split(",");
    		List<String> loggrpcdList = Arrays.asList(loggrpcdArr);
    		paramMap.put("LOGGRPCD_SELECTION_VALUE", loggrpcdList);
    	}
    	
    	String rep_ackey = requestMap.getCommonUserInfo("rep_ackey");
    	String uskey = (String)requestMap.getCommon("uskey");
    	paramMap.put("rep_ackey", rep_ackey);
    	paramMap.put("uskey", uskey);
    	LOG.debug("======================================");
    	LOG.debug("paramMap => " + paramMap);
    	LOG.debug("======================================");	
    	
    	Map map = commonService.selectCommonCode(paramMap);
		
		requestMap.addList("COMMON_CODE_LIST" ,  (List)map.get("list"));
		requestMap.putParam("COUNT" , (String)map.get("count"));
		requestMap.putParam("COMMON_CODE_FIELD" , (String)map.get("FIELD"));
		
    	LOG.debug("======================================");
    	//LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    } 
    
	/**
	 * 공통 검색 Popup 을 위한 마스터  Code를 가지고 오는 구문 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value="/commonController/selectCenterSelect.do")
    @ResponseBody   
    public CommandMap selectCenterSelect (@RequestBody CommandMap requestMap) throws Exception  {
    	Map paramMap = requestMap.getParamMap();
		List list = commonService.selectCenterSelect(paramMap);
		requestMap.addList("CENTER_LIST" ,  list);
    	return requestMap;
    } 
    
    /**
     * 모듈 로딩 전체 데이터를 가지고 오는 구문 
     * 다국어 , 시스템 환경 설정 , 검색조건 , 메뉴 , Role 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value="/commonController/selectModuleConfig.do")
    @ResponseBody   
    public CommandMap selectModuleConfig (@RequestBody CommandMap requestMap) throws Exception  {
    	Map paramMap = requestMap.getParamMap();
    	Map commonMap = requestMap.getCommonMap();
    	
    	//파라미터에 laKey 로 세팅할경우 appMessageTxt에 다국어 값이 세팅이 안됨
    	//파라미터에 lakey 로 세팅할경우 로그인시 문제 발생
    	
    	Object laKey = paramMap.get("laKey");
    	Object lakey = paramMap.get("lakey");
    	
    	if(NexacroUtil.isNull(laKey)){
    		if(NexacroUtil.isNull(lakey)){
    			//lakey가 없음 딮폴트 값 설정
    			lakey ="ENG";
    		}else{
    			paramMap.put("laKey", lakey.toString());
    		}
    	}else{
    		paramMap.put("lakey", laKey.toString());
    	}
    	
    	/**
    	 * Active APP Check 
    	 */

    	// Client Active App    
    	String activeApp = (String)requestMap.getCommon("ACTIVE_APP");
    	if ("WMS".equals(activeApp) ) { 
    		activeApp = "WM";
    	}
    	if ("AMS".equals(activeApp) ) { 
    		activeApp = "ADMIN";
    	}
    	
    	Map appRoleMap  = userByRoleAdmService.selectAppKeyRole(paramMap);
    	List appKeyRoleList = (List)appRoleMap.get("appKeyRole");

    	// active app check
    	if (appKeyRoleList != null && !appKeyRoleList.isEmpty() ) { 
        	boolean activeAppChk = false;
        	int size = appKeyRoleList.size();
        	for (int i=0 ; size > i ; i++ ) {
        		Map row = (Map)appKeyRoleList.get(i);
        		String apkey = MapUtil.getStr(row, "appKey");
        		if (apkey.equals(activeApp) ) {
        			activeAppChk = true;
        			break;
        		}
        	}
        	
        	if (!activeAppChk ) {
        		
        		Map row = (Map)appKeyRoleList.get(0);
        		activeApp = MapUtil.getStr(row, "appKey");
        		//commonMap.put("ACTIVE_APP", activeApp);
        		commonMap.put("ACTIVE_APP", activeApp);
        		commonMap.put("APKEY", activeApp);
        		
        		//해당 어플리케이션에 대한 권한이 불충분 합니다. 사용이 불가능한 유저 입니다.
        		//throw new MsgException("해당 어플리케이션에 대한 권한이 불충분 합니다. 사용이 불가능한 유저 입니다.");
        	}
    	}else{
    		throw new MsgException(00117);
    	}
    	
    	paramMap.put("activeApp", activeApp);
    	paramMap.put("apkey", activeApp);
    	paramMap.put("appKey", activeApp);
    	String ctkey = (String)paramMap.get("ctkey");
    	if (ctkey == null || "".equals(ctkey) ) { 
        	Map userInfoMap = (Map)commonMap.get("USER_INFO");
        	String userInfoCtkey = (String)userInfoMap.get("ctKey");
        	paramMap.put("ctkey", userInfoCtkey);
    	}

		/**
		 * ────────────────────────────────────
		 * 다국어   
		 * ────────────────────────────────────
		 */

    	// 2018.08.03 : KSH - GMT옵션 사용 여부 추가.(로그인 시 로컬스토리지 저장을 위한 구문)
    	Map paramgmtMap = new HashMap();
		paramgmtMap.put("sckey", "GMTUSEYN"); 
		List gmtList = commonService.selectSystemConfigList(paramgmtMap);
		String gmtUseYn = "N"; // Defalut 'N'
				
		if(gmtList != null && !gmtList.isEmpty()){
		  Map row = (Map)gmtList.get(0);
		  gmtUseYn = (String)row.get("value1");
		}
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req);
		sessionVO.setGmtUseYn(gmtUseYn);    	
    	
    	LOG.debug("다국어 관련 => " + paramMap);
    	
    	paramMap.put("msgtype" , "TEXT");
    	requestMap.addList("appMessageTxt", commonService.selectAppMessage(paramMap));
    	LOG.debug("다국어 관련 => " + requestMap.toString());

    	
    	paramMap.put("msgtype" , "MSG");
    	requestMap.addList("appMessageMsg", commonService.selectAppMessage(paramMap));    	
    	
		/**
		 * ────────────────────────────────────
		 * 시스템 환경 조건  
		 * ────────────────────────────────────
		 */
    	List list = commonService.selectSystemConfigList(paramMap);
    	requestMap.addList("SYSTEM_CONFIG", list); 
    	
		/**
		 * ────────────────────────────────────
		 * ADMIN 시스템 환경 조건  
		 * ────────────────────────────────────
		 */
    	List admList = null;
    	String apkey = (String) paramMap.get("apkey");
    	
    	if("ADMIN".equals(apkey)){
    		admList = list;
    	}else{
    		admList = commonService.selectAdmSystemConfigList(paramMap);
    	}
    	
    	requestMap.addList("ADM_SYSTEM_CONFIG", admList); 
    	
    	
		/**
		 * ────────────────────────────────────
		 * 검색 조건  
		 * ────────────────────────────────────
		 */
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	paramMap.put("activeApp", requestMap.getCommon("ACTIVE_APP"));
    	Map searchMap = commonService.selectUserSearchCondtion(paramMap);
    	requestMap.addList("SRCH_CONDITION", (List)searchMap.get("SRCH_CONDITION"));
    	requestMap.addList("SRCH_CONDITION2", (List)searchMap.get("SRCH_CONDITION2"));
    	requestMap.addList("CODE_LIST", (List)searchMap.get("CODE_LIST"));
    	
		/**
		 * ────────────────────────────────────
		 * 메뉴 
		 * ────────────────────────────────────
		 */
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	//Map menuMap = commonService.selectModuleMenu(paramMap);
    	//requestMap.putMap("menuStructure", menuMap);
    	List nexacroMenuList = commonService.selectModuleMenuNexacro(paramMap);
    	requestMap.addList("MENU_LIST", nexacroMenuList);
		/**
		 * ────────────────────────────────────
		 * ROLE
		 * ────────────────────────────────────
		 */
    	Map menuObjectRoleMap  = userByRoleAdmService.selectMenuObjectKeyRole(paramMap);
    	Map whRoleMap     	   = userByRoleAdmService.selectCenterKeyRole(paramMap);
    	Map ownerRoleMap  	   = userByRoleAdmService.selectOwnerKeyRole(paramMap);
		requestMap.addList("rtnWHRole" , (ArrayList)whRoleMap.get("whKeyRole"));
		requestMap.addList("rtnOwnerRole" , (ArrayList)ownerRoleMap.get("ownerKeyRole"));
		requestMap.addList("rtnAppRole" , (ArrayList)appRoleMap.get("appKeyRole"));
		requestMap.addList("rtnMenuObjectRole" , (ArrayList)menuObjectRoleMap.get("menuObjectKeyRole"));
		
		if ("WM".equals(activeApp)) {
	    	List acXUrList = commonWmsDAO.selectTWorkMstAcXUr(paramMap);
			requestMap.addList("ACXUR_LIST" , acXUrList);
		}
		
		/**
		 * ────────────────────────────────────
		 * 장비 리스트 
		 * ────────────────────────────────────
		 */
    	List eqList = commonService.selectEquipmentList(paramMap);
    	requestMap.addList("EQ_LIST", eqList);
    	
		/** ────────────────────────────────────
		 * BGF Version . Mobile 용 상저온 구분 코드  
		 ──────────────────────────────────── */
    	String device = (String)requestMap.getCommon("DEVICE");
    	if( "MOBILE".equals(device)) {
    		
    		paramMap.put("searchid", "20");
    		paramMap.put("activeApp", EnvConstant.APP_WMS);
    		paramMap.put("ctcd_hdkey", "LOGGRPCD");
    		paramMap.put("type", "combo");
    		paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    		
        	Map map = commonService.selectCommonCode(paramMap);
        	List loggrpcdList = (List)map.get("list");
        	requestMap.addList("LOGGRPCD_LIST", loggrpcdList);
    	}
    	
    	
    	/**
    	 * 
    	 * @author 미래웹 박무형 선임컨설턴트
    	 * selectMyGridMenu통합작업
    	 * 
    	 */
    	List menuList = commonService.selectMyMenuGrouping(requestMap.getParamMap());
    	requestMap.addList("USER_MENU", menuList);
    	
    	
    	/**
    	 * 
    	 * @author 미래웹 박무형 선임컨설턴트
    	 * selectMyGridHeaderFormat통합작업
    	 * 
    	 */
    	List gridHeader = commonService.selectMyGridHeaderFormat(requestMap.getParamMap());
    	requestMap.addList("USER_GRID", gridHeader);
    	
    	
    	
    	return requestMap;
    } 
    
    /**
     * 모듈 로딩 전체 데이터를 가지고 오는 구문 
     * 다국어 , 시스템 환경 설정 , 검색조건 , 메뉴 , Role 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectModuleMobRoleConfig.do")
    @ResponseBody   
    public CommandMap selectModuleMobRoleConfig (@RequestBody CommandMap requestMap) throws Exception  {

    	Map paramMap = requestMap.getParamMap();
    	
    	// role
    	Map menuObjectRoleMap  = userByRoleAdmService.selectMenuObjectKeyRole(paramMap);
    	Map appRoleMap    	   = userByRoleAdmService.selectAppKeyRole(paramMap);
    	Map whRoleMap     	   = userByRoleAdmService.selectCenterKeyRole(paramMap);
    	Map ownerRoleMap  	   = userByRoleAdmService.selectOwnerKeyRole(paramMap);
		requestMap.addList("rtnWHRole" , (ArrayList)whRoleMap.get("whKeyRole"));
		requestMap.addList("rtnOwnerRole" , (ArrayList)ownerRoleMap.get("ownerKeyRole"));
		requestMap.addList("rtnAppRole" , (ArrayList)appRoleMap.get("appKeyRole"));
		requestMap.addList("rtnMenuObjectRole" , (ArrayList)menuObjectRoleMap.get("menuObjectKeyRole"));
    	
    	return requestMap;
    }     

    /**
     * 각 모듈에서  Config가 변경시 Admin 모듈 메모리 변경을 위한 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/setConfigSetup.do")
    @ResponseBody   
    public CommandMap setConfigSetup(@RequestBody CommandMap requestMap) throws Exception{
    	
    	String appCode = requestMap.getParam("APPCODE");
    	List list = requestMap.getList("DATA_LIST");
    	commonService.setConfigSetup(appCode, list);
    	return requestMap;
    }

    /**
     * 각 모듈에서  Config가 변경시 Admin 모듈 메모리 변경을 위한 구문 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/setCommonCodeSetup.do")
    @ResponseBody   
    public CommandMap setCommonCodeSetup(@RequestBody CommandMap requestMap) throws Exception{
    	
    	String appCode = requestMap.getParam("APPCODE");
    	List list = requestMap.getList("DATA_LIST");
    	commonService.setCommonCodeSetup(appCode, list);
    	return requestMap;
    }
    
    /**
     * 모듈 로딩 전체 데이터를 가지고 오는 구문 
     * 다국어 , 시스템 환경 설정 , 검색조건 , 메뉴 , Role 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectModuleMobConfig.do")
    @ResponseBody   
    public CommandMap selectModuleMobConfig (@RequestBody CommandMap requestMap) throws Exception  {

    	Map paramMap = requestMap.getParamMap();
    	Map commonMap = requestMap.getCommonMap();
    	
    	// 다국어   
    	String activeApp = (String)requestMap.getCommon("ACTIVE_APP");
    	// 디바이스 
//    	String device = (String)requestMap.getCommon("DEVICE");			// 현재 사용되지 않아 주석처리함
    	
    	paramMap.put("activeApp", activeApp);
    	String ctkey = (String)paramMap.get("ctkey");
    	if (ctkey == null || "".equals(ctkey) ) { 
        	Map userInfoMap = (Map)commonMap.get("USER_INFO");
        	String userInfoCtkey = (String)userInfoMap.get("ctKey");
        	paramMap.put("ctkey", userInfoCtkey);
    	}
    	
    	paramMap.put("msgtype" , "TEXT");
    	requestMap.addList("appMessageTxt", commonService.selectAppMessage(paramMap));
    	paramMap.put("msgtype" , "MSG");
    	requestMap.addList("appMessageMsg", commonService.selectAppMessage(paramMap));    	
    	
    	// 시스템 환경 설정
    	List list = commonService.selectSystemConfigList(paramMap);
    	requestMap.addList("SYSTEM_CONFIG", list);    	
    	
    	// 검색조건 
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	paramMap.put("activeApp", requestMap.getCommon("ACTIVE_APP"));
    	paramMap.put("apkey", requestMap.getCommon("ACTIVE_APP"));

    	Map menuMap = commonService.selectModuleMenu(paramMap);
    	requestMap.putMap("menuStructure", menuMap);
    	
    	return requestMap;
    }     

    
    /**
     * Sencha 차트 및 출력용 컴포넌트
     * 
     * Sencha 내부적으로 이미지 파일을 Base64로 인코딩 시켜서 보내준다.
     * 여기에서는 base64파일을 다시 이미지화시켜 파일을 다시만들어서 다운로드 시켜준다.
     * @author  Miraeweb 박무형 선임컨설턴트
     * 
     * */
    @RequestMapping(value="/commonController/senchaImageDown.do", method=RequestMethod.POST)
    
    public void senchaDown(HttpServletRequest request, HttpServletResponse response) throws Exception  {
    	String data = request.getParameter("data"); // ExtJS에서 넘겨주것으로 Binary데이터가 Base64로 인코딩되어서 넘어온다.
    	String fileName = request.getParameter("fileName"); //ExtJS에서 넘어오는 값으로 파일이름
    	String format = request.getParameter("format");		//파일확장자
    	
    	if("".equals(fileName) || fileName==null){
    		fileName ="ChartImage";
    	}
    	
    	if("".equals(format) || format==null){
    		format="png";
    	}
    	
    	
    	
    	// Base64코드를 Binary로 변환뒤 InputStream에 저장
    	int startOfBase64Data = data.indexOf(",") + 1;
        byte[] imagefile = Base64.decodeBase64(data.substring(startOfBase64Data,data.length()));
    	InputStream is  = new ByteArrayInputStream(imagefile);

    	
    	//파일 확장자에따른 mimeType 설정
    	Map<String,String> formatMap = new HashMap<String,String>();
    	formatMap.put("png", "image/png");
    	formatMap.put("pdf", "application/pdf");
    	formatMap.put("jpeg", "image/jpeg");
    	formatMap.put("gif", "image/gif");
    	String mimeType = formatMap.get(format);
    	
  
    	// Response Header 세팅
        response.setContentType(mimeType); 
        response.setHeader("Content-Disposition", "attachment; filename="
                + new String(fileName.getBytes("UTF-8"), "ISO-8859-1")  +"."+format);

        //파일다운로드 로직
        OutputStream os = response.getOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        os.flush();
        os.close();
        is.close();
    }
    
    
    
	/**
	 * 공통 ExtJS GridPanel에서 지원 하는 excel 양식으로 엑셀을 생성하는 구문  
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value="/commonController/excelDown.do", method=RequestMethod.POST)
    @ResponseBody   
    public void excelDown(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		
	       String fileName = request.getParameter("xlsTitle");
	       
	       fileName = fileName.replaceAll(" ", "_");
	       
	       String charset = "UTF-8";
	       //String language = request.getLocale().getLanguage();
	       
	       response.setCharacterEncoding(charset);
	       response.setContentType("application/vnd.ms-excel; charset=" + charset);
	       response.setHeader("Content-Encoding",charset);
	       response.setHeader("Content-Description", "JSP Generated Data");
	       response.setHeader("Content-Disposition", "attachment; filename="+new String(fileName.getBytes("UTF-8"), "ISO-8859-1") +".xls");
	       PrintWriter fout = response.getWriter();
	      
	       String strXls = request.getParameter("xlsParam");

	       strXls = strXls.replaceAll("&lt;", "<");
	       strXls = strXls.replaceAll("&quot;", "\"");
	       strXls = strXls.replaceAll("&gt;", ">");
	       
	       fout.write(strXls);
	 
	       fout.close();
	}
	

	/**
	 * 현재 xls 파일만 읽음.
	 * 자료를 찾아보니 xls 와 xlsx 파싱하는 Object 가 다른것으로 판단됨.
	 * xls => POIFSFileSystem , SSFWorkbook 
	 * xlsx => XSSFWorkbook 
	 * 
	 * 아래 링크 참고 하여 추가 개발 하면 될것으로 보임.
	 * http://enosent.tistory.com/30
	 */
    @RequestMapping(value="/commonController/excelUp.do", method=RequestMethod.POST)
    @ResponseBody   
	public void excelUp(HttpServletRequest request) throws Exception  {
	    
    	MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multiRequest.getFileMap();				
		LOG.debug("###fileMap=>"+fileMap);
				

	    Iterator<Entry<String, MultipartFile>> itr = fileMap.entrySet().iterator();
		MultipartFile file;
		InputStream fis = null;
	    
		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();

			file = entry.getValue();
			if (!"".equals(file.getOriginalFilename())) {

				if (file.getOriginalFilename().endsWith(".xls")
						|| file.getOriginalFilename().endsWith(".xlsx")
						|| file.getOriginalFilename().endsWith(".XLS")
						|| file.getOriginalFilename().endsWith(".XLSX")) {
					

					try {
						fis = file.getInputStream();
						
						POIFSFileSystem fs = new POIFSFileSystem(fis);		// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
						HSSFWorkbook wb = new HSSFWorkbook(fs);				// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
				        HSSFSheet sheet = wb.getSheetAt(0);
				        
				        long rowCnt = sheet.getPhysicalNumberOfRows();
				        int colCnt = 0;
				        
				        for (int i=0; i<rowCnt; i++){
				        	colCnt = sheet.getRow(i).getPhysicalNumberOfCells();
				        	for (int j=0; j<colCnt; j++){
				        		LOG.debug("##"+i+"열 |||"+j+"행=>"+sheet.getRow(i).getCell(j));
				        	}
				        }				       
				        
					} catch(Exception e) {
						throw e;
					} finally {
						if (fis != null){	// 2011.11.1 보안점검 후속조치
							fis.close();
						}
					}
					
				}else{
					LOG.info("xls, xlsx 파일 타입만 등록이 가능합니다.");
				}
				// *********** 끝 ***********
		    	
			}
		}		
	}
    
    /**
     * 개인화 데이타 조회(그리드,메뉴)
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectMyGridMenu.do")
    @ResponseBody   
    public CommandMap selectMyGridMenu(@RequestBody CommandMap requestMap) throws Exception{
    	
    	//개인 그리드 포맷 가져오기
    	//List gridList = myGridHeaderFormatService.selectMyGridHeaderFormat(requestMap.getParamMap());
    	//requestMap.addList("USER_GRID", gridList);
    	
    	//개인 메뉴 가져오기
    	List menuList = myMenuSetupService.selectMyMenuGrouping(requestMap.getParamMap());
    	requestMap.addList("USER_MENU", menuList);
    	
    	List list = myGridHeaderFormatService.selectMyGridHeaderFormat(requestMap.getParamMap());
    	requestMap.addList("USER_GRID", list);
    	
    	return requestMap;
    }
   
    /**
     * StaticManager 메모리 갱신. 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/setStaticCache.do")
    @ResponseBody   
    public CommandMap setStaticCache(@RequestBody CommandMap requestMap) throws Exception{
    	
    	//메모리 관리 정보 갱신.
    	this.staticManager.init();
    	return requestMap;
    }
    
    @RequestMapping(value="/commonController/selectCommonSearch.do")
    @ResponseBody   
    public CommandMap selectCommonSearch (@RequestBody CommandMap requestMap) throws Exception  {
    	
    	if (LOG.isDebugEnabled()) {
	    	LOG.debug("======================================");
	    	LOG.debug("resultMap => " + requestMap);
	    	LOG.debug("======================================");
    	}
    	
		Map map = commonSearchService.selectCommonSearch(requestMap.getParamMap());
		
		requestMap.addList("COMMON_CODE_LIST" ,  (List)map.get("list"));
		requestMap.putParam("COUNT" , (String)map.get("count"));
		requestMap.putParam("COMMON_CODE_FIELD" , (String)map.get("FIELD"));
		
    	if (LOG.isDebugEnabled()) {
			LOG.debug("======================================");
	    	LOG.debug("resultMap => " + requestMap);
	    	LOG.debug("======================================");
    	}
    	
    	return requestMap;
    }
    
    @RequestMapping(value="/commonController/selectNewBbcnMekey.do")
    @ResponseBody 
    public CommandMap selectNewBbcnMekey (@RequestBody CommandMap requestMap) throws Exception  {
    	
    	List newBbsMekeyList = commonService.selectNewBbcnMekey(requestMap.getParamMap());
    	requestMap.addList("NEW_BBS_MEKEY", newBbsMekeyList);
    	
    	return requestMap;
    }
    
    /**
     * 푸쉬메시지 전송
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/commonController/sendMsg.do")
    @ResponseBody   
    public CommandMap sendMsg (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("###################################################");
    	LOG.debug("EAI DPS PICKING CONFIRM START | packet :" + requestMap);
    	LOG.debug("###################################################");
    	
    	// BASE EAI packet argument setting 
    	requestMap.putCommon("ERROR_CODE", "0000");
    	requestMap.putCommon("MESSAGE", "SUCCESS");
    	requestMap.putCommon("MESSAGE_ARG", "");
    	requestMap.putCommon("MESSAGE_CODE", "00000");
    	
    	try {
//    		Map paramMap = requestMap.getParamMap();		// 사용되지 않으므로 주석처리함.	
        	mobileMessageService.pushNotify();  		
    	} catch (Exception e) {
			Throwable throwable = e.getCause();
			logger.error("### Interface Exception Errror=>"+ e.getMessage(), e);
	    	requestMap.putCommon("ERROR_CODE", "9999");
			if ( throwable instanceof MsgException ){
				logger.error("### MSG CODE=>"+ ((MsgException)throwable).getCode());
				MsgException msgException  =(MsgException)throwable;
		    	requestMap.putCommon("MESSAGE", msgException.getMessage());
				requestMap.putCommon("MESSAGE_ARG", msgException.getArgMap());
		    	requestMap.putCommon("MESSAGE_CODE", msgException.getCode());
			} else {
		    	requestMap.putCommon("MESSAGE", "SYSTEM ERROR");
				requestMap.putCommon("MESSAGE_ARG", "");
		    	requestMap.putCommon("MESSAGE_CODE", "9999");
			}
		}

    	LOG.debug("###################################################");
    	LOG.debug("EAI DPS PICKING CONFIRM  packet :" + requestMap);
    	LOG.debug("###################################################");    	

    	return requestMap;
    }
    
    /**
     * 엑셀업로드 팝업 그리드 컬럼 조회
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/commonController/selectGridInfo.do")
    @ResponseBody   
    public CommandMap selectGridInfo (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map gridMap = commonService.selectGridInfo(paramMap);
    	Integer count = MapUtil.getInt(gridMap, "cnt");
    	if (count > 0 ) {
    		requestMap.addList("grdColList", (ArrayList)gridMap.get("list"));
    		requestMap.addList("grdColList2", (ArrayList)gridMap.get("list2"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
	
    
    /**
	 * 우편번호 조회 공통 Popup 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value="/commonController/selectZipCode.do")
    @ResponseBody   
    

    	public CommandMap selectZipCode (@RequestBody CommandMap requestMap) throws Exception  {
    	LOG.debug("======================================");
    	LOG.debug("selectZipCode__requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	String p = MapUtil.getStr(param, "search");
    	p = p.replace("-", " ");
    	param.put("search", p);
    	paramMap.putAll(param);    	
    	
    	Map gridMap = commonService.selectZipCode(paramMap);
    	Integer count = MapUtil.getInt(gridMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)gridMap.get("rtnGrid"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectZipCode_resultMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
 
        }

}
