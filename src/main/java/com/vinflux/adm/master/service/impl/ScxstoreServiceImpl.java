package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.ScxstoreService;
import com.vinflux.adm.persistence.ScxstoreDAO;

@Service("scxstoreService")
public class ScxstoreServiceImpl implements ScxstoreService {
	
	@Resource(name="scxstoreDAO")
	private ScxstoreDAO scxstoreDAO;
	
//	@Resource(name="scxstoreIcomDAO")
//	private ScxstoreIcomDAO scxstoreIcomDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectScxstoreInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = scxstoreDAO.selectScxstoreCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = scxstoreDAO.selectScxstore(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectScxstore (Map parameterMap) throws Exception {
		return scxstoreDAO.selectPkScxstore(parameterMap);
	}
	
	@Override
	public int checkScxstoreCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return scxstoreDAO.checkScxstoreCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.ScxstoreService#insertScxstore(java.util.Map)
	 */
	@Override
	public void insertScxstore (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.scxstoreDAO.insertTAdminMstScxstore(parameterMap);
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.scxstoreIcomDAO.insertTOrderMstScxstore(parameterMap);	
		}*/
	}
	
	/** **/
	@Override
	public void updateScxstore (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.scxstoreDAO.updateTAdminMstScxstore(parameterMap);
		
//		int count = 0;
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = this.scxstoreIcomDAO.updateTOrderMstScxstore(parameterMap);
			if(count==0){
				this.scxstoreIcomDAO.updateTOrderMstScxstore(parameterMap);
			}
		}*/
	}

	/** **/
	@Override
	public void deleteScxstore (Map parameterMap) throws Exception {
		// ICOM DELETE
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.scxstoreIcomDAO.deleteTOrderMstScxstore(parameterMap);
		}*/
		
		// ADMIN DELETE
		this.scxstoreDAO.deleteTAdminMstScxstore(parameterMap);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map searchStoreInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = scxstoreDAO.searchStoreCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = scxstoreDAO.searchStore(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}

	/** **/
	@Override
	public List selectExcelDownScxstore (Map parameterMap) throws Exception {
    	return scxstoreDAO.selectExcelDownScxstore(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownScxstore(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return scxstoreDAO.selectExcelDownScxstore(parameterMap, excelFormat);
	}
}