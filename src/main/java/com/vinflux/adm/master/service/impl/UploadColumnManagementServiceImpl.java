package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.master.service.UploadColumnManagementService;
import com.vinflux.adm.persistence.UploadColumnManagementDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.tms.persistence.AccountTmsDAO;
import com.vinflux.wms.persistence.AccountWmsDAO;



@Service("uploadColumnManagementService")
public class UploadColumnManagementServiceImpl implements UploadColumnManagementService {
	
	@Resource(name="uploadColumnManagementDAO")
	private UploadColumnManagementDAO uploadColumnManagementDAO;

//	@Resource(name="accountIcomDAO")
//	private AccountIcomDAO accountIcomDAO;
	
	@Resource(name="accountWmsDAO")
	private AccountWmsDAO accountWmsDAO;
	
	@Resource(name="accountTmsDAO")
	private AccountTmsDAO accountTmsDAO;
	
//	@Resource(name="accountVimsDAO")
//	private AccountVimsDAO accountVimsDAO;
	
	
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Resource(name="txAdmManager")
	private DataSourceTransactionManager txAdmManager;
	
//	@Resource(name = "entityAddrService")
//	private EntityAddrService entityAddrService;	
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccountInfo(java.util.Map)
	 */
	@Override
	public Map selectUploadColumnHeaderInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = uploadColumnManagementDAO.selectUploadColumnHeaderInfoCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = uploadColumnManagementDAO.selectUploadColumnHeaderInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map selectUploadColumnDetailInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	List list = uploadColumnManagementDAO.selectUploadColumnDetailInfo(parameterMap);
    	resultMap.put("list", list);
		return resultMap;
	}	

	@Override
	public int checkSave(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return uploadColumnManagementDAO.selectUploadColumnHDForUpdateDuplicateCheck(parameterMap);
	}	
	
	@Override
	public List selectUploadColumnHDExcel(Map parameterMap) throws Exception {
		return uploadColumnManagementDAO.selectUploadColumnHDExcel(parameterMap);
	}
	
	@Override
	public void insertUploadColunmHD(Map parameterMap) throws Exception {
		int cnt = uploadColumnManagementDAO.selectUploadColumnHDForInsertDuplicateCheck(parameterMap);
		if(cnt < 1){
			this.uploadColumnManagementDAO.insertUploadColunmHD(parameterMap);
		}else {
			throw new MsgException(10002);
		}
	}
	
	@Override
	public void updateUploadColunmHD(Map parameterMap) throws Exception {
		this.uploadColumnManagementDAO.updateUploadColunmHD(parameterMap);
	}
	
	@Override
	public void deleteUploadColunmHD(Map parameterMap) throws Exception {
		//DETAIL DELETE  
		uploadColumnManagementDAO.deleteUploadColunmDT(parameterMap);
		// HEADER DELETE 
		uploadColumnManagementDAO.deleteUploadColunmHD(parameterMap);
	}
	
	@Override
	public void insertUploadColunmDT(Map parameterMap) throws Exception {
		this.uploadColumnManagementDAO.insertUploadColunmDT(parameterMap);
	}
	
	@Override
	public void updateUploadColunmDT(Map parameterMap) throws Exception {
		this.uploadColumnManagementDAO.updateUploadColunmDT(parameterMap);
	}
	
	@Override
	public void deleteUploadColunmDT(Map parameterMap) throws Exception {
		uploadColumnManagementDAO.deleteUploadColunmDT(parameterMap);
	}	
	
	@Override
	public Workbook selectUploadColumnHDExcel(Map parameterMap, List<Map> excelFormat) throws Exception {
		return uploadColumnManagementDAO.selectUploadColumnHDExcel(parameterMap, excelFormat);
	}
	
	@Override
	public Workbook excelDownSearchConditionDetail(Map parameterMap, List<Map> excelFormat) throws Exception {
		return uploadColumnManagementDAO.excelDownSearchConditionDetail(parameterMap, excelFormat);
	}	
	
}