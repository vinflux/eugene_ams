package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface MessageService {
	

	/** **/
	public Map selectMessageInfo (Map parameterMap) throws Exception;

	/** **/
	public Map selectMessage (Map parameterMap) throws Exception;
	
	/** **/
	public List selectMessageByApKeyList(Map parameterMap) throws Exception;
	
	/** 메세지 업로드 **/
	public void saveUploadMessage(List parameterMap) throws Exception;
	
	/** **/
	public void insertMessage (Map parameterMap) throws Exception;
	
	/** **/
	public void updateMessage (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteMessage (Map parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMstCt(Map parameterMap) throws Exception;
	
	/** **/
	public List selectTAdminMsgMulappmsgxct (Map parameterMap) throws Exception;
	
	/** **/
	public void saveMessageCenter (Map parameterMap,List parameterList) throws Exception;
	
	/** **/
	public List selectExcelDownMessage (Map parameterMap) throws Exception;

	public Workbook selectExcelDownMessage(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	
}
