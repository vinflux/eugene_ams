package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.master.service.PushMobService;
import com.vinflux.adm.persistence.PushMobDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;

@Service("pushMobService")
public class PushMobServiceImpl implements PushMobService {
	
	@Resource(name="pushMobDAO")
	private PushMobDAO pushMobDAO;

	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccountInfo(java.util.Map)
	 */
	@Override
	public Map selectPushInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = pushMobDAO.selectPushInfoCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = pushMobDAO.selectPushInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}

	@Override
	public Map selectDetailPushInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deletePush(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deletePushDtl(Map parameterMap, boolean isRootDelete)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void insertPushInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePushInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List selectUserListByUserGroup(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insertPush(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updatePushHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updatePush(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}







