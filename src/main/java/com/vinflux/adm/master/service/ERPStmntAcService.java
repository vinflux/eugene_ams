package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ERPStmntAcService {
	
	public List selectExcelERPStmntAc (Map parameterMap) throws Exception;
	public Workbook selectExcelERPStmntAc(Map paramMap, List<Map> excelFormat);
	
	public Map selectERPStmntAc (Map paramMap) throws Exception;
	
	public List selectERPStmntAcDt (Map parameterMap) throws Exception;
	
		
}
