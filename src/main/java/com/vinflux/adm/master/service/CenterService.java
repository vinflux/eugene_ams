package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface CenterService {
	
	/** **/
	public Map selectCenterInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectCenter (Map parameterMap) throws Exception;
	
	/** **/
	public void insertCenter (Map parameterMap) throws Exception;
	
	/** **/
	public void updateCenter (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteCenter (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownCenter (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownCenter(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
	 * 마스터 화면에서 사용하는 센터 리스트
	 * @param cskey
	 * @return String
	 * @throws Exception
	 */	
	public List getTAdminMstCtList(Map parameterMap) throws Exception;

	
}
