package com.vinflux.adm.master.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.MessageUploadService;
import com.vinflux.adm.persistence.ItemCodeUploadDAO;
import com.vinflux.adm.persistence.MessageUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("messageUploadService")
public class MessageUploadServiceImpl implements MessageUploadService {
	
	@Resource(name="messageUploadDAO")
	private MessageUploadDAO messageUploadDAO;
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	
	protected static final Log LOG = LogFactory.getLog(MessageUploadServiceImpl.class);
	
	private static final String EXCEL_FORMAT_UPLOAD [][] =
		{
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" ,"mulaapmsg_hdkey" 	,"" },
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"lakey" 		    	,"" },	
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"apkey" 				,"" },
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"msgtype" 			,"" },	
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"1000"		,"Y" ,"displaymessage" 		,"" },	
			{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"ctkey" 				,"" }
							
		};

	@Override
	public Map selectMessageUploadInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = messageUploadDAO.selectMessageUploadCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = messageUploadDAO.selectMessageUpload(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public Map selectMessageTemp(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = messageUploadDAO.selectMessageUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = messageUploadDAO.selectMessageTemp(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	public Map selectMessageTempAll(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = messageUploadDAO.selectMessageUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = messageUploadDAO.selectMessageTempAll(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	public String uploadMessageExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
			
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
			
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
			
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
			
	    SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
	    String id = sessionVo.getUrKey();
	    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
		    row.put("sessionid", id);
		    row.put("seq", i+1);

		    //ULHSKEY
		    messageUploadDAO.insertTadminTempMsgup(row);
			}
			
			Map insertTadminInfoUlhsMap = new HashMap();
			insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
			insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
			insertTadminInfoUlhsMap.put("uploadfilename", fileName);
			
			// insert TORDER_INFO_ULHS
			messageUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
			
			return ulhsKey;
	 }

	 
	@Override
	public List selectExcelDownMessageUpload(Map parameterMap) throws Exception {
		return messageUploadDAO.selectExcelDownMessageUpload(parameterMap);
	}
		
	@Override
	public Workbook selectExcelDownMessageUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return messageUploadDAO.selectExcelDownMessageUpload(parameterMap, excelFormat);
	}
		
}