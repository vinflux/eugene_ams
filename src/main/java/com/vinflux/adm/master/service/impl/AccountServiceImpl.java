package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.persistence.AccountDAO;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.tms.persistence.AccountTmsDAO;
import com.vinflux.wms.persistence.AccountWmsDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;



@Service("accountService")
public class AccountServiceImpl implements AccountService {
	
	@Resource(name="accountDAO")
	private AccountDAO accountDAO;

//	@Resource(name="accountIcomDAO")
//	private AccountIcomDAO accountIcomDAO;
	
	@Resource(name="accountWmsDAO")
	private AccountWmsDAO accountWmsDAO;
	
	@Resource(name="accountTmsDAO")
	private AccountTmsDAO accountTmsDAO;
	
//	@Resource(name="accountVimsDAO")
//	private AccountVimsDAO accountVimsDAO;
	
	@Resource(name = "entityAddrService")
	private EntityAddrService entityAddrService;	
	
	@Resource(name="mstAcGenService")
	private IdGenService mstAcGenService;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
/*	@Resource(name="txAdmManager")
	private DataSourceTransactionManager txAdmManager;*/
	
//	@Resource(name = "entityAddrService")
//	private EntityAddrService entityAddrService;	
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccountInfo(java.util.Map)
	 */
	@Override
	public Map selectAccountInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = accountDAO.selectAccountCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = accountDAO.selectAccount(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map selectAccount(Map parameterMap) throws Exception {
    	return accountDAO.selectPkAccount(parameterMap);
	}	

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#insertOwAccount(java.util.Map)
	 */
	@Override
	public void insertOwAccount (CommandMap requestMap) throws Exception {
		// 화면에서 넘견 화주변경 대상 데이터
    	List list = requestMap.getList("list");
    	// 로긴한 사용자의 센터 정보
    	Map userInfo = requestMap.getCommonUserInfoMap();
    	// 화면에서 넘겨준 변경할 화주 정보
    	Map paramMap = requestMap.getParamMap();
    	String owkey = (String) paramMap.get("owkey");
    	// 변경 대상 화주 리스트 
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			// 기존 화주 정보 조회
    			Map resultMap = selectAccount(inputMap);
    			// 신규 화주 코드 셋팅
    			resultMap.put("owkey", owkey);
    			resultMap.put("inserturkey", "");
    			resultMap.put("updateurkey", "");
    			resultMap.put("ctkey", userInfo.get("ctKey"));
    			resultMap.put("ackey", "");
    			// 변경된 화주로 저장
    			insertAccount (resultMap);
    		}
    	}
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#insertAccount(java.util.Map)
	 */
	@Override
	public void insertAccount (Map parameterMap) throws Exception {
		String zipNo = "";
		String lnmAdres = "";
		String address2 = "";
		String rep_actype = "";
		String ackey = "";
		if(parameterMap.get("zipNo") != null) {
			zipNo = (String)parameterMap.get("zipNo");
		}
		if(parameterMap.get("lnmAdres") != null) {
			lnmAdres = (String)parameterMap.get("lnmAdres");
		}
		if(parameterMap.get("address2") != null) {
			address2 = (String)parameterMap.get("address2");
		}
		if(parameterMap.get("rep_actype") != null) {
			rep_actype = (String)parameterMap.get("rep_actype");
		}
		if(parameterMap.get("ackey") != null) {
			ackey = (String)parameterMap.get("ackey");
		}
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		parameterMap.put("countrycode","KOR");
		
		
		String owkey = (String) parameterMap.get("owkey");
		int cnt = accountDAO.selectPkCntMstAcxct(parameterMap);
		if(cnt > 0) {
			throw new MsgException(20006);
		}
		if("".equals(ackey) && ackey.isEmpty()){
			ackey = mstAcGenService.getNextStringId();	
			 parameterMap.put("ackey", owkey+ackey);
		}
		
		if("1".equals(rep_actype)){
			parameterMap.put("rep_ackey",ackey);
		}
		
		/*	String ctynm2 = (String)parameterMap.get("address1");
		String zipcd2 = (String)parameterMap.get("zipcode");
		
		if(!"".equals(ctynm2) || !ctynm2.isEmpty()){
			ctynm = ctynm2;
			zipcd = zipcd2;
		}*/
		
		
		/*DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);
		
		
		try { */
			
			/* 위경도 정보(TADMIN_MST_ADDR_T) 추후 의사 결정 필요 - 2017.11.08-ksh*/
	 		if(!"".equals(parameterMap.get("zipNo"))){
				int intAddrId = entityAddrService.getAddrId();
				parameterMap.put("addrid", intAddrId);
				LOG.debug("위경도 정보(TADMIN_MST_ADDR_T) : " + parameterMap);
				entityAddrService.addAddressEntitymaster(parameterMap);
			}
			
	 		
			// INSERT ADMIN 
			LOG.debug("INSERT ADMINPARAM : " + parameterMap);
			accountDAO.insertTAdminMstAc(parameterMap);
			accountDAO.mergeTtShpgLocT(parameterMap);
			//entityAddrService.addAddressEntitymaster(parameterMap);
			
			/* 10.21 OMS 삭제작업
			// ICOM
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				accountIcomDAO.insertTOrderMstAc(parameterMap);
				entityAddrService.addAddressEntitymasterIcom(parameterMap);
			}*/
			
			// TMS
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				accountTmsDAO.insertTTMstAc(parameterMap);
				LOG.debug("INSERT TMSPARAM : " + parameterMap);
				entityAddrService.addAddressEntitymasterTms(parameterMap);
			}
		
			// WMS
			LOG.debug("INSERT WMSPARAM : " + parameterMap);
			entityAddrService.addAddressEntitymasterWms(parameterMap);
			
			// AMS, TMS, WMS --센터 맵핑
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
				//List chkList  = storeDAO.selectTAdminMstStorexct(parameterMap);//detail mapping data
				List chkList = accountDAO.selectTAdminMstCt(parameterMap);
				//Map  storeMap = storeDAO.selectPkStore(parameterMap);//selected header data
			
				if (chkList != null && !chkList.isEmpty() ) {
					for (int i=0 ;chkList.size() > i ; i++ ) { 
						Map chkRow = (Map)chkList.get(i);
						String ctkey = (String)chkRow.get("ctkey");
						parameterMap.put("ctkey", ctkey);
						
						//parameterMap.put("loggrpcd", "1");
						parameterMap.put("dckey", "1");
						//parameterMap.put("icgrkey", "02"); 
						parameterMap.put("acicgrtype", "0");
						parameterMap.put("countrycode","KOR");
						
						accountDAO.insertTAdminMstAcxct(parameterMap);
						accountTmsDAO.insertTTMstAcxct(parameterMap);	
						accountWmsDAO.insertTWorkMstAc(parameterMap);
						LOG.debug("INSERT CENT PARAM : " + parameterMap);
					}
				}		
			}
			/*
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
				List chkList  = accountDAO.selectTAdminMstAcxct(parameterMap);//detail mapping data
				Map  accountMap = accountDAO.selectPkAccount(parameterMap);//selected header data
				entityAddrService.addAddressEntitymasterWms(parameterMap);
				if (chkList != null && !chkList.isEmpty() ) {
					
					for (int i=0 ;chkList.size() > i ; i++ ) { 
						Map chkRow = (Map)chkList.get(i);
						String ctkey = (String)chkRow.get("ctkey");
						accountMap.put("ctkey", ctkey);
						
						accountWmsDAO.insertTWorkMstAc(accountMap);
						
					}
				}
				
			}
			*/
			/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			accountVimsDAO.insertTViewMstAc(parameterMap);
		}*/

		/*}catch( MsgException me ){
			LOG.error("### ERROR=>"+me.getLog());
			txAdmManager.rollback(tmsSts);
			throw me;
		}catch (Exception e) {
			LOG.error("", e);
			txAdmManager.rollback(tmsSts);
			throw e;
			
		} finally {
			if(!tmsSts.isCompleted()){
				txAdmManager.commit(tmsSts);
			}
		}*/
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#updateAccount(java.util.Map)
	 */
	@Override
	public void updateAccount (Map parameterMap) throws Exception {
		
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
		String rep_actype = (String)parameterMap.get("rep_actype");
		String rep_ackey = (String)parameterMap.get("rep_ackey");
		String ackey = (String)parameterMap.get("ackey");
		
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		parameterMap.put("countrycode","KOR");

		if("1".equals(rep_actype)){
			parameterMap.put("rep_ackey",ackey);
		}else{
			parameterMap.put("rep_ackey",rep_ackey);
		}
	/*	if(ctynm2 != null){
			ctynm = ctynm2;
			zipcd = zipcd2;
		}*/
		
		if("".equals(parameterMap.get("addrid")) &&!"".equals(parameterMap.get("zipNo"))&&!"".equals(parameterMap.get("address2"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
		
		
		
		/*DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);

		try { */
			int resultint = 0;
			/* 10.21 OMS 삭제작업
			// ICOM
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {			
				resultint = accountIcomDAO.updateTOrderMstAc(parameterMap);
				//데이타가 없으면 insert
				if(resultint==0){
					accountIcomDAO.insertTOrderMstAc(parameterMap);
					entityAddrService.changeAddressEntitymasterIcom(parameterMap);
					
				}
			}*/
			
		/*	 위경도 정보(TADMIN_MST_ADDR_T) 
			if("".equals(parameterMap.get("zipNo"))){
				int intAddrId = entityAddrService.getAddrId();
				parameterMap.put("addrid", intAddrId);
				//entityAddrService.addAddressEntitymaster(parameterMap);
			}*/

			// TMS
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {			
				resultint = accountTmsDAO.updateTTMstAc(parameterMap);			
				entityAddrService.changeAddressEntitymasterTms(parameterMap);
				if(resultint==0){
					accountTmsDAO.insertTTMstAc(parameterMap);
				}
			}
			
			// WMS
			entityAddrService.changeAddressEntitymasterWms(parameterMap);
			/*if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				resultint = accountWmsDAO.updateTWorkMstAc(parameterMap);
			}*/
			
			// WMS
			
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
				List chkList  = accountDAO.selectTAdminMstAcxct(parameterMap);//detail mapping data
				Map  accountMap = accountDAO.selectPkAccount(parameterMap);//selected header data
				
				if (chkList != null && !chkList.isEmpty() ) {
					
					for (int i=0 ;chkList.size() > i ; i++ ) { 
						Map chkRow = (Map)chkList.get(i);
						String ctkey = (String)chkRow.get("ctkey");
						accountMap.put("ctkey", ctkey);
						
						resultint = accountWmsDAO.updateTWorkMstAc(parameterMap);			
						if(resultint==0){
							accountWmsDAO.insertTWorkMstAc(parameterMap);
							entityAddrService.addAddressEntitymasterWms(parameterMap);
						}
						
					}
				}
				
			}
			
			// VIMS
			/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {			
				resultint = accountVimsDAO.updateTViewMstAc(parameterMap);			
				if(resultint==0){
					accountVimsDAO.insertTViewMstAc(parameterMap);
				}
			}*/
			
			// UPDATE ADMIN
			accountDAO.updateTAdminMstAc(parameterMap);
			entityAddrService.changeAddressEntitymaster(parameterMap);
			accountDAO.mergeTtShpgLocT(parameterMap);
		/*}catch( MsgException me ){
			LOG.error("### ERROR=>"+me.getLog());
			txAdmManager.rollback(tmsSts);
			throw me;

		}
		catch (Exception e) {
			LOG.error("", e);
							
			txAdmManager.rollback(tmsSts);

			throw e;

		} finally {
			if(!tmsSts.isCompleted()){
				txAdmManager.commit(tmsSts);
			}
		}			*/	
	}

	@Override
	public void deleteAccount (Map parameterMap) throws Exception {
		/*DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);		
		try{*/
	
		// DELETE WMS 
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				
				accountWmsDAO.deleteTWorkMstAcAll(parameterMap);
				//entityAddrService.deleteAddressEntitymasterWms(parameterMap);
			}
	
			/* 10.21 OMS 삭제작업
			// ICOM
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				accountIcomDAO.deleteTOrderMstAc(parameterMap);
				entityAddrService.deleteAddressEntitymasterIcom(parameterMap);
			}*/
		
			// TMS
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				parameterMap.put("delyn", "Y");
				accountTmsDAO.deleteTTMstAcxct(parameterMap);
				accountTmsDAO.deleteTTMstAc(parameterMap);
				//entityAddrService.deleteAddressEntitymasterTms(parameterMap);
			}
			
			// VIMS
			/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
				accountVimsDAO.deleteTViewMstAc(parameterMap);
			}*/
			
			
			// DELETE ADMIN
			parameterMap.put("delyn", "Y");
			accountDAO.deleteTAdminMstAcxct(parameterMap);
			accountDAO.deleteTAdminMstAc(parameterMap);
			accountDAO.mergeTtShpgLocT(parameterMap);
			//entityAddrService.deleteAddressEntitymaster(parameterMap);
		/*}catch( MsgException me ){
			LOG.error("### ERROR=>"+me.getLog());
			txAdmManager.rollback(tmsSts);
			throw me;
		}catch (Exception e) {
			LOG.error("", e);
			txAdmManager.rollback(tmsSts);
			throw e;
	
		} finally {
			if(!tmsSts.isCompleted()){
				txAdmManager.commit(tmsSts);
			}
		}		*/				
	}
	
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return accountDAO.selectTAdminMstCt(parameterMap);
	}
	
	@Override
	public List selectTAdminMstAcxct(Map parameterMap) throws Exception {
		return accountDAO.selectTAdminMstAcxct(parameterMap);
	}

	@Override
	public void saveAccountCenter(Map parameterMap ,  List parameterList) throws Exception {
		List chkList = accountDAO.selectTAdminMstAcxct(parameterMap);
		for (int j=0, size = chkList.size(); j < size; j++ ) {
			Map row = (Map)chkList.get(j);
			row.put("dckey", "1");
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				row.put("delyn", "Y");
				accountWmsDAO.deleteTWorkMstAc(row);
			}
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				row.put("delyn", "Y");
				accountTmsDAO.deleteTTMstAcxct(row);
			}
			accountDAO.deleteTAdminMstAcxct(row);
		}
		
		for(Object rowObj: parameterList) {
			Map row = (Map)rowObj;
			row.put("dckey", "1");
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				int wmCnt = accountWmsDAO.selectPkCntTWorkMstAc(row);
				if (wmCnt > 0) {
					row.put("delyn", "N");
					accountWmsDAO.deleteTWorkMstAc(row);
				} else {
					accountWmsDAO.insertTWorkMstAc(row);
				}
			}
			
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {	
				int tmCnt = accountTmsDAO.selectPkCntTTMstAcxct(row);
				if (tmCnt > 0) {
					row.put("delyn", "N");
					accountTmsDAO.deleteTTMstAcxct(row);
				} else {
					accountTmsDAO.insertTTMstAcxct(row);
				}
			}
			
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {	
				int amCnt = accountDAO.selectPkCntMstAcxct(row);
				if (amCnt > 0) {
					row.put("delyn", "N");
					row.put("delyn", "N");
					accountDAO.deleteTAdminMstAcxct(row);
				} else {
					accountDAO.insertTAdminMstAcxct(row);
				}
			}
		}
	}
	
	@Override
	public void saveAccountCenter(Map parameterMap) throws Exception {
		/*String zipcode = (String)parameterMap.get("zipNo");
		String address1 = (String)parameterMap.get("lnmAdres");
		parameterMap.put("zipcode", zipcode);
		parameterMap.put("address1",address1);*/
		
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			int cntSel = accountWmsDAO.selectPkCntTWorkMstAc(parameterMap);
			if ( cntSel == 0) {
				accountWmsDAO.insertTWorkMstAc(parameterMap);
			}
		}
		
		//추후 결정 - 2017.11.07 : ksh
		if (accountDAO.selectPkCntTadminMstAcxct(parameterMap) == 0) {
			accountDAO.insertTAdminMstAcxct(parameterMap);
		}
	}
	
	@Override
	public List selectExcelDownAccount(Map parameterMap) throws Exception {
		return accountDAO.selectExcelDownAccount(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownAccount(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return accountDAO.selectExcelDownAccount(parameterMap, excelFormat);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map selectRepAccountInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = accountDAO.selectRepAccountCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = accountDAO.selectRepAccount(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}

}