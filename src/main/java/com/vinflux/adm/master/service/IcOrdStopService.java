package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface IcOrdStopService {

	public Map selectIcOrdStop(Map parameterMap) throws Exception;
	public List selectIcOrdStopXct(Map parameterMap) throws Exception;
	public Workbook selectIcOrdStopXct(Map parameterMap, List<Map> excelFormat)throws Exception;
	public List selectIcOrdStopExcelDown(Map parameterMap) throws Exception;
	public Workbook selectIcOrdStopExcelDown(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	public void saveIcOrdStop(Map parameterMap) throws Exception;
	public void updateIcOrdStop(List paramList) throws Exception;
	public Map selectPoStopConfirm(Map parameterMap) throws Exception;
	public List selectPoStopConfirmExcelDown(Map parameterMap) throws Exception;
	public Workbook selectPoStopConfirmExcelDown(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	public void updatePoStopConfirm(List paramList) throws Exception;
	
	
	
	public void updateIcOrdStopXct(List paramList) throws Exception;
	public void deleteIcOrdStop(List paramList) throws Exception;
	
	public Map selectIcOrdStopRemove(Map parameterMap) throws Exception;
	public List selectIcOrdStopXctForRemove(Map parameterMap) throws Exception;
	public Workbook selectIcOrdStopXctForRemove(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	public List selectIcOrdStopRemoveExcelDown(Map parameterMap) throws Exception;
	public Workbook selectIcOrdStopRemoveExcelDown(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	public void saveIcOrdStopRemove(Map parameterMap) throws Exception;
	
	public Map selectIcOrdStopStatus(Map parameterMap) throws Exception;
	public List selectIcOrdStopStatusExcelDown(Map parameterMap) throws Exception;
	public Workbook selectIcOrdStopStatusExcelDown(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	
}
