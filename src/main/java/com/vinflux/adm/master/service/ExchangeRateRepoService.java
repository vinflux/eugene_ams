package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ExchangeRateRepoService {
	
	public Map selectExchangeRateRepoList (Map paramMap) throws Exception;

	public List selectExcelDownUser(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)throws Exception;

}
