package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.CntrService;
import com.vinflux.adm.persistence.CntrDAO;
import com.vinflux.adm.role.service.impl.OwnerByRoleServiceImpl;
import com.vinflux.tms.persistence.CntrTmsDAO;
import com.vinflux.wms.persistence.CntrWmsDAO;

@Service("cntrService")
public class CntrServiceImpl implements CntrService {

	protected static final Log LOG = LogFactory.getLog(OwnerByRoleServiceImpl.class);
	
	@Resource(name="cntrDAO")
	private CntrDAO cntrDAO;
	
//	@Resource(name="cntrIcomDAO")
//	private CntrIcomDAO cntrIcomDAO;
	
	@Resource(name="cntrWmsDAO")
	private CntrWmsDAO cntrWmsDAO;
	
	@Resource(name="cntrTmsDAO")
	private CntrTmsDAO cntrTmsDAO;

	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectCntrInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = cntrDAO.selectCntrCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = cntrDAO.selectCntr(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectCntr (Map parameterMap) throws Exception {
		return cntrDAO.selectPkCntr(parameterMap);
	}
	
	@Override
	public int checkCntrCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return cntrDAO.checkCntrCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.CntrService#insertCntr(java.util.Map)
	 */
	@Override
	public void insertCntr (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.cntrDAO.insertTAdminMstCntr(parameterMap);
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.cntrIcomDAO.insertTOrderMstCntr(parameterMap);	
		}*/
		
		// TMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.cntrTmsDAO.insertTTMstCntr(parameterMap);	
		}
		
		// WMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = cntrDAO.selectCtkey(parameterMap);
			    
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			this.cntrWmsDAO.insertTWorkMstCntr(parameterMap);
        		}
        	}
        	
			
			
		}
	}
	
	/** **/
	@Override
	public void updateCntr (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.cntrDAO.updateTAdminMstCntr(parameterMap);
		
		int count = 0;
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = this.cntrIcomDAO.updateTOrderMstCntr(parameterMap);
			if(count==0){
				this.cntrIcomDAO.insertTOrderMstCntr(parameterMap);
			}
		}*/
		
		// TMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			count = this.cntrTmsDAO.updateTTMstCntr(parameterMap);
			//if(count==0){
				//this.cntrTmsDAO.insertTTMstCntr(parameterMap);
			//}
		}
		
		// WMS UPDATE 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			count = this.cntrWmsDAO.updateTWorkMstCntr(parameterMap);
		}
		
		LOG.debug( "## Update Count : " + count);
	}

	/** **/
	@Override
	public void deleteCntr (Map parameterMap) throws Exception {
		// ICOM DELETE
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.cntrIcomDAO.deleteTOrderMstCntr(parameterMap);
		}*/

		// WMS DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.cntrWmsDAO.deleteTWorkMstCntr(parameterMap);
		}
		
		// TMS DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.cntrTmsDAO.deleteTTMstCntr(parameterMap);
		}

		// ADMIN DELETE
		this.cntrDAO.deleteTAdminMstCntr(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownCntr (Map parameterMap) throws Exception {
    	return cntrDAO.selectExcelDownCntr(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownCntr(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return cntrDAO.selectExcelDownCntr(parameterMap, excelFormat);
	}
	
	@Override
	public Map searchCntrKeySearchInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		int count = cntrDAO.searchCntrKeySearchInfoCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = cntrDAO.searchCntrKeySearchInfo(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}
	
}