package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vinflux.adm.master.service.AlertSettingService;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.vims.persistence.AlertSettingDAO;

@Service("alertSettingService")
public class AlertSettingServiceImpl implements AlertSettingService {

	@Resource(name="alertSettingDAO")
	private AlertSettingDAO alertSettingDAO;
	
	@Resource(name="txVimsManager")
	private DataSourceTransactionManager txVimsManager;

	protected static final Log LOG = LogFactory.getLog(AlertSettingServiceImpl.class);

	
	@Override
	public int checkAlertSetting(Map paramMap) throws Exception {
        return alertSettingDAO.checkAlertSetting(paramMap);
    }

	@Override
	public Map selectAlertSettingHdList(Map parameterMap) throws Exception {
		
		Map resultMap = new HashMap();
		
		Integer count = alertSettingDAO.selectAlertSettingHdCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		
		if (count > 0 ) {     		
	    	List list = alertSettingDAO.selectAlertSettingHdList(parameterMap);
	    	
	    	if (list != null && !list.isEmpty()) {
	    		resultMap.put("list", list);
	    	}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}

	@Override
	public void deleteAlertSettingHd(Map row) throws Exception {
		
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		TransactionStatus vimsSts = txVimsManager.getTransaction(def);

		int cnt = alertSettingDAO.selectAlertSettingHdCount(row);

		LOG.debug("======================================");
		LOG.debug("cnt => " + cnt);
		LOG.debug("======================================");

		if (cnt <= 0) {
			throw new MsgException(10003);
		} else {
			try{
				alertSettingDAO.deleteAlertSettingHd(row);
			} catch (Exception e) {
				LOG.error("", e);
				txVimsManager.rollback(vimsSts);
				throw e;

			} finally {
				if(!vimsSts.isCompleted()){
					txVimsManager.commit(vimsSts);
				}
			}
		}
		
	}

	@Override
	public Map selectAlertSettingHd(Map paramMap) throws Exception {
		return alertSettingDAO.selectAlertSettingHd(paramMap);
	}

	@Override
	public void insertAlertSettingHd(Map map) throws Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus vimsSts = txVimsManager.getTransaction(def);

		try {

			alertSettingDAO.insertAlertSettingHd(map);

		} catch (Exception e) {
			LOG.error("", e);
			txVimsManager.rollback(vimsSts);
			throw e;

		} finally {
			if(!vimsSts.isCompleted()){
				txVimsManager.commit(vimsSts);
			}
		}
	}

	@Override
	public void updateAlertSettingHd(Map map) throws Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		
		TransactionStatus vimsSts = txVimsManager.getTransaction(def);

		int cnt = alertSettingDAO.selectAlertSettingHdCount(map);

		LOG.debug("======================================");
		LOG.debug("cnt => " + cnt);
		LOG.debug("======================================");

		if (cnt <= 0) {
			throw new MsgException(10003);
		} else {
			try{
				alertSettingDAO.updateAlertSettingHd(map);
			} catch (Exception e) {
				LOG.error("", e);
				txVimsManager.rollback(vimsSts);
				throw e;
			} finally {
				if(!vimsSts.isCompleted()){
					txVimsManager.commit(vimsSts);
				}
			}
		}
		
	}
	
	@Override
	public Map selectEventid(Map map) throws Exception {
		Map resultMap = new HashMap();
		List list = alertSettingDAO.selectEventid(map);
    	
    	if (list != null && !list.isEmpty()) {
    		resultMap.put("list", list);
    	}
		
		return resultMap;
	}
	
	@Override
	public Map selectCenterList(Map map) throws Exception {
		Map resultMap = new HashMap();
		List list = alertSettingDAO.selectCenterList(map);
    	
    	if (list != null && !list.isEmpty()) {
    		resultMap.put("list", list);
    	}
		
		return resultMap;
	}
	
}