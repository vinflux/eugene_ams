package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface EquipmentService {
	
	//헤더 그리드 카운트
	public int selectTAdminMstEqCnt(Map parameterMap) throws Exception;
	
	//헤더 그리드 조회
	public Map selectEquipment (Map parameterMap) throws Exception;
	
	//PK 조회
	public Map selectPkTAdminMstEq(Map parameterMap) throws Exception;
	
	//헤더 그리드 저장
	public void saveEquipment (Map parameterMap) throws Exception;	
	
	//헤더 그리드 삭제
	public void deleteEquipment (Map parameterMap) throws Exception;
	
	//헤더 그리드 엑셀
	public List selectEquipmentExcel (Map parameterMap) throws Exception;
	
	public Workbook selectEquipmentExcel(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	//센터 조회
	public List selectTAdminMstCt(Map parameterMap) throws Exception;
	
	//물품별 센터 조회
	public List selectTAdminMstEqxct (Map parameterMap) throws Exception;
	
	//하단 저장
	public void saveEquipmentCenter (Map parameterMap,List parameterList) throws Exception;
	
	public void saveEquipmentCenter (Map parameterMap) throws Exception;

}
