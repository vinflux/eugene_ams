package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface UploadColumnManagementService {


	public Map selectUploadColumnHeaderInfo (Map parameterMap) throws Exception;

	public Map selectUploadColumnDetailInfo (Map parameterMap) throws Exception;
	
	public int checkSave (Map parameterMap) throws Exception;	

	public List selectUploadColumnHDExcel(Map parameterMap) throws Exception;
	
	public void insertUploadColunmHD (Map parameterMap) throws Exception;
	
	public void updateUploadColunmHD (Map parameterMap) throws Exception;
	
	public void deleteUploadColunmHD (Map parameterMap) throws Exception;
	
	public void insertUploadColunmDT (Map parameterMap) throws Exception;
	
	public void updateUploadColunmDT (Map parameterMap) throws Exception;
	
	public void deleteUploadColunmDT (Map parameterMap) throws Exception;	
	
	public Workbook selectUploadColumnHDExcel(Map parameterMap,List<Map> excelFormat)throws Exception;	
	
	public Workbook excelDownSearchConditionDetail(Map parameterMap,List<Map> excelFormat)throws Exception;

}
