package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.master.service.IcOrdStopService;
import com.vinflux.adm.persistence.IcOrdStopDAO;
import com.vinflux.framework.exception.MsgException;

@Service("icOrdStopService")
public class IcOrdStopServiceImpl implements IcOrdStopService {
	
	@Resource(name="icOrdStopDAO")
	private IcOrdStopDAO icOrdStopDAO;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccountInfo(java.util.Map)
	 */
	@Override
	public Map selectIcOrdStop(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icOrdStopDAO.selectIcOrdStopCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icOrdStopDAO.selectIcOrdStop(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectIcOrdStopXct(Map parameterMap) throws Exception {
		List list = icOrdStopDAO.selectIcOrdStopXct(parameterMap);
		if(list == null) {
			list = new ArrayList();
    	}
		return list;
	}
	
	@Override
	public Workbook selectIcOrdStopXct(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return icOrdStopDAO.selectIcOrdStopXct(parameterMap, excelFormat);
	}

	@Override
	public List selectIcOrdStopExcelDown(Map parameterMap) throws Exception {
		return icOrdStopDAO.selectIcOrdStopExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectIcOrdStopExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icOrdStopDAO.selectIcOrdStopExcelDown(parameterMap, excelFormat);
	}
	
	/**
	 * 센터발주정지
	 * @param paramList
	 * @throws Exception
	 */
	@Override
	public void updateIcOrdStop(List paramList) throws Exception {
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> ctkey :: " + paramMap.get("ctkey"));
			LOG.debug("> loggrpcd :: " + paramMap.get("loggrpcd"));
			LOG.debug("> dckey :: " + paramMap.get("dckey"));
			LOG.debug("> icgrkey :: " + paramMap.get("icgrkey"));
			LOG.debug("> ickey :: " + paramMap.get("ickey"));
			LOG.debug("> chgstartdate :: " + paramMap.get("chgstartdate"));
			LOG.debug("> chgworktype :: " + paramMap.get("chgworktype"));
			LOG.debug(">> ctpostdate ::===> " + paramMap.get("ctpostdate"));
			if(icOrdStopDAO.updateIcOrdStop(paramMap) == 0){
				throw new MsgException("| 데이터가 정확하지 않아 적용되지 않았습니다.");
			}
		}	
	}
	
	@Override
	public Map selectPoStopConfirm(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icOrdStopDAO.selectPoStopConfirmCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icOrdStopDAO.selectPoStopConfirm(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectPoStopConfirmExcelDown(Map parameterMap) throws Exception {
		return icOrdStopDAO.selectPoStopConfirmExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectPoStopConfirmExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icOrdStopDAO.selectPoStopConfirmExcelDown(parameterMap, excelFormat);
	}
	
	@Override
	public void updatePoStopConfirm(List paramList) throws Exception {
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> owkey :: " + paramMap.get("owkey"));
			LOG.debug("> ctkey :: " + paramMap.get("ctkey"));
			LOG.debug("> ackey :: " + paramMap.get("ackey"));
			LOG.debug("> ickey :: " + paramMap.get("ickey"));
			
			if(icOrdStopDAO.updatePoStopConfirm(paramMap) == 0){
				throw new MsgException("| 데이터가 정확하지 않아 적용되지 않았습니다.");
			}
		}	
	}
	
	
	
	
	
	
	
	@Override
	public void updateIcOrdStopXct(List paramList) throws Exception {
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> ctkey :: " + paramMap.get("ctkey"));
			LOG.debug("> loggrpcd :: " + paramMap.get("loggrpcd"));
			LOG.debug("> dckey :: " + paramMap.get("dckey"));
			LOG.debug("> icgrkey :: " + paramMap.get("icgrkey"));
			LOG.debug("> ickey :: " + paramMap.get("ickey"));
			LOG.debug("> chgstartdate :: " + paramMap.get("chgstartdate"));
			LOG.debug("> chgworktype :: " + paramMap.get("chgworktype"));
			LOG.debug(">> ctpostdate ::===> " + paramMap.get("ctpostdate"));
			if(icOrdStopDAO.updateIcOrdStopXct(paramMap) == 0){
				throw new MsgException("| 데이터가 정확하지 않아 적용되지 않았습니다.");
			}
		}	
	}
	
	@Override
	public void deleteIcOrdStop(List paramList) throws Exception {
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> ctkey :: " + paramMap.get("ctkey"));
			LOG.debug("> ickey :: " + paramMap.get("ickey"));
			LOG.debug("> chgstartdate :: " + paramMap.get("chgstartdate"));
			LOG.debug("> chgworktype :: " + paramMap.get("chgworktype"));
			if(icOrdStopDAO.deleteIcOrdStop(paramMap) == 0){
				throw new MsgException("| 데이터가 정확하지 않아 적용되지 않았습니다.");
			}
		}	
	}
	
	@Override
	public Map selectIcOrdStopRemove(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icOrdStopDAO.selectIcOrdStopRemoveCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icOrdStopDAO.selectIcOrdStopRemove(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectIcOrdStopXctForRemove(Map parameterMap) throws Exception {
		List list = icOrdStopDAO.selectIcOrdStopXctForRemove(parameterMap);
		if(list == null) {
			list = new ArrayList();
    	}
		return list;
	}
	
	@Override
	public Workbook selectIcOrdStopXctForRemove(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icOrdStopDAO.selectIcOrdStopXctForRemove(parameterMap, excelFormat);
	}

	@Override
	public List selectIcOrdStopRemoveExcelDown(Map parameterMap) throws Exception {
		return icOrdStopDAO.selectIcOrdStopRemoveExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectIcOrdStopRemoveExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icOrdStopDAO.selectIcOrdStopRemoveExcelDown(parameterMap, excelFormat);
	}
	
	@Override
	public Map selectIcOrdStopStatus(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icOrdStopDAO.selectIcOrdStopStatusCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icOrdStopDAO.selectIcOrdStopStatus(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public List selectIcOrdStopStatusExcelDown(Map parameterMap) throws Exception {
		return icOrdStopDAO.selectIcOrdStopStatusExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectIcOrdStopStatusExcelDown(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icOrdStopDAO.selectIcOrdStopStatusExcelDown(parameterMap, excelFormat);
	}
	
	/**
	 * TADMIN_MST_ICORDSTOP 저장, 수정
	 */
	@Override
	public void saveIcOrdStop(Map parameterMap) throws Exception {
		icOrdStopDAO.mergeIcOrdStop(parameterMap);	
	}
	
	@Override
	public void saveIcOrdStopRemove(Map parameterMap) throws Exception {
		icOrdStopDAO.mergeIcOrdStopRemove(parameterMap);	
	}
}