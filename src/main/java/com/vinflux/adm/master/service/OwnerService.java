package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface OwnerService {
	
	/** **/
	public Map selectOwnerInfo (Map parameterMap) throws Exception;
	
	/** **/
	public Map selectOwner (Map parameterMap) throws Exception;
	
	/** **/
	public int checkOwnerCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertOwner (Map parameterMap) throws Exception;
	
	/** **/
	public void updateOwner (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteOwner (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownOwner (Map parameterMap) throws Exception;

	public Workbook selectExcelDownOwner(Map parameterMap, List<Map> excelFormat)throws Exception;
	
}
