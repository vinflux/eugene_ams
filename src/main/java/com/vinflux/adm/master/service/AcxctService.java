package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface AcxctService {
	
	/** **/
	public Map selectAcxctInfo (Map parameterMap) throws Exception;
	
	/** **/
	public Map selectDetailAcxctInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectAcxct (Map parameterMap) throws Exception;
	
	/** **/
	public int checkAcxctCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertAcxct (Map parameterMap) throws Exception;
	
	/** **/
	public void updateAcxct (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteAcxct (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownHerderAcxct(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	public Workbook selectExcelDownDetailAcxct(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	public Workbook selectExcelDownAllAcxct(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	
}
