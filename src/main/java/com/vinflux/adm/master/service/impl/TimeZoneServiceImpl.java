package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.TimeZoneService;
import com.vinflux.adm.persistence.TimeZoneDAO;
import com.vinflux.framework.exception.MsgException;

@Service("timeZoneService")
public class TimeZoneServiceImpl implements TimeZoneService {

	@Resource(name = "timeZoneDAO")
	private TimeZoneDAO timeZoneDao;

	@Override
	public Map selectTimezoneHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub

		Map resultMap = new HashMap();

		Integer count = timeZoneDao.selectTimezoneCountHd(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = timeZoneDao.selectTimezoneHd(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}
		else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}

	@Override
	public Map selectTimezoneDtl(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
		Integer count = timeZoneDao.selectTimezoneCountDtl(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		List list = timeZoneDao.selectTimezoneDtl(parameterMap);
		if (list != null && !list.isEmpty()) {
			resultMap.put("list", list);
		}
		else {
			resultMap.put("list", new ArrayList());
		}

		return resultMap;
	}

	@Override
	public int checkTimezoneInfoHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return timeZoneDao.checkTimezoneInfoHd(parameterMap);
	}

	@Override
	public int checkTimezoneInfoDtl(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return timeZoneDao.checkTimezoneInfoDtl(parameterMap);
	}

	@Override
	public void insertTimezoneInfoHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = timeZoneDao.checkTimezoneInfoHd(parameterMap);

		if (cnt > 0) {
			throw new MsgException(30018);
		}
		else {
			timeZoneDao.insertTimezoneInfoHd(parameterMap);
		}
	}

	@Override
	public void insertTimezoneInfoDtl(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = timeZoneDao.checkTimezoneInfoDtl(parameterMap);
		if (cnt > 0) {
			throw new MsgException(30018);
		}
		else {
			timeZoneDao.insertTimezoneInfoDtl(parameterMap);
		}
	}

	@Override
	public void updateTimezoneInfoHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = timeZoneDao.checkTimezoneInfoHd(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(30002);
		}
		else {
			this.timeZoneDao.updateTimezoneInfoHd(parameterMap);
		}
	}

	@Override
	public void updateTimezoneInfoDtl(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub		
		int cnt = timeZoneDao.checkTimezoneInfoDtl(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(30002);
		}
		else {
			timeZoneDao.updateTimezoneInfoDtl(parameterMap);
		}
	}

	@Override
	public int deleteTimezoneInfoHd(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = timeZoneDao.checkTimezoneInfoHd(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(30002);
		}
		else {
			this.timeZoneDao.deleteTimezoneInfoHd(parameterMap);
		}
		return cnt;
	}

	@Override
	public int deleteTimezoneInfoDtl(Map parameterMap, boolean isRootDelete) throws Exception {
		// TODO Auto-generated method stub
		int cnt = timeZoneDao.checkTimezoneInfoDtl(parameterMap);
		if ( !isRootDelete ) {
			if (cnt <= 0) {
				throw new MsgException(30002);
			}
			else {
				this.timeZoneDao.deleteTimezoneInfoDtl(parameterMap);
			}
		}
		else {
			if (cnt >= 0) {
				this.timeZoneDao.deleteTimezoneInfoDtl(parameterMap);
			}
		}
		return cnt;
	}

	/**
	 * 표준시간대 상단 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectTimzoneHdExcel(Map parameterMap) throws Exception {
		return timeZoneDao.selectTimzoneHdExcel(parameterMap);
	}
	
	@Override
	public Workbook selectTimzoneHdExcel(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return timeZoneDao.selectTimzoneHdExcel(parameterMap, excelFormat);
	}

	/**
	 * 표준시간대 하단 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectTimzoneDtlExcel(Map parameterMap) throws Exception {
		return timeZoneDao.selectTimzoneDtlExcel(parameterMap);
	}
	
	@Override
	public Workbook selectTimzoneDtlExcel(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return timeZoneDao.selectTimzoneDtlExcel(parameterMap, excelFormat);
	}
}
