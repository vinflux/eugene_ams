package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;

public interface CtgService {
	
	/** **/
	public Map selectCtgInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectCtg (Map parameterMap) throws Exception;
	
	/** **/
	public int checkCtgCount (Map parameterMap) throws Exception;

	/** 화주변경 저장 **/
	public void insertOwCtg (CommandMap requestMap) throws Exception;
	
	/** **/
	public void insertCtg (Map parameterMap) throws Exception;
	
	/** **/
	public void updateCtg (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteCtg (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownCtg (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownCtg(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map searchCtgInfo (Map parameterMap) throws Exception;

	
}
