package com.vinflux.adm.master.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.framework.client.amf.AmfClient;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.adm.master.service.BoardService;
import com.vinflux.adm.master.web.BoardController;
import com.vinflux.adm.persistence.BoardDAO;

@Service("boardService")
public class BoardServiceImpl implements BoardService{

	@Resource(name="boardDAO")
	private BoardDAO boardDAO;
	
    @Resource(name="amfClient")
    protected AmfClient amfClient;	
	
    /** LOG */
    protected static final Log LOG = LogFactory.getLog(BoardController.class);
    
    public Map selectNoticeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = boardDAO.selectNoticeCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = boardDAO.selectNoticeInfoList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
   	
		return resultMap;	
	}
    
    public Map selectFileList(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
        HttpServletRequest req = HttpUtil.getCurrentRequest();
        SessionVO sessionVo = SessionManager.getSessionVO(req);
        String lakey = sessionVo.getLaKey();
        parameterMap.put("lakey", lakey);
    		
    	List list = boardDAO.selectFileList(parameterMap);
    	if (list != null && !list.isEmpty()) { 
    		resultMap.put("list", list);	
    	}

		return resultMap;	
	}

    public Map selectNoticeGrInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
        HttpServletRequest req = HttpUtil.getCurrentRequest();
        SessionVO sessionVo = SessionManager.getSessionVO(req);
        String lakey = sessionVo.getLaKey();
        parameterMap.put("lakey", lakey);
    		
    	List list = boardDAO.selectNoticeGrInfoList(parameterMap);
    	if (list != null && !list.isEmpty()) { 
    		resultMap.put("list", list);	
    	}

		return resultMap;	
	}

    public Map selectNoticeUrGrInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    		
    	List list = boardDAO.selectNoticeUrGrInfoList(parameterMap);
    	if (list != null && !list.isEmpty()) { 
    		resultMap.put("list", list);	
    	}
		return resultMap;	
	}
	
	public List selectNotice(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return boardDAO.selectNoticeInfo(parameterMap);
	}

	
	public void insertNoticeRepple(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub

		parameterMap.put("MSG_NO"   , boardDAO.selectMsgNo());
		parameterMap.put("FILE_YN","N");
		this.boardDAO.insertNoticeInfo(parameterMap);
		this.boardDAO.insertNoticeReppleTarget(parameterMap);		
		this.boardDAO.updateNoticeRepple(parameterMap);
	}
	public void insertNotice(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
		parameterMap.put("MSG_NO"   , boardDAO.selectMsgNo());
		
		
    	String fileYn  = "N";
    	List filelist = (List) parameterMap.get("file");
    	
    	if(filelist != null){
    		for(int i = 0 ; i < filelist.size() ; i++){
    			Map fileMap = (Map)filelist.get(i);
    			if("Y".equals(fileMap.get("FILECHG"))){
    				fileYn = "Y";
    			}
    		}
    	}
		parameterMap.put("FILE_YN",fileYn);				
		this.boardDAO.insertNoticeInfo(parameterMap);
    	if(filelist != null){
    		for(int i = 0 ; i < filelist.size() ; i++){
    			Map fileMap = (Map)filelist.get(i);

    			if("Y".equals(fileMap.get("FILECHG"))){
    				long currentTime = System.currentTimeMillis();
    				String folder = new SimpleDateFormat("yyyyMMdd").format(currentTime);
    				fileMap.put("FILE_URL", folder);
    				fileMap.put("MSG_NO", parameterMap.get("MSG_NO"));
    				this.boardDAO.insertFileInfo(fileMap);
    			}
    		}
    	}
    	
    	List grlist = (List) parameterMap.get("gr");
    	if(grlist != null){
    		for(int i = 0 ; i < grlist.size() ; i++){
    			Map inputMap = (Map)grlist.get(i);
    			inputMap.put("MSG_NO", parameterMap.get("MSG_NO"));

    			if(inputMap.get("CHK").equals("1")){
    				this.boardDAO.insertNoticeTargetInfo(inputMap);
    			}
    		}
    	}		
	}
	
	
	

	
	
	public void updateNotice(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
		/*String inserturkey = (String)parameterMap.get("INSERTURKEY");
		String updateurkey = (String)parameterMap.get("UPDATEURKEY");
		System.out.println("+++++++++++++++++++inserturkey : "+inserturkey);
		System.out.println("+++++++++++++++++++updateurkey : "+updateurkey);
		
		int keyStart = inserturkey.indexOf("(");
		int keyEnd = inserturkey.indexOf(")");
		System.out.println("+++++++++++++++++++keyStart : "+keyStart);
		System.out.println("+++++++++++++++++++keyEnd : "+keyEnd);
			
		inserturkey = inserturkey.substring(keyStart+1,keyEnd);
		updateurkey = updateurkey.substring(keyStart+1,keyEnd);
		System.out.println("+++++++++++++++++++inserturkey : "+inserturkey);
		System.out.println("+++++++++++++++++++updateurkey : "+updateurkey);
		
		parameterMap.put("inserturkey", inserturkey);
		parameterMap.put("updateurkey", updateurkey);*/
		
		int cnt = boardDAO.selectNoticeCount(parameterMap);
		LOG.debug("======================================");
		LOG.debug("grCnt => " + cnt);
		LOG.debug("======================================");
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else { 
			
	    	List grlist = (List) parameterMap.get("gr");
	    	this.boardDAO.deleteNoticeTargetInfo(parameterMap);
	    	if(grlist != null){
	    		for(int i = 0 ; i < grlist.size() ; i++){
	    			Map inputMap = (Map)grlist.get(i);
	    			inputMap.put("MSG_NO", parameterMap.get("MSG_NO"));
	    			LOG.debug("======================================");
	    			LOG.debug("inputMap.CHK => " + inputMap.get("CHK"));
	    			LOG.debug("======================================");
	    			if(inputMap.get("CHK").equals("1")){
	    				this.boardDAO.insertNoticeTargetInfo(inputMap);
	    			}
	    		}
	    	}
	    	
			
	    	String fileYn  = "N";
	    	List filelist = (List) parameterMap.get("file");
	    	LOG.debug("filelist => " + filelist);
	    	if(filelist != null){
	    		for(int i = 0 ; i < filelist.size() ; i++){
	    			Map fileMap = (Map)filelist.get(i);

	    			if("Y".equals(fileMap.get("FILECHG"))){
	    				if(!"".equals(fileMap.get("BE_URL"))){
	    					String filePath = EnvConstant.BOARD_FILE_PATH +fileMap.get("BE_URL")+"/"+fileMap.get("BE_FILE_NM");
	    					File file = new File(filePath);
	    					file.delete();
	    					this.boardDAO.deleteFileInfo(fileMap);
	    				}
	    				if(!"".equals(fileMap.get("FILE_NM"))){
	    					fileYn = "Y";
		    				long currentTime = System.currentTimeMillis();
		    				String folder = new SimpleDateFormat("yyyyMMdd").format(currentTime);
		    				fileMap.put("FILE_URL", folder);	    				
		    				this.boardDAO.insertFileInfo(fileMap);
	    				}

	    			}
	    		}
	    	}
			
			parameterMap.put("FILE_YN",fileYn);
			this.boardDAO.updateNoticeInfo(parameterMap);		
		}		
	}
	
	
	
	public void deleteNotice(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = boardDAO.selectDeleteNoticeCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10001);
		} else { 
			try{
				if(!"0".equals(parameterMap.get("PARENT_MSG_NO"))){
					this.boardDAO.deleteNoticeRepple(parameterMap);
				}
				if("Y".equals(parameterMap.get("FILE_YN"))){
					List filelist = boardDAO.selectFileList(parameterMap);
			    	if(filelist != null){
			    		for(int i = 0 ; i < filelist.size() ; i++){
			    			Map fileMap = (Map)filelist.get(i);
	    					String filePath = EnvConstant.BOARD_FILE_PATH +fileMap.get("FILE_URL")+"/"+fileMap.get("FILE_ID");
	    					File file = new File(filePath);
	    					file.delete();
			    		}
			    	}
					this.boardDAO.deleteFileAll(parameterMap);
					
				}
				this.boardDAO.deleteNoticeTargetInfo(parameterMap);
				this.boardDAO.deleteNoticeInfo(parameterMap);	
			}
			catch(Exception e){
				throw new MsgException(1002);
			}
		}		
	}
	
	
	
	
	
	
	
	public Map selectNewsInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = boardDAO.selectNewsCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = boardDAO.selectNewsInfoList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
   	
		return resultMap;	
	}
    
	


	
	public List selectNews(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return boardDAO.selectNewsInfo(parameterMap);
	}

	
	
	public void insertNews(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = boardDAO.selectNewsCount(parameterMap);
		if (cnt > 0) {
			throw new MsgException(10002);
		} else {
						
			this.boardDAO.insertNewsInfo(parameterMap);	
		}
	}
	
	

	
	
	public void updateNews(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
		String inserturkey = (String)parameterMap.get("INSERTURKEY");
		String updateurkey = (String)parameterMap.get("UPDATEURKEY");
		int keyStart = inserturkey.indexOf("(");
		int keyEnd = inserturkey.indexOf(")");
			
		inserturkey = inserturkey.substring(keyStart+1,keyEnd);
		updateurkey = updateurkey.substring(keyStart+1,keyEnd);
		parameterMap.put("inserturkey", inserturkey);
		parameterMap.put("updateurkey", updateurkey);
		
		int cnt = boardDAO.selectNoticeCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else { 
			
			String changefileinfo = (String)parameterMap.get("changefileinfo");
			if("true".equals(changefileinfo)){
//			if(changefileinfo.equals("true")){
				List boardInfo = boardDAO.selectNoticeInfo(parameterMap);
				String filepath = (String)((Map)(boardInfo.get(0))).get("FILE_URL");
				
				if(filepath != null){
					File folderPathFile = new File(filepath);
					if(folderPathFile.exists()){
						folderPathFile.delete();
					}
				}
				this.boardDAO.updateNewsInfo(parameterMap);
			}
			else{
				this.boardDAO.updateNewsInfo(parameterMap);
			}
				
		}
		
	}
	
	
	
	public void deleteNews(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = boardDAO.selectDeleteNewsCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10001);
		} else { 
			try{
				this.boardDAO.deleteNewsInfo(parameterMap);	
			}
			catch(Exception e){
				throw new MsgException(1002);
			}
		}		
	}
	
	@Override
	public List selectExcelDownNotice(Map parameterMap) throws Exception {
		return boardDAO.selectExcelDownNotice(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownNotice(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return boardDAO.selectExcelDownNotice(parameterMap, excelFormat);
	}
		
}
