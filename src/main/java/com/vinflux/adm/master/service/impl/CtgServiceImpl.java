package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.CtgService;
import com.vinflux.adm.persistence.CtgDAO;
import com.vinflux.adm.role.service.impl.OwnerByRoleServiceImpl;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.wms.persistence.CtgWmsDAO;

@Service("ctgService")
public class CtgServiceImpl implements CtgService {
	
	protected static final Log LOG = LogFactory.getLog(OwnerByRoleServiceImpl.class);
	
	@Resource(name="ctgDAO")
	private CtgDAO ctgDAO;
	
//	@Resource(name="ctgIcomDAO")
//	private CtgIcomDAO ctgIcomDAO;
	
	@Resource(name="ctgWmsDAO")
	private CtgWmsDAO ctgWmsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectCtgInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = ctgDAO.selectCtgCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = ctgDAO.selectCtg(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectCtg (Map parameterMap) throws Exception {
		return ctgDAO.selectPkCtg(parameterMap);
	}
	
	@Override
	public int checkCtgCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return ctgDAO.checkCtgCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#insertOwAccount(java.util.Map)
	 */
	@Override
	public void insertOwCtg (CommandMap requestMap) throws Exception {
		// 화면에서 넘견 화주변경 대상 데이터
    	List list = requestMap.getList("list");
    	// 화면에서 넘겨준 변경할 화주 정보
    	Map paramMap = requestMap.getParamMap();
    	String owkey = (String) paramMap.get("owkey");
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			Map resultMap = ctgDAO.selectPkCtgMap(inputMap);
    			resultMap.put("owkey", owkey);
    			resultMap.put("inserturkey", "");
    			resultMap.put("updateurkey", "");
    			insertCtg (resultMap);
    		}
    	}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.CtgService#insertCtg(java.util.Map)
	 */
	@Override
	public void insertCtg (Map parameterMap) throws Exception {
		// 이미 저장된 내역 여부 조회
		List listCnt = ctgDAO.selectPkCtg(parameterMap);
		if(listCnt != null && listCnt.size() > 0) {
			throw new MsgException(20006);
		}

		// ADMIN INSERT
		this.ctgDAO.insertTAdminMstCtg(parameterMap);
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.ctgIcomDAO.insertTOrderMstCtg(parameterMap);	
		}*/
		
		// WMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = ctgDAO.selectCtkey(parameterMap);
			
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			this.ctgWmsDAO.insertTWorkMstCtg(parameterMap);
        		}
        	}
        	
			
			
		}
	}
	
	/** **/
	@Override
	public void updateCtg (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.ctgDAO.updateTAdminMstCtg(parameterMap);
		
		int count = 0;
		
		// ICOM INSERT
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = this.ctgIcomDAO.updateTOrderMstCtg(parameterMap);
			if(count==0){
				this.ctgIcomDAO.updateTOrderMstCtg(parameterMap);
			}
		}*/
		
		// WMS UPDATE 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			count = this.ctgWmsDAO.updateTWorkMstCtg(parameterMap);
			//if(count==0){
				//this.ctgWmsDAO.insertTWorkMstCtg(parameterMap);
			//}
		}
		
		LOG.debug("##[ updateCtg ] update Count : " + count );
	}

	/** **/
	@Override
	public void deleteCtg (Map parameterMap) throws Exception {
		// ICOM DELETE
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.ctgIcomDAO.deleteTOrderMstCtg(parameterMap);
		}*/

		// WMS DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.ctgWmsDAO.deleteTWorkMstCtg(parameterMap);
		}
		
		// ADMIN DELETE
		this.ctgDAO.deleteTAdminMstCtg(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownCtg (Map parameterMap) throws Exception {
    	return ctgDAO.selectExcelDownCtg(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownCtg(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return ctgDAO.selectExcelDownCtg(parameterMap, excelFormat);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map searchCtgInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = ctgDAO.searchCtgCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = ctgDAO.searchCtg(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	} 
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}	
}