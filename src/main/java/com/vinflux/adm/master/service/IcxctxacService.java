package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface IcxctxacService {
	
	/** **/
	public Map selectIcxctxacInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectIcxctxac (Map parameterMap) throws Exception;
	
	/** **/
	public int checkIcxctxacCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertIcxctxac (Map parameterMap) throws Exception;
	
	/** **/
	public void updateIcxctxac (Map parameterMap) throws Exception;

	/** **/
	public void updateIcxctxacAll (Map parameterMap) throws Exception;	
	
	/** **/
	public void deleteIcxctxac (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownIcxctxac (Map parameterMap) throws Exception;

	public Workbook selectExcelDownIcxctxac(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
