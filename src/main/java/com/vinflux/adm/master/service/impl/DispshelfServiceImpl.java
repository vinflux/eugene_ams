package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.DispshelfService;
import com.vinflux.adm.persistence.DispshelfDAO;
import com.vinflux.adm.role.service.impl.OwnerByRoleServiceImpl;
import com.vinflux.wms.persistence.DispshelfWmsDAO;

@Service("dispshelfService")
public class DispshelfServiceImpl implements DispshelfService {
	
	protected static final Log LOG = LogFactory.getLog(OwnerByRoleServiceImpl.class);
	
	@Resource(name="dispshelfDAO")
	private DispshelfDAO dispshelfDAO;
	
	@Resource(name="dispshelfWmsDAO")
	private DispshelfWmsDAO dispshelfWmsDAO;
	
	// 모듈별 전송여부 판별을 위함.
	@Resource(name="commonService")
	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectDispshelfInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = dispshelfDAO.selectDispshelfCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = dispshelfDAO.selectDispshelf(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectDispshelf (Map parameterMap) throws Exception {
		return dispshelfDAO.selectPkDispshelf(parameterMap);
	}
	
	@Override
	public int checkDispshelfCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return dispshelfDAO.checkDispshelfCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.DispshelfService#insertDispshelf(java.util.Map)
	 */
	@Override
	public void insertDispshelf (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.dispshelfDAO.insertTAdminMstDispshelf(parameterMap);
		
		// WMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = dispshelfDAO.selectCtkey(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			this.dispshelfWmsDAO.insertTWorkMstDispshelf(parameterMap);
        		}
        	}
			
			
		}
	}
	
	/** **/
	@Override
	public void updateDispshelf (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.dispshelfDAO.updateTAdminMstDispshelf(parameterMap);
		
		int count = 0;
		
		// WMS UPDATE 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			count = this.dispshelfWmsDAO.updateTWorkMstDispshelf(parameterMap);
		}
		
		LOG.debug("##[ updateDispshelf ] Update count : " + count );
	}

	/** **/
	@Override
	public void deleteDispshelf (Map parameterMap) throws Exception {
		

		// WMS DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.dispshelfWmsDAO.deleteTWorkMstDispshelf(parameterMap);
		}
		
		// ADMIN DELETE
		//this.ownerByRoleDAO.deleteTAdminMstRoxowgt(parameterMap);
		this.dispshelfDAO.deleteTAdminMstDispshelf(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownDispshelf (Map parameterMap) throws Exception {
    	return dispshelfDAO.selectExcelDownDispshelf(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDispshelf(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return dispshelfDAO.selectExcelDownDispshelf(parameterMap, excelFormat);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccount(java.util.Map)
	 */
	@Override
	public Map searchDispshelfInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = dispshelfDAO.searchDispshelfCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = dispshelfDAO.searchDispshelf(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}	
}