package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;


public interface StoreService {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectStoreInfo (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectStore (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCeckStore (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertOwStore (CommandMap parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertStore (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateStore (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteStore (Map parameterMap) throws Exception;
	

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstStorexct (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @param parameterList
	 * @throws Exception
	 */
	public void saveStoreCenter (Map parameterMap,List parameterList) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void saveStoreCenter(Map parameterMap) throws Exception;	
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownStore (Map parameterMap) throws Exception;

	public Workbook selectExcelDownStore(Map parameterMap, List<Map> excelFormat) throws Exception;

}
