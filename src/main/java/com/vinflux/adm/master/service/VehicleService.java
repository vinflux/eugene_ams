package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface VehicleService {

	//검색조건 리스트
	public Map selectEqmtList (Map paramMap) throws Exception;

	public List selectEqmt (Map paramMap) throws Exception;
	
	public int checkEqmt(Map paramMap) throws Exception;
	
	public int insertEqmt (Map paramMap) throws Exception;

	public void updateEqmt (Map paramMap) throws Exception;

	public List selectExcelDown (Map paramMap) throws Exception;
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	public List selectEqmtDtlList(Map paramMap) throws Exception;

	public int checkEqmtDtl(Map param) throws Exception;
	
	public void insertEqmtDtl (Map paramMap) throws Exception;
	
	public void deleteEqmtDtl (Map paramMap) throws Exception;

	public List selectEqmtDtl(Map paramMap) throws Exception;

	public void updateEqmtDtl(Map paramMap) throws Exception;

	public void approveEquipList(List paramList) throws Exception;
	
	public int checkValidEqmtList(List parameterList) throws Exception;

	
}
