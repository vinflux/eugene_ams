package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.IcxctxacService;
import com.vinflux.adm.persistence.IcxctxacDAO;

@Service("icxctxacService")
public class IcxctxacServiceImpl implements IcxctxacService {
	
	@Resource(name="icxctxacDAO")
	private IcxctxacDAO icxctxacDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectIcxctxacInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icxctxacDAO.selectIcxctxacCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icxctxacDAO.selectIcxctxac(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectIcxctxac (Map parameterMap) throws Exception {
		return icxctxacDAO.selectPkIcxctxac(parameterMap);
	}
	
	@Override
	public int checkIcxctxacCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return icxctxacDAO.checkIcxctxacCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.IcxctxacService#insertIcxctxac(java.util.Map)
	 */
	@Override
	public void insertIcxctxac (Map parameterMap) throws Exception {
		// ADMIN INSERT
		/*
		if(parameterMap.get("ibtime") != null){
			if( parameterMap.get("ibtime").toString().length() == 14)
			{
				parameterMap.put("ibtime", parameterMap.get("ibtime").toString().substring(8));
			}
		}*/
		
		this.icxctxacDAO.insertTAdminMstIcxctxac(parameterMap);
		
		// WMS INSERT
		/*
		 * 처리로직 주석으로 인해 전체 주석처리
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			List list = icxctxacDAO.selectCtkey(parameterMap);
//        	if (list != null && !list.isEmpty()) { 
//        		for(int i=0;i<list.size();i++){
//        			Map ctKeyMap = (Map) list.get(i);
//        			parameterMap.remove("ctkey");
//        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
//        			this.icxctxacWmsDAO.insertTWorkMstIcxctxac(parameterMap);
//        		}
//        	}
		}
		*/
	}
	
	/** **/
	@Override
	public void updateIcxctxac (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		/*
		if(parameterMap.get("ibtime") != null){
			if( parameterMap.get("ibtime").toString().length() == 14)
			{
				parameterMap.put("ibtime", parameterMap.get("ibtime").toString().substring(8));
			}
		}*/
		
		this.icxctxacDAO.updateTAdminMstIcxctxac(parameterMap);
		
//		int count = 0;
		
		// WMS UPDATE 
		/*
		 * 처리로직이 없으므로 주석처리
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//count = this.icxctxacWmsDAO.updateTWorkMstIcxctxac(parameterMap);
			if(count==0){
				//this.icxctxacWmsDAO.insertTWorkMstIcxctxac(parameterMap);
			}
		}
		*/
		
	}
	
	/** **/
	@Override
	public void updateIcxctxacAll (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		/*
		if(parameterMap.get("ibtime") != null){
			if( parameterMap.get("ibtime").toString().length() == 14)
			{
				parameterMap.put("ibtime", parameterMap.get("ibtime").toString().substring(8));
			}
		}*/
		
		this.icxctxacDAO.updateTAdminMstIcxctxacAll(parameterMap);
		
//		int count = 0;
		
		// WMS UPDATE 
		/*
		 * 처리내용이 없으므로 주석처리
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//count = this.icxctxacWmsDAO.updateTWorkMstIcxctxac(parameterMap);
			if(count==0){
				//this.icxctxacWmsDAO.insertTWorkMstIcxctxac(parameterMap);
			}
		}
		*/
	}	

	/** **/
	@Override
	public void deleteIcxctxac (Map parameterMap) throws Exception {
		// WMS DELETE
		/*
		 * 처리로직이 주석처리되어 전체 주석
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//this.icxctxacWmsDAO.deleteTWorkMstIcxctxac(parameterMap);
		}
		*/
		
		// ADMIN DELETE
		this.icxctxacDAO.deleteTAdminMstIcxctxac(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownIcxctxac (Map parameterMap) throws Exception {
    	return icxctxacDAO.selectExcelDownIcxctxac(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownIcxctxac(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return icxctxacDAO.selectExcelDownIcxctxac(parameterMap, excelFormat);
	}
}