package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.ApplicationService;
import com.vinflux.adm.persistence.ApplicationDAO;
import com.vinflux.framework.exception.MsgException;

@Service("applicationService")
public class ApplicationServiceImpl implements ApplicationService{

	@Resource(name="applicationDAO")
	private ApplicationDAO applicationDAO;
	
	@Override
	public Map selectApplicationInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = applicationDAO.selectApplicationCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = applicationDAO.selectApplicationInfoList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
   	
		return resultMap;	
	}

	@Override
	public List selectApplication(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return applicationDAO.selectApplicationInfo(parameterMap);
	}

	@Override
	public void insertApplication(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = applicationDAO.checkApplicationInfo(parameterMap);
		if (cnt > 0) {
			throw new MsgException(10002);
		} else { 
			this.applicationDAO.insertApplicationInfo(parameterMap);	
		}
	}

	@Override
	public void updateApplication(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = applicationDAO.selectApplicationCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else { 
			this.applicationDAO.updateApplicationInfo(parameterMap);	
		}
		
	}

	@Override
	public void deleteApplication(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = applicationDAO.selectApplicationCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10005);
		} else { 
			try{
				this.applicationDAO.deleteApplicationInfo(parameterMap);	
			}
			catch(Exception e){
				throw new MsgException(10004);
			}
		}		
	}

	@Override
	public List selectExcelDownApplication(Map parameterMap) throws Exception {
		return applicationDAO.selectExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownApplication(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return applicationDAO.selectExcelDown(parameterMap, excelFormat);
	}

	@Override
	public int checkApplicationInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return applicationDAO.checkApplicationInfo(parameterMap);
	}

}
