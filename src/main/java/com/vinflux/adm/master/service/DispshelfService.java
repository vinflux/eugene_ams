package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface DispshelfService {
	
	/** **/
	public Map selectDispshelfInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectDispshelf (Map parameterMap) throws Exception;
	
	/** **/
	public int checkDispshelfCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertDispshelf (Map parameterMap) throws Exception;
	
	/** **/
	public void updateDispshelf (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteDispshelf (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownDispshelf (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownDispshelf(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map searchDispshelfInfo (Map parameterMap) throws Exception;

	
}
