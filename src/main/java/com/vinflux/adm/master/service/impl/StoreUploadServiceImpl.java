package com.vinflux.adm.master.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.StoreService;
import com.vinflux.adm.master.service.StoreUploadService;
import com.vinflux.adm.persistence.StoreUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("storeUploadService")
public class StoreUploadServiceImpl implements StoreUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="storeUploadDAO")
	private StoreUploadDAO storeUploadDAO;
	
	@Resource(name="mstStoreGenService")
	private IdGenService mstStoreGenService;

	
//	@Resource(name="centerByRoleDAO")
//	private CenterByRoleDAO centerByRoleDAO;
	
	@Resource(name = "storeService")
	private StoreService storeService;
	
	protected static final Log LOG = LogFactory.getLog(StoreUploadServiceImpl.class);
	
//	@Resource(name="txAdmManager")
//	private DataSourceTransactionManager txAdmManager;
	
	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" 	,"owkey"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"Y" 	,"store_name"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"Y" 	,"delyn"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"13"		,"N" 	,"regno"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"N" 	,"address1"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"N" 	,"address2"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" 	,"zipcode"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" 	,"tel1"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" 	,"tel2"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" 	,"owner_name"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" 	,"urkey"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" 	,"closingdate"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" 	,"sc_nm"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" 	,"store_key"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"28"		,"N" 	,"addrid"			,"" }	
		
	};	

	@Override
	public Map selectStoreUploadInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storeUploadDAO.selectStoretUploadCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeUploadDAO.selectStoreUpload(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	
	@Override
	public Map selectStoreTemp(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = storeUploadDAO.selectStoretUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeUploadDAO.selectStoreTemp(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectStoreTempAll(Map parameterMap) throws Exception {
		return storeUploadDAO.selectStoreTempAll(parameterMap);
	}
	
	@Override
	public void updateTadminTempStoreup(Map parameterMap) throws Exception {
		storeUploadDAO.updateTadminTempStoreup(parameterMap);
	}
	
	@Override
	public void saveStoreCode(Map parameterMap, Iterator pItr, Map ctMap) throws Exception {
		String ctKey = MapUtil.getStr(parameterMap, "ctkey");
		String eCtKey = null;
		int txFlag = 0;
		int wmsTxFlag = 0;
		String zipNo = (String) parameterMap.get("zipcode");
		String lnmAdres = (String) parameterMap.get("address1");
		parameterMap.put("zipNo", zipNo);
		parameterMap.put("lnmAdres", lnmAdres);
		
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
		if("".equals(ctKey) || ctKey == null) {
			txFlag = storeUploadDAO.selectPkCntTadminMstStore(parameterMap);
			
			
			if(txFlag == 0) {
				storeService.insertStore(parameterMap);
    		} else {
    			storeService.updateStore(parameterMap);
    		}
			
			while(pItr.hasNext()) {
				eCtKey = pItr.next().toString();
				parameterMap.put("ctkey", eCtKey);
				
				wmsTxFlag = storeUploadDAO.selectPkCntTadminMstStorexCt(parameterMap);
				if(wmsTxFlag == 0) {
					storeService.saveStoreCenter(parameterMap);
    			}
			}
			
		} else {
			if ( !ctMap.containsKey(ctKey) ) {
				throw new MsgException(10013); //유효하지 않은 센터 코드 입니다.
			}
			
    		txFlag = storeUploadDAO.selectPkCntTadminMstStore(parameterMap);

    		if(txFlag == 0) {
    			storeService.insertStore(parameterMap);
    			storeService.saveStoreCenter(parameterMap);
    		} else {
    			storeService.updateStore(parameterMap);
    			wmsTxFlag = storeUploadDAO.selectPkCntTadminMstStorexCt(parameterMap);
    			if(wmsTxFlag == 0) {
    				storeService.saveStoreCenter(parameterMap);
    			}
    		}
		}
		
    	parameterMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_SUCCESS);
		parameterMap.put("ifmessage", "Success");

		this.updateTadminTempStoreup(parameterMap);
	}
	
	    
    @Override
	public String uploadStoreExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			
			String store_key = (String) row.get("store_key");
			String owkey = (String) row.get("owkey");
	    	
			if(store_key == null || "".equals(store_key) || store_key.isEmpty()){
				store_key = mstStoreGenService.getNextStringId();	
				row.put("store_key",owkey+store_key);
			}	
			
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);
	    	row.put("dckey", "1");

	    	

			//ULHSKEY
	    	storeUploadDAO.insertTadminTempStoreup(row);
		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		storeUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }
	
	@Override
	public Workbook selectExcelDownStoreUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return storeUploadDAO.selectExcelDownStoreUpload(parameterMap,excelFormat);
	}
}