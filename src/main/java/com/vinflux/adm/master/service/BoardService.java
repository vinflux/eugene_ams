package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface BoardService {

	
	
	
	public Map selectNoticeInfo (Map parameterMap) throws Exception;
	
	public Map selectFileList (Map parameterMap) throws Exception;
		
	public Map selectNoticeGrInfo (Map parameterMap) throws Exception;
	
	public Map selectNoticeUrGrInfo (Map parameterMap) throws Exception;
	
	public List selectNotice (Map parameterMap) throws Exception;


	
	
	public void insertNotice (Map parameterMap) throws Exception;
	
	
	public void insertNoticeRepple (Map parameterMap) throws Exception;

	
	public void updateNotice (Map parameterMap) throws Exception;

	
	
	
	public void deleteNotice (Map parameterMap) throws Exception;

	
	
	public Map selectNewsInfo (Map parameterMap) throws Exception;

	
	
	public List selectNews (Map parameterMap) throws Exception;


	
	
	public void insertNews (Map parameterMap) throws Exception;
	
	

	
	public void updateNews (Map parameterMap) throws Exception;

	
	
	
	public void deleteNews (Map parameterMap) throws Exception;

	//헤더 그리드 엑셀
	public List selectExcelDownNotice (Map parameterMap) throws Exception;
	
	
	public Workbook selectExcelDownNotice(Map parameterMap, List<Map> excelFormat) throws Exception;


	
	
}
