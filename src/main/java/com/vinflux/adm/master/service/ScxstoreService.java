package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ScxstoreService {
	
	/** **/
	public Map selectScxstoreInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectScxstore (Map parameterMap) throws Exception;
	
	/** **/
	public int checkScxstoreCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertScxstore (Map parameterMap) throws Exception;
	
	/** **/
	public void updateScxstore (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteScxstore (Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map searchStoreInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownScxstore (Map parameterMap) throws Exception;

	public Workbook selectExcelDownScxstore(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
