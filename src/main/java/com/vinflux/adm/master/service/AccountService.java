package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;


public interface AccountService {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectAccountInfo (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectAccount (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertOwAccount (CommandMap parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertAccount (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateAccount (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteAccount (Map parameterMap) throws Exception;
	

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstCt(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTAdminMstAcxct (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @param parameterList
	 * @throws Exception
	 */
	public void saveAccountCenter (Map parameterMap,List parameterList) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void saveAccountCenter(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownAccount (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownAccount(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectRepAccountInfo (Map parameterMap) throws Exception;
}
