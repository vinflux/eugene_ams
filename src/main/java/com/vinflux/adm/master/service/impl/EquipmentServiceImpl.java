package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.EquipmentService;
import com.vinflux.adm.persistence.EquipmentDAO;
import com.vinflux.vims.persistence.EquipmentVimsDAO;
import com.vinflux.wms.persistence.EquipmentWmsDAO;

@Service("equipmentService")
public class EquipmentServiceImpl implements EquipmentService {

	@Resource(name = "equipmentDAO")
	private EquipmentDAO equipmentDAO;

	@Resource(name = "equipmentWmsDAO")
	private EquipmentWmsDAO equipmentWmsDAO;

//	@Resource(name = "equipmentIcomDAO")
//	private EquipmentIcomDAO equipmentIcomDAO;

	@Resource(name = "equipmentVimsDAO")
	private EquipmentVimsDAO equipmentVimsDAO;
	
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;	

	@Override
	public int selectTAdminMstEqCnt(Map parameterMap) throws Exception {
		return equipmentDAO.selectTAdminMstEqCnt(parameterMap);
	}

	@Override
	public Map selectEquipment(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		
		Integer count = equipmentDAO.selectTAdminMstEqCnt(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	
		if(count > 0) {
			List list = equipmentDAO.selectEquipment(parameterMap);
			if(list != null && !list.isEmpty()){
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
    		resultMap.put("list", list);
		}
		
		return resultMap;

	}
	
	@Override
	public Map selectPkTAdminMstEq(Map parameterMap) throws Exception {
		return this.equipmentDAO.selectPkTAdminMstEq(parameterMap);
	}

	// 헤더 그리드 저장
	@Override
	public void saveEquipment(Map parameterMap) throws Exception {
		
		String apKey = (String) parameterMap.get("apkey");
		List adminList = equipmentDAO.selectEquipment(parameterMap);
		if (adminList.isEmpty()) {
			equipmentDAO.insertEquipment(parameterMap);
		} else {
			equipmentDAO.updateEquipment(parameterMap);
		}
		/* 10.21 OMS 삭제작업
		if ("ICOM".equals(apKey)) {
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				int cnt = equipmentIcomDAO.selectTOrderMstEq(parameterMap);
				if (cnt <= 0) {
					equipmentIcomDAO.insertTOrderMstEq(parameterMap);
				} else {
					equipmentIcomDAO.updateTOrderMstEq(parameterMap);
				}
			}
		} else */
		if ("WM".equals(apKey)) {
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				int cnt = equipmentWmsDAO.selectPkCntTWorkMstEq(parameterMap);
				if (cnt > 0) {
					equipmentWmsDAO.updateTWorkMstEq(parameterMap);
				}
			}
		
		} /*else if ("VIMS".equals(apKey)) {
			if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {		// NOPMD - 모듈별 구분 처리를 위한 로직 
				int cnt = equipmentVimsDAO.selectTViewMstEq(parameterMap);
				if (cnt <= 0) {
					equipmentVimsDAO.insertTViewMstEq(parameterMap);
				} else{
					equipmentVimsDAO.updateTViewMstEq(parameterMap);
				}
			}
		}*/
	
	}

	@Override
	public void deleteEquipment(Map parameterMap) throws Exception {
		// wms
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			equipmentWmsDAO.deleteTWorkMstEq(parameterMap);
		}
		// icom
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			equipmentIcomDAO.deleteTOrderMstEq(parameterMap);
		}
		// vims
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			equipmentVimsDAO.deleteTViewMstEq(parameterMap);
		}*/
		
		// admin(TADMIN_MST_ICSBXCT)
		equipmentDAO.deleteTAdminMstEqxct(parameterMap);
		// admin(TADMIN_MST_ICSB)
		equipmentDAO.deleteEquipment(parameterMap);

	}

	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return equipmentDAO.selectTAdminMstCt(parameterMap);
	}

	@Override
	public List selectTAdminMstEqxct(Map parameterMap) throws Exception {
		return equipmentDAO.selectTAdminMstEqxct(parameterMap);
	}

	@Override
	public void saveEquipmentCenter(Map parameterMap, List parameterList) throws Exception {

		// 선택된 장비별 대체코드 마스터 데이타 가져오기
		List equipmentList = equipmentDAO.selectEquipment(parameterMap);
		Map equipmentMaster = (Map) equipmentList.get(0);

		// DB저장센터
		List EqxctCenterList = equipmentDAO.selectTAdminMstEqxct(parameterMap);

		Map equipmentCenter = null; // 화면기준센타
		Map EqxctCenterMap = null; // DB저장 센타

		String masterCenter = null;
		String EqxceCenter = null;

		// 화면에서 넘어온 센터 리스트
		if ( parameterList != null && !parameterList.isEmpty() ) {
			boolean insert = false;
			for (int i = 0; parameterList.size() > i; i++) {
				insert = true;
				equipmentCenter = (Map) parameterList.get(i);
				for (int j = 0; EqxctCenterList.size() > j; j++) {
					EqxctCenterMap = (Map) EqxctCenterList.get(j);
					masterCenter = (String) equipmentCenter.get("ctkey");
					EqxceCenter = (String) EqxctCenterMap.get("ctkey");
					if (masterCenter.equals(EqxceCenter)) {
						insert = false;
						break;
					}
				}
				// 추가분 입력
				if (insert ) {
					// admin 입력
					parameterMap.put("ctkey", equipmentCenter.get("ctkey"));
					parameterMap.put("eqkey", equipmentMaster.get("eqkey"));
					parameterMap.put("prtype", equipmentMaster.get("prtype"));
					equipmentDAO.insertTAdminMstEqxct(parameterMap);
					// wms 입력
					equipmentMaster.put("ctkey", equipmentCenter.get("ctkey"));
					equipmentMaster.put("eqkey", equipmentMaster.get("eqkey"));
					equipmentMaster.put("prtype", equipmentMaster.get("prtype"));
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						equipmentWmsDAO.insertTWorkMstEq(equipmentMaster);
					}
				}
			}
		}

		// 삭제
		if ( EqxctCenterList != null && !EqxctCenterList.isEmpty() ) {
			boolean delete = false;
			for (int i = 0; EqxctCenterList.size() > i; i++) {
				EqxctCenterMap = (Map) EqxctCenterList.get(i);
				delete = true;
				for (int j = 0; parameterList.size() > j; j++) {
					equipmentCenter = (Map) parameterList.get(j);
					masterCenter = (String) equipmentCenter.get("ctkey");
					EqxceCenter = (String) EqxctCenterMap.get("ctkey");
					if (masterCenter.equals(EqxceCenter)) {
						delete = false;
						break;
					}
				}

				// 삭제
				if (delete ) {
					// wms 삭제
					parameterMap.put("ctkey", EqxctCenterMap.get("ctkey"));
					parameterMap.put("eqkey", equipmentMaster.get("eqkey"));
					if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
						equipmentWmsDAO.deleteTWorkMstEq(parameterMap);
					}
					// admin 삭제
					equipmentDAO.deleteTAdminMstEqxct(parameterMap);
				}
			}
		}
	}

	@Override
	public void saveEquipmentCenter(Map parameterMap) throws Exception {
		if (equipmentDAO.selectPkCntTadminMstEqxct(parameterMap) == 0) {
			equipmentDAO.insertTAdminMstEqxct(parameterMap);
		}
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) && equipmentWmsDAO.selectPkCntTWorkMstEq(parameterMap) == 0) {
				equipmentWmsDAO.insertTWorkMstEq(parameterMap);
		}
	}

	@Override
	public List selectEquipmentExcel(Map parameterMap) throws Exception {
		return equipmentDAO.selectEquipmentExcel(parameterMap);
	}
	
	@Override
	public Workbook selectEquipmentExcel(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return equipmentDAO.selectEquipmentExcel(parameterMap, excelFormat);
	}
}