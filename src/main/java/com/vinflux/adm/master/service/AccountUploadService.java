package com.vinflux.adm.master.service;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface AccountUploadService {
	/**
     * 거래처 코드 업로드 임시 테이블 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectAccountUploadInfo (Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 업로드 항목 조회 with paging
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectAccountTemp(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 업로드 항목 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectAccountTempAll(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 임시 저장 테이블 수정
     * @param Map parameterMap
     * @return
     * @throws Exception
     */
	public void updateTadminTempAcup(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드  저장
     * @param Map parameterMap, Iterator pItr, Map ctMap
     * @return
     * @throws Exception
     */
	public void saveAccountCode(Map parameterMap, Iterator pItr, Map ctMap) throws Exception;
	
	/**
     * 거래처 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadAccountExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception;
	
	/**
     * 거래처 코드  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownAccountUpload (Map parameterMap) throws Exception;

	public Workbook selectExcelDownAccountUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
