package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.DrvrService;
import com.vinflux.adm.persistence.DrvrDAO;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;

@Service("drvrService")
public class DrvrServiceImpl implements DrvrService {

@Resource(name="drvrDAO")
private DrvrDAO drvrDAO;

//@Resource(name="drvrTmsDAO")
//private DrvrTmsDAO drvrTmsDAO;

protected static final Log LOG = LogFactory.getLog(DrvrServiceImpl.class);

	@Override
	public Map selectDrvrList(Map paramMap) throws Exception {
		Map resultMap = new HashMap();

		Integer count = drvrDAO.selectDrvrCount(paramMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0 ) {
	    	List list = drvrDAO.selectDrvrList(paramMap);

	    	if (list != null && !list.isEmpty()) {
	    		resultMap.put("list", list);
	    	}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}

	@Override
	public List selectDrvr(Map paramMap) throws Exception {
		return drvrDAO.selectDrvr(paramMap);
	}

	@Override
	public int checkDrvr(Map paramMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("paramMap => " + paramMap);
		LOG.debug("======================================");

		return drvrDAO.selectDrvrCount(paramMap);
	}

	/**
	 * 승인 대상이 삭제 or 이미 활성화 상태인지 체크
	 */
	@Override
	public int checkValidDrvrList(List paramList) throws Exception {
		//List carrcdList = new ArrayList();
		List<String> list = new ArrayList<String>();
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> checkValidDrvrList - drvno :: " + paramMap.get("drvno"));
			list.add((String)paramMap.get("drvno"));
		}
		return drvrDAO.checkValidDrvrList(list);
	}
	
	@Override
	public void updateDrvr(Map paramMap) throws Exception {
		String pushid = MapUtil.getStr(paramMap, "pushid");
		
		LOG.debug("## updateDrvr : " + pushid);
		
		if(StringUtil.isNotEmpty(pushid)){
			this.drvrDAO.updateDrvrRegId(paramMap);
		}
	}

	@Override
	public List selectExcelDown(Map paramMap) throws Exception {
		return drvrDAO.selectExcelDown(paramMap);
	}
	
	@Override
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
				throws Exception {
		return drvrDAO.selectExcelDown(parameterMap, excelFormat);
	}
	
	@Override
	public void approveDrvrList(List paramList) throws Exception {
		
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> approveDrvrList - drvno :: " + paramMap.get("drvno"));
			LOG.debug("> approveDrvrList - sttcd :: " + paramMap.get("sttcd"));
			drvrDAO.approveDrvr(paramMap);
			//drvrTmsDAO.approveTMSDrvr(paramMap);
		}
	}
}
