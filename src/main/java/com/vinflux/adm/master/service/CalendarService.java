package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface CalendarService {
	
	/**
	 * ADMIN > 관리자 마스터 > 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCalendarInfo (Map parameterMap) throws Exception;
	
	/**
	 * ADMIN > 관리자 마스터 > PK 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectCalendar (Map parameterMap) throws Exception;
	
	/**
	 * ADMIN > 관리자 마스터 > 등록
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertCalendar (Map parameterMap) throws Exception;
	
	/**
	 * ADMIN > 관리자 마스터 > 수정
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateCalendar (Map parameterMap) throws Exception;
	
	/**
	 * ADMIN > 관리자 마스터 > 삭제 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteCalendar (Map parameterMap) throws Exception;
	
	/**
	 * ADMIN > 관리자 마스터 > 엑셀 다운 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownCalendar (Map parameterMap) throws Exception;

	public Workbook selectExcelDownCalendar(Map parameterMap,
			List<Map> excelFormat)throws Exception;
}