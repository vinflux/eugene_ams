package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface MasterAdminCodeService {

	public Map selectAdminCodeInfo(Map parameterMap) throws Exception;
	
	public Map selectAdminCode(Map parameterMap) throws Exception;
	
	public void insertAdminCodeInfo(Map parameterMap) throws Exception;
	
	public void updateAdminCodeInfo(Map parameterMap) throws Exception;
	
	public void deleteAdminCodeInfo(Map parameterMap) throws Exception;
	
	public List selectAdminCodeExcelDown(Map parameterMap) throws Exception ;

	public int checkAdminCodeInfo(Map parameterMap) throws Exception;

	public List selectExcelDownAdminCode (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownAdminCode(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	public Map selectDetailAdminCodeInfo(Map parameterMap) throws Exception;
	
	public void insertDetailAdminCodeInfo(Map parameterMap, Map amsHDCodeMap) throws Exception;
	
	public void updateDetailAdminCodeInfo(Map parameterMap) throws Exception;
	
	public void deleteDetailAdminCodeInfo(Map parameterMap, boolean isRootDelete) throws Exception;
	
	public int checkDetailAdminCodeInfo(Map parameterMap) throws Exception;
	
	public List selectExcelDownDetailAdminCode (Map parameterMap) throws Exception;

	public Workbook selectExcelDownDetailAdminCode(Map parameterMap,
			List<Map> excelFormat)throws Exception;

	
}
