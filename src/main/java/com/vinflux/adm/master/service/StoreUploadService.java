package com.vinflux.adm.master.service;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface StoreUploadService {
	/**
     * 거래처 코드 업로드 임시 테이블 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectStoreUploadInfo (Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 업로드 항목 조회 with paging
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectStoreTemp(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 업로드 항목 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectStoreTempAll(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드 임시 저장 테이블 수정
     * @param Map parameterMap
     * @return
     * @throws Exception
     */
	public void updateTadminTempStoreup(Map parameterMap) throws Exception;
	
	/**
     * 거래처 코드  저장
     * @param Map parameterMap, Iterator pItr, Map ctMap
     * @return
     * @throws Exception
     */
	public void saveStoreCode(Map parameterMap, Iterator pItr, Map ctMap) throws Exception;
	
	/**
     * 거래처 코드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadStoreExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception;

	public Workbook selectExcelDownStoreUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
