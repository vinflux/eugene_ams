package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vinflux.adm.master.service.VehicleService;
import com.vinflux.adm.persistence.VehicleDAO;
import com.vinflux.framework.exception.MsgException;


@Service("vehicleService")
public class VehicleServiceImpl implements VehicleService {

@Resource(name="vehicleDAO")
private VehicleDAO vehicleDAO;

//@Resource(name="vehicleTmsDAO")
//private VehicleTmsDAO vehicleTmsDAO;

@Resource(name="txTmsManager")
private DataSourceTransactionManager txTmsManager;

protected static final Log LOG = LogFactory.getLog(VehicleServiceImpl.class);

	@Override
	public Map selectEqmtList(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		
		Integer count = vehicleDAO.selectEqmtCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		
		if (count > 0 ) {     		
	    	List list = vehicleDAO.selectEqmtList(parameterMap);
	    	
	    	if (list != null && !list.isEmpty() ) {
	    		resultMap.put("list", list);
	    	}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}
	
	@Override
	public List selectEqmt(Map parameterMap) throws Exception {
		return vehicleDAO.selectEqmt(parameterMap);
	}
	
	@Override
	public int checkEqmt(Map parameterMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("parameterMap => " + parameterMap);
		LOG.debug("======================================");
		
		return vehicleDAO.selectEqmtCount(parameterMap);
	}
	
	@Override
	public void updateEqmt(Map parameterMap) throws Exception {
	    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus tmsSts = txTmsManager.getTransaction(def);
        
        try {
    		int cnt = vehicleDAO.selectEqmtCount(parameterMap);
    	
    		LOG.debug("======================================");
    		LOG.debug("cnt => " + cnt);
    		LOG.debug("======================================");
    		
    		if (cnt <= 0) {
    			throw new MsgException(10003);
    		} else { 
    			try{
    				vehicleDAO.updateEqmt(parameterMap);
    				
    				//AMS TADMIN_EQUIP_T TABLE UPDATE 
    				//amsEqmtDAO.updateAmsEqmt(parameterMap);
    			}
    			catch(Exception e){
    				throw new MsgException(1002);
    			}
    		}
        }catch(MsgException msgE){
        	LOG.error("### ERROR=>"+msgE.getLog());
        	txTmsManager.rollback(tmsSts);
            throw msgE;
        }catch (Exception e) {
            LOG.error("", e);
            txTmsManager.rollback(tmsSts);
            throw e;
        } finally {
            if(!tmsSts.isCompleted()){
            	txTmsManager.commit(tmsSts);
            }
        }
	}
	


	//DTL START=======================================
	
	@Override
	public List selectEqmtDtlList(Map parameterMap) throws Exception {
		return vehicleDAO.selectEqmtDtlList(parameterMap);
	}
	
	@Override
	public int checkEqmtDtl(Map parameterMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("parameterMap => " + parameterMap);
		LOG.debug("======================================");
		
		return vehicleDAO.selectEqmtDtlCount(parameterMap);
	}

	@Override
	public void insertEqmtDtl(Map parameterMap) throws Exception {
		vehicleDAO.insertEqmtDtl(parameterMap);
	}
	
	@Override
	public void updateEqmtDtl(Map parameterMap) throws Exception {
		int cnt = vehicleDAO.selectEqmtDtlCount(parameterMap);
	
		LOG.debug("======================================");
		LOG.debug("cnt => " + cnt);
		LOG.debug("======================================");
		
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else { 
			try{
				vehicleDAO.updateEqmtDtl(parameterMap);	
			}
			catch(Exception e){
				throw new MsgException(1002);
			}
		}
	}

	@Override
	public void deleteEqmtDtl(Map parameterMap) throws Exception {
		int cnt = vehicleDAO.selectEqmtDtlCount(parameterMap);
		
		LOG.debug("======================================");
		LOG.debug("cnt => " + cnt);
		LOG.debug("======================================");
		
		if (cnt <= 0) {
			throw new MsgException(10001);
		} else { 
			try{
				vehicleDAO.deleteEqmtDtl(parameterMap);	
			}
			catch(Exception e){
				throw new MsgException(1002);
			}
		}
	}

	@Override
	public List selectEqmtDtl(Map paramMap) throws Exception {
		return vehicleDAO.selectEqmtDtl(paramMap);
	}

	@Override
	public int insertEqmt(Map paramMap) throws Exception {
		return vehicleDAO.insertEqmt(paramMap);
		
	}

	@Override
	public List selectExcelDown(Map paramMap) throws Exception {
		return vehicleDAO.selectExcelDown(paramMap);
	}
	
	@Override
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
				throws Exception {
		return vehicleDAO.selectExcelDown(parameterMap,excelFormat);
	}
	
	@Override
	public void approveEquipList(List paramList) throws Exception {
		
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> approveEquipList - equipid :: " + paramMap.get("equipid"));
			LOG.debug("> approveEquipList - sttcd :: " + paramMap.get("sttcd"));
			vehicleDAO.approveEqmt(paramMap);
			//vehicleTmsDAO.approveTMSEquip(paramMap);
		}
	}

	@Override
	public int checkValidEqmtList(List paramList) throws Exception {
		//List carrcdList = new ArrayList();
		List<String> list = new ArrayList<String>();
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> checkValidEqmtList - equipid :: " + paramMap.get("equipid"));
			list.add((String)paramMap.get("equipid"));
		}
		return vehicleDAO.checkValidEqmtList(list);
	}
}
