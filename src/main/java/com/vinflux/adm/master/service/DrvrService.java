package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
/**
 * 운전기사 관련 서비스
 * @author gltkorea
 *
 */
public interface DrvrService {

	/**
	 * 운전기사 검색조건 리스트
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author gltkorea
	 * @since 2014. 3. 21.
	 */
	public Map selectDrvrList (Map parameterMap) throws Exception;

	/**
	 * 운전기사 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author gltkorea
	 * @since 2014. 3. 21.
	 */
	public List selectDrvr (Map parameterMap) throws Exception;

	/**
	 * 운전기사 존재유무
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author gltkorea
	 * @since 2014. 3. 21.
	 */
	public int checkDrvr(Map parameterMap) throws Exception;

	public int checkValidDrvrList(List parameterList) throws Exception;
	
	/**
	 * 운전기사 수정
	 * @param parameterMap
	 * @throws Exception
	 * @author gltkorea
	 * @since 2014. 3. 21.
	 */
	public void updateDrvr (Map parameterMap) throws Exception;

	/**
	 * 운전기사 엑셀다운
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 * @author gltkorea
	 * @since 2014. 3. 21.
	 */
	public List selectExcelDown (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	/**
	 * 승인
	 * @param paramList
	 * @throws Exception
	 */
	public void approveDrvrList(List paramList) throws Exception;

	
}

