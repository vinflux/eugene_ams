package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface CntrService {
	
	/** **/
	public Map selectCntrInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectCntr (Map parameterMap) throws Exception;
	
	/** **/
	public int checkCntrCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertCntr (Map parameterMap) throws Exception;
	
	/** **/
	public void updateCntr (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteCntr (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownCntr (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownCntr(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	public Map searchCntrKeySearchInfo (Map parameterMap) throws Exception;

	
}
