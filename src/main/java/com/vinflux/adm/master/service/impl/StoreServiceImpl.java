package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.master.service.StoreService;
import com.vinflux.adm.persistence.StoreDAO;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.tms.persistence.StoreTmsDAO;
import com.vinflux.wms.persistence.StoreWmsDAO;



@Service("storeService")
public class StoreServiceImpl implements StoreService {
	
	@Resource(name="storeDAO")
	private StoreDAO storeDAO;

//	@Resource(name="storeIcomDAO")
//	private StoreIcomDAO storeIcomDAO;
	
	@Resource(name="storeWmsDAO")
	private StoreWmsDAO storeWmsDAO;
	
	@Resource(name="storeTmsDAO")
	private StoreTmsDAO storeTmsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Resource(name = "entityAddrService")
	private EntityAddrService entityAddrService;	
	
	@Resource(name="mstStoreGenService")
	private IdGenService mstStoreGenService;

	
    protected static final Log LOG = LogFactory.getLog(StoreService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StoreService#selectStoreInfo(java.util.Map)
	 */
	@Override
	public Map selectStoreInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storeDAO.selectStoreCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storeDAO.selectStore(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StoreService#selectStore(java.util.Map)
	 */
	@Override
	public Map selectStore(Map parameterMap) throws Exception {
    	return storeDAO.selectPkStore(parameterMap);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StoreService#selectStore(java.util.Map)
	 */
	@Override
	public Map selectCeckStore(Map parameterMap) throws Exception {
    	return storeDAO.selectCeckStore(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#insertOwAccount(java.util.Map)
	 */
	@Override
	public void insertOwStore (CommandMap requestMap) throws Exception {
    	List list = requestMap.getList("list");
    	List returnList = new ArrayList();
    	Map paramMap = requestMap.getParamMap();
    	String owkey = (String) paramMap.get("owkey");
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			Map resultMap = selectStore(inputMap);
    			resultMap.put("owkey", owkey);
    			resultMap.put("inserturkey", "");
    			resultMap.put("updateurkey", "");
    			resultMap.put("store_key", "");
    			insertStore (resultMap);
    		}
    	}
//    	System.out.println("aaaa".substring(0, 1000));
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StoreService#insertStore(java.util.Map)
	 */
	@Override
	public void insertStore (Map parameterMap) throws Exception {
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		
		String store_key = (String) parameterMap.get("store_key");
		String owkey = (String) parameterMap.get("owkey");
		
		if("".equals(store_key) || store_key.isEmpty()){
			store_key = mstStoreGenService.getNextStringId();	
			parameterMap.put("store_key",owkey+store_key);
		}	

		int cnt = storeDAO.selectPkCntTadminMstStorexct(parameterMap);
		if(cnt > 0) {
			throw new MsgException(20006);
		}
		
		/* 위경도 정보(TADMIN_MST_ADDR_T) 추후 의사 결정 필요 - 2017.11.08-ksh*/
 		if(!"".equals(parameterMap.get("zipNo"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
		
		
		// INSERT ADMIN 
		storeDAO.insertTAdminMstStore(parameterMap);
		storeDAO.mergeTtShpgLocT(parameterMap);
		
		/* 10.21 OMS 삭제작업
		// ICOM
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			storeIcomDAO.insertTOrderMstStore(parameterMap);
		}*/
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			storeTmsDAO.insertTTMstStore(parameterMap);
			// 위경도 정보(TADMIN_MST_ADDR_T) 추후 의사 결정 필요 - 2017.11.08-ksh
			entityAddrService.addAddressEntitymasterTms(parameterMap);
		}
	
		// WMS
		entityAddrService.addAddressEntitymasterWms(parameterMap);
		
		// AMS, TMS, WMS --센터 맵핑
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
			//List chkList  = storeDAO.selectTAdminMstStorexct(parameterMap);//detail mapping data
			List chkList = storeDAO.selectTAdminMstCt(parameterMap);
			//Map  storeMap = storeDAO.selectPkStore(parameterMap);//selected header data
			//List loggList = storeDAO.selectAdminMstLogg(parameterMap);
			
			if (chkList != null && !chkList.isEmpty() ) {
				
				for (int i=0 ;chkList.size() > i ; i++ ) { 
					
					//for(int j=0;loggList.size() > j; j++){
						Map chkRow = (Map)chkList.get(i);
						String ctkey = (String)chkRow.get("ctkey");
						parameterMap.put("ctkey", ctkey);
						
						//Map chkRow2 = (Map)loggList.get(j);
						//String loggrpcd = (String)chkRow2.get("loggrpcd");
						parameterMap.put("dckey", "1");
						//parameterMap.put("loggrpcd", "1");
						parameterMap.put("icgrkey", "02");
						storeDAO.insertTAdminMstStorexct(parameterMap);
						storeTmsDAO.insertTTMstStorexct(parameterMap);
						storeWmsDAO.insertTWorkMstStore(parameterMap);
					//}
					
				}
			}
			
		}
		
	/*	if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			storeVimsDAO.insertTViewMstAc(parameterMap);
		}*/
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StoreService#updateStore(java.util.Map)
	 */
	@Override
	public void updateStore (Map parameterMap) throws Exception {
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		
		//parameterMap.put("loggrpcd", "1");
		
	/*	String ctynm2 = (String)parameterMap.get("address1");
		String zipcd2 = (String)parameterMap.get("zipcode");*/
		
		/*if(ctynm2 != null && !"".equals(ctynm2) && !ctynm2.isEmpty()){
			//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			ctynm = ctynm2;
			zipcd = zipcd2;
		}*/
		
		if("".equals(parameterMap.get("addrid")) &&!"".equals(parameterMap.get("zipNo"))&&!"".equals(parameterMap.get("address2"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
				
		int resultint = 0;
		/* 10.21 OMS 삭제작업
		// ICOM
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {			
			resultint = storeIcomDAO.updateTOrderMstStore(parameterMap);
			//데이타가 없으면 insert
			if(resultint==0){
				storeIcomDAO.insertTOrderMstStore(parameterMap);
			}
		}*/
		
	/*	 위경도 정보(TADMIN_MST_ADDR_T) 
		if(!"".equals(parameterMap.get("zipNo")) && !"".equals(parameterMap.get("addrid"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			//entityAddrService.addAddressEntitymaster(parameterMap);
		}*/
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {			
			resultint = storeTmsDAO.updateTTMstStore(parameterMap);
			entityAddrService.changeAddressEntitymasterTms(parameterMap);
			if(resultint==0){
				storeTmsDAO.insertTTMstStore(parameterMap);
				//위경도 정보(TADMIN_MST_ADDR_T) 추후 의사 결정 필요 - 2017.11.08-ksh
			}
			/*if(!"".equals(parameterMap.get("addrid"))){
				entityAddrService.changeAddressEntitymasterTms(parameterMap);
			}*/
		}
		// WMS
		entityAddrService.changeAddressEntitymasterWms(parameterMap);
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ){
			List chkList  = storeDAO.selectTAdminMstStorexct(parameterMap);//detail mapping data
			Map  storeMap = storeDAO.selectPkStore(parameterMap);//selected header data
			
			if (chkList != null && !chkList.isEmpty() ) {
				
				for (int i=0 ;chkList.size() > i ; i++ ) { 
					Map chkRow = (Map)chkList.get(i);
					String ctkey = (String)chkRow.get("ctkey");
					storeMap.put("ctkey", ctkey);
					
					resultint = storeWmsDAO.updateTWorkMstStore(parameterMap);
					if(resultint==0){
						storeWmsDAO.insertTWorkMstStore(storeMap);
					}
					
				}
			}
	
		}
		// VIMS
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {			
			resultint = storeVimsDAO.updateTViewMstAc(parameterMap);			
			if(resultint==0){
				storeVimsDAO.insertTViewMstAc(parameterMap);
			}
		}*/
		// UPDATE ADMIN
		storeDAO.updateTAdminMstStore(parameterMap);
		entityAddrService.changeAddressEntitymaster(parameterMap);
		storeDAO.mergeTtShpgLocT(parameterMap);
	}

	@Override
	public void deleteStore (Map parameterMap) throws Exception {
		
		// DELETE WMS 
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			parameterMap.put("delyn", "Y");
			storeWmsDAO.deleteTWorkMstStore(parameterMap);
		}

		/* 10.21 OMS 삭제작업
		// ICOM
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			storeIcomDAO.deleteTOrderMstStore(parameterMap);
		}*/
		
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("delyn", "Y");
			storeTmsDAO.deleteTTMstStorexct(parameterMap);
			storeTmsDAO.deleteTTMstStore(parameterMap);
		}
		
		// VIMS
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			storeVimsDAO.deleteTViewMstAc(parameterMap);
		}*/
		
		
		// DELETE ADMIN
		parameterMap.put("delyn", "Y");
		storeDAO.deleteTAdminMstStorexct(parameterMap);
		storeDAO.deleteTAdminMstStore(parameterMap);	
		storeDAO.mergeTtShpgLocT(parameterMap);
	}
	
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return storeDAO.selectTAdminMstCt(parameterMap);
	}
	
	@Override
	public List selectTAdminMstStorexct(Map parameterMap) throws Exception {
		return storeDAO.selectTAdminMstStorexct(parameterMap);
	}

	@Override
	public void saveStoreCenter(Map parameterMap ,  List parameterList) throws Exception {
		List chkList = storeDAO.selectTAdminMstStorexct(parameterMap);
		for(Object rowObj: chkList) {
			Map row = (Map)rowObj;
			row.put("dckey", "1");
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				row.put("delyn", "Y");
				storeWmsDAO.deleteTWorkMstStore(row);
			}
			row.put("delyn", "Y");
			storeDAO.deleteTAdminMstStorexct(row);
			storeTmsDAO.deleteTTMstStorexct(row);
		}

		for(Object rowObj: parameterList) {
			int cnt = 0;
			Map row = (Map) rowObj;
			row.put("dckey", "1");
			
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				cnt = storeWmsDAO.selectPkCntTWorkMstStorexct(row);
				if(cnt > 0) {
					row.put("delyn", "N");
					storeWmsDAO.deleteTWorkMstStore(row);
				} else {
					storeWmsDAO.insertTWorkMstStore(row);
				}
			}
			cnt = storeDAO.selectPkCntTadminMstStorexct(row);
			if(cnt > 0) {
				row.put("delyn", "N");
				storeDAO.deleteTAdminMstStorexct(row);
				storeTmsDAO.deleteTTMstStorexct(row);
			} else {
				storeDAO.insertTAdminMstStorexct(row);
				storeTmsDAO.insertTTMstStorexct(row);
			}
		}
	}
	
	
	@Override
	public void saveStoreCenter(Map parameterMap) throws Exception {
		/*String zipcode = (String)parameterMap.get("zipNo");
		String address1 = (String)parameterMap.get("lnmAdres");
		parameterMap.put("zipcode", zipcode);
		parameterMap.put("address1",address1);*/
		
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			int cntSel = storeWmsDAO.selectPkCntTWorkMstStorexct(parameterMap);
			if ( cntSel == 0) {
				storeWmsDAO.insertTWorkMstStore(parameterMap);
			}
		}
		
		//추후 결정 - 2017.11.07 : ksh
		if (storeDAO.selectPkCntTadminMstStorexct(parameterMap) == 0) {
			storeDAO.insertTAdminMstStorexct(parameterMap);
		}
	}	
	
	
	@Override
	public List selectExcelDownStore(Map parameterMap) throws Exception {
		return storeDAO.selectExcelDownStore(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownStore(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return storeDAO.selectExcelDownStore(parameterMap, excelFormat);
	}

}