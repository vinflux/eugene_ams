package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.CarrierService;
import com.vinflux.adm.persistence.CarrierDAO;

@Service("carrierService")
public class CarrierServiceImpl implements CarrierService {

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(CarrierServiceImpl.class);

	@Resource(name="carrierDAO")
	private CarrierDAO carrierDAO;
	
//	@Resource(name="carrierTmsDAO")
//	private CarrierTmsDAO carrierTmsDAO;

//	@Resource(name = "carrierService")
//	private CarrierService carrierService;
	
	
	@Override
	public Map selectCarrInfo(Map paramMap) throws Exception {
		Map resultMap = new HashMap();
		int count = carrierDAO.selectCarrCount(paramMap);
		resultMap.put("cnt", Integer.toString(count));
		if (count > 0 ) {
			List list = carrierDAO.selectCarr(paramMap);
			if (list != null && !list.isEmpty()) { 
				resultMap.put("list", list);	
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;	
	}

	@Override
	public Map selectCarr(Map paramMap) throws Exception {
		return carrierDAO.selectPkCarr(paramMap);
	}	

	@Override
	public List selectExcelDownCarr(Map paramMap) throws Exception {
		return carrierDAO.selectExcelDown(paramMap);
	}
	
	@Override
	public Workbook selectExcelDownCarr(Map parameterMap, List<Map> excelFormat) throws Exception {
		return carrierDAO.selectExcelDown(parameterMap, excelFormat);
	}

	@Override
	public List selectCarrCdReport(Map paramMap) throws Exception {
		// TODO Auto-generated method stub
		return carrierDAO.selectCarrCdReport(paramMap);
	}

	/**
	 * TADMIN_CARR_T update -> TT_CARR_T update
	 */
	@Override
	public void approveCarrList(List paramList) throws Exception {
		
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> approveCarrList - carrcd :: " + paramMap.get("carrcd"));
			LOG.debug("> approveCarrList - sttcd :: " + paramMap.get("sttcd"));
			carrierDAO.approveCarr(paramMap);
			//carrierTmsDAO.approveTMSCarr(paramMap);
		}
	}

	/**
	 * 승인 대상이 삭제 or 이미 활성화 상태인지 체크
	 */
	@Override
	public int checkValidCarrList(List paramList) throws Exception {
		//List carrcdList = new ArrayList();
		List<String> list = new ArrayList<String>();
		for(int i=0; i<paramList.size(); i++){
			Map paramMap = (Map)paramList.get(i);
			LOG.debug("> approveCarrList - carrcd :: " + paramMap.get("carrcd"));
			list.add((String)paramMap.get("carrcd"));
		}
		return carrierDAO.checkValidCarrList(list);
	}
}
