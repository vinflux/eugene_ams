package com.vinflux.adm.master.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.StoreTrans;
import com.vinflux.adm.master.service.StoreUploadService;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Service("storeTrans")
public class StoreTransImpl implements StoreTrans {
	
	
//	@Resource(name="accountUploadDAO")
//	private AccountUploadDAO accountUploadDAO;
	
//	@Resource(name = "accountService")
//	private AccountService accountService;
	
	@Resource(name = "storeUploadService")
	private StoreUploadService storeUploadService;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	protected static final Log LOG = LogFactory.getLog(StoreTransImpl.class);

	@Override
	public Map saveStoreUpload(Map parameterMap ,  List parameterList) throws Exception {
    	if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104);
    	}

//    	String ulhsKey = (String)parameterMap.get("ulhskey");
    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
		
		CommandController cc = new CommandController();
    	Map msgMap = new HashMap();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    	
    	//center list
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	//actype list
    	getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "STORETYPE");
    	Map storeTypeMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
    	Set ctSet = ctMap.keySet();
    	Iterator ctIrt = ctSet.iterator();
    	int size = parameterList.size();
    	
    	for (int i=0 ;size > i ; i++ ) { 
	    	Map storeMap = (Map)parameterList.get(i);
			try {
				storeMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
				storeUploadService.updateTadminTempStoreup(storeMap);
				
		    	String storeType = MapUtil.getStr(storeMap, "store_type");
		    	
				if(storeType != null && !storeTypeMap.containsKey(storeType) ) {
					throw new MsgException(10015); // 유효하지 않은 거래처 유형입니다.
				}
				
				storeUploadService.saveStoreCode(storeMap, ctIrt, ctMap);
			}catch( DataAccessException dae ){
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				storeMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				storeMap.put("ifmessage", ifMessage);
				storeUploadService.updateTadminTempStoreup(storeMap);
			}catch( SQLException se ){
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				storeMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				storeMap.put("ifmessage", ifMessage);
				storeUploadService.updateTadminTempStoreup(storeMap);
			}catch( MsgException me ){
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap",me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				storeMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				storeMap.put("ifmessage", ifMessage);
				storeUploadService.updateTadminTempStoreup(storeMap);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e); 
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				storeMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				storeMap.put("ifmessage", ifMessage);
				storeUploadService.updateTadminTempStoreup(storeMap);
			} 
    	}

    	// Select 
    	return storeUploadService.selectStoreUploadInfo(parameterMap);
    }

}
