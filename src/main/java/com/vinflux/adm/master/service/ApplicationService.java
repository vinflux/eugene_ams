package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ApplicationService {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectApplicationInfo (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectApplication (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkApplicationInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertApplication (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateApplication (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteApplication (Map parameterMap) throws Exception;
	

		/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownApplication (Map parameterMap) throws Exception;

	public Workbook selectExcelDownApplication(Map parameterMap,
			List<Map> excelFormat) throws Exception;
	
}
