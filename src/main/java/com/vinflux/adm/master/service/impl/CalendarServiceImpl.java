package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.CalendarService;
import com.vinflux.adm.persistence.CalendarDAO;
import com.vinflux.framework.util.MapUtil;

@Service("calendarService")
public class CalendarServiceImpl implements CalendarService {
	
	@Resource(name="calendarDAO")
	private CalendarDAO calendarDAO;
	
	/**
	 * ADMIN > 관리자 마스터 > 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map selectCalendarInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = calendarDAO.selectCalendarCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = calendarDAO.selectCalendar(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/**
	 * ADMIN > 관리자 마스터 > PK 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map selectCalendar (Map parameterMap) throws Exception {
		return calendarDAO.selectPkCalendar(parameterMap);
	}

	/**
	 * ADMIN > 관리자 마스터 > 등록
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void insertCalendar (Map parameterMap) throws Exception {
		String cfulldate = MapUtil.getStr(parameterMap , "cfulldate");
		
		String year = cfulldate.substring(0, 4);
		String month = cfulldate.substring(4, 6);
		String cdate = cfulldate.substring(6, 8);

		parameterMap.put("cyear", year);
		parameterMap.put("cmonth", month);
		parameterMap.put("cdate", cdate);

		this.calendarDAO.insertCalendar(parameterMap);	
	}

	/**
	 * ADMIN > 관리자 마스터 > 수정
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void updateCalendar (Map parameterMap) throws Exception {
		this.calendarDAO.updateCalendar(parameterMap);
	}

	/**
	 * ADMIN > 관리자 마스터 > 삭제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void deleteCalendar (Map parameterMap) throws Exception {
		this.calendarDAO.deleteCalendar(parameterMap);
	}

	/**
	 * ADMIN > 관리자 마스터 > 엑셀다운
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectExcelDownCalendar (Map parameterMap) throws Exception {
    	return calendarDAO.selectExcelDownCalendar(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownCalendar(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return calendarDAO.selectExcelDownCalendar(parameterMap, excelFormat);
	}
}