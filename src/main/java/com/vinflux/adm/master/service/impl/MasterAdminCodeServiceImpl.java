package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.MasterAdminCodeService;
import com.vinflux.adm.persistence.CenterDAO;
import com.vinflux.adm.persistence.MasterAdminCodeDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.icom.persistence.AdminCodeIcomDAO;
import com.vinflux.pms.persistence.AdminCodePmsDAO;
import com.vinflux.bbs.persistence.AdminCodeBbsDAO;
import com.vinflux.portal.persistence.AdminCodePortalDAO;
import com.vinflux.tms.persistence.AdminCodeTmsDAO;
import com.vinflux.vims.persistence.AdminCodeVimsDAO;
import com.vinflux.wms.persistence.AdminCodeWmsDAO;



@Service("masterAdminCodeService")
public class MasterAdminCodeServiceImpl implements MasterAdminCodeService{

	@Resource(name="masterAdminCodeDAO")
	private MasterAdminCodeDAO masterAdminCodeDAO;

	@Resource(name="adminCodeIcomDAO")
	private AdminCodeIcomDAO adminCodeIcomDAO;

	@Resource(name="adminCodeTmsDAO")
	private AdminCodeTmsDAO adminCodeTmsDAO;
	
	@Resource(name="adminCodePortalDAO")
	private AdminCodePortalDAO adminCodePortalDAO;
	
	@Resource(name="adminCodeVimsDAO")
	private AdminCodeVimsDAO adminCodeVimsDAO;
	
	@Resource(name="adminCodePmsDAO")
	private AdminCodePmsDAO adminCodePmsDAO;
	
	@Resource(name="adminCodeBbsDAO")
	private AdminCodeBbsDAO adminCodeBbsDAO;
	
	@Resource(name="adminCodeWmsDAO")
	private AdminCodeWmsDAO adminCodeWmsDAO;
	
	@Resource(name="centerDAO")
	private CenterDAO centerDAO;

	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Override
	public Map selectAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = masterAdminCodeDAO.selectAdminCodeCountInfo(parameterMap);
    	resultMap.put("itemCodeCnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = masterAdminCodeDAO.selectAdminCodeInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("itemCodeList", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("itemCodeList", list);
    	}
   	
		return resultMap;	
	}
	
	@Override
	public Map selectAdminCode(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
    	
    	List list = masterAdminCodeDAO.selectAdminCode(parameterMap);
   		resultMap.put("itemCodeList", list);	

        return resultMap;	
	}

	@Override
	public void insertAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectAdminCodeCountInfo(parameterMap);
		if (cnt > 0) {
			throw new Exception("중복된 기준 공통 코드가 존재 합니다.");
		} else { 
			this.masterAdminCodeDAO.insertAdminCodeInfo(parameterMap);
			parameterMap.put("eai_if_dml", "I");
			this.masterAdminCodeDAO.insertAdminCodeInfoIF(parameterMap);

		}
		
		String codeKey = (String)parameterMap.get("adcd_hdkey");
		String codeName = (String)parameterMap.get("adcd_hdname");

		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_hdname", codeName);
			adminCodeIcomDAO.mergeTOrderAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_hdname", codeName);
			adminCodeTmsDAO.mergeTTAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			Map noParam = new HashMap();
			List ctList = centerDAO.selectExcelDownCenter(noParam);//for no paging result
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_hdname", codeName);
			String ctKey = "";
			for(int i=0; i<ctList.size(); i++){
				Map ctMap = (Map)ctList.get(i);
				ctKey = (String)ctMap.get("ctkey");
				
				parameterMap.put("ctkey", ctKey);
				
				adminCodeWmsDAO.mergeTWorkAdminCodeInfo(parameterMap);
			}
		}
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePortalDAO.mergeTPortalAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_hdname", codeName);
			adminCodeVimsDAO.mergeTViewAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePmsDAO.mergeTPmsAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_hdname", codeName);
			adminCodeBbsDAO.mergeTBbsAdminCodeInfo(parameterMap);
		}
		*/
	}
	

	@Override
	public void updateAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectAdminCodeCountInfo(parameterMap);
		if (cnt <= 0) {
			throw new Exception("기준 공통 코드가 존재하지 않습니다.");
		} else { 
			this.masterAdminCodeDAO.updateAdminCodeInfo(parameterMap);
			parameterMap.put("eai_if_dml", "U");
			this.masterAdminCodeDAO.insertAdminCodeInfoIF(parameterMap);
		}
		
		String codeKey = (String)parameterMap.get("adcd_hdkey");
		String codeName = (String)parameterMap.get("adcd_hdname");

		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_hdname", codeName);
			adminCodeIcomDAO.mergeTOrderAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_hdname", codeName);
			adminCodeTmsDAO.mergeTTAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			Map noParam = new HashMap();
			List ctList = centerDAO.selectExcelDownCenter(noParam);//for no paging result
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_hdname", codeName);

			String ctKey = "";
			for(int i=0; i<ctList.size(); i++){
				Map ctMap = (Map)ctList.get(i);
				ctKey = (String)ctMap.get("ctkey");
				
				parameterMap.put("ctkey", ctKey);
				
				//adminCodeWmsDAO.mergeTWorkAdminCodeInfo(parameterMap);
				adminCodeWmsDAO.mergeTWorkAdminCodeInfo(parameterMap);

			}
		}
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePortalDAO.mergeTPortalAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_hdname", codeName);
			adminCodeVimsDAO.mergeTViewAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePmsDAO.mergeTPmsAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_hdname", codeName);
			adminCodeBbsDAO.mergeTBbsAdminCodeInfo(parameterMap);
		}
		*/
		
	}

	
	@Override
	public void deleteAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectAdminCodeCountInfo(parameterMap);
		if (cnt <= 0) {
			throw new MsgException("기준 공통 코드가 존재하지 않습니다.");
		} else { 
			this.masterAdminCodeDAO.deleteAdminCodeInfo(parameterMap);
			parameterMap.put("eai_if_dml", "D");
			this.masterAdminCodeDAO.insertAdminCodeInfoIF(parameterMap);
		}
		
		String codeKey = (String)parameterMap.get("adcd_hdkey");
		String codeName = (String)parameterMap.get("adcd_hdname");

		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_hdname", codeName);
			adminCodeIcomDAO.deleteTOrderAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_hdname", codeName);
			adminCodeTmsDAO.deleteTTAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
//					데이터만 가져오고 처리하지 않음.			
//					Map noParam = new HashMap();
//					List ctList = centerDAO.selectExcelDownCenter(noParam);//for no paging result
			
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_hdname", codeName);
			adminCodeWmsDAO.deleteTWorkAdminCodeInfo(parameterMap);
		}
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePortalDAO.deleteTPortalAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_hdname", codeName);
			adminCodeVimsDAO.deleteTViewAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			adminCodePmsDAO.deleteTPmsAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_hdname", codeName);
			adminCodeBbsDAO.deleteTBbsAdminCodeInfo(parameterMap);
		}
		*/
		
	}

	
	@Override
	public List selectAdminCodeExcelDown(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public Map selectDetailAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = masterAdminCodeDAO.selectDetailAdminCodeCountInfo(parameterMap);
    	resultMap.put("itemCodeCnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("itemCodeList", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("itemCodeList", list);
    	}
   	
		return resultMap;
	}
	

	@Override
	public void insertDetailAdminCodeInfo(Map parameterMap, Map amsHDCodeMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectDetailAdminCodeCountInfo(parameterMap);
		if (cnt > 0) {
			throw new Exception("중복된 상세 기준공통코드가 존재 합니다.");
		} else { 
			this.masterAdminCodeDAO.insertDetailAdminCodeInfo(parameterMap);
			parameterMap.put("eai_if_dml", "I");
			this.masterAdminCodeDAO.insertDetailAdminCodeInfoIF(parameterMap);

		}
		
		String codeKey 	 = (String)parameterMap.get("adcd_hdkey");	// HD Code Key
		String codeName	 = (String)amsHDCodeMap.get("adcd_hdname");	// HD Code Name
		String detailKey = (String)parameterMap.get("adcd_dtkey");	// DT Code Key
		String dtOrder 	 = (String)parameterMap.get("adcd_dtorder");// DT Code Name
		String dtName	 = (String)parameterMap.get("adcd_dtname"); // DT Code Name
		String dtValue	 = (String)parameterMap.get("adcd_dtvalue");// DT Code Value

		String lakey     = (String)parameterMap.get("lakey");// lakey 
		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_hdname", codeName);
			parameterMap.put("orcd_dtkey", detailKey);
			parameterMap.put("orcd_dtorder", dtOrder);
			parameterMap.put("orcd_dtname", dtName);
			parameterMap.put("orcd_dtvalue", dtValue);
			
			int omsHCnt = adminCodeIcomDAO.selectTOrderAdminCodeCountInfo(parameterMap);
			if(omsHCnt == 0){
				adminCodeIcomDAO.insertTOrderAdminCodeInfo(parameterMap);
			}
			
			adminCodeIcomDAO.deleteByHDKeyTOrderDetailAdminCodeInfo(parameterMap);
			adminCodeIcomDAO.mergeTOrderAdminCodeInfo(parameterMap);// HD 정보를 AMS 정보 기준으로 Update
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if( amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodeIcomDAO.insertTOrderDetailAdminCodeInfo(rowMap);	
				}
				
			}
			parameterMap.put("lakey", lakey);
			adminCodeIcomDAO.mergeTOrderDetailAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_hdname", codeName);
			parameterMap.put("tmcd_dtkey", detailKey);
			parameterMap.put("tmcd_dtorder", dtOrder);
			parameterMap.put("tmcd_dtname", dtName);
			parameterMap.put("tmcd_dtvalue", dtValue);
			
			int tmsHCnt = adminCodeTmsDAO.selectTTAdminCodeCountInfo(parameterMap);
			if(tmsHCnt == 0){
				adminCodeTmsDAO.insertTTAdminCodeInfo(parameterMap);
			}
			
			adminCodeTmsDAO.deleteByHDKeyTTDetailAdminCodeInfo(parameterMap);
			adminCodeTmsDAO.mergeTTAdminCodeInfo(parameterMap);
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if( amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodeTmsDAO.insertTTDetailAdminCodeInfo(rowMap);	
				}
			}
			parameterMap.put("lakey", lakey);
			adminCodeTmsDAO.mergeTTDetailAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			Map noParam = new HashMap();
			List ctList = centerDAO.selectExcelDownCenter(noParam);//for no paging result
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_hdname", codeName);
			parameterMap.put("ctcd_dtkey", detailKey);
			parameterMap.put("ctcd_dtorder", dtOrder);
			parameterMap.put("ctcd_dtname", dtName);
			parameterMap.put("ctcd_dtvalue", dtValue);
			String ctKey = "";

			adminCodeWmsDAO.deleteByHDKeyTWorkDetailAdminCodeInfo(parameterMap);
			
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			parameterMap.put("lakey", lakey);
			if(amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					
					for(int j=0; j<ctList.size(); j++){
						Map ctMap = (Map)ctList.get(j);
						ctKey = (String)ctMap.get("ctkey");
						
						parameterMap.put("ctkey", ctKey);
						rowMap.put("ctkey", ctKey);
						adminCodeWmsDAO.mergeTWorkAdminCodeInfo(parameterMap);
						adminCodeWmsDAO.insertTWorkDetailAdminCodeInfo(rowMap);	
						
					}
				}
				
			}				
		}
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			parameterMap.put("ptcd_dtkey", detailKey);
			parameterMap.put("ptcd_dtorder", dtOrder);
			parameterMap.put("ptcd_dtname", dtName);
			parameterMap.put("ptcd_dtvalue", dtValue);
			
			int portalHCnt = adminCodePortalDAO.selectTPortalAdminCodeCountInfo(parameterMap);
			if(portalHCnt == 0){
				adminCodePortalDAO.insertTPortalAdminCodeInfo(parameterMap);
			}
			
			adminCodePortalDAO.deleteByHDKeyTPortalDetailAdminCodeInfo(parameterMap);
			adminCodePortalDAO.mergeTPortalAdminCodeInfo(parameterMap);
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if(amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodePortalDAO.insertTPortalDetailAdminCodeInfo(rowMap);	
				}
			}
			parameterMap.put("lakey", lakey);
			adminCodePortalDAO.mergeTPortalDetailAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_hdname", codeName);
			parameterMap.put("vicd_dtkey", detailKey);
			parameterMap.put("vicd_dtorder", dtOrder);
			parameterMap.put("vicd_dtname", dtName);
			parameterMap.put("vicd_dtvalue", dtValue);
			
			int vimsHCnt = adminCodeVimsDAO.selectTViewAdminCodeCountInfo(parameterMap);
			if(vimsHCnt == 0){
				adminCodeVimsDAO.insertTViewAdminCodeInfo(parameterMap);
			}
			
			adminCodeVimsDAO.deleteByHDKeyTViewDetailAdminCodeInfo(parameterMap);
			adminCodeVimsDAO.mergeTViewAdminCodeInfo(parameterMap);
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if( amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodeVimsDAO.insertTViewDetailAdminCodeInfo(rowMap);	
				}
			}
			parameterMap.put("lakey", lakey);
			adminCodeVimsDAO.mergeTViewDetailAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_hdname", codeName);
			parameterMap.put("ptcd_dtkey", detailKey);
			parameterMap.put("ptcd_dtorder", dtOrder);
			parameterMap.put("ptcd_dtname", dtName);
			parameterMap.put("ptcd_dtvalue", dtValue);
			
			int pmsHCnt = adminCodePmsDAO.selectTPmsAdminCodeCountInfo(parameterMap);
			if(pmsHCnt == 0){
				adminCodePmsDAO.insertTPmsAdminCodeInfo(parameterMap);
			}
			
			adminCodePmsDAO.deleteByHDKeyTPmsDetailAdminCodeInfo(parameterMap);
			adminCodePmsDAO.mergeTPmsAdminCodeInfo(parameterMap);
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if(amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodePmsDAO.insertTPmsDetailAdminCodeInfo(rowMap);	
				}
			}
			parameterMap.put("lakey", lakey);
			adminCodePmsDAO.mergeTPmsDetailAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_hdname", codeName);
			parameterMap.put("bbcd_dtkey", detailKey);
			parameterMap.put("bbcd_dtorder", dtOrder);
			parameterMap.put("bbcd_dtname", dtName);
			parameterMap.put("bbcd_dtvalue", dtValue);
			
			int bbsHCnt = adminCodeBbsDAO.selectTBbsAdminCodeCountInfo(parameterMap);
			if(bbsHCnt == 0){
				adminCodeBbsDAO.insertTBbsAdminCodeInfo(parameterMap);
			}
			
			adminCodeBbsDAO.deleteByHDKeyTBbsDetailAdminCodeInfo(parameterMap);
			adminCodeBbsDAO.mergeTBbsAdminCodeInfo(parameterMap);
			parameterMap.remove("adcd_dtkey");
			parameterMap.remove("lakey");
			List amsDtList = masterAdminCodeDAO.selectDetailAdminCodeInfo(parameterMap);//AMS DT 정보로 갱신
			if( amsDtList != null && !amsDtList.isEmpty() ){
				for(int i=0; i<amsDtList.size(); i++){
					Map rowMap = (Map)amsDtList.get(i);
					adminCodeBbsDAO.insertTBbsDetailAdminCodeInfo(rowMap);	
				}
			}
			parameterMap.put("lakey", lakey);
			adminCodeBbsDAO.mergeTBbsDetailAdminCodeInfo(parameterMap);
		}
		*/
	}

	@Override
	public void updateDetailAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectDetailAdminCodeCountInfo(parameterMap);
		if (cnt <= 0) {
			throw new Exception("상세 기준공통코드가 존재하지 않습니다.");
		} else { 
			this.masterAdminCodeDAO.updateDetailAdminCodeInfo(parameterMap);
			parameterMap.put("eai_if_dml", "U");
			this.masterAdminCodeDAO.insertDetailAdminCodeInfoIF(parameterMap);

		}
		
		String codeKey 	 = (String)parameterMap.get("adcd_hdkey");
		String detailKey = (String)parameterMap.get("adcd_dtkey");
		String dtOrder 	 = (String)parameterMap.get("adcd_dtorder");
		String dtName	 = (String)parameterMap.get("adcd_dtname");
		String dtValue	 = (String)parameterMap.get("adcd_dtvalue");

		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_dtkey", detailKey);
			parameterMap.put("orcd_dtorder", dtOrder);
			parameterMap.put("orcd_dtname", dtName);
			parameterMap.put("orcd_dtvalue", dtValue);
			int omsHCnt = adminCodeIcomDAO.selectTOrderAdminCodeCountInfo(parameterMap);
			if(omsHCnt == 0){
				adminCodeIcomDAO.insertTOrderAdminCodeInfo(parameterMap);
			}
			adminCodeIcomDAO.mergeTOrderDetailAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_dtkey", detailKey);
			parameterMap.put("tmcd_dtorder", dtOrder);
			parameterMap.put("tmcd_dtname", dtName);
			parameterMap.put("tmcd_dtvalue", dtValue);
			
			int tmsHCnt = adminCodeTmsDAO.selectTTAdminCodeCountInfo(parameterMap);
			if(tmsHCnt == 0){
				adminCodeTmsDAO.insertTTAdminCodeInfo(parameterMap);
			}
			
			adminCodeTmsDAO.mergeTTDetailAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			Map noParam = new HashMap();
			List ctList = centerDAO.selectExcelDownCenter(noParam);//for no paging result
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_dtkey", detailKey);
			parameterMap.put("ctcd_dtorder", dtOrder);
			parameterMap.put("ctcd_dtname", dtName);
			parameterMap.put("ctcd_dtvalue", dtValue);
			String ctKey = "";
			for(int i=0; i<ctList.size(); i++){
				Map ctMap = (Map)ctList.get(i);
				ctKey = (String)ctMap.get("ctkey");
				
				parameterMap.put("ctkey", ctKey);
				adminCodeWmsDAO.mergeTWorkAdminCodeInfo(parameterMap);
				adminCodeWmsDAO.mergeTWorkDetailAdminCodeInfo(parameterMap);
			}
		}		
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_dtkey", detailKey);
			parameterMap.put("ptcd_dtorder", dtOrder);
			parameterMap.put("ptcd_dtname", dtName);
			parameterMap.put("ptcd_dtvalue", dtValue);
			
			int portalHCnt = adminCodePortalDAO.selectTPortalAdminCodeCountInfo(parameterMap);
			if(portalHCnt == 0){
				adminCodePortalDAO.insertTPortalAdminCodeInfo(parameterMap);
			}
			
			adminCodePortalDAO.mergeTPortalDetailAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_dtkey", detailKey);
			parameterMap.put("vicd_dtorder", dtOrder);
			parameterMap.put("vicd_dtname", dtName);
			parameterMap.put("vicd_dtvalue", dtValue);
			
			int vimsHCnt = adminCodeVimsDAO.selectTViewAdminCodeCountInfo(parameterMap);
			if(vimsHCnt == 0){
				adminCodeVimsDAO.insertTViewAdminCodeInfo(parameterMap);
			}
			
			adminCodeVimsDAO.mergeTViewDetailAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_dtkey", detailKey);
			parameterMap.put("ptcd_dtorder", dtOrder);
			parameterMap.put("ptcd_dtname", dtName);
			parameterMap.put("ptcd_dtvalue", dtValue);
			
			int pmsHCnt = adminCodePmsDAO.selectTPmsAdminCodeCountInfo(parameterMap);
			if(pmsHCnt == 0){
				adminCodePmsDAO.insertTPmsAdminCodeInfo(parameterMap);
			}
			
			adminCodePmsDAO.mergeTPmsDetailAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_dtkey", detailKey);
			parameterMap.put("bbcd_dtorder", dtOrder);
			parameterMap.put("bbcd_dtname", dtName);
			parameterMap.put("bbcd_dtvalue", dtValue);
			
			int bbsHCnt = adminCodeBbsDAO.selectTBbsAdminCodeCountInfo(parameterMap);
			if(bbsHCnt == 0){
				adminCodeBbsDAO.insertTBbsAdminCodeInfo(parameterMap);
			}
			adminCodeBbsDAO.mergeTBbsDetailAdminCodeInfo(parameterMap);
		}
		*/
	}

	@Override
	public void deleteDetailAdminCodeInfo(Map parameterMap, boolean isRootDelete) throws Exception {
		// TODO Auto-generated method stub
		int cnt = masterAdminCodeDAO.selectDetailAdminCodeCountInfo(parameterMap);
		if( !isRootDelete ){
			if (cnt <= 0) {
				throw new Exception("상세 기준공통코드가 존재하지 않습니다.");
			} else { 
				this.masterAdminCodeDAO.deleteDetailAdminCodeInfo(parameterMap);
				parameterMap.put("eai_if_dml", "D");
				this.masterAdminCodeDAO.insertDetailAdminCodeInfoIF(parameterMap);
			}
		}
		else{
			if (cnt >= 0) {
				this.masterAdminCodeDAO.deleteDetailAdminCodeInfo(parameterMap);
				parameterMap.put("eai_if_dml", "D");
				this.masterAdminCodeDAO.insertDetailAdminCodeInfoIF(parameterMap);
			}
		}
		String codeKey 	 = (String)parameterMap.get("adcd_hdkey");
		String detailKey = (String)parameterMap.get("adcd_dtkey");
		// OMS
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			parameterMap.put("orcd_hdkey", codeKey);
			parameterMap.put("orcd_dtkey", detailKey);
			adminCodeIcomDAO.deleteTOrderDetailAdminCodeInfo(parameterMap);
		}
		*/
		// TMS
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			parameterMap.put("tmcd_hdkey", codeKey);
			parameterMap.put("tmcd_dtkey", detailKey);
			adminCodeTmsDAO.deleteTTDetailAdminCodeInfo(parameterMap);
		}
		// WMS
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			parameterMap.put("ctcd_hdkey", codeKey);
			parameterMap.put("ctcd_dtkey", detailKey);
			adminCodeWmsDAO.deleteTWorkDetailAdminCodeInfo(parameterMap);
		}
		/* 이하 코드 테이블은 정지 2016-01-25 KJM
		// PORTAL
		if( "YES".equals(commonService.getAdmConfigVal("USEPORTAL")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_dtkey", detailKey);
			adminCodePortalDAO.deleteTPortalDetailAdminCodeInfo(parameterMap);
		}
		// VIMS
		if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			parameterMap.put("vicd_hdkey", codeKey);
			parameterMap.put("vicd_dtkey", detailKey);
			adminCodeVimsDAO.deleteTViewDetailAdminCodeInfo(parameterMap);
		}
		// PMS
		if( "YES".equals(commonService.getAdmConfigVal("USEPMS")) ) {
			parameterMap.put("ptcd_hdkey", codeKey);
			parameterMap.put("ptcd_dtkey", detailKey);
			adminCodePmsDAO.deleteTPmsDetailAdminCodeInfo(parameterMap);
		}
		// BBS
		if( "YES".equals(commonService.getAdmConfigVal("USEBBS")) ) {
			parameterMap.put("bbcd_hdkey", codeKey);
			parameterMap.put("bbcd_dtkey", detailKey);
			adminCodeBbsDAO.deleteTBbsDetailAdminCodeInfo(parameterMap);
		}
		*/
	}

	@Override
	public int checkAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return masterAdminCodeDAO.selectAdminCodeCountInfo(parameterMap);
	}


	@Override
	public int checkDetailAdminCodeInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return masterAdminCodeDAO.selectDetailAdminCodeCountInfo(parameterMap);
	}


	@Override
	public List selectExcelDownAdminCode(Map parameterMap) throws Exception {
		return masterAdminCodeDAO.selectExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownAdminCode(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return masterAdminCodeDAO.selectExcelDown(parameterMap, excelFormat);
	}


	@Override
	public List selectExcelDownDetailAdminCode(Map parameterMap)
			throws Exception {
		return masterAdminCodeDAO.selectExcelDownDetail(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownDetailAdminCode(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return masterAdminCodeDAO.selectExcelDownDetail(parameterMap, excelFormat);
	}
}
