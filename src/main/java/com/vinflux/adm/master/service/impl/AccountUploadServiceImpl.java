package com.vinflux.adm.master.service.impl;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.master.service.AccountUploadService;
import com.vinflux.adm.persistence.AccountUploadDAO;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.MapUtil;

@Service("accountUploadService")
public class AccountUploadServiceImpl implements AccountUploadService {
	
	@Resource(name="ulhsKeyTadminInfoUlhsIdGenService")
	private IdGenService ulhsKeyTadminInfoUlhsIdGenService;
	
	@Resource(name="accountUploadDAO")
	private AccountUploadDAO accountUploadDAO;
	
//	@Resource(name="centerByRoleDAO")
//	private CenterByRoleDAO centerByRoleDAO;
	
	@Resource(name = "accountService")
	private AccountService accountService;
	
	@Resource(name="mstAcGenService")
	private IdGenService mstAcGenService;
	
	protected static final Log LOG = LogFactory.getLog(AccountUploadServiceImpl.class);
	
//	@Resource(name="txAdmManager")
//	private DataSourceTransactionManager txAdmManager;
	
	private static final String EXCEL_FORMAT_UPLOAD [][] =
	{
		/* 기존 ACUPLOAD FORMAT 주석 - 2017.11.07 : ksh
 		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"ctkey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"ackey" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"actype" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"N" ,"acname" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"countrycode" 		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"N" ,"address1" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"N" ,"address2" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"city" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"state" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"zipcode" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel1" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel2" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"fax" 				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"contact" 			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"ceo" 				,"" }*/
		
		/* 수정 주석
		 * {ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"ctkey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"owkey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"ackey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"Y" ,"delyn"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"Y" ,"asnuseyn"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"16"		,"Y" ,"loggrpcd"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"Y" ,"dckey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"icgrkey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"actype"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"N" ,"acname"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"rep_ackey"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"7"		,"N" ,"rep_actype"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"regno"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"regdate"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"urkey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"businesstype"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"businesscategory"	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"countrycode"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"ceo"					,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"28"		,"N" ,"addressid"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"N" ,"address1"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"N" ,"address2"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"city"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"state"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"N" ,"zipcode"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel1"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel2"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"fax"					,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"email"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"startdate"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"enddate"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"closingdate"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"contact"				,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"N" ,"operstatype"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"6"		,"N" ,"pltmaxrt"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"25.5"		,"N" ,"pltgrrt"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"N" ,"pltaplyyn"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"N" ,"scmcheck"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"scmurkey"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf1"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf2"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf3"            	,"" }*/		
		
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"owkey"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"100"		,"Y" ,"acname"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"Y" ,"actype"				,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"10"		,"Y" ,"zipcode"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"Y" ,"address1"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"200"		,"Y" ,"address2"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"regno"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"businesscategory"	,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"businesstype"		,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"1"		,"N" ,"asnuseyn"			,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"contact"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel1"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"tel2"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"fax"					,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"30"		,"N" ,"ceo"					,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"7"		,"N" ,"rep_actype"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"rep_ackey"			,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"14"		,"N" ,"regdate"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"email"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf1"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf2"				,"" },	
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"udf3"            	,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"50"		,"N" ,"delyn"            	,"" },
		{ExcelUtil.EX_FORM_TYPE_VC2 			,"20"		,"N" ,"ackey"				,"" },
		{ExcelUtil.EX_FORM_TYPE_NUM 			,"28"		,"N" ,"addrid"				,"" }
	};	
	
	@Override
	public Map selectAccountUploadInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = accountUploadDAO.selectAccountUploadCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = accountUploadDAO.selectAccountUpload(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	
	@Override
	public Map selectAccountTemp(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = accountUploadDAO.selectAccountUploadCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = accountUploadDAO.selectAccountTemp(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	public List selectAccountTempAll(Map parameterMap) throws Exception {
		return accountUploadDAO.selectAccountTempAll(parameterMap);
	}
	
	@Override
	public void updateTadminTempAcup(Map parameterMap) throws Exception {
		accountUploadDAO.updateTadminTempAcup(parameterMap);
	}
	
	@Override
	public void saveAccountCode(Map parameterMap, Iterator pItr, Map ctMap) throws Exception {
		String ctKey = MapUtil.getStr(parameterMap, "ctkey");
		String eCtKey = null;
		int txFlag = 0;
		int wmsTxFlag = 0;
		String zipNo = (String) parameterMap.get("zipcode");
		String lnmAdres = (String) parameterMap.get("address1");
		
		parameterMap.put("zipNo", zipNo);
		parameterMap.put("lnmAdres", lnmAdres);
		parameterMap.put("dckey", "1");
		
		
		
		String rep_ackey = (String) parameterMap.get("rep_ackey");
		
		//Center가 Null 일경우 시스템에 등록된 모든 센터로 업로드 한다.
		if("".equals(ctKey) || ctKey == null) {
			txFlag = accountUploadDAO.selectPkCntTadminMstAc(parameterMap);
			
			if(txFlag == 0) {
				accountService.insertAccount(parameterMap);
			}else {
				accountService.updateAccount(parameterMap);
			}

			while(pItr.hasNext()) {
				
				eCtKey = pItr.next().toString();
				parameterMap.put("ctkey", eCtKey);
				
				wmsTxFlag = accountUploadDAO.selectPkCntTadminMstAcxCt(parameterMap);
				if(wmsTxFlag == 0) {
    				accountService.saveAccountCenter(parameterMap);
    			}
			}
			
		}else {
			
			if ( !ctMap.containsKey(ctKey) ) {
				throw new MsgException(10013); //유효하지 않은 센터 코드 입니다.
			}
			
    		txFlag = accountUploadDAO.selectPkCntTadminMstAc(parameterMap);
    		if(txFlag == 0) {
    			accountService.insertAccount(parameterMap);
    			accountService.saveAccountCenter(parameterMap);
    		} else {
    			accountService.updateAccount(parameterMap);
    			wmsTxFlag = accountUploadDAO.selectPkCntTadminMstAcxCt(parameterMap);
    			if(wmsTxFlag == 0) {
    				accountService.saveAccountCenter(parameterMap);
    			}
    		}
		}
		
    	parameterMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_SUCCESS);
		parameterMap.put("ifmessage", "Success");

		this.updateTadminTempAcup(parameterMap);
	}
	
	    
    @Override
	public String uploadAccountExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception {
		// get format key 
		int formaLen = EXCEL_FORMAT_UPLOAD.length;
		String excelFormatKey[] = new String [formaLen];
		for (int i=0 ;formaLen > i; i++) {
			excelFormatKey[i] = EXCEL_FORMAT_UPLOAD[i][ExcelUtil.EX_FORM_KEY];
		}
		
		// get data
		List<Map> excelData = ExcelUtil.excelStreamConverList(excelFis , excelFormatKey);
		
		// validation check 
		ExcelUtil.uploadDataValidactionCheck(excelData , EXCEL_FORMAT_UPLOAD );
		
		//get ulhsKey 
		String ulhsKey = ulhsKeyTadminInfoUlhsIdGenService.getNextStringId();
		
    	SessionVO sessionVo = (SessionVO)parameterMap.get("userinfo");
    	String id = sessionVo.getUrKey();
    	
		// data insert 
		int size = excelData.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = excelData.get(i);
			String ackey = (String) row.get("ackey");
			String owkey = (String) row.get("owkey");
			
			if("".equals(ackey) && ackey.isEmpty()){
				ackey = mstAcGenService.getNextStringId();	
				row.put("ackey", owkey+ackey);
			}
			
			
			// insert param add TORDER_TEMP_STOCK
			row.put("ifstatus", EnvConstant.INTF_STATUS_CODE_STANDBY);
			row.put("ifmessage", "");
			row.put("ulhskey", ulhsKey);   	
	    	row.put("sessionid", id);
	    	row.put("seq", i+1);
	    	row.put("dckey", "1");

			//ULHSKEY
			accountUploadDAO.insertTadminTempAcup(row);
		}
		
		Map insertTadminInfoUlhsMap = new HashMap();
		insertTadminInfoUlhsMap.put("ulhskey", ulhsKey);
		insertTadminInfoUlhsMap.put("ulhstype", EnvConstant.ULHS_TYPE_MASTER);
		insertTadminInfoUlhsMap.put("uploadfilename", fileName);
		
		// insert TORDER_INFO_ULHS
		accountUploadDAO.insertTadminInfoUlhs(insertTadminInfoUlhsMap);
		
		return ulhsKey;
    }

	@Override
	public List selectExcelDownAccountUpload(Map parameterMap) throws Exception {
		return accountUploadDAO.selectExcelDownAccountUpload(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownAccountUpload(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return accountUploadDAO.selectExcelDownAccountUpload(parameterMap,excelFormat);
	}
}