package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.StorexctService;
import com.vinflux.adm.persistence.StorexctDAO;

@Service("storexctService")
public class StorexctServiceImpl implements StorexctService {
	
	@Resource(name="storexctDAO")
	private StorexctDAO storexctDAO;
	
	//@Resource(name="storexctIcomDAO")
	//private StorexctIcomDAO storexctIcomDAO;
	
	//@Resource(name="storexctWmsDAO")
	//private StorexctWmsDAO storexctWmsDAO;
	
	// 모듈별 전송여부 판별을 위함
//	@Resource(name="commonService")
//	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectStorexctInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = storexctDAO.selectStorexctCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = storexctDAO.selectStorexct(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectStorexct (Map parameterMap) throws Exception {
		return storexctDAO.selectPkStorexct(parameterMap);
	}
	
	@Override
	public int checkStorexctCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return storexctDAO.checkStorexctCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.StorexctService#insertStorexct(java.util.Map)
	 */
	@Override
	public void insertStorexct (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.storexctDAO.insertTAdminMstStorexct(parameterMap);
		
		// ICOM INSERT
		/*
		 * 내부 처리로직이 주석처리되어 우선 전체 주석처리 [ sw.yoo - 2015.12.23 ]
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.storexctIcomDAO.insertTOrderMstStorexct(parameterMap);	
		}
		*/
		
		// WMS INSERT
		/*
		 * 내부 처리로직이 주석처리되어 우선 전체 주석처리 [ sw.yoo - 2015.12.23 ]
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			
			List list = storexctDAO.selectCtkey(parameterMap);
			*/		
			/*
        	if (list != null && !list.isEmpty()) { 
        		for(int i=0;i<list.size();i++){
        			Map ctKeyMap = (Map) list.get(i);
        			parameterMap.remove("ctkey");
        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
        			this.storexctWmsDAO.insertTWorkMstStorexct(parameterMap);
        		}
        	}
		}
		*/
	}
	
	/** **/
	@Override
	public void updateStorexct (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.storexctDAO.updateTAdminMstStorexct(parameterMap);
		
//		int count = 0;
		
		// ICOM INSERT
		/*
		 * 처리 로직이 없으므로 우선 주석처리 [ sw.yoo - 2015.12.23 ]
		 * 
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//count = this.storexctIcomDAO.updateTOrderMstStorexct(parameterMap);
			if(count==0){
				//this.storexctIcomDAO.updateTOrderMstStorexct(parameterMap);
			}
		}
		*/
		// WMS UPDATE 
		/*
		 * 처리 로직이 주석처리되어 우선 전체 주석처리 [ sw.yoo - 2015.12.23 ]
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//count = this.storexctWmsDAO.updateTWorkMstStorexct(parameterMap);
			if(count==0){
				//this.storexctWmsDAO.insertTWorkMstStorexct(parameterMap);
			}
		}
		*/
	}

	/** **/
	@Override
	public void deleteStorexct (Map parameterMap) throws Exception {
		// ICOM DELETE
		/*
		 * 내부 처리로직이 주석처리되어 우선 전체 주석처리 [ sw.yoo - 2015.12.23 ] 
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.storexctIcomDAO.deleteTOrderMstStorexct(parameterMap);
		}
		*/

		// WMS DELETE
		/*
		 * 내부 처리로직이 주석처리되어 우선 전체 주석처리 [ sw.yoo - 2015.12.23 ]
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//this.storexctWmsDAO.deleteTWorkMstStorexct(parameterMap);
		}
		*/
		
		// ADMIN DELETE
		this.storexctDAO.deleteTAdminMstStorexct(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownStorexct (Map parameterMap) throws Exception {
    	return storexctDAO.selectExcelDownStorexct(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownStorexct(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return storexctDAO.selectExcelDownStorexct(parameterMap, excelFormat);
	}
}