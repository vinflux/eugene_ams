package com.vinflux.adm.master.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;


public interface MessageUploadService {
	/**
     * 메시지 업로드 임시 테이블 조회
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMessageUploadInfo (Map parameterMap) throws Exception;
	
	/**
     * 메시지 업로드 항목 조회 with paging
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMessageTemp(Map parameterMap) throws Exception;
	
	/**
     * 메시지 업로드 항목 조회 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMessageTempAll(Map parameterMap) throws Exception;
	
	/**
     * 메시지 업로드 엑셀 업로드
     * @param Map parameterMap
     * @return String
     * @throws Exception
     */
	public String uploadMessageExcel (Map parameterMap, InputStream excelFis , String fileName) throws Exception;
	
	/**
     * 메시지  임시 테이블 엑셀 다운로드
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	public List selectExcelDownMessageUpload (Map parameterMap) throws Exception;

	public Workbook selectExcelDownMessageUpload(Map parameterMap,
			List<Map> excelFormat)throws Exception;
		
}
