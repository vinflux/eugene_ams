package com.vinflux.adm.master.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.AccountService;
import com.vinflux.adm.master.service.PushService;
import com.vinflux.adm.persistence.PushDAO;
import com.vinflux.adm.push.service.MqttService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.push.service.MobileMessageService;
import com.vinflux.framework.exception.MsgException;


@Service("pushService")
public class PushServiceImpl implements PushService {
	
	@Resource(name="pushDAO")
	private PushDAO pushDAO;

	@Resource(name="mqttService")
	private MqttService mqttService;
	
	@Resource(name="mobileMessageService")
	private MobileMessageService mobileMessageService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountService.class);

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AccountService#selectAccountInfo(java.util.Map)
	 */
	@Override
	public Map selectPushInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = pushDAO.selectPushInfoCount(parameterMap);
    	resultMap.put("count", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = pushDAO.selectPushInfo(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;

	}
	
	@Override
	public Map selectDetailPushInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		List list = pushDAO.selectDetailPushInfo(parameterMap);
		if (list != null && !list.isEmpty()) {
			resultMap.put("list", list);
		}
		else {
			resultMap.put("list", new ArrayList());
		}

		return resultMap;
	}
	
	@Override
	public int deletePush(Map parameterMap) throws Exception {
		int cnt = pushDAO.checkPush(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(30002);
		}
		else {
			this.pushDAO.deletePush(parameterMap);
		}
		return cnt;
	}
	
	@Override
	public int deletePushDtl(Map parameterMap, boolean isRootDelete) throws Exception {
		// TODO Auto-generated method stub
		int cnt = pushDAO.checkPush(parameterMap);
		if ( !isRootDelete ) {
			if (cnt <= 0) {
				throw new MsgException(30002);
			}
			else {
				this.pushDAO.deletePushDtl(parameterMap);
			}
		}
		else {
			if (cnt >= 0) {
				this.pushDAO.deletePushDtl(parameterMap);
			}
		}
		return cnt;
	}
	
	@Override
	public void insertPushInfo(Map parameterMap) throws Exception {
		this.pushDAO.insertPushInfo(parameterMap);	
	}
	
	@Override
	public void updatePushInfo(Map parameterMap) throws Exception {
		
		this.pushDAO.updatePushInfo(parameterMap);	
		
	}
	
	@Override
	public List selectUserListByUserGroup(Map parameterMap) throws Exception {
		return pushDAO.selectUserListByUserGroup(parameterMap);
	}
	
	@Override
	public int insertPush(Map parameterMap) throws Exception {
		//String pushid = (String) parameterMap.get("pushid");
		String pushid = pushDAO.selectUser(parameterMap);
		String title = (String) parameterMap.get("title");
		String message = (String) parameterMap.get("pushmsg");
		int retval = pushDAO.insertPush(parameterMap);
		
		LOG.debug("insertPush pushid => " + pushid);
		LOG.debug("insertPush title => " + title);
		LOG.debug("insertPush message => " + message);
		this.mqttService.sendMsg(pushid, createMsgJson(title, message));
		return retval;
	}

	private String createMsgJson(String title, String message) {
		CommandMap commandMap = new CommandMap();
		Map messageData = new HashMap();
		messageData.put("title", title);
		messageData.put("message", message);
		commandMap.putMap("MSG_DATA", messageData);
		return JSONObject.toJSONString(commandMap);
	}
	
	public int updatePushHd(Map parameterMap) throws Exception {
		return pushDAO.updatePushHd(parameterMap);
	}
	
	public int updatePush(Map parameterMap) throws Exception {
		int retval = pushDAO.updatePushStatus(parameterMap);
//		String pushid = (String) parameterMap.get("pushid");
		String pushid = pushDAO.selectUser(parameterMap);
		
		String title = (String) parameterMap.get("title");
		String message = (String) parameterMap.get("pushmsg");
		LOG.debug("updatePush pushid => " + pushid);
		LOG.debug("updatePush title => " + title);
		LOG.debug("updatePush message => " + message);
		this.mqttService.sendMsg(pushid, createMsgJson(title, message));
		return retval;
	}
	
}







