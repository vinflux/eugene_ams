package com.vinflux.adm.master.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface IcxctService {
	
	/** **/
	public Map selectIcxctInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectIcxct (Map parameterMap) throws Exception;
	
	/** **/
	public int checkIcxctCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertIcxct (Map parameterMap) throws Exception;
	
	/** **/
	public void updateIcxct (Map parameterMap) throws Exception;
	
	/** **/
	public void updateAllIcxct (Map parameterMap) throws Exception;	
	
	/** **/
	public void updateChkIcxct (HashMap parameterMap) throws Exception;	
	
	/** **/
	public void deleteIcxct (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownIcxct (Map parameterMap) throws Exception;

	public Workbook selectExcelDownIcxct(Map parameterMap, List<Map> excelFormat)throws Exception;
	
}
