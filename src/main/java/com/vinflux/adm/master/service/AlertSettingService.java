package com.vinflux.adm.master.service;

import java.util.Map;

public interface AlertSettingService {

	//검색조건 리스트

	public Map selectAlertSettingHdList(Map paramMap) throws Exception;

	public Map selectAlertSettingHd(Map paramMap) throws Exception;

	public void insertAlertSettingHd(Map map) throws Exception;

	public void updateAlertSettingHd(Map map) throws Exception;

	public void deleteAlertSettingHd(Map row) throws Exception;

	public int checkAlertSetting(Map param) throws Exception;
	
	public Map selectEventid(Map map) throws Exception;
	
	public Map selectCenterList(Map map) throws Exception; 
	
}