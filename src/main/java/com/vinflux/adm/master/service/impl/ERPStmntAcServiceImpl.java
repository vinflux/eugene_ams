package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.ERPStmntAcService;
import com.vinflux.adm.persistence.ERPStmntAcDAO;


@Service("ERPStmntAcService")
public class ERPStmntAcServiceImpl implements ERPStmntAcService {

	@Resource(name="ERPStmntAcDAO")
	private ERPStmntAcDAO ERPStmntAcDAO;

	@Override
	public Map selectERPStmntAc(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = ERPStmntAcDAO.selectERPStmntAcCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		if(count > 0 ){
			List list = ERPStmntAcDAO.selectERPStmntAcList(parameterMap);
			if( list != null && !list.isEmpty()){
				resultMap.put("list", list);
			}
		}else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}
	
	public List selectExcelERPStmntAc(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return ERPStmntAcDAO.selectExcelERPStmntAc(parameterMap);
	}
	
	public Workbook selectExcelERPStmntAc(Map parameterMap, List<Map> excelFormat) {
		return ERPStmntAcDAO.selectExcelERPStmntAc(parameterMap, excelFormat);
	}

	@Override
	public List selectERPStmntAcDt(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return ERPStmntAcDAO.selectERPStmntAcDt(parameterMap);
	}

	
	
}
