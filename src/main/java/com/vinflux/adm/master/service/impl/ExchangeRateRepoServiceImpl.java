package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.service.ExchangeRateRepoService;
import com.vinflux.adm.persistence.ExchangeRateRepocDAO;


@Service("exchangeRateRepoService")
public class ExchangeRateRepoServiceImpl implements ExchangeRateRepoService {

	@Resource(name="exchangeRateRepocDAO")
	private ExchangeRateRepocDAO exchangeRateRepocDAO;


	public Map selectExchangeRateRepoList(Map paramMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = exchangeRateRepocDAO.selectExchangeRateRepoCount(paramMap);
		resultMap.put("cnt", Integer.toString(count));
		if(count > 0 ){
			List list = exchangeRateRepocDAO.selectExchangeRateRepoList(paramMap);
			if( list != null && !list.isEmpty()){
				resultMap.put("list", list);
			}
		}else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;
	}
	
	public List selectExcelDownUser(Map parameterMap) throws Exception {
		return exchangeRateRepocDAO.selectExcelDown(parameterMap);
	}
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return exchangeRateRepocDAO.selectExcelDown(parameterMap, excelFormat);
	}
	
}
