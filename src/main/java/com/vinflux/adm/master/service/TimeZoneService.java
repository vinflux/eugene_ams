package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface TimeZoneService {

	/**
	 * 표준 시간대 코드 헤더 정보 조회
	 * @param Map parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectTimezoneHd(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 상세 정보 조회
	 * @param Map parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectTimezoneDtl(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 정보 중복 확인
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int checkTimezoneInfoHd(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 정보 중복 확인
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int checkTimezoneInfoDtl(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 헤더 정보 생성
	 * @param Map parameterMap
	 * @return void
	 * @throws Exception
	 */
	public void insertTimezoneInfoHd(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 상세 정보 생성
	 * @param Map parameterMap
	 * @return void
	 * @throws Exception
	 */
	public void insertTimezoneInfoDtl(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 헤더 정보 갱신
	 * @param Map parameterMap
	 * @return void
	 * @throws Exception
	 */
	public void updateTimezoneInfoHd(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 상세 정보 갱신
	 * @param Map parameterMap
	 * @return void
	 * @throws Exception
	 */
	public void updateTimezoneInfoDtl(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 헤더 정보 삭제
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTimezoneInfoHd(Map parameterMap) throws Exception;

	/**
	 * 표준 시간대 코드 상세 정보 삭제
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int deleteTimezoneInfoDtl(Map parameterMap, boolean isRootDelete) throws Exception;

	/**
	 * 표준시간대 상단 엑셀 다운 
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public List selectTimzoneHdExcel(Map parameterMap) throws Exception;

	public Workbook selectTimzoneHdExcel(Map parameterMap, List<Map> excelFormat)throws Exception;
	
	/**
	 * 표준시간대 하단 엑셀 다운 
	 * @param Map parameterMap
	 * @return int
	 * @throws Exception
	 */
	public List selectTimzoneDtlExcel(Map parameterMap) throws Exception;

	public Workbook selectTimzoneDtlExcel(Map parameterMap,
			List<Map> excelFormat)throws Exception;

}
