package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

public interface StoreTrans {
	/**
     * 거래처 코드 저장
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	public Map saveStoreUpload(Map parameterMap ,  List parameterList) throws Exception;
}
