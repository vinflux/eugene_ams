package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.master.service.OwnerService;
import com.vinflux.adm.persistence.OwnerByRoleDAO;
import com.vinflux.adm.persistence.OwnerDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.tms.persistence.OwnerTmsDAO;
import com.vinflux.vims.persistence.OwnerVimsDAO;
import com.vinflux.wms.persistence.OwnerWmsDAO;

@Service("ownerService")
public class OwnerServiceImpl implements OwnerService {
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(OwnerServiceImpl.class);

	
	@Resource(name="ownerDAO")
	private OwnerDAO ownerDAO;
	
	@Resource(name="ownerWmsDAO")
	private OwnerWmsDAO ownerWmsDAO;
	
//	@Resource(name="ownerIcomDAO")
//	private OwnerIcomDAO ownerIcomDAO;	

	@Resource(name="ownerTmsDAO")
	private OwnerTmsDAO ownerTmsDAO;
	
	@Resource(name="ownerVimsDAO")
	private OwnerVimsDAO ownerVimsDAO;
	
	@Resource(name="ownerByRoleDAO")
	private OwnerByRoleDAO ownerByRoleDAO;
	
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	
	@Resource(name="txAdmManager")
	private DataSourceTransactionManager txAdmManager;
	
	@Resource(name = "entityAddrService")
	private EntityAddrService entityAddrService;
	

	/** **/
	@Override
	public Map selectOwnerInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = ownerDAO.selectOwnerCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = ownerDAO.selectOwner(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public Map selectOwner (Map parameterMap) throws Exception {
		return ownerDAO.selectPkOwner(parameterMap);
	}
	
	@Override
	public int checkOwnerCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return ownerDAO.checkOwnerCount(parameterMap);
	}
	

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.OwnerService#insertOwner(java.util.Map)
	 */
	@Override
	public void insertOwner (Map parameterMap) throws Exception {
		
	/*	DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);*/
		
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		
		if(!"".equals(parameterMap.get("zipNo"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
		

			// ADMIN INSERT
			this.ownerDAO.insertTAdminMstOw(parameterMap);
			
			// WMS INSERT
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				this.ownerWmsDAO.insertTWorkMstOw(parameterMap);
				entityAddrService.addAddressEntitymasterWms(parameterMap);
			}
			
			/* 10.21 OMS 삭제작업
			// ICOM INSERT
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				this.ownerIcomDAO.insertTOrderMstOw(parameterMap);	
				entityAddrService.addAddressEntitymasterIcom(parameterMap);
			}*/
			
			// TMS INSERT
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				this.ownerTmsDAO.insertTOrderMstOw(parameterMap);
				entityAddrService.addAddressEntitymasterTms(parameterMap);
			}
			
			// VIMS INSERT
			//if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			//	this.ownerVimsDAO.insertTViewMstOw(parameterMap);
			//	entityAddrService.addAddressEntitymasterVims(parameterMap);
			//}			
			
	}
	
	/** **/
	@Override
	public void updateOwner (Map parameterMap) throws Exception {
		/*DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);
		try { */
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		
			// ADMIN UPDATE
			this.ownerDAO.updateTAdminMstOw(parameterMap);
			entityAddrService.changeAddressEntitymaster(parameterMap);
			
			// WMS UPDATE 
			this.ownerWmsDAO.updateTWorkMstOw(parameterMap);
			entityAddrService.changeAddressEntitymasterWms(parameterMap);
			
			
			// TMS UPDATE
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				this.ownerTmsDAO.updateTOrderMstOw(parameterMap);
				entityAddrService.changeAddressEntitymasterTms(parameterMap);
			}
			
			//int count = 0;
			/*if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				count = this.ownerWmsDAO.updateTWorkMstOw(parameterMap);
				if(count==0){
					this.ownerWmsDAO.insertTWorkMstOw(parameterMap);
					entityAddrService.changeAddressEntitymasterWms(parameterMap);
				}
			}*/
	
			/* 10.21 OMS 삭제작업
			// ICOM UPDATE
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				count = this.ownerIcomDAO.updateTOrderMstOw(parameterMap);
				if(count==0){
					this.ownerIcomDAO.insertTOrderMstOw(parameterMap);
					entityAddrService.changeAddressEntitymasterIcom(parameterMap);
				}
			}*/
	
			// TMS UPDATE
			/*if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				this.ownerTmsDAO.updateTOrderMstOw(parameterMap);
				entityAddrService.changeAddressEntitymasterTms(parameterMap);
			}
			*/
			/*/ VIMS UPDATE
			if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
				count = this.ownerVimsDAO.updateTViewMstOw(parameterMap);
				if(count==0){
					this.ownerVimsDAO.insertTViewMstOw(parameterMap);
					entityAddrService.changeAddressEntitymasterVims(parameterMap);
				}
			}*/
		
	}

	/** **/
	@Override
	public void deleteOwner (Map parameterMap) throws Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus tmsSts = txAdmManager.getTransaction(def);
		try { 		

			// WMS DELETE
			if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
				this.ownerWmsDAO.deleteTWorkMstOw(parameterMap);
				entityAddrService.deleteAddressEntitymasterWms(parameterMap);
			}
	
			/* 10.21 OMS 삭제작업
			// ICOM DELETE
			if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
				this.ownerIcomDAO.deleteTOrderMstOw(parameterMap);	
				entityAddrService.deleteAddressEntitymasterIcom(parameterMap);
			}*/
			
			// TMS DELETE
			if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
				this.ownerTmsDAO.deleteTOrderMstOw(parameterMap);
				entityAddrService.deleteAddressEntitymasterTms(parameterMap);
			}
			
			/*/ VIMS DELETE
			if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
				this.ownerVimsDAO.deleteTViewMstOw(parameterMap);
				entityAddrService.deleteAddressEntitymasterVims(parameterMap);
			}*/
			
			// ADMIN DELETE
			this.ownerByRoleDAO.deleteTAdminMstRoxowgt(parameterMap);
			this.ownerDAO.deleteTAdminMstOw(parameterMap);
			entityAddrService.deleteAddressEntitymaster(parameterMap);
		} catch( MsgException msgEx ){
			LOG.error("### ERROR=>"+msgEx.getLog());
			txAdmManager.rollback(tmsSts);
			throw msgEx;
		}catch (Exception e) {
			LOG.error("", e);
			txAdmManager.rollback(tmsSts);
			throw e;
	
		} finally {
			if(!tmsSts.isCompleted()){
				txAdmManager.commit(tmsSts);
			}
		}			
	}

	/** **/
	@Override
	public List selectExcelDownOwner (Map parameterMap) throws Exception {
    	return ownerDAO.selectExcelDownOwner(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownOwner(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return ownerDAO.selectExcelDownOwner(parameterMap, excelFormat);
	}
}