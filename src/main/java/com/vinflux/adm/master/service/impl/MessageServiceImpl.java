package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.MessageService;
import com.vinflux.adm.persistence.MessageDAO;
import com.vinflux.bbs.persistence.MessageBbsDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.icom.persistence.MessageIcomDAO;
import com.vinflux.kpi.persistence.MessageKpiDAO;
import com.vinflux.pms.persistence.MessagePmsDAO;
import com.vinflux.portal.persistence.MessagePortalDAO;
import com.vinflux.tms.persistence.MessageTmsDAO;
import com.vinflux.vims.persistence.MessageVimsDAO;
import com.vinflux.wms.persistence.MessageWmsDAO;

@Service("messageService")
public class MessageServiceImpl implements MessageService {
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(MessageServiceImpl.class);

	@Resource(name = "messageDAO")
	private MessageDAO messageDAO;

	@Resource(name = "messageWmsDAO")
	private MessageWmsDAO messageWmsDAO;

	@Resource(name = "messageIcomDAO")
	private MessageIcomDAO messageIcomDAO;

	@Resource(name = "messageTmsDAO")
	private MessageTmsDAO messageTmsDAO;
	
	@Resource(name = "messagePortalDAO")
	private MessagePortalDAO messagePortalDAO;

	@Resource(name = "messageVimsDAO")
	private MessageVimsDAO messageVimsDAO;
	
	@Resource(name = "messagePmsDAO")
	private MessagePmsDAO messagePmsDAO;
	
	@Resource(name = "messageBbsDAO")
	private MessageBbsDAO messageBbsDAO;

	@Resource(name = "messageKpiDAO")
	private MessageKpiDAO messageKpiDAO;

	// 모듈별 전송여부 판별을 위함
	@Resource(name = "commonService")
	private CommonService commonService;

	@Override
	public Map selectMessageInfo(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = messageDAO.selectMessageCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		if (count > 0) {
			List list = messageDAO.selectMessage(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		}
		else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}
		return resultMap;

	}

	/** **/
	@Override
	public Map selectMessage(Map parameterMap) throws Exception {
		return messageDAO.selectPkMessage(parameterMap);
	}

	/** **/
	@Override
	public List selectMessageByApKeyList(Map parameterMap) throws Exception {
		return messageDAO.selectMessageByApKeyList(parameterMap);
	}

	/** **/
	@Override
	public void insertMessage(Map parameterMap) throws Exception {

		// INSERT ADMIN 
		Map map = messageDAO.selectTAdminMstMulaapmsgHd(parameterMap);
		if (map == null || map.isEmpty()) {
			messageDAO.insertTAdminMstMulaapmsgHd(parameterMap);
		}

		messageDAO.insertTAdminMstMulaapmsgDt(parameterMap);

		String apkey = MapUtil.getStr(parameterMap, "apkey");
		String msgType = MapUtil.getStr(parameterMap, "msgtype");
		if ("MENU".equals(msgType)) {
			if (!"ADMIN".equals(apkey) ) {
				throw new MsgException(10028);
			}
			return ;
		}
		
		if ("ICOM".equals(apkey)) {
			// INSERT ICOM
			if ("YES".equals(commonService.getAdmConfigVal("USEICOM"))) {
				messageIcomDAO.insertTOrderMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("VIMS".equals(apkey)) {
			// INSERT VIMS
			if ("YES".equals(commonService.getAdmConfigVal("USEVIMS"))) {
				messageVimsDAO.insertTVimsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("TMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
				messageTmsDAO.insertTTmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("PORTAL".equals(apkey) || "PTL".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPORTAL"))) {
				messagePortalDAO.insertTPortalMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("PMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {
				messagePmsDAO.insertTPmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("BBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEBBS"))) {			// NOPMD - 모듈 구분을 위해 사용됨.
				messageBbsDAO.insertTBbsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("MDM".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEMDM"))) {
				messageDAO.insertTMdmMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("CMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USECMS"))) {
				messageDAO.insertTCmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("FIS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEFIS"))) {
				messageDAO.insertTFisMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("IBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				messageDAO.insertTIbsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("KPI".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				messageKpiDAO.insertTKpiMstMulaapmsgDt(parameterMap);
			}
		}
		
	}
	
	/** 메시지업로드  **/
	@Override
	public void saveUploadMessage(List parameterMap) throws Exception {

		// data insert 
		int size = parameterMap.size();
		for (int i=0 ;size > i ; i++ ) { 
			Map row = (Map) parameterMap.get(i);
	
		// INSERT ADMIN 
			Map map = messageDAO.selectTAdminMstMulaapmsgHd(row);
			if (map == null || map.isEmpty()) {
				messageDAO.insertTAdminMstMulaapmsgHd(row);
			}

			messageDAO.insertTAdminMstMulaapmsgDt(row);

			String apkey = (String) row.get("apkey");
			if ("ICOM".equals(apkey)) {
			// INSERT ICOM
				if ("YES".equals(commonService.getAdmConfigVal("USEICOM"))) {
					messageIcomDAO.insertTOrderMstMulaapmsgDt(row);
				}
			}
			else if ("VIMS".equals(apkey)) {
			// INSERT VIMS
				if ("YES".equals(commonService.getAdmConfigVal("USEVIMS"))) {
					messageVimsDAO.insertTVimsMstMulaapmsgDt(row);
				}
			}
			else if ("TMS".equals(apkey)) {
				if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
					messageTmsDAO.insertTTmsMstMulaapmsgDt(row);
				}
			}
			else if ("PORTAL".equals(apkey) || "PTL".equals(apkey)) {
				if ("YES".equals(commonService.getAdmConfigVal("USEPORTAL"))) {
					messagePortalDAO.insertTPortalMstMulaapmsgDt(row);
				}
			}
			else if ("PMS".equals(apkey)) {
				if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {
					messagePmsDAO.insertTPmsMstMulaapmsgDt(row);
				}
			}
			else if ("BBS".equals(apkey)) {
				if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {			// NOPMD - 모듈 구분을 위해 사용됨.
					messageBbsDAO.insertTBbsMstMulaapmsgDt(row);
				}
			}
			else if ("KPI".equals(apkey)) {
				if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {
					messageKpiDAO.insertTKpiMstMulaapmsgDt(row);
				}
			}else if ("WM".equals(apkey)) {
			// INSERT WMS
				messageWmsDAO.insertTWorkMstMulaapmsgDt(row);
				
				if ("YES".equals(commonService.getAdmConfigVal("USEWM"))) {
					
					// ## SELECT ## 
					List chkList = messageDAO.selectTAdminMsgMulappmsgxct(row);


					// ### INSERT LOGIC ### //
					boolean chkInsert = true;
					if (row != null && !row.isEmpty()) {
						
							chkInsert = true;

							String ctkey1 = (String) row.get("ctkey");
							String ctname1 = (String) row.get("ctname");
							String apkey1 = (String) row.get("apkey");
							String mulaapmsg_hdkey1 = (String) row.get("mulaapmsg_hdkey");
							String msgtype1 = (String) row.get("msgtype");

							if (chkList != null && !chkList.isEmpty()) {
								for (int j = 0; chkList.size() > j; j++) {
									Map chkRow = (Map) chkList.get(j);

									String chkCtkey = (String) chkRow.get("ctkey");
									String chkCtname = (String) chkRow.get("ctname");
									String chkApkey = (String) chkRow.get("apkey");
									String chkMulaapmsg_hdkey = (String) chkRow.get("mulaapmsg_hdkey");
									String chkMsgtype = (String) chkRow.get("msgtype");

									if (ctkey1.equals(chkCtkey) && ctname1.equals(chkCtname) && apkey1.equals(chkApkey) && mulaapmsg_hdkey1.equals(chkMulaapmsg_hdkey) && msgtype1.equals(chkMsgtype)

									) {
										chkInsert = false;
										break;
									  }
								}
							}

							if (chkInsert) {
								messageDAO.insertTAdminMsgMulappmsgxct(row);
							}
				    }
				}
			}
			// 임시테이블 ifstatus update 추가 : 2017.11.01 - ksh
			messageDAO.updateTadminTempMulaapmsgUp(row);
		}
	}
	

	/** **/
	@Override
	public void updateMessage(Map parameterMap) throws Exception {

		messageDAO.updateTAdminMstMulaapmsgHd(parameterMap);

		String apkey = MapUtil.getStr(parameterMap, "apkey");
		String msgType = MapUtil.getStr(parameterMap, "msgtype");
		if ("MENU".equals(msgType)) {
			if (!"ADMIN".equals(apkey) ) {
				throw new MsgException(10028);
			}
			//return ;
		}		
		
		if ("ICOM".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEICOM"))) {
				int chk = messageIcomDAO.selectCountTOrderMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageIcomDAO.updateTOrderMstMulaapmsgDt(parameterMap);
				}
				else {
					messageIcomDAO.insertTOrderMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("WM".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEWM"))) {
				int chk = messageWmsDAO.selectCountTWorkMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageWmsDAO.updateTWorkMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("TMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
				int chk = messageTmsDAO.selectCountTTmsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageTmsDAO.updateTTmsMstMulaapmsgDt(parameterMap);
				}
				else {
					messageTmsDAO.insertTTmsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("PORTAL".equals(apkey) || "PTL".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPORTAL"))) {
				int chk = messagePortalDAO.selectCountTPortalMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messagePortalDAO.updateTPortalMstMulaapmsgDt(parameterMap);
				}
				else {
					messagePortalDAO.insertTPortalMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("VIMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEVIMS"))) {
				int chk = messageVimsDAO.selectCountTVimsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageVimsDAO.updateTVimsMstMulaapmsgDt(parameterMap);
				}
				else {
					messageVimsDAO.insertTVimsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("PMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {
				int chk = messagePmsDAO.selectCountTPmsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messagePmsDAO.updateTPmsMstMulaapmsgDt(parameterMap);
				}
				else {
					messagePmsDAO.insertTPmsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("BBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEBBS"))) {				// NOPMD - 모듈별 처리 구분을 위해 사용됨.
				int chk = messageBbsDAO.selectCountTBbsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageBbsDAO.updateTBbsMstMulaapmsgDt(parameterMap);
				}
				else {
					messageBbsDAO.insertTBbsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("MDM".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEMDM"))) {
				int chk = messageDAO.selectCountTMdmMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageDAO.updateTMdmMstMulaapmsgDt(parameterMap);
				}
				else {
					messageDAO.insertTMdmMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("CMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USECMS"))) {
				int chk = messageDAO.selectCountTCmsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageDAO.updateTCmsMstMulaapmsgDt(parameterMap);
				}
				else {
					messageDAO.insertTCmsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("FIS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEFIS"))) {
				int chk = messageDAO.selectCountTFisMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageDAO.updateTFisMstMulaapmsgDt(parameterMap);
				}
				else {
					messageDAO.insertTFisMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("IBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				int chk = messageDAO.selectCountTIbsMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageDAO.updateTIbsMstMulaapmsgDt(parameterMap);
				}
				else {
					messageDAO.insertTIbsMstMulaapmsgDt(parameterMap);
				}
			}
		}
		else if ("KPI".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				int chk = messageKpiDAO.selectCountTKpiMstMulaapmsgDt(parameterMap);
				if (chk > 0) {
					messageKpiDAO.updateTKpiMstMulaapmsgDt(parameterMap);
				}
				else {
					messageKpiDAO.insertTKpiMstMulaapmsgDt(parameterMap);
				}
			}
		}

		int chk = messageDAO.selectTAdminMstMulaapmsgDtCount(parameterMap);
		System.out.println("☆★☆★chk☆★☆★ : " + chk);
		if (chk > 0) {
			messageDAO.updateTAdminMstMulaapmsgDt(parameterMap);
		}
		else {
			messageDAO.insertTAdminMstMulaapmsgDt(parameterMap);
		}
	}

	/** **/
	@Override
	public void deleteMessage(Map parameterMap) throws Exception {

		String apkey = (String) parameterMap.get("apkey");
		if ("ICOM".equals(apkey)) {
			// UPDATE ICOM 
			if ("YES".equals(commonService.getAdmConfigVal("USEICOM"))) {
				messageIcomDAO.deleteTOrderMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("WM".equals(apkey)) {
			// UPDATE WMS
			if ("YES".equals(commonService.getAdmConfigVal("USEWM"))) {
				messageWmsDAO.deleteTWorkMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("TMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
				messageTmsDAO.deleteTTmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("PORTAL".equals(apkey) || "PTL".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPORTAL"))) {
				messagePortalDAO.deleteTPortalMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("VIMS".equals(apkey)) {
			// UPDATE WMS
			if ("YES".equals(commonService.getAdmConfigVal("USEVIMS"))) {
				messageVimsDAO.deleteTVimsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("PMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEPMS"))) {
				messagePmsDAO.deleteTPmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("BBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEBBS"))) {		// NOPMD - 모듈별 처리 구분을 위해 사용됨.
				messageBbsDAO.deleteTBbsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("MDM".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEMDM"))) {
				messageDAO.deleteTMdmMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("CMS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USECMS"))) {
				messageDAO.deleteTCmsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("FIS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEFIS"))) {
				messageDAO.deleteTFisMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("IBS".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				messageDAO.deleteTIbsMstMulaapmsgDt(parameterMap);
			}
		}
		else if ("KPI".equals(apkey)) {
			if ("YES".equals(commonService.getAdmConfigVal("USEIBS"))) {
				messageKpiDAO.deleteTKpiMstMulaapmsgDt(parameterMap);
			}
		}
		// DELETE ADMIN
		messageDAO.deleteTAdminMsgMulappmsgxct(parameterMap);
		messageDAO.deleteTAdminMstMulaapmsgDt(parameterMap);

		Map selectMap = new HashMap();
		selectMap.put("mulaapmsg_hdkey", parameterMap.get("mulaapmsg_hdkey"));
		int cnt = messageDAO.selectTAdminMstMulaapmsgDtCount(selectMap);
		if (cnt < 1) {
			messageDAO.deleteTAdminMstMulaapmsgHd(parameterMap);
		}
	}

	/** **/
	@Override
	public List selectTAdminMstCt(Map parameterMap) throws Exception {
		return messageDAO.selectTAdminMstCt(parameterMap);
	}

	/** **/
	@Override
	public List selectTAdminMsgMulappmsgxct(Map parameterMap) throws Exception {
		return messageDAO.selectTAdminMsgMulappmsgxct(parameterMap);
	}

	/** **/
	@Override
	public void saveMessageCenter(Map parameterMap, List parameterList) throws Exception {

		List saveList = new ArrayList();
		// ### DELETE LOGIC ### //

		// ## SELECT ## 
		List chkList = messageDAO.selectTAdminMsgMulappmsgxct(parameterMap);
		boolean chkDelete = true;
		if (chkList != null && !chkList.isEmpty()) {
			for (int i = 0; chkList.size() > i; i++) {
				chkDelete = true;

				Map chkRow = (Map) chkList.get(i);

				LOG.debug("chkRow => " + chkRow);

				String chkCtkey = (String) chkRow.get("ctkey");
				String chkCtname = (String) chkRow.get("ctname");
				String chkApkey = (String) chkRow.get("apkey");
				String chkMulaapmsg_hdkey = (String) chkRow.get("mulaapmsg_hdkey");
				String chkMsgtype = (String) chkRow.get("msgtype");

				if (parameterList != null && !parameterList.isEmpty()) {
					for (int j = 0; parameterList.size() > j; j++) {
						Map row = (Map) parameterList.get(j);

						String ctkey = (String) row.get("ctkey");
						String ctname = (String) row.get("ctname");
						String apkey = (String) row.get("apkey");
						String mulaapmsg_hdkey = (String) row.get("mulaapmsg_hdkey");
						String msgtype = (String) row.get("msgtype");

						if (ctkey.equals(chkCtkey) && ctname.equals(chkCtname) && apkey.equals(chkApkey) && mulaapmsg_hdkey.equals(chkMulaapmsg_hdkey) && msgtype.equals(chkMsgtype)) {
							chkDelete = false;
							parameterList.remove(j);
							break;
						}
						
					}
					if (chkDelete) {
						// ## DELETE ##
						if ("WM".equals(chkApkey)) {
							if ("YES".equals(commonService.getAdmConfigVal("USEWM"))) {
								messageWmsDAO.deleteTWorkMstMulaapmsgDt(chkRow);
							}
						}
						else {
							if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
								messageTmsDAO.deleteTTmsMstMulaapmsgDt(chkRow);
							}
						}
						// ADMIN.TADMIN_MST_MULAAPMSGXCT DELETE
						messageDAO.deleteTAdminMsgMulappmsgxct(chkRow);
					}
					
				} else {
					
					// ## DELETE ##
					if ("WM".equals(chkApkey)) {
						if ("YES".equals(commonService.getAdmConfigVal("USEWM"))) {
							messageWmsDAO.deleteTWorkMstMulaapmsgDt(chkRow);
						}
					}
					else {
						if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
							messageTmsDAO.deleteTTmsMstMulaapmsgDt(chkRow);
						}
					}
					// ADMIN.TADMIN_MST_MULAAPMSGXCT DELETE
					messageDAO.deleteTAdminMsgMulappmsgxct(chkRow);
				}
			}
		}

		// ### INSERT LOGIC ### //
		boolean chkInsert = true;
		if (parameterList != null && !parameterList.isEmpty()) {
			for (int i = 0; parameterList.size() > i; i++) {

				chkInsert = true;

				Map row = (Map) parameterList.get(i);

				String ctkey = (String) row.get("ctkey");
				String ctname = (String) row.get("ctname");
				String apkey = (String) row.get("apkey");
				String mulaapmsg_hdkey = (String) row.get("mulaapmsg_hdkey");
				String msgtype = (String) row.get("msgtype");
				
				if ("WM".equals(apkey) && "YES".equals(commonService.getAdmConfigVal("USEWM"))) {
					messageWmsDAO.insertTWorkMstMulaapmsgDt(row);
				}
				
				/*else {
					if ("YES".equals(commonService.getAdmConfigVal("USETMS"))) {
						messageTmsDAO.insertTTmsMstMulaapmsgDt(row);
					}
				}*/
				
				messageDAO.insertTAdminMsgMulappmsgxct(row);
			}
		}
	}

	@Override
	public List selectExcelDownMessage(Map parameterMap) throws Exception {
		return messageDAO.selectExcelDownMessage(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownMessage(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return messageDAO.selectExcelDownMessage(parameterMap,excelFormat);
	}

}
