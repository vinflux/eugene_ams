package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface StorexctService {
	
	/** **/
	public Map selectStorexctInfo (Map parameterMap) throws Exception;
	
	/** **/
	public List selectStorexct (Map parameterMap) throws Exception;
	
	/** **/
	public int checkStorexctCount (Map parameterMap) throws Exception;
	
	/** **/
	public void insertStorexct (Map parameterMap) throws Exception;
	
	/** **/
	public void updateStorexct (Map parameterMap) throws Exception;
	
	/** **/
	public void deleteStorexct (Map parameterMap) throws Exception;
	
	/** **/
	public List selectExcelDownStorexct (Map parameterMap) throws Exception;

	public Workbook selectExcelDownStorexct(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
