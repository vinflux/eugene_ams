package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.AcxctService;
import com.vinflux.adm.persistence.AcxctDAO;

@Service("acxctService")
public class AcxctServiceImpl implements AcxctService {
	
	@Resource(name="acxctDAO")
	private AcxctDAO acxctDAO;
	
	//@Resource(name="acxctIcomDAO")
	//private AcxctIcomDAO acxctIcomDAO;
	
	//@Resource(name="acxctWmsDAO")
	//private AcxctWmsDAO acxctWmsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectAcxctInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = acxctDAO.selectAcxctCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = acxctDAO.selectAcxct(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	/** **/
	@Override
	public Map selectDetailAcxctInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = acxctDAO.selectDetailAcxctCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = acxctDAO.selectDetailAcxct(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectAcxct (Map parameterMap) throws Exception {
		return acxctDAO.selectPkAcxct(parameterMap);
	}
	
	@Override
	public int checkAcxctCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return acxctDAO.checkAcxctCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.AcxctService#insertAcxct(java.util.Map)
	 */
	@Override
	public void insertAcxct (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.acxctDAO.insertTAdminMstAcxct(parameterMap);
		
		// ICOM INSERT
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.acxctIcomDAO.insertTOrderMstAcxct(parameterMap);	
		}
		*/
		
		// WMS INSERT
		/*
		 * 로직 처리하는 부분이 주석이므로 전체 주석처리
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			List list = acxctDAO.selectCtkey(parameterMap);
//			if (list != null && !list.isEmpty()) { 
//        		for(int i=0;i<list.size();i++){
//        			Map ctKeyMap = (Map) list.get(i);
//        			parameterMap.remove("ctkey");
//        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
//        			this.acxctWmsDAO.insertTWorkMstAcxct(parameterMap);
//        		}
//        	}
		}
		*/
	}
	
	/** **/
	@Override
	public void updateAcxct (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.acxctDAO.updateTAdminMstAcxct(parameterMap);
		
//		int count = 0;
		
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.acxctDAO.updateTWorkMstAcxct(parameterMap);
		}
	}

	/** **/
	@Override
	public void deleteAcxct (Map parameterMap) throws Exception {
		// ICOM DELETE
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			//this.acxctIcomDAO.deleteTOrderMstAcxct(parameterMap);
		}
		*/

		// WMS DELETE
		/*
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//this.acxctWmsDAO.deleteTWorkMstAcxct(parameterMap);
		}
		*/
		// ADMIN DELETE
		this.acxctDAO.deleteTAdminMstAcxct(parameterMap);
		
		
	}
	
	@Override
	public Workbook selectExcelDownHerderAcxct(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return acxctDAO.selectExcelDownHerderAcxct(parameterMap, excelFormat);
	}
	
	@Override
	public Workbook selectExcelDownDetailAcxct(Map parameterMap, List<Map> excelFormat) throws Exception {
		return acxctDAO.selectExcelDownDetailAcxct(parameterMap, excelFormat);
	}
	
	@Override
	public Workbook selectExcelDownAllAcxct(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return acxctDAO.selectExcelDownAllAcxct(parameterMap, excelFormat);
	}
}