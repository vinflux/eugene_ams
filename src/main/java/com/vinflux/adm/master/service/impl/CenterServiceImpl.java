package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.common.service.EntityAddrService;
import com.vinflux.adm.master.service.CenterService;
import com.vinflux.adm.persistence.CenterByRoleDAO;
import com.vinflux.adm.persistence.CenterDAO;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.tms.persistence.CenterTmsDAO;
import com.vinflux.wms.persistence.CenterWmsDAO;

@Service("centerService")
public class CenterServiceImpl implements CenterService {
	
	@Resource(name="centerDAO")
	private CenterDAO centerDAO;
	
	@Resource(name="centerWmsDAO")
	private CenterWmsDAO centerWmsDAO;

//	@Resource(name="centerIcomDAO")
//	private CenterIcomDAO centerIcomDAO;

	@Resource(name="centerTmsDAO")
	private CenterTmsDAO centerTmsDAO;

//	@Resource(name="centerVimsDAO")
//	private CenterVimsDAO centerVimsDAO;
	
	@Resource(name="centerByRoleDAO")
	private CenterByRoleDAO centerByRoleDAO;

	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;	
	
	@Resource(name = "entityAddrService")
	private EntityAddrService entityAddrService;	
	
	/** **/
	@Override
	public Map selectCenterInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = centerDAO.selectCenterCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = centerDAO.selectCenter(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectCenter (Map parameterMap) throws Exception {
		return centerDAO.selectPkCenter(parameterMap);
	}

	@Override
	public void insertCenter (Map parameterMap) throws Exception {
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");

		
		if(!"".equals(parameterMap.get("zipNo"))){
			//SELECT SEQ_TADMIN_MST_ADDR_T.nextval as SEQ  FROM dual
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
		
		this.centerDAO.insertCenter(parameterMap);
		this.centerDAO.mergeTtShpgLocT(parameterMap);
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.centerIcomDAO.insertTOrderMstCt(parameterMap);
			entityAddrService.addAddressEntitymasterIcom(parameterMap);
		}*/

		// WMS 추가.
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.centerWmsDAO.insertTWorkMstCt(parameterMap);
			entityAddrService.addAddressEntitymasterWms(parameterMap);
		}

		// TMS 추가.
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.centerTmsDAO.insertTTMstCt(parameterMap);
			entityAddrService.addAddressEntitymasterTms(parameterMap);
		}	
		// VIMS 추가.
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			this.centerVimsDAO.insertTViewMstCt(parameterMap);
		}*/			
	}
	
	/** **/
	@Override
	public void updateCenter (Map parameterMap) throws Exception {
		String zipNo = (String)parameterMap.get("zipNo");
		String lnmAdres = (String)parameterMap.get("lnmAdres");
		String address2 = (String)parameterMap.get("address2");
	
		parameterMap.put("zipcd", zipNo);
		parameterMap.put("zipcode", zipNo);
		parameterMap.put("address1", lnmAdres);
		parameterMap.put("ctynm",lnmAdres);
		parameterMap.put("stnm",address2);
		parameterMap.put("lon","0");
		parameterMap.put("lat","0");
		parameterMap.put("addrsttcd","1");
		parameterMap.put("cntrycd","KOR");
		
		if("".equals(parameterMap.get("addrid")) && !"".equals(parameterMap.get("zipNo"))){
			int intAddrId = entityAddrService.getAddrId();
			parameterMap.put("addrid", intAddrId);
			entityAddrService.addAddressEntitymaster(parameterMap);
		}
		
//		int count=0;
		
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = this.centerIcomDAO.updateTOrderMstCt(parameterMap);
			if(count==0){
				this.centerIcomDAO.insertTOrderMstCt(parameterMap);
				entityAddrService.changeAddressEntitymasterIcom(parameterMap);
			}
		}*/
		this.centerDAO.updateCenter(parameterMap);
		this.centerDAO.mergeTtShpgLocT(parameterMap);
		
		
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.centerWmsDAO.updateTWorkMstCt(parameterMap);
			entityAddrService.changeAddressEntitymasterWms(parameterMap);
		}
		
		if(!"".equals(parameterMap.get("addrid"))) {
			entityAddrService.changeAddressEntitymaster(parameterMap);
		}
		
		// TMS 추가.
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.centerTmsDAO.updateTTMstCt(parameterMap);
			entityAddrService.changeAddressEntitymasterTms(parameterMap);
		}
		// VIMS 추가.
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			count = this.centerVimsDAO.updateTViewMstCt(parameterMap);
			if(count==0){
				this.centerVimsDAO.insertTViewMstCt(parameterMap);
			}
		}*/
		
	}

	
	
	/** **/
	@Override
	public void deleteCenter (Map parameterMap) throws Exception {
		
		// CENTER CHECK 
		int chk = centerDAO.selectTAdminMstIcxctCount(parameterMap);
		if (chk > 0  ) {
			throw new MsgException(11004);
		}
		chk = centerDAO.selectTAdminMstIcgrxctCount(parameterMap);
		if (chk > 0  ) { 
			throw new MsgException(11005);
		}
		chk = centerDAO.selectTAdminMsticutxtypexctCount(parameterMap);
		if (chk > 0  ) { 
			throw new MsgException(11006);
		}
		chk = centerDAO.selectTAdminMstMulaapmsgxctCount(parameterMap);
		if (chk > 0  ) { 
			throw new MsgException(11007);
		}
		chk = centerDAO.selectTAdminMstAcxctCount(parameterMap);
		if (chk > 0  ) { 
			throw new MsgException(11008);
		}
		chk = centerDAO.selectTAdminMstEqxctCount(parameterMap);
		if (chk > 0  ) { 
			throw new MsgException(11009);
		}
		//chk = centerDAO.selectTAdminMstRoxctgtCount(parameterMap);
		//if (chk > 0  ) { 
		//	throw new MsgException(10004);
		//}
		
		/* 10.21 OMS 삭제작업
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.centerIcomDAO.deleteTOrderMstCt(parameterMap);
			entityAddrService.deleteAddressEntitymasterIcom(parameterMap);
		}*/
		
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			this.centerWmsDAO.deleteTWorkMstCt(parameterMap);
			entityAddrService.deleteAddressEntitymasterWms(parameterMap);
		}
		
		
		// TMS 추가.
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.centerTmsDAO.deleteTTMstCt(parameterMap);
			entityAddrService.deleteAddressEntitymasterTms(parameterMap);
		}
		
		// VIMS 추가.
		/*if( "YES".equals(commonService.getAdmConfigVal("USEVIMS")) ) {
			this.centerVimsDAO.deleteTViewMstCt(parameterMap);
		}*/
		
		this.centerByRoleDAO.deleteTAdminMstRoxctgt(parameterMap);
		this.centerDAO.deleteCenter(parameterMap);
		entityAddrService.deleteAddressEntitymaster(parameterMap);
		parameterMap.put("delyn", "Y");
		this.centerDAO.mergeTtShpgLocT(parameterMap);
		
	}

	/** **/
	@Override
	public List selectExcelDownCenter (Map parameterMap) throws Exception {
    	return centerDAO.selectExcelDownCenter(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownCenter(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return centerDAO.selectExcelDownCenter(parameterMap, excelFormat);
	}
	
	/**
	 * 마스터 화면에서 사용하는 센터 리스트
	 * @param cskey
	 * @return String
	 * @throws Exception
	 */	
	@Override
	public List getTAdminMstCtList(Map parameterMap) throws Exception {
		return this.centerDAO.getTAdminMstCtList(parameterMap);
	}
}