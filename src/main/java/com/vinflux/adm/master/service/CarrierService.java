package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface CarrierService {
	
	public Map selectCarrInfo(Map paramMap) throws Exception;
	
	public Map selectCarr(Map paramMap) throws Exception;
	
	public List selectExcelDownCarr(Map paramMap) throws Exception;
	
	public Workbook selectExcelDownCarr(Map parameterMap, List<Map> excelFormat) throws Exception;
	
	public List selectCarrCdReport(Map paramMap) throws Exception;
	
	public void approveCarrList(List paramList) throws Exception;

	public int checkValidCarrList(List parameterList) throws Exception;

}