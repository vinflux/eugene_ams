package com.vinflux.adm.master.service;

import java.util.List;
import java.util.Map;


public interface PushMobService {

	public Map selectPushInfo(Map parameterMap) throws Exception;
	
	public Map selectDetailPushInfo(Map parameterMap) throws Exception;
	
	public int deletePush(Map parameterMap) throws Exception;
	
	public int deletePushDtl(Map parameterMap, boolean isRootDelete) throws Exception;
	
	public void insertPushInfo(Map parameterMap) throws Exception;
	
	public void updatePushInfo(Map parameterMap) throws Exception;
	
	List selectUserListByUserGroup(Map parameterMap) throws Exception;
	
	int insertPush(Map parameterMap) throws Exception;
	
	int updatePushHd(Map parameterMap) throws Exception;
	
	int updatePush(Map parameterMap) throws Exception;
}
