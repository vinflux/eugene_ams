package com.vinflux.adm.master.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.IcxctService;
import com.vinflux.adm.persistence.IcxctDAO;
import com.vinflux.icom.persistence.IcxctIcomDAO;
import com.vinflux.tms.persistence.IcxctTmsDAO;

@Service("icxctService")
public class IcxctServiceImpl implements IcxctService {
	
	@Resource(name="icxctDAO")
	private IcxctDAO icxctDAO;
	
	@Resource(name="icxctIcomDAO")
	private IcxctIcomDAO icxctIcomDAO;
	
	//@Resource(name="icxctWmsDAO")
	//private IcxctWmsDAO icxctWmsDAO;
	
	@Resource(name="icxctTmsDAO")
	private IcxctTmsDAO icxctTmsDAO;
	
	// 모듈별 전송여부 판별을 위함
	@Resource(name="commonService")
	private CommonService commonService;
	

	/** **/
	@Override
	public Map selectIcxctInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = icxctDAO.selectIcxctCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = icxctDAO.selectIcxct(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	/** **/
	@Override
	public List selectIcxct (Map parameterMap) throws Exception {
		return icxctDAO.selectPkIcxct(parameterMap);
	}
	
	@Override
	public int checkIcxctCount(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return icxctDAO.checkIcxctCount(parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * @see com.vinflux.adm.master.service.IcxctService#insertIcxct(java.util.Map)
	 */
	@Override
	public void insertIcxct (Map parameterMap) throws Exception {
		// ADMIN INSERT
		this.icxctDAO.insertTAdminMstIcxct(parameterMap);
		
		// ICOM INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.icxctIcomDAO.insertTOrderMstIcxct(parameterMap);	
		}
		
		// WMS INSERT
		/*
		 * 처리로직이 주석처리되어 있으므로 전체 주석처리
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			List list = icxctDAO.selectCtkey(parameterMap);
//        	if (list != null && !list.isEmpty()) { 
//        		for(int i=0;i<list.size();i++){
//        			Map ctKeyMap = (Map) list.get(i);
//        			parameterMap.remove("ctkey");
//        			parameterMap.put("ctkey", ctKeyMap.get("ctkey"));
//        			this.icxctWmsDAO.insertTWorkMstIcxct(parameterMap);
//        		}
//        	}
		}
	*/
		
		// TMS INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.icxctTmsDAO.insertTTMstIcxct(parameterMap);
		}
	}
	
	/** **/
	@Override
	public void updateIcxct (Map parameterMap) throws Exception {
		// ADMIN UPDATE
		this.icxctDAO.updateTAdminMstIcxct(parameterMap);
		
		int count = 0;
		
		// ICOM INSERT
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			count = this.icxctIcomDAO.updateTOrderMstIcxct(parameterMap);
			if(count==0){
				this.icxctIcomDAO.updateTOrderMstIcxct(parameterMap);
			}
		}
		
		// WMS UPDATE 
		/*
		 * 처리되는 내용이 없으므로 전체 주석처리함
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//count = this.icxctWmsDAO.updateTWorkMstIcxct(parameterMap);
			if(count==0){
				//this.icxctWmsDAO.insertTWorkMstIcxct(parameterMap);
			}
		}
		*/
		
		// TMS UPDATE
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.icxctTmsDAO.updateTTMstIcxct(parameterMap);
		}
	}
	
	
	/** **/
	@Override
	public void updateAllIcxct (Map parameterMap) throws Exception {
		this.icxctDAO.updateAllIcxct(parameterMap);
	}
	
	/** **/
	@Override
	public void updateChkIcxct (HashMap parameterMap) throws Exception {

		String[] chkList = parameterMap.get("chkList").toString().split(",");
		
		List<String> list = new ArrayList<String>();
		for(int i=0; i<chkList.length; i++){
			list.add((String)chkList[i]);
		}
		parameterMap.put("chkArray", list);
		
		
		this.icxctDAO.updateChkIcxct(parameterMap);
	}

	/** **/
	@Override
	public void deleteIcxct (Map parameterMap) throws Exception {
		// ICOM DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USEICOM")) ) {
			this.icxctIcomDAO.deleteTOrderMstIcxct(parameterMap);
		}

		// WMS DELETE
		/*
		 * 처리내용 주석처리되어있음.
		if( "YES".equals(commonService.getAdmConfigVal("USEWM")) ) {
			//this.icxctWmsDAO.deleteTWorkMstIcxct(parameterMap);
		}
		*/
		
		// TMS DELETE
		if( "YES".equals(commonService.getAdmConfigVal("USETMS")) ) {
			this.icxctTmsDAO.deleteTTMstIcxct(parameterMap);
		}
		
		// ADMIN DELETE
		this.icxctDAO.deleteTAdminMstIcxct(parameterMap);
		
		
	}

	/** **/
	@Override
	public List selectExcelDownIcxct (Map parameterMap) throws Exception {
    	return icxctDAO.selectExcelDownIcxct(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownIcxct(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return icxctDAO.selectExcelDownIcxct(parameterMap, excelFormat);
	}
}