package com.vinflux.adm.master.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.master.service.AccountTrans;
import com.vinflux.adm.master.service.AccountUploadService;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Service("accountTrans")
public class AccountTransImpl implements AccountTrans {
	
	
//	@Resource(name="accountUploadDAO")
//	private AccountUploadDAO accountUploadDAO;
	
//	@Resource(name = "accountService")
//	private AccountService accountService;
	
	@Resource(name = "accountUploadService")
	private AccountUploadService accountUploadService;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	
	protected static final Log LOG = LogFactory.getLog(AccountTransImpl.class);

	@Override
	public Map saveAccountUpload(Map parameterMap ,  List parameterList) throws Exception {
    	if (parameterList == null  || parameterList.isEmpty() ) { 
    		throw new MsgException(104);
    	}

//    	String ulhsKey = (String)parameterMap.get("ulhskey");
    	String laKey = (String)parameterMap.get("lakey");
    	String errorCode = null;
    	String mulMsgCode = null;
    	String ifMessage = null;
		
		CommandController cc = new CommandController();
    	Map msgMap = new HashMap();
    	Map getMapParam = new HashMap();
    	
    	//common param
    	getMapParam.put("lakey", laKey);
    	
    	//center list
    	getMapParam.put("searchid", "60");
    	Map ctMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
    	//actype list
    	getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "ACTYPE");
    	Map acTypeMap = commonService.getAdmMasternCodeMap(getMapParam);
    	
    	//countrycode list
    	/*getMapParam.put("searchid", "20");
    	getMapParam.put("adcd_hdkey", "COUNTRYCODE");
    	Map ccMap = commonService.getAdmMasternCodeMap(getMapParam);*/
    	
    	Set ctSet = ctMap.keySet();
    	Iterator ctIrt = ctSet.iterator();
  
    	int size = parameterList.size();
    	
    	for (int i=0 ;size > i ; i++ ) { 
	    	Map acMap = (Map)parameterList.get(i);
			try {
				acMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_WORKING);
		    	accountUploadService.updateTadminTempAcup(acMap);
				
		    	String acType = MapUtil.getStr(acMap, "actype");
		    	//String countryCode = MapUtil.getStr(acMap, "countrycode");

		    	boolean isSame = true;
	    		if(acTypeMap.containsKey(acType)){
	    			isSame = false;
	    		}
		    	
				if(acType != null && isSame ) {
					throw new MsgException(10015); // 유효하지 않은 거래처 유형입니다.
				}
				
				/*if(countryCode != null && !ccMap.containsKey(countryCode) ) {
					throw new MsgException(10016); //유효하지 않은 국가 코드입니다.
				}*/
				
				accountUploadService.saveAccountCode(acMap, ctIrt, ctMap);
			}catch( DataAccessException dae ){
				Throwable throwable = dae.getCause();
				if ( throwable instanceof SQLException ){    		
					errorCode = String.valueOf(((SQLException) throwable).getErrorCode());
		    	} 
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				acMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				acMap.put("ifmessage", ifMessage);
				accountUploadService.updateTadminTempAcup(acMap);
			}catch( SQLException se ){
				errorCode = String.valueOf(se.getErrorCode());
				mulMsgCode = "MSG_" + cc.getDBErrorMsgCode(errorCode);
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				acMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				acMap.put("ifmessage", ifMessage);
				accountUploadService.updateTadminTempAcup(acMap);
			}catch( MsgException me ){
				errorCode = String.valueOf( me.getCode() );
				mulMsgCode = "MSG_" + errorCode;
				msgMap.put("msgcode", mulMsgCode);
				msgMap.put("lakey", laKey);
				msgMap.put("argmap",me.getArgMap() );
				ifMessage = commonService.getAdmMulLangMsg(msgMap);
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				acMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				acMap.put("ifmessage", ifMessage);
				accountUploadService.updateTadminTempAcup(acMap);
			}
			catch (Exception e) {
				ifMessage = e.getMessage();
				LOG.error("", e); 
				
				if(ifMessage == null || "".equals(ifMessage)) {
					ifMessage = mulMsgCode;
				}

				acMap.put("ifstatus", EnvConstant.INTF_STATUS_CODE_FAIL);
				acMap.put("ifmessage", ifMessage);
				accountUploadService.updateTadminTempAcup(acMap);
			} 
    	}

    	// Select 
    	return accountUploadService.selectAccountUploadInfo(parameterMap);
    }

}
