package com.vinflux.adm.master.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.idgen.IdGenService;
import com.vinflux.framework.util.CommonUtil;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.UnicodeUtil;
import com.vinflux.framework.web.CommandController;
import com.vinflux.adm.master.service.BoardService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 


@Controller("boardController")
@RemotingDestination
public class BoardController extends CommandController {
	
	@Resource(name = "boardService")
	private BoardService boardService;
	
/*	@Resource(name="newsKeyIdGenService")
	private IdGenService newsKeyIdGenService;

	@Resource(name="noticeKeyIdGenService")
	private IdGenService noticeKeyIdGenService;

	*/
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(BoardController.class);
	
    /**
     * 공지사항
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNoticeInfoList.do")
    @ResponseBody   
    public CommandMap selectNoticeInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getParamMap(); 
    	Map param = requestMap.getParamMap();
    	//쿼리와 조회조건을 맞추기 위한 로직
    	List SearchList = requestMap.getSearchList();
    	//건수 가져오기
    	

    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, SearchList);
    	LOG.debug("CommandMap => " + CommandMap.COMMAND_SEARCHLIST);
    	paramMap = requestMap.getSearchQuery();
    	paramMap.put("START_DT", param.get("START_DT"));
    	paramMap.put("END_DT", param.get("END_DT"));
    	paramMap.putAll(param);
    	Map noticeMap = boardService.selectNoticeInfo(paramMap);
    	Integer count = MapUtil.getInt(noticeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)noticeMap.get("list"));
    	} 	
    	
    	return requestMap;
    }
    
   
    /**
     * 공지사항 사용자그룹
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNoticeGrList.do")
    @ResponseBody   
    public CommandMap selectNoticeGrList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();	
    	paramMap.putAll(param);    	
    	
    	Map noticeMap = boardService.selectNoticeGrInfo(paramMap);

		requestMap.putParam("COUNT", Integer.toString(noticeMap.size()));
		requestMap.addList("rtnGrid", (ArrayList)noticeMap.get("list"));

    	
    	return requestMap;
    }
    
    /**
     * 공지사항 FILE
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectFileList.do")
    @ResponseBody   
    public CommandMap selectFileList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();	
    	paramMap.putAll(param);    	
    	
    	Map noticeMap = boardService.selectFileList(paramMap);

		requestMap.putParam("COUNT", Integer.toString(noticeMap.size()));
		requestMap.addList("rtnGrid", (ArrayList)noticeMap.get("list"));

    	
    	return requestMap;
    }
    
    /**
     * 공지사항 사용자그룹 유저
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNoticeUrGrList.do")
    @ResponseBody   
    public CommandMap selectNoticeUrGrList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();	
    	paramMap.putAll(param);    	
    	
    	Map noticeMap = boardService.selectNoticeUrGrInfo(paramMap);

    		requestMap.putParam("COUNT", Integer.toString(noticeMap.size()));
    		requestMap.addList("rtnGrid", (ArrayList)noticeMap.get("list"));

    	
    	return requestMap;
    }
    public static String getString(Map map, String paramStr) throws Exception {
    	String returnString = "";
    	if(map != null && map.get(paramStr) != null) {
    	returnString = map.get(paramStr).toString();
    	}
    	return returnString;
    }
    /**
     * ADM > 마스터 > 게시판 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNoticeInfo.do")
    @ResponseBody   
    public CommandMap selectNoticeInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	Map paramMap = requestMap.getParamMap();
    	Map param = new HashMap();
    	//쿼리와 조회조건을 맞추기 위한 로직
    	List SearchList = requestMap.getSearchList();
    	//건수 가져오기
    	

    	
    	for( int i = SearchList.size()-1 ; i >= 0 ; i--){
	    	Map map = (Map)SearchList.get(i);
	    	String dbColoum = getString(map, "dbColoum");
	    	
	    	if("NOTICEDATE".equals(dbColoum)){
	    		param.put("START_DT", getString(map, "fromVal").substring(0, 8));
	    		param.put("END_DT", getString(map, "toVal").substring(0, 8));
	    		SearchList.remove(i);
		    } 	    	
    	}
    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, SearchList);
    	LOG.debug("requestMap => " + requestMap);
    	LOG.debug("SearchList => " + SearchList);
    	
    	paramMap = requestMap.getSearchQuery();
    	paramMap.put("START_DT", param.get("START_DT"));
    	paramMap.put("END_DT", param.get("END_DT"));
    	LOG.debug("requsetMap => " + paramMap);
    	List list = boardService.selectNotice(paramMap);
    	if (list != null && !list.isEmpty() ) {
    		requestMap.addList("FORM_DATA", list);
    		//requestMap.putMap("FORM_DATA", (Map)list.get(0));	
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}
    	    	

    	return requestMap;
    }
    
    
    /**
     * 공지사항 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/saveNotice.do")
    @ResponseBody   
    public CommandMap saveNotice (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List gr = requestMap.getList("gr_LIST");
    	List file = requestMap.getList("file_LIST");
    	List returnList = new ArrayList();
    	
	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS"))&&"1".equals(inputMap.get("CHK")) ) {
    				inputMap.put("ctkey", requestMap.getCommonUserInfo().getCtKey());
    				inputMap.put("gr", gr);
    				inputMap.put("file", file);
    				boardService.insertNotice(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS"))) {
    				inputMap.put("gr", gr);
    				inputMap.put("file", file);
    				boardService.updateNotice(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS"))&&"1".equals(inputMap.get("CHK"))) {
    				boardService.deleteNotice(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }
    
    /**
     * 공지사항 댓글 등록
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/saveNoticeRepple.do")
    @ResponseBody   
    public CommandMap saveNoticeRepple (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");
		Map formMap = requestMap.getMap("FORM_DATA");		
		
    	List returnList = new ArrayList();   	
		
		formMap.put("ctkey", requestMap.getCommonUserInfo().getCtKey());
		
		boardService.insertNoticeRepple(formMap);
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }
    
    
    /**
     * 공지사항
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNewsInfoList.do")
    @ResponseBody   
    public CommandMap selectNewsInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();	
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = boardService.selectNewsInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	} 	
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 게시판 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/selectNewsInfo.do")
    @ResponseBody   
    public CommandMap selectNewsInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	List list = boardService.selectNews(requestMap.getParamMap());
    	if (list != null && !list.isEmpty() ) {
    		requestMap.addList("FORM_DATA", list);
    		//requestMap.putMap("FORM_DATA", (Map)list.get(0));	
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}
    	    	

    	return requestMap;
    }
    
    
    /**
     * 공지사항 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/saveNews.do")
    @ResponseBody   
    public CommandMap saveNews (@RequestBody CommandMap requestMap) throws Exception {
    	
    	Map paramMap = requestMap.getParamMap();
    	Map formMap = requestMap.getMap("FORM_DATA");
    	String workType = (String)paramMap.get("workType");
    	 	
    	if ("INSERT".equals(workType) ) {
    		Map insertParam = requestMap.getMap("FORM_DATA");
    		insertParam.put("ctkey", requestMap.getCommonUserInfo().getCtKey());
    		//insertParam.put("NO_NO"   , newsKeyIdGenService.getNextStringIdByKey(MapUtil.getStr(insertParam, "ctkey")));
    		
    		boardService.insertNews(insertParam);
    	} else if ("UPDATE".equals(workType) ) {
    		boardService.updateNews(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			boardService.deleteNews(row);
    		}
    	} 
    	return requestMap;
    }
    
    
    /**
     * ADM >  공지사항 엑셀다운
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map excelParam = requestMap.getParamMap();
 	
    	
    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)excelParam.get("EXCEL_FILENAME");

    	
    	Map paramMap = requestMap.getParamMap();
    	Map param = new HashMap();
    	//쿼리와 조회조건을 맞추기 위한 로직
    	List SearchList = requestMap.getSearchList();
    	//건수 가져오기
    	

    	
    	for( int i = SearchList.size()-1 ; i >= 0 ; i--){
	    	Map map = (Map)SearchList.get(i);
	    	String dbColoum = getString(map, "dbColoum");
	    	
	    	if("NOTICEDATE".equals(dbColoum)){
	    		param.put("START_DT", getString(map, "fromVal").substring(0, 8));
	    		param.put("END_DT", getString(map, "toVal").substring(0, 8));
	    		SearchList.remove(i);
		    } 	    	
    	}
    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, SearchList);
    	requestMap.put("START_DT", param.get("START_DT"));
    	requestMap.put("END_DT", param.get("END_DT"));
    	
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				boardService.selectExcelDownNotice(requestMap, excelFormat),
				fileName, response);
    }
    
    /**
     * ADM > 마스터 > 거래처 > 파일업로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/fileUpload.do")
    @ResponseBody
    public JSONObject  fileUpload  (HttpServletRequest request , HttpServletResponse response) throws Exception {
    	
    	MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multiRequest.getFileMap();				
		JSONObject jsonObject = new JSONObject();
	    Iterator<Entry<String, MultipartFile>> itr = fileMap.entrySet().iterator();
		MultipartFile file;

		boolean transactionStatus = false; 
		int errorCode = 101;
		Map argMap = new HashMap();
		//String ulhsKey = "";
	    
		OutputStream out = null;
		StringBuffer filePath = new StringBuffer();
		
		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();

			file = entry.getValue();
			if (!"".equals(file.getOriginalFilename())) {
/*
				if (file.getOriginalFilename().endsWith(".xls")
//						|| file.getOriginalFilename().endsWith(".xlsx")
//						|| file.getOriginalFilename().endsWith(".XLSX")
						|| file.getOriginalFilename().endsWith(".XLS")
					) {
	*/				
				
					try {
			     
						long currentTime = System.currentTimeMillis();

						String folderPath = EnvConstant.BOARD_FILE_PATH + new SimpleDateFormat("yyyyMMdd").format(currentTime); // NOPMD – 초기화를 위한 로직 [ 김상헌 ]
						
						File folderPathFile = new File(folderPath); // NOPMD – 초기화를 위한 로직 [ 김상헌 ]
						if(!folderPathFile.exists()){
							folderPathFile.mkdirs();
						}
						String saveFile = currentTime+"."+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1,file.getOriginalFilename().length());
			            filePath.append(folderPath);
			            filePath.append("/");
			            filePath.append(saveFile);
//			            filePath.append("_");
//			            filePath.append(currentTime);
			            jsonObject.put("fileName", file.getOriginalFilename());
			            jsonObject.put("fileid", saveFile);
		//		    	LOG.debug("======================================");
				    	LOG.debug("fileName => " + file.getOriginalFilename());
		//		    	LOG.debug("saveFile => " + saveFile);
		//		    	LOG.debug("======================================");
				    	

				    	// 설정한 path에 파일저장
				    	File serverFile = new File(filePath.toString()); 
				    	file.transferTo(serverFile); 

/*				    		
						out = new FileOutputStream(filePath.toString()); // NOPMD – 초기화를 위한 로직 [ 김상헌 ]
			            BufferedInputStream bis = new BufferedInputStream(file.getInputStream()); // NOPMD – 초기화를 위한 로직 [ 김상헌 ]
			            byte[] buffer = new byte[8106]; // NOPMD – 초기화를 위한 로직 [ 김상헌 ]

			            int read = bis.read(buffer);
			            do{
			            	out.write(buffer, 0, read);
			            	read = bis.read(buffer);
			            }while(read>0);
			            
			            bis.close();
			            out.close();*/

			            
						transactionStatus = true;
					} catch (Exception e) {
						LOG.error(e);
						//e.printStackTrace();
						errorCode = 101;
					}
					
				}else{
					errorCode = 102;
				}
//			}
		}
		
	  //  response.setContentType("text/html; charset=UTF-8");

	    String sh = "\"";
    	String argCon = "";
    	
	    if (transactionStatus ) { 
	   // 	response.getWriter().write("{success:true" + ", filepath:" + sh + filePath.toString() + sh + "}");
	    } else { 
	    	
	    	
	    	if(!argMap.isEmpty()) {
		    	Iterator iterator = argMap.entrySet().iterator();
		    	
		    	while (iterator.hasNext()) {
		    		Entry entry = (Entry)iterator.next();
		    		
		    		String arg = "";
		    		String key = (String)entry.getKey();
		    		Object oVal =  entry.getValue();
		    		String value = "";
		    		
		    		if (oVal instanceof Integer) {
		    			Integer iVal = (Integer)oVal;
		    			value =iVal.toString();
		    		} else {
		    			value = (String)oVal; 
		    		}
	
		    		if (iterator.hasNext() ) { 
			    		arg = sh + key + sh  
			    				+ ":" + sh  
			    				+ value + sh + ",";  
		    			
		    		} else  {
			    		arg = sh + key + sh  
			    				+ ":" + sh  
			    				+ value + sh;  
		    		}
		    		
//		    		argCon += arg;
		    		argCon = argCon.concat(arg); 
		    	}
		/*    	
		    	// RETURN JSON DATA 
//		    	response.getWriter().write("" +
		    	response.getWriter().write(
		    			"{success:false," + sh  
		    			+ CommandMap.COMMAND_COMMON_MESSAGE_CODE + sh 
		    			+  ":" 
		    			+ sh + errorCode +sh 
		    			+ "," + argCon
		    			+ "}"
		    	);
	    	} else {
	    		// RETURN JSON DATA 
		    	//response.getWriter().write("" +
		    	response.getWriter().write(
		    			"{success:false," + sh  
		    			+ CommandMap.COMMAND_COMMON_MESSAGE_CODE + sh 
		    			+  ":" 
		    			+ sh + errorCode +sh 
		    			+ "}"
		    	);*/
	    	}
	    }
	    
        
        jsonObject.put("success", true);
        
        return jsonObject;
    } 
    
    
    
    /**
     * ADM > 마스터 > 거래처 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/boardController/fileDown.do")
    @ResponseBody   
    public void fileDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
  
    	// JSON parsing fileurl
    	String filepath = EnvConstant.BOARD_FILE_PATH + (String)commandMap.get("fileurl");
    	String filename = (String)commandMap.get("filename");
    	String fileid = (String)commandMap.get("fileid");
    	//한글깨짐 처리
    	filename = new String(filename.getBytes("iso-8859-1"), "UTF-8");
    	filepath = filepath.concat(fileid);
        // MIME Type 을 application/octet-stream 타입으로 변경
        // 무조건 팝업(다운로드창)이 뜨게 된다.
        //response.setContentType("application/octet-stream");
       
        // 브라우저는 ISO-8859-1을 인식하기 때문에
        // UTF-8 -> ISO-8859-1로 디코딩, 인코딩 한다.
        //filename = new String(filename.getBytes("UTF-8"), "iso-8859-1");

        // 파일명 지정
        //response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
    	
    	/**-------------------------------------------------------------------*/
        String str_Browser = CommonUtil.getBrowser();
		
		if(str_Browser.indexOf("MSIE")>-1){
			response.setContentType("doesn/matter;");
		}else{
			response.setContentType("application/vnd.ms-excel;charset=EUC-KR");
		}
		
	    response.setHeader("Content-Disposition", CommonUtil.getDisposition(filename, str_Browser));
	    /**-------------------------------------------------------------------*/
	    
        OutputStream os = response.getOutputStream();
        // String path = servletContext.getRealPath("/resources");
        // d:/upload 폴더를 생성한다.
        // server에 clean을 하면 resources 경로의 것이 다 지워지기 때문에
        // 다른 경로로 잡는다(실제 서버에서는 위의 방식으로)
   
        FileInputStream fis = new FileInputStream(filepath);
        
        byte[] b = new byte[512];
//        int n = 0;
//        while((n = fis.read(b)) != -1 ) {
//            os.write(b, 0, n);
//        }
        
        int n = fis.read(b);
        do{
        	os.write(b, 0, n);
        	n = fis.read(b);
        }while(n != -1);
        
        fis.close();
        os.close();
    }    
    
    
    
}
