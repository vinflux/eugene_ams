package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.UploadColumnManagementService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("uploadColumnManagementController")
@RemotingDestination
public class UploadColumnManagementController extends CommandController {

	@Resource(name = "uploadColumnManagementService")
	private UploadColumnManagementService uploadColumnManagementService;
	 
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UploadColumnManagementController.class);
	
    /**
     * ADM > 기본마스터관리 > 업로드 컬럼관리 > GRID헤더 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadColumnManagementController/selectUploadColumnHeaderInfo.do")
    @ResponseBody   
    public CommandMap selectUploadColumnHeaderInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccountInfo__requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map UploadColumnHeaderInfoMap = uploadColumnManagementService.selectUploadColumnHeaderInfo(paramMap);
    	Integer count = MapUtil.getInt(UploadColumnHeaderInfoMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)UploadColumnHeaderInfoMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccountInfo_resultMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 기본마스터관리 > 업로드 컬럼관리 > GRID상세 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadColumnManagementController/selectUploadColumnDetailInfo.do")
    @ResponseBody   
    public CommandMap selectUploadColumnDetailInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectUploadColumnDetailInfo_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map map = uploadColumnManagementService.selectUploadColumnDetailInfo(paramMap);
		requestMap.addList("rtnGrid2", (ArrayList)map.get("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 기본마스터관리 > 업로드 컬럼관리 > GRID헤더 수정 체크
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadColumnManagementController/checkSave.do")
    @ResponseBody   
    public CommandMap checkSave (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map param = requestMap.getParamMap();
    	Map duplicatioMap = new HashedMap();
    	int count = uploadColumnManagementService.checkSave(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check dickey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 기본마스터관리 > 업로드 컬럼관리 > GRID헤더 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadColumnManagementController/saveUploadColumnHD.do")
    @ResponseBody   
    public CommandMap saveUploadColumnHD (@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	Map param = requestMap.getParamMap();
      	
      	if ("ALL".equals(param.get("chkAll")) ) {
        	Map paramMap = requestMap.getSearchQuery();
        	paramMap.putAll(param);
        	list = uploadColumnManagementService.selectUploadColumnHDExcel(paramMap);
      	}
      	
    	List returnList = new ArrayList();
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				uploadColumnManagementService.insertUploadColunmHD(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				uploadColumnManagementService.updateUploadColunmHD(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) || "ALL".equals(param.get("chkAll"))) {
    				uploadColumnManagementService.deleteUploadColunmHD(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }
    
    /**
     * ADM > 기본마스터관리 > 업로드 컬럼관리 > GIRD상세 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/uploadColumnManagementController/saveUploadColumnDT.do")
    @ResponseBody   
    public CommandMap saveUploadColumnDT (@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				uploadColumnManagementService.insertUploadColunmDT(inputMap);
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				uploadColumnManagementService.updateUploadColunmDT(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				uploadColumnManagementService.deleteUploadColunmDT(inputMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }    

	/**
	 * ADM > 기본마스터관리 > 업로드 컬럼관리 > Header Gird 엑셀 다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uploadColumnManagementController/excelDownUploadColumnHD.do")
	@ResponseBody
	public void excelDownUploadColumnHD(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		ExcelWriteHandler.downloadExcelFile(uploadColumnManagementService
				.selectUploadColumnHDExcel(parameterMap, excelFormat),
				fileName, response);
	}    
	
	/**
	 * ADM > 기본마스터관리 > 업로드 컬럼관리 > detail Gird 엑셀 다운로드
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/uploadColumnManagementController/excelDownSearchConditionDetail.do")
	@ResponseBody
	public void excelDownSearchConditionDetail(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		ExcelWriteHandler.downloadExcelFile(uploadColumnManagementService
				.excelDownSearchConditionDetail(parameterMap, excelFormat),
				fileName, response);
	}    
    
}
