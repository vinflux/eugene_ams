package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.MasterAdminCodeService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("masterAdminCodeController")
@RemotingDestination
public class MasterAdminCodeController extends CommandController  {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MessageController.class);

	@Resource(name = "masterAdminCodeService")
	private MasterAdminCodeService masterAdminCodeService;
	
    
    @RequestMapping(value="/masterAdminCodeController/selectAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap selectAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);
    	
    	Map itemCodeMap = masterAdminCodeService.selectAdminCodeInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "itemCodeCnt");
    	    	
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("itemCodeList"));
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    @RequestMapping(value="/masterAdminCodeController/selectAdminCode.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap selectAdminCode (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = masterAdminCodeService.selectAdminCode(paramMap);
    	    	
   		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("itemCodeList"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    @RequestMapping(value="/masterAdminCodeController/selectDetailAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap selectDetailAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = masterAdminCodeService.selectDetailAdminCodeInfo(param);
    	Integer count = MapUtil.getInt(itemCodeMap, "itemCodeCnt");
    	    	
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("itemCodeList"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
   
    
    @RequestMapping(value="/masterAdminCodeController/insertAdminCode.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap insertAdminCode (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map formData = requestMap.getMap("FORM_DATA");
    	masterAdminCodeService.insertAdminCodeInfo(formData);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
     
    
    @RequestMapping(value="/masterAdminCodeController/updateAdminCode.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap updateAdminCode (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	masterAdminCodeService.updateAdminCodeInfo(requestMap.getMap("FORM_DATA"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    @RequestMapping(value="/masterAdminCodeController/deleteAdminCode.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap deleteAdminCode (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
      	List list = requestMap.getList("AdminCode");
    	Map param = requestMap.getParamMap();
      	
      	if ("ALL".equals(param.get("chkAll")) ) {
        	Map paramMap = requestMap.getSearchQuery();
        	paramMap.putAll(param);
        	list = masterAdminCodeService.selectExcelDownAdminCode(paramMap);
      	}
      	
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = (Map)list.get(i);
    		masterAdminCodeService.deleteDetailAdminCodeInfo(map, true);
    		masterAdminCodeService.deleteAdminCodeInfo(map);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
 		
    	requestMap.putParam("RESULT", "SUCCESS");
 		
    	return requestMap;
    }
    
    
    @RequestMapping(value="/masterAdminCodeController/insertDetailAdminCode.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap insertDetailAdminCode (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	/*** 각 모듈별 HD CODE 부재시 해당 HD CODE를 먼저 INSERT 해주기 위해 AMS 의 HD CODE 정보를 가져온다. 
    	 *	 1. 해당 모듈의 HD 정보를 INSER OR UPDATE.
    	 *	 2. 해당 모듈의 DT 정보를 모두 DELETE.
    	 *   3. 해당 모듈의 DT 정보를 다시 INSERT.
    	 ****/
    	String adcd_hdkey = (String)requestMap.getMap("FORM_DATA").get("adcd_hdkey");
    	
    	LOG.debug("======================================");
    	LOG.debug("> adcd_hdkey :: " + adcd_hdkey);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	param.put("adcd_hdkey", param.get("adcd_hdkey"));
    	paramMap.putAll(param);    	
    	
    	Map amsHDCodeMapList = masterAdminCodeService.selectAdminCodeInfo(paramMap);
    	Map amsHdCodeMap = new HashMap();
    	Integer count = MapUtil.getInt(amsHDCodeMapList, "itemCodeCnt");
    	if(count > 0){
    		List hdList = (ArrayList)amsHDCodeMapList.get("itemCodeList");
    		amsHdCodeMap = (Map)hdList.get(0);
    	}
    	
    	masterAdminCodeService.insertDetailAdminCodeInfo(requestMap.getMap("FORM_DATA"), amsHdCodeMap);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    @RequestMapping(value="/masterAdminCodeController/updateDetailAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap updateDetailAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	masterAdminCodeService.updateDetailAdminCodeInfo(requestMap.getMap("FORM_DATA"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    

    @RequestMapping(value="/masterAdminCodeController/deleteDetailAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap deleteDetailAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	
      	List list = requestMap.getList("AdminCode");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = (Map)list.get(i);
    		masterAdminCodeService.deleteDetailAdminCodeInfo(map, false);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
 		
    	requestMap.putParam("RESULT", "SUCCESS");
 		
    	return requestMap;
    }
    
    
    @RequestMapping(value="/masterAdminCodeController/checkAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap checkAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("requsetMap adcd_hdkey VALUE=> " + requestMap.getParamMap().get("adcd_hdkey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param);    	
    	
    	int count = masterAdminCodeService.checkAdminCodeInfo(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check adcd_hdkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    @RequestMapping(value="/masterAdminCodeController/checkDetailAdminCodeInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap checkDetailAdminCodeInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map param = requestMap.getParamMap();
    	
    	int count = masterAdminCodeService.checkDetailAdminCodeInfo(param);
    	    	
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check adcd_dtkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    
    @RequestMapping(value="/masterAdminCodeController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = masterAdminCodeService.selectExcelDownAdminCode(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(masterAdminCodeService
				.selectExcelDownAdminCode(parameterMap, excelFormat), fileName,
				response);
    }  
    
    
    @RequestMapping(value="/masterAdminCodeController/excelDownDetail.do")
    @ResponseBody   
    public void excelDownDetail (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = masterAdminCodeService.selectExcelDownDetailAdminCode(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(masterAdminCodeService
				.selectExcelDownDetailAdminCode(parameterMap, excelFormat),
				fileName, response);
    }  
    
    
}
