package com.vinflux.adm.master.web;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.CarrierService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.collections.ListMap;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.util.UbiUtil;
import com.vinflux.framework.web.CommandController;

@Controller("carrierController")
@RemotingDestination
public class CarrierController extends CommandController{

	@Resource(name = "carrierService")
	private CarrierService carrService;
	private static final String RESULT_SUCCESS = "true";
	private static final String RESULT_FAIL	  = "false";
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(CarrierController.class);

	/**
	 *
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/carrierController/selectCarrInfo.do")
	@ResponseBody   
	public CommandMap selectCarrInfo (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("_requsetMap => " + requestMap);
		LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);

		Map carrMap = carrService.selectCarrInfo(paramMap);
		Integer count = MapUtil.getInt(carrMap, "cnt");
		if (count > 0 ) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList)carrMap.get("list"));
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/carrierController/approveCarrList.do")
	@ResponseBody   
	public CommandMap approveCarrList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("approveCarrList_requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		carrService.approveCarrList(list);
    	
		LOG.debug("======================================");
		LOG.debug("approveCarrList_resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/carrierController/checkValidCarrList.do")
	@ResponseBody   
	public CommandMap checkValidCarrList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("checkValidCarrList_requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		int count = carrService.checkValidCarrList(list);

   		if(count > 0){
   			requestMap.putParam("result", RESULT_FAIL);
   		}else{
   			requestMap.putParam("result", RESULT_SUCCESS);
   		}
		LOG.debug("======================================");
		LOG.debug("checkValidCarrList_resultMap => " + requestMap.getParam("result"));
		LOG.debug("======================================");

		return requestMap;
	}
	
    /**
     * 
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/carrierController/report.do")
    public void report (Map<String,Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	LOG.debug("request.getParameter => " + request.getParameter("reportParam"));
    	
    	LOG.debug("======================================");
    	LOG.debug("report_commandMap => " + commandMap);
    	LOG.debug("======================================");

    	String strStream = null;
    	// JSON parsing 
    	String reportParam = (String)commandMap.get("reportParam");
    	
    	if ( StringUtil.isNotEmpty(reportParam) ) {
	    	CommandMap requestMap  = JsonUtil.jsonCovertCommandMap(reportParam);
	    	
	    	LOG.debug("_requsetMap => " + requestMap);
	    	
	    	SessionVO sessVO = SessionManager.getSessionVO(request, (String)requestMap.getCommonUserInfo("urKey"));
	    	SessionManager.setUserInfo(request, sessVO, "Y");
	    	
	    	//Search condition setting 
	    	Map paramMap = requestMap.getSearchQuery();
	    	Map param = requestMap.getParamMap();
	    	paramMap.putAll(param);    	
	    	
	    	//check List 추출.
	    	List keyList = new ArrayList();
	    	Map chkMap = null;
	    	List checkList = requestMap.getList("DATA_LIST");
//	    	if ( checkList!=null && checkList.size()>0 ) {
	    	if ( checkList!=null && !checkList.isEmpty() ) {
	    		for(int i=0; i<checkList.size(); i++){
	    			chkMap = (HashMap)checkList.get(i);
	    			keyList.add((String)chkMap.get("carrcd"));
	    		}
	    		paramMap.put("carrcdList", keyList); 
	    		LOG.debug("### carrcdList => " + keyList);
	    	}
	    	paramMap.put("udsName", (String)commandMap.get("udsName"));
    	    
	    	List<ListMap> list = this.carrService.selectCarrCdReport(paramMap);
	    	//LOG.debug("### list => " + list);
	    	
	    	strStream = UbiUtil.convertDataToStream(list);
	    	// LOG.debug("### strStream => " + strStream);
    	}
    	
    	//strStream = "OBHDKEY^tSUPPLIER_REGNO^tSUPPLIER_OWNM^tSUPPLIER_CEO^tSUPPLIER_ADDRESS1^tSUPPLIER_ADDRESS2^tSUPPLIER_BUSINESSTYPE^tSUPPLIER_BUSINESSCATEGORY^tSUPPLIER_BUSINESSCATEGORY^tBUYER_REGNO^tBUYER_ORDERKEY^tBUYER_ACNAME^tBUYER_ADDRESS1^tBUYER_ADDRESS2^tBUYER_TEL^tBOTTOM_ADDRESS^tBOTTOM_CONTACT^tBOTTOM_COMMENT^t^n";
    	
		response.setContentType("text/html");
		String LANG_ENCODING = "UTF-8";
	    OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), LANG_ENCODING);
	    PrintWriter out = new PrintWriter (writer);
        out.println(strStream);
	    out.close();	
    }

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/carrierController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("excelDown_requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));

    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");

    	// db selsect
//    	List excelList = carrService.selectExcelDownCarr(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				carrService.selectExcelDownCarr(parameterMap, excelFormat),
				fileName, response);
    }

}