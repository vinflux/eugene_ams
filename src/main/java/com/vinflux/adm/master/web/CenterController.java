package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.CenterService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("centerController")
@RemotingDestination
public class CenterController extends CommandController {

	@Resource(name = "centerService")
	private CenterService centerService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(CenterController.class);
	
    /**
     * ADM > 마스터 > 센터 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/selectCenterInfo.do")
    @ResponseBody   
    public CommandMap selectCenterInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = centerService.selectCenterInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 센터 > update 시 refresh  
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/selectCenter.do")
    @ResponseBody   
    public CommandMap selectCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
		requestMap.addList("rtnList", centerService.selectCenter(paramMap));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    
    
    /**
     * ADM > 마스터 > 센터 > 등록
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/insertCenter.do")
    @ResponseBody   
    public CommandMap insertCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	centerService.insertCenter(requestMap.getMap("CENTER"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 센터 > 수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/updateCenter.do")
    @ResponseBody   
    public CommandMap updateCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	centerService.updateCenter(requestMap.getMap("CENTER"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 센터 > 삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/deleteCenter.do")
    @ResponseBody   
    public CommandMap deleteCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = requestMap.getList("DELETE_LIST");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = (Map)list.get(i);
    		centerService.deleteCenter(map);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    /**
     * ADM > 마스터 > 센터 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/centerController/excelDownCenter.do")
    @ResponseBody   
    public void excelDownCenter (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = centerService.selectExcelDownCenter(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				centerService.selectExcelDownCenter(parameterMap, excelFormat),
				fileName, response);
    }      
}