package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.AlertSettingService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("alertSettingController")
@RemotingDestination
public class AlertSettingController extends CommandController {

	@Resource(name = "alertSettingService")
	private AlertSettingService alertSettingService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AlertSettingController.class);    
    
    
    @RequestMapping(value="/alertSettingController/checkAlertSetting.do")
    @ResponseBody   
    public CommandMap checkAlertSetting (@RequestBody CommandMap requestMap) throws Exception {
    	
        Map param = requestMap.getParamMap();
        
        param.putAll(requestMap.getMap("FORM_DATA"));
        
        int count = alertSettingService.checkAlertSetting(param);
        if (count == 0 ) {
            requestMap.putParam("isDuplicate", "false");
        }
        else{
            requestMap.putParam("isDuplicate", "true");
        }

        LOG.debug("======================================");
        LOG.debug("resultMap Size => " + Integer.toString(count));
        LOG.debug("resultMap check urkey => " + requestMap.getParam("isDuplicate"));
        LOG.debug("======================================");

        return requestMap;
    }
    
    /**
     * 
     * \//FIXME
     * @param requestMap
     * @return
     * @throws Exception
     * @author gltkor
     * @since 2014. 3. 25.
     */
    @RequestMapping(value="/alertSettingController/selectAlertSettingHdInfoList.do")
    @ResponseBody
    public CommandMap selectAlertSettingHdInfoList (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);
    	
    	Map alertSettingMap = alertSettingService.selectAlertSettingHdList(paramMap);

    	Integer count = MapUtil.getInt(alertSettingMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)alertSettingMap.get("list"));
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * 
     * \//FIXME
     * @param requestMap
     * @return
     * @throws Exception
     * @author gltkor
     * @since 2014. 3. 25.
     */
    @RequestMapping(value="/alertSettingController/selectAlertSettingDtl.do")
    @ResponseBody
    public CommandMap selectAlertSettingHd (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

		Map resultMap = alertSettingService.selectAlertSettingHd(requestMap.getParamMap());
		requestMap.putMap("FORM_DATA", resultMap);

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * 
     * \//FIXME
     * @param requestMap
     * @return
     * @throws Exception
     * @author gltkor
     * @since 2014. 3. 25.
     */
    @RequestMapping(value="/alertSettingController/saveAlertSettingHd.do")
    @ResponseBody
    public CommandMap saveAlertSettingHd (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("============= alertSettingController.saveAlertSettingDtl=========================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();

    	LOG.debug("★★★ paramMap => " + paramMap);

    	String workType = (String)paramMap.get("workType");
    	Map row = new HashMap();
    	
    	if ("DELETE".equals(workType) ) {
    		List<Map<String, Object>> list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			if(list.get(i).get("seqnum") == null){
    				LOG.debug("seqnum is null");
    			}else{
    				row = list.get(i);
        			row.putAll(paramMap);
        			alertSettingService.deleteAlertSettingHd(row);
    			}
    		}
    	} else if ("SAVE".equals(workType) ) {
    		List<Map<String, Object>> list = requestMap.getList("SAVE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			String seqnum = (String)list.get(i).get("seqnum");
    			if(seqnum == null || "".equals(seqnum)){
    				row = list.get(i);
        			row.putAll(paramMap);
        			alertSettingService.insertAlertSettingHd(row);
    			}else{
    				row = list.get(i);
        			row.putAll(paramMap);
        			alertSettingService.updateAlertSettingHd(row);
    			}
    		}
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }
    
    /**
     * 
     * \//FIXME
     * @param requestMap
     * @return
     * @throws Exception
     * @author gltkor
     * @since 2014. 3. 25.
     */
    @RequestMapping(value="/alertSettingController/selectAlertSettingDtlInfoList.do")
    @ResponseBody
    public CommandMap selectAlertSettingDtlInfoList (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug(this.getClass() + "======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug(this.getClass() + "======================================");

    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);
    	
    	Map eventIdMap = alertSettingService.selectEventid(param);
    	requestMap.addList("rtnGrid1", (ArrayList)eventIdMap.get("list"));
    	
    	Map centerListMap = alertSettingService.selectCenterList(param);
    	requestMap.addList("rtnGrid2", (ArrayList)centerListMap.get("list"));

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

}