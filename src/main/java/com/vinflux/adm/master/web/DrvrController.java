package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.DrvrService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;


@Controller("drvrController")
@RemotingDestination
public class DrvrController extends CommandController {
	@Resource(name = "drvrService")
	private DrvrService drvrService;
	private static final String RESULT_SUCCESS = "true";
	private static final String RESULT_FAIL	  = "false";
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(DrvrController.class);

    /**
     * TMS > 마스터 > 기타 > 운전기사 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/drvrController/selectDrvrList.do")
    @ResponseBody
    public CommandMap selectDrvrList (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug(this.getClass() + "======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug(this.getClass() + "======================================");

    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);

    	Map drvrMap = drvrService.selectDrvrList(paramMap);

    	Integer count = MapUtil.getInt(drvrMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList) drvrMap.get("list"));
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 기타 > 운전기사 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/drvrController/selectDrvr.do")
    @ResponseBody
public CommandMap selectDrvr (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);
		//paramMap.put("SEARCH_DIVLOGGRPCD", TmsMapUtil.setCtLoggrp(requestMap, ""));

		Map resultMap = (Map) drvrService.selectDrvr(paramMap).get(0);
		requestMap.putMap("FORM_DATA", resultMap);

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/drvrController/approveDrvrList.do")
	@ResponseBody   
	public CommandMap approveDrvrList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		drvrService.approveDrvrList(list);
    	
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
    /**
     * TMS > 마스터 > 기타 > 운전기사 > 중복체크
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/drvrController/checkDrvr.do")
    @ResponseBody
    public CommandMap checkDrvr (@RequestBody CommandMap requestMap) throws Exception {

    	Map param = requestMap.getParamMap();

    	int count = drvrService.checkDrvr(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check urkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");

    	return requestMap;
    }

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/drvrController/checkValidDrvrList.do")
	@ResponseBody   
	public CommandMap checkValidDrvrList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		int count = drvrService.checkValidDrvrList(list);

   		if(count > 0){
   			requestMap.putParam("result", RESULT_FAIL);
   		}else{
   			requestMap.putParam("result", RESULT_SUCCESS);
   		}
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap.getParam("result"));
		LOG.debug("======================================");

		return requestMap;
	}
	
    /**
     * TMS > 마스터 > 기타 > 운전기사 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/drvrController/saveDrvr.do")
    @ResponseBody
    public CommandMap saveDrvr (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();

    	LOG.debug("★★★ paramMap => " + paramMap);

    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {					// NOPMD - 추후 구현되어야 할 듯 함.
//    		drvrService.insertDrvr(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		drvrService.updateDrvr(requestMap.getMap("FORM_DATA"));
    	}
    	/*
    	 * 처리내용 주석처리로 처리되지 않음. 전체 주석처리
    	else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);

//    			drvrService.deleteDrvr(row);
    		}
    	}
    	*/

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 기타 > 운전기사 > 엑셀다운
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/drvrController/excelDown.do")
    @ResponseBody
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {

    	LOG.debug(this.getClass() + "======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug(this.getClass() + "======================================");

    	// JSON parsing
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));

    	// Search condition setting
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);

    	// excel parameter setting
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");

    	// db selsect
//    	List excelList = drvrService.selectExcelDown(paramMap);
//
//    	// exceldownload
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				drvrService.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }

}