package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.IcOrdStopService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("icOrdStopController")
@RemotingDestination
public class IcOrdStopController extends CommandController {

	@Resource(name = "icOrdStopService")
	private IcOrdStopService icOrdStopService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountController.class);
	
	private static final String RESULT_SUCCESS = "success";
	private static final String RESULT_FAIL    = "fail";
    /**
     * ADM > 상품 > 센터발주정지 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStop.do")
    @ResponseBody   
    public CommandMap selectIcOrdStop (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = icOrdStopService.selectIcOrdStop(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    //--- DEAD METHOD
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopXct.do")
    @ResponseBody   
    public CommandMap selectIcOrdStopXct (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	List itemCodeMap = icOrdStopService.selectIcOrdStopXct(paramMap);
   		requestMap.addList("rtnGrid", itemCodeMap);
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * 센터발주정지 엑셀 다운로드
     * @param commandMap
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopExcelDown.do")
    @ResponseBody   
    public void selectIcOrdStopExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	List queryList = requestMap.getSearchList();
    	Map param = requestMap.getParamMap();

    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, queryList);
    	Map parameterMap = requestMap.getSearchQuery();    	
    	parameterMap.putAll(param);    	
    	
    	//#####################
//		Map orderMap = etcOrderService.selectSoHd(paramMap);    
    	//##################

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectIcOrdStopExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(icOrdStopService
				.selectIcOrdStopExcelDown(parameterMap, excelFormat), fileName,
				response);
    }
    
    /**
     * 센터 발주정지 : 센터 발주정지  & 해제
     * @param requestMap
     * @return
     * @throws Exception
     */
	@RequestMapping(value="/icOrdStopController/saveTheDateList.do")
	@ResponseBody   
	public CommandMap saveTheDateList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DATA_LIST");
		

//		String result = "";
		String message = "";
		try{
			icOrdStopService.updateIcOrdStop(list);
			requestMap.putCommon("result", RESULT_SUCCESS);
		}catch(Exception ex){
			requestMap.putCommon("result", RESULT_FAIL);
			String[] arrMsg = ex.getMessage().split("\\|"); 
			if(arrMsg != null && arrMsg.length > 1){
				message = arrMsg[1];	
			}else{
				message = ex.getMessage();
			}
			requestMap.putCommon("message", message);
		}
		
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
    @RequestMapping(value="/icOrdStopController/selectPoStopConfirm.do")
    @ResponseBody   
    public CommandMap selectPoStopConfirm (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = icOrdStopService.selectPoStopConfirm(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * 발주정지상품 신청확인 엑셀 다운로드
     * @param commandMap
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectPoStopConfirmExcelDown.do")
    @ResponseBody   
    public void selectPoStopConfirmExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	List queryList = requestMap.getSearchList();
    	Map param = requestMap.getParamMap();

    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, queryList);
    	Map parameterMap = requestMap.getSearchQuery();    	
    	parameterMap.putAll(param);    	
    	
    	//#####################
//		Map orderMap = etcOrderService.selectSoHd(paramMap);    
    	//##################

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectPoStopConfirmExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(icOrdStopService
				.selectPoStopConfirmExcelDown(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * 발주정지상품 신청확인 - 저장
     * @param requestMap
     * @return
     * @throws Exception
     */
	@RequestMapping(value="/icOrdStopController/savePoConfirm.do")
	@ResponseBody   
	public CommandMap savePoConfirm (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DATA_LIST");
		

//		String result = "";
		String message = "";
		try{
			icOrdStopService.updatePoStopConfirm(list);
			requestMap.putCommon("result", RESULT_SUCCESS);
		}catch(Exception ex){
			requestMap.putCommon("result", RESULT_FAIL);
			String[] arrMsg = ex.getMessage().split("\\|"); 
			if(arrMsg != null && arrMsg.length > 1){
				message = arrMsg[1];	
			}else{
				message = ex.getMessage();
			}
			requestMap.putCommon("message", message);
		}
		
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
    
    
    
    
    
    
    //-- DEATH METHOD
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopIcxExcelDown.do")
    @ResponseBody   
    public void selectIcOrdStopIcxExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	List queryList = requestMap.getSearchList();
    	Map param = requestMap.getParamMap();

    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, queryList);
    	Map parameterMap = requestMap.getSearchQuery();    	
    	parameterMap.putAll(param);    	
    	
    	//#####################
//		Map orderMap = etcOrderService.selectSoHd(paramMap);    
    	//##################

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectIcOrdStopXct(paramMap);
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				icOrdStopService.selectIcOrdStopXct(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * 센터 발주정지 : 저장, 수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/saveIcOrdStop.do")
    @ResponseBody  
    public CommandMap saveIcOrdStop (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map formData = requestMap.getMap("FORM_DATA");
    	icOrdStopService.saveIcOrdStop(formData);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    

	
    /**
     * 센터 발주정지 : 발주정지 삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
	@RequestMapping(value="/icOrdStopController/deleteIcOrdStop.do")
	@ResponseBody   
    public CommandMap deleteIcOrdStop (@RequestBody CommandMap requestMap) throws Exception {
    	   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
//		String result = "";
		String message = "";
		
      	List list = requestMap.getList("DATA_LIST");
    	try{
    		icOrdStopService.deleteIcOrdStop(list);
    		requestMap.putCommon("result", RESULT_SUCCESS);
    	}catch(Exception ex){
    		requestMap.putCommon("result", RESULT_FAIL);
			String[] arrMsg = ex.getMessage().split("\\|"); 
			if(arrMsg != null && arrMsg.length > 1){
				message = arrMsg[1];	
			}else{
				message = ex.getMessage();
			}
			requestMap.putCommon("message", message);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
 		
    	return requestMap;
    }
	
    /**
     * ADM > 상품 > 센터발주정지 해제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopRemove.do")
    @ResponseBody   
    public CommandMap selectIcOrdStopRemove (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = icOrdStopService.selectIcOrdStopRemove(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * ADM > 상품 > 센터발주정지 해제 Excel Down
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopRemoveExcelDown.do")
    @ResponseBody   
    public void selectIcOrdStopRemoveExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectIcOrdStopRemoveExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(icOrdStopService
				.selectIcOrdStopRemoveExcelDown(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * ADM > 상품 > 센터발주정지 해제 Excel Down
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopRemoveIcxExcelDown.do")
    @ResponseBody   
    public void selectIcOrdStopRemoveIcxExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectIcOrdStopXctForRemove(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(icOrdStopService
				.selectIcOrdStopXctForRemove(parameterMap, excelFormat),
				fileName, response);
    }
    
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopXctForRemove.do")
    @ResponseBody   
    public CommandMap selectIcOrdStopXctForRemove (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	List itemCodeMap = icOrdStopService.selectIcOrdStopXctForRemove(paramMap);
   		requestMap.addList("rtnGrid", itemCodeMap);
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * 센터 발주정지 해제 : 저장, 수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/saveIcOrdStopRemove.do")
    @ResponseBody  
    public CommandMap saveIcOrdStopRemove (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map formData = requestMap.getMap("FORM_DATA");
    	icOrdStopService.saveIcOrdStopRemove(formData);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 상품 > 점포 발주정지 대상 현황 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopStatus.do")
    @ResponseBody   
    public CommandMap selectIcOrdStopStatus(@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = icOrdStopService.selectIcOrdStopStatus(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    @RequestMapping(value="/icOrdStopController/selectIcOrdStopStatusExcelDown.do")
    @ResponseBody   
    public void selectIcOrdStopStatusExcelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	List queryList = requestMap.getSearchList();
    	Map param = requestMap.getParamMap();

    	requestMap.put(CommandMap.COMMAND_SEARCHLIST, queryList);
    	Map parameterMap = requestMap.getSearchQuery();    	
    	parameterMap.putAll(param);    	
    	
    	//#####################
//		Map orderMap = etcOrderService.selectSoHd(paramMap);    
    	//##################

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icOrdStopService.selectIcOrdStopStatusExcelDown(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(icOrdStopService
				.selectIcOrdStopStatusExcelDown(parameterMap, excelFormat),
				fileName, response);
    }
}
