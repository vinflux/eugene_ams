package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.IcxctService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("icxctController")
@RemotingDestination
public class IcxctController extends CommandController {

	@Resource(name = "icxctService")
	private IcxctService icxctService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(IcxctController.class);
	
    /**
     * ADM > 마스터 > 센터/거래처/상품마스터 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/selectIcxctInfo.do")
    @ResponseBody   
    public CommandMap selectIcxctInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = icxctService.selectIcxctInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > PK Check , Update Check   
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/selectIcxct.do")
    @ResponseBody   
    public CommandMap selectIcxct (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
		requestMap.addList("rtnList", icxctService.selectIcxct(requestMap.getParamMap()));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    
    /**
     * ADM > 마스터 > 진열집기 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/checkIcxct.do")
    @ResponseBody   
    public CommandMap checkIcxctCount (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("requsetMap icxctkey VALUE=> " + requestMap.getParamMap().get("icxctkey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param); 	
    	
    	int count = icxctService.checkIcxctCount(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check icxctkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/saveIcxct.do")
    @ResponseBody   
    public CommandMap saveIcxct (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		icxctService.insertIcxct(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		icxctService.updateIcxct(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			icxctService.deleteIcxct(row);
    		}
    	}
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = icxctService.selectExcelDownIcxct(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
    	
		ExcelWriteHandler.downloadExcelFile(
				icxctService.selectExcelDownIcxct(parameterMap, excelFormat),
				fileName, response);
    }
    
    
    /**
     * ADM > 마스터 > 센터/거래처/상품마스터 > 일괄수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/updateAllIcxct.do")
    @ResponseBody   
    public CommandMap updateAllIcxct (@RequestBody CommandMap requestMap) throws Exception {
    	   	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	    	
    	icxctService.updateAllIcxct(paramMap);
    	    	   	
    	return requestMap;
    }
    /**
     * ADM > 마스터 > 센터/거래처/상품마스터 > 일괄수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/icxctController/updateChkIcxct.do")
    @ResponseBody   
    public CommandMap updateChkIcxct (@RequestBody CommandMap requestMap) throws Exception {
    	   	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	    	
    	icxctService.updateChkIcxct((HashMap) paramMap);
    	    	   	
    	return requestMap;
    }
}
