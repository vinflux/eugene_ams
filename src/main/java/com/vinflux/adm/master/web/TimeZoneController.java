package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.TimeZoneService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("timeZoneController")
@RemotingDestination
public class TimeZoneController extends CommandController {

	@Resource(name = "timeZoneService")
	private TimeZoneService timeZoneService;

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(TimeZoneController.class);

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 검색
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/selectTimezoneHd.do")
	@ResponseBody
	public CommandMap selectTimezoneHd(@RequestBody CommandMap requestMap) throws Exception {

		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);

		Map timeZoneMap = timeZoneService.selectTimezoneHd(paramMap);
		Integer count = MapUtil.getInt(timeZoneMap, "cnt");

		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList) timeZoneMap.get("list"));
		}

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 검색
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/selectTimezoneDtl.do")
	@ResponseBody
	public CommandMap selectTimezoneDtl(@RequestBody CommandMap requestMap) throws Exception {

		Map param = requestMap.getParamMap();
		Map timeZoneMap = timeZoneService.selectTimezoneDtl(param);
		Integer count = MapUtil.getInt(timeZoneMap, "cnt");

		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList) timeZoneMap.get("list"));
		}
		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 검색
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/checkTimezoneInfoHd.do")
	@ResponseBody
	public CommandMap checkTimezoneInfoHd(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("requsetMap tzkey VALUE=> " + requestMap.getParamMap().get("tzkey"));
		LOG.debug("======================================");

		Map param = requestMap.getParamMap();

		int count = timeZoneService.checkTimezoneInfoHd(param);

		if (count == 0) {
			requestMap.putParam("isDuplicate", "false");
		}
		else {
			requestMap.putParam("isDuplicate", "true");
		}

		LOG.debug("======================================");
		LOG.debug("resultMap Size => " + Integer.toString(count));
		LOG.debug("resultMap check tzkey => " + requestMap.getParam("isDuplicate"));
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 검색
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/checkTimezoneInfoDtl.do")
	@ResponseBody
	public CommandMap checkTimezoneInfoDtl(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("requsetMap tzkey VALUE=> " + requestMap.getParamMap().get("tzkey"));
		LOG.debug("requsetMap continentcode VALUE=> " + requestMap.getParamMap().get("continentcode"));
		LOG.debug("requsetMap countrycode VALUE=> " + requestMap.getParamMap().get("countrycode"));
		LOG.debug("requsetMap citycode VALUE=> " + requestMap.getParamMap().get("citycode"));
		LOG.debug("======================================");

		Map param = requestMap.getParamMap();

		int count = timeZoneService.checkTimezoneInfoDtl(param);

		if (count == 0) {
			requestMap.putParam("isDuplicate", "false");
		}
		else {
			requestMap.putParam("isDuplicate", "true");
		}

		LOG.debug("======================================");
		LOG.debug("resultMap Size => " + Integer.toString(count));
		LOG.debug("resultMap check tz_hdkey => " + requestMap.getParam("isDuplicate"));
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 생성
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/insertTimezoneInfoHd.do")
	@ResponseBody
	public CommandMap insertTimezoneInfoHd(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		timeZoneService.insertTimezoneInfoHd(requestMap.getMap("TZDATA"));

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 생성
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/insertDetailOrderCode.do")
	@ResponseBody
	public CommandMap insertTimezoneInfoDtl(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		timeZoneService.insertTimezoneInfoDtl(requestMap.getMap("TZDATA"));

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 갱신
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/updateTimezoneInfoHd.do")
	@ResponseBody
	public CommandMap updateTimezoneInfoHd(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");
		timeZoneService.updateTimezoneInfoHd(requestMap.getMap("TZDATA"));

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 갱신
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/updateTimezoneInfoDtl.do")
	@ResponseBody
	public CommandMap updateTimezoneInfoDtl(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		timeZoneService.updateTimezoneInfoDtl(requestMap.getMap("TZDATA"));

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 삭제
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/deleteTimezoneInfo.do")
	@ResponseBody
	public CommandMap deleteTimezoneInfo(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("TZDATA");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			LOG.debug("======================================");
			LOG.debug("requsetMap tzkey VALUE          => " + map.get("tzkey"));
			LOG.debug("requsetMap continentcode VALUE  => " + map.get("continentcode"));
			LOG.debug("requsetMap countrycode VALUE    => " + map.get("countrycode"));
			LOG.debug("requsetMap citycode VALUE       => " + map.get("citycode"));
			LOG.debug("======================================");

			timeZoneService.deleteTimezoneInfoDtl(map, true);
			timeZoneService.deleteTimezoneInfoHd(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		requestMap.putParam("RESULT", "SUCCESS");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 삭제
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/deleteTimezoneInfoDtl.do")
	@ResponseBody
	public CommandMap deleteTimezoneInfoDtl(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("TZDATA");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			LOG.debug("======================================");
			LOG.debug("requsetMap tzkey VALUE          => " + map.get("tzkey"));
			LOG.debug("requsetMap continentcode VALUE  => " + map.get("continentcode"));
			LOG.debug("requsetMap countrycode VALUE    => " + map.get("countrycode"));
			LOG.debug("requsetMap citycode VALUE       => " + map.get("citycode"));
			LOG.debug("======================================");

			timeZoneService.deleteTimezoneInfoDtl(map, false);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		requestMap.putParam("RESULT", "SUCCESS");

		return requestMap;
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 엑셀다운(상단)
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/excelDown.do")
	@ResponseBody
	public void excelDown(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");

		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = timeZoneService.selectTimzoneHdExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler
				.downloadExcelFile(timeZoneService.selectTimzoneHdExcel(
						parameterMap, excelFormat), fileName, response);
	}

	/**
	 * ADM > 기본 마스터 관리 > 표준시간대 > 엑셀다운(하단)
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/timeZoneController/excelDownDtl.do")
	@ResponseBody
	public void excelDownDtl(Map<String, Object> commandMap, HttpServletResponse response) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + commandMap);
		LOG.debug("======================================");
		// JSON parsing 
		CommandMap requestMap = ExcelUtil.jsonCovertCommandMap((String) commandMap.get("xlsParam"));

		// Search condition setting 
		Map parameterMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		parameterMap.putAll(param);

		// excel parameter setting 
		List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
		String fileName = (String) param.get("EXCEL_FILENAME");

		// db selsect
//		List excelList = timeZoneService.selectTimzoneDtlExcel(paramMap);
//
//		// exceldownload 
//		ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName, response);
		ExcelWriteHandler.downloadExcelFile(timeZoneService
				.selectTimzoneDtlExcel(parameterMap, excelFormat), fileName,
				response);
	}
}
