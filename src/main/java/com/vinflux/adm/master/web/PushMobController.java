package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.PushMobService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("pushMobController")
@RemotingDestination
public class PushMobController extends CommandController {

	@Resource(name = "pushMobService")
	private PushMobService pushMobService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountController.class);
	
    /**
     * ADM > 기본마스터관리 > 푸시 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/pushMobController/selectPushInfo.do")
    @ResponseBody   
    public CommandMap selectPushInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectPushInfo requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = pushMobService.selectPushInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectPushInfo resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    

    
}
