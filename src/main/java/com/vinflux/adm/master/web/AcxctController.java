package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.AcxctService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("acxctController")
@RemotingDestination
public class AcxctController extends CommandController {

	@Resource(name = "acxctService")
	private AcxctService acxctService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AcxctController.class);
	
    /**
     * ADM > 마스터 > 거래처별센터 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/selectAcxctInfo.do")
    @ResponseBody   
    public CommandMap selectAcxctInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAcxctInfo_requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = acxctService.selectAcxctInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectAcxctInfo_requsetMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 거래처별센터 (detail)> 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/selectDetailAcxctInfo.do")
    @ResponseBody   
    public CommandMap selectDetailAcxctInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectDetailAcxctInfo_requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = acxctService.selectDetailAcxctInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectDetailAcxctInfo_requsetMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > PK Check , Update Check   
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/selectAcxct.do")
    @ResponseBody   
    public CommandMap selectAcxct (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAcxct_requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
		requestMap.addList("rtnList", acxctService.selectAcxct(requestMap.getParamMap()));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectAcxct_requsetMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    
    /**
     * ADM > 마스터 > 진열집기 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/checkAcxct.do")
    @ResponseBody   
    public CommandMap checkAcxctCount (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("checkAcxctCount_requsetMap => " + requestMap);
    	LOG.debug("requsetMap acxctkey VALUE=> " + requestMap.getParamMap().get("acxctkey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param); 	
    	
    	int count = acxctService.checkAcxctCount(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check acxctkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/saveAcxct.do")
    @ResponseBody   
    public CommandMap saveAcxct (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("saveAcxct_requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		acxctService.insertAcxct(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		acxctService.updateAcxct(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			row.put("owkey", paramMap.get("owkey"));
    			acxctService.deleteAcxct(row);
    		}
    	}
    	LOG.debug("======================================");
    	LOG.debug("saveAcxct_resultMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 거래처별센터 > 헤더엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("excelDown_requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = acxctService.selectExcelDownAcxct(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				acxctService.selectExcelDownHerderAcxct(parameterMap, excelFormat),
				fileName, response);
    }
    
    /**
     * ADM > 마스터 > 거래처별센터 > 디테일엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/excelDownDetail.do")
    @ResponseBody   
    public void excelDownDetail (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("excelDownDetail_requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);   
    	
    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = acxctService.selectExcelDownDetailAcxct(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(acxctService
				.selectExcelDownDetailAcxct(parameterMap, excelFormat),
				fileName, response);
    } 
    
    /**
     * ADM > 마스터 > 거래처별센터 > 전체엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/acxctController/excelDownAll.do")
    @ResponseBody   
    public void excelDownAll (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("excelDownAll_requsetMap => " + commandMap);
    	LOG.debug("======================================");
 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
		ExcelWriteHandler.downloadExcelFile(
				acxctService.selectExcelDownAllAcxct(parameterMap, excelFormat),
				fileName, response);
    }    
}
