package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.AccountService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("accountController")
@RemotingDestination
public class AccountController extends CommandController {

	@Resource(name = "accountService")
	private AccountService accountService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountController.class);
	
    /**
     * ADM > 마스터 > 거래처 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/selectAccountInfo.do")
    @ResponseBody   
    public CommandMap selectAccountInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccountInfo__requsetMap1 => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = accountService.selectAccountInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccountInfo_resultMap2 => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 거래처 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/selectAccount.do")
    @ResponseBody   
    public CommandMap selectAccount (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map map = accountService.selectAccount(paramMap);
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 거래처 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/saveAccount.do")
    @ResponseBody   
    public CommandMap saveAccount (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		accountService.insertAccount(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		accountService.updateAccount(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			accountService.deleteAccount(row);
    		}
    	} 
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * 화주변경 일괄저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/saveOwAccount.do")
    @ResponseBody   
    public CommandMap saveOwAccount (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
   		accountService.insertOwAccount(requestMap);
    	return requestMap;
    }    
    
    /**
     * ADM > 마스터 > 거래처 > 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/selectAccountCenter.do")
    @ResponseBody   
    public CommandMap selectAccountCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	requestMap.addList("rtnGrid1", accountService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", accountService.selectTAdminMstAcxct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 거래처 > 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/saveAccountCenter.do")
    @ResponseBody   
    public CommandMap saveAccountCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
/*    	List<Map> list = requestMap.getList("list");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) {
    		Map map = list.get(i);
    		accountService.saveAccountCenter(map);
    	}
*/    	
    	accountService.saveAccountCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 마스터 > 거래처 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = accountService.selectExcelDownAccount(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				accountService.selectExcelDownAccount(parameterMap,excelFormat), fileName,
				response);
    }
    
    /**
     * ADM > 마스터 > 거래처 > 등록,수정 > 대표거래처 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/accountController/selectRepAccountInfo.do")
    @ResponseBody   
    public CommandMap selectRepAccountInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = accountService.selectRepAccountInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
}
