package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.ExchangeRateRepoService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.web.CommandController;

@Controller("exchangeRateRepoController")
@RemotingDestination
public class ExchangeRateRepoController extends CommandController {

	@Resource(name="exchangeRateRepoService")
	private ExchangeRateRepoService exchangeRateRepoService;
	
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(ExchangeRateRepoController.class) ;
	
	
	/**
	 * AMS > 기본 마스터 관리 > 환율정보
	 * @param requestMap
     * @return
     * @throws Exception 
	 */
	@RequestMapping(value="/exchangeRateRepoController/selectExchangeRateRepoList.do")
	@ResponseBody
	public CommandMap selectExchangeRateRepoList (@RequestBody CommandMap requestMap) throws Exception {
		
		LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		List queryList = requestMap.getSearchList();

		if (queryList != null && !queryList.isEmpty()) {
			for (int i = 0; queryList.size() > i; i++) {
				Map row = (Map) queryList.get(i);
				String dbColoum = MapUtil.getStr(row, "dbColoum");
				if ("YYMMDD".equals(dbColoum)) {
					String fromVal = MapUtil.getStr(row, "fromVal");
					String toVal = MapUtil.getStr(row, "toVal");
					if( StringUtil.isNotEmpty(fromVal) && StringUtil.isNotEmpty(toVal) ) {
						param.put("fromVal", fromVal.substring(0,8));
						param.put("toVal", toVal.substring(0,8));
					}
					queryList.remove(i);
				}
			}
		}
    
		requestMap.put(CommandMap.COMMAND_SEARCHLIST, queryList);
		paramMap = requestMap.getSearchQuery();		
		paramMap.putAll(param);
		
		
		
		
		Map resultMap = exchangeRateRepoService.selectExchangeRateRepoList(paramMap);
		Integer count = MapUtil.getInt(resultMap, "cnt");
		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList)resultMap.get("list"));
		}
		
		LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
		return requestMap;
		
	}
	    
	 /**
     * ADM > 환율정보 >  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/exchangeRateRepoController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));

    	// Search condition setting 
		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		List queryList = requestMap.getSearchList();
		paramMap.put("YYMMDD_TO",paramMap.get("YYMMDD_TO").toString().substring(0,8));
		paramMap.put("YYMMDD_FROM",paramMap.get("YYMMDD_FROM").toString().substring(0,8));
		paramMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = erpUserService.selectExcelDownUser(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				exchangeRateRepoService.selectExcelDownUser(paramMap, excelFormat),
				fileName, response);
    }    
	
}
