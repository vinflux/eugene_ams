package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.master.service.MessageService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("messageController")
@RemotingDestination
public class MessageController extends CommandController {

	@Resource(name = "messageService")
	private MessageService messageService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MessageController.class);
	
    /**
     * ADM > 마스터 > 메시지 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/selectMessageInfo.do")
    @ResponseBody   
    public CommandMap selectMessageInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = messageService.selectMessageInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 메시지 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/selectMessage.do")
    @ResponseBody   
    public CommandMap selectMessage (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String apkeyList[] = MapUtil.getStr(paramMap, "apkey").split(",");  
    	if(apkeyList.length > 1 ) {
    		paramMap.put("apkeylist", apkeyList);
    		List list = messageService.selectMessageByApKeyList(paramMap);
    		if( list!=null && !list.isEmpty() ) {
    			requestMap.putMap("rtnMap", (Map)list.get(0));
    		} 
    	} else {
	    	Map map = messageService.selectMessage(paramMap);
			requestMap.putMap("rtnMap", map);
    	}
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 메시지 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/saveMessage.do")
    @ResponseBody   
    public CommandMap saveMessage (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	Map messageMap = requestMap.getMap("MESSAGE");
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		String apkeyList[] = MapUtil.getStr(messageMap, "apkey").split(",");
    		for(int i=0; i<apkeyList.length; i++) {
    			messageMap.put("apkey", apkeyList[i].trim());
    			messageService.insertMessage(messageMap);
    		}
    	} else if ("UPDATE".equals(workType) ) {
    		String apkeyList[] = MapUtil.getStr(messageMap, "apkey").split(",");
    		for(int i=0; i<apkeyList.length; i++) {
    			messageMap.put("apkey", apkeyList[i].trim());
    			messageService.updateMessage(messageMap);
    		}
    		
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			messageService.deleteMessage(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    @RequestMapping(value="/messageController/saveMessageList.do")
    @ResponseBody   
    public CommandMap saveMessageList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	List list = requestMap.getList("MESSAGE_LIST");
    	List returnList = new ArrayList();
    	if(list != null){
    		Map map;
    		for(int i = 0 ; i < list.size() ; i++){
    			Map messageMap = (Map)list.get(i);
    			
    			//if (DataSet.ROW_TYPE_NAME_INSERTED.equals(messageMap.get("rowTypeName")) ) {
    			if ("C".equals(messageMap.get("STATUS")) ) {
	    				map = messageService.selectMessage(messageMap);
	    				if(map == null || map.get("displaymessage") == null || "".equals(map.get("displaymessage"))){
	    					messageService.insertMessage(messageMap);
	    				}else{
	    					throw new MsgException(30018);
	    					//returnList.add(map);
	    				}
    			//}else if (DataSet.ROW_TYPE_NAME_UPDATED.equals(messageMap.get("rowTypeName")) ) {
    			}else if ("U".equals(messageMap.get("STATUS")) ) {
    				
    				messageService.updateMessage(messageMap);
    			//}else if (DataSet.ROW_TYPE_NAME_DELETED.equals(messageMap.get("rowTypeName")) ) {
    			}else if ("D".equals(messageMap.get("STATUS")) ) {
    				messageService.deleteMessage(messageMap);
    			}
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 메시지 > 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/selectMessageCenter.do")
    @ResponseBody   
    public CommandMap selectMessageCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	requestMap.addList("rtnGrid1", messageService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", messageService.selectTAdminMsgMulappmsgxct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 메시지 > 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/saveMessageCenter.do")
    @ResponseBody   
    public CommandMap saveMessageCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
/*    	List<Map> list = requestMap.getList("list");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) {
    		Map map = list.get(i);
    		messageService.saveMessageCenter(map);
    	}
*/    	
    	messageService.saveMessageCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 마스터 > 메시지 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/messageController/excelDownMessage.do")
    @ResponseBody   
    public void excelDownMessage (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = messageService.selectExcelDownMessage(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(messageService
				.selectExcelDownMessage(parameterMap, excelFormat), fileName,
				response);
    }      
    
}
