package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.master.service.ApplicationService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("applicationController")
@RemotingDestination
public class ApplicationController extends CommandController {
	
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ApplicationController.class);
	
    /**
     * ADM > 마스터 > 어플리케이션 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/applicationController/selectApplicationInfoList.do")
    @ResponseBody   
    public CommandMap selectApplicationInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = applicationService.selectApplicationInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 어플리케이션 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/applicationController/selectApplicationInfo.do")
    @ResponseBody   
    public CommandMap selectApplicationInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = applicationService.selectApplication(requestMap.getParamMap());
    	if (list != null && !list.isEmpty() ) {
    		requestMap.putMap("FORM_DATA", (Map)list.get(0));	
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    /**
     * ADM > 마스터 > 어플리케이션 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/applicationController/checkApplicationInfo.do")
    @ResponseBody   
    public CommandMap checkApplicationInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + requestMap);
    	LOG.debug("_requsetMap apkey VALUE=> " + requestMap.getParamMap().get("apkey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param);    	
    	
    	int count = applicationService.checkApplicationInfo(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check apkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

	/**
	 * ADM > 마스터 > 어플리케이션 > 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/applicationController/saveApplicationInfo.do")
	@ResponseBody
	public CommandMap saveApplicationInfo(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		String sMsg = "";
    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				int count = applicationService.checkApplicationInfo(inputMap);
    				
    				if(count == 0){
    					applicationService.insertApplication(inputMap);
    				}else{
    					/*if(i == 0) sMsg = "" + inputMap.get("apkey");
    					else sMsg += "/" + inputMap.get("apkey");*/
    					throw new MsgException(10028);
    				}
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				applicationService.updateApplication(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				applicationService.deleteApplication(inputMap);
    			}
    		}
    		
    		if(!"".equals(sMsg)){
				requestMap.setErrCode("-30");
				requestMap.setMessage(sMsg);
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    
    /**
     * ADM > 마스터 > 어플리케이션 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/applicationController/saveApplication.do")
    @ResponseBody   
    public CommandMap saveApplication (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		applicationService.insertApplication(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		applicationService.updateApplication(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			applicationService.deleteApplication(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
  
    /**
     * ADM > 마스터 > 거래처 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/applicationController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = applicationService.selectExcelDownApplication(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				applicationService.selectExcelDownApplication(parameterMap, excelFormat),
				fileName, response);
    }    
    
}
