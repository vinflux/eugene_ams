package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.CalendarService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("calendarController")
@RemotingDestination
public class CalendarController extends CommandController {

	@Resource(name = "calendarService")
	private CalendarService calendarService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(CalendarController.class);
	
    /**
     * ADM > 관리자 마스터 > 캘린더 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/selectCalendarInfo.do")
    @ResponseBody   
    public CommandMap selectCalendarInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = calendarService.selectCalendarInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 관리자 마스터 > 캘린더 > update 시 refresh  
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/selectCalendar.do")
    @ResponseBody   
    public CommandMap selectCalendar (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
		requestMap.putMap("rtnMap", calendarService.selectCalendar(paramMap));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    

    /**
     * ICOM > ATOMY > 고객 관리 > 클레임 > 하단 그리드 조회 
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/saveCalendar.do")
    @ResponseBody   
    public CommandMap saveCalendar (@RequestBody CommandMap requestMap) throws Exception {
 	    
 		LOG.debug("======================================");
 		LOG.debug("requsetMap => " + requestMap);
 		LOG.debug("======================================");

 		Map paramMap = requestMap.getParamMap();
 		String workType = (String) paramMap.get("workType");
 		if ("INSERT".equals(workType)) {
 			Map formMap = requestMap.getMap("FORM_DATA");
 			calendarService.insertCalendar(formMap);
 		} else if ("UPDATE".equals(workType)) {
 			Map formMap = requestMap.getMap("FORM_DATA");
 			calendarService.updateCalendar(formMap);
 		} else if ("DELETE".equals(workType)) {
 			List list = requestMap.getList("DATA_LIST");
 			int size = list.size();
 			for (int i=0 ; size > i ; i++ ) {
 				Map row = (Map)list.get(i);
 	 			calendarService.deleteCalendar(row);
 			}
 		} 
 		
 		LOG.debug("======================================");
 		LOG.debug("resultMap => " + requestMap);
 		LOG.debug("======================================");

 		return requestMap;	   
    }    
    
    /**
     * ADM > 관리자 마스터 > 캘린더 > 등록
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/insertCalendar.do")
    @ResponseBody   
    public CommandMap insertCalendar (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	calendarService.insertCalendar(requestMap.getMap("CENTER"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 관리자 마스터 > 캘린더 > 수정
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/updateCalendar.do")
    @ResponseBody   
    public CommandMap updateCalendar (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	calendarService.updateCalendar(requestMap.getMap("CENTER"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 관리자 마스터 > 캘린더 > 삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/deleteCalendar.do")
    @ResponseBody   
    public CommandMap deleteCalendar (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = requestMap.getList("DELETE_LIST");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = (Map)list.get(i);
    		calendarService.deleteCalendar(map);
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    /**
     * ADM > 관리자 마스터 > 캘린더 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/calendarController/excelDownCalendar.do")
    @ResponseBody   
    public void excelDownCalendar (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = calendarService.selectExcelDownCalendar(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(calendarService
				.selectExcelDownCalendar(parameterMap, excelFormat), fileName,
				response);
    }      
}