package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.VehicleService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("vehicleController")
@RemotingDestination
public class VehicleController extends CommandController {

	@Resource(name = "vehicleService")
	private VehicleService vehicleService;
	private static final String RESULT_SUCCESS = "true";
	private static final String RESULT_FAIL	  = "false";
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(VehicleController.class);

    /**
     * TMS > 마스터 > 요율 > 장비 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/selectEqmtList.do")
    @ResponseBody
    public CommandMap selectEqmtList (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug(this.getClass() + "======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug(this.getClass() + "======================================");

    	Map paramMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);

    	Map tffEqmtCodeMap = vehicleService.selectEqmtList(paramMap);

    	Integer count = MapUtil.getInt(tffEqmtCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)tffEqmtCodeMap.get("list"));
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/selectEqmt.do")
    @ResponseBody
    public CommandMap selectEqmt (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	List list = vehicleService.selectEqmt(requestMap.getParamMap());
    	if ( list != null && !list.isEmpty() ) {
    		requestMap.putMap("FORM_DATA", (Map)list.get(0));
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }
    
	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/vehicleController/checkValidVehicleList.do")
	@ResponseBody   
	public CommandMap checkValidVehicleList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		int count = vehicleService.checkValidEqmtList(list);

   		if(count > 0){
   			requestMap.putParam("result", RESULT_FAIL);
   		}else{
   			requestMap.putParam("result", RESULT_SUCCESS);
   		}
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap.getParam("result"));
		LOG.debug("======================================");

		return requestMap;
	}

	/**
	 * 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */  
	@RequestMapping(value="/vehicleController/approveVehicleList.do")
	@ResponseBody   
	public CommandMap approveVehicleList (@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("APPROVAL_LIST");
   		vehicleService.approveEquipList(list);
    	
		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		return requestMap;
	}
	
    /**
     * TMS > 마스터 > 요율 > 장비 > 중복체크
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/checkEqmt.do")
    @ResponseBody
    public CommandMap checkEqmt (@RequestBody CommandMap requestMap) throws Exception {

    	Map param = requestMap.getParamMap();

    	int count = vehicleService.checkEqmt(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check urkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/saveEqmt.do")
    @ResponseBody
    public CommandMap saveEqmt (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();

    	LOG.debug("★★★ paramMap => " + paramMap);

    	String workType = (String)paramMap.get("workType");

    	if ("INSERT".equals(workType) ) {
    		vehicleService.insertEqmt(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		vehicleService.updateEqmt(requestMap.getMap("FORM_DATA"));
    	}
    	/*
		 * 사용되지 않는 부분으로 우선 주석처리 [ sw.yoo - 2015.12.23 ]
    	else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);

    			//vehicleService.deleteEqmt(row);
    		}
    	}
		*/
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 엑셀다운
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/excelDown.do")
    @ResponseBody
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {

    	LOG.debug(this.getClass() + "======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug(this.getClass() + "======================================");

    	// JSON parsing
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));

    	// Search condition setting
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);

    	// excel parameter setting
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");

    	// db selsect
//    	List excelList = vehicleService.selectExcelDown(paramMap);
//
//    	// exceldownload
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				vehicleService.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 검색 DTL
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/selectEqmtDtlList.do")
    @ResponseBody
    public CommandMap selectEqmtDtlList (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	List list = vehicleService.selectEqmtDtlList(requestMap.getParamMap());

		requestMap.addList("rtnGrid", list);

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }
    
    @RequestMapping(value="/vehicleController/selectEqmtDtl.do")
    @ResponseBody
    public CommandMap selectEqmtDtl (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	List list = vehicleService.selectEqmtDtl(requestMap.getParamMap());
    	if (list != null && !list.isEmpty() ) {
    		requestMap.putMap("FORM_DATA", (Map)list.get(0));
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}

		requestMap.addList("rtnGrid", list);

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 중복체크 DTL
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/checkEqmtDtl.do")
    @ResponseBody
    public CommandMap checkEqmtDtl (@RequestBody CommandMap requestMap) throws Exception {

    	Map param = requestMap.getParamMap();

    	int count = vehicleService.checkEqmtDtl(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check key => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");

    	return requestMap;
    }

    /**
     * TMS > 마스터 > 요율 > 장비 > 등록, 삭제 DTL
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/vehicleController/saveEqmtDtl.do")
    @ResponseBody
    public CommandMap saveEqmtDtl (@RequestBody CommandMap requestMap) throws Exception {

    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	Map paramMap = requestMap.getParamMap();

    	LOG.debug("★★★ paramMap => " + paramMap);

    	String workType = (String)paramMap.get("workType");

    	if ("INSERT".equals(workType) ) {
    		vehicleService.insertEqmtDtl(requestMap.getMap("FORM_DATA"));
    	}else if ("UPDATE".equals(workType) ) {
    		vehicleService.updateEqmtDtl(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);

    			vehicleService.deleteEqmtDtl(row);
    		}
    	}

    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;
    }

}
