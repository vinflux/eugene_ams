package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.EquipmentService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("equipmentController")
@RemotingDestination
public class EquipmentController extends CommandController {
	
	

	@Resource(name = "equipmentService")
	private EquipmentService equipmentService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(EquipmentController.class);
    
    
	
    /**
     * ADM > 마스터 >  장비> 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/selectEquipment.do")
    @ResponseBody   
    public CommandMap selectEquipment (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map resultMap = equipmentService.selectEquipment(paramMap);

    	//건수 가져오기
    	Integer count = MapUtil.getInt(resultMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count)); 		
    		requestMap.addList("rtnGrid", (ArrayList)resultMap.get("list"));
    	}else{
    		requestMap.putParam("COUNT", "0"); 
    	}

    	//int cnt = equipmentService.selectTAdminMstEqCnt(param);
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 >  장비> 검색 for Update
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/selectEquipmentForUpdate.do")
    @ResponseBody   
    public CommandMap selectEquipmentForUpdate (@RequestBody CommandMap requestMap) throws Exception {
       
    	Map paramMap = requestMap.getParamMap();    	
    	
    	int pkCnt = this.equipmentService.selectTAdminMstEqCnt(paramMap);
    	
    	if (pkCnt < 1) {
    		throw new MsgException(10003); //선택된 정보가 존재하지 않아 수정할 수 없습니다.
    	} else {
    		Map rtnMap = this.equipmentService.selectPkTAdminMstEq(paramMap);
        	requestMap.putMap("rtnMap", rtnMap);
    	}

    	return requestMap;
    }

    /**
     * ADM > 마스터 >  장비> 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/saveEquipment.do")
    @ResponseBody   
    public CommandMap saveEquipment (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		equipmentService.saveEquipment(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		equipmentService.saveEquipment(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			equipmentService.deleteEquipment(row);
    		}
    	}
    	
    	
    	return requestMap;
    }
    
    
    
    /**
     * ADM > 마스터 >  장비> 하단 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/selectEquipmentCenter.do")
    @ResponseBody   
    public CommandMap selectEquipmentCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
 
    	requestMap.addList("rtnGrid1", equipmentService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", equipmentService.selectTAdminMstEqxct(paramMap));
    	
    	
    	
    	return requestMap;
    }
    
   
    
    /**
     * ADM > 마스터 >  장비> 하단  저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/saveEquipmentCenter.do")
    @ResponseBody   
    public CommandMap saveEquipmentCenter (@RequestBody CommandMap requestMap) throws Exception {    	
    	
 	
    	equipmentService.saveEquipmentCenter(requestMap.getParamMap() , requestMap.getList("list"));
    	
    	
    	return requestMap;
    }    
    
    
    
    /**
     * ADM > 마스터 >  장비> 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/equipmentController/selectEquipmentExcel.do")
    @ResponseBody   
    public void selectEquipmentExcel (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = equipmentService.selectEquipmentExcel(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(equipmentService
				.selectEquipmentExcel(parameterMap, excelFormat), fileName,
				response);
    }      
    
}
