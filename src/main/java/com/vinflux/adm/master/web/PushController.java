package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.flex.remoting.RemotingInclude;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.master.service.PushService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("pushController")
@RemotingDestination
public class PushController extends CommandController {

	@Resource(name = "pushService")
	private PushService pushService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountController.class);
	
    /**
     * ADM > 기본마스터관리 > 푸시 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/pushController/selectPushInfo.do")
    @ResponseBody   
    public CommandMap selectPushInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectPushInfo requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = pushService.selectPushInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "count");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("selectPushInfo resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
	 * ADM > 기본 마스터 관리 > 푸시 디테일 조회
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/pushController/selectDetailPushInfo.do")
	@ResponseBody
	public CommandMap selectDetailPushInfo(@RequestBody CommandMap requestMap) throws Exception {

		Map param = requestMap.getParamMap();
		Map timeZoneMap = pushService.selectDetailPushInfo(param);

		requestMap.addList("rtnGrid", (ArrayList) timeZoneMap.get("list"));

		return requestMap;
	}
	
	/**
	* ADM > 기본마스터관리 > 푸시 삭제
	* @param requestMap
	* @return
	* @throws Exception
	*/
	@RequestMapping(value="/pushController/deletePush.do")
	@ResponseBody
	public CommandMap deletePush(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DATA_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			LOG.debug("======================================");
			LOG.debug("requsetMap pushhdkey VALUE          => " + map.get("pushhdkey"));
			LOG.debug("======================================");

			pushService.deletePushDtl(map, true);
			pushService.deletePush(map);
		}

		LOG.debug("======================================");
		LOG.debug("resultMap => " + requestMap);
		LOG.debug("======================================");

		requestMap.putParam("RESULT", "SUCCESS");

		return requestMap;
	}
	
	/**
	 * ADM > 기본마스터관리 > 푸시 디테일 삭제
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */ 
	@RequestMapping(value="/pushController/deletePushDtl.do")
	@ResponseBody
	public CommandMap deletePushDtl(@RequestBody CommandMap requestMap) throws Exception {

		LOG.debug("======================================");
		LOG.debug("deletePushDtl requsetMap => " + requestMap);
		LOG.debug("======================================");

		List list = requestMap.getList("DATA_LIST");
		int size = list.size();
		for (int i = 0; size > i; i++) {
			Map map = (Map) list.get(i);
			LOG.debug("======================================");
			LOG.debug("requsetMap pushhdkey VALUE          => " + map.get("pushhdkey"));
			LOG.debug("requsetMap pushdtkey VALUE          => " + map.get("pushdtkey"));
			LOG.debug("======================================");
			
			pushService.deletePushDtl(map, false);
			
		}

		LOG.debug("======================================");
		LOG.debug("deletePushDtl resultMap => " + requestMap);
		LOG.debug("======================================");

		requestMap.putParam("RESULT", "SUCCESS");

		return requestMap;
	}
	
	/**
	 * ADM > 기본마스터관리 > 푸시 등록
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value="/pushController/insertPushInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap insertPushInfo (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map formData = requestMap.getMap("FORM_DATA");
    	pushService.insertPushInfo(formData);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
	
	/**
	 * ADM > 기본마스터관리 > 푸시 수정
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value="/pushController/updatePushInfo.do")
    @RemotingInclude // BDS
    @ResponseBody   
    public CommandMap updatePushInfo (@RequestBody CommandMap requestMap) throws Exception {
   
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	pushService.updatePushInfo(requestMap.getMap("FORM_DATA"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
	
	/**
	 * ADM > 기본마스터관리 > 사용자그룹조회
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value="/pushController/selectUserListByUserGroup.do")
	@ResponseBody
	public CommandMap selectUserListByUserGroup(@RequestBody CommandMap requestMap)
			throws Exception {
		Map param = requestMap.getParamMap();
		
		List list = pushService.selectUserListByUserGroup(param);

		requestMap.addList("DATA_LIST", list);
		
		return requestMap;
	}
	
	/**
	 * ADM > 기본마스터관리 > 푸시 등록
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/pushController/sendPush.do")
	@ResponseBody
	public CommandMap sendPush(@RequestBody CommandMap requestMap)
			throws Exception {
		
		Map paramMap = requestMap.getParamMap();
		String workType = (String) paramMap.get("workType");
		List parameterList = requestMap.getList("DATA_LIST");
		List pushconList = requestMap.getList("PUSH_CON");
		
		LOG.debug("======================================");
    	LOG.debug("sendPush requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
		for (Object map : parameterList) {
			String title = null;
			String pushmsg = null;
			
			Map parameterMap = (Map) map;
			
			//넥사크로에서 param으로 title, pushmsg 넘겨받을때 띄어쓰기 이후문자 인신이 안되 분기처리 20170725 장호진
	    	if(pushconList != null && !pushconList.isEmpty()){
	    		Map row = (Map) pushconList.get(0);
				
	    		title = (String) row.get("title");
				pushmsg = (String) row.get("pushmsg");
	    	}else{
	    		title = (String) paramMap.get("title");
	    		pushmsg = (String) paramMap.get("pushmsg");
	    	}
	    	
	    	String pushhdkey = (String) paramMap.get("pushhdkey");

			/* base64 encoding */
			title = Base64.encodeBase64String(title.getBytes("UTF-8"));
			pushmsg = Base64.encodeBase64String(pushmsg.getBytes("UTF-8"));

			parameterMap.put("title", title);
			parameterMap.put("pushhdkey", pushhdkey);
			parameterMap.put("pushmsg", pushmsg);
			
			if ("INSERT".equals(workType)) {

				pushService.insertPush(parameterMap);
				
				//헤더의 발송상태 변경
				String pushstatus = "90";
				parameterMap.put("pushstatus", pushstatus);
				pushService.updatePushHd(parameterMap);
				
			} else if ("UPDATE".equals(workType)) {
				
				//헤더의 발송상태 변경
				String pushstatus = "91";
				parameterMap.put("pushstatus", pushstatus);
				pushService.updatePushHd(parameterMap);
				pushService.updatePush(parameterMap);
			}
		}

		return requestMap;
	}
	
	/**
	 * 20170406 KYJ nexacro
	 * ADM > 기본마스터관리 > 푸시 등록
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@RequestMapping(value = "/pushController/savePushInfo.do")
	@ResponseBody
	public CommandMap savePushInfo(@RequestBody CommandMap requestMap) throws Exception {
		
		LOG.debug("======================================");
		LOG.debug("sendPush requsetMap => " + requestMap);
		LOG.debug("======================================");
		
		Map paramMap = requestMap.getParamMap();
		List parameterList = requestMap.getList("DATA_LIST");
		
    	Map pushInfoMap = new HashedMap();
    	if(parameterList != null && !parameterList.isEmpty()){
    		for(int i = 0 ; i < parameterList.size(); i ++ ){
    			pushInfoMap = (Map)parameterList.get(i);
				if ("C".equals(pushInfoMap.get("STATUS")) ) {
	    				pushService.insertPushInfo(pushInfoMap);
				}else if ("U".equals(pushInfoMap.get("STATUS")) ) {
					pushService.updatePushInfo(pushInfoMap);
				}else if ("D".equals(pushInfoMap.get("STATUS")) ) {
					pushService.deletePushDtl(pushInfoMap, true);
					pushService.deletePush(pushInfoMap);
				}
				pushInfoMap.clear();
    		}
    	}
		return requestMap;
	}
}
