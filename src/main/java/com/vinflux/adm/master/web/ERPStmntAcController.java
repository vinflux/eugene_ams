package com.vinflux.adm.master.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.ERPStmntAcService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("ERPStmntAcController")
@RemotingDestination
public class ERPStmntAcController extends CommandController {

	@Resource(name="ERPStmntAcService")
	private ERPStmntAcService ERPStmntAcService;
	
	
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(ERPStmntAcController.class) ;
	
	
	/**
	 * AMS > 기본 마스터 관리 > ERP 전표계정  헤더 조회 
	 * @param requestMap
     * @return
     * @throws Exception 
	 */
	@RequestMapping(value="/ERPStmntAcController/selectERPStmntAc.do")
	@ResponseBody
	public CommandMap selectERPStmntAc (@RequestBody CommandMap requestMap) throws Exception {
		
		LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
		Map paramMap = requestMap.getSearchQuery();
		Map param = requestMap.getParamMap();
		paramMap.putAll(param);
		
		Map resultMap = ERPStmntAcService.selectERPStmntAc(paramMap);
		Integer count = MapUtil.getInt(resultMap, "cnt");
		if (count > 0) {
			requestMap.putParam("COUNT", Integer.toString(count));
			requestMap.addList("rtnGrid", (ArrayList)resultMap.get("list"));
		}
		
		LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
		return requestMap;
		
	}
	  /**
	 	 * AMS > 기본 마스터 관리 > ERP 전표계정  헤더 엑셀
	     * @param requestMap
	     * @return
	     * @throws Exception
	     */
	    @RequestMapping(value="/ERPStmntAcController/selectExcelERPStmntAc.do")
	    @ResponseBody   
	    public void selectExcelOrType (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
	    	
	    	// JSON parsing 
	    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));

	     	LOG.debug("======================================");
	    	LOG.debug("requsetMap => " + requestMap);
	    	LOG.debug("======================================");
	    	
	    	Map paramMap = requestMap.getSearchQuery();    	        
	    	Map param = requestMap.getParamMap();
	    	paramMap.putAll(param);    	

	    	// excel parameter setting 
	    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
	    	String fileName = (String)param.get("EXCEL_FILENAME");
	    	

	    	// exceldownload 
	    	//ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
	    	
	    	ExcelWriteHandler.downloadExcelFile(
	    			ERPStmntAcService.selectExcelERPStmntAc(paramMap, excelFormat),
	    			fileName, response);
	    }   
	    
	    
	    /**
	     *  AMS > 기본 마스터 관리 > ERP 전표계정  디테일 조회
	     * @param CommandMap requestMap
	     * @return CommandMap
	     * @throws Exception
	     */
	    @RequestMapping(value="/ERPStmntAcController/selectERPStmntAcDt.do")
	    @ResponseBody   
	    public CommandMap selectERPStmntAcDt (@RequestBody CommandMap requestMap) throws Exception {
	    	LOG.debug("======================================");
	    	LOG.debug("requsetMap => " + requestMap);
	    	LOG.debug("======================================");
	    	
	    	Map param = requestMap.getParamMap();
	    	
	    	requestMap.addList("rtnList", ERPStmntAcService.selectERPStmntAcDt(param));

	    	LOG.debug("======================================");
	    	LOG.debug("resultMap => " + requestMap);
	    	LOG.debug("======================================");
	    	
	    	return requestMap;
	    }        

	    
	
}
