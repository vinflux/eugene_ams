package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.StoreService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("storeController")
@RemotingDestination
public class StoreController extends CommandController {

	
	@Resource(name = "storeService")
	private StoreService storeService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(StoreController.class);
	
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> 寃�깋
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/selectStoreInfo.do")
    @ResponseBody   
    public CommandMap selectStoreInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = storeService.selectStoreInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> 寃�깋
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/selectStore.do")
    @ResponseBody   
    public CommandMap selectStore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map map = storeService.selectStore(paramMap);
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> 寃�깋
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/selectCeckStore.do")
    @ResponseBody   
    public CommandMap selectCeckStore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map map = storeService.selectCeckStore(paramMap);
		requestMap.putMap("rtnMap", map);
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> �깅줉 ,�섏젙 ,��젣
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/saveStore.do")
    @ResponseBody   
    public CommandMap saveStore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		storeService.insertStore(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		storeService.updateStore(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			storeService.deleteStore(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * 화주변경 일괄저장
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/saveOwStore.do")
    @ResponseBody   
    public CommandMap saveOwStore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("selectAccount_requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	storeService.insertOwStore(requestMap);
    	
    	return requestMap;
    }
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> �섎떒 議고쉶
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/selectStoreCenter.do")
    @ResponseBody   
    public CommandMap selectStoreCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	requestMap.addList("rtnGrid1", storeService.selectTAdminMstCt(paramMap));
    	requestMap.addList("rtnGrid2", storeService.selectTAdminMstStorexct(paramMap));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> �섎떒  ��옣
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/saveStoreCenter.do")
    @ResponseBody   
    public CommandMap saveStoreCenter (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
/*    	List<Map> list = requestMap.getList("list");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) {
    		Map map = list.get(i);
    		storeService.saveStoreCenter(map);
    	}
*/    	
    	storeService.saveStoreCenter(requestMap.getParamMap() , requestMap.getList("DATA_LIST"));
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    
    
    /**
     * ADM > 留덉뒪��> 嫄곕옒泥�> �묒��ㅼ슫濡쒕뱶
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/storeController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = storeService.selectExcelDownStore(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				storeService.selectExcelDownStore(parameterMap, excelFormat),
				fileName, response);
    }
}
