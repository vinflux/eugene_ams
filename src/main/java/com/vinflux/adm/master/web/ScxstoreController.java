package com.vinflux.adm.master.web;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.master.service.ScxstoreService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("scxstoreController")
@RemotingDestination
public class ScxstoreController extends CommandController {

	@Resource(name = "scxstoreService")
	private ScxstoreService scxstoreService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ScxstoreController.class);
	
    /**
     * ADM > 마스터 > 화주 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/selectScxstoreInfo.do")
    @ResponseBody   
    public CommandMap selectScxstoreInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = scxstoreService.selectScxstoreInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > PK Check , Update Check   
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/selectScxstore.do")
    @ResponseBody   
    public CommandMap selectScxstore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
		requestMap.addList("rtnList", scxstoreService.selectScxstore(requestMap.getParamMap()));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    
    /**
     * ADM > 마스터 > 진열집기 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/checkScxstore.do")
    @ResponseBody   
    public CommandMap checkScxstoreCount (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("requsetMap scxstorekey VALUE=> " + requestMap.getParamMap().get("scxstorekey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param); 	
    	
    	int count = scxstoreService.checkScxstoreCount(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check scxstorekey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > 화주 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/saveScxstore.do")
    @ResponseBody   
    public CommandMap saveScxstore (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		scxstoreService.insertScxstore(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		scxstoreService.updateScxstore(requestMap.getMap("FORM_DATA"));
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			scxstoreService.deleteScxstore(row);
    		}
    	}
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > sc별점포 > 등록,수정 > 점포 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/searchStoreInfo.do")
    @ResponseBody   
    public CommandMap searchStoreInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = scxstoreService.searchStoreInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 마스터 > sc별점포> 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/scxstoreController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = scxstoreService.selectExcelDownScxstore(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				scxstoreService.selectExcelDownScxstore(parameterMap, excelFormat), fileName,
				response);
    }
}
