package com.vinflux.adm.user.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.user.service.PtlUserService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.web.CommandController;

@Controller("ptlUserController")
@RemotingDestination
@SuppressWarnings( {"unchecked","rawtypes"})
public class PtlUserController extends CommandController {
	
	@Resource(name = "ptlUserService")
	private PtlUserService service;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ErpUserController.class);
	
    /**
     * ADM > 관리자 마스터 >  검색
     * @param requestMap
     * @return
     * @throws Exception
     */

	@RequestMapping(value="/ptlUserController/selectPtlUserInfoList.do")
    @ResponseBody   
    public CommandMap selectPtlUserInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	List list = service.selectPtlUserInfoList(paramMap);
        String cnt = "0";
    	if ( list.size() > 0 ) {
    		cnt = ((Map)list.get(0)).get("TOTAL_COUNT").toString() ;
    	}
    	requestMap.putParam("COUNT",  cnt  );
    	requestMap.addList("rtnGrid", list );
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM >  포탈사용자관리 > 승인처리
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/ptlUserController/authUser.do")
    @ResponseBody   
    public CommandMap authUser (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	 
       
    	service.authUser(   requestMap );   	
    
    	
    	return requestMap;
    }
    /**
     * ADM >  포탈사용자관리 > 휴면해제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/ptlUserController/inActiveUserClear.do")
    @ResponseBody   
    public CommandMap inActiveUserClear (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	
    	service.inActiveUserClear(   requestMap );   	
    	
    	
    	return requestMap;
    }
    
    /**
     * ADM > 관리자 마스터 >  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/ptlUserController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
		ExcelWriteHandler.downloadExcelFile(
				service.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }    
}