package com.vinflux.adm.user.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.user.service.ApService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("apController")
@RemotingDestination
public class ApController extends CommandController {
	
	@Resource(name = "apService")
	private ApService apService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ApController.class);
	
    /**
     * ADM >
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/apController/selectApInfoList.do")
    @ResponseBody   
    public CommandMap selectApInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map apMap = apService.selectApInfo(paramMap);
    	Integer count = MapUtil.getInt(apMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)apMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    /**
     * ADM >
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/apController/save.do")
    @ResponseBody   
    public CommandMap save (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("DELETE".equals(workType) ) {
        	List list = requestMap.getList("DELETE_LIST");
        	int size = list.size();
        	for (int i=0 ; i<size ; i++ ) { 
        		Map map = (Map)list.get(i);
        		apService.deleteAp(map);
        	}
    	}else if("INSERT".equals(workType)){
        	List list = requestMap.getList("SAVE_DATA");
        	int size = list.size();
        	for (int i=0 ; i<size ; i++ ) { 
        		Map map = (Map)list.get(i);
        		apService.insertAp(map);
        	}
    	}else{
        	List list = requestMap.getList("SAVE_DATA");
        	int size = list.size();
        	for (int i=0 ; i<size ; i++ ) { 
        		Map map = (Map)list.get(i);
        		apService.updateAp(map);
        	}
    	}

    	

    	
        
        LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;	
    }
    /**
     * ADM>  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/apController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = userService.selectExcelDownUser(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(
				apService.selectExcelDown(parameterMap, excelFormat),
				fileName, response);
    }
}