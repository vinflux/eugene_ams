package com.vinflux.adm.user.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.user.service.EmcontactrService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("emcontactrController")
@RemotingDestination
public class EmcontactrController extends CommandController {
	
	@Resource(name = "emcontactrService")
	private EmcontactrService emcontactrService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(EmcontactrController.class);
	
    /**
     * ADM > 관리자 마스터 >  검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/emcontactrController/selectEmcontactr.do")
    @ResponseBody   
    public CommandMap selectEmcontactr (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = emcontactrService.selectEmcontactr(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 관리자 마스터 >  검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/emcontactrController/updateEmcontactr.do")
    @ResponseBody   
    public CommandMap updateEmcontactr (@RequestBody CommandMap requestMap) throws Exception {
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = requestMap.getList("SAVE_LIST");
    	int size = list.size();
    	for (int i=0 ; i<size ; i++ ) { 
    		Map map = (Map)list.get(i);
    		emcontactrService.updateEmcontactr(map);
    	}
    	
        
        LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");

    	return requestMap;	
    }
    
  
    /**
     * ADM > 관리자 마스터 >  엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/emcontactrController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = emcontactrService.selectExcelDownEmcontactr(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(emcontactrService
				.selectExcelDownEmcontactr(parameterMap, excelFormat),
				fileName, response);
    }    
    
    
    /**
     * ADM > 관리자 마스터 >  비상연락망 > 사용자검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/emcontactrController/selectTAdminMstUr.do")
    @ResponseBody   
    public CommandMap selectTAdminMstUr (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = emcontactrService.selectTAdminMstUr(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
}