package com.vinflux.adm.user.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.user.service.UserGroupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("userGroupController")
@RemotingDestination
public class UserGroupController extends CommandController {
	
	@Resource(name = "userGroupService")
	private UserGroupService userGroupService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserGroupController.class);
	
    /**
     * ADM > 사용자 > 사용자그룹 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userGroupController/selectUserGroupInfoList.do")
    @ResponseBody   
    public CommandMap selectUserGroupInfoList (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = userGroupService.selectUserGroupInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 사용자 > 사용자그룹 > 검색
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userGroupController/selectUserGroupInfo.do")
    @ResponseBody   
    public CommandMap selectUserGroupInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List list = userGroupService.selectUserGroup(requestMap.getParamMap());
    	if (list != null && !list.isEmpty() ) {
    		requestMap.putMap("FORM_DATA", (Map)list.get(0));	
    	} else {
    		requestMap.putMap("FORM_DATA", new HashMap());
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    /**
     * ADM > 사용자 > 사용자그룹 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userGroupController/checkUserGroupInfo.do")
    @ResponseBody   
    public CommandMap checkUserGroupInfo (@RequestBody CommandMap requestMap) throws Exception {
   
     	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("requsetMap urgrkey VALUE=> " + requestMap.getParamMap().get("urgrkey"));
    	LOG.debug("======================================");
    	
    //	Map paramMap = requestMap.getSearchQuery();    	        
    	Map param = requestMap.getParamMap();
   // 	paramMap.putAll(param);    	
    	
    	int count = userGroupService.checkUserGroupInfo(param);
    	if (count == 0 ) {
    		requestMap.putParam("isDuplicate", "false");
    	}
    	else{
    		requestMap.putParam("isDuplicate", "true");
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap Size => " + Integer.toString(count));
    	LOG.debug("resultMap check urgrkey => " + requestMap.getParam("isDuplicate"));
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    
    /**
     * ADM > 사용자 > 사용자그룹 > 등록 ,수정 ,삭제
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userGroupController/saveUserGroup.do")
    @ResponseBody   
    public CommandMap saveUserGroup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getParamMap();
    	String workType = (String)paramMap.get("workType");
    	if ("INSERT".equals(workType) ) {
    		userGroupService.insertUserGroup(requestMap.getMap("FORM_DATA"));
    	} else if ("UPDATE".equals(workType) ) {
    		userGroupService.updateUserGroup(requestMap.getMap("FORM_DATA"));	
    	} else if ("DELETE".equals(workType) ) {
    		List list = requestMap.getList("DELETE_LIST");
    		for (int i=0 ;list.size() > i ; i++  ) {
    			Map row = (Map)list.get(i);
    			userGroupService.deleteUserGroup(row);
    		}
    	}
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
  
    /**
     * ADM > 사용자 > 사용자그룹 > 엑셀다운로드
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/userGroupController/excelDown.do")
    @ResponseBody   
    public void excelDown (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = userGroupService.selectExcelDownUserGroup(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(userGroupService
				.selectExcelDownUserGroup(parameterMap, excelFormat), fileName,
				response);
    }    

}
