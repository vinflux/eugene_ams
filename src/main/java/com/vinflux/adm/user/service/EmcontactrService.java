package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface EmcontactrService {


	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectEmcontactr(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public void updateEmcontactr(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownEmcontactr(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownEmcontactr(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectTAdminMstUr(Map parameterMap) throws Exception;

	
}
