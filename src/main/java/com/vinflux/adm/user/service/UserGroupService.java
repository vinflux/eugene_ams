package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface UserGroupService {

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectUserGroupInfo (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectUserGroup (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkUserGroupInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertUserGroup (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateUserGroup (Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteUserGroup (Map parameterMap) throws Exception;
	

		/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownUserGroup (Map parameterMap) throws Exception;

	public Workbook selectExcelDownUserGroup(Map parameterMap,
			List<Map> excelFormat)throws Exception;
}
