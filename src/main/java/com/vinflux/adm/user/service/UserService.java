package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface UserService {
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectUserCount(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectUserInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectUserForUpdate(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return Map
	 * @throws Exception
	 */
	public Map selectPkTAdminMstUr(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkUserInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateUserPw(Map parameterMap) throws Exception;
	

	/**
	 * 비밀번호 초기화
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void actionInitUrpwInit(Map parameterMap) throws Exception;
	
	/**
	 * 유효일자 체크
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public int checkExpireDateInfo(Map parameterMap) throws Exception;

	/**
	 * 유저 등록 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void insertUser(Map parameterMap) throws Exception;

	/**
	 * 유저 수정
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateUser(Map parameterMap) throws Exception;

	/**
	 * 유저 로그인 실패 횟수 추가  
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateMaxLoginFailCount(Map parameterMap) throws Exception;


	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void deleteUser(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownUser(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)throws Exception;

	/**
	 * 유저별 Device Setting 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void setUserXEq(Map parameterMap) throws Exception;

}