package com.vinflux.adm.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.ErpUserDAO;
import com.vinflux.adm.user.service.ErpUserService;
import com.vinflux.framework.util.CipherUtil;
import com.vinflux.framework.util.MapUtil;

@Service("erpUserService")
public class ErpUserServiceImpl implements ErpUserService {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ErpUserServiceImpl.class);
    
	@Resource(name = "erpUserDAO")
	private ErpUserDAO erpUserDAO;
	
	public int selectErpUserCount(Map parameterMap) throws Exception {
		return erpUserDAO.selectErpUserCount(parameterMap);
	}
	
	public int selectErpUserCheckCount(Map parameterMap) throws Exception {
		return erpUserDAO.selectErpUserCheckCount(parameterMap);
	}
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectErpUserInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		int count = this.selectErpUserCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = erpUserDAO.selectErpUserInfoList(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}

	@Override
	public void saveErpUserInfo(List<Map<String, Object>> parameterList) throws Exception {
		int count;
		Map checkMap = new HashMap();
		for(int i = 0; i <parameterList.size(); i++) {
			checkMap.put("no_emp", parameterList.get(i).get("no_emp"));
			count = this.selectErpUserCheckCount(checkMap);
			
			Map pramMap = new HashMap();
			pramMap.putAll(parameterList.get(i));
			if (count > 0) {
				erpUserDAO.updateErpUserInfo(pramMap);
			} else {
				String encryptPw = CipherUtil.encryptPassword(MapUtil.getStr(pramMap, "no_emp"));
				pramMap.put("urpw", encryptPw);
				erpUserDAO.insertErpUserInfo(pramMap); 
			}
			
		}

	}
	
	@Override
	public void deleteErpUserInfo(List<Map<String, Object>> parameterList) throws Exception {
		for(int i = 0; i <parameterList.size(); i++) {
			Map pramMap = new HashMap();
			pramMap.putAll(parameterList.get(i));
			erpUserDAO.deleteErpUserInfo(pramMap);
		}
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownUser(Map parameterMap) throws Exception {
		return erpUserDAO.selectExcelDown(parameterMap);
	}
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return erpUserDAO.selectExcelDown(parameterMap, excelFormat);
	}

}