package com.vinflux.adm.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.ApDAO;
import com.vinflux.adm.user.service.ApService;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.CipherUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;

@Service("apService")
public class ApServiceImpl implements ApService {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ApServiceImpl.class);
    
	@Resource(name = "apDAO")
	private ApDAO apDAO;
	
	public int selectUserCount(Map parameterMap) throws Exception {
		return apDAO.selectApCount(parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectApInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		//int count = this.selectUserCount(parameterMap);
		//resultMap.put("cnt", Integer.toString(count));

		//if (count > 0) {
			List list = apDAO.selectApInfoList(parameterMap);
		//	if (list != null && !list.isEmpty()) {
				resultMap.put("cnt", list.size());
				resultMap.put("list", list);
		//	}
		//} else {
		//	List list = new ArrayList();
		//	resultMap.put("list", list);
		//}

		return resultMap;
	}
	

	@Override
	public void insertAp(Map parameterMap) throws Exception {
		apDAO.insertAp(parameterMap);
	}
	@Override
	public void updateAp(Map parameterMap) throws Exception {
		apDAO.updateAp(parameterMap);
	}
	@Override
	public void deleteAp(Map parameterMap) throws Exception {
		apDAO.deleteAp(parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception {
		return apDAO.selectExcelDown(parameterMap);
	}
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return apDAO.selectExcelDown(parameterMap, excelFormat);
	}
}