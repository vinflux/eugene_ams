package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.vinflux.framework.collections.CommandMap;

public interface PtlUserService {
 
	/**
	 * 메인조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectPtlUserInfoList(Map parameterMap) throws Exception;
	
	/**
	 * 승인
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void authUser( CommandMap commandMap) throws Exception; 
	
	/**
	 * 휴면해제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void inActiveUserClear( CommandMap commandMap) throws Exception; 
	
 
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;
}