package com.vinflux.adm.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.UserGroupDAO;
import com.vinflux.adm.user.service.UserGroupService;
import com.vinflux.framework.exception.MsgException;

@Service("userGroupService")
public class UserGroupServiceImpl implements UserGroupService {

	@Resource(name="userGroupDAO")
	private UserGroupDAO userGroupDAO;
	
	@Override
	public Map selectUserGroupInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();
		
    	Integer count = userGroupDAO.selectUserGroupCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	
    	if (count > 0 ) {     		
        	List list = userGroupDAO.selectUserGroupInfoList(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
   	
		return resultMap;	
	}

	@Override
	public List selectUserGroup(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return userGroupDAO.selectUserGroupInfo(parameterMap);
	}

	@Override
	public void insertUserGroup(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = userGroupDAO.selectUserGroupCount(parameterMap);
		if (cnt > 0) {
			throw new MsgException(10002);
		} else { 
			this.userGroupDAO.insertUserGroupInfo(parameterMap);	
		}
	}

	@Override
	public void updateUserGroup(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = userGroupDAO.selectUserGroupCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else { 
			this.userGroupDAO.updateUserGroupInfo(parameterMap);	
		}
		
	}

	@Override
	public void deleteUserGroup(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = userGroupDAO.selectUserGroupCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10005);
		} else { 
			try{
				this.userGroupDAO.deleteUserGroupInfo(parameterMap);
			}
			catch(Exception e){
				throw new MsgException(10004);
			}
		}		
	}

	@Override
	public List selectExcelDownUserGroup(Map parameterMap) throws Exception {
		return userGroupDAO.selectExcelDown(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownUserGroup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return userGroupDAO.selectExcelDown(parameterMap, excelFormat);
	}

	@Override
	public int checkUserGroupInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		return userGroupDAO.selectUserGroupCount(parameterMap);
	}
	
}
