package com.vinflux.adm.user.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.PtlUserDAO;
import com.vinflux.adm.persistence.UserDAO;
import com.vinflux.adm.user.service.PtlUserService;
import com.vinflux.adm.user.service.UserService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.exception.MsgException;

@Service("ptlUserService")
@SuppressWarnings( {"rawtypes","unchecked","unused"} )
public class PtlUserServiceImpl implements PtlUserService {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(ErpUserServiceImpl.class);
    
	@Resource(name = "ptlUserDAO")
	private PtlUserDAO dao;
	
	@Resource(name = "userDAO")
	private UserDAO userDAO;
	
	@Resource(name = "userService")
	private  UserService userService;
	
	@Override
	public List selectPtlUserInfoList(Map parameterMap) throws Exception {
		return dao.selectPtlUserInfoList(parameterMap);
	}
 
//	@Override
//	public void savePtlUserInfo(CommandMap commandMap ) throws Exception {
// 
//		List<Map<String, Object>> list = commandMap.getList("FORM_DATA");
//		Map param = commandMap.getParamMap();
//		for(int i = 0 ; i < list.size() ; i++){
//			Map inputMap = list.get(i);
//			
//			if ("C".equals(inputMap.get("STATUS")) ) {
//				dao.insertPtlUserInfo(inputMap);
//			}else if ("U".equals(inputMap.get("STATUS")) ) {
//				dao.updatePtlUserInfo(inputMap);
//			}else if ("D".equals(inputMap.get("STATUS")) ) {
//				dao.deletePtlUserInfo(inputMap);
//			}
//		} 
//	}
	@Override
	public void authUser(CommandMap commandMap ) throws Exception {
 
		List<Map<String, Object>> list = commandMap.getList("FORM_DATA");
		Map param = commandMap.getParamMap();
		for(int i = 0 ; i < list.size() ; i++){
			Map inputMap = list.get(i);
			
			if ( "U".equals(inputMap.get("STATUS"))  ||  "1".equals(inputMap.get("CHK")) ) {
				  
				//저장전 Customer 체크
				int Chk = userDAO.selectUserInfoChkInsert(inputMap);
				//0:customer아님 , 1:customer지만 대표아님 , 2:customer이고 대표
				
				if(Chk == 1){
					throw new MsgException("등록 할수있는 대표거래처가 아닙니다.");
				}
				this.dao.updatePtlUserInfo(inputMap);
				
				String urpartner = (String) inputMap.get("cd_company");
				inputMap.put("urpartner", urpartner);
				
				int cnt = userDAO.selectUserCount(inputMap); 
				if (cnt > 0) {
					this.dao.updateAdminMstUr(inputMap);
				} else { 
					this.userDAO.insertUserInfo(inputMap);
				} 
			}
		} 
		
		if( 1==1 ) {
			
		//	throw new Exception("aaa");
		}
	}

	/**
	 * 휴면해제
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */ 
	@Override
	public void inActiveUserClear(CommandMap commandMap) throws Exception {
		List<Map<String, Object>> list = commandMap.getList("FORM_DATA");
		Map param = commandMap.getParamMap();
		for(int i = 0 ; i < list.size() ; i++){
			Map inputMap = list.get(i); 
			if ( "U".equals(inputMap.get("STATUS"))  ||  "1".equals(inputMap.get("CHK")) ) { 
				dao.updateAdminMstUr(inputMap);
			}
		} 
	}

	@Override
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return dao.selectExcelDown(parameterMap, excelFormat);
	}
    
 

}