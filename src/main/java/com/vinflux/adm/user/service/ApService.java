package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ApService {
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectUserCount(Map parameterMap) throws Exception;

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectApInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void insertAp(Map parameterMap) throws Exception ;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void updateAp(Map parameterMap) throws Exception ;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void deleteAp(Map parameterMap) throws Exception ;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDown(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDown(Map parameterMap, List<Map> excelFormat)throws Exception;

}