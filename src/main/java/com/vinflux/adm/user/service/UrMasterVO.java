package com.vinflux.adm.user.service;

public class UrMasterVO {
	
	private String urKey;
	private String passWd;
	private String urName;
	private String urGrKey;
	private String gtKey;
	private String expireDate;
	private int loginFailCount = 0;
	private String insertTm;
	private String insertUrKey;
	private String updateTm;
	private String updateUrkey;
	private String mulaKey;
	private String usKey;
	
	
	public String getUrKey() {
		return urKey;
	}
	public void setUrKey(String urKey) {
		this.urKey = urKey;
	}
	public String getPassWd() {
		return passWd;
	}
	public void setPassWd(String passWd) {
		this.passWd = passWd;
	}
	public String getUrName() {
		return urName;
	}
	public void setUrName(String urName) {
		this.urName = urName;
	}
	public String getUrGrKey() {
		return urGrKey;
	}
	public void setUrGrKey(String urGrKey) {
		this.urGrKey = urGrKey;
	}
	public String getGtKey() {
		return gtKey;
	}
	public void setGtKey(String gtKey) {
		this.gtKey = gtKey;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public int getLoginFailCount() {
		return loginFailCount;
	}
	public void setLoginFailCount(int loginFailCount) {
		this.loginFailCount = loginFailCount;
	}
	public String getInsertTm() {
		return insertTm;
	}
	public void setInsertTm(String insertTm) {
		this.insertTm = insertTm;
	}
	public String getInsertUrKey() {
		return insertUrKey;
	}
	public void setInsertUrKey(String insertUrKey) {
		this.insertUrKey = insertUrKey;
	}
	public String getUpdateTm() {
		return updateTm;
	}
	public void setUpdateTm(String updateTm) {
		this.updateTm = updateTm;
	}
	public String getUpdateUrkey() {
		return updateUrkey;
	}
	public void setUpdateUrkey(String updateUrkey) {
		this.updateUrkey = updateUrkey;
	}
	public String getMulaKey() {
		return mulaKey;
	}
	public void setMulaKey(String mulaKey) {
		this.mulaKey = mulaKey;
	}
	public String getUsKey() {
		return usKey;
	}
	public void setUsKey(String usKey) {
		this.usKey = usKey;
	}
	
	
}
