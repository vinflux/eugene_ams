package com.vinflux.adm.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.EmcontactrDAO;
import com.vinflux.adm.user.service.EmcontactrService;

@Service("emcontactrService")
public class EmcontactrServiceImpl implements EmcontactrService {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(EmcontactrServiceImpl.class);
    
	@Resource(name = "emcontactrDAO")
	private EmcontactrDAO emcontactrDAO;

	@Override
	public Map selectEmcontactr(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		int count = emcontactrDAO.selectEmcontactrCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = emcontactrDAO.selectEmcontactrList(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}

	@Override
	public void updateEmcontactr(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		this.emcontactrDAO.updateEmcontactr(parameterMap);
	}

	@Override
	public List selectExcelDownEmcontactr(Map parameterMap) throws Exception {
		return emcontactrDAO.selectExcelDownEmcontactr(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownEmcontactr(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return emcontactrDAO.selectExcelDownEmcontactr(parameterMap, excelFormat);
	}
	
	@Override
	public Map selectTAdminMstUr(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		int count = emcontactrDAO.selectTAdminMstUrCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = emcontactrDAO.selectTAdminMstUr(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}


}
