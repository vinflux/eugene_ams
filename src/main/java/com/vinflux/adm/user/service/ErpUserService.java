package com.vinflux.adm.user.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface ErpUserService {
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectErpUserCount(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return int
	 * @throws Exception
	 */
	public int selectErpUserCheckCount(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectErpUserInfo(Map parameterMap) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void saveErpUserInfo(List<Map<String, Object>> parameterList) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void deleteErpUserInfo(List<Map<String, Object>> parameterList) throws Exception;
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownUser(Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)throws Exception;
}