package com.vinflux.adm.user.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.DrvrDAO;
import com.vinflux.adm.persistence.UserDAO;
import com.vinflux.adm.user.service.UserService;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.util.CipherUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;

@Service("userService")
public class UserServiceImpl implements UserService {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(UserServiceImpl.class);
    
	@Resource(name = "userDAO")
	private UserDAO userDAO;
	private DrvrDAO drvrDAO;
	
	public int selectUserCount(Map parameterMap) throws Exception {
		return userDAO.selectUserCount(parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public Map selectUserInfo(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		Map resultMap = new HashMap();

		int count = this.selectUserCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));

		if (count > 0) {
			List list = userDAO.selectUserInfoList(parameterMap);
			if (list != null && !list.isEmpty()) {
				resultMap.put("list", list);
			}
		} else {
			List list = new ArrayList();
			resultMap.put("list", list);
		}

		return resultMap;
	}

	public Map selectUserForUpdate(Map parameterMap) throws Exception {
		int pkCnt = this.selectUserCount(parameterMap);
		
		if (pkCnt < 1) {
    		throw new MsgException(10003); //선택된 정보가 존재하지 않아 수정할 수 없습니다.
    	}
				
		return this.selectPkTAdminMstUr(parameterMap);
	}
	
	public Map selectPkTAdminMstUr(Map parameterMap) throws Exception {
		return this.userDAO.selectPkTAdminMstUr(parameterMap);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void insertUser(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		
		//저장전 Customer 체크
		int Chk = userDAO.selectUserInfoChkInsert(parameterMap);
		//0:customer아님 , 1:customer지만 대표아님 , 2:customer이고 대표
		
		if(Chk == 1){
			throw new MsgException("등록 할수있는 대표거래처가 아닙니다.");
		}
    	
		int cnt = userDAO.selectUserCount(parameterMap);
		if (cnt > 0) {
			throw new MsgException(10002);
		} else {
			// 비밀번호 암호화.
			String encryptPw = CipherUtil.encryptPassword(MapUtil.getStr(parameterMap, "urpw"));
			parameterMap.put("urpw", encryptPw);
			this.userDAO.insertUserInfo(parameterMap);
		}
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void actionInitUrpwInit(Map parameterMap) throws Exception {
		LOG.debug("parameterMap=>"+parameterMap);
		this.userDAO.updateLoginFailInit(parameterMap);
	}

	public int checkExpireDateInfo(Map parameterMap) throws Exception {
		return this.userDAO.checkExpireDateInfo(parameterMap);
		
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void updateUser(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = userDAO.selectUserCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else {
			
			//저장전 Customer 체크
			int Chk = userDAO.selectUserInfoChkUpdate(parameterMap);
			//0:customer아님 , 1:customer지만 대표아님 , 2:customer이고 대표
			
			if(Chk == 1){
				throw new MsgException("등록 할수있는 대표거래처가 아닙니다.");
			}
			
			if (StringUtil.isNotEmpty(MapUtil.getStr(parameterMap, "urpw"))) {
				// 비밀번호 암호화.
				String encryptPw = CipherUtil.encryptPassword(MapUtil.getStr(parameterMap, "urpw"));
				parameterMap.put("urpw", encryptPw);
			}
			this.userDAO.updateUserInfo(parameterMap);
		}

	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void updateMaxLoginFailCount(Map parameterMap) throws Exception {
		this.userDAO.updateMaxLoginFailCount(parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void deleteUser(Map parameterMap) throws Exception {
		// TODO Auto-generated method stub
		int cnt = userDAO.selectUserCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10005);
		} else {
			try {
				this.userDAO.deleteUserInfo(parameterMap);
			} catch (Exception e) {
				throw new MsgException(10004);
			}
		}
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectExcelDownUser(Map parameterMap) throws Exception {
		return userDAO.selectExcelDown(parameterMap);
	}
	
	public Workbook selectExcelDownUser(Map parameterMap, List<Map> excelFormat)
			throws Exception {
		return userDAO.selectExcelDown(parameterMap, excelFormat);
	}

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int checkUserInfo(Map parameterMap) throws Exception {
		return userDAO.selectUserCount(parameterMap);
	}	
	
	/**
	 * 
	 * @param parameterMap
	 * @throws Exception
	 */
	public void updateUserPw(Map parameterMap) throws Exception { 
		String urpw = MapUtil.getStr(parameterMap, "urpw");
		int cnt = userDAO.selectUserCount(parameterMap);
		if (cnt <= 0) {
			throw new MsgException(10003);
		} else {
			// 비밀번호 암호화.			
			if (StringUtil.isNotEmpty(urpw)) {
				String encryptPw = CipherUtil.encryptPassword(urpw);
				parameterMap.put("urpw", encryptPw);
			}
			this.userDAO.updateUserPw(parameterMap);
		}
	}
	
	/**
	 * 유저별 Device Setting 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public void setUserXEq(Map parameterMap) throws Exception {
		String eqtype = MapUtil.getStr(parameterMap, "eqtype");
		
		if ("10".equals(eqtype) ) {
			parameterMap.put("pushid", parameterMap.get("urkey"));
		}
		this.userDAO.deleteTAdminMstUrXEq(parameterMap);
		
		int cnt = this.userDAO.selectTAdminMstUrXEqCount(parameterMap);
		if ( cnt > 0 ) {
			this.userDAO.updateTAdminMstUrXEq(parameterMap);
		} else { 
			this.userDAO.insertTAdminMstUrXEq(parameterMap);	
		}
		
	}
}