package com.vinflux.adm.push.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import com.vinflux.adm.master.web.AccountController;
import com.vinflux.adm.push.service.MqttService;
import com.vinflux.mobile.mqtt.VinfluxMqttClientPublish;
import com.vinflux.mobile.mqtt.interfaces.VinfluxMqttClientCallback;

@Service("mqttService")
public class MqttServiceImpl implements MqttService {
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(AccountController.class);
    
	@Override
	public void sendMsg(String topicStr, String msg) throws Exception {
		
		// TODO Auto-generated method stub
		String str_BrokerIP = "tcp://192.168.192.213";
		//String str_BrokerIP = "tcp://210.116.118.98:1883";
//		String str_BrokerIP = "tcp://175.123.17.18:1883";
		
		int int_QoS = 0;
		String str_ClientID = "MqttPublisher";
		
		
		
		class VinfluxMqttClientCallback implements MqttCallback{

			@Override
			public void connectionLost(Throwable arg0) {
				// TODO Auto-generated method stub
				LOG.debug("Mqtt Client Callbak : connectionLost");
				
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) {
				// TODO Auto-generated method stub
				LOG.debug("Mqtt Client Callbak : deliveryComplete");
				
			}

			@Override
			public void messageArrived(String arg0, MqttMessage arg1)
					throws Exception {
				// TODO Auto-generated method stub
				LOG.debug("Mqtt Client Callbak : Message : " + arg0);
				LOG.debug("Mqtt Client Callbak : MqttMessage : " + arg1.toString());
			}
			
		}
		
		VinfluxMqttClientCallback obj_VinfluxMqttClientCallback = new VinfluxMqttClientCallback();
		VinfluxMqttClientPublish obj_VinfluxMqttClientPublish = new VinfluxMqttClientPublish(str_BrokerIP, int_QoS, str_ClientID, obj_VinfluxMqttClientCallback);
		
		if(obj_VinfluxMqttClientPublish.connect()){
			LOG.debug("Publisher : Mqtt Broker Connected.");
		} else{
			LOG.debug("Publisher : Mqtt Broker Connecting is Fail.");
		}
		
		LOG.debug("topicStr : " + topicStr);
		LOG.debug("msg : " + msg);
		
		boolean isPublish = obj_VinfluxMqttClientPublish.publish(topicStr, msg);

		if(isPublish){
			LOG.debug("Publisher : Mqtt Broker Publish.");
		}
		else{
			LOG.debug("Publisher : Mqtt Broker Publish is Fail.");
		}
		
		obj_VinfluxMqttClientPublish.disconnect();
		
	}
	
}
