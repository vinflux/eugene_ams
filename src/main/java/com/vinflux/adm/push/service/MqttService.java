package com.vinflux.adm.push.service;

public interface MqttService {

	public void sendMsg(String topicStr, String msg) throws Exception;
}
