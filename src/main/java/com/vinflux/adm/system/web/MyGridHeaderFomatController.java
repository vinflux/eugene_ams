package com.vinflux.adm.system.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.system.service.MyGridHeaderFormatService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.web.CommandController;

@Controller("myGridHeaderFomatController")
@RemotingDestination
public class MyGridHeaderFomatController extends CommandController {

   
	/** commonService */
	@Resource(name = "myGridHeaderFormatService")
    private MyGridHeaderFormatService myGridHeaderFormatService;
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MyGridHeaderFomatController.class);

    
    
    /**
     * 그리드 개인화 조회
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/myGridHeaderFomatController/selectMyGridHeaderFormat.do")
    @ResponseBody   
    public CommandMap selectMyGridHeaderFormat(@RequestBody CommandMap requestMap) throws Exception{
    	
    	
    	List list = myGridHeaderFormatService.selectMyGridHeaderFormat(requestMap.getParamMap());
    	requestMap.addList("USER_GRID", list);

    	
    	return requestMap;
    }
   
    
    

	 /**
     * 그리드 개인화 입력
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/myGridHeaderFomatController/saveMyGridHeaderFormat.do")
    @ResponseBody   
    public CommandMap saveMyGridHeaderFormat(@RequestBody CommandMap requestMap) throws Exception{    	
   
    	Map paramMap = requestMap.getParamMap();	

    	List list = requestMap.getList("GRID_HEADER_LIST");
    	
    	myGridHeaderFormatService.saveMyGridHeaderFormat(paramMap, list);
    	
    	//수정된 데이타 가져오기
    	paramMap.put("usKey", null);
    	paramMap.put("usgridId", null);
    	List gridlist = myGridHeaderFormatService.selectMyGridHeaderFormat(paramMap);
    	requestMap.addList("USER_GRID", gridlist);

    	return requestMap;
    
    }
    
    
    
    
    
    
}
