package com.vinflux.adm.system.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.system.service.AdminConfigSetupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("adminConfigSetupController")
@RemotingDestination
public class AdminConfigSetupController extends CommandController {

	@Resource(name = "adminConfigSetupService")
	private AdminConfigSetupService adminConfigSetupService;

	@Resource(name = "commonService")
    private CommonService commonService;
	
    protected static final Log LOG = LogFactory.getLog(AdminConfigSetupController.class);
	
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/adminConfigSetupController/selectAdminConfigSetupInfo.do")
    @ResponseBody   
    public CommandMap selectAdminConfigSetupInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = adminConfigSetupService.selectAdminConfigSetupInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (List)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(수정)
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
	@RequestMapping(value="/adminConfigSetupController/selectAdminConfigSetup.do")
    @ResponseBody   
    public CommandMap selectAdminConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	@SuppressWarnings("rawtypes")
		Map paramMap  = requestMap.getParamMap();    	
    	
		requestMap.putMap("rtnMap", adminConfigSetupService.selectAdminConfigSetup(paramMap));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

	/**
	 * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/adminConfigSetupController/saveAdminConfigSetup.do")
	@ResponseBody
	public CommandMap saveAdminConfigSetup(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");
		Map param = (Map) requestMap.get("PARAM");
		String urKey = (String) param.get("urKey");
		String sMsg = "";
    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		Map map;
    		
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			inputMap.put("updateurkey", urKey);
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				map = adminConfigSetupService.selectAdminConfigSetup(inputMap);
    				
    				if(map == null || map.get("adcskey") == null || "".equals(map.get("adcskey"))){
    					adminConfigSetupService.insertAdminConfigSetup(inputMap);
    				}else{
    					if(i == 0) sMsg = "" + inputMap.get("adcskey");
    					else sMsg += "/" + inputMap.get("adcskey");
    				}
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				adminConfigSetupService.updateAdminConfigSetup(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				adminConfigSetupService.deleteAdminConfigSetup(inputMap);
    			}
    		}
    		
    		commonService.otherModuleAdmSysConfigSetting();
    		
    		if(!"".equals(sMsg)){
				requestMap.setErrCode("-30");
				requestMap.setMessage(sMsg);
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(추가) > 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/adminConfigSetupController/insertAdminConfigSetup.do")
    @ResponseBody   
    public CommandMap insertAdminConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	this.adminConfigSetupService.insertAdminConfigSetup(requestMap.getMap("ADMINCONFIGSETUP"));
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(수정) > 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/adminConfigSetupController/updateAdminConfigSetup.do")
    @ResponseBody   
    public CommandMap updateAdminConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	this.adminConfigSetupService.updateAdminConfigSetup(requestMap.getMap("ADMINCONFIGSETUP"));
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 삭제
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/adminConfigSetupController/deleteAdminConfigSetup.do")
    @ResponseBody   
    public CommandMap deleteAdminConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List<Map> list = requestMap.getList("DELETE_LIST");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = list.get(i);
    		this.adminConfigSetupService.deleteAdminConfigSetup(map);
    	}
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 엑셀다운로드
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/adminConfigSetupController/excelDownAdminConfigSetup.do")
    @ResponseBody   
    public void excelDownAdminConfigSetup (Map commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List<Map> excelList = adminConfigSetupService.selectExcelDownAdminConfigSetup(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(adminConfigSetupService
				.selectExcelDownAdminConfigSetup(parameterMap, excelFormat),
				fileName, response);
    }      
}