package com.vinflux.adm.system.web;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.system.service.MyMenuSetupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.web.CommandController;

@Controller("myMenuSetupController")

@RemotingDestination
public class MyMenuSetupController extends CommandController {

	@Resource(name = "myMenuSetupService")
	private MyMenuSetupService myMenuSetupService;

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(MyMenuSetupController.class);
    
    /**
     * ADM > 권한 관리 > Role별 메뉴 > menu list select
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/myMenuSetupController/selectMyMenu.do", method=RequestMethod.POST)
    @ResponseBody   
    public CommandMap selectMyMenu(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getSearchQuery(); 	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);   
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	Map map = myMenuSetupService.selectMyMenu(paramMap);
    	List list = myMenuSetupService.selectMyMenuGrouping(paramMap);

    	requestMap.putMap("menuStructure", map);
    	requestMap.addList("USER_MENU", list);

    	
    	return requestMap;
    }
    
    
    /**
     * WMS>메뉴개인화 리스트
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/myMenuSetupController/selectMyMenuList.do")
    @ResponseBody   
    public CommandMap selectMyMenuList(@RequestBody CommandMap requestMap) throws Exception{
    	
    	
    	List list = myMenuSetupService.selectMyMenuList(requestMap.getParamMap());
    	
    	requestMap.addList("USER_GRID", list);
 	
    	
    	return requestMap;
    }
    
    /**
     * WMS>메뉴개인화 리스트
     * @param requestMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/myMenuSetupController/selectMyMenuListLevel.do")
    @ResponseBody   
    public CommandMap selectMyMenuListLevel(@RequestBody CommandMap requestMap) throws Exception{
    	Map paramMap = requestMap.getSearchQuery(); 	        
    	Map param = requestMap.getParamMap();
    	paramMap.putAll(param);   
    	paramMap.put("lakey", requestMap.getCommon("LAKEY"));
    	
    	List list = myMenuSetupService.selectMyMenuGroupingLevel(paramMap);
    	
    	requestMap.addList("USER_GRID", list);
 	
    	
    	return requestMap;
    }
    

    /**
     * ADM > 권한 관리 > Role별 메뉴 > 수정
     * @param requestMap
     * @return requestMap
     * @throws Exception
     */
    @RequestMapping(value="/myMenuSetupController/updateMyMenu.do")
    @ResponseBody   
    public CommandMap updateMyMenu (@RequestBody CommandMap requestMap) throws Exception {
    	
    	Map paramMap = requestMap.getParamMap();
		List paramList = requestMap.getList("list");
		myMenuSetupService.insertTworkMstUrdfme(paramMap, paramList);
    	
    	return requestMap;
    } 
}
