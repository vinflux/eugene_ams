package com.vinflux.adm.system.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vinflux.adm.system.service.UserHistoryService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("userHistoryController")
@RemotingDestination
public class UserHistoryController extends CommandController {

	@Resource(name = "userHistoryService")
	private UserHistoryService userHistoryService;

    protected static final Log LOG = LogFactory.getLog(UserHistoryController.class);
	
    /**
     * ADM > 시스템 환경 설정 > 사용자 이력 조회 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/userHistoryController/selectUserHistoryInfo.do")
    @ResponseBody   
    public CommandMap selectUserHistoryInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = userHistoryService.selectUserHistoryInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (List)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    /**
	 * ADM > 기본 마스터 관리 > 사용자 이력 조회 > 상세 검색
	 * @param CommandMap requestMap
	 * @return CommandMap
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/userHistoryController/selectUserHistoryDtl.do")
	@ResponseBody
	public CommandMap selectUserHistoryDtl(@RequestBody CommandMap requestMap) throws Exception {

		Map param = requestMap.getParamMap();
		
		Map userHistoryMap = userHistoryService.selectUserHistoryDtl(param);
    	Integer count = MapUtil.getInt(userHistoryMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (ArrayList) userHistoryMap.get("list"));
    	}		

		return requestMap;
	}
    
    /**
     * ADM > 시스템 환경 설정 > 사용자 이력 조회 > 헤더 엑셀다운로드
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/userHistoryController/excelDownUserHistory.do")
    @ResponseBody   
    public void excelDownUserHistory (Map commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List<Map> excelList = userHistoryService.selectExcelDownUserHistory(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(userHistoryService
				.selectExcelDownUserHistory(parameterMap, excelFormat), fileName,
				response);
    }      
    
    /**
     * ADM > 시스템 환경 설정 > 사용자 이력 조회 > 상세 엑셀다운로드
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/userHistoryController/selectExcelDownUserHistoryDtl.do")
    @ResponseBody  
    public void selectExcelDownUserHistoryDtl (Map<String,Object> commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	Map parameterMap = requestMap.getParamMap();
 	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)parameterMap.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List excelList = userHistoryService.selectExcelDownUserHistoryDtl(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(userHistoryService
				.selectExcelDownUserHistoryDtl(parameterMap, excelFormat),
				fileName, response);
    }  
}