package com.vinflux.adm.system.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.flex.remoting.RemotingDestination;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexacro.xapi.data.DataSet;
import com.vinflux.adm.common.service.CommonService;
import com.vinflux.adm.system.service.SystemConfigSetupService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.ExcelWriteHandler;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.web.CommandController;

@Controller("systemConfigSetupController")
@RemotingDestination
public class SystemConfigSetupController extends CommandController {

	@Resource(name = "systemConfigSetupService")
	private SystemConfigSetupService systemConfigSetupService;

	@Resource(name = "commonService")
    private CommonService commonService;
	
    protected static final Log LOG = LogFactory.getLog(SystemConfigSetupController.class);
	
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 검색
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/systemConfigSetupController/selectSystemConfigSetupInfo.do")
    @ResponseBody   
    public CommandMap selectSystemConfigSetupInfo (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	Map paramMap = requestMap.getSearchQuery();    	        
		Map param = requestMap.getParamMap();
    	paramMap.putAll(param);    	
    	
    	Map itemCodeMap = systemConfigSetupService.selectSystemConfigSetupInfo(paramMap);
    	Integer count = MapUtil.getInt(itemCodeMap, "cnt");
    	if (count > 0 ) {
    		requestMap.putParam("COUNT", Integer.toString(count));
    		requestMap.addList("rtnGrid", (List)itemCodeMap.get("list"));
    	}
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }

    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(수정)
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
	@RequestMapping(value="/systemConfigSetupController/selectSystemConfigSetup.do")
    @ResponseBody   
    public CommandMap selectSystemConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	@SuppressWarnings("rawtypes")
		Map paramMap  = requestMap.getParamMap();    	
    	
		requestMap.putMap("rtnMap", systemConfigSetupService.selectSystemConfigSetup(paramMap));
    	    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }    

	/**
	 * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 저장
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/systemConfigSetupController/saveSystemConfigSetup.do")
	@ResponseBody
	public CommandMap saveSystemConfigSetup(@RequestBody CommandMap requestMap) throws Exception {
		LOG.debug("======================================");
		LOG.debug("requsetMap => " + requestMap);
		LOG.debug("======================================");

		String sMsg = "";
    	List list = requestMap.getList("input_LIST");
    	List returnList = new ArrayList();
    	
    	if(list != null){
    		Map map;
    		
    		for(int i = 0 ; i < list.size() ; i++){
    			Map inputMap = (Map)list.get(i);
    			
    			if ("C".equals(inputMap.get("STATUS")) ) {
    				map = systemConfigSetupService.selectSystemConfigSetup(inputMap);
    				
    				if(map == null || map.get("sckey") == null || "".equals(map.get("sckey"))){
    					systemConfigSetupService.insertSystemConfigSetup(inputMap);
    				}else{
    					if(i == 0) sMsg = "" + inputMap.get("sckey");
    					else sMsg += "/" + inputMap.get("sckey");
    				}
    			}else if ("U".equals(inputMap.get("STATUS")) ) {
    				systemConfigSetupService.updateSystemConfigSetup(inputMap);
    			}else if ("D".equals(inputMap.get("STATUS")) ) {
    				systemConfigSetupService.deleteSystemConfigSetup(inputMap);
    			}
    		}
    		
    		commonService.otherModuleAdmSysConfigSetting();
    		
    		if(!"".equals(sMsg)){
				requestMap.setErrCode("-30");
				requestMap.setMessageCode(sMsg);
    		}
    	}
    	
    	if(returnList.size() > 0){
    		requestMap.addList("rtnMap", returnList);
    	}
    	
    	return requestMap;
	}
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(추가) > 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/systemConfigSetupController/insertSystemConfigSetup.do")
    @ResponseBody   
    public CommandMap insertSystemConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	this.systemConfigSetupService.insertSystemConfigSetup(requestMap.getMap("ADMINCONFIGSETUP"));
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > popup(수정) > 저장
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @RequestMapping(value="/systemConfigSetupController/updateSystemConfigSetup.do")
    @ResponseBody   
    public CommandMap updateSystemConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	this.systemConfigSetupService.updateSystemConfigSetup(requestMap.getMap("ADMINCONFIGSETUP"));
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 삭제
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/systemConfigSetupController/deleteSystemConfigSetup.do")
    @ResponseBody   
    public CommandMap deleteSystemConfigSetup (@RequestBody CommandMap requestMap) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	List<Map> list = requestMap.getList("DELETE_LIST");
    	int size = list.size();
    	for (int i=0 ;size > i ; i++ ) { 
    		Map map = list.get(i);
    		this.systemConfigSetupService.deleteSystemConfigSetup(map);
    	}
    	this.commonService.otherModuleAdmSysConfigSetting();
    	
    	LOG.debug("======================================");
    	LOG.debug("resultMap => " + requestMap);
    	LOG.debug("======================================");
    	
    	return requestMap;
    }  
    
    /**
     * ADM > 시스템 환경 설정 > 관리자 환경 설정 > 엑셀다운로드
     * @param CommandMap requestMap
     * @return CommandMap
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/systemConfigSetupController/excelDownSystemConfigSetup.do")
    @ResponseBody   
    public void excelDownSystemConfigSetup (Map commandMap ,HttpServletResponse response) throws Exception {
    	
    	LOG.debug("======================================");
    	LOG.debug("requsetMap => " + commandMap);
    	LOG.debug("======================================");
    	// JSON parsing 
    	CommandMap requestMap  = ExcelUtil.jsonCovertCommandMap((String)commandMap.get("xlsParam"));
    	
    	// Search condition setting 
    	Map parameterMap = requestMap.getSearchQuery();
    	Map param = requestMap.getParamMap();
    	parameterMap.putAll(param);    	

    	// excel parameter setting 
    	List<Map> excelFormat = requestMap.getList("EXCEL_FORMAT");
    	String fileName = (String)param.get("EXCEL_FILENAME");
    	
    	// db selsect
//    	List<Map> excelList = systemConfigSetupService.selectExcelDownSystemConfigSetup(paramMap);
//
//    	// exceldownload 
//    	ExcelUtil.excelWriteDownload(excelFormat, excelList, fileName , response);
		ExcelWriteHandler.downloadExcelFile(systemConfigSetupService
				.selectExcelDownSystemConfigSetup(parameterMap, excelFormat),
				fileName, response);
    }      
}