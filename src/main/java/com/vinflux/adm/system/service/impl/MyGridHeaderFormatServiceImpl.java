package com.vinflux.adm.system.service.impl;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.MyGridHeaderFormatDAO;
import com.vinflux.adm.system.service.MyGridHeaderFormatService;
import com.vinflux.framework.util.StringUtil;

@Service("myGridHeaderFormatService")
public class MyGridHeaderFormatServiceImpl implements MyGridHeaderFormatService {
	
	protected static final Log LOG = LogFactory.getLog(MyGridHeaderFormatServiceImpl.class);
	
	@Resource(name="myGridHeaderFormatDAO")
	private MyGridHeaderFormatDAO myGridHeaderFormatDAO;

	
	
	/**
	 * 그리드 개인화 select 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectMyGridHeaderFormat (Map paramMap ) throws Exception {
		
		return myGridHeaderFormatDAO.selectTAdminMstUrdfus(paramMap);
	}
	
	
	
	/**
	 * 그리드 개인화 insert 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public void saveMyGridHeaderFormat (Map paramMap, List list ) throws Exception {
		
		List orglist = null;	
		if( list != null && !list.isEmpty() ){
			
			for (int i=0 ;list.size() > i ; i++  ) {
				
				Map row = (Map)list.get(i);	
				if ("DynamicScreen".equals(row.get("uskey")) ) {
					LOG.error("DYNAMIC SCREEN ERROR ... " + row);
					continue;
				}
	
				paramMap.put("usKey", row.get("usKey"));
				paramMap.put("usgridId", row.get("usgridId"));
				paramMap.put("urdfusInfo", row.get("urdfusInfo"));	
				paramMap.put("urdfusInfoInit", row.get("urdfusInfoInit"));
				paramMap.put("delYn", row.get("delYn"));			
				
				orglist = myGridHeaderFormatDAO.selectTAdminMstUrdfus(paramMap);

				//해당데이타가 있으면, 삭제 후 저장
				if( !orglist.isEmpty() && !"Y".equals(paramMap.get("delYn")) ){
					myGridHeaderFormatDAO.deleteTAdminMstUrdfus(paramMap);
					myGridHeaderFormatDAO.insertTAdminMstUrdfus(paramMap);
				}else if("Y".equals(paramMap.get("delYn"))){
					myGridHeaderFormatDAO.deleteTAdminMstUrdfus(paramMap);
				}else{
					myGridHeaderFormatDAO.insertTAdminMstUrdfus(paramMap);
				}
			}
		}	
	}
	
	
	
}