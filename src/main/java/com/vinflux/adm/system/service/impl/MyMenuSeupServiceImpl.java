package com.vinflux.adm.system.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.vinflux.framework.util.MapUtil;
import com.vinflux.adm.persistence.MyMenuSetupDAO;
import com.vinflux.adm.system.service.MyMenuSetupService;

@Service("myMenuSetupService")
public class MyMenuSeupServiceImpl implements MyMenuSetupService {
	
	protected static final Log LOG = LogFactory.getLog(MyMenuSeupServiceImpl.class);
	
	@Resource(name="myMenuSetupDAO")
	private MyMenuSetupDAO myMenuSetupDAO;

	@Override
	public Map selectMyMenu(Map paramMap) throws Exception {
		List list = myMenuSetupDAO.selectMyMenu(paramMap);
		String urKey = MapUtil.getStr(paramMap, "URKEY");
		
		Map menuMap = new HashMap();
		menuMap.put("urkey", urKey);
		menuMap.put("mekey", "0");
    	menuMap.put("isleaf", "N");
    	menuMap.put("mename", "My Menu");
    	menuMap.put("children", new ArrayList());
    	menuMap.put("exclude", new ArrayList());
    	List excludeList = (List)menuMap.get("exclude");
		
    	if ( list == null || list.isEmpty() ){
			 return menuMap;
			 
		} else {

			int listSize = list.size();
	    	
	    	String mekey = (String)menuMap.get("mekey");
	
	    	// 1Depth Setting
	    	for (int i=0 ;listSize > i ; i++ ) {
	    		Map row = (Map)list.get(i);
	    		String uppermekey = (String)row.get("uppermekey");
	    		if (mekey.equals(uppermekey) ) {
	    			List rootChildrenList = (List)menuMap.get("children");
	    			rootChildrenList.add(row);
	    		}
				String isleaf = (String)row.get("isleaf");
				if ("N".equals(isleaf) ) { 
					row.put("children", new ArrayList());			// NOPMD - 내부 처리를 위해 loop안에서 object생성 [ sw.yoo ]
				} else {
					excludeList.add(MapUtil.getStr(row, "mekey"));
				}
	    	}
	    	
	    	// 2Depth Setting
	    	List rootChildrenList = (List)menuMap.get("children");
    		
	    	for (int i=0 ;rootChildrenList.size()> i ; i++ ) {
	    		Map depth1Map = (Map)rootChildrenList.get(i);
	    		
	    		String meKey = (String)depth1Map.get("mekey");
	        	for (int j=0 ;listSize > j ; j++ ) {
	        		Map row = (Map)list.get(j);
	        		String uppermekey = (String)row.get("uppermekey");
	        		if (meKey.equals(uppermekey) ) {
	        			List childrenList = (List)depth1Map.get("children");
	        			childrenList.add(row);
	        		}
	        	}
	    	}
		}
    	
		return menuMap;
	}
	
	
	/**
	 * adm 메뉴 개인화 select 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectMyMenuList (Map paramMap ) throws Exception {
		
		return myMenuSetupDAO.selectMyMenu(paramMap);
	}
	
	/**
	 * adm 메뉴 개인화 select(메인화면) 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectMyMenuGroupingLevel (Map paramMap ) throws Exception {
		return myMenuSetupDAO.selectMyMenuGroupingLevel(paramMap);
	}
	
	/**
	 * adm 메뉴 개인화 select(메인화면) 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@Override
	public List selectMyMenuGrouping (Map paramMap ) throws Exception {
		
		return myMenuSetupDAO.selectMyMenuGrouping(paramMap);
	}
	
	

	@Override
	public void deleteTworkMstUrdfme (Map parameterMap) throws Exception {
		myMenuSetupDAO.deleteTworkMstUrdfme(parameterMap);	
	}

	@Override
	public void insertTworkMstUrdfme (Map parameterMap, List parameterList) throws Exception {
		this.deleteTworkMstUrdfme(parameterMap);
		
		for(int i=0; i<parameterList.size(); i++) {
			Map row = (Map)parameterList.get(i);
			row.put("urkey", MapUtil.getStr(parameterMap, "urkey"));
			myMenuSetupDAO.insertTworkMstUrdfme(row);	
		}
	}
}