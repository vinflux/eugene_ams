package com.vinflux.adm.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.UserHistoryDAO;
import com.vinflux.adm.system.service.UserHistoryService;
import com.vinflux.framework.idgen.IdGenService;

@Service("userHistoryService")
public class UserHistoryServiceImpl implements UserHistoryService {
	
	@Resource(name="userHistoryDAO")
	private UserHistoryDAO userHistoryDAO;

	@Resource(name="urlilohKeyGenService")
	private IdGenService urlilohKeyGenService;
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map selectUserHistoryInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = userHistoryDAO.selectUserHistoryCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = userHistoryDAO.selectUserHistory(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map selectUserHistoryDtl(Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
		Integer count = userHistoryDAO.selectUserHistoryDtlCount(parameterMap);
		resultMap.put("cnt", Integer.toString(count));
		
    	if (count > 0 ) { 
    		List list = userHistoryDAO.selectUserHistoryDtl(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistory (Map parameterMap) throws Exception {
    	return userHistoryDAO.selectExcelDownUserHistory(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownUserHistory(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return userHistoryDAO.selectExcelDownUserHistory(parameterMap,excelFormat);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistoryDtl (Map parameterMap) throws Exception{
		return userHistoryDAO.selectExcelDownUserHistoryDtl(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownUserHistoryDtl(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return userHistoryDAO.selectExcelDownUserHistoryDtl(parameterMap, excelFormat);
	}
	
	/**
     * 사용자 이력 정보 등록
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@Override
	public Map insertUserHistoryHd (Map parameterMap) throws Exception {
		
		String urlilohKey = this.urlilohKeyGenService.getNextStringId();
		parameterMap.put("urliloh_key", urlilohKey);
		
		this.userHistoryDAO.insertTAdminInfoUrliloh(parameterMap);
		return parameterMap;
	}

	/**
     * 사용자 이력 정보 수정
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@Override
	public int updateUserHistoryHd (Map parameterMap) throws Exception {
		return this.userHistoryDAO.updateTAdminInfoUrliloh(parameterMap);
	}
}