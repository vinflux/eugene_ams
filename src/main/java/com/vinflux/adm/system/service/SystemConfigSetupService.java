package com.vinflux.adm.system.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface SystemConfigSetupService {
	
	/**
	 * 관리자 환경 설정  검색 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectSystemConfigSetupInfo (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 수정버튼 클릭
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectSystemConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 popup(추가) 저장
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void insertSystemConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 popup(수정) 저장
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void updateSystemConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 삭제
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void deleteSystemConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정  엑셀 다운
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownSystemConfigSetup (Map parameterMap) throws Exception;

	public Workbook selectExcelDownSystemConfigSetup(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
