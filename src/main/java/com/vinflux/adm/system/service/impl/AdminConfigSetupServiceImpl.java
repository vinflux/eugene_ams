package com.vinflux.adm.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.AdminConfigSetupDAO;
import com.vinflux.adm.system.service.AdminConfigSetupService;
import com.vinflux.framework.common.AdmSysCfgStaticHashtable;
import com.vinflux.framework.util.MapUtil;

@Service("adminConfigSetupService")
public class AdminConfigSetupServiceImpl implements AdminConfigSetupService {
	
	@Resource(name="adminConfigSetupDAO")
	private AdminConfigSetupDAO adminConfigSetupDAO;

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(AdminConfigSetupServiceImpl.class);
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map selectAdminConfigSetupInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = adminConfigSetupDAO.selectAdminConfigSetupCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = adminConfigSetupDAO.selectAdminConfigSetup(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map selectAdminConfigSetup (Map parameterMap) throws Exception {
		return adminConfigSetupDAO.selectPkAdminConfigSetup(parameterMap);
	}
	

	@Override
	@SuppressWarnings("rawtypes")
	public void insertAdminConfigSetup (Map parameterMap) throws Exception {
		this.adminConfigSetupDAO.insertAdminConfigSetup(parameterMap);
		
		//기존 static 정보 변경.
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("key", MapUtil.getStr(parameterMap, "adcskey"));
		dataMap.put("name", MapUtil.getStr(parameterMap, "adcsname"));
		dataMap.put("defaultvalue", MapUtil.getStr(parameterMap, "defaultvalue"));
		dataMap.put("value1", MapUtil.getStr(parameterMap, "value1"));
		dataMap.put("value2", MapUtil.getStr(parameterMap, "value2"));
		AdmSysCfgStaticHashtable.setData(dataMap);
		
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public void updateAdminConfigSetup (Map parameterMap) throws Exception {
		this.adminConfigSetupDAO.updateAdminConfigSetup(parameterMap);
		
		//기존 static 정보 변경.
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("key", MapUtil.getStr(parameterMap, "adcskey"));
		dataMap.put("name", MapUtil.getStr(parameterMap, "adcsname"));
		dataMap.put("defaultvalue", MapUtil.getStr(parameterMap, "defaultvalue"));
		dataMap.put("value1", MapUtil.getStr(parameterMap, "value1"));
		dataMap.put("value2", MapUtil.getStr(parameterMap, "value2"));
		AdmSysCfgStaticHashtable.setData(dataMap);
		
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void deleteAdminConfigSetup (Map parameterMap) throws Exception {
		this.adminConfigSetupDAO.deleteAdminConfigSetup(parameterMap);
	
		//기존 static 정보 변경.
		AdmSysCfgStaticHashtable.removeData(MapUtil.getStr(parameterMap, "adcskey"));
		
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List selectExcelDownAdminConfigSetup (Map parameterMap) throws Exception {
    	return adminConfigSetupDAO.selectExcelDownAdminConfigSetup(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownAdminConfigSetup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return adminConfigSetupDAO.selectExcelDownAdminConfigSetup(parameterMap, excelFormat);
	}
}