package com.vinflux.adm.system.service;

import java.util.List;
import java.util.Map;


public interface MyGridHeaderFormatService {
	
	/**
	 * 그리드 개인화 select 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List selectMyGridHeaderFormat (Map paramMap ) throws Exception;
	
	
	
	/**
	 * 그리드 개인화 insert 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public void saveMyGridHeaderFormat (Map paramMap, List list) throws Exception;
	
}
