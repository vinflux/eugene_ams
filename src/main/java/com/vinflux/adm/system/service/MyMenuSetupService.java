package com.vinflux.adm.system.service;

import java.util.List;
import java.util.Map;


public interface MyMenuSetupService {
	/**
     * 메뉴별 어플리케이션 정보 검색
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public Map selectMyMenu(Map paramMap) throws Exception;
		
	/**
     * 메뉴개인화 리스트
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectMyMenuList(Map paramMap) throws Exception;
	
	/**
     * 메뉴개인화 리스트(메인화면)
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectMyMenuGroupingLevel(Map paramMap) throws Exception;
	
	/**
     * 메뉴개인화 리스트(메인화면)
     * @param parameterMap
     * @return Map
     * @throws Exception
     */
	public List selectMyMenuGrouping(Map paramMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 삭제
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void deleteTworkMstUrdfme (Map parameterMap) throws Exception;
	
	/**
     * 메뉴별 어플리케이션 정보 추가
     * @param parameterMap
     * @return 
     * @throws Exception
     */
	public void insertTworkMstUrdfme (Map parameterMap, List parameterList) throws Exception;
}
