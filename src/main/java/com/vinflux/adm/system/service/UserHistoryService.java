package com.vinflux.adm.system.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface UserHistoryService {
	
	/**
	 * 사용자 이력 조회  검색 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectUserHistoryInfo (Map parameterMap) throws Exception;
	
	/**
	 * 사용자 이력 조회 상세  검색 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectUserHistoryDtl(Map parameterMap) throws Exception;
	
	/**
     * 사용자 이력 조회 헤더 엑셀 다운
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistory (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUserHistory(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
     * 사용자 이력 조회 상세 엑셀 다운
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownUserHistoryDtl (Map parameterMap) throws Exception;
	
	public Workbook selectExcelDownUserHistoryDtl(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
	/**
     * 사용자 이력 정보 등록
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map insertUserHistoryHd (Map parameterMap) throws Exception;
	
	/**
     * 사용자 이력 정보 수정
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	public int updateUserHistoryHd (Map parameterMap) throws Exception;

	

	
}
