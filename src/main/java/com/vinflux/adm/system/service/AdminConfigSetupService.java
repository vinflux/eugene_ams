package com.vinflux.adm.system.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public interface AdminConfigSetupService {
	
	/**
	 * 관리자 환경 설정  검색 
     * @param Map parameterMap
     * @return Map
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectAdminConfigSetupInfo (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 수정버튼 클릭
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public Map selectAdminConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 popup(추가) 저장
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void insertAdminConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 popup(수정) 저장
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void updateAdminConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정 삭제
     * @param Map parameterMap
     * @return 
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public void deleteAdminConfigSetup (Map parameterMap) throws Exception;
	
	/**
     * 관리자 환경 설정  엑셀 다운
     * @param Map parameterMap
     * @return List
     * @throws Exception
     */
	@SuppressWarnings("rawtypes")
	public List selectExcelDownAdminConfigSetup (Map parameterMap) throws Exception;

	public Workbook selectExcelDownAdminConfigSetup(Map parameterMap,
			List<Map> excelFormat)throws Exception;
	
}
