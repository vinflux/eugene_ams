package com.vinflux.adm.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.SystemConfigSetupDAO;
import com.vinflux.adm.system.service.SystemConfigSetupService;
import com.vinflux.framework.client.amf.AmfClient;
import com.vinflux.framework.common.AdmSysCfgStaticHashtable;
import com.vinflux.framework.util.MapUtil;

@Service("systemConfigSetupService")
public class SystemConfigSetupServiceImpl implements SystemConfigSetupService {

    @Resource(name="amfClient")
    protected AmfClient amfClient;
    
	@Resource(name="systemConfigSetupDAO")
	private SystemConfigSetupDAO systemConfigSetupDAO;

//	@Resource(name = "commonService")
//    private CommonService commonService;
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map selectSystemConfigSetupInfo (Map parameterMap) throws Exception {
		Map resultMap = new HashMap();
    	Integer count = systemConfigSetupDAO.selectSystemConfigSetupCount(parameterMap);
    	resultMap.put("cnt", Integer.toString(count));
    	if (count > 0 ) {     		
        	List list = systemConfigSetupDAO.selectSystemConfigSetup(parameterMap);
        	if (list != null && !list.isEmpty()) { 
        		resultMap.put("list", list);	
        	}
    	} else {
    		List list = new ArrayList();
    		resultMap.put("list", list);
    	}
		return resultMap;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map selectSystemConfigSetup (Map parameterMap) throws Exception {
		return systemConfigSetupDAO.selectPkSystemConfigSetup(parameterMap);
	}
	

	@Override
	@SuppressWarnings("rawtypes")
	public void insertSystemConfigSetup (Map parameterMap) throws Exception {
		this.systemConfigSetupDAO.insertSystemConfigSetup(parameterMap);	
		
		//기존 static 정보 변경.
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("key", MapUtil.getStr(parameterMap, "sckey"));
		dataMap.put("name", MapUtil.getStr(parameterMap, "scname"));
		dataMap.put("defaultvalue", MapUtil.getStr(parameterMap, "defaultvalue"));
		dataMap.put("value1", MapUtil.getStr(parameterMap, "value1"));
		dataMap.put("value2", MapUtil.getStr(parameterMap, "value2"));
		AdmSysCfgStaticHashtable.setData(dataMap);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public void updateSystemConfigSetup (Map parameterMap) throws Exception {
		this.systemConfigSetupDAO.updateSystemConfigSetup(parameterMap);
		
		//기존 static 정보 변경.
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("key", MapUtil.getStr(parameterMap, "sckey"));
		dataMap.put("name", MapUtil.getStr(parameterMap, "scname"));
		dataMap.put("defaultvalue", MapUtil.getStr(parameterMap, "defaultvalue"));
		dataMap.put("value1", MapUtil.getStr(parameterMap, "value1"));
		dataMap.put("value2", MapUtil.getStr(parameterMap, "value2"));
		AdmSysCfgStaticHashtable.setData(dataMap);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void deleteSystemConfigSetup (Map parameterMap) throws Exception {
		this.systemConfigSetupDAO.deleteSystemConfigSetup(parameterMap);
		
		//기존 static 정보 변경.
		AdmSysCfgStaticHashtable.removeData(MapUtil.getStr(parameterMap, "sckey"));
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List selectExcelDownSystemConfigSetup (Map parameterMap) throws Exception {
    	return systemConfigSetupDAO.selectExcelDownSystemConfigSetup(parameterMap);
	}
	
	@Override
	public Workbook selectExcelDownSystemConfigSetup(Map parameterMap,
			List<Map> excelFormat) throws Exception {
		return systemConfigSetupDAO.selectExcelDownSystemConfigSetup(parameterMap, excelFormat);
	}
}