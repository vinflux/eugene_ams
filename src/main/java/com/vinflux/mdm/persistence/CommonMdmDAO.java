package com.vinflux.mdm.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.MdmAbstractDAO;

@Repository("commonMdmDAO")
public class CommonMdmDAO extends MdmAbstractDAO {

	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.mdm.persistence.CommonMdmDAO.selectAppMessage",parameterMap);
	}	
	
	public List selectTMdmMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.mdm.persistence.CommonMdmDAO.selectTMdmMstUrdfus",parameterMap);
	}
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTMdmSetMdcsCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.mdm.persistence.CommonMdmDAO.selectTMdmSetMdcsCount",parameterMap);
	}
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTMdmSetMdcs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.mdm.persistence.CommonMdmDAO.selectTMdmSetMdcs",parameterMap);
	}
	
	public int selectTMdmMstMdcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.mdm.persistence.CommonMdmDAO.selectTMdmMstMdcdDtCount",parameterMap);
	}
	
	public List selectTMdmMstMdcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.mdm.persistence.CommonMdmDAO.selectTMdmMstMdcdDt",parameterMap);
	}
	
}