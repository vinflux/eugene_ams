package com.vinflux.ibs.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IbsAbstractDAO;

@Repository("commonIbsDAO")
public class CommonIbsDAO extends IbsAbstractDAO {

	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectAppMessage",parameterMap);
	}
	
	public List selectTIbsMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTIbsMstUrdfus",parameterMap);
	}
	
	public List selectIbsConfigList(Map paramMap) {
    	return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectIbsConfigList", paramMap);
    }
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTIbsSetIbcsCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTIbsSetIbcsCount",parameterMap);
	}
	
	/**
	 * Master data select (SYSTEM CONFIG)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTIbsSetIbcs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTIbsSetIbcs",parameterMap);
	}
	
	public int selectTIbsMstIbcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTIbsMstIbcdDtCount",parameterMap);
	}
	
	public List selectTIbsMstIbcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTIbsMstIbcdDt",parameterMap);
	}
	
	public int selectTitObjectMasterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitObjectMasterCount",parameterMap);
	}
	
	public List selectTitObjectMaster(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitObjectMaster",parameterMap);
	}	
	
	public int selectTitClassMasterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitClassMasterCount",parameterMap);
	}
	
	public List selectTitClassMaster(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitClassMaster",parameterMap);
	}	
	
	public List selectTitClassMasterList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitClassMasterList",parameterMap);
	}
	
	public int selectFactorListCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorListCount",parameterMap);
	}
	
	public List selectFactorList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorList",parameterMap);
	}
	
	public int selectTitServiceMasterCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitServiceMasterCount",parameterMap);
	}
	
	public List selectTitSerivceMaster(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTitSerivceMaster",parameterMap);
	}
	
	public int selectMainFactorCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectMainFactorCount",parameterMap);
	}
	
	public List selectMainFactor(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectMainFactor",parameterMap);
	}
	
	public int selectFactorValueCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorValueCount",parameterMap);
	}
	
	public List selectFactorValue(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorValue",parameterMap);
	}
	
	public int selectLccCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectLccCodeCount",parameterMap);
	}
	
	public List selectLccCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectLccCode",parameterMap);
	}
	
	public int selectVATCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectVATCodeCount",parameterMap);
	}
	
	public List selectVATCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectVATCode",parameterMap);
	}
	
	public int selectBillPartCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectBillPartCodeCount",parameterMap);
	}
	
	public List selectBillPartCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectBillPartCode",parameterMap);
	}
	
	public int selectTableUserCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectTableUserCount",parameterMap);
	}
	
	public List selectTableUser(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTableUser",parameterMap);
	}

	public List selectTableName(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectTableName",parameterMap);
	}
	
	public int selectSourceTableCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectSourceTableCount",parameterMap);
	}
	
	public List selectSourceTable(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectSourceTable",parameterMap);
	}
	
	public int selectKeyColumnNameCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectKeyColumnNameCount",parameterMap);
	}
	
	public List selectKeyColumnName(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectKeyColumnName",parameterMap);
	}

	public String selectFactorDataQuery(Map parameterMap) throws Exception {
		return selectForStr("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorDataQuery",parameterMap);
	}
	
	public int selectFactorDataQueryResultCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorDataQueryResultCount",parameterMap);
	}

	public List selectFactorDataQueryResult(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectFactorDataQueryResult", parameterMap);
	}
	
	public int selectCustomerCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectCustomerCodeCount",parameterMap);
	}
	
	public List selectCustomerCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectCustomerCode",parameterMap);
	}
	
	public int selectCenterCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectCenterCodeCount",parameterMap);
	}
	
	public List selectCenterCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectCenterCode",parameterMap);
	}
	
	public int selectOwkeyCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectOwkeyCodeCount",parameterMap);
	}
	
	public List selectOwkeyCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectOwkeyCode",parameterMap);
	}
	
	public int selectContractNoCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectContractNoCount",parameterMap);
	}
	
	public List selectContractNo(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectContractNo",parameterMap);
	}
	
	public int selectCarrierCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectCarrierCodeCount",parameterMap);
	}
	
	public List selectCarrierCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectCarrierCode",parameterMap);
	}
	public int selectIckeyCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectIckeyCodeCount",parameterMap);
	}
	
	public List selectIckeyCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectIckeyCode",parameterMap);
	}
	public int selectBankCodeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectBankCodeCount",parameterMap);
	}
	
	public List selectBankCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectBankCode",parameterMap);
	}
	
	public int selectAckeyCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.ibs.persistence.CommonIbsDAO.selectAckeyCount",parameterMap);
	}
	
	public List selectAckeyCode(Map parameterMap) throws Exception {
		return selectList("com.vinflux.ibs.persistence.CommonIbsDAO.selectAckeyCode",parameterMap);
	}
}