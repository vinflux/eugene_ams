package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("ownerIcomDAO")
public class OwnerIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstOw(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.OwnerIcomDAO.insertTOrderMstOw",parameterMap);
	}
	
	public int updateTOrderMstOw(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.OwnerIcomDAO.updateTOrderMstOw",parameterMap);
	}
	
	public int deleteTOrderMstOw(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.OwnerIcomDAO.deleteTOrderMstOw",parameterMap);
	}
	
}
