package com.vinflux.icom.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("commonIcomDAO")
public class CommonIcomDAO extends IcomAbstractDAO {

	/**
	 * SELECT TORDER_SET_SC 
	 * @param paramMap
	 * @return
	 */
    public List selectIcomConfigList(Map paramMap) {
    	return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectIcomConfigList", paramMap);
    }
	
	/**
	 * Master data select (ADMIN CODE) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstOrcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstOrcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdDt",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) count (for SelectCommonCode : 290)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstOrcdCstDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdCstDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) (for SelectCommonCode : 290)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstOrcdCstDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdCstDt",parameterMap);
	}


	
	/**
	 * Master data select (MULTI LANGUAGE APPLICATION MESSAGE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstMulaapmsgDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstMulaapmsgDtCount",parameterMap);
	}
	
	public List selectTOrderMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstMulaapmsgDt",parameterMap);
	}
	
	/** Master data select (ACCOUNT) **/
	public int selectTOrderMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAcCount", parameterMap);
	}
	
	/** Master data select (ACCOUNT) **/
	public List selectTOrderMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAc",parameterMap);
	}
	
	/** Master data select (OWNER) **/
	public int selectTOrderMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOwCount",parameterMap);
	}
	
	/** Master data select (OWNER) **/
	public List selectTOrderMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOw",parameterMap);
	}
	
	/** Master data select (CENTER) **/
	public int selectTOrderMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtCount", parameterMap);
	}

	/** Master data select (CENTER) **/
	public List selectTOrderMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCt",parameterMap);
	}
	
	/** Master data select count (ITEMCODE GROUP) **/
	public int selectTOrderMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcgrCount",parameterMap);
	}
	
	/** Master data select (ITEMCODE GROUP) **/
	public List selectTOrderMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcgr",parameterMap);
	}

	/** Master data select (ITEMCODE UNIT) **/
	public List selectTOrderMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcut",parameterMap);
	}

	/** Master data select count (ITEMCODE UNIT) **/
	public int selectTOrderMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcutCount",parameterMap);
	}
	
	/** Master data select count (ITEMCODE) **/
	public int selectTOrderMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcCount",parameterMap);
	}

	/** Master data select (ITEMCODE) **/
	public List selectTOrderMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIc",parameterMap);
	}
	
	/** Master data select count (ITEMCODEXAVQTY) **/
	public int selectTOrderMstIcAvqtyCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcAvqtyCount",parameterMap);
	}

	/** Master data select (ITEMCODEXAVQTY) **/
	public List selectTOrderMstIcAvqty(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcAvqty",parameterMap);
	}
	
	public int selectTOrderMstIcsbCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcsbCount",parameterMap);
	}
	

	/** Master data select (SUBSTITUTION) **/
	public List selectTOrderMstIcsb(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcsb",parameterMap);
	}
	
	/** Master data select (SYSTEM CONFIG) **/
	public List selectTOrderSetOrcs(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderSetOrcs",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectAppMessage",parameterMap);
	}

	/**
	 * Master data select (ITEM UNIT TYPE) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstIcutxtypeCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcutxtypeCount",parameterMap);
	}
	/**
	 * Master data select (ITEM UNIT TYPE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcutxtype",parameterMap);
	}
	

	/**
	 * Master data select (ZIPCODE) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstZcCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstZcCount",parameterMap);
	}
	/**
	 * Master data select (ZIPCODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstZc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstZc",parameterMap);
	}

	/**
	 * Master data select (ICOM DT CODE) : TORDER_MST_ORCD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTOrderMstOrcdDtList() throws Exception {
		Map parameterMap = new HashMap();
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdDtList", parameterMap);
	}

	/**
	 * Master data select type lakey (ICOM DT CODE) : TORDER_MST_ORCD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTOrderMstOrcdDtLakeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrcdDtLakeyList", parameterMap);
	}
	
	/**
	 * Master data select (ICMP) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstIcmpCount(Map parameterMap) throws Exception { 
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcmpCount",parameterMap);
	}
	/**
	 * Master data select (ICMP)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstIcmp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcmp",parameterMap);
	}
	
	/**
	 * SELECT EquipmentList
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectEquipmentList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectEquipmentList",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstCtgCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstCtg(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtg",parameterMap);
	}

	/**
	 * SELECT TORDER_MST_STORE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstStoreCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstStoreCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_STORE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstStore(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstStore",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstAddrTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAddrTCount",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_CTG
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstAddrT(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAddrT",parameterMap);
	}
	
	/** Master data select (SeIc) **/
	public int selectTOrderMstSeIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstSeIcCount",parameterMap);
	}
	
	/** Master data select (SeIc) **/
	public List selectTOrderMstSeIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstSeIc",parameterMap);
	}
	
	/** Master data select (Pogrp) **/
	public int selectTOrderMstPogrpCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstPogrpCount",parameterMap);
	}
	
	/** Master data select (Pogrp) **/
	public List selectTOrderMstPogrp(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstPogrp",parameterMap);
	}
	
	/** Master data select (ICXCT) **/
	public int selectTOrderMstIcxCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcxCtCount", parameterMap);
	}

	/** Master data select (ICXCT) **/
	public List selectTOrderMstIcxCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIcxCt",parameterMap);
	}
	
	/** Master data select (STOREXCT) **/
	public int selectTOrderMstStorexCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstStorexCtCount",parameterMap);
	}
	
	/** Master data select (STOREXCT) **/
	public List selectTOrderMstStorexCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstStorexCt",parameterMap);
	}
	
	/** Master data select (AcForRe) **/
	public int selectTOrderMstAcForRepCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAcForRepCount",parameterMap);
	}
	
	
	/** Master data select (AcForRe) **/
	public List selectTOrderMstAcForRep(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAcForRep",parameterMap);
	}
	
	/** Master data select (ORREASONCD) **/
	public int selectTOrderMstOrTypeReasonCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrTypeReasonCount",parameterMap);
	}
	
	/** Master data select (ORREASONCD) **/
	public List selectTOrderMstOrTypeReason(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstOrTypeReason",parameterMap);
	}
	
	/** Master data select (ACXURCOMMONCD) **/
	public int selectTOrderMstAcXUrCommonCdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAcXUrCommonCdCount",parameterMap);
	}
	
	/** Master data select (ACXURCOMMONCD) **/
	public List selectTOrderMstAcXUrCommonCd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstAcXUrCommonCd",parameterMap);
	}
	
	/** Master data select (PROMOTION IC) **/
	public int selectTOrderMstPromotionIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstPromotionIcCount", parameterMap);
	}

	/** Master data select (PROMOTION IC) **/
	public List selectTOrderMstPromotionIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstPromotionIc",parameterMap);
	}
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectMyMenuGrouping",parameterMap);
	}
	
	public List selectTOrderMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstUrdfus",parameterMap);
	}
	
	/** Master data select count (BATCHORDER ITEMCODE) **/
	public int selectBatchOrderMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectBatchOrderMstIcCount",parameterMap);
	}

	/** Master data select (BATCHORDER ITEMCODE) **/
	public List selectBatchOrderMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectBatchOrderMstIc",parameterMap);
	}
	
	/** Master data select count (NewProduct ITEMCODE) **/
	public int selectNewProductMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectNewProductMstIcCount",parameterMap);
	}

	/** Master data select (NewProduct ITEMCODE) **/
	public List selectNewProductMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectNewProductMstIc",parameterMap);
	}
	
	/** Master data select count (NewProduct SIM ITEMCODE) **/
	public int selectNewProductMstSimIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectNewProductMstSimIcCount",parameterMap);
	}

	/** Master data select (NewProduct SIM ITEMCODE) **/
	public List selectNewProductMstSimIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectNewProductMstSimIc",parameterMap);
	}
	
	/** Master data select count (CTG H-level) **/
	public int selectTOrderMstCtgHlevelCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgHlevelCount",parameterMap);
	}

	/** Master data select (CTG H-level) **/
	public List selectTOrderMstCtgHlevel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgHlevel",parameterMap);
	}
	
	/** Master data select count (CTG M-level) **/
	public int selectTOrderMstCtgMlevelCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgMlevelCount",parameterMap);
	}

	/** Master data select (CTG M-level) **/
	public List selectTOrderMstCtgMlevel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgMlevel",parameterMap);
	}
	
	/** Master data select count (CTG B-level) **/
	public int selectTOrderMstCtgBlevelCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgBlevelCount",parameterMap);
	}

	/** Master data select (CTG B-level) **/
	public List selectTOrderMstCtgBlevel(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstCtgBlevel",parameterMap);
	}
	/**
	 * Master data select (ITEMCODE) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstIvChangeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIvChangeCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEMCODE)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstIvChange(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.CommonIcomDAO.selectTOrderMstIvChange",parameterMap);
	}
	
}
