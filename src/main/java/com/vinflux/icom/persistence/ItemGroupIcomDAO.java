package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("itemGroupIcomDAO")
public class ItemGroupIcomDAO extends IcomAbstractDAO {

	
	/**
	 * INSERT TORDER_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemGroupIcomDAO.insertTOrderMstIcgr",parameterMap);
	}
	
	/**
	 * update TORDER_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemGroupIcomDAO.updateTOrderMstIcgr",parameterMap);
	}

	/**
	 * DELETE TORDER_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemGroupIcomDAO.deleteTOrderMstIcgr",parameterMap);
	}
}
