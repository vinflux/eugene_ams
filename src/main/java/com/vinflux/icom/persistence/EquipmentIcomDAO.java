package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("equipmentIcomDAO")
public class EquipmentIcomDAO extends IcomAbstractDAO {

	
	
	/**
	 * SELECT TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTOrderMstEq (Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.EquipmentIcomDAO.selectTOrderMstEq",parameterMap);
	}
	
	
	/**
	 * INSERT TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstEq(Map parameterMap) throws Exception {
		
		return insert("com.vinflux.icom.persistence.EquipmentIcomDAO.insertTOrderMstEq",parameterMap);
	}
	
	
	/**
	 * UPDATE TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstEq(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.EquipmentIcomDAO.updateTOrderMstEq",parameterMap);
	}
	

	/**
	 * DELETE TORDER_MST_EQ
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstEq(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.EquipmentIcomDAO.deleteTOrderMstEq",parameterMap);
	}
	
	
	
	
}
