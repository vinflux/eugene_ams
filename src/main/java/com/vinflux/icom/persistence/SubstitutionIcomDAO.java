package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("substitutionIcomDAO")
public class SubstitutionIcomDAO extends IcomAbstractDAO {

	/**
	 * INSERT TORDER_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.SubstitutionIcomDAO.insertTOrderMstIcsb",parameterMap);
	}
	
	
	/**
	 * UPDATE TORDER_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstIcsb(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.SubstitutionIcomDAO.updateTOrderMstIcsb",parameterMap);
	}
	

	/**
	 * DELETE TORDER_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstIcsb(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.SubstitutionIcomDAO.deleteTOrderMstIcsb",parameterMap);
	}
	
	
	
	
}
