package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("centerIcomDAO")
public class CenterIcomDAO extends IcomAbstractDAO {

	public Map selectTOrderMstCt(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.icom.persistence.CenterIcomDAO.selectTOrderMstCt",parameterMap);
	}
	
	public int insertTOrderMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.CenterIcomDAO.insertTOrderMstCt",parameterMap);
	}

	public int updateTOrderMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.CenterIcomDAO.updateTOrderMstCt",parameterMap);
	}
	
	public int deleteTOrderMstCt(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.CenterIcomDAO.deleteTOrderMstCt",parameterMap);
	}
}