package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("ctgIcomDAO")
public class CtgIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstCtg(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.CtgIcomDAO.insertTOrderMstCtg",parameterMap);
	}
	
	public int updateTOrderMstCtg(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.CtgIcomDAO.updateTOrderMstCtg",parameterMap);
	}
	
	public int deleteTOrderMstCtg(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.CtgIcomDAO.deleteTOrderMstCtg",parameterMap);
	}
	
}
