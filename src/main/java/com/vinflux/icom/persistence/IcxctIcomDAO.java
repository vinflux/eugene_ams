package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("icxctIcomDAO")
public class IcxctIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstIcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.IcxctIcomDAO.insertTOrderMstIcxct",parameterMap);
	}
	
	public int updateTOrderMstIcxct(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.IcxctIcomDAO.updateTOrderMstIcxct",parameterMap);
	}
	
	public int deleteTOrderMstIcxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.IcxctIcomDAO.deleteTOrderMstIcxct",parameterMap);
	}
	
}
