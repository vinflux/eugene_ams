package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("scxstoreIcomDAO")
public class ScxstoreIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstScxstore(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ScxstoreIcomDAO.insertTOrderMstScxstore",parameterMap);
	}
	
	public int updateTOrderMstScxstore(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.ScxstoreIcomDAO.updateTOrderMstScxstore",parameterMap);
	}
	
	public int deleteTOrderMstScxstore(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.ScxstoreIcomDAO.deleteTOrderMstScxstore",parameterMap);
	}
	
}
