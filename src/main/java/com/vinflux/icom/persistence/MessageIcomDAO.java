package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("messageIcomDAO")
public class MessageIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.MessageIcomDAO.insertTOrderMstMulaapmsgDt",parameterMap);
	}
	
	public int selectCountTOrderMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.MessageIcomDAO.selectCountTOrderMstMulaapmsgDt",parameterMap);
	}
	
	public int updateTOrderMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.MessageIcomDAO.updateTOrderMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTOrderMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.MessageIcomDAO.deleteTOrderMstMulaapmsgDt",parameterMap);
	}
	
}
