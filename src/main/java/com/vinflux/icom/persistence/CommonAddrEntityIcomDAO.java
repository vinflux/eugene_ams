package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("commonAddrEntityIcomDAO")
public class CommonAddrEntityIcomDAO extends IcomAbstractDAO {

	public int addAddressEntitymaster(Map paramMap) throws Exception {
		return insert("com.vinflux.icom.persistence.CommonAddrEntityDAO.addAddressEntitymaster", paramMap);
	}
	
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.icom.persistence.CommonAddrEntityDAO.queryAddressEntitymaster", paramMap);
	}
	
	public int changeAddressEntitymaster(Map paramMap) throws Exception {
		return update("com.vinflux.icom.persistence.CommonAddrEntityDAO.changeAddressEntitymaster", paramMap);
	}
	
	public int deleteAddressEntitymaster(Map paramMap) throws Exception {    	    	
		return update("com.vinflux.icom.persistence.CommonAddrEntityDAO.deleteAddressEntitymaster", paramMap);
	}

}
