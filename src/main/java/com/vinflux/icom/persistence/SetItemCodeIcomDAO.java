package com.vinflux.icom.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("setItemCodeIcomDAO")
public class SetItemCodeIcomDAO extends IcomAbstractDAO {

	/**
	* SET 물품 헤더 정보 조회 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeHd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.SetItemCodeDAO.selectSetItemCodeHd",parameterMap);
	}

	/**
	* SET 물품 헤더 정보 count : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int selectSetItemCodeHdCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.icom.persistence.SetItemCodeDAO.selectSetItemCodeHdCount", parameterMap);
	}

	/**
	* SET 물품 상세 정보 조회 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public List selectSetItemCodeDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.SetItemCodeDAO.selectSetItemCodeDt",parameterMap);
	}

	/**
	* SET 물품 정보 중복 확인 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public int checkSetItemCodeHdInfo(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.SetItemCodeDAO.checkSetItemCodeHdInfo",parameterMap);
	}

	/**
	* SET 물품 정보 중복 확인 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int checkSetItemCodeDtInfo(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.icom.persistence.SetItemCodeDAO.checkSetItemCodeDtInfo",parameterMap);
	}

	/**
	* SET 물품 헤더 정보 생성 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertSetItemCodeHd(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.icom.persistence.SetItemCodeDAO.insertSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 생성 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int insertSetItemCodeDt(Map parameterMap) throws Exception {    	    	
		return insert("com.vinflux.icom.persistence.SetItemCodeDAO.insertSetItemCodeDt", parameterMap);
	}

	/**
	* SET 물품 헤더 정보 갱신 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateSetItemCodeHd(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.SetItemCodeDAO.updateSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 갱신 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int updateSetItemCodeDt(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.SetItemCodeDAO.updateSetItemCodeDt", parameterMap);
	}

	/**
	* SET 물품 헤더 정보 삭제 : TORDER_MST_SEIC_HD
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int deleteSetItemCodeHd(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.SetItemCodeDAO.deleteSetItemCodeHd", parameterMap);
	}

	/**
	* SET 물품 상세 정보 삭제 : TORDER_MST_SEIC_DT
	* {@link com.vinflux.icom.master.service.impl.SetItemCodeServiceImpl}
	* @param Map parameterMap
	* @return int
	* @throws Exception
	*/
	public int deleteSetItemCodeDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.SetItemCodeDAO.deleteSetItemCodeDt", parameterMap);
	}

}
