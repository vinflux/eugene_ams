package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("storeIcomDAO")
public class StoreIcomDAO extends IcomAbstractDAO {

	/**
	 * insert tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstStore(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.icom.persistence.StoreIcomDAO.insertTOrderMstStore", parameterMap);
	}

	
	/**
	 * update tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstStore(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.StoreIcomDAO.updateTOrderMstStore", parameterMap);
	}


	/**
	 * delete tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstStore(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.icom.persistence.StoreIcomDAO.deleteTOrderMstStore", parameterMap);
	}
	

	/**
	 * INSERT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstStorexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.StoreIcomDAO.insertTOrderMstStorexct",parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstStorexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.StoreIcomDAO.deleteTOrderMstStorexct",parameterMap);
	}
}