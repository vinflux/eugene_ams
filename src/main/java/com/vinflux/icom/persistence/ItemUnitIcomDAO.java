package com.vinflux.icom.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("itemUnitIcomDAO")
public class ItemUnitIcomDAO extends IcomAbstractDAO {

	
	/**
	 * INSERT TORDER_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemUnitIcomDAO.insertTOrderMstIcut",parameterMap);
	}
	
	/**
	 * update TORDER_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemUnitIcomDAO.updateTOrderMstIcut",parameterMap);
	}

	/**
	 * DELETE TORDER_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemUnitIcomDAO.deleteTOrderMstIcut",parameterMap);
	}
	
	/**
	 * SELECT TORDER_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTOrderMstIcutxtype (Map parameterMap) throws Exception {
		return selectList("com.vinflux.icom.persistence.ItemUnitIcomDAO.selectTOrderMstIcutxtype",parameterMap);
	}
	
	
	/**
	 * INSERT TORDER_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstIcutxtype(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemUnitIcomDAO.insertTOrderMstIcutxtype",parameterMap);
	}

	/**
	 * UPDATE TORDER_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstIcutxtype(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.ItemUnitIcomDAO.updateTOrderMstIcutxtype",parameterMap);
	}

	/**
	 * DELETE TORDER_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstIcutxtype(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.ItemUnitIcomDAO.deleteTOrderMstIcutxtype",parameterMap);
	}
}
