package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("accountIcomDAO")
public class AccountIcomDAO extends IcomAbstractDAO {
	
	public Map selectTOrderMstAc(Map parameterMap) throws Exception {
		return selectForMap("com.vinflux.icom.persistence.AccountIcomDAO.selectTOrderMstAc",parameterMap);
	}
	
	public int insertTOrderMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AccountIcomDAO.insertTOrderMstAc",parameterMap);
	}

	public int updateTOrderMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AccountIcomDAO.updateTOrderMstAc",parameterMap);
	}
	
	public int deleteTOrderMstAc(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.AccountIcomDAO.deleteTOrderMstAc",parameterMap);
	}
}
