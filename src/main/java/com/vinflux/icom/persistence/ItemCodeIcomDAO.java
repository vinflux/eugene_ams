package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("itemCodeIcomDAO")
public class ItemCodeIcomDAO extends IcomAbstractDAO {

	/**
	 * INSERT TORDER_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTOrderMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemCodeIcomDAO.insertTOrderMstIc",parameterMap);
	}
	
	/**
	 * UPDATE TORDER_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTOrderMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemCodeIcomDAO.updateTOrderMstIc",parameterMap);
	}

	/**
	 * DELETE TORDER_MST_IC
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTOrderMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.ItemCodeIcomDAO.deleteTOrderMstIc",parameterMap);
	}
}
