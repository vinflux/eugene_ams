package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("adminCodeIcomDAO")
public class AdminCodeIcomDAO extends IcomAbstractDAO {
	
	public int mergeTOrderAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AdminCodeIcomDAO.mergeTOrderAdminCodeInfo",parameterMap);
	}

	public int deleteTOrderAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.AdminCodeIcomDAO.deleteTOrderAdminCodeInfo",parameterMap);
	}
	
	public int mergeTOrderDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AdminCodeIcomDAO.mergeTOrderDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTOrderDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.AdminCodeIcomDAO.deleteTOrderDetailAdminCodeInfo",parameterMap);
	}
	
	public int deleteByHDKeyTOrderDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.AdminCodeIcomDAO.deleteByHDKeyTOrderDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTOrderAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.icom.persistence.AdminCodeIcomDAO.selectTOrderAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTOrderDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AdminCodeIcomDAO.insertTOrderDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTOrderAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.AdminCodeIcomDAO.insertTOrderAdminCodeInfo",parameterMap);
	}
}
