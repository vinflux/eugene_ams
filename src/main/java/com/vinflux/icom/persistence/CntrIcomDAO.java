package com.vinflux.icom.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("cntrIcomDAO")
public class CntrIcomDAO extends IcomAbstractDAO {
	
	public int insertTOrderMstCntr(Map parameterMap) throws Exception {
		return insert("com.vinflux.icom.persistence.CntrIcomDAO.insertTOrderMstCntr",parameterMap);
	}
	
	public int updateTOrderMstCntr(Map parameterMap) throws Exception {
		return update("com.vinflux.icom.persistence.CntrIcomDAO.updateTOrderMstCntr",parameterMap);
	}
	
	public int deleteTOrderMstCntr(Map parameterMap) throws Exception {
		return delete("com.vinflux.icom.persistence.CntrIcomDAO.deleteTOrderMstCntr",parameterMap);
	}
	
}
