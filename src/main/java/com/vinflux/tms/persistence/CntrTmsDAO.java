package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("cntrTmsDAO")
public class CntrTmsDAO extends TmsAbstractDAO {
	
	public int insertTTMstCntr(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.CntrTmsDAO.insertTTMstCntr",parameterMap);
	}
	
	public int updateTTMstCntr(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.CntrTmsDAO.updateTTMstCntr",parameterMap);
	}
	
	public int deleteTTMstCntr(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.CntrTmsDAO.deleteTTMstCntr",parameterMap);
	}
	
}
