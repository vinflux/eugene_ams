package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("icxctTmsDAO")
public class IcxctTmsDAO extends TmsAbstractDAO {
	
	public int insertTTMstIcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.IcxctTmsDAO.insertTTMstIcxct",parameterMap);
	}
	
	public int updateTTMstIcxct(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.IcxctTmsDAO.updateTTMstIcxct",parameterMap);
	}
	
	public int deleteTTMstIcxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.IcxctTmsDAO.deleteTTMstIcxct",parameterMap);
	}
	
}
