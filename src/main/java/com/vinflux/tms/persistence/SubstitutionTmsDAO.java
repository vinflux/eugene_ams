package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("substitutionTmsDAO")
public class SubstitutionTmsDAO extends TmsAbstractDAO {
	/**
	 * INSERT TT_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.SubstitutionTmsDAO.insertTTMstIcsb",parameterMap);
	}
	
	/**
	 * INSERT TT_MST_ICSBXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstxCtIcsb(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.SubstitutionTmsDAO.insertTTMstxCtIcsb",parameterMap);
	} 
	
	
	/**
	 * UPDATE TT_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstIcsb(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.SubstitutionTmsDAO.updateTTMstIcsb",parameterMap);
	}
	

	/**
	 * DELETE TT_MST_ICSB
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcsb(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.SubstitutionTmsDAO.deleteTTMstIcsb",parameterMap);
	}
	
	public int insertTTMstIcsbxct (Map parameterMap) throws Exception {		    
		return insert("com.vinflux.tms.persistence.SubstitutionTmsDAO.insertTTMstIcsbxct", parameterMap);
	}
	
	public int deleteTTMstIcsbxct(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.tms.persistence.SubstitutionTmsDAO.deleteTTMstIcsbxct", parameterMap);
	}
	
	
	public int selectPkCountTtMstIcsbxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.SubstitutionTmsDAO.selectPkCountTtMstIcsbxct", parameterMap);
	} 
	
	
	/**
	 * UPDATE TT_MST_ICSBXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstIcsbxct(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.SubstitutionTmsDAO.updateTTMstIcsbxct",parameterMap);
	}
	
	public int selectPkCountTtMstIcsb(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.SubstitutionTmsDAO.selectPkCountTtMstIcsb", parameterMap);
	} 
	
}
