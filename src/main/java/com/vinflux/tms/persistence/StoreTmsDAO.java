package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("storeTmsDAO")
public class StoreTmsDAO extends TmsAbstractDAO{

	/**
	 * insert tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstStore(Map parameterMap) throws Exception {		    
		return insert("com.vinflux.tms.persistence.StoreTmsDAO.insertTTMstStore", parameterMap);
	}

	
	/**
	 * update tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstStore(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.StoreTmsDAO.updateTTMstStore", parameterMap);
	}


	/**
	 * delete tadmin_mst_ac
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstStore(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.tms.persistence.StoreTmsDAO.deleteTTMstStore", parameterMap);
	}
	

	/**
	 * INSERT TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstStorexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.StoreTmsDAO.insertTTMstStorexct",parameterMap);
	}

	/**
	 * DELETE TADMIN_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstStorexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.StoreTmsDAO.deleteTTMstStorexct",parameterMap);
	}
}