package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("itemCodeTmsDAO")
public class ItemCodeTmsDAO extends TmsAbstractDAO {
	
	public int insertTTMstIc(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemCodeTmsDAO.insertTTMstIc",parameterMap);
	}
	
	public int updateTTMstIc(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.ItemCodeTmsDAO.updateTTMstIc",parameterMap);
	}
	public int updateTTMstIcxct(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.ItemCodeTmsDAO.updateTTMstIcxct",parameterMap);
	}
	
	public int deleteTTMstIc(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemCodeTmsDAO.deleteTTMstIc",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemCodeTmsDAO.deleteTTMstIcxct",parameterMap);
	}
	
	public int selectPkCntTTMstIc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.ItemCodeTmsDAO.selectPkCntTTMstIc",parameterMap);
	}
	
	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemCodeTmsDAO.insertTTMstIcxct",parameterMap);
	}
	
}
