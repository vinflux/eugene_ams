package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("commonAddrEntityTmsDAO")
public class CommonAddrEntityTmsDAO extends TmsAbstractDAO {

	public int addAddressEntitymaster(Map paramMap) throws Exception {
		return insert("com.vinflux.tms.persistence.CommonAddrEntityDAO.addAddressEntitymaster", paramMap);
	}
	
	public Map queryAddressEntitymaster(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.tms.persistence.CommonAddrEntityDAO.queryAddressEntitymaster", paramMap);
	}
	
	public int changeAddressEntitymaster(Map paramMap) throws Exception {
		return update("com.vinflux.tms.persistence.CommonAddrEntityDAO.changeAddressEntitymaster", paramMap);
	}
	
	public int deleteAddressEntitymaster(Map paramMap) throws Exception {    	    	
		return update("com.vinflux.tms.persistence.CommonAddrEntityDAO.deleteAddressEntitymaster", paramMap);
	}

}
