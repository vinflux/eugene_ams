package com.vinflux.tms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("itemUnitTmsDAO")
public class ItemUnitTmsDAO extends TmsAbstractDAO {
	
	/**
	 * DELETE TT_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcutxtype(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemUnitTmsDAO.deleteTTMstIcutxtype",parameterMap);
	}
	
	/**
	 * DELETE TT_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcut(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemUnitTmsDAO.deleteTTMstIcut",parameterMap);
	}
	
	/**
	 * UPDATE TT_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstIcutxtype(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.ItemUnitTmsDAO.updateTTMstIcutxtype",parameterMap);
	}
	
	/**
	 * UPDATE TT_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstIcut(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.ItemUnitTmsDAO.updateTTMstIcut",parameterMap);
	}
	
	
	
	
	/**
	 * INSERT TT_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcut(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemUnitTmsDAO.insertTTMstIcut",parameterMap);
	}
	
	/**
	 * INSERT TT_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcutxtype(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemUnitTmsDAO.insertTTMstIcutxtype",parameterMap);
	}
	
	/**
	 * SELECT TT_MST_ICUTXTYPE
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTTMstIcutxtype(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.ItemUnitTmsDAO.selectTTMstIcutxtype",parameterMap);
	}
	
	/**
	 * SELECT TT_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTTMstIcut(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.ItemUnitTmsDAO.selectTTMstIcut",parameterMap);
	}
	/**
	 * SELECT COUNT TT_MST_ICUT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTTMstIcutCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.ItemUnitTmsDAO.selectTTMstIcutCount",parameterMap);
	}
	
	/**
	 * DELETE TT_MST_ICUTXTYPEXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcutxtypexct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemUnitTmsDAO.deleteTTMstIcutxtypexct",parameterMap);
	}
	
	/**
	 * INSERT TT_MST_ICUTXTYPEXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcutxtypexct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemUnitTmsDAO.insertTTMstIcutxtypexct",parameterMap);
	}
	
}
