package com.vinflux.tms.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("commonTmsDAO")
public class CommonTmsDAO extends TmsAbstractDAO {

    /**
     * SELECT TT_SET_TMCS 
     * @param paramMap
     * @return
     */
    public List selectTmsConfigList(Map paramMap) {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTmsConfigList", paramMap);
    }

	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTtMstTmcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtMstTmcdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtMstTmcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtMstTmcdDt",parameterMap);
	}
	
	/**
     * Master data select (ACCOUNT) Count 
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTMMstAcCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTMMstAcCount",parameterMap);
    }

    /**
     * Master data select (ACCOUNT)
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTMMstAc(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTMMstAc",parameterMap);
    }

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTTMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstOwCount",parameterMap);
	}
	
	/**
	 * Master data select (OWNER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTTMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstOw",parameterMap);
	}

	/**
	 * Master data select (CENTER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTTMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstCtCount",parameterMap);
	}
	
	/**
	 * Master data select (CENTER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTTMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstCt",parameterMap);
	}

	/** Master data select count (ITEMCODE GROUP) **/
    public int selectTTMstIcgrCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstIcgrCount",parameterMap);
    }
    
    /** Master data select (ITEMCODE GROUP) **/
    public List selectTTMstIcgr(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstIcgr",parameterMap);
    }
    
    /**
     * Master data select (SUBSTITUTION) COUNT
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTTMstIcsbCount(Map parameterMap) throws Exception { 
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstIcsbCount",parameterMap);
    }
    
    /**
     * Master data select (SUBSTITUTION)
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTTMstIcsb(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTTMstIcsb",parameterMap);
    }
	
	/**
     * SELECT TORDER_MST_CTG
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTMMstStoreCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTMMstStoreCount",parameterMap);
    }
    
    /**
     * SELECT TORDER_MST_CTG
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTMMstStore(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTMMstStore",parameterMap);
    }
	
	/**
	 * Master data select (PLAN) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtPlanTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtPlanTCount",parameterMap);
	}
	
	/**
	 * Master data select (PLAN)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtPlanTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtPlanTList",parameterMap);
	}

	/**
	 * Master data select (CARRIER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtCarrTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCarrTCount",parameterMap);
	}
	
	/**
	 * Master data select (CARRIER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtCarrTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCarrTList",parameterMap);
	}

	/**
	 * Master data select (CARRIER) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtCustTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCustTCount",parameterMap);
	}
	
	/**
	 * Master data select (CARRIER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtCustTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCustTList",parameterMap);
	}
	
	/**
     * Master data select (SMESTMINFO) count
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTtSmEstmTCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtSmEstmTCount",parameterMap);
    }
    
    /**
     * Master data select (SMESTMINFO)
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTtSmEstmTList(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtSmEstmTList",parameterMap);
    }

    /**
     * Master data select (SMESTM HD INFO) count
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public int selectTtSmEstmHdCount(Map parameterMap) throws Exception {
        return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtSmEstmHdCount",parameterMap);
    }
    
    /**
     * Master data select (SMESTM HD INFO)
     * @param parameterMap
     * @return
     * @throws Exception
     */
    public List selectTtSmEstmHdList(Map parameterMap) throws Exception {
        return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtSmEstmHdList",parameterMap);
    }
    
	/**
	 * Master data select (ITEM GROUP) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtItmGrpTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtItmGrpTCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEM GROUP)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtItmGrpTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtItmGrpTList",parameterMap);
	}

	/**
	 * Master data select (ITEM) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtItmMstTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtItmMstTCount",parameterMap);
	}
	
	/**
	 * Master data select (ITEM)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtItmMstTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtItmMstTList",parameterMap);
	}

	/**
	 * Master data select (CURRENCY) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtCurcncyTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCurcncyTCount",parameterMap);
	}
	
	/**
	 * Master data select (CURRENCY)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtCurcncyTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtCurcncyTList",parameterMap);
	}
	
	/**
	 * Master data select (EQUIPTYPCD) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtMstEqmtTypCdCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtMstEqmtTypCdCount",parameterMap);
	}
	
	/**
	 * Master data select (EQUIPTYPCD)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtMstEqmtTypCd(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtMstEqmtTypCd",parameterMap);
	}
	


	
	/**
	 * TMS Message Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectAppMessage",parameterMap);
	}	


	/**
	 * Master data select Center Type (ADMIN DT CODE) :TWORK_MST_CTCD_DT 
	 * @return List
	 * @throws Exception
	 */
	public List selectTmMstTmcdDtList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTmMstTmcdDtList", parameterMap);
	}

	
	/**
	 * Master data select (EQUIPTYPCD) count
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTtShpglocTCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.CommonTmsDAO.selectTtShpglocTCount",parameterMap);
	}
	
	/**
	 * Master data select (EQUIPTYPCD)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTtShpglocTList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtShpglocTList",parameterMap);
	}
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectMyMenuGrouping",parameterMap);
	}
	
	
	public List selectTtMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.tms.persistence.CommonTmsDAO.selectTtMstUrdfus",parameterMap);
	}
	
	
}
