package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("itemGroupTmsDAO")
public class ItemGroupTmsDAO extends TmsAbstractDAO {

	
	/**
	 * INSERT TT_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemGroupTmsDAO.insertTTMstIcgr",parameterMap);
	}
	
	/**
	 * update TT_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int updateTTMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemGroupTmsDAO.updateTTMstIcgr",parameterMap);
	}

	/**
	 * DELETE TT_MST_ICGR
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcgr(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemGroupTmsDAO.deleteTTMstIcgr",parameterMap);
	}
	
	/**
	 * DELETE TT_MST_ICGRXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int deleteTTMstIcgrxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.ItemGroupTmsDAO.deleteTTMstIcgrxct",parameterMap);
	}

	/**
	 * DELETE TT_MST_ICGRXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstIcgrxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.ItemGroupTmsDAO.insertTTMstIcgrxct",parameterMap);
	}

}
