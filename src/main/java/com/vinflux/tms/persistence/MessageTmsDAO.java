package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("messageTmsDAO")
public class MessageTmsDAO extends TmsAbstractDAO {

	public int selectCountTTmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.MessageTmsDAO.selectCountTTmsMstMulaapmsgDt",parameterMap);
	}
	
	public int insertTTmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.MessageTmsDAO.insertTTmsMstMulaapmsgDt",parameterMap);
	}

	public int updateTTmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.MessageTmsDAO.updateTTmsMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTTmsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.MessageTmsDAO.deleteTTmsMstMulaapmsgDt",parameterMap);
	}
}