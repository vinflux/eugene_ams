package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("ownerTmsDAO")
public class OwnerTmsDAO extends TmsAbstractDAO {
	
	public int insertTOrderMstOw(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.OwnerTmsDAO.insertTTMstOw",parameterMap);
	}
	
	public int updateTOrderMstOw(Map parameterMap) throws Exception {
		return update("com.vinflux.tms.persistence.OwnerTmsDAO.updateTTMstOw",parameterMap);
	}
	
	public int deleteTOrderMstOw(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.OwnerTmsDAO.deleteTTMstOw",parameterMap);
	}
	
}
