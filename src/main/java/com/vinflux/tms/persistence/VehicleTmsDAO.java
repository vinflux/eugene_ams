package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("vehicleTmsDAO")
public class VehicleTmsDAO extends TmsAbstractDAO{

	/**
	 * update tt_equip_t sttcd
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int approveTMSEquip(Map parameterMap) throws Exception {		    
		return update("com.vinflux.tms.persistence.VehicleTmsDAO.approveTMSEquip", parameterMap);
	}
}