package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("accountTmsDAO")
public class AccountTmsDAO extends TmsAbstractDAO {

	public int selectPkCntTTMstAc(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.AccountTmsDAO.selectPkCntTTMstAc",parameterMap);
	}
	
	public int insertTTMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AccountTmsDAO.insertTTMstAc",parameterMap);
	}

	public int updateTTMstAc(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AccountTmsDAO.updateTTMstAc",parameterMap);
	}
	
	public int deleteTTMstAc(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.AccountTmsDAO.deleteTTMstAc",parameterMap);
	}
	
	/**
	 * INSERT TT_MST_ACXCT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTTMstAcxct(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AccountTmsDAO.insertTTMstAcxct",parameterMap);
	}
	
	public int deleteTTMstAcxct(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.AccountTmsDAO.deleteTTMstAcxct",parameterMap);
	}
	
	public int selectPkCntTTMstAcxct(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.tms.persistence.AccountTmsDAO.selectPkCntTTMstAcxct",parameterMap);
	}
}
