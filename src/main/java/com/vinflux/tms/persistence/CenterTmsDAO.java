package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("centerTmsDAO")
public class CenterTmsDAO extends TmsAbstractDAO {

	public int insertTTMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.CenterTmsDAO.insertTTMstCt",parameterMap);
	}

	public int updateTTMstCt(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.CenterTmsDAO.updateTTMstCt",parameterMap);
	}
	
	public int deleteTTMstCt(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.CenterTmsDAO.deleteTTMstCt",parameterMap);
	}
}