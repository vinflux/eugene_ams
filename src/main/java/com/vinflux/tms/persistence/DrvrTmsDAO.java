package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("drvrTmsDAO")
public class DrvrTmsDAO extends TmsAbstractDAO{

	/**
	 * update tt_drvr_t 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int approveTMSDrvr(Map parameterMap) throws Exception {		    
		return update("com.vinflux.tms.persistence.DrvrTmsDAO.approveTMSDrvr", parameterMap);
	}
}