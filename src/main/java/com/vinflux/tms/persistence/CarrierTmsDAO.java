package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("carrierTmsDAO")
public class CarrierTmsDAO extends TmsAbstractDAO{

	/**
	 * update tt_carr_t sttcd
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int approveTMSCarr(Map parameterMap) throws Exception {		    
		return update("com.vinflux.tms.persistence.CarrierTmsDAO.approveTMSCarr", parameterMap);
	}
}