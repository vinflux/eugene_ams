package com.vinflux.tms.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.TmsAbstractDAO;

@Repository("adminCodeTmsDAO")
public class AdminCodeTmsDAO extends TmsAbstractDAO {

	public int mergeTTAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AdminCodeTmsDAO.mergeTTAdminCodeInfo",parameterMap);
	}

	public int deleteTTAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.AdminCodeTmsDAO.deleteTTAdminCodeInfo",parameterMap);
	}
	
	public int mergeTTDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AdminCodeTmsDAO.mergeTTDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTTDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.AdminCodeTmsDAO.deleteTTDetailAdminCodeInfo",parameterMap);
	}
	
	
	public int deleteByHDKeyTTDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.tms.persistence.AdminCodeTmsDAO.deleteByHDKeyTTDetailAdminCodeInfo",parameterMap);
	}
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTTAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.tms.persistence.AdminCodeTmsDAO.selectTTAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTTDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AdminCodeTmsDAO.insertTTDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTTAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.tms.persistence.AdminCodeTmsDAO.insertTTAdminCodeInfo",parameterMap);
	}
}