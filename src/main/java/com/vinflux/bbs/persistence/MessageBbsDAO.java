package com.vinflux.bbs.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.BbsAbstractDAO;

@Repository("messageBbsDAO")
public class MessageBbsDAO extends BbsAbstractDAO {

	public int selectCountTBbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.bbs.persistence.MessageBbsDAO.selectCountTBbsMstMulaapmsgDt",parameterMap);
	}
	
	public int insertTBbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.bbs.persistence.MessageBbsDAO.insertTBbsMstMulaapmsgDt",parameterMap);
	}

	public int updateTBbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.bbs.persistence.MessageBbsDAO.updateTBbsMstMulaapmsgDt",parameterMap);
	}
	
	public int deleteTBbsMstMulaapmsgDt(Map parameterMap) throws Exception {
		return delete("com.vinflux.bbs.persistence.MessageBbsDAO.deleteTBbsMstMulaapmsgDt",parameterMap);
	}
}