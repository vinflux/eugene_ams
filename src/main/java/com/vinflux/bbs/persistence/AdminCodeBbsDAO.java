package com.vinflux.bbs.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.BbsAbstractDAO;

@Repository("adminCodeBbsDAO")
public class AdminCodeBbsDAO extends BbsAbstractDAO {

	public int mergeTBbsAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.bbs.persistence.AdminCodeBbsDAO.mergeTBbsAdminCodeInfo",parameterMap);
	}

	public int deleteTBbsAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.bbs.persistence.AdminCodeBbsDAO.deleteTBbsAdminCodeInfo",parameterMap);
	}
	
	public int mergeTBbsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.bbs.persistence.AdminCodeBbsDAO.mergeTBbsDetailAdminCodeInfo",parameterMap);
	}

	public int deleteTBbsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.bbs.persistence.AdminCodeBbsDAO.deleteTBbsDetailAdminCodeInfo",parameterMap);
	}
	
	
	public int deleteByHDKeyTBbsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return delete("com.vinflux.bbs.persistence.AdminCodeBbsDAO.deleteByHDKeyTBbsDetailAdminCodeInfo",parameterMap);
	}
	
	
	/**
	 * HD 정보가 있는지 없는지 확인
	 * @param parameterMap
	 * @return
	 */
	public int selectTBbsAdminCodeCountInfo(Map parameterMap) {
		return selectForInt("com.vinflux.bbs.persistence.AdminCodeBbsDAO.selectTBbsAdminCodeCountInfo", parameterMap);
	}
	
	public int insertTBbsDetailAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.bbs.persistence.AdminCodeBbsDAO.insertTBbsDetailAdminCodeInfo",parameterMap);
	}
	
	public int insertTBbsAdminCodeInfo(Map parameterMap) throws Exception {
		return insert("com.vinflux.bbs.persistence.AdminCodeBbsDAO.insertTBbsAdminCodeInfo",parameterMap);
	}
	
}