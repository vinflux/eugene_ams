package com.vinflux.bbs.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.BbsAbstractDAO;


@Repository("commonBbsDAO")
public class CommonBbsDAO extends BbsAbstractDAO {

	public int selectTBbsMstBbcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.bbs.persistence.CommonBbsDAO.selectTBbsMstBbcdDtCount",parameterMap);
	}

	public List selectTBbsMstBbcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.bbs.persistence.CommonBbsDAO.selectTBbsMstBbcdDt",parameterMap);
	}	
	
	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.bbs.persistence.CommonBbsDAO.selectAppMessage",parameterMap);
	}	

	public int selectTBbsMstMeCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.bbs.persistence.CommonBbsDAO.selectTBbsMstMeCount",parameterMap);
	}

	public List selectTBbsMstMe(Map parameterMap) throws Exception {
		return selectList("com.vinflux.bbs.persistence.CommonBbsDAO.selectTBbsMstMe",parameterMap);
	}

	public List selectTBbsMstMeAll(Map parameterMap) throws Exception {
		return selectList("com.vinflux.bbs.persistence.CommonBbsDAO.selectTBbsMstMeAll",parameterMap);
	}
	
	public List selectNewBbcnMekey(Map parameterMap) throws Exception {
		return selectList("com.vinflux.bbs.persistence.CommonBbsDAO.selectNewBbcnMekey",parameterMap);
	}
	

	
}