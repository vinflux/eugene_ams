package com.vinflux.kpi.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.KpiAbstractDAO;

@Repository("messageKpiDAO")
	
public class MessageKpiDAO extends KpiAbstractDAO {
	public int insertTKpiMstMulaapmsgDt(Map parameterMap) throws Exception {
		return insert("com.vinflux.kpi.persistence.MessageKpiDAO.insertTKpiMstMulaapmsgDt", parameterMap);
	}

	public int selectCountTKpiMstMulaapmsgDt(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.MessageKpiDAO.selectCountTKpiMstMulaapmsgDt", parameterMap);
	}
	
	public int updateTKpiMstMulaapmsgDt(Map parameterMap) throws Exception {
		return update("com.vinflux.kpi.persistence.MessageKpiDAO.updateTKpiMstMulaapmsgDt", parameterMap);
	}
	
	public int deleteTKpiMstMulaapmsgDt(Map parameterMap) throws Exception {    	    	
		return delete("com.vinflux.kpi.persistence.MessageKpiDAO.deleteTKpiMstMulaapmsgDt", parameterMap);
	}
	
	public List<Map> selectTKpiIfAlertList(){
		return selectList("com.vinflux.kpi.persistence.MessageKpiDAO.selectTKpiIfAlertList", null);
	} 
	
}