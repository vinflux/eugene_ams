package com.vinflux.kpi.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.KpiAbstractDAO;

@Repository("commonKpiDAO")
public class CommonKpiDAO extends KpiAbstractDAO {

	/**
	 * TMS Message Select
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectAppMessage (Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectAppMessage",parameterMap);
	}	

	/**
	 * 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTKpiMstOwCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstOwCount",parameterMap);
	}
	
	/**
	 * Master data select (OWNER)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTKpiMstOw(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstOw",parameterMap);
	}
	
	/**
	 * Master data select (CENTER) count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTKpiMstCtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstCtCount",parameterMap);
	}
	
	/** Master data select (CENTER) 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 **/
	public List selectTKpiMstCt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstCt",parameterMap);
	}
	
	/** Master data select count (ITEMCODE GROUP) **/
	public int selectTKpiMstIcgrCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstIcgrCount",parameterMap);
	}
	
	/** Master data select (ITEMCODE GROUP) **/
	public List selectTKpiMstIcgr(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstIcgr",parameterMap);
	}
	
	/** Master data select count (ITEMCODE) **/
	public int selectTKpiMstIcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstIcCount",parameterMap);
	}

	/** Master data select (ITEMCODE) **/
	public List selectTKpiMstIc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstIc",parameterMap);
	}
	
	public List selectMyMenuGrouping(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectMyMenuGrouping",parameterMap);
	}
	
	public List selectTKpiMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstUrdfus",parameterMap);
	}
	
	
	/**
	 * Master data select (VIMS DT CODE) : TKPI_MST_VICD_DT
	 * @return List
	 * @throws Exception
	 */
	public List selectTKpiMstKpcdDtList() throws Exception {
		Map parameterMap = new HashMap();
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstKpcdDtList", parameterMap);
	}
	
	public List selectTKpiMstKpcdDtkeyList(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstKpcdDtkeyList", parameterMap);
	}	
	
	/**
	 * 공통 코드 테이블 Paging을 위한 Count: TWORK_MST_CTCD_DT   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
	public int selectTKpiMstKpcdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstKpcdDtCount",parameterMap);
	}
	
	/**
	 * Master data select (ADMIN CODE) :TWORK_MST_CTCD_DT
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTKpiMstKpcdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstKpcdDt",parameterMap);
	}
	
	/**
	 * Master data select (ACCOUNT) Count 
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int selectTKpiMstAcCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstAcCount",parameterMap);
	}

	/**
	 * Master data select (ACCOUNT)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public List selectTKpiMstAc(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTKpiMstAc",parameterMap);
	}
	

	/**
	* 상세 KPI 기준 코드 조회 : TKPI_MST_KPCD_DT
	* {@link com.vinflux.kpi.KpiCodeIcomServiceImpl.service.impl.KpiCodeIcomServiceImpl}
	* @param Map parameterMap
	* @return List
	* @throws Exception
	*/
	public int selectTKpiMstOrdCdDtCount(Map parameterMap) throws Exception {
		return this.selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectKpiCodeDtCount", parameterMap);
	}

	public List selectTKpiMstOrdCdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectKpiCodeDt",parameterMap);
	}
	
	
	
	
	
	
	
	
	
	

//	public int selectTViewMstIcsbCount(Map parameterMap) throws Exception { 
//		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcsbCount",parameterMap);
//	}
//	
//
//	/** Master data select (SUBSTITUTION) **/
//	public List selectTViewMstIcsb(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcsb",parameterMap);
//	}
	
//	/** Master data select (ITEMCODE UNIT) **/
//	public List selectTViewMstIcut(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcut",parameterMap);
//	}
//
//	/** Master data select count (ITEMCODE UNIT) **/
//	public int selectTViewMstIcutCount(Map parameterMap) throws Exception {
//		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcutCount",parameterMap);
//	}
//	
	/**
	 * Master data select (ITEM UNIT TYPE) count  
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
//	public int selectTViewMstIcutxtypeCount(Map parameterMap) throws Exception { 
//		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcutxtypeCount",parameterMap);
//	}
//	/**
//	 * Master data select (ITEM UNIT TYPE)
//	 * @param parameterMap
//	 * @return
//	 * @throws Exception
//	 */
//	public List selectTViewMstIcutxtype(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstIcutxtype",parameterMap);
//	}
	
	/**
	 * 데쉬보드 헤더 Count: TVIEW_MST_DASH_HD   
	 * @param parameterMap
	 * @return int 
	 * @throws Exception
	 */
//	public int selectTViewMstDashHdCount(Map parameterMap) throws Exception {
//		return selectForInt("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstDashHdCount",parameterMap);
//	}
//	
//	/**
//	 * 데쉬보드 헤더 :TVIEW_MST_DASH_HD
//	 * @param parameterMap
//	 * @return
//	 * @throws Exception
//	 */
//	public List selectTViewMstDashHd(Map parameterMap) throws Exception {
//		return selectList("com.vinflux.kpi.persistence.CommonKpiDAO.selectTViewMstDashHd",parameterMap);
//	}	
	
}
