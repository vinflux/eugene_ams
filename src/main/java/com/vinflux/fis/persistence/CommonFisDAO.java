package com.vinflux.fis.persistence;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.FisAbstractDAO;

@Repository("commonFisDAO")
public class CommonFisDAO extends FisAbstractDAO {

	public List selectAppMessage (Map parameterMap) throws Exception { 
		return selectList("com.vinflux.fis.persistence.CommonFisDAO.selectAppMessage",parameterMap);
	}	
	
	public List selectTFisMstUrdfus(Map parameterMap) throws Exception {
		return selectList("com.vinflux.fis.persistence.CommonFisDAO.selectTFisMstUrdfus",parameterMap);
	}
	
	public int selectTFisMstFicdDtCount(Map parameterMap) throws Exception {
		return selectForInt("com.vinflux.fis.persistence.CommonFisDAO.selectTFisMstFicdDtCount",parameterMap);
	}
	
	public List selectTFisMstFicdDt(Map parameterMap) throws Exception {
		return selectList("com.vinflux.fis.persistence.CommonFisDAO.selectTFisMstFicdDt",parameterMap);
	}
}