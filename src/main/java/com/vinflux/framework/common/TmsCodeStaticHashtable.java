package com.vinflux.framework.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TmsCodeStaticHashtable {
	public static ConcurrentHashMap<String, HashMap<String, String>> tmsMappingTable = new ConcurrentHashMap<String, HashMap<String, String>>();

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(TmsCodeStaticHashtable.class);
	
	/**
	 * Mapping Table에서 value를 추출
	 * 
	 * @param CTKEY
	 * @param TMCD_HDKEY
	 * @param TMCD_DTKEY
	 * @param LAKEY
	 * @return
	 */
	public static Map<String, String> getData(String TMCD_HDKEY, String TMCD_DTKEY, String LAKEY) {
		String key = TMCD_HDKEY + TMCD_DTKEY + LAKEY;
		HashMap<String, String> resData = tmsMappingTable.get(key);
		if (resData == null || resData.isEmpty()) {
			resData = new HashMap();
			resData.put("tmcd_dtname", TMCD_DTKEY);
		}
		return resData;
	}

	/**
	 * DB가 아닌  단일 개의 데이터를 Mapping Table에 저장
	 * 
	 * @param CTKEY
	 * @param CTCD_HDKEY
	 * @param CTCD_DTKEY
	 * @param CTCD_DTORDER
	 * @param CTCD_DTNAME
	 * @param CTCD_DTVALUE
	 * @param LAKEY
	 * @return
	 */
	public static boolean setData(String TMCD_HDKEY, String TMCD_DTKEY, String TMCD_DTORDER, String TMCD_DTNAME, String TMCD_DTVALUE,
			String LAKEY) {
		boolean result = false;

		try {
			String key = TMCD_HDKEY + TMCD_DTKEY + LAKEY;
			HashMap<String, String> dataMap = new HashMap<String, String>();

			dataMap.put("tmcd_hdkey", TMCD_HDKEY);
			dataMap.put("tmcd_dtkey", TMCD_DTKEY);
			dataMap.put("tmcd_dtorder", TMCD_DTORDER);
			dataMap.put("tmcd_dtname", TMCD_DTNAME);
			dataMap.put("tmcd_dtvalue", TMCD_DTVALUE);
			dataMap.put("lakey", LAKEY);

			tmsMappingTable.put(key, dataMap);
			result = true;
		}
		catch (Exception e) {
			LOG.error("", e);
		}
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. List에서 HashMap을 추출
	 * 2. HashMap에서 Key 요소가 되는 데이터(CTKEY, CTCD_HDKEY, CTCD_DTKEY, LAKEY)를 추출
	 * 3. Key 생성 후 HashMap Object에 적제
	 * 4. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData(List<HashMap<String, String>> dataList) {
		boolean result = false;
		for (int i = 0; i < dataList.size(); i++) {
			HashMap<String, String> dataMap = dataList.get(i);
			try {
				String key = dataMap.get("tmcd_hdkey") + dataMap.get("tmcd_dtkey") + dataMap.get("lakey");
				tmsMappingTable.put(key, dataMap);
			}
			catch (Exception e) {
				return result;
			}
		}
		result = true;
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. HashMap에서 Key 요소가 되는 데이터(CTKEY, CTCD_HDKEY, CTCD_DTKEY, LAKEY)를 추출
	 * 2. Key 생성 후 HashMap Object에 적제
	 * 3. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData(Map<String, String> dataMap) {
		boolean result = false;
		try {
			String key = dataMap.get("tmcd_hdkey") + dataMap.get("tmcd_dtkey") + dataMap.get("lakey");
			LOG.debug(key);
			tmsMappingTable.put(key, (HashMap<String, String>)dataMap);
		}
		catch (Exception e) {
			LOG.error(e);
		}
		result = true;
		return result;
	}
	
	/**
	 * Mapping ConcurrentHashMap 의 보유 데이터 count 추출.
	 * @return int
	 */
	public static int getDataSize() {
		
		Iterator itr = tmsMappingTable.keySet().iterator();
		int i = 0;
		while (itr.hasNext()) {
			i++;
			itr.next();
		}
		
		return i;
	}	
	
	/**
	 * Mapping ConcurrentHashMap Clear.
	 * @param 
	 * @return
	 */
	public static void clear() {	
		tmsMappingTable.clear();
	}		
}
