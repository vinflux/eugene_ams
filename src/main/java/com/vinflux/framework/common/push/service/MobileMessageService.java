package com.vinflux.framework.common.push.service;

public interface MobileMessageService {
    public void pushNotify() throws Exception;
    public void sendMsgIos(String deviceToken, String msg) throws Exception;
}
