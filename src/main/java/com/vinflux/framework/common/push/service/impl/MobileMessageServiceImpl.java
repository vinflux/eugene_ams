package com.vinflux.framework.common.push.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.vinflux.adm.push.service.MqttService;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.push.service.MobileMessageService;
import com.vinflux.vims.persistence.MessageVimsDAO;

@Service("mobileMessageService")
public class MobileMessageServiceImpl implements MobileMessageService {

	protected static final Log LOG = LogFactory.getLog(MobileMessageServiceImpl.class);

	@Resource(name = "mqttService")
	private MqttService mqttService;
	
	@Resource(name = "messageVimsDAO")
	private MessageVimsDAO messageVimsDAO;
	
//	@Resource(name="amfClient")
//    private AmfClient amfClient;

	

	private String createMsgJson(String title, String message) {
		CommandMap commandMap = new CommandMap();
		Map messageData = new HashMap();
		messageData.put("title", title);
		messageData.put("message", message);
		commandMap.putMap("MSG_DATA", messageData);
		return JSONObject.toJSONString(commandMap);
	}
	
	@Override
	public void pushNotify() throws Exception {
//		String seq;
		String eqtype;
		String pushid;
		String title;
		String message;
//		String sendyn;
//		String strInsDate;
//		Long paramInseDate = 0L;
//		Long insDate = 0L;
		List<Map> pushList = messageVimsDAO.selectTviewIfAlertList();
		
		if ( pushList != null && !pushList.isEmpty() ) {
			this.messageVimsDAO.updateTviewIfAlertSENDYNBySeq(pushList);
			
	//		for(int i=0 ; i < pushList.size() ; i++){			
	//			insDate = Long.parseLong(pushList.get(i).get("insertdate").toString());
	//			
	//			if(insDate > paramInseDate) {
	//				paramInseDate = insDate;
	//			}
	//		}
	//		strInsDate = Long.toString(paramInseDate);
	//		messageVimsDAO.updateTviewIfAlertSENDYN(strInsDate);
			
			for(int i=0 ; i < pushList.size() ; i++){
//				seq = pushList.get(i).get("seq").toString();		// 처리가 주석처리 되어 있음.
				eqtype = pushList.get(i).get("eqtype").toString();
				pushid = pushList.get(i).get("pushid").toString();
				title = pushList.get(i).get("title").toString();
				message = pushList.get(i).get("pushmsg").toString();
//				sendyn = pushList.get(i).get("sendyn").toString();			// 처리되는 내용이 없음.	
				if("40".equals(eqtype)){
					this.mqttService.sendMsg(pushid, createMsgJson(title, message));
				}else if("30".equals(eqtype)){
					this.sendMsgIos(pushid, message);
				}
	//			parameterMap.put("seq", seq);
	//			messageVimsDAO.updateTviewIfAlertSENDYN(seq);
			}
		}
	}
	
	@Override
	public void sendMsgIos(String deviceToken, String msg) throws Exception {
	    //LOG.debug(this.getClass().getResourceAsStream("/com/vinflux/tms/mobile/cert/iphone_dev.p12"));
	    InputStream isCert = this.getClass().getResourceAsStream("/com/vinflux/tms/mobile/cert/iphone_dev.p12");
		ApnsService pushService = APNS.newService()
				.withCert(isCert, "mbsys")
	            .withProductionDestination()
	            .build();
		String payload = APNS.newPayload().alertBody(msg).sound("default").build();
        String token = deviceToken;
        pushService.testConnection();
        pushService.push(token, payload);
	}
}