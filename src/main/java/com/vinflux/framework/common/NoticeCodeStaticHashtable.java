package com.vinflux.framework.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NoticeCodeStaticHashtable {
	public static Map<String, String> noticeStaticData = new HashMap<String, String>();

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(NoticeCodeStaticHashtable.class);
	
	public static Map getData() {
		return noticeStaticData  ;
	}

	public static void setData(String notice , String updatedate) {
		noticeStaticData.put("NOTICE_DATA" , notice);
		noticeStaticData.put("CHECKDATE" , updatedate);
	}
}