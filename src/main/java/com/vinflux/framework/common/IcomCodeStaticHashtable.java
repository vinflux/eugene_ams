package com.vinflux.framework.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IcomCodeStaticHashtable {
	public static ConcurrentHashMap<String, HashMap<String, String>> icomMappingTable = new ConcurrentHashMap<String, HashMap<String, String>>();

	/** LOG */
	protected static final Log LOG = LogFactory.getLog(IcomCodeStaticHashtable.class);
	
	/**
	 * Mapping Table에서 value를 추출
	 * 
	 * @param ORCD_HDKEY
	 * @param ORCD_DTKEY
	 * @param LAKEY
	 * @return
	 */
	public static Map<String, String> getData(String ORCD_HDKEY, String ORCD_DTKEY, String LAKEY) {
		String key = ORCD_HDKEY + ORCD_DTKEY + LAKEY;
		HashMap<String, String> resData = icomMappingTable.get(key);
		if (resData == null || resData.isEmpty()) {
			resData = new HashMap();
			resData.put("orcd_dtname", ORCD_DTKEY);
		}
		return resData;
	}

	/**
	 * DB가 아닌  단일 개의 데이터를 Mapping Table에 저장
	 * 
	 * @param ORCD_HDKEY
	 * @param ORCD_DTKEY
	 * @param ORCD_DTORDER
	 * @param ORCD_DTNAME
	 * @param ORCD_DTVALUE
	 * @param LAKEY
	 * @return
	 */

	public static boolean setData(String ORCD_HDKEY, String ORCD_DTKEY, String ORCD_DTORDER, String ORCD_DTNAME, String ORCD_DTVALUE, String LAKEY) {
		boolean result = false;

		try {
			String key = ORCD_HDKEY + ORCD_DTKEY + LAKEY;
			HashMap<String, String> dataMap = new HashMap<String, String>();

			dataMap.put("orcd_hdkey", ORCD_HDKEY);
			dataMap.put("orcd_dtkey", ORCD_DTKEY);
			dataMap.put("orcd_dtorder", ORCD_DTORDER);
			dataMap.put("orcd_dtname", ORCD_DTNAME);
			dataMap.put("orcd_dtvalue", ORCD_DTVALUE);
			dataMap.put("lakey", LAKEY);

			icomMappingTable.put(key, dataMap);
			result = true;
		}
		catch (Exception e) {
			LOG.error("", e);
		}
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. List에서 HashMap을 추출
	 * 2. HashMap에서 Key 요소가 되는 데이터(ORCD_HDKEY, ORCD_DTKEY, LAKEY)를 추출
	 * 3. Key 생성 후 HashMap Object에 적제
	 * 4. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData(List<HashMap<String, String>> dataList) {
		boolean result = false;
		for (int i = 0; i < dataList.size(); i++) {
			HashMap<String, String> dataMap = dataList.get(i);
			try {
				String key = dataMap.get("orcd_hdkey") + dataMap.get("orcd_dtkey") + dataMap.get("lakey");
				icomMappingTable.put(key, dataMap);
			}
			catch (Exception e) {
				return result;
			}
		}
		result = true;
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. HashMap에서 Key 요소가 되는 데이터(ORCD_HDKEY, ORCD_DTKEY, LAKEY)를 추출
	 * 2. Key 생성 후 HashMap Object에 적제
	 * 3. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData(Map<String, String> dataMap) {
		boolean result = false;
		try {
			String key = dataMap.get("orcd_hdkey") + dataMap.get("orcd_dtkey") + dataMap.get("lakey");
			icomMappingTable.put(key, (HashMap<String, String>)dataMap);
		}
		catch (Exception e) {
			LOG.error( e );
		}
		result = true;
		return result;
	}
	
	/**
	 * Mapping ConcurrentHashMap 의 보유 데이터 count 추출.
	 * @return int
	 */
	public static int getDataSize() {
		
		Iterator itr = icomMappingTable.keySet().iterator();
		int i = 0;
		while (itr.hasNext()) {
			i++;
			itr.next();
		}
		
		return i;
	}	
	
	/**
	 * Mapping ConcurrentHashMap Clear.
	 * @param 
	 * @return
	 */
	public static void clear() {	
		icomMappingTable.clear();
	}	
}
