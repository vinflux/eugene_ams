package com.vinflux.framework.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AdmSysCfgStaticHashtable {
	public static ConcurrentHashMap<String, HashMap<String, String>> admSysConfigMappingTable = new ConcurrentHashMap<String, HashMap<String, String>>();
		
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(AdmSysCfgStaticHashtable.class);
	
	/**
	 * Mapping Table에서 value를 추출
	 * @param key
	 * @return
	 */
	public static Map<String, String> getData(String key) {
		/*
		HashMap<String, String> resData = admSysConfigMappingTable.get(key);
//		if (resData == null || "".equals(resData) ) {
//			resData = new HashMap();
//			resData.put("key", key);
//			resData.put("value1", key);
//		}
		return resData;
		*/
		return admSysConfigMappingTable.get(key);
	}

	/**
	 * Mapping Table에서 List<Map> 형식으로 추출
	 * @param 
	 * @return
	 */
	public static List<HashMap<String, String>> getDataList() {
		List<HashMap<String, String>> list = new ArrayList();
		String key;
		Iterator itr = admSysConfigMappingTable.keySet().iterator();
		while ( itr.hasNext() ) {
			key = (String)itr.next();
			list.add((HashMap)getData(key));
		}
		
		return list;
	}
	
	/**
	 * DB가 아닌  단일 개의 데이터를 Mapping Table에 저장
	 * @param key
	 * @param value1
	 * @param value2
	 * @return
	 */
	public static boolean setData(String key, String value1, String value2) {
		boolean result = false;
		try {
			HashMap<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("key", key);
			dataMap.put("value1", value1);
			dataMap.put("value2", value2);
			admSysConfigMappingTable.put(key, dataMap);
			result = true;
		} catch (Exception e) {
			LOG.error("", e);
		}
		LOG.debug("### AdmSysCfgStaticHashtable=>"+AdmSysCfgStaticHashtable.admSysConfigMappingTable);
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. List에서 HashMap을 추출
	 * 2. HashMap에서 Key 요소가 되는 데이터(urkey)를 추출
	 * 3. Key 생성 후 HashMap Object에 적제
	 * 4. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData(List<HashMap<String, String>> dataList) {
		boolean result = false;
		
		admSysConfigMappingTable.clear();
		for (int i = 0; i < dataList.size(); i++) {
			HashMap<String, String> dataMap = dataList.get(i);
			try {
				String key = dataMap.get("key");
				admSysConfigMappingTable.put(key, dataMap);
			}
			catch (Exception e) {
				return result;
			}
		}
		LOG.debug("### AdmSysCfgStaticHashtable=>"+AdmSysCfgStaticHashtable.admSysConfigMappingTable);
		result = true;
		return result;
	}

	/**
	 * Mapping 대상 테이블의 데이터를 리스트로 가져온 후 적제
	 * 1. HashMap에서 Key 요소가 되는 데이터(urkey)를 추출
	 * 2. Key 생성 후 HashMap Object에 적제
	 * 3. Static MappingTable로 로딩
	 * @param dataList
	 * @return
	 */
	public static boolean setData( Map<String, String> dataMap) {
		boolean result = false;
		try {
			String key = dataMap.get("key") ;
			admSysConfigMappingTable.put(key, (HashMap<String, String>)dataMap);
		}
		catch (Exception e) {
			LOG.error( e );
		}
		LOG.debug("### AdmSysCfgStaticHashtable=>"+AdmSysCfgStaticHashtable.admSysConfigMappingTable);
		result = true;
		return result;
	}
	
	/**
	 * Mapping 데이터 삭제
	 * @param HashMap
	 * @return
	 */
	public static boolean removeData(String key) {
		boolean result = false;
		try {
			admSysConfigMappingTable.remove(key);
		}
		catch (Exception e) {
			LOG.error( e );
		}
		LOG.debug("### AdmSysCfgStaticHashtable=>"+AdmSysCfgStaticHashtable.admSysConfigMappingTable);
		
		result = true;
		return result;
	}	
	
	/**
	 * Mapping ConcurrentHashMap 의 보유 데이터 count 추출.
	 * @return int
	 */
	public static int getDataSize() {
		
		Iterator itr = admSysConfigMappingTable.keySet().iterator();
		int i = 0;
		while (itr.hasNext()) {
			i++;
			itr.next();
		}
		
		return i;
	}	
	
	/**
	 * Mapping ConcurrentHashMap Clear.
	 * @param 
	 * @return
	 */
	public static void clear() {	
		admSysConfigMappingTable.clear();
	}	
}
