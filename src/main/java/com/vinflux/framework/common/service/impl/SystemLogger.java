package com.vinflux.framework.common.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.vinflux.adm.persistence.SysLogDAO;
import com.vinflux.framework.common.SysLogVO;
import com.vinflux.framework.common.service.SystemLog;
import com.vinflux.framework.idgen.IdGenService;

@Service("systemLog")
public class SystemLogger implements SystemLog {
	
	@Resource(name="sysLogKeyGenService")
	private IdGenService sysLogKeyGenService;
	
	@Resource(name = "sysLogDAO")
	private SysLogDAO sysLogDAO;
	
	@Override
	public void insertSystemLog(SysLogVO vo) throws Exception {
		
    	//키 발급
		String sysLogKey = sysLogKeyGenService.getNextStringId();
		
		vo.setSysLogKey(sysLogKey);
		this.sysLogDAO.insertSysLog(vo);	
	}

}
