package com.vinflux.framework.web;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nexacro.spring.data.NexacroFileResult;
import com.nexacro.spring.data.NexacroFirstRowHandler;
import com.nexacro.spring.data.NexacroResult;
import com.nexacro.spring.util.NexacroUtil;
import com.nexacro.xapi.data.PlatformData;
import com.nexacro.xapi.tx.HttpPlatformRequest;
import com.nexacro.xapi.tx.HttpPlatformResponse;
import com.nexacro.xapi.tx.PlatformType;
import com.vinflux.framework.collections.CommandMap;
import com.vinflux.framework.common.EnvConstant;
import com.vinflux.framework.common.EnvProperties;
import com.vinflux.framework.common.SysLogVO;
import com.vinflux.framework.common.service.SystemLog;
import com.vinflux.framework.context.ApplicationContextBean;
import com.vinflux.framework.exception.MsgException;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.ExcelUtil;
import com.vinflux.framework.util.FileUtil;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.JsonUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;
import com.vinflux.framework.util.UbiUtil;

@Controller("nexacroController")
public class NexacroController extends CommandController {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(NexacroController.class);
    
	@Autowired
	private ApplicationContextBean applicationContextBean;
	
	@Resource(name = "systemLog")
	private SystemLog systemLog;
	
	/**
	 * Nexacro 통신 Adapter method
	 * @param requestMap
	 * @return
	 */
	@RequestMapping(value = "/nexacro/handlerAdapter.nx")
	public NexacroResult handlerAdapter(PlatformData platformData
							            , HttpPlatformRequest platformRequest
							            , HttpPlatformResponse platformResponse
							            , NexacroFirstRowHandler firstRowHandler
							            , HttpServletRequest request
							            , HttpServletResponse response ) {		//NOPMD
		
		CommandMap commandMap = NexacroUtil.platformDataToCommandMapConverter(platformData, false);
		
		//fileSettingTest();
		
		CommandMap requestMap;
		
		requestMap = this.sessionCheck(commandMap);
		if(NexacroUtil.isNotNull(requestMap)){
			LOG.debug("### Service Invoke After 1 >> "+requestMap.toString());
			LOG.debug("#######################################################");
		}
		return NexacroUtil.commandMapToNexacroResultConverter(requestMap);
	}

	/**
	 * Nexacro 통신 Adapter method
	 * @param requestMap
	 * @return
	 */
	@RequestMapping(value = "/nexacro/handlerAdapterSave.nx")
	public NexacroResult handlerAdapterSave(PlatformData platformData
							            , HttpPlatformRequest platformRequest
							            , HttpPlatformResponse platformResponse
							            , NexacroFirstRowHandler firstRowHandler
							            , HttpServletRequest request
							            , HttpServletResponse response ) {		//NOPMD
		
		CommandMap commandMap = NexacroUtil.platformDataToCommandMapConverter(platformData, true);
		
		//fileSettingTest();
		
		CommandMap requestMap;
		
		requestMap = sessionCheck(commandMap);
		if(NexacroUtil.isNotNull(requestMap)){
			LOG.debug("### Service Invoke  After 2 >> "+requestMap.toString());
			LOG.debug("#######################################################");
		}
		return NexacroUtil.commandMapToNexacroResultConverter(requestMap);
	}
	
	public void requestInfo(){

//		LOG.debug("Convert Complete >> commandMap : "+commandMap.toString());
//		LOG.debug("#######################################################");
//		
//		LOG.debug("request.getContextPath() : "+request.getContextPath());
//		LOG.debug("request.getLocalAddr() : "+request.getLocalAddr());
//		LOG.debug("request.getLocalName() : "+request.getLocalName());
//		LOG.debug("request.getRemoteAddr() : "+request.getRemoteAddr());
//		LOG.debug("request.getRemoteHost() : "+request.getRemoteHost());
//		LOG.debug("request.getRemoteUser() : "+request.getRemoteUser());
//		LOG.debug("request.getLocalPort() : "+request.getLocalPort());
//		LOG.debug("request.getPathInfo() : "+request.getPathInfo());
//		LOG.debug("request.getAuthType() : "+request.getAuthType());
//		LOG.debug("request.getContentType() : "+request.getContentType());
//		LOG.debug("request.getProtocol() : "+request.getProtocol());
//		LOG.debug("request.getRequestURI() : "+request.getRequestURI());
//		LOG.debug("request.getScheme() : "+request.getScheme());
//		LOG.debug("request.getRequestURL() : "+request.getRequestURL());
//		LOG.debug("request.getServletPath() : "+request.getServletPath());
//		
//		Enumeration enumeration = request.getHeaderNames();
//		while(enumeration.hasMoreElements()){
//			Object object = enumeration.nextElement();
//			LOG.debug("request.HeaderName : "+object.toString());
//			LOG.debug("request.getHeader('"+object.toString()+"') : "+request.getHeader(object.toString()));
//		}	
	}
	
	public void fileSettingTest(){
		
		String sourceWebappPath = "D:/Vinflux/workspace/vinFluxSuite1.0/AMS/src/main/webapp";
		String sourceContext = "/ams";
		String sourceFolder = "/cond";
		String sourceFileName = "/checkVersion.txt";
		
		String targetWebappPath = "D:/Vinflux/workspace/vinFluxSuite1.0/WMS/src/main/webapp";
		String targetContext = "/wms";
		String targetFolder = "/cond";
		String targetFileName = "/checkVersion.txt";
		
		String originalDirPath = sourceWebappPath+sourceContext+sourceFolder;
		String originalFilePath = sourceWebappPath+sourceContext+sourceFileName;

		String targetDirPath = targetWebappPath+targetContext+targetFolder;
		String targetFilePath = targetWebappPath+targetContext+targetFileName;
		try {
			if(FileUtil.checkFileExstByName((targetWebappPath+targetContext), "checkVersion.txt")){
				LOG.debug(">>> checkVersion.txt 이 존재합니다.");
				LOG.debug(">>> 폴더를 생성하지 않습니다.");
			}else{
				LOG.debug(">>> checkVersion.txt 이 존재 하지 않습니다!!!");
				FileUtil.copyDirectory(originalDirPath, targetDirPath);
				LOG.debug(">>> "+targetFolder+" 폴더를 생성 하였습니다.");
				FileUtil.copyFile(originalFilePath, targetFilePath);
					
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/nexacro/jspHandlerAdapter.nx")
	public void jspHandlerAdapter(HttpServletRequest request, HttpServletResponse response){
			
		String xlsParam = request.getParameter("xlsParam");
		String beanID = request.getParameter("BEANID");
		String methodNm = request.getParameter("METHODNM");
		
		LOG.debug("################## [ File DownLoad(JSP) ] ##################");
		
		try {
			ApplicationContext context = applicationContextBean.getApplicationContext();
			
			Object objController = context.getBean( beanID );
			Class cls = objController.getClass();
			
			Map<String, Object> map = new HashMap();
			map.put("xlsParam", xlsParam);
			
			Class[] paramsType = { Map.class, HttpServletResponse.class };
			Object paramsValue[] = { map, response };
			Method method = cls.getDeclaredMethod( methodNm , paramsType);
			method.invoke(objController, paramsValue);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@RequestMapping(value = "/nexacro/ubiHandlerAdapter.nx")
	public void ubiHandlerAdapter(HttpServletRequest request, HttpServletResponse response){
			
    	String udsName = (String)request.getParameter("udsName");
    	String url = (String)request.getParameter("url");
    	LOG.debug("### url => " + url); 
    	try {
    		// JSON parsing 
    		String reportParam = URLDecoder.decode(((String)request.getParameter("reportParam")), "UTF-8");
    		LOG.debug("reportParam => " + reportParam);    	
    		if ( StringUtil.isNotEmpty(reportParam) ) {
            	CommandMap requestMap  = JsonUtil.jsonCovertCommandMap(reportParam);
            	Map paramMap = requestMap.getParamMap();
            	
            	LOG.debug("requsetMap => " + requestMap);       	

            	String printType = MapUtil.getStr(paramMap, "printType");
            	String execType = null;
            	
            	if ( "direct".equals(printType) ) {	//direct print.
            		execType = "TYPE4";
            	} else {	//preview.
            		execType = "TYPE4";
            	}
            	     	
            	String appUrl = EnvConstant.APP_URL_ADM;
            	
            	String context = request.getContextPath();
            	if(NexacroUtil.isNotNull(context)){
            		if(context.indexOf("ams") >= 0){
            			appUrl = EnvConstant.APP_URL_ADM;
            		}else if(context.indexOf("wms") >= 0){
            			appUrl = EnvConstant.APP_URL_WMS;
            		}else if(context.indexOf("tms") >= 0){
            			appUrl = EnvConstant.APP_URL_TMS;
            		}else if(context.indexOf("icom") >= 0){
            			appUrl = EnvConstant.APP_URL_ICOM;
            		}else if(context.indexOf("vims") >= 0){
            			appUrl = EnvConstant.APP_URL_VIMS;
            		}else if(context.indexOf("pms") >= 0){
            			appUrl = EnvConstant.APP_URL_PMS;
            		}else if(context.indexOf("hdms") >= 0){
            			appUrl = EnvConstant.APP_URL_HDMS;
            		}else if(context.indexOf("ems") >= 0){
            			appUrl = EnvConstant.APP_URL_EMS;
            		}else if(context.indexOf("portal") >= 0){
            			appUrl = EnvConstant.APP_URL_PORTAL;
            		}else if(context.indexOf("bbs") >= 0){
            			appUrl = EnvConstant.APP_URL_BBS;
            		}
            	}
            	
            	String jrfDir = appUrl + EnvConstant.UBIREPORT_URL;
            	String jrfName = MapUtil.getStr(paramMap, "jrfName");    	
            	
            	LOG.debug("### printType => " + printType);
            	LOG.debug("### appUrl => " + appUrl);
            	LOG.debug("### jrfDir => " + jrfDir);
            	LOG.debug("### jrfName => " + jrfName);
            	
            	String title = MapUtil.getStr(paramMap, "title");       	       	        
            	LOG.debug("### title => " + title);
            	        	
            	//전송 파라미터로 사용 되기 때문 &연산자가 있으면 안됨.
            	reportParam = StringUtil.replace(reportParam, "&quot;", "\"");
            	        	
            	//getArg() 사용 되는 argument 생성.
            	String strArg = UbiUtil.makeUbiStrArgument(udsName, reportParam);
            	
            	request.setAttribute("title", title);
            	request.setAttribute("appUrl", appUrl);    
            	request.setAttribute("jrfDir", jrfDir);
            	request.setAttribute("jrfName", jrfName);
            	request.setAttribute("strArg", strArg);
            	request.setAttribute("execType", execType);
            	request.setAttribute("printType", printType);
            	LOG.debug("### Call JSP :  => " + appUrl+"/ubireport/sample/ubinonax.jsp");
            	request.getRequestDispatcher("/ubireport/sample/ubinonax.jsp").forward(request, response);
            	
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/nexacro/fileHandlerAdapter.nx")
	public NexacroResult fileHandlerAdapter(PlatformData platformData
							            , HttpPlatformRequest platformRequest
							            , HttpPlatformResponse platformResponse
							            , NexacroFirstRowHandler firstRowHandler
							            , HttpServletRequest request
							            , HttpServletResponse response ) {		//NOPMD
		
		CommandMap commandMap = NexacroUtil.platformDataToCommandMapConverter(platformData, false);
		String processType = commandMap.getParam("EXCEL_FORMAT");
		String fileName = commandMap.getParam("EXCEL_FILENAME");
		String fileExtension = ".xls";
		
		LOG.debug("################## [ File DownLoad(Comp) ] ##################");
		LOG.debug("Convert Complete >> commandMap : "+commandMap.toString());
		
		Map parameterMap = commandMap.getSearchQuery();
    	Map param = commandMap.getParamMap();
    	parameterMap.putAll(param);
    	
    	List<Map> excelFormat = commandMap.getList("EXCEL_FORMAT");
    	
    	Workbook wb = null;
    	
    	ApplicationContext context = applicationContextBean.getApplicationContext();
		
    	String fileBeanID = (String)commandMap.getCommon("FILE_BEANID");
		String fileMethodNm = (String)commandMap.getCommon("FILE_METHODNM");
		
		Object objController = context.getBean( fileBeanID );
		Class cls = objController.getClass();
		
    	try {
    		Class[] paramsType = { Map.class, List.class };
    		Object paramsValue[] = { parameterMap, excelFormat };
    		Method method = cls.getDeclaredMethod( fileMethodNm , paramsType);
    		wb = (Workbook)method.invoke(objController, paramsValue);
    		
    		if (wb == null) {
    			LOG.debug("Workbook Null");
    			//에러 처리
    		}else{
        		if (wb instanceof SXSSFWorkbook) {
        			fileExtension = fileExtension.concat("x");
        		}
    		}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		File file = new File(fileName+fileExtension);
		NexacroFileResult fileResult = new NexacroFileResult(file);
		fileResult.setOriginalName(fileName+fileExtension);
		fileResult.setFileName(fileName);
		fileResult.setFileExtension(fileExtension);
		fileResult.setWorkbook(wb);
		NexacroResult result = new NexacroResult();
		result.setErrorCode(0);
		result.setErrorMsg("");
		result.setFileResult(fileResult);
		
		return result;
	}
	
	@RequestMapping(value = "/nexacro/jspPtlLoginHandlerAdapter.nx")
	public NexacroResult jspPtlLoginHandlerAdapter(PlatformData platformData
							            , HttpPlatformRequest platformRequest
							            , HttpPlatformResponse platformResponse
							            , NexacroFirstRowHandler firstRowHandler
							            , HttpServletRequest request
							            , HttpServletResponse response ) {		//NOPMD
		
		CommandMap commandMap = NexacroUtil.platformDataToCommandMapConverter(platformData, false);
		Map param = commandMap.getCommonMap();
		//fileSettingTest();
		
		CommandMap requestMap = null;
		
		try {
			ApplicationContext context = applicationContextBean.getApplicationContext();
			String beanId = (String)param.get("BEANID");
			String methodNm = (String)param.get("METHODNM");
			
			Object objController = context.getBean(beanId);
			Class cls = objController.getClass();
			
			Class[] paramsType = {CommandMap.class};
			Object paramsValue[] = {commandMap};
			Method method = cls.getDeclaredMethod( methodNm , paramsType);
			requestMap = (CommandMap)method.invoke(objController, paramsValue);
		} catch (Exception e) {
			e.printStackTrace();
			param.put("messageCode", "99999");
			param.put("message", "로그인 실패");  
		}
		return NexacroUtil.commandMapToNexacroResultConverter(requestMap);
	}
	
	public CommandMap sessionCheck(CommandMap requestMap) {		//NOPMD
		
		boolean errorFlag = false;
		String errorMessage = null;	
		CommandMap commandMap = null;
		try {
			commandMap = requestMap;
			String beanID = (String)requestMap.getCommon("BEANID");
			String methodNm = (String)requestMap.getCommon("METHODNM");
			LOG.debug("beanID : "+beanID);
			LOG.debug("methodNm : "+methodNm);
			
			if ( checkMethod(methodNm) ) {
				HttpServletRequest request = HttpUtil.getCurrentRequest();		
		
				String sessionKeepYn = EnvProperties.getProperty("Globals.Session.Keep.YN");											
				SessionVO userVO = null;
				//userInfo 데이터가 없을겨우 session expire	처리.
				Map<String, String> urMap = requestMap.getCommonUserInfoMap();	
				if ( urMap == null ) {
					logger.info("### requestMap=>"+requestMap);				
					logger.info("### session expire");		
					throw new MsgException(99998); //session expire
				}
				try {
					userVO = SessionManager.getSessionVO(request);
					logger.debug("### userVO=>"+userVO.getUrKey());					
				} catch (MsgException e) { 	//session is null 일때.
					//Session Check.
					if ( "Y".equals(sessionKeepYn) ){						
						userVO = requestMap.getCommonUserInfo();
						SessionManager.setUserInfo(request, userVO, "Y");						
					} else {
						throw new MsgException(99998); //session expire				
					}					
				}

			}
			
			ApplicationContext context = applicationContextBean.getApplicationContext();
			
			Object objController = context.getBean( beanID );
			Class cls = objController.getClass();				
		
			Class[] paramsType = { CommandMap.class };
			Object paramsValue[] = { requestMap };
			LOG.debug("methodNm >> getDeclaredMethod : "+methodNm.toString());
			Method method = cls.getDeclaredMethod( methodNm , paramsType);
			
			LOG.debug("method invoke >> objController : "+objController.toString());
			LOG.debug("method invoke >> paramsValue : "+paramsValue.toString());
			
			requestMap = (CommandMap)method.invoke(objController, paramsValue);
			
		} catch (NoSuchBeanDefinitionException e) {		//beanID Search Error
			e.printStackTrace();
			errorFlag = true;
			errorMessage = ExceptionUtils.getStackTrace(e);
			logger.error("### requestMap=>"+ requestMap);
			requestMap = this.handlerException( e );
		} catch (NoSuchMethodException e) {		//실행 메소드 Search Error
			e.printStackTrace();
			errorFlag = true;
			errorMessage = ExceptionUtils.getStackTrace(e);
			logger.error("### requestMap=>"+ requestMap);
			requestMap = this.handlerException( e );
			e.printStackTrace();
		} catch (MsgException e) {
			errorFlag = true;
			errorMessage = e.getLog();
			logger.error("### MSG CODE=>"+ e.getCode());
			logger.error("### requestMap=>"+ requestMap);
			requestMap = this.handlerMsgException( e );						
		} catch (Exception e) {
			e.printStackTrace();
			e.printStackTrace();
			Throwable throwable = e.getCause();
			logger.error("### requestMap=>"+ requestMap);
			logger.error("### Error=>", throwable);
			errorFlag = true;
			if(throwable == null){
				errorMessage = "";
			}else{
				errorMessage = ExceptionUtils.getStackTrace(throwable);
				
				if ( throwable instanceof MsgException ){
					logger.error("### MSG CODE=>"+ ((MsgException)throwable).getCode());	
					requestMap = this.handlerMsgException( (MsgException)throwable );
					errorMessage = ((MsgException)throwable).getLog();
				} else if ( throwable instanceof SQLException ){
					requestMap = this.handlerSQLException( (SQLException)throwable );
				} else if ( throwable instanceof DataAccessException ){
					requestMap = this.handlerDataAccessException( (DataAccessException)throwable );
				} else {
					requestMap = this.handlerException( (Exception)throwable );				
				}
			}
		}

		//에러 발생시 로그 추출 후  error table Insert.
		if ( errorFlag ) {
			try {
				//request Data 추출.
				Writer writer = new StringWriter();
				JsonGenerator jsonGenerator = new JsonFactory().
				createJsonGenerator(writer);
				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(jsonGenerator, commandMap);
				jsonGenerator.close();
				//logger.debug("### requestData=>"+ writer.toString());										
				
				Map<String,String> commonMap = commandMap.getCommonMap();				
				SessionVO sessionVO = commandMap.getCommonUserInfo();
				
				SysLogVO vo = new SysLogVO();
				vo.setCtKey(sessionVO.getCtKey());
				vo.setUsKey(MapUtil.getStr(commonMap, "uskey"));
				vo.setUrKey(sessionVO.getUrKey());
				vo.setMessage(errorMessage);
				vo.setDetailMessage(StringUtils.remove(writer.toString(), "\""));
				logger.debug("###errorMessage=>"+vo.getMessage());
//				logger.debug("###DetailMessage=>"+vo.getDetailMessage());
				this.systemLog.insertSystemLog(vo);
				
			} catch (Exception e) {
				logger.error("##SYSLOG INSERT ERROR!!", e);
			}
		}
		
		return requestMap;
	}
	
	public String mapToStringConverter(Map map){
		StringBuffer sbBuffer = new StringBuffer();
    	if(NexacroUtil.isNotNull(map)){
			Iterator iterMap = map.entrySet().iterator();
			String key ="";
			while(iterMap.hasNext()){
				Entry entry = (Entry) iterMap.next();
				key = ""+entry.getKey().toString()+"";
				LOG.debug("key : "+key);
				if(map.get(key) instanceof List){
					LOG.debug("리스트 찾음!!");
					List list = (List)map.get(key);
					if(NexacroUtil.isNotNull(list) && list.size() > 0){
						//addDataSet(nexacroResult, createDataSet(key.toString(), list));
					}
				}else{
					LOG.debug("리스트가 아님."+map.get(key).toString());
				}
			}
    	}
		return sbBuffer.toString();
	}
	/**
	 * 세션 체크 필요 Url 일 경우 true return
	 * @param url String
	 * @return String[]
	 */
	private boolean checkMethod(String Method) {		
		String[] arrUrl = EnvProperties.getPropertyArray("Globals.Session.No.Check.Method");
		int size = arrUrl.length;
		for (int i=0; i<size; i++){
			if ( Method.equals(arrUrl[i]) ) {
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		String testVale = "%7B%22PARAM%22%3A%7B%22pagingLimit%22%3A50%2C%22currentPage%22%3A%221%22%2C%22EXCEL_FILENAME%22%3A%22%5BAdminCode%5D_%5B20161206%5D_%5B161605%5D%22%7D%2C%22MAP%22%3A%7B%7D%2C%22LIST%22%3A%7B%22EXCEL_FORMAT%22%3A%5B%7B%22DATA_FILED%22%3A%22No%22%2C%22FILED_NAME1%22%3A%22NO%22%2C%22FILED_NAME2%22%3A%22NO%22%2C%22FILED_NAME3%22%3A%22NO%22%7D%2C%7B%22DATA_FILED%22%3A%22adcd_hdkey%22%2C%22FILED_NAME1%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%2C%22FILED_NAME2%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%2C%22FILED_NAME3%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%7D%2C%7B%22DATA_FILED%22%3A%22adcd_hdname%22%2C%22FILED_NAME1%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%2C%22FILED_NAME2%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%2C%22FILED_NAME3%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%7D%2C%7B%22DATA_FILED%22%3A%22insertdate%22%2C%22FILED_NAME1%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME2%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME3%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%7D%2C%7B%22DATA_FILED%22%3A%22inserturkey%22%2C%22FILED_NAME1%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME2%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME3%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%7D%2C%7B%22DATA_FILED%22%3A%22updatedate%22%2C%22FILED_NAME1%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME2%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME3%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%7D%2C%7B%22DATA_FILED%22%3A%22updateurkey%22%2C%22FILED_NAME1%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME2%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME3%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22HEADER_CNT%22%3A%221%22%7D%5D%7D%2C%22COMMON%22%3A%7B%22DEVICE%22%3A%22PC%22%2C%22EQTYPE%22%3A%2210%22%2C%22TIMEZONE%22%3A%229%22%2C%22uskey%22%3A%22US00000080%22%2C%22USER_INFO%22%3A%7B%22urKey%22%3A%22eungheon.kim%22%2C%22rstclgst%22%3A%22N%22%2C%22loginStatus%22%3A%22SUCCESS%22%2C%22owkeym%22%3A%22HANSOL%22%2C%22ctKey_desc%22%3A%22%5Cud55c%5Cuc194+%5Cuc13c%5Cud130+2%22%2C%22icgrkey%22%3A%22%22%2C%22dst%22%3A%22N%22%2C%22rstcdiv%22%3A%22N%22%2C%22urGrKey%22%3A%22SYSADM%22%2C%22ctKey%22%3A%22802%22%2C%22SESSION_USERINFO%22%3A%22SESSION_USERINFO%22%2C%22rep_ackey%22%3A%220000000%22%2C%22ipAddress%22%3A%22%22%2C%22expireDateStatus%22%3A%22VALID%22%2C%22usKey%22%3A%22%22%2C%22divcd%22%3A%22%22%2C%22urName%22%3A%22%5Cuae40%5Cuc751%5Cud5cc%22%2C%22usrtyp%22%3A%22%22%2C%22urliloh_key%22%3A%221600009698%22%2C%22loggrpcd%22%3A%22%22%2C%22duplLog%22%3A%22Y%22%2C%22utcHour%22%3A%229%22%2C%22laKey%22%3A%22KOR%22%2C%22utcOffset%22%3A%22%2B0900%22%2C%22utcMinute%22%3A%220%22%7D%2C%22ACTIVE_APP%22%3A%22ADM%22%2C%22OWKEY_AUTH%22%3A%22BGF%2CHANSOL%2CWINFOOD%2C%5Cuc9c0%5Cuc624%5Cuc601%2C11ST%22%2C%22CTKEY_AUTH%22%3A%22801%2C802%2C901%2C902%2C903%2C904%2C905%2C906%2C907%2C908%2C909%2C912%2C913%2C916%2C917%2C918%2C920%2C921%2C923%2C924%2C925%2C926%2C927%2C928%2C929%2C930%2C998%2C999%22%7D%2C%22SEARCHLIST%22%3A%5B%7B%22dbColoum%22%3A%22ADCD_HDKEY%22%2C%22operator%22%3A%22%3D%22%2C%22value%22%3A%22%22%7D%2C%7B%22dbColoum%22%3A%22ADCD_HDNAME%22%2C%22operator%22%3A%22LIKE%22%2C%22value%22%3A%22%22%7D%5D%7D";
		String decodeString = URLDecoder.decode(testVale);
		System.out.println("decodeString : "+decodeString);
		
		String fileName = decodeString.substring((decodeString.indexOf("EXCEL_FILENAME")+17), (decodeString.indexOf("MAP")-4));
		
		System.out.println("FileName : >>> "+fileName+".xlsx");
		try {
			CommandMap testMap  = ExcelUtil.jsonCovertCommandMap(decodeString);
			System.out.println("testMap : "+testMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
