package com.vinflux.framework.idgen.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

//import com.vinflux.framework.common.PmsCodeStaticHashtable;
import com.vinflux.framework.idgen.persistence.IdGenAdmDAO;
import com.vinflux.framework.idgen.persistence.IdGenIcomDAO;
import com.vinflux.framework.idgen.persistence.IdGenWmsDAO;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;

public class PixDtlIncrementidGenService extends AbstractDataIdGenService {
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(PixDtlIncrementidGenService.class);
	
	@Resource(name="idGenAdmDAO")
	private IdGenAdmDAO idGenAdmDAO;
	
	@Resource(name="idGenIcomDAO")
	private IdGenIcomDAO idGenIcomDAO;
	
	@Resource(name="idGenWmsDAO")
	private IdGenWmsDAO idGenWmsDAO;
	
    /**
     * 사용 모듈.
     */
    private String useDB = "ADM";
	
    /**
     * Detail Key 가 사용 되는 테이블
     */
    private String tableName = "TBNAME";

    /**
     * Header Key 컬럼 이름
     */
    private String hdKeyName = "hdKey";
    
    /**
     * Detail Key 컬럼 이름
     */
    private String keyName = "key";     
    
    /**
     * id 증가 수.
     */    
    private int blockSize = 1;
    
 // 자리수로 디폴트는 10자리
    private int cipher = 10;
    
    /**
     * BigDecimal 유형의 ID 제공
     * @return the next id as a BigDecimal.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때
     */
    @Override
	protected BigDecimal getNextBigDecimalIdInner() throws Exception {
    	throw new Exception("Current service doesn't support to generate next BigDecimal id.");
    }

    /**
     * BigDecimal 유형의 ID 제공
     * @return the next id as a BigDecimal.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때
     */
    @Override
	protected BigDecimal getNextBigDecimalIdInner(String key) throws Exception {
    	throw new Exception("Current service doesn't support to generate next BigDecimal id.");
    }
    
    /**
     * long 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때
     */
    @Override
	protected long getNextLongIdInner() throws Exception {
    	throw new Exception("Current service doesn't support to generate next long id.");      
    }
    
    /**
     * long 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때
     */
    @Override
	protected long getNextLongIdInner(String key) throws Exception {
    	throw new Exception("Current service doesn't support to generate next long id.");      
    }
    
    /**
     * BigDecimal 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때Inner()
     */    
    protected BigDecimal getNextBigDecimalDtKey(String hdKeyVal) throws Exception {
    	
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("keyName", keyName);
        paramMap.put("tableName", tableName);
        paramMap.put("hdKeyName", hdKeyName);
        paramMap.put("hdKeyVal", hdKeyVal);
        
        Map keyMap = null;
        if ( "ADM".equals( useDB ) ) {
        	//keyMap = idGenAdmMapper.getDetailKey(keyName, tableName, hdKeyName, hdKeyVal);
        	keyMap = idGenAdmDAO.getDetailKey(paramMap);
        } else if ( "ICOM".equals(useDB) ) {
        	//keyMap = idGenIcomMapper.getDetailKey(keyName, tableName, hdKeyName, hdKeyVal);
        	keyMap = idGenIcomDAO.getDetailKey(paramMap);
        }               
        
        if (keyMap != null ){
        	BigDecimal maxid = BigDecimal.ZERO;
        	int cnt = MapUtil.getInt(keyMap, "cnt");
        	
//         	if (getLogger().isErrorEnabled()) {
//        		getLogger().debug("###maxid=>"+MapUtil.getBigDecimal(keyMap, "maxid"));
//    	    	getLogger().debug("###cnt=>"+cnt);    	    	
//        	}        	
        	
        	if ( cnt == 0 ) {
        		//maxid = BigDecimal.ONE;
        		maxid = new BigDecimal(blockSize);
        	} else {
        		//maxid = ((BigDecimal)MapUtil.getBigDecimal(keyMap, "maxid")).add(BigDecimal.ONE) ;
        		maxid = MapUtil.getBigDecimal(keyMap, "maxid").add(new BigDecimal(blockSize)) ;
        	}
        	
        	return maxid;
        } else {
        	return BigDecimal.ZERO;
        } 
    }
    
    /**
     * BigDecimal 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때Inner()
     */    
    protected BigDecimal getNextBigDecimalDtKey(String ctKey, String hdKeyVal) throws Exception {
    	
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("keyName", keyName);
        paramMap.put("tableName", tableName);
        paramMap.put("hdKeyName", hdKeyName);
        paramMap.put("hdKeyVal", hdKeyVal);
        
        Map keyMap = null;
        if ( "WMS".equals( useDB ) ) {
        	//keyMap = idGenWmsMapper.getWmsDetailKey(keyName, tableName, hdKeyName, hdKeyVal, ctKey);
        	 paramMap.put("ctKey", ctKey);
        	keyMap = idGenWmsDAO.getDetailKey(paramMap);
        }                
        
        if (keyMap != null ){
        	BigDecimal maxid = BigDecimal.ZERO;
        	int cnt = MapUtil.getInt(keyMap, "cnt");
        	
//         	if (getLogger().isErrorEnabled()) {
//        		getLogger().debug("###maxid=>"+MapUtil.getBigDecimal(keyMap, "maxid"));
//    	    	getLogger().debug("###cnt=>"+cnt);    	    	
//        	}        	
        	
        	if ( cnt == 0 ) {
        		//maxid = BigDecimal.ONE;
        		maxid = new BigDecimal(blockSize);
        	} else {
        		//maxid = ((BigDecimal)MapUtil.getBigDecimal(keyMap, "maxid")).add(BigDecimal.ONE) ;
        		maxid = MapUtil.getBigDecimal(keyMap, "maxid").add(new BigDecimal(blockSize)) ;
        	}
        	
        	return maxid;
        } else {
        	return BigDecimal.ZERO;
        } 
    } 
    
//    protected BigDecimal getNextBigDecimalId(String hdKeyVal) throws Exception {
//        
//    	Connection conn = null;
//    	PreparedStatement stmt = null;
//        ResultSet rs = null;
//        
//        try {
//            conn = getConnection();
//            
//            String query = "SELECT MAX("+ keyName +") "
//            		+ ", COUNT("+ keyName +") "
//            		+ " FROM " + tableName
//                    + " WHERE "+ hdKeyName +" = '" + hdKeyVal + "'";
//            
//            if (getLogger().isDebugEnabled())
//                getLogger().debug(
//                    "[IDGeneration Service] Requesting an Id using query: "+query);
//            
//            stmt = conn.prepareStatement(query);
//            rs = stmt.executeQuery();
//            
//            if (rs.next()) {
//            	if ( BigDecimal.ZERO.equals(rs.getBigDecimal(2)) ) {
//            		return BigDecimal.ONE;
//            	} else {
//            		return ((BigDecimal)rs.getBigDecimal(1)).add(BigDecimal.ONE);
//            	}
//            } else {
//                if (getLogger().isErrorEnabled()) {
//                    getLogger().error("[IDGeneration Service] Unable to allocate a block of Ids. Query for Id did not return a value.");
//                }
//                throw new Exception("Unable to allocate a block of Ids. Query for Id did not return a value.");
//            }
//        } catch (SQLException e) {
//            if (getLogger().isErrorEnabled()) {
//                getLogger().error(
//                    "[IDGeneration Service] We can't get a connection. So, unable to allocate a block of Ids.");
//            }
//            throw new Exception("We can't get a connection. So, unable to allocate a block of Ids.",e);
//        } finally {
//        	closeResultSet(rs);
//        	closeStatement(stmt);            	
//        	closeConnection(conn);
//        }     	
//    	
//    	
//    	//return null;
//    }
    
    /**
     * String 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때Inner()
     */
    @Override
    public String getNextStringIdByKey(String hdKeyVal) throws Exception {
     	if (getLogger().isErrorEnabled()) {
    		getLogger().debug("###useDB=>"+useDB);
	    	getLogger().debug("###hdKeyVal=>"+hdKeyVal);
	    	getLogger().debug("###tableName=>"+tableName);
	    	getLogger().debug("###hdKeyName=>"+hdKeyName);
	    	getLogger().debug("###keyName=>"+keyName);
	    	getLogger().debug("###blockSize=>"+blockSize);
	    	getLogger().debug("###cipher=>"+cipher);
    	}
    	
    	String fillChar = "0";
    	BigDecimal id = this.getNextBigDecimalDtKey(hdKeyVal);
    	String strId = StringUtil.fillString(id.toString(), fillChar, cipher); 
    	    	
    	if (getLogger().isErrorEnabled()) {
    		getLogger().debug("###strId=>"+strId);
    	}
    	
    	return strId;
    }
    
    /**
     * String 유형의 ID 제공
     * @return the next id as a long.
     * @throws FdlException
     *         여타이유에 의해 아이디 생성이 불가능 할때Inner()
     */
    @Override
    public String getNextStringIdByKey(String ctKey, String hdKeyVal) throws Exception {
     	if (getLogger().isErrorEnabled()) {
    		getLogger().debug("###useDB=>"+useDB);
	    	getLogger().debug("###hdKeyVal=>"+hdKeyVal);
	    	getLogger().debug("###tableName=>"+tableName);
	    	getLogger().debug("###hdKeyName=>"+hdKeyName);
	    	getLogger().debug("###keyName=>"+keyName);
	    	getLogger().debug("###blockSize=>"+blockSize);
	    	getLogger().debug("###cipher=>"+cipher);
    	}
    	
    	String fillChar = "0";
    	BigDecimal id = this.getNextBigDecimalDtKey(ctKey, hdKeyVal);
    	String strId = StringUtil.fillString(id.toString(), fillChar, cipher); 
    	    	
    	if (getLogger().isErrorEnabled()) {
    		getLogger().debug("###strId=>"+strId);
    	}
    	
    	return strId;
    }
    
    /**
     * 사용 DB.
     * @param useDB
     *        config로 지정되는 정보
     */
    public void setUseDB(String useDB) {
        this.useDB = useDB;
    }
    
    /**
     * Detail ID 가 사용 되는 테이블 이름.
     * @param tableName
     *        config로 지정되는 정보
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    
    /**
     * Header ID 컬럼 이름
     * @param hdKeyName
     *        config로 지정되는 정보
     */
    public void setHdKeyName(String hdKeyName) {
        this.hdKeyName = hdKeyName;
    }  
    
    /**
     * Detail ID 컬럼 이름
     * @param keyName
     *        config로 지정되는 정보
     */
    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }    
    
    /**
     * application Context configuration 에서 blockSize
     * 입력받기
     * @param blockSize
     *        application Context Configuration 에 세팅한
     *        blocksize
     */
    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
    }
    
    /**
     * ID 자리수 
     * @param cipher
     *        config로 지정되는 정보
     */
    public void setCipher(int cipher) {
        this.cipher = cipher;
    }     
    
    
    /**
     * ResultSet Close Method
     * @param rs ResultSet
     */
    private void closeResultSet(ResultSet rs) {			// NOPMD - abstract class 내 정의된 method [ sw.yoo ]
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException e) {
          // ignore
        	LOG.error( e );
        }
      }
    }

    /**
     * Statement Close Method
     * @param ps Statement
     */
    private static void closeStatement(Statement ps) {			// NOPMD - abstract class 내 정의된 method [ sw.yoo ]
      if (ps != null) {
        try {
            ps.close();
        } catch (SQLException e) {
            // ignore
        	LOG.error(e);
        }
      }
    }

    /**
     * Connection Close Method
     * @param conn Connection
     */
    private static void closeConnection(Connection conn) {		// NOPMD - abstract class 내 정의된 method [ sw.yoo ]
      if (conn != null) {
        try {
        	conn.close();
        } catch (SQLException e) {
            // ignore
        	LOG.error( e );
        }
      }
    }
}
