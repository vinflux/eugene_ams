package com.vinflux.framework.idgen.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.AdmAbstractDAO;


@Repository("idGenAdmDAO")
public class IdGenAdmDAO extends AdmAbstractDAO {

	
	public Map getDetailKey(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.framework.idgen.persistence.IdGenAdmDAO.selectDetailKey", paramMap);
	}
}
