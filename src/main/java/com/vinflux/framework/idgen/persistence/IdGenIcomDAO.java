package com.vinflux.framework.idgen.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.IcomAbstractDAO;

@Repository("idGenIcomDAO")
public class IdGenIcomDAO extends IcomAbstractDAO {

	public Map getDetailKey(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.framework.idgen.persistence.IdGenIcomDAO.selectDetailKey", paramMap);
	}
}
