package com.vinflux.framework.idgen.persistence;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.vinflux.framework.db.WmsAbstractDAO;

@Repository("idGenWmsDAO")
public class IdGenWmsDAO extends WmsAbstractDAO {

	public Map getDetailKey(Map paramMap) throws Exception {
		return selectForMap("com.vinflux.framework.idgen.persistence.IdGenWmsDAO.selectDetailKey", paramMap);
	}
}
