package com.vinflux.framework.aop.exception;

import org.aspectj.lang.annotation.Aspect;


/**  
 * @author http://java-sample-program.blogspot.in/  
 */  
@Aspect  
public class AfterThrowingPartyAspect {  

/**  
  * Advice to send thank You Email to all guests, irrespective of whether they were   
  * too drunk or not.  
  * @param joinPoint  
  */  
//	@AfterThrowing(value="(execution(* com.vinflux.*..*.*(..)))", throwing="exception")  
//	public void callCabForDrunkGuests(JoinPoint joinPoint, Exception exception) {  
// 
//		//get method arguments   
//		Object[] args = joinPoint.getArgs();  
//
//		// getting the method argument using Joinpoint API  
//		Object obj = (Object)args[0];  
//		
//		if ( obj instanceof CommandMap ) {
//			System.out.println((CommandMap)obj);
//		} else if ( obj instanceof HashMap ) {
//			System.out.println((CommandMap)obj);
//		}
//		
//		  
//
//	//No need to handle NotOnListException as exceptions thrown by @Before advice never come to @AfterThrowing  
//	}
	
	
}  