package com.vinflux.framework.aop.exception;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.ThrowsAdvice;

public class LogDBThrowsAdvice implements ThrowsAdvice {
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(LogDBThrowsAdvice.class);
	
    public void afterThrowing(Exception ex) throws Throwable {
        LOG.debug("***");
        LOG.debug("Generic Exception Capture");
        LOG.debug("Caught: " + ex.getClass().getName());
        LOG.debug("***\n");
    }

    public void afterThrowing(Method method, Object[] args, Object target,
            IllegalArgumentException ex) throws Throwable {
        LOG.debug("***");
        LOG.debug("IllegalArgumentException Capture");
        LOG.debug("Caught: " + ex.getClass().getName());
        LOG.debug("Method: " + method.getName());
        LOG.debug("***\n");
    }
}