package com.vinflux.framework.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public abstract class VimsAbstractDAO extends AbstractDAO {
	
	/**
	 * Wms 모듈 sqlSessionFactory 지정.
	 */
    @Resource(name = "vinSqlSessionFactory")
    public void setSuperSqlSessionFactory(SqlSessionFactory vinSqlSessionFactory) {
        super.setSqlSessionFactory(vinSqlSessionFactory);
    }
    
	/**
	 * Mybatis Batch 처리를 위해 필요함.
	 */
	@Resource(name = "vinSqlSessionFactory")
	protected SqlSessionFactory vinSqlSessionFactory;
	
	/**
	 * Mybatis Batch insert 처리.
	 * @param queryId
	 * @param list
	 */
	public void insertExecuteBatch(String queryId, List<Map> list) {
		if ( list!=null && !list.isEmpty() ) {
			SqlSession sqlSession = this.vinSqlSessionFactory.openSession(ExecutorType.BATCH);
			
			try {
				int size = list.size();
				for (int i=0; i<size; i++) {
					Map parameterMap = (HashMap)list.get(i);
					sqlSession.insert(queryId , parameterMap);
				}
				sqlSession.commit();
			} finally {
				sqlSession.close();
			}
		}
	}
	
	/**
	 * Mybatis Batch update 처리.
	 * @param queryId
	 * @param list
	 */
	public void updateExecuteBatch(String queryId, List<Map> list) {
		if ( list!=null && !list.isEmpty() ) {
			SqlSession sqlSession = this.vinSqlSessionFactory.openSession(ExecutorType.BATCH);
			
			try {
				int size = list.size();
				for (int i=0; i<size; i++) {
					Map parameterMap = (HashMap)list.get(i);
					sqlSession.update(queryId , parameterMap);
				}
				sqlSession.commit();
			} finally {
				sqlSession.close();
			}
		}
	}
	
	/**
	 * Mybatis Batch delete 처리.
	 * @param queryId
	 * @param list
	 */
	public void deleteExecuteBatch(String queryId, List<Map> list) {
		if ( list!=null && !list.isEmpty() ) {
			SqlSession sqlSession = this.vinSqlSessionFactory.openSession(ExecutorType.BATCH);
			
			try {
				int size = list.size();
				for (int i=0; i<size; i++) {
					Map parameterMap = (HashMap)list.get(i);
					sqlSession.delete(queryId , parameterMap);
				}
				sqlSession.commit();
			} finally {
				sqlSession.close();
			}
		}
	}
}
