package com.vinflux.framework.db;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;

public abstract class IcomAbstractDAO extends AbstractDAO {
    
	/**
	 * ICOM 모듈 sqlSessionFactory 지정.
	 */
    @Resource(name = "vinSqlSessionFactory")
    public void setSuperSqlSessionFactory(SqlSessionFactory vinSqlSessionFactory) {
        super.setSqlSessionFactory(vinSqlSessionFactory);
    }      

}
