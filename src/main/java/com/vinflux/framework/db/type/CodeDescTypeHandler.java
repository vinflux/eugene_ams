package com.vinflux.framework.db.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.TypeHandler;

import com.vinflux.framework.common.CodeStaticHashtable;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.MapUtil;
import com.vinflux.framework.util.StringUtil;

/**
 * @Class Name  : CodeDescTypeHandler.java
 * @Description : mybatis TypeHandler Class
 * 			CenterCode를 기준으로 CenterCodeMappingTable(in memory)에 맵핑된  특정 값을 반환
 * 
 * @Modification Information  
 * <p>
 * <b>NOTE</b>: CenterCode를 기준으로 CenterCodeMappingTable(in memory)에 맵핑된  특정 값을 반환 
 * @author 연구개발팀 박영서
 * @since 2013.10.08
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *   
 *      수정일                 수정자                                       수정내용
 *  -----------  --------    ---------------------------
 *   2013.10.08    박영서                                       최초 생성 
 * 
 * </pre>
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
public class CodeDescTypeHandler implements TypeHandler<String> {

	protected final Log LOG = LogFactory.getLog(getClass());

	@Override
	public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {	// NOPMD - 상위 인터페이스에서 정의된 Overwrite method	
	}

	@Override
	public String getResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		String result = getNullableResult(rs, columnName);

		if (rs.wasNull()) {
			return null;
		}
		else {
			return result;
		}
	}

	@Override
	public String getResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);

		try {
			return this.getDtName(result);
		}
		catch (Exception e) {
			LOG.error("### ERROR result=>["+result+"]", e);
			throw new SQLException();
		}
	}

	public String getDtName(String key) throws Exception {
		HashMap<String, String> resultMap = null;
		
		if ( StringUtil.isEmpty(key) ) {
			return null;
		}
		
		try {
			String ADCD_HDKEY = null;
			String ADCD_DTKEY = null;
			String LAKEY = getLaKey();
			String result[] = key.split(";");
			if (result.length == 2) {
				ADCD_HDKEY = result[0];
				ADCD_DTKEY = result[1];
				String dtkeyArr[] = ADCD_DTKEY.split(",");
				if (dtkeyArr.length > 1 ) {
					String returnValue = "";
					for (int i=0 ; dtkeyArr.length > i ; i++) {
						HashMap<String, String> resultMapRow = (HashMap<String, String>)CodeStaticHashtable.getData(ADCD_HDKEY, dtkeyArr[i], LAKEY);
						String value = MapUtil.getStr(resultMapRow, "adcd_dtname");
						if ("".equals(returnValue) ) { 
							returnValue = value;
						} else {
							returnValue = returnValue.concat(",").concat(value);
						}
					}
					return returnValue;
				} else {
					resultMap = (HashMap<String, String>)CodeStaticHashtable.getData(ADCD_HDKEY, ADCD_DTKEY, LAKEY);
					return resultMap.get("adcd_dtname");
				}
			}
		}
		catch (Exception e) {
			LOG.error("### ERROR key=>["+key+"]", e);
		}
		return null;
	}

	public String getLaKey() throws Exception {
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req);
		return sessionVO.getLaKey();
	}

	public String getCtKey() throws Exception {

		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req);
		return sessionVO.getCtKey();
	}
}
