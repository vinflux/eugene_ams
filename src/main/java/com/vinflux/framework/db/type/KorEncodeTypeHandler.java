package com.vinflux.framework.db.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.TypeHandler;

/**
 * @Class Name  : KorEncodeTypeHandler.java
 * @Description : mybatis TypeHandler Class
 * 			ISO_8859_1로 인코딩된 문자열을 KSC5601로 로 인코딩된 문자열로 번환 한다.
 * 			US7ASCII 인코딩의 한글 변환.
 * 
 * @Modification Information  
 * <p>
 * <b>NOTE</b>: ISO_8859_1로 인코딩된 문자열을 KSC5601로 로 인코딩된 문자열로 번환 한다, US7ASCII 인코딩의 한글 변환.
 * @author 연구개발팀 최순봉
 * @since 2014.11.12
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *   
 *      수정일                 수정자                                       수정내용
 *  -----------  --------    ---------------------------
 *   2014.11.12    최순봉                                       최초 생성 
 * 
 * </pre>
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
public class KorEncodeTypeHandler implements TypeHandler<String> {

	protected final Log LOG = LogFactory.getLog(KorEncodeTypeHandler.class);

	@Override
	public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {		// NOPMD - 상위 인터페이스에서 정의된 Overwrite method
	}

	@Override
	public String getResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		String result = getNullableResult(rs, columnName);

		if (rs.wasNull()) {
			return null;
		}
		else {
			return result;
		}
	}

	@Override
	public String getResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);

		try {
			return this.convertUS7ASCIItoKor(result);
		}
		catch (Exception e) {
			LOG.error("### ERROR result=>["+result+"]", e);
			throw new SQLException();
		}
	}

	/*
	 * Encoding 변한.
	 */
	public String convertUS7ASCIItoKor( String value )  throws Exception {		
		return new String(value.getBytes("8859_1"), "KSC5601");
	}
}
