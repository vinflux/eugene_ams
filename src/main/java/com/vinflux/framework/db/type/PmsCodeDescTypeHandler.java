package com.vinflux.framework.db.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.TypeHandler;

import com.vinflux.framework.common.PmsCodeStaticHashtable;
import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
import com.vinflux.framework.util.HttpUtil;
import com.vinflux.framework.util.StringUtil;

/**
 * @Class Name  : PmsCodeDescTypeHandler.java
 * @Description : mybatis TypeHandler Class
 * 			CenterCode를 기준으로 CenterCodeMappingTable(in memory)에 맵핑된  특정 값을 반환
 * 
 * @Modification Information  
 * <p>
 * <b>NOTE</b>: CenterCode를 기준으로 CenterCodeMappingTable(in memory)에 맵핑된  특정 값을 반환 
 * @author 연구개발팀 박영서
 * @since 2013.10.08
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *   
 *      수정일                 수정자                                       수정내용
 *  -----------  --------    ---------------------------
 *   2013.10.08    박영서                                       최초 생성 
 * 
 * </pre>
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
public class PmsCodeDescTypeHandler implements TypeHandler<String> {

	protected final Log LOG = LogFactory.getLog(getClass());

	@Override
	public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {		// NOPMD - 상위 인터페이스에서 정의된 Overwrite method
	}

	@Override
	public String getResult(ResultSet rs, String columnName) throws SQLException {
		// TODO Auto-generated method stub
		String result = getNullableResult(rs, columnName);

		if (rs.wasNull()) {
			return null;
		}
		else {
			return result;
		}
	}

	@Override
	public String getResult(ResultSet rs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);

		try {
			return this.getDtName(result);
		}
		catch (Exception e) {
			LOG.error("### ERROR result=>["+result+"]", e);
			throw new SQLException();
		}
	}

	public String getDtName(String key) throws Exception {
		HashMap<String, String> resultMap = null;
		
		if ( StringUtil.isEmpty(key) ) {
			return null;
		}
		
		try {
			String PTCD_HDKEY = null;
			String PTCD_DTKEY = null;
			String LAKEY = getLaKey();
			String result[] = key.split(";");

			if (result.length == 2) {
				PTCD_HDKEY = result[0];
				PTCD_DTKEY = result[1];
				resultMap = (HashMap<String, String>)PmsCodeStaticHashtable.getData(PTCD_HDKEY, PTCD_DTKEY, LAKEY);
				return resultMap.get("ptcd_dtname");
			}
		} catch (Exception e) {
			LOG.error("### ERROR key=>["+key+"]", e);
		}
		return null;
	}

	public String getLaKey() throws Exception {
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req);
		return sessionVO.getLaKey();
	}

	public String getCtKey() throws Exception {
		HttpServletRequest req = HttpUtil.getCurrentRequest();
		SessionVO sessionVO = SessionManager.getSessionVO(req);
		return sessionVO.getCtKey();
	}
}
