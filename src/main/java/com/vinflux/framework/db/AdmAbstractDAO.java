package com.vinflux.framework.db;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;

public abstract class AdmAbstractDAO extends AbstractDAO {
    
	/**
	 * ADM 모듈 sqlSessionFactory 지정.
	 */
    @Resource(name = "vinSqlSessionFactory")
    public void setSuperSqlSessionFactory(SqlSessionFactory vinSqlSessionFactory) {
        super.setSqlSessionFactory(vinSqlSessionFactory);
    }      

}
