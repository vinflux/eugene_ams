package com.vinflux.framework.client.mqtt;

import java.nio.charset.Charset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttAsyncClient;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.vinflux.framework.common.EnvProperties;

public class MQTTSender {

	protected static final Log LOG = LogFactory.getLog(MQTTSender.class);
	
	private static final String PROP_NAME_MQTT_HOSTNAME = "Globals.mqtt.hostname";
	private static final String PROP_NAME_MQTT_PORT = "Globals.mqtt.port";

//	private String hostname;
//	private int port;

	private IMqttAsyncClient mqtt;

	public MQTTSender() throws MqttException, NumberFormatException {
		String hostname = EnvProperties.getProperty(PROP_NAME_MQTT_HOSTNAME);
		int port = Integer.parseInt(EnvProperties.getProperty(PROP_NAME_MQTT_PORT));
		mqtt = new MqttAsyncClient("tcp://" + hostname + ":" + port, "WAS-MQTT");
	}

	public void send(MQTTCallback callback) throws MqttException {
		mqtt.connect(new MqttConnectOptions(), callback, new ConnectionHandler());
	}
	
	private class ConnectionHandler implements IMqttActionListener {

		@Override
		public void onSuccess(IMqttToken asyncActionToken) {
			MQTTCallback callback = (MQTTCallback) asyncActionToken
					.getUserContext();
			MqttMessage mqttMessage = new MqttMessage(callback.getMessage()
					.getBytes(Charset.forName("UTF-8")));
			mqttMessage.setQos(1);
			try {
				mqtt.publish(callback.getTopic(), mqttMessage, callback,
						new DeliveryHandler());
			} catch (MqttPersistenceException e) {
				LOG.warn("Send failed.", e);
			} catch (MqttException e) {
				LOG.warn("Send failed.", e);
			}
		}

		@Override
		public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
			LOG.warn("Connection failed to MQTT Broker.");
		}
	}

	private class DeliveryHandler implements IMqttActionListener {

		@Override
		public void onSuccess(IMqttToken asyncActionToken) {
			try {
				mqtt.disconnect();
			} catch (MqttException e) {
				LOG.warn("Send failed.", e);
			}
		}

		@Override
		public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
			MQTTCallback callback = (MQTTCallback) asyncActionToken
					.getUserContext();
			callback.onFailure();
			try {
				mqtt.disconnect();
			} catch (MqttException e) {
				LOG.warn("Send failed.", e);
			}
		}
	}
	
	public static abstract class MQTTCallback {

		private String message;
		private String topic;

		public MQTTCallback(String message, String topic) {
			this.message = message;
			this.topic = topic;
		}

		public String getMessage() {
			return message;
		}

		public String getTopic() {
			return topic;
		}
		
		public abstract void onFailure();

	}

}
