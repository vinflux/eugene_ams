package com.vinflux.batch.schedule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.vinflux.batch.job.JobWorker;

/**
 * Scheduler for handling jobs
 */
@Service
public class Scheduler {
	
	/** LOG */
    protected static final Log LOG = LogFactory.getLog(Scheduler.class);
    
	@Autowired
	@Qualifier("defaultJob")
	private JobWorker defaultJob;
    
	/**
	 * You can opt for cron expression or fixedRate or fixedDelay
	 * <p>
	 * See Spring Framework 3 Reference:
	 * Chapter 25.5 Annotation Support for Scheduling and Asynchronous Execution
	 */

	//@Scheduled(fixedDelay=5000)
	//@Scheduled(fixedRate=60*1000)
	//@Scheduled(cron="1/30 * * * * *") 	//1시간 간격
	@Scheduled(cron="${Batch.Static.MstAdCd.Cron.Value}") 	//properties/globals.properties 참조.	
	public void doSchedule() {
		LOG.debug("### Start ADM schedule");		
			
		//코드 데이터 갱신.
		this.defaultJob.work();
		
		LOG.debug("### End ADM schedule");
	}
}