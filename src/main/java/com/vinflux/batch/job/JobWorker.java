package com.vinflux.batch.job;

public interface JobWorker {
	
	public void work();
	
}