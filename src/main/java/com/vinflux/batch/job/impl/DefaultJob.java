package com.vinflux.batch.job.impl;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.vinflux.batch.job.JobWorker;
import com.vinflux.adm.common.service.StaticManager;

/**
 * A synchronous worker
 */
@Component("defaultJob")
public class DefaultJob implements JobWorker {

	/** LOG */
    protected static final Log LOG = LogFactory.getLog(DefaultJob.class);
    
	@Resource(name="staticManager")
	private StaticManager staticManager;
	
	@Override
	public void work() {

		String threadName = Thread.currentThread().getName();
		LOG.debug("   " + threadName + " has began working.");
	
		
        try {
        	        		
    		//코드 데이터 갱신.
    		this.staticManager.reload();
        		       		        	        
            Thread.sleep(10000); // simulates work
            
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOG.error("", e);
        }
        LOG.debug("   " + threadName + " has completed work.");		
	}

}
