﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide05");
                this.set_classname("CompGuide01");
                this.set_titletext("ProgressBar, Calendar");
                this._setFormPosition(0,0,765,540);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new ProgressBar("Pbr00", "absolute", "140", "75", "400", "17", null, null, this);
            obj.set_taborder("27");
            obj.set_max("100");
            obj.set_min("0");
            obj.set_pos("30");
            obj.style.set_bartype("image");
            obj.style.set_smooth("false");
            obj.style.set_barcolor("transparent");
            obj.style.set_bargradation("linear 0,0 white 100,100 black");
            obj.set_blocksize("20");
            obj.set_blockgap("10");
            this.addChild(obj.name, obj);

            obj = new Button("btntest", "absolute", "607", "75", "120", "20", null, null, this);
            obj.set_taborder("28");
            obj.set_text("ProgressBar Test");
            this.addChild(obj.name, obj);

            obj = new ProgressBar("Pbr01", "absolute", "140", "155", "400", "17", null, null, this);
            obj.set_taborder("29");
            obj.set_max("100");
            obj.set_min("0");
            obj.set_pos("30");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "140", "120", "100", "25", null, null, this);
            obj.set_taborder("30");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Calendar("Calendar00", "absolute", "145", "269", "130", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("33");

            obj = new Calendar("Calendar01", "absolute", "337", "269", "130", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("34");
            obj.set_enable("false");

            obj = new Static("Static12", "absolute", "546", "234", "130", "25", null, null, this);
            obj.set_taborder("40");
            obj.set_text("type = monthonly");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Calendar("Calendar04", "absolute", "546", "274", "163", "186", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("41");
            obj.set_type("monthonly");

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("42");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("43");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "185", null, null, this);
            obj.set_taborder("44");
            obj.set_text("ProgressBar");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "140", "40", "100", "25", null, null, this);
            obj.set_taborder("45");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "213", "765", "1", null, null, this);
            obj.set_taborder("46");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "0", "213", "124", "275", null, null, this);
            obj.set_taborder("47");
            obj.set_text("Calendar");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "145", "234", "100", "25", null, null, this);
            obj.set_taborder("48");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "487", "765", "1", null, null, this);
            obj.set_taborder("49");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "337", "234", "100", "25", null, null, this);
            obj.set_taborder("50");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 765, 540, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("ProgressBar, Calendar");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("CompGuide05.xfdl", function(exports) {

        this.btntest_onclick = function(obj,e){
        	this.Pbr00.set_pos(1);

        	this.setTimer(0, 10);		
        }

        this.CompGuide05_ontimer = function(obj,e)
        {
        	if(e.timerid == 0)
        	{
        		if(this.Pbr00.pos == 100)
        		{
        			this.killTimer(0);		
        		}
        		this.Pbr00.getNumSetter("pos").postInc();
        			
        	}	
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("ontimer", this.CompGuide05_ontimer, this);
            this.btntest.addEventHandler("onclick", this.btntest_onclick, this);

        };

        this.loadIncludeScript("CompGuide05.xfdl", true);

       
    };
}
)();
