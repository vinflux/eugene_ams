﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("Static, Edit, MaskEdit, TextArea");
                this._setFormPosition(0,0,765,540);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static04", "absolute", "135", "35", "100", "25", null, null, this);
            obj.set_taborder("8");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "311", "71", "130", "20", null, null, this);
            obj.set_taborder("10");
            obj.set_text("넥사크로플랫폼");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "311", "35", "100", "25", null, null, this);
            obj.set_taborder("11");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("31");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("32");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "104", null, null, this);
            obj.set_taborder("33");
            obj.set_text("Static");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "132", "765", "1", null, null, this);
            obj.set_taborder("34");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "135", "71", "130", "20", null, null, this);
            obj.set_taborder("35");
            obj.set_text("넥사크로플랫폼");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "135", "138", "100", "25", null, null, this);
            obj.set_taborder("36");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "0", "132", "124", "104", null, null, this);
            obj.set_taborder("39");
            obj.set_text("Edit");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "235", "765", "1", null, null, this);
            obj.set_taborder("40");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "311", "182", "120", "22", null, null, this);
            obj.set_taborder("44");
            obj.set_value("Text");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "311", "141", "140", "25", null, null, this);
            obj.set_taborder("45");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "135", "241", "100", "25", null, null, this);
            obj.set_taborder("46");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "338", "765", "1", null, null, this);
            obj.set_taborder("48");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "311", "244", "140", "25", null, null, this);
            obj.set_taborder("52");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "0", "235", "124", "104", null, null, this);
            obj.set_taborder("53");
            obj.set_text("MaskEdit");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit00", "absolute", "135", "278", "120", "22", null, null, this);
            obj.set_taborder("54");
            obj.set_value("123456789");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit02", "absolute", "311", "278", "120", "22", null, null, this);
            obj.set_taborder("56");
            obj.set_value("123456789");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "0", "338", "765", "1", null, null, this);
            obj.set_taborder("57");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "135", "344", "100", "25", null, null, this);
            obj.set_taborder("58");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "0", "525", "765", "1", null, null, this);
            obj.set_taborder("60");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "311", "347", "140", "25", null, null, this);
            obj.set_taborder("61");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "0", "338", "124", "188", null, null, this);
            obj.set_taborder("62");
            obj.set_text("TextArea");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "135", "386", "120", "116", null, null, this);
            obj.set_taborder("63");
            obj.set_value("Text");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea02", "absolute", "311", "386", "120", "116", null, null, this);
            obj.set_taborder("65");
            obj.set_value("Text");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "135", "182", "120", "22", null, null, this);
            obj.set_taborder("66");
            obj.set_value("Text");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 765, 540, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Static, Edit, MaskEdit, TextArea");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("CompGuide01.xfdl", function(exports) {

        this.Edit01_oneditclick = function(obj,e)
        {
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Edit01.addEventHandler("oneditclick", this.Edit01_oneditclick, this);

        };

        this.loadIncludeScript("CompGuide01.xfdl", true);

       
    };
}
)();
