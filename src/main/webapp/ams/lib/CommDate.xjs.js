﻿//XJS=CommDate.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /*
         ===============================================================================
         ==  Date 관련 공통 함수
         ===============================================================================
         ● gfn_isDate      : 날짜에 대한 형식 체크
         ● gfn_isDate6     : 날짜 형태의 6자리 입력값 인지 체크하는 함수
         ● gfn_isDate8     : 날짜 형태의 8자리 입력값 인지 체크하는 함수
         ● gfn_today       : 해당 PC의 오늘 날짜를 가져온다.
         ● gfn_todayTime   : 해당 PC의 오늘 날짜+시간를 가져온다.
         ● gfn_firstDate   : 해당월의 시작 날짜를 yyyyMMdd형태로 구하기
         ● gfn_lastDate    : 해당월의 마지막 날짜를 yyyyMMdd형태로 구하기
         ● gfn_lastDateNum : 해당월의 마지막 날짜를 숫자로 구하기
         ● gfn_addDate     : 입력된 날자에 OffSet 으로 지정된 만큼의 일을 더한다.
         ● gfn_addMonth    : 입력된 날자에 OffSet 으로 지정된 만큼의 달을 더한다.
         ● gfn_minusMonth  : 입력된 날자에 OffSet 으로 지정된 만큼의 일을 뺀다.
         ● gfn_datetime    : MiPlatform에서 사용하던 Datetime형식으로 변환
         ● gfn_makeDate    : MiPlatform에서 사용하던 Datetime형식으로 변환
         ● gfn_getDiffDay  : 2개의 날짜간의 Day count
         ● gfn_diffMonth   : 두 월간의 차이 월수 계산
         ● gfn_getDay      : 입력된 날자로부터 요일을 구함
         ● gfn_getDayName  : 입력된 날자로부터 요일명을 구함
         ● gfn_strToDate   : String 타입의 형식을 날짜형식으로 변환
         ● gfn_strToDate2  : String 타입의 형식을 날짜형식으로 변환
         ● gfn_isLeapYear  : 윤년여부 확인
         ● gfn_solar2Lunar : 양력을 음력으로 변환해주는 함수 (처리가능 기간  1841 - 2043년)
         ● gfn_lunar2Solar : 음력을 양력으로 변환해주는 함수 (처리가능 기간  1841 - 2043년)
         ● gfn_getHolidays : 양력 nYear에 해당하는 년도의 법정 공휴일(양력) List 모두 구하기
         ● gfn_solarBase   : 각 월별 음력 기준 정보를 처리하는 함수(처리가능 기간  1841 - 2043년) 단, 내부에서 사용하는 함수임
         */

        /**
         * @class 날짜에 대한 형식 체크
         * @param sDate - 검사일자
         * @return 유효성반환 (날짜형식이 아닐경우 FLASE)
         */  
        this.gfn_isDate = function (value)
        {
            if (this.gfn_isNull(value)) {
                return false;
            }

            value = value.split("-").join("");

            if (value.length != 8) {
                return false;
            }

            var year = nexacro.toNumber(value.toString().substr(0, 4));
            var month = nexacro.toNumber(value.toString().substr(4, 2));
            var date = nexacro.toNumber(value.toString().substr(6, 2));

            var dayExpression = new RegExp(/^(19|20)[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/);
            var noExpression = new RegExp(/^[0-9]{8}$/);

            if (!dayExpression.test(value)) {
                return false;
            }

            if (!noExpression.test(value)) {
                return false;
            }

            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                if (date > 31 || date < 1) {
                    return false;
                }
            } else if (month == 2) {
                if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
                    if (date > 29 || date < 1) {
                        return false;
                    }
                } else {
                    if (date > 28 || date < 1) {
                        return false;
                    }
                }
            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                if (date > 30 || date < 1) {
                    return false;
                }
            }

            return true;
        }

        /**
         * @class 입력값이 6자리 년월 형태 인지 체크하는 함수
         * @param strValue(String)
         * @return true = 입력값이 6자리 날짜(yyyyMMdd) 형태일 경우
         */  
        this.gfn_isDate6 = function (strValue)
        {
            if (this.gfn_length(strValue) != 6){
                return false;
            } else if (!this.gfn_isDate(strValue + "01")){
                return false;
            }
            return true;
        }

        /**
         * @class 입력값이 8자리 날짜 형태 인지 체크하는 함수
         * @param strValue(String)
         * @return true = 입력값이 8자리 날짜(yyyyMMdd) 형태일 경우
         */  
        this.gfn_isDate8 = function (strValue)
        {
            if (this.gfn_length(strValue) != 8){
                return false;
            } else if (!this.gfn_isDate(strValue)){
                return false;
            }
            return true;
        }

        /********************************************************************************
         * 오늘 날짜를 가져오는 함수(클라이언트 PC기준)
         * @return String
         ********************************************************************************/
        this.gfn_today = function()
        {
            return this.gfn_getClientDate("YYYYMMDD");
        }

        /********************************************************************************
         * 오늘 날짜시간를 가져오는 함수(클라이언트 PC기준)
         * @return String
         ********************************************************************************/
        this.gfn_toDateTime = function()
        {
            return this.gfn_getClientDate("YYYYMMDDHH24MISS");
        }

        /********************************************************************************
         * 오늘 날짜시간를 가져오는 함수(클라이언트 PC기준)
         * @return String
         ********************************************************************************/
        this.gfn_toDateTime2 = function()
        {
            return this.gfn_getClientDate("YYYYMMDDHH24MI");
        }

        /********************************************************************************
         * 오늘 날짜를 가져오는 함수(클라이언트 PC기준)
         *
         * @author 조효성
         * @since 2012-11-12
         * @version 1.0
         *
         * @param String 리턴할 값을 지정(YYYY, MM, DD, HH, HH24, MI, SS, MIL)
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_getClientDate = function (value)
        {
            var date = new Date();

            if (value.indexOf("YYYY") >= 0) 
            {
                value = value.replace(/YYYY/g, date.getFullYear());
            }

            if (value.indexOf("MM") >= 0) 
            {
                value = value.replace(/MM/g, (date.getMonth() + 1).toString().padLeft(2, "0"));
            }

            if (value.indexOf("DD") >= 0) 
            {
                value = value.replace(/DD/g, date.getDate().toString().padLeft(2, "0"));
            }

            if (value.indexOf("HH24") >= 0) 
            {
                value = value.replace(/HH24/g, date.getHours().toString().padLeft(2, "0"));
            }

            if (value.indexOf("HH") >= 0) 
            {
                value = value.replace(/HH/g, (date.getHours() % 12).toString().padLeft(2, "0"));
            }

            if (value.indexOf("MI") >= 0) 
            {
                value = value.replace(/MI/g, date.getMinutes().toString().padLeft(2, "0"));
            }

            if (value.indexOf("SS") >= 0) 
            {
                value = value.replace(/SS/g, date.getSeconds().toString().padLeft(2, "0"));
            }

            if (value.indexOf("MIL") >= 0) 
            {
                value = value.replace(/MIL/g, date.getMilliseconds().toString().padLeft(3, "0"));
            }

            if (value.indexOf("TIME") >= 0) 
            {
                value = value.replace(/TIME/g, date.getTime().toString().padLeft(3, "0"));
            }

            return value;
        }

        /**********************************************************************************
         * 함수명      : gfn_AddDate
         * 설명        : 입력된 날자에 OffSet 으로 지정된 만큼의 일을 더한다.
         *               Date Type을 String으로 변환
         * argument    : date ('yyyyMMdd' 형태로 표현된 날자)
         *               nOffSet (날짜로부터 증가 감소값. 지정하지 않으면 Default Value = 1 로 적용됩니다)
         * return Type : String
         * return 내용 : Date에 nOffset이 더해진 결과를 'yyyyMMdd'로 표현된 날자.
         **********************************************************************************/
        this.gfn_addDate = function(sDate,nOffSet)
        {
            sDate = sDate.substr(0, 8);
            var oDate = this.gfn_stringToDate(sDate);
            
            var oAddDate = oDate.addDate(nOffSet);
            oDate = new Date(oAddDate);
            return this.gfn_dateFormatString(oDate, "%Y%m%d");
        }

        /**********************************************************************************
         * 함수명      : gfn_AddMonth
         * 설명        : 입력된 날자에 OffSet 으로 지정된 만큼의 달을 더한다.
         *               Date Type을 String으로 변환
         * argument    : date ('yyyyMMdd' 형태로 표현된 날자)
         *               nOffSet (날짜로부터 증가 감소값. 지정하지 않으면 Default Value = 1 로 적용됩니다)
         * return Type : String
         * return 내용 : Date에 nOffset이 더해진 결과를 'yyyyMMdd'로 표현된 날자.
         **********************************************************************************/
        this.gfn_addMonth = function(sDate,nOffSet)
        {
            var oDate = this.gfn_stringToDate(sDate);

            var oAddMonth = oDate.addMonth(nOffSet);
            oDate = new Date(oAddMonth);
            return this.gfn_dateFormatString(oDate, "%Y%m%d");
        }

        /**
         * @class 해당월의 시작 날짜를 yyyyMMdd형태로 구하기
         * @param sDate : yyyyMMdd형태의 날짜 (예 : "20121122")
         * @return - 성공 = yyyyMMdd형태의 시작 날짜 ( 예 : "20121101" )/ - 실패 = ""
         */  
        this.gfn_firstDate = function (sDate)
        {
            if (this.gfn_isNull(sDate)){
                return "";
            }
            
            return sDate.substr(0, 6) + "01";
        }

        /**
         * @class 해당월의 마지막 날짜를 yyyyMMdd형태로 구하기
         * @param sDate : yyyyMMdd형태의 날짜 (예 : "20121122")
         * @return - 성공 = yyyyMMdd형태의 마지막날짜 ( 예 : "20121130" )/ - 실패 = ""
         */  
        this.gfn_lastDate = function (sDate)
        {
            if (this.gfn_isNull(sDate)){
                return "";
            }
            
            var nLastDate = this.gfn_lastDateNum(sDate);
            return sDate.substr(0, 6) + nLastDate.toString();
        }

        /**
         * @class 해당월의 마지막 날짜를 숫자로 구하기
         * @param sDate : yyyyMMdd형태의 날짜 (예 : "20121122")
         * @return - 성공 = 마지막 날짜 숫자값 (예 : 30)/ - 실패 = -1
         */  
        this.gfn_lastDateNum = function (sDate)
        {
            var nMonth,nLastDate;

            if (this.gfn_isNull(sDate)){
                return -1;
            }

            nMonth = parseInt(sDate.substr(4, 2), 10);
            if (nMonth == 1 || nMonth == 3 || nMonth == 5 || nMonth == 7 || nMonth == 8 || nMonth == 10 || nMonth == 12){
                nLastDate = 31;
            } else if (nMonth == 2){
                if (this.gfn_isLeapYear(sDate) == true){
                    nLastDate = 29;
                } else {
                    nLastDate = 28;
                }
            } else {
                nLastDate = 30;
            }

            return nLastDate;
        }

        /**
         * @class MiPlatform에서 사용하던 Datetime형식으로 변환
         *          Date Type을 String으로 변환
         * @param nYear (년도)
         * @param nMonth (월)
         * @param nDate (일)
         * @return String, 조합한 날짜를 리턴
         */  
        this.gfn_datetime = function (nYear,nMonth,nDate)
        {
            if (nYear.toString().trim().length >= 5){
                var sDate = new String(nYear);
                var nYear = sDate.substr(0, 4);
                var nMonth = sDate.substr(4, 2);
                var nDate = ((sDate.substr(6, 2) == "") ? 1 : sDate.substr(6, 2));
                var nHours = ((sDate.substr(8, 2) == "") ? 0 : sDate.substr(8, 2));
                var nMinutes = ((sDate.substr(10, 2) == "") ? 0 : sDate.substr(10, 2));
                var nSeconds = ((sDate.substr(12, 2) == "") ? 0 : sDate.substr(12, 2));

                var objDate = new Date(parseInt(nYear), parseInt(nMonth) - 1, parseInt(nDate), parseInt(nHours), parseInt(nMinutes), parseInt(nSeconds));
            } else {
                var objDate = new Date(parseInt(nYear), parseInt(nMonth) - 1, parseInt(((nDate == null) ? 1 : nDate)));
            }
            
            var strYear = objDate.getYear().toString();
            var strMonth = (objDate.getMonth() + 1).toString();
            var strDate = objDate.getDate().toString();

            if (strMonth.length == 1){
                strMonth = "0" + strMonth;
            }
            if (strDate.length == 1){
                strDate = "0" + strDate;
            }

            return strYear + strMonth + strDate;
        }

        /**
         * @class MiPlatform에서 사용하던 Datetime형식으로 변환
         *          Date Type을 String으로 변환
         * @param nYear (년도)
         * @param nMonth (월)
         * @param nDate (일)
         * @return String, 조합한 날짜를 리턴
         */  
        this.gfn_makeDate = function (nYear,nMonth,nDate)
        {
            if (this.gfn_isNull(nYear) || this.gfn_isNull(nMonth) || this.gfn_isNull(nDate)){
                return "";
            }
            
            var objDate = new Date(nYear, nMonth - 1, nDate);
            var sYear = objDate.getFullYear().toString();
            var sMonth = this.gfn_right("0" + (objDate.getMonth() + 1), 2);
            var sDate = this.gfn_right("0" + objDate.getDate(), 2);
            
            return sYear + sMonth + sDate;
        }

        /**
         * @class 2개의 날짜간의 Day count
         * @param sFdate   시작일자
         * @param sTdate   종료일자
         * @return 양 일자간의 Day count/gfn_getDiffDay("20090808", "20091001")
         */  
        this.gfn_getDiffDay = function (sFdate,sTdate)
        {
            sFdate = new String(sFdate);
            sFdate = sFdate.split(" ").join("").split("-").join("").split("/").join("");
            sTdate = new String(sTdate);
            sTdate = sTdate.split(" ").join("").split("-").join("").split("/").join("");
            
            sFdate = this.gfn_left(sFdate, 8);
            sTdate = this.gfn_left(sTdate, 8);
            
            if (sFdate.length != 8 || sTdate.length != 8
                 || nexacro.isNumeric(sFdate) == false || nexacro.isNumeric(sTdate) == false
                 || this.gfn_getDay(sFdate) == -1 || this.gfn_getDay(sTdate) == -1){
                return null;
            }
            
            var nDiffDate = this.gfn_strToDate(sTdate) - this.gfn_strToDate(sFdate);
            var nDay = 1000 * 60 * 60 * 24;
            
            nDiffDate = parseInt(nDiffDate / nDay);
            if (nDiffDate < 0){
                nDiffDate = nDiffDate - 1;
            } else {
                nDiffDate = nDiffDate + 1;
            }
            
            return nDiffDate;
        }

        /**
         * @class 두 월간의 차이 월수 계산
         * @param sStartDate : yyyyMMdd형태의 From 일자 ( 예 : "20121122" )
         * @param sEndDate   : yyyyMMdd형태의 To 일자 ( 예 : "20131202" )
         * @return - 성공 = 숫자 형태의 차이월수  ( 예 : 10 )
         *             단, sEndDate < sStartDate이면 음수가 return된다.
         * @주의사항 단, sStartDate, sEndDate의 일은 포함하지 않고 계산된다.
         */  
        this.gfn_diffMonth = function (sStartDate,sEndDate)
        {
            var nStartMon,nEndMon;

            if (this.gfn_isNull(sStartDate) || this.gfn_isNull(sEndDate)){
                return NaN;
            }

            nStartMon = parseInt(sStartDate.substr(0, 4), 10) * 12 + parseInt(sStartDate.substr(4, 2), 10);
            nEndMon = parseInt(sEndDate.substr(0, 4), 10) * 12 + parseInt(sEndDate.substr(4, 2), 10);

            return (nEndMon - nStartMon);
        }

        /**
         * @class 입력된 날자로부터 요일을 구함
         * @param sFdate   sDate  8자리 형식으로된 날짜. yyyyMMdd의 형식으로 입력됩니다.
         * @return 요일에 따른 숫자.
         *           0 = 일요일 ~ 6 = 토요일 로 대응됩니다.
         *           오류가 발생할 경우 -1이 Return됩니다.
         */  
        this.gfn_getDay = function (sDate)
        {
            var objDate = this.gfn_strToDate(sDate);
            return objDate.getDay();
        }

        /**
         * @class 입력된 날자로부터 요일명을 구함
         * @param sFdate   sDate  8자리 형식으로된 날짜. yyyyMMdd의 형식으로 입력됩니다.
         * @return 요일명
         */  
        this.gfn_getDayName = function (sDate)
        {
            var objDayName = new Array("일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일");
            var objDate = this.gfn_getDay(sDate);

            return objDayName[objDate];
        }

        /**
         * @class String타입의 형식을 날짜형식으로 변환
         * @param sFdate   sDate  8자리 형식으로된 날짜. yyyyMMdd의 형식으로 입력됩니다.
         * @return Date
         */  
        this.gfn_strToDate = function (sDate)
        {
            if(this.gfn_isNull(sDate)) return;
            
            var nYear = parseInt(sDate.substr(0, 4));
            var nMonth = parseInt(sDate.substr(4, 2)) - 1;
            var nDate = parseInt(sDate.substr(6, 2));

            return new Date(nYear, nMonth, nDate);
        }

        /**
         * @class String타입의 형식을 날짜형식으로 변환
         * @param sFdate   sDate.
         * @return Date
         */  
        this.gfn_strToDate2 = function (value)
        {
            if(this.gfn_isNull(value)) return;
            
            var dt = new Date();
            
            if ( value.length == 4 ) //yyyy
            {
                dt.setFullYear(parseInt(value), 0, 0);
                dt.setHours(0, 0, 0);
                dt.setMilliseconds(0);
            }
            else if ( value.length == 6 ) //yyyyMM
            {
                dt.setFullYear(parseInt(value.substr(0,4)), parseInt(value.substr(4,2))-1, 0);
                dt.setHours(0, 0, 0);
                dt.setMilliseconds(0);
            }
            else if ( value.length == 8 ) //yyyyMMdd
            {
                dt.setFullYear(parseInt(value.substr(0,4)), parseInt(value.substr(4,2))-1, parseInt(value.substr(6,2)));
                dt.setHours(0, 0, 0);
                dt.setMilliseconds(0);
            }
            else if ( value.length == 10 ) //yyyyMMddhh
            {
                dt.setFullYear(parseInt(value.substr(0,4)), parseInt(value.substr(4,2))-1, parseInt(value.substr(6,2)));
                dt.setHours(parseInt(value.substr(8,2)), 0, 0);
                dt.setMilliseconds(0);
            }
            else if ( value.length == 12 )//yyyyMMddhhmm
            {
                dt.setFullYear(parseInt(value.substr(0,4)), parseInt(value.substr(4,2))-1, parseInt(value.substr(6,2)));
                dt.setHours(parseInt(value.substr(8,2)), parseInt(value.substr(10,2)), 0);
                dt.setMilliseconds(0);
            }
            else if ( value.length == 14 ) //yyyyMMddhhmmss
            {
                dt.setFullYear(parseInt(value.substr(0,4)), parseInt(value.substr(4,2))-1, parseInt(value.substr(6,2)));
                dt.setHours(parseInt(value.substr(8,2)), parseInt(value.substr(10,2)), parseInt(value.substr(12,2)));
                dt.setMilliseconds(0);
            }
            
            return dt;
        }

        /**
         * @class 윤년여부 확인
         * @param sDate : yyyyMMdd형태의 날짜 ( 예 : "20121122" )
         * @return sDate가 윤년인 경우 = true
         *           sDate가 윤년이 아닌 경우 = false
         *           sDate가 입력되지 않은 경우 = false
         */  
        this.gfn_isLeapYear = function (sDate)
        {
            var ret;
            var nY;

            if (this.gfn_isNull(sDate)){
                return false;
            }

            nY = parseInt(sDate.substring(0, 4), 10);

            if ((nY % 4) == 0){
                if ((nY % 100) != 0 || (nY % 400) == 0){
                    ret = true;
                } else {
                    ret = false;
                }
            } else {
                ret = false;
            }

            return ret;
        }

        /**
         * @class 양력을 음력으로 변환해주는 함수 (처리가능 기간  1841 - 2043년)
         * @param sDate : yyyyMMdd형태의 날짜 (예 : "20121122")
         * @return return값이 8자리가 아니고 9자리임에 주의
         *           성공 = Flag(1 Byte) + (yyyyMMdd형태의 음력일자)
         *                  (Flag : 평달 = "0", 윤달 = "1")
         *           실패 = "" (1841 ~ 2043 범위 오류시)
         */  
        this.gfn_solar2Lunar = function (sDate)
        {
            var sMd = "31,0,31,30,31,30,31,31,30,31,30,31";
            var aMd = new Array();

            var aBaseInfo = new Array();
            var aDt = new Array();
            var td;
            var td1;
            var td2;
            var mm,m1,m2;
            var nLy,nLm,nLd;
            var sLyoon;
            if (this.gfn_isNull(sDate)){
                return "";
            }

            var sY = parseInt(sDate.substr(0, 4), 10);
            var sM = parseInt(sDate.substr(4, 2), 10);
            var sD = parseInt(sDate.substr(6, 2), 10);
            if (sY < 1841 || sY > 2043){
                return "";
            }

            aBaseInfo = this.gfn_solarBase();
            aMd = sMd.split(",");
            if (this.gfn_isLeapYear(sDate) == true){
                aMd[1] = 29;
            } else {
                aMd[1] = 28;
            }

            td1 = 672069; //672069 = 1840 * 365 + 1840/4 - 1840/100 + 1840/400 + 23  //1840년까지 날수

            // 1841년부터 작년까지의 날수
            td2 = (sY - 1) * 365 + parseInt((sY - 1) / 4) - parseInt((sY - 1) / 100) + parseInt((sY - 1) / 400);

            // 전월까지의 날수를 더함
            for (var i=0; i <= sM - 2; i++){
                td2 = td2 + parseInt(aMd[i]);
            }

            // 현재일까지의 날수를 더함
            td2 = td2 + sD;

            // 양력현재일과 음력 1840년까지의 날수의 차이
            td = td2 - td1 + 1;

            // 1841년부터 음력날수를 계산
            for (var i=0; i<=sY-1841; i++){
                aDt[i] = 0;
                for (j=0; j<=11; j++){
                    switch (parseInt(aBaseInfo[i*12 + j])){
                        case 1: 
                            mm = 29;
                            break;
                        case 2:
                            mm = 30;
                            break;                
                        case 3:
                            mm = 58;    // 29 + 29
                            break;                
                        case 4:
                            mm = 59;    // 29 + 30
                            break;                
                        case 5:
                            mm = 59;    // 30 + 29
                            break;                
                        case 6:
                            mm = 60;    // 30 + 30
                            break;                
                    }
                    aDt[i] = aDt[i] + mm;
                }
            }

            // 1840년 이후의 년도를 계산 - 현재까지의 일수에서 위에서 계산된 1841년부터의 매년 음력일수를 빼가면수 년도를 계산
            nLy = 0;
            do{
                td = td - aDt[nLy];
                nLy = nLy + 1;
            } while (td > aDt[nLy]);

            nLm = 0;
            sLyoon = "0"; //현재월이 윤달임을 표시할 변수 - 기본값 평달
            do{
                if (parseInt(aBaseInfo[nLy * 12 + nLm]) <= 2){
                    mm = parseInt(aBaseInfo[nLy * 12 + nLm]) + 28;
                    if (td > mm){
                        td = td - mm;
                        nLm = nLm + 1;
                    } else {
                        break;
                    }
                } else {
                    switch (parseInt(aBaseInfo[nLy * 12 + nLm])){
                        case 3:
                            m1 = 29;
                            m2 = 29;
                            break;
                        case 4:
                            m1 = 29;
                            m2 = 30;
                            break;
                        case 5:
                            m1 = 30;
                            m2 = 29;
                            break;
                        case 6:
                            m1 = 30;
                            m2 = 30;
                            break;
                    }

                    if (td > m1){
                        td = td - m1;
                        if (td > m2){
                            td = td - m2;
                            nLm = nLm + 1;
                        } else {
                            sLyoon = "1";
                        }
                    } else {
                        break;
                    }
                }
            } while (1);
            
            nLy = nLy + 1841;
            nLm = nLm + 1;
            nLd = td;
            
            return sLyoon + nLy + this.gfn_right("0" + nLm, 2) + this.gfn_right("0" + nLd, 2);
        }

        /**
         * @class 음력을 양력으로 변환해주는 함수 (처리가능 기간  1841 - 2043년)
         * @param sDate : Flag(1 Byte)+yyyyMMdd형태의 음력일자 ( 예 : "020121122" ) ( Flag : 평달 = "0", 윤달 = "1" )
         * @return 성공 = yyyyMMdd형태의 양력일자
         *                  (Flag : 평달 = "0", 윤달 = "1")
         *           실패 = null
         *                  1841 ~ 2043 범위 오류의 경우
         *                  sDate가 9자리가 아닐경우
         *                  sDate의 첫자리 Flag가 "0"도 아니고 "1"도 아닌 경우
         */  
        this.gfn_lunar2Solar = function (sDate)
        {
            var sMd = "31,0,31,30,31,30,31,31,30,31,30,31";
            var aMd = new Array();    
            var aBaseInfo = new Array();    
            
            var nLy, nLm, nLd, sLflag;        // 전해온 음력 인자값을 저장할 년, 월, 일, 윤달여부 임시변수
            var nSy, nSm, nSd;                // 계산된 양력 년, 월, 일을 저장할 변수
            var y1, m1, i, j, y2, y3;    // 임시변수    
            var leap;

            if (this.gfn_isNull(sDate))    return "";
            if (sDate.length != 9)        return "";
            
            sLflag = sDate.substr(0,1);
            nLy = parseInt(sDate.substr(1,4), 10);
            nLm = parseInt(sDate.substr(5,2), 10);
            nLd = parseInt(sDate.substr(7,2), 10);
            if (nLy < 1841 || nLy > 2043)            return "";
            if (sLflag != "0" && sLflag != "1")    return "";
                
            aBaseInfo = this.gfn_solarBase();
            aMd = sMd.split(",");
            if (this.gfn_isLeapYear(sDate.substr(1,8)) == true){                
                aMd[1] = 29;
            } else {
                aMd[1] = 28;    
            }
            y1 = nLy - 1841;
            m1 = nLm - 1;
            leap = 0;
            if (parseInt(aBaseInfo[y1*12 + m1]) > 2){
                leap = this.gfn_isLeapYear(nLy+"0101");
            }
            if (leap == 1){
                switch (parseInt(aBaseInfo[y1*12 + m1])){
                    case 3:
                        mm = 29;
                        break;
                    case 4:
                        mm = 30;
                        break;            
                    case 5:
                        mm = 29;
                        break;            
                    case 6:
                        mm = 30;
                        break;
                }
            } else {
                switch (parseInt(aBaseInfo[y1*12 + m1])){
                    case 1:
                        mm = 29;
                        break;            
                    case 2:
                        mm = 30;
                        break;            
                    case 3:
                        mm = 29;
                        break;            
                    case 4:
                        mm = 29;
                        break;            
                    case 5:
                        mm = 30;
                        break;            
                    case 6:
                        mm = 30;
                        break;            
                }
            }

            td = 0;
            for (i=0; i<=y1-1; i++){
                for(j=0; j<=11; j++){
                    switch (parseInt(aBaseInfo[i*12+j])){
                        case 1:
                            td = td + 29;
                            break;
                        case 2:
                            td = td + 30;
                            break;                
                        case 3:
                            td = td + 58;
                            break;                
                        case 4:
                            td = td + 59;
                            break;                
                        case 5: 
                            td = td + 59;
                            break;                
                        case 6:
                            td = td + 60;
                            break;                
                    }
                }
            }

            for (j=0; j<=m1-1; j++){
                switch (parseInt(aBaseInfo[y1*12+j])){
                    case 1:
                        td = td + 29;
                        break;            
                    case 2:
                        td = td + 30;
                        break;                        
                    case 3:
                        td = td + 58;
                        break;                        
                    case 4:
                        td = td + 59;
                        break;                        
                    case 5:
                        td = td + 59;
                        break;                        
                    case 6:
                        td = td + 60;
                        break;                        
                }
            }

            if (leap == 1){
                switch (parseInt(aBaseInfo[y1*12+m1])){
                    case 3:
                        mm = 29;
                        break;                        
                    case 4:
                        mm = 29;
                        break;                        
                    case 5:
                        mm = 30;
                        break;                        
                    case 6:
                        mm = 30;
                        break;                        
                }
            }
            
            td = td + nLd + 22;
            if (sLflag == "1"){
                switch (parseInt(aBaseInfo[y1*12+m1])){
                    case 3:
                        td = td + 29;
                        break;                        
                    case 4:
                        td = td + 30;
                        break;                        
                    case 5:
                        td = td + 29;
                        break;                        
                    case 6:
                        td = td + 30;
                        break;                        
                }
            }
            
            y1 = 1840;
            do{
                y1 = y1 + 1;
                leap = this.gfn_isLeapYear(y1+"0101");

                if (leap == 1){
                    y2 = 366;
                } else {
                    y2 = 365;
                }
                if (td <= y2){
                    break;
                }
                td = td - y2;
            } while(1);

            nSy = y1;
            aMd[1] = y2 - 337;
            m1 = 0;
            do{
                m1 = m1 + 1;
                if (td <= parseInt(aMd[m1-1])){
                    break;
                }
                td = td - parseInt(aMd[m1-1]);
            } while(1);
            
            nSm = m1;
            nSd = td;
            y3 = nSy;
            td = y3 * 365 + parseInt(y3/4) - parseInt(y3/100) + parseInt(y3/400);
            for (i=0; i<=nSm-1; i++){
                td = td + parseInt(aMd[i]);
            }
            td = td + nSd;
            
            return y3 + this.gfn_right("0" + nSm, 2)+this.gfn_right("0" + nSd, 2);
        }

        /**
         * @class 양력 nYear에 해당하는 년도의 법정 공휴일(양력) List 모두 구하기
         * @param nYear : nYear에 해당하는 년도 (예 : 2012)
         * @return 성공 = 공휴일 List Array ==> 각 Array[i]="yyyyMMdd공휴일명" 으로 return된다.
         *                  (예 : Array[0] = "20120101신정")
         *           실패 = 빈 Array
         */  
        this.gfn_getHolidays = function (nYear)
        {
            var nYear;
            var aHoliday = new Array();

            if (this.gfn_isNull(nYear)){
                return aHoliday;
            }

            // ///// 음력 체크
            // 구정
            aHoliday[0] = this.gfn_lunar2Solar("0" + (nYear - 1) + "1230") + "설날";
            aHoliday[1] = this.gfn_addDate(aHoliday[0], 1) + "설날";
            aHoliday[2] = this.gfn_addDate(aHoliday[1], 1) + "설날";
            // 석가탄신일
            aHoliday[3] = this.gfn_lunar2Solar("0" + nYear + "0408") + "석가탄신일";
            // 추석
            aHoliday[4] = this.gfn_lunar2Solar("0" + nYear + "0814") + "추석";
            aHoliday[5] = this.gfn_addDate(aHoliday[4], 1) + "추석";
            aHoliday[6] = this.gfn_addDate(aHoliday[5], 1) + "추석";
            
            // ///// 양력 체크
            aHoliday[7] = nYear + "0101" + "신정";
            aHoliday[8] = nYear + "0301" + "삼일절";
            aHoliday[9] = nYear + "0505" + "어린이날";
            aHoliday[10] = nYear + "0606" + "현충일";
            aHoliday[11] = nYear + "0815" + "광복절";
            aHoliday[12] = nYear + "1003" + "개천절";
            aHoliday[13] = nYear + "1009" + "한글날";
            aHoliday[14] = nYear + "1225" + "성탄절";

            return aHoliday.sort();
        }

        /**
         * @class 각 월별 음력 기준 정보를 처리하는 함수(처리가능 기간  1841 - 2043년) 단, 내부에서 사용하는 함수임
         * @param None
         * @return 성공 = 음력 기준정보
         */  
        this.gfn_solarBase = function ()
        {
            var kk;

            // 1841
            kk = "1,2,4,1,1,2,1,2,1,2,2,1,";
            kk += "2,2,1,2,1,1,2,1,2,1,2,1,";
            kk += "2,2,2,1,2,1,4,1,2,1,2,1,";
            kk += "2,2,1,2,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,2,1,2,1,2,1,2,1,";
            kk += "2,1,2,1,5,2,1,2,2,1,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,2,3,2,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,1,2,1,1,2,2,1,2,";
            // 1851
            kk += "2,2,1,2,1,1,2,1,2,1,5,2,";
            kk += "2,1,2,2,1,1,2,1,2,1,1,2,";
            kk += "2,1,2,2,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,1,2,5,2,1,2,1,2,";
            kk += "1,1,2,1,2,2,1,2,2,1,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,5,2,1,2,1,2,2,2,";
            kk += "1,2,1,1,2,1,1,2,2,1,2,2,";
            kk += "2,1,2,1,1,2,1,1,2,1,2,2,";
            kk += "2,1,6,1,1,2,1,1,2,1,2,2,";
            // 1861
            kk += "1,2,2,1,2,1,2,1,2,1,1,2,";
            kk += "2,1,2,1,2,2,1,2,2,3,1,2,";
            kk += "1,2,2,1,2,1,2,2,1,2,1,2,";
            kk += "1,1,2,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,1,2,4,1,2,2,1,2,2,1,";
            kk += "2,1,1,2,1,1,2,2,1,2,2,2,";
            kk += "1,2,1,1,2,1,1,2,1,2,2,2,";
            kk += "1,2,2,3,2,1,1,2,1,2,2,1,";
            kk += "2,2,2,1,1,2,1,1,2,1,2,1,";
            kk += "2,2,2,1,2,1,2,1,1,5,2,1,";
            // 1871
            kk += "2,2,1,2,2,1,2,1,2,1,1,2,";
            kk += "1,2,1,2,2,1,2,1,2,2,1,2,";
            kk += "1,1,2,1,2,4,2,1,2,2,1,2,";
            kk += "1,1,2,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,1,2,1,1,2,1,2,2,2,1,";
            kk += "2,2,1,1,5,1,2,1,2,2,1,2,";
            kk += "2,2,1,1,2,1,1,2,1,2,1,2,";
            kk += "2,2,1,2,1,2,1,1,2,1,2,1,";
            kk += "2,2,4,2,1,2,1,1,2,1,2,1,";
            kk += "2,1,2,2,1,2,2,1,2,1,1,2,";
            // 1881
            kk += "1,2,1,2,1,2,5,2,2,1,2,1,";
            kk += "1,2,1,2,1,2,1,2,2,1,2,2,";
            kk += "1,1,2,1,1,2,1,2,2,2,1,2,";
            kk += "2,1,1,2,3,2,1,2,2,1,2,2,";
            kk += "2,1,1,2,1,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,2,1,1,2,1,2,1,2,";
            kk += "2,2,1,5,2,1,1,2,1,2,1,2,";
            kk += "2,1,2,2,1,2,1,1,2,1,2,1,";
            kk += "2,1,2,2,1,2,1,2,1,2,1,2,";
            kk += "1,5,2,1,2,2,1,2,1,2,1,2,";
            // 1891
            kk += "1,2,1,2,1,2,1,2,2,1,2,2,";
            kk += "1,1,2,1,1,5,2,2,1,2,2,2,";
            kk += "1,1,2,1,1,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,1,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,5,1,2,1,2,1,2,1,";
            kk += "2,2,2,1,2,1,1,2,1,2,1,2,";
            kk += "1,2,2,1,2,1,2,1,2,1,2,1,";
            kk += "2,1,5,2,2,1,2,1,2,1,2,1,";
            kk += "2,1,2,1,2,1,2,2,1,2,1,2,";
            kk += "1,2,1,1,2,1,2,5,2,2,1,2,";
            // 1901
            kk += "1,2,1,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,2,1,1,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,3,2,1,1,2,2,1,2,";
            kk += "2,2,1,2,1,1,2,1,1,2,2,1,";
            kk += "2,2,1,2,2,1,1,2,1,2,1,2,";
            kk += "1,2,2,4,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,1,2,2,1,2,1,2,1,";
            kk += "2,1,1,2,2,1,2,1,2,2,1,2,";
            kk += "1,5,1,2,1,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,2,1,2,2,2,1,";
            // 1911
            kk += "2,1,2,1,1,5,1,2,2,1,2,2,";
            kk += "2,1,2,1,1,2,1,1,2,2,1,2,";
            kk += "2,2,1,2,1,1,2,1,1,2,1,2,";
            kk += "2,2,1,2,5,1,2,1,2,1,1,2,";
            kk += "2,1,2,2,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,1,2,2,1,2,1,2,1,";
            kk += "2,3,2,1,2,2,1,2,2,1,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,5,2,2,1,2,2,";
            kk += "1,2,1,1,2,1,1,2,2,1,2,2,";
            // 1921
            kk += "2,1,2,1,1,2,1,1,2,1,2,2,";
            kk += "2,1,2,2,3,2,1,1,2,1,2,2,";
            kk += "1,2,2,1,2,1,2,1,2,1,1,2,";
            kk += "2,1,2,1,2,2,1,2,1,2,1,1,";
            kk += "2,1,2,5,2,1,2,2,1,2,1,2,";
            kk += "1,1,2,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,1,2,2,";
            kk += "1,5,1,2,1,1,2,2,1,2,2,2,";
            kk += "1,2,1,1,2,1,1,2,1,2,2,2,";
            kk += "1,2,2,1,1,5,1,2,1,2,2,1,";
            // 1931
            kk += "2,2,2,1,1,2,1,1,2,1,2,1,";
            kk += "2,2,2,1,2,1,2,1,1,2,1,2,";
            kk += "1,2,2,1,6,1,2,1,2,1,1,2,";
            kk += "1,2,1,2,2,1,2,2,1,2,1,2,";
            kk += "1,1,2,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,4,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,1,2,1,1,2,1,2,2,2,1,";
            kk += "2,2,1,1,2,1,4,1,2,2,1,2,";
            kk += "2,2,1,1,2,1,1,2,1,2,1,2,";
            kk += "2,2,1,2,1,2,1,1,2,1,2,1,";
            // 1941
            kk += "2,2,1,2,2,4,1,1,2,1,2,1,";
            kk += "2,1,2,2,1,2,2,1,2,1,1,2,";
            kk += "1,2,1,2,1,2,2,1,2,2,1,2,";
            kk += "1,1,2,4,1,2,1,2,2,1,2,2,";
            kk += "1,1,2,1,1,2,1,2,2,2,1,2,";
            kk += "2,1,1,2,1,1,2,1,2,2,1,2,";
            kk += "2,5,1,2,1,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,2,1,1,2,1,2,1,2,";
            kk += "2,2,1,2,1,2,3,2,1,2,1,2,";
            kk += "2,1,2,2,1,2,1,1,2,1,2,1,";
            // 1951
            kk += "2,1,2,2,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,4,2,1,2,1,2,1,2,";
            kk += "1,2,1,1,2,2,1,2,2,1,2,2,";
            kk += "1,1,2,1,1,2,1,2,2,1,2,2,";
            kk += "2,1,4,1,1,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,1,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,2,1,1,5,2,1,2,2,";
            kk += "1,2,2,1,2,1,1,2,1,2,1,2,";
            kk += "1,2,2,1,2,1,2,1,2,1,2,1,";
            kk += "2,1,2,1,2,5,2,1,2,1,2,1,";
            // 1961
            kk += "2,1,2,1,2,1,2,2,1,2,1,2,";
            kk += "1,2,1,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,2,3,2,1,2,1,2,2,2,1,";
            kk += "2,1,2,1,1,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,1,1,2,1,1,2,2,1,";
            kk += "2,2,5,2,1,1,2,1,1,2,2,1,";
            kk += "2,2,1,2,2,1,1,2,1,2,1,2,";
            kk += "1,2,2,1,2,1,5,2,1,2,1,2,";
            kk += "1,2,1,2,1,2,2,1,2,1,2,1,";
            kk += "2,1,1,2,2,1,2,1,2,2,1,2,";
            // 1971
            kk += "1,2,1,1,5,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,2,1,1,2,1,1,2,2,2,1,";
            kk += "2,2,1,5,1,2,1,1,2,2,1,2,";
            kk += "2,2,1,2,1,1,2,1,1,2,1,2,";
            kk += "2,2,1,2,1,2,1,5,2,1,1,2,";
            kk += "2,1,2,2,1,2,1,2,1,2,1,1,";
            kk += "2,2,1,2,1,2,2,1,2,1,2,1,";
            kk += "2,1,1,2,1,6,1,2,2,1,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,1,2,2,";
            // 1981
            kk += "1,2,1,1,2,1,1,2,2,1,2,2,";
            kk += "2,1,2,3,2,1,1,2,2,1,2,2,";
            kk += "2,1,2,1,1,2,1,1,2,1,2,2,";
            kk += "2,1,2,2,1,1,2,1,1,5,2,2,";
            kk += "1,2,2,1,2,1,2,1,1,2,1,2,";
            kk += "1,2,2,1,2,2,1,2,1,2,1,1,";
            kk += "2,1,2,2,1,5,2,2,1,2,1,2,";
            kk += "1,1,2,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,1,2,1,2,1,2,2,1,2,2,";
            kk += "1,2,1,1,5,1,2,1,2,2,2,2,";
            // 1991
            kk += "1,2,1,1,2,1,1,2,1,2,2,2,";
            kk += "1,2,2,1,1,2,1,1,2,1,2,2,";
            kk += "1,2,5,2,1,2,1,1,2,1,2,1,";
            kk += "2,2,2,1,2,1,2,1,1,2,1,2,";
            kk += "1,2,2,1,2,2,1,5,2,1,1,2,";
            kk += "1,2,1,2,2,1,2,1,2,2,1,2,";
            kk += "1,1,2,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,1,2,3,2,2,1,2,2,2,1,";
            kk += "2,1,1,2,1,1,2,1,2,2,2,1,";
            kk += "2,2,1,1,2,1,1,2,1,2,2,1,";
            // 2001
            kk += "2,2,2,3,2,1,1,2,1,2,1,2,";
            kk += "2,2,1,2,1,2,1,1,2,1,2,1,";
            kk += "2,2,1,2,2,1,2,1,1,2,1,2,";
            kk += "1,5,2,2,1,2,1,2,2,1,1,2,";
            kk += "1,2,1,2,1,2,2,1,2,2,1,2,";
            kk += "1,1,2,1,2,1,5,2,2,1,2,2,";
            kk += "1,1,2,1,1,2,1,2,2,2,1,2,";
            kk += "2,1,1,2,1,1,2,1,2,2,1,2,";
            kk += "2,2,1,1,5,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,2,1,1,2,1,2,1,2,";
            // 2011
            kk += "2,1,2,2,1,2,1,1,2,1,2,1,";
            kk += "2,1,6,2,1,2,1,1,2,1,2,1,";
            kk += "2,1,2,2,1,2,1,2,1,2,1,2,";
            kk += "1,2,1,2,1,2,1,2,5,2,1,2,";
            kk += "1,2,1,1,2,1,2,2,2,1,2,2,";
            kk += "1,1,2,1,1,2,1,2,2,1,2,2,";
            kk += "2,1,1,2,3,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,1,1,2,1,2,1,2,2,";
            kk += "2,1,2,1,2,1,1,2,1,2,1,2,";
            kk += "2,1,2,5,2,1,1,2,1,2,1,2,";
            // 2021
            kk += "1,2,2,1,2,1,2,1,2,1,2,1,";
            kk += "2,1,2,1,2,2,1,2,1,2,1,2,";
            kk += "1,5,2,1,2,1,2,2,1,2,1,2,";
            kk += "1,2,1,1,2,1,2,2,1,2,2,1,";
            kk += "2,1,2,1,1,5,2,1,2,2,2,1,";
            kk += "2,1,2,1,1,2,1,2,1,2,2,2,";
            kk += "1,2,1,2,1,1,2,1,1,2,2,2,";
            kk += "1,2,2,1,5,1,2,1,1,2,2,1,";
            kk += "2,2,1,2,2,1,1,2,1,1,2,2,";
            kk += "1,2,1,2,2,1,2,1,2,1,2,1,";
            // 2031
            kk += "2,1,5,2,1,2,2,1,2,1,2,1,";
            kk += "2,1,1,2,1,2,2,1,2,2,1,2,";
            kk += "1,2,1,1,2,1,5,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,2,1,2,2,2,1,";
            kk += "2,1,2,1,1,2,1,1,2,2,1,2,";
            kk += "2,2,1,2,1,4,1,1,2,1,2,2,";
            kk += "2,2,1,2,1,1,2,1,1,2,1,2,";
            kk += "2,2,1,2,1,2,1,2,1,1,2,1,";
            kk += "2,2,1,2,5,2,1,2,1,2,1,1,";
            kk += "2,1,2,2,1,2,2,1,2,1,2,1,";
            // 2041
            kk += "2,1,1,2,1,2,2,1,2,2,1,2,";
            kk += "1,5,1,2,1,2,1,2,2,2,1,2,";
            kk += "1,2,1,1,2,1,1,2,2,1,2,2";

            var arr = new Array();
            arr = kk.split(",");

            return arr;
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
