﻿//XJS=CommForm.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************
        * Name       : CmmnForm.xjs
        * @desc      : FORM 관련  및 시스템 공통 함추
        * 작성자    : heobg
        * 작성일    : 2016-05-01
        * 변경사항  :
        ********************************************************************************/ 

        /**
         * 공통 popup
         * @param    : sPopupId - 팝업 ID
         * @param    : sUrl - 팝업 Url
         * @param    : oArg - 팝업 parameter
         * @param    : nWidth - Popup 가로 사이즈
         * @param    : nHeight - Popup 세로 사이즈
         * @param    : sPopupCallback - Modal 일경우 callback 함수
         * @param    : bModeless - Modaless 여부 (Default : false)
         * @return    : N/A  
         */

        this.gfn_popup = function(sPopupId,sUrl,oArg,nWidth,nHeight,sPopupCallback,bModeless,bResizable)
        {
            var nLeft = -1;
            var nTop = -1;
            var bShowTitle = false;
            var bShowStatus = false;
            var bLayered = true;
            var nOpacity = 100;
            var bAutoSize = false;
            //var bResizable = false;
            var bRound = false;
            
            if(this.gfn_isNull(bModeless)) bModeless = false;
            if(this.gfn_isNull(sPopupCallback)) sPopupCallback = "fn_popupAfter";
            if(this.gfn_isNull(bResizable)) bResizable = false;
            var oPopup = application.popupframes[sPopupId];
            
            if(oPopup != null) {
                trace(sPopupId + " === Popup ID 중복 ");
                oPopup.setFocus();
                return;
            }
            
            var sTitleText = "";
            var sOpenalign = "";
            
            if (nLeft == -1 && nTop == -1) {
                sOpenalign = "center middle";
                nLeft = (application.mainframe.width / 2) - Math.round(nWidth / 2);
                nTop  = (application.mainframe.height / 2) - Math.round(nHeight / 2);
            }
            
            var objParentFrame = this.getOwnerFrame();
            
            if(bModeless == true) {
                var sOpenStyle= "showtitlebar=" + bShowTitle + " showstatusbar=" + bShowStatus;
                application.open(sPopupId, sUrl, objParentFrame, oArg, sOpenStyle, nLeft, nTop, nWidth, nHeight, this);
            } else {
                newChild = new nexacro.ChildFrame;
                newChild.init(sPopupId,"absolute", nLeft, nTop, nWidth, nHeight, null, null, sUrl);
        //        newChild.style.set_overlaycolor("#ffffff65");
                newChild.set_dragmovetype("all");
                newChild.set_showtitlebar(bShowTitle);
                newChild.set_autosize(bAutoSize);
                newChild.set_resizable(bResizable);
                newChild.set_titletext(sTitleText);
                newChild.set_showstatusbar(bShowStatus);
                newChild.set_openalign(sOpenalign);
                newChild.showModal(objParentFrame, oArg, this, sPopupCallback, true);
        		
        		this.gfn_setLocale(application.popupframes[sPopupId].form); // 시스템 언어 설정
            }
        }

        /**
         * 화면에서 debug 창 호출
         * @return    : N/A  
         */
        this.gfn_showDegug = function()
        {
            this.gfn_popup("popup_debug", "comm::CommDebugPop.xfdl", null, 800, 600, "");
        }

        /**
         * 사용자 정보 찾기
         * @param    : sUsrInfoId - 사용자정보 컬럼 아이디
         * @return    : String
         */
        this.gfn_getUserInfo = function(sUsrInfoId)
        {
            if(this.gfn_isNull(sUsrInfoId)) return "";
            
            var sRtnVal = "";
            var oCol = application.gds_userInfo.getColumnInfo(sUsrInfoId);
            if(oCol != null) {
                sRtnVal = application.gds_userInfo.getColumn(0, sUsrInfoId);
            }
            
            return sRtnVal;
        }

        /**
         * 사용자 정보 찾기
         * @param    : sUsrAuthId - 사용자정보 컬럼 아이디
         * @return    : String
         */
        this.gfn_getUserAuth = function(sUsrAuthId)
        {
            if(this.gfn_isNull(sUsrAuthId)) return "";
            
            var sRtnVal = "";
            var oCol = application.gds_userAuth.getColumnInfo(sUsrAuthId);
            if(oCol != null) {
                sRtnVal = application.gds_userAuth.getColumn(0, sUsrAuthId);
            }
            
            return sRtnVal;
        }

        /**
         * Message 찾기(값만 반환)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : String
         */
        this.gfn_getMsgVal = function(sMsgCd)
        {
            var nRow = application.gds_msg.findRow("MSG_CD", sMsgCd);
            if(nRow >= 0) {
                return this.gfn_isNullEmpty(application.gds_msg.getColumn(nRow, "MSG_TXT"));
            } else {
                return "";
            }
        }

        /**
         * Message 찾기(Array 반환)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : Array
         */
        this.gfn_getMsgToArray = function(sMsgCd)
        {
            var nRow = application.gds_msg.findRow("mulaapmsg_hdkey", sMsgCd);
            if(nRow >= 0) {
                return this.gfn_getRowDataToArray(application.gds_msg, nRow);
            } else {
                return "";
            }
        }

        /* 메시지 변수 치환 함수*/
        this.gfn_getTransMsg = function(sMsg,sTrans)
        {
            sMsg = this.gfn_replaceAll(sMsg, "\\n", String.fromCharCode(10) + String.fromCharCode(13));
            
            var mulLang = sMsg.split("}");
            var aTrans;
            var sParam;
            
            if(this.gfn_isNull(sTrans)) {
        		for(var i = 0 ; i < mulLang.length ; i++){
        			sMsg = this.gfn_replaceAll(sMsg, "#{" + mulLang[i].substr(mulLang[i].indexOf("#{")+2) + "}", application.gds_lang.getColumn(0, mulLang[i].substr(mulLang[i].indexOf("#{")+2)));
        		}
            }else{
        		aTrans = sTrans.split("|");
        		
        		for(var i = 0 ; i < aTrans.length ; i++) {
        			sParam = aTrans[i].split(",");
        			sMsg = this.gfn_replaceAll(sMsg, "#{" + sParam[0] + "}", sParam[1]);
        		}
        	}
        	
        	return sMsg;
        }

        /**
         * 공통 Alert 메시지 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @return    : N/A
         */
        this.gfn_alert = function(sMsgCd,sTrans,sCallback,sBeforeMsg)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans, argBeforeMsg:sBeforeMsg};
            this.gfn_popup("popup_" + sMsgCd, "comm::Alert.xfdl", oArg, 380, 208, sCallback);
        }

        /**
         * 공통 Error Alert 메시지 팝업 호출 ( 코드로 정의되지 못하는 DB 오류 표시할 메시지 예외처리)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : N/A
         */
        this.gfn_errAlert = function(sMsgCd,sTrans,sCallback)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans};
            this.gfn_popup("popup_" + sMsgCd, "comm::Error.xfdl", oArg, 380, 194, sCallback);
        }

        /**
         * 공통 Confirm 메시지 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @param    : sCallback - 공통 메시지 창 호출후 결과를 받을 함수명 (Default : fn_confirmAfter)
         * @return    : N/A
         */
        this.gfn_confirm = function(sMsgCd,sTrans,sCallback)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans};
            this.gfn_popup("popup_" + sMsgCd, "comm::Confirm.xfdl", oArg, 380, 208, sCallback);
        }

        /**
         * 공통 배치작업정보 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @param    : sCallback - 공통 메시지 창 호출후 결과를 받을 함수명 (Default : fn_confirmAfter)
         * @return    : N/A
         */
        this.gfn_workInfo = function(sWorkCnt,sSucCnt,sFailCnt,sFailWorkList)
        {
            var oArg = {workCnt:sWorkCnt, successCnt:sSucCnt, failCnt:sFailCnt, failWorkList:sFailWorkList};
            this.gfn_popup("BatchInfoPop", "comm::BatchInfoPop.xfdl", oArg, 374, 279, "");
        }

        /**
         * 공통 Confirm 메시지 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @param    : sCallback - 공통 메시지 창 호출후 결과를 받을 함수명 (Default : fn_confirmAfter)
         * @return    : N/A
         */
        this.gfn_confirm3Btn = function()
        {
            var oArg = this.gfn_confirm3Btn.arguments;

        	if (oArg.length <= 0 && oArg >= 5) {
        		trace("Error number of arguments!!!");
        		return;
        	}

        	var objArgs = new Object();

        	if (oArg.length == 5) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array && oArg[3] instanceof Function) {
        			objArgs.btnText  = oArg[0];
        			objArgs.msgCode  = oArg[1];
        			objArgs.msgtext  = oArg[2];
        			objArgs.callback = oArg[3];
        			objArgs.ds_srchCondition = oArg[4];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	} else if (oArg.length == 4) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array && oArg[3] instanceof Function) {
        			objArgs.btnText  = oArg[0];
        			objArgs.msgCode  = oArg[1];
        			objArgs.msgtext  = oArg[2];
        			objArgs.callback = oArg[3];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	} else if (oArg.length == 3) {
        		if (typeof(oArg[0]) == "string" && oArg[1] instanceof Array && oArg[2] instanceof Function) {
        			objArgs.msgCode  = oArg[0];
        			objArgs.msgtext  = oArg[1];
        			objArgs.callback = oArg[2];
        		} else if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array) {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        			objArgs.msgtext  = oArg[2];
        		} else if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Function) {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        			objArgs.callback  = oArg[2];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        		
        	} else if (oArg.length == 2) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string") {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        		} else if (typeof(oArg[0]) == "string" && oArg[1] instanceof Array) {
        			objArgs.msgCode = oArg[0];
        			objArgs.msgtext = oArg[1];
        		} else if (typeof(oArg[0]) == "string" && oArg[1] instanceof Function) {
        			objArgs.msgCode = oArg[0];
        			objArgs.callback = oArg[1];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}

        	} else if (oArg.length == 1) {
        		if (typeof(oArg[0]) == "string") {
        			objArgs.msgCode = oArg[0];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	}

        	this.gfn_popup("popup_" + objArgs.msgCode, "comm::Confirm3Btn.xfdl", objArgs, 380, 208, objArgs.callback);
        }

        this.gfn_confirm2Btn = function()
        {
            var oArg = this.gfn_confirm2Btn.arguments;

        	if (oArg.length <= 0 && oArg >= 5) {
        		trace("Error number of arguments!!!");
        		return;
        	}

        	var objArgs = new Object();
        	
        	if (oArg.length == 5) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array && oArg[3] instanceof Function) {
        			objArgs.btnText  = oArg[0];
        			objArgs.msgCode  = oArg[1];
        			objArgs.msgtext  = oArg[2];
        			objArgs.callback = oArg[3];
        			objArgs.ds_srchCondition = oArg[4];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	} else if (oArg.length == 4) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array && oArg[3] instanceof Function) {
        			objArgs.btnText  = oArg[0];
        			objArgs.msgCode  = oArg[1];
        			objArgs.msgtext  = oArg[2];
        			objArgs.callback = oArg[3];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	} else if (oArg.length == 3) {
        		if (typeof(oArg[0]) == "string" && oArg[1] instanceof Array && oArg[2] instanceof Function) {
        			objArgs.msgCode  = oArg[0];
        			objArgs.msgtext  = oArg[1];
        			objArgs.callback = oArg[2];
        		} else if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Array) {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        			objArgs.msgtext  = oArg[2];
        		} else if (oArg[0] instanceof Array && typeof(oArg[1]) == "string" && oArg[2] instanceof Function) {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        			objArgs.callback  = oArg[2];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        		
        	} else if (oArg.length == 2) {
        		if (oArg[0] instanceof Array && typeof(oArg[1]) == "string") {
        			objArgs.btnText = oArg[0];
        			objArgs.msgCode = oArg[1];
        		} else if (typeof(oArg[0]) == "string" && oArg[1] instanceof Array) {
        			objArgs.msgCode = oArg[0];
        			objArgs.msgtext = oArg[1];
        		} else if (typeof(oArg[0]) == "string" && oArg[1] instanceof Function) {
        			objArgs.msgCode = oArg[0];
        			objArgs.callback = oArg[1];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}

        	} else if (oArg.length == 1) {
        		if (typeof(oArg[0]) == "string") {
        			objArgs.msgCode = oArg[0];
        		} else {
        			trace("Arguments Error!!!");
        			return;
        		}
        	}

        	this.gfn_popup("popup_" + objArgs.msgCode, "comm::Confirm2Btn.xfdl", objArgs, 380, 208, objArgs.callback);
        }

        
        // 스플릿 시작

        this.gv_split = ""; // 스플릿 변수
        this.splitBtn; // 스플릿 사용 변수
        this.splitBtnObj; // 스플릿 버튼
        this.splitVariable = "";

        /**
         * @class gfn_split
         * @param form - form
                  splitButton - 스플릿 버튼 배열(첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명)
         * @return None
         */

        this.gfn_split = function(form,splitButton)
        {
        	if(this.gfn_isNull(form) || this.gfn_isNull(splitButton)) return;
        	
        	form.addEventHandler("ondrag"    , this._gfn_ondrag    , this);
        	form.addEventHandler("ondragmove", this._gfn_ondragmove, this);
        	form.addEventHandler("ondrop"    , this._gfn_ondrop    , this);
        	
        	this.splitBtn = splitButton;
        }

        /* _gfn_ondrag 실행 */
        this._gfn_ondrag = function(obj,e)
        {
        	for(var i = 0 ; i < this.splitBtn.length ; i++){
        		this.splitVariable = this.splitBtn[i].split("|");
        		this.splitBtnObj = eval(this.splitVariable[3]);
        		
        		if(e.fromreferenceobject == this.splitBtnObj){
        			this.gv_split = "in_" + this.splitBtnObj.name;
        			return true;
        		}
        	}
        }

        /* _gfn_ondragmove 실행 */
        this._gfn_ondragmove = function(obj,e)
        {
        	var sSplit = "";
        	var divObj = "";
        	if(this.gfn_isNull(this.splitBtnObj)) return; 
        	
        	sSplit = "in_" + this.splitBtnObj.name;
        	
        	if(this.gv_split == sSplit && this.splitVariable[2] == "H"){
        		this.gfn_splitControlH(eval(this.splitVariable[0]), eval(this.splitVariable[1]), this.splitBtnObj, e);
        		return;
        	}else if(this.gv_split == sSplit && this.splitVariable[2] == "W"){
        		this.gfn_splitControlW(eval(this.splitVariable[0]), eval(this.splitVariable[1]), this.splitBtnObj, e);
        		return;
        	}else if(this.gv_split == ""){
        		return;
        	}
        }

        /* _gfn_ondrop 실행 */
        this._gfn_ondrop = function(obj,e)
        {
        	this.gv_split = "";
        	this.style.set_cursor("");
        }

        /**
         * @class split 세로
         * @param div01
                  div02
                  btn_split - button
                  e - 이벤트
         * @return None
         */  
        this.gfn_splitControlH = function(div01,div02,btn_split,e)
        {
        	var splitSize = 8;
        	var workSpace = 10;
        	var gridheaderSize = 26;
        	var searchSize = 29;
        	var div01GridTop = 0;
        	var div02GridTop = 0;
        	
        	if(div01.id.substr(0, 3) == "div"){
        		for(var i = 0 ; i < div01.components.length ; i++){
        			if(div01.components[i].id.substr(0, 3) == "grd"){
        				div01GridTop = nexacro.toNumber(div01.components[i].top);
        				break;
        			}
        		}
        	}
        	if(div02.id.substr(0, 3) == "div"){
        		for(var i = 0 ; i < div02.components.length ; i++){
        			if(div02.components[i].id.substr(0, 3) == "grd"){
        				div02GridTop = nexacro.toNumber(div02.components[i].top);
        				break;
        			}
        		}
        	}
        	
        	var objHeight = this.getOffsetHeight();
        	var iTop = objHeight - (nexacro.toNumber(div01.top) + gridheaderSize + searchSize + splitSize + div01GridTop);
        	var iBottom = nexacro.toNumber(div02.bottom) + gridheaderSize + workSpace + div02GridTop;
        	
        	this.style.set_cursor("n-resize");
        	
        	if((objHeight - e.clientY) < iBottom){
        		btn_split.set_bottom(iBottom);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace - nexacro.toNumber(div02.bottom));
        	}else if((objHeight - e.clientY) > iTop){
        		btn_split.set_bottom(iTop);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace - nexacro.toNumber(div02.bottom));
         	}else{
        		btn_split.set_bottom(objHeight - e.clientY);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace - nexacro.toNumber(div02.bottom));
        	}
        }

        /**
         * @class split 가로
         * @param div01
                  div02
                  btn_split - button
                  e - 이벤트
         * @return None
         */  
        this.gfn_splitControlW = function(div01,div02,btn_split,e)
        {
        	var splitSize = 8;
        	
        	var objWidth = (this.getOffsetWidth());
        	var iRight = nexacro.toNumber(div02.right) + splitSize;
        	var iLeft = nexacro.toNumber(div01.left);
        	
        	this.style.set_cursor("e-resize");
        	
        	if(e.clientX < iLeft){
        		btn_split.set_left(iLeft);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + splitSize);
        	}else if(e.clientX > objWidth){
        		btn_split.set_left(objWidth);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + splitSize);
        	}else{
        		btn_split.set_left(e.clientX);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + splitSize);
        	}
        }

        // 스플릿 끝

        /**
         * @class 테마 설정
         * @param strTema - 테마 flag
         * @return None
         */  
        this.gfn_thema = function(strThema)
        {
        	// 테마기본컬러(다크그레이:DG, 블루:BL, 레드:RD, 그린:GR, 네이비:NY, 바이올렛:VT, 청록색:BG, 그레이:GY) gds_admSystemConfig
        	var sysColor = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "THEMA_COLOR"), "value1");
        	var colorGb = "";
        	var sThema = "";
        	
        	if(this.gfn_isNotNull(sysColor)) colorGb = sysColor.substr(0, 1); 
        	else sysColor = "";
        	
        	if(colorGb != "#"){
        		if(sysColor == "DG") sysColor = "";
        		else if(sysColor == "BL") sysColor = "bl_";
        		else if(sysColor == "RD") sysColor = "re_";
        		else if(sysColor == "GR") sysColor = "gn_";
        		else if(sysColor == "NY") sysColor = "nv_";
        		else if(sysColor == "VT") sysColor = "vi_";
        		else if(sysColor == "BG") sysColor = "cy_";
        		else if(sysColor == "GY") sysColor = "gy_";
        	}
        	
            if(this.gfn_isNull(strThema)){
        		sThema = sysColor;
            }else{
        		if(strThema != "dg_") sThema = strThema;
        	}
        	
        	if(colorGb != "#"){
        		application.gv_TopFrame.form.sta_back.set_cssclass("sta_"+sThema+"TF_back");
        		application.gv_TopFrame.form.sta_menu.set_cssclass("sta_"+sThema+"TF_menu");
        		application.gv_TopFrame.form.sta_moduleName.set_cssclass("sta_"+sThema+"TF_module");
        	}else{
        	}
        }

        /**
        * Data의 빈값 확인
        * @param  {object} dsObj - dstaSet
        * @param  {array}  dsCol - dstaSet 컬럼 명
        * @param  {array}  sComp - component 명
        * @return (string) 필수값을 입력하세요.
        */
        this.gfn_isFormDataNullCheck = function(dsObj,dsCol,sComp)
        {
            if(this.gfn_isNull(dsObj)) return false;
            if(this.gfn_isNull(dsCol)) return false;
            if(this.gfn_isNull(sComp)) return false;
            
            var bDsCol = dsCol.split("|");
            var bComp = sComp.split("|");
            var objComp;
            var nullCnt = 0;
            
            for(var i = 0 ; i < bDsCol.length ; i++){
        		if(this.gfn_isNull(dsObj.getColumn(0, bDsCol[i]))){
        			nullCnt++;
        			objComp = eval("this."+bComp[i]);
        			
        			this.gfn_alert("MSG_10001", "", function(msg, flag){
        				objComp.setFocus();
        			});
        			
        			break;
        		}
            }
            
            if(nullCnt > 0) return false;
            else return true;
        }

        //라니안
        this.gfn_ExButtonPop = function(obj,datasetObj)
        {
        	obj.parent.popDiv_ExButton.grd_ExButton.setBindDataset(datasetObj);
        	
        	if(!obj.parent.popDiv_ExButton.isPopup()){
        		
        		var v_nX = system.clientToScreenX(obj.div_splitTop.btn_excel, 0) - system.clientToScreenX(application.mainframe, 0) - obj.parent.popDiv_ExButton.width;
        		var v_nY = system.clientToScreenY(obj.div_splitTop.btn_excel, parseInt(obj.div_splitTop.btn_excel.height)) - system.clientToScreenY(application.mainframe, 0) ;

        		obj.parent.popDiv_ExButton.set_left(v_nX);
        		obj.parent.popDiv_ExButton.set_top(v_nY);
        		obj.parent.popDiv_ExButton.set_visible(true);
        		obj.parent.popDiv_ExButton.trackPopup(v_nX, v_nY);	

        	}
        	
        	var gridItemHeight = 24;
        	var gridGap = 5;
        	if(datasetObj.getRowCount() > 0){
        		obj.parent.popDiv_ExButton.set_height(datasetObj.getRowCount()*gridItemHeight+gridGap);
        		//for(var i=0 ; i < datasetObj.getRowCount() ; i++){
        		//obj.parent.popDiv_ExButton.grd_ExButton.addEventHandler("oncellclick", this.exButton_oncellclick, this);	
        		//}
        	}
        }

        // this.exButton_oncellclick = function(obj, datasetObj){
        // 	var index = obj.parent.popDiv_ExButton.grd_ExButton.getSelectedRows(); 
        // 	trace(index);
        // 	var callbackFun = datasetObj.getColumn(index, "callback");
        // 	eval("obj."+callbackFun);
        // }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
