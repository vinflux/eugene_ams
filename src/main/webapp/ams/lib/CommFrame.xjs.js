﻿//XJS=CommFrame.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************
        * Name       : CmmnManager.xjs
        * Title      : nexacro 공통 함수 모음
        * @desc      : FORM 관련  및 시스템 공통 함추
        * 작성자    : 유희남
        * 작성일    : 2014-06-20
        * 변경사항  :
        ********************************************************************************/

        /*********************************************************************
         * gfn_openMenu         :    화면 호출 
         * gfn_openAddMenu      :    오픈 화면 저장
         * gfn_ArrangeWin       :    열려있는 윈도우 화면을 정렬
         * gfn_ActiveFrame      :    윈도우 키를 기준으로 열려있는 화면일 경우 focus, maximize 처리
         * gfn_TabOnClose       :    윈도우 타이틀 탭 닫힘 처리
         * gfn_ActiveForm       :    윈도우 키를 기준으로 Active된 화면의 타이틀 탭을 Active시킨다
         * gfn_FrameOnClose     :    윈도우 화면 닫힘 처리
         * gfn_activeWin        :    지정 화면 활성화
         * gfn_menuCloseAll     :    열려있는 화면을 정렬
         *********************************************************************/

        this.fv_nWinMax;

        /**********************************************************************************
        * Function Name: gfn_openMenuRow
        * Description  : gds_openMenu의 해당 Row의 정보를 기준으로 신규 윈도우 화면을 생성하고 open 시킴
        * @param        : menuid: 메뉴아이디, sArgs:전달인자
        * @Return       : 없음
        **********************************************************************************/
        this.gfn_openMenu = function (sMenuId,sArgs)
        {

        /*
        	2017.07.04 정상희 - vms 메뉴관련 에러 처리로 인한 처리  gds_systemConfig -> gds_admSystemConfig 
        						 추후 에러 수정되면 복원 처리
        	
        */
        	if(this.gfn_isNull(this.fv_nWinMax)) this.fv_nWinMax = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "ENABLEMAXUSERSCREEN"), "value1");
        	if(this.gfn_isNull(this.fv_nWinMax)) this.fv_nWinMax = application.gds_admSystemConfig.getColumn(application.gds_admSystemConfig.findRow("key", "ENABLEMAXUSERSCREEN"), "value1");
        	if(this.gfn_isNull(this.fv_nWinMax)) this.fv_nWinMax = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "ENABLEMAXUSERSCREEN"), "defaultvalue");
        	if(this.gfn_isNull(this.fv_nWinMax)) this.fv_nWinMax = application.gds_admSystemConfig.getColumn(application.gds_admSystemConfig.findRow("key", "ENABLEMAXUSERSCREEN"), "defaultvalue");
        	if(this.gfn_isNull(this.fv_nWinMax)) this.fv_nWinMax = 0;
        	
            if (this.gfn_isNull(sMenuId)) {
                return;
            }
            
            var nRow = application.gds_menu.findRow("mekey", sMenuId);

            if(nRow < 0) {
                this.gfn_alert("화면 권한이 없습니다.");
                return;
            }
            
            var sWinId = "M_" + sMenuId;
            
            if (application.gds_openMenu.findRow("WINID", sWinId) >= 0) {
                this.gfn_showMenu(sMenuId);
                return;
            }
            
            var sMenuNm = application.gds_menu.getColumn(nRow , "mename");
            var screenGb = application.gds_menu.getColumn(nRow , "nxpath").split("/");
            var sAppGrp = screenGb[0] + "::" + screenGb[1];
            
            if(this.gfn_isNull(sAppGrp) || this.gfn_isNull(sMenuNm)) {
                return;
            }
            
            if(application.gds_openMenu.rowcount >= this.fv_nWinMax) {
                this.gfn_alert("MSG_CHK_ENABLEMAXUSERSCREEN", this.fv_nWinMax);	
            }else{
        		this.gfn_openAddMenu(sMenuId);
        		
        		var objNewWin = new ChildFrame;
        		objNewWin.init(sWinId, "absolute", 0, 0, application.gv_WorkFrame.getOffsetWidth() - 0, application.gv_WorkFrame.getOffsetHeight() - 0);
        		objNewWin.set_openstatus("maximize");
        		
        		application.gv_WorkFrame.addChild(sWinId, objNewWin);
        		objNewWin.arguments = {};
        		// VMS 메뉴, 모듈, 검색조건 제거 폼 적용
        		if(application.gv_activeApp == "VIMS"){
        			objNewWin.set_formurl("frame::Work_vms.xfdl");
        		} else {
        			objNewWin.set_formurl("frame::Work.xfdl");
        		}
        		objNewWin.set_dragmovetype("all");
        		objNewWin.set_showtitlebar(false);
        		objNewWin.set_showtitleicon(true);
        		objNewWin.set_resizable(true);
        		objNewWin.set_openstatus("maximize");
        		objNewWin.arguments["winKey"]    = sWinId;
        		objNewWin.arguments["menuId"]    = sMenuId;
        		objNewWin.arguments["menuNm"]    = sMenuNm;
        		objNewWin.arguments["pageUrl"]   = sAppGrp;
        		objNewWin.arguments["menuParam"] = this.gfn_isNullEmpty(sArgs);
        		objNewWin.arguments["menuNavi"]  = this.gfn_getMenuNavi(sMenuId);

        		objNewWin.show();
        		
        		application.gv_TabFrame.form.fn_addTab(sMenuId, sMenuNm);
            }
        }

        /**
        *  신규 생성된 윈도우 화면을 gds_openMenu 에 등록
        * @param  sMenuId: 메뉴 ID
        * @return  없음
        */
        this.gfn_openAddMenu = function (sMenuId) 
        {
            var nRow = application.gds_menu.findRow("mekey", sMenuId);
            var curRow = application.gds_openMenu.addRow();
            var sWinId = "M_" + sMenuId;
            application.gds_openMenu.setColumn(curRow, "WINID", sWinId);
            application.gds_openMenu.setColumn(curRow, "mekey", sMenuId);
        }

        /**
        *  윈도우 키를 기준으로 열려있는 화면일 경우 focus, maximize 처리
        * @param  winId: 윈도우 생성 키
        * @return  열린화면이면 true
        */
        this.gfn_showMenu = function (sMenuId) 
        {
            application.gv_TabFrame.form.fn_moveTab(sMenuId);
            
            var sWinId = "M_" + sMenuId;
            var oWorkFrame = application.gv_WorkFrame.frames[sWinId];
            
            for(var i = 0 ; i < application.gds_openMenu.rowcount ; i++){
        		if(application.gv_WorkFrame.frames[application.gds_openMenu.getColumn(i, "WINID")].name == oWorkFrame.name){
        			application.gv_WorkFrame.frames[application.gds_openMenu.getColumn(i, "WINID")].set_visible(true);
        		}else{
        			application.gv_WorkFrame.frames[application.gds_openMenu.getColumn(i, "WINID")].set_visible(false);
        		}
            }
            
            if(this.gfn_isNotNull(oWorkFrame)){
        		oWorkFrame.setFocus();
            }
        }

        /**
        *  메뉴 Navigation 찾기
        * @param  sMenuId: 메뉴 ID
        * @return  메뉴 Navigation
        */
        this.gfn_getMenuNavi = function(sMenuId)
        {
            var aNavi = "";
            var upMenu = "";
            var nRow = application.gds_menu.findRow("mekey", sMenuId);
            var melvl = application.gds_menu.getColumn(nRow, "melvl");
            
            aNavi = application.gds_menu.getColumn(nRow, "mename");
            
            for(var i = melvl ; i > 0 ; i--){
        		upMenu = application.gds_menu.getColumn(nRow, "uppermekey");
        		nRow = application.gds_menu.findRow("mekey", upMenu);
        		aNavi = application.gds_menu.getColumn(nRow, "mename") + " > " + "<fc v='#000000'>" + aNavi + "</fc>";
            }
            
            return aNavi;
        }

        /**
        * 로그아웃 체크
        * @param
        * @return
        */
        this.gfn_chkLogout = function()
        {
        	if(application.getPrivateProfile("LOGIN_FLAG") != "Y"){
        		application.gv_loginStatus = "";
        		application.gv_system      = "";
        		application.gv_language    = "";
        		
                application.gv_LoginFrame.form.div_login.set_visible(true);
        		application.gfn_setFrame("L");
        	}
        }

        /* 메뉴 설정
        * @param
        * @return
        */
        this.gfn_setMenu = function(appId)
        {
        	var sMenu = "";
        	var dsTab;
        	
        	for(var i = 0 ; i < application.gds_openMenu.rowcount ; i++){
        		if(i == 0) sMenu += application.gds_openMenu.getColumn(i, "mekey");
        		else sMenu += "|" + application.gds_openMenu.getColumn(i, "mekey");
        	}
        	
        	if(system.navigatorname == "nexacro") application.setPrivateProfile("openMenu"+appId, sMenu);
        	else window.localStorage.setItem("openMenu"+appId, sMenu);
        	
        	dsTab = application.gv_TabFrame.form;
        	
        	for(var i = 0 ; i < dsTab.ds_Tab.rowcount ; i++){
        		TabObj = dsTab.fn_findObj(dsTab.ds_Tab.getColumn(i, "TAB_ID"));
        		
        		if(TabObj.cssclass == "btn_TaF_tab_on"){
        			if(system.navigatorname == "nexacro") application.setPrivateProfile("showMenu"+appId, dsTab.ds_Tab.getColumn(i, "WINID"));
        			else window.localStorage.setItem("showMenu"+appId, dsTab.ds_Tab.getColumn(i, "WINID"));
        			
        			break;
        		}
        	}
        }

        /* 메뉴별 버튼 권한 설정
        * @param
        * @return
        */
        this.gfn_setUsobXMenu = function(obj,menuId) {
        	
        	var authArr = application.gv_usobAuth[menuId];
        	if (this.gfn_isNull(authArr)) {
        		//console.log("no Auth ==>" + menuId)
        		return;
        	}
        	// 메뉴에 부여된 권한이 없으면 통과
        	
        	var arrCompArr = obj.components;
        	
        	for(var i=0; i < arrCompArr.length; i++) {
        	
        		//console.log(arrCompArr[i]);
        	
        		var childCompArr = arrCompArr[i].components;
        		
        		if(!this.gfn_isNull(childCompArr)) { 
        			this.gfn_setUsobXMenu(arrCompArr[i], menuId);
        		}	
        		
        		if(arrCompArr[i] instanceof nexacro.Button) { 
        		
        			//console.log("enable ==>" + arrCompArr[i].enable);
        			//console.log("authProps ==>" + arrCompArr[i].authProps);
        			if (arrCompArr[i].enable == true && !this.gfn_isNull(arrCompArr[i].authProps )) {
        				arrCompArr[i].enable=false;
        				
        				for(var j=0; j<authArr.length; j++) {
        					if (authArr[j] == arrCompArr[i].authProps ) {
        						//console.log("authArr["+j+"] ==>" + authArr[j]);
        						arrCompArr[i].enable=true;
        						break;
        					}
        				}
        			}
        		} 
        		else if(arrCompArr[i] instanceof nexacro.Menu) {
        			//console.log(arrCompArr[i].innerdataset);
        			var innerDs = arrCompArr[i]._innerdataset;
        			
        			if (!this.gfn_isNull(innerDs.getColumnInfo('authProps'))) {
        			
        				//authFlag 컬럼이 데이터셋에 없으면 생성
        				if (this.gfn_isNull(innerDs.getColumnInfo('authFlag'))) {
        					innerDs.addColumn('authFlag', 'string');
        				}
        				
        				for(var k=0; k<innerDs.rowcount; k++ ) {
        					//level = 0 이면 버튼 자체  
        					if (innerDs.getColumn(k, 'level') == 0 || innerDs.getColumn(k, 'authFlag') == 'false') {
        						continue;
        					}
        				
        					innerDs.setColumn(k, 'authFlag', 'false');
        					for(var j=0; j<authArr.length; j++) {
        						if ( authArr[j] == innerDs.getColumn(k, 'authProps') ) {
        							innerDs.setColumn(k, 'authFlag', 'true');
        							break;
        						}
        					}
        				}
        				
        				arrCompArr[i].set_enablecolumn('authFlag'); 
        			}
        		}
        	} 
        }
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
