﻿//XJS=CommReport.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name  		: commReport.xjs
        * Title      	: nexacro 공통 Excel (Server Side , Ws(Web Socket Mode) 관련 함수 모음
        * @desc      	: Server Output Stream 을 이용한 UBI REPORT 구현 
        * 작성자    	: cch
        * 작성일    	:
        * 변경사항  	:
        ********************************************************************************/ 
        this.gv_paramObj = null;

        this.gfn_GetCommandStructure = function (isSearchCondition) { 
        	// CommandMap Base Struncture 
        	var oValue = {
        		PARAM : {} , 
        		MAP: {},
        		LIST: {
        		},
        		COMMON : { 
        			USER_INFO : {} 
        		},
        		SEARCHLIST : []
        	};
        	
        	// CommandMap Setting ; 
        	var gdsCommonCount = application.gds_common.getColCount();
        	for (var i=0 ; gdsCommonCount > i ; i++) { 
        		var obj_RowValue = application.gds_common.getColumn(0,i);
        		var obj_RowInfoId = application.gds_common.getColumnInfo(i).id;
        		oValue.COMMON[obj_RowInfoId] = obj_RowValue;
        	}

        	// Common.UserInfo
        	var gdsUserInfoCount = application.gds_userInfo.getColCount();
        	for (var i=0 ; gdsUserInfoCount > i ; i++) { 
        		var obj_RowValue = application.gds_userInfo.getColumn(0,i);
        		var obj_RowInfoId = application.gds_userInfo.getColumnInfo(i).id;
        		if (obj_RowValue === undefined ) { 
        			obj_RowValue = "";
        		}
        		oValue.COMMON.USER_INFO[obj_RowInfoId] = obj_RowValue;
        	}
        	
        	if (isSearchCondition) { 
        		// Search Condition
        		var searchCellCount = application.gds_searchList.getColCount();
        		var searchRowCount = application.gds_searchList.getRowCount();
        		for (var i=0 ; searchRowCount > i ; i++) { 
        			var obj_Row = {};
        			for (var j=0 ; searchCellCount > j ; j++ ) { 
        				var obj_RowValue = application.gds_searchList.getColumn(i,j);
        				var obj_RowInfoId = application.gds_searchList.getColumnInfo(j).id;
        				obj_Row[obj_RowInfoId] = obj_RowValue;
        			}
        			oValue.SEARCHLIST.push(obj_Row); 
        		}	
        	}
        	return oValue;
        }

        /*******************************************************************************
        *
        ********************************************************************************/
        this.gfn_callReport =  function(jrfName,printType,udsName,oValue,prType,eqtype,target,printLogYN,sourcekey,wkprhstype,width,height,isMultiReport,multiCount
        )
        {

        	// Url Setting 
        	var sBaseUrl = application.services.get_item("svc").url;
        	var sActionUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase() + "/reportController/ubiReport.do";
        	
        	// Report default value setting 
        	oValue.PARAM.jrfName = jrfName;
        	oValue.PARAM.prType = prType;
        	oValue.PARAM.eqtype = eqtype;
        	oValue.PARAM.printType = printType;

        	// Multi page Setting 
        	if (isMultiReport == null ) {
        		isMultiReport = false;
        		multiCount = 0;
        	}
        	oValue.PARAM.isMultiReport = isMultiReport;
        	oValue.PARAM.multiCount = multiCount;

        	// Server Print log Setting 
        	if (printLogYN == undefined || printLogYN == null ) {
        		printLogYN = "N";
        	}
        	if (printLogYN  == "Y" ) {
        		var sourceKeyStr = "";
        		for ( var i=0 ; sourcekey.length > i ; i++) {
        			if (i > 0 ) {
        				sourceKeyStr = sourceKeyStr + "@" +sourcekey[i] ;
        			} else {
        				sourceKeyStr = sourcekey[i] ;
        			}
        		}
        		oValue.PARAM.sourcekey = sourceKeyStr;
        		oValue.PARAM.wkprhstype = wkprhstype;
        	}
        	
        	if (printType == "direct" ) {
        		if (target == null ) {
        			target = "iframe_hidden";
        		}
        	} else if (printType == "preview" ) {
        		if (target === null || target === undefined ) {
        			target = 'iframe_hidden';
        		}
        	} else {
        		if (width == null ) {
        			width = 1280;
        		}
        		if (height == null ) {
        			height = 1024;
        		}
        		if (target === null || target === undefined ) {
        			target = "ubiReport";
        		}
        	}
        	
        	// wms default center code setting 
        	if (application.gv_activeApp == "WMS" ) { 
        		oValue.PARAM.ctkey =  this.gfn_getUserInfo("ctKey");
        	}
        	
        	var str_JsonData = JSON.stringify(oValue);
        	// Form structure setting 
        	if(system.navigatorname == "nexacro"){
        		this.gfn_alert("MSG_RUN_BROWSER");
        		return;
        	}
        	
        	var objForm = document.createElement("form"); 
        	var objInput1 = document.createElement("input");

        	objForm.setAttribute("target",target);
        	objForm.setAttribute("method","post");
        	objForm.setAttribute("action",sActionUrl);
        	objForm.setAttribute("style","width:0;height:0;visibility: hidden;");
        	
        	document.body.appendChild(objForm);

        	//─────── UbiReport UDS Input value setting  ─────── //
        	var objInput1 = parent.document.createElement("input");
        	objInput1.setAttribute("type","hidden");
        	objInput1.setAttribute("name","udsName");
        	objInput1.setAttribute("value",udsName);
        	objForm.appendChild(objInput1);

        	//─────── UbiReport User dataObj setting  ─────── //
        	var objInput2 = parent.document.createElement("input");
        	objInput2.setAttribute("type","hidden");
        	objInput2.setAttribute("name","reportParam");
        	objInput2.setAttribute("value",str_JsonData);
        	objForm.appendChild(objInput2);

        	//─────────────────────── Form submit  ─────────────────────── //
        	objForm.submit();
        }

        this.gfn_report = function(gridObj,thisObj,tranObj,paramObj)
        {
        	if(this.gfn_isNull(gridObj)){
        		this.gfn_callReport(
        							 paramObj.jrfName
        							,paramObj.printType
        							,paramObj.udsName
        							,this.makeDatasetToJSON(paramObj.oDatasetList)
        							,paramObj.prType
        							,paramObj.eqtype
        							,paramObj.target
        							,paramObj.printLogYN
        							,paramObj.sourcekey
        							,paramObj.wkprhstype
        							,paramObj.width
        							,paramObj.height
        							,paramObj.isMultiReport
        							,paramObj.multiCount
        		);	
        		return;
        	}

        	if(this.gfn_isNull(thisObj)){
        		return;
        	}

        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var privateDataset = this.lookup(gridObj.binddataset);

        	if(checkList.length == privateDataset.rowcount){
        	//전체
        		this.gv_paramObj = paramObj;
        		thisObj.parent.div_cond.fn_reportSearch(gridObj, thisObj, tranObj, paramObj);

        	}else{
        	//체크된 항목
        		this.gfn_callReport(
        							 paramObj.jrfName
        							,paramObj.printType
        							,paramObj.udsName
        							,this.makeDatasetToJSON(paramObj.oDatasetList)
        							,paramObj.prType
        							,paramObj.eqtype
        							,paramObj.target
        							,paramObj.printLogYN
        							,paramObj.sourcekey
        							,paramObj.wkprhstype
        							,paramObj.width
        							,paramObj.height
        							,paramObj.isMultiReport
        							,paramObj.multiCount
        		);
        	}	
        }

        this.removeDatasetToJSON = function(datasetList)
        {
        	if(this.gfn_isNull(datasetList) || datasetList.length == 0){
        		return;
        	}
        	
        	var datainfoObj;
        	var mapStructure = this.gfn_GetCommandStructure();
        	for(var i = 0 ; i < datasetList.length ; i++){
        		datainfoObj = datasetList[i];
        		eval("mapStructure.LIST."+datainfoObj.datasetKey+" = [];");
        	}
        	return mapStructure;
        }

        this.makeDatasetToJSON = function(datasetList)
        {
        	if(this.gfn_isNull(datasetList) || datasetList.length == 0){
        		return;
        	}
        	
        	var datainfoObj;
        	var mapStructure = this.gfn_GetCommandStructure();
        	for(var i = 0 ; i < datasetList.length ; i++){
        		datainfoObj = datasetList[i];
        		if(datainfoObj.dataType == "LIST_KEY_ARRAY"){
        			this.fn_setListKeyArray(mapStructure, datainfoObj);
        		}else if(datainfoObj.dataType == "LIST_MAP"){
        			this.fn_setListMap(mapStructure, datainfoObj);
        		}else{
        			this.fn_setListMap(mapStructure, datainfoObj);
        		}
        	}
        	return mapStructure;
        }

        this.fn_setListKeyArray = function(mapStructure,datainfoObj)
        {
        	var keyValue = "";
        	var sValue = "";
        	for(var j = 0 ; j < datainfoObj.datasetObj.getRowCount() ; j++){
        		if(j != 0){
        			keyValue +=",";
        		}
        		sValue = datainfoObj.datasetObj.getColumn(j, datainfoObj.keyName);
        		keyValue += "'"+sValue+"'";
        	}
        	eval("mapStructure.LIST."+datainfoObj.datasetKey+" = ["+keyValue+"];");
        }

        this.fn_setListMap = function(mapStructure,datainfoObj)
        {
        	if(this.gfn_isNull(datainfoObj)){
        		return;
        	}
        	
        	if(this.gfn_isNull(datainfoObj.datasetKey)){
        		return;
        	}
        	
        	if(this.gfn_isNull(datainfoObj.datasetObj)){
        		return;
        	}
        		
        	eval("mapStructure.LIST."+datainfoObj.datasetKey+" = [];");

        	var sKey,sValue;
        	var mapValue = "";
        	for(var j = 0 ; j < datainfoObj.datasetObj.getRowCount() ; j++){
        		mapValue = "{";
        		for(var k = 0 ; k < datainfoObj.datasetObj.getColCount() ; k++){
        			sKey = datainfoObj.datasetObj.getColID(k);
        			sValue = datainfoObj.datasetObj.getColumn(j, sKey);

        			if(k != 0){
        				mapValue += ",";
        			}
        			if(this.gfn_isNull(sValue)){
        				mapValue += "'"+sKey+"'"+":"+"null";
        			}else{
        				mapValue += "'"+sKey+"'"+":"+"'"+sValue+"'";
        			}
        			
        		}
        		mapValue += "}";
        		eval("mapStructure.LIST."+datainfoObj.datasetKey+"[j] = "+mapValue);	
        	}
        }

        this.gfn_callReportTransaction = function(tranObj,paramObj)
        {
        	this.gv_paramObj = paramObj;
        	
        	this.gfn_setCommon("BEANID",   tranObj.beanId);
        	this.gfn_setCommon("METHODNM", tranObj.method);
        	
            var sSvcId   = "callReport";
            var sSvcUrl  = tranObj.sSvcUrl;
            var sInData  = tranObj.sInData;
            var sOutData = tranObj.sOutData;
            var sParam   = tranObj.sParam;

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBackReport");
        }

        this.fn_callBackReport = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	if(sSvcId == "callReport") {
        		trace("callReport : "+this.gv_paramObj.jrfName);
        		this.gfn_callReport(
        					 this.gv_paramObj.jrfName
        					,this.gv_paramObj.printType
        					,this.gv_paramObj.udsName
        					,this.makeDatasetToJSON(this.gv_paramObj.oDatasetList)
        					,this.gv_paramObj.prType
        					,this.gv_paramObj.eqtype
        					,this.gv_paramObj.target
        					,this.gv_paramObj.printLogYN
        					,this.gv_paramObj.sourcekey
        					,this.gv_paramObj.wkprhstype
        					,this.gv_paramObj.width
        					,this.gv_paramObj.height
        					,this.gv_paramObj.isMultiReport
        					,this.gv_paramObj.multiCount
        		);
        	}
        }
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
