﻿//XJS=CommUpload.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name  		: commUpload.xjs
        * Title      	: html WebUpload Popup Util
        * @desc      	: html 팝업을 이용해서 파일을 업로드 할 수 있는 로직 
        * 작성자    	: cch
        * 작성일    	:
        * 변경사항  	:
        ********************************************************************************/ 

        var obj_PopupWindow = null;
        var obj_ReturnValue = null;
        this.obj_CallbackFun = null;
        var obj_Self = this;

        
        /**
        * Upload popup complete close Event 
        */
        this.gtn_showUploadPopClose = function() { 
            
            var timer = setInterval(function() {   
        		if (obj_PopupWindow != null ) { 
        			clearInterval(timer);
        			obj_PopupWindow.close();
        			var obj_ReturnValue = obj_PopupWindow.opener.obj_ReturnValue;
        			if (obj_ReturnValue.success) { 
        				obj_CallbackFun(obj_ReturnValue);
        			} else { 
        				var msgCode = "MSG_" + obj_ReturnValue.MESSAGE_CODE;
        				var msgArg = {};
        				for (key in obj_ReturnValue) {
        					if(key != 'success' && key != 'MESSAGE_CODE')	{
        						msgArg[key] = obj_ReturnValue[key];
        					}

        				}
         
        				if( msgCode == "MSG_00105" ) {
        					 var ss =  nexacro.replaceAll (   JSON.stringify( msgArg ) , ",", "|" ); 
        						 ss =  nexacro.replaceAll (  ss , ":", "," );
        						 ss =  ss.replace(/[{}"]/g,'');  
        						 
        						 obj_Self.gfn_alert(msgCode, ss  );
                        }else{
                           obj_Self.gfn_alert(msgCode  );
                        } 
        				
        				
        			}
        			return;
        		}
        	}, 1500); 	
        }

        /**
        * 
        */
        this.gtn_uploadPopShow =  function(objCommandMapValue ,sUrl ,funCallback)
        {
        	var sBaseUrl = application.services.get_item("svc").url;
        	var sActionUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase() + "/jsp/upload/upload.html";
        	sActionUrl = sBaseUrl + "ams/jsp/upload/upload.html"
        	var oWindowOption = 
        			"width=550," +
        			"height=240," +
        			"toolbar=0," +
        			"location=0," +
        			"status=0," +
        			"menuber=0," + 
        			"scrollbars=no," +
        			"resizable=no," +
        			"top=100," +
        			"left=100";
        			
        	obj_PopupWindow = window;
        	obj_PopupWindow = obj_PopupWindow.open(sActionUrl, "uploadForm", oWindowOption); 
        	
        	obj_PopupWindow.opener.gtn_showUploadPopClose = this.gtn_showUploadPopClose;

        	// CommandMap setting 
        	obj_PopupWindow.opener.objCommandMapValue = objCommandMapValue;
        	obj_PopupWindow.opener.obj_ReturnValue = obj_ReturnValue;
        	
        	sUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase()  + sUrl;
        	obj_PopupWindow.opener.destinationUrl = sUrl;

        	// function setting 
        	obj_CallbackFun = funCallback;
        }

        this.gtn_showUploadImgPopClose = function() { 
            
            var timer = setInterval(function() {   
        		if (obj_PopupWindow != null ) { 
        			clearInterval(timer);
        			obj_PopupWindow.close();
        			var obj_ReturnValue = obj_PopupWindow.opener.obj_ReturnValue;
        			if (obj_ReturnValue.success) { 
        				obj_CallbackFun(obj_ReturnValue);
        			} else { 
        				var msgCode = "MSG_" + obj_ReturnValue.MESSAGE_CODE;
        				var msgArg = {};
        				for (key in obj_ReturnValue) {
        					if(key != 'success' && key != 'MESSAGE_CODE')	{
        						msgArg[key] = obj_ReturnValue[key];
        					}

        				}
        				// alert 
        				obj_Self.gfn_alert(msgCode);
        			}
        			return;
        		}
        	}, 1500); 	
        }

        this.gtn_uploadImgPopShow =  function(objCommandMapValue ,sUrl ,funCallback)
        {
        	var sBaseUrl = application.services.get_item("svc").url;
        	var sActionUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase() + "/jsp/upload/upload.html";
        	sActionUrl = sBaseUrl + "wms/jsp/upload/upload.html"
        	var oWindowOption = 
        			"width=550," +
        			"height=480," +
        			"toolbar=0," +
        			"location=0," +
        			"status=0," +
        			"menuber=0," + 
        			"scrollbars=no," +
        			"resizable=no," +
        			"top=100," +
        			"left=100";
        			
        	obj_PopupWindow = window;
        	obj_PopupWindow = obj_PopupWindow.open(sActionUrl, "uploadForm", oWindowOption); 
        	
        	obj_PopupWindow.opener.gtn_showUploadImgPopClose = this.gtn_showUploadImgPopClose;

        	// CommandMap setting 
        	obj_PopupWindow.opener.objCommandMapValue = objCommandMapValue;
        	obj_PopupWindow.opener.obj_ReturnValue = obj_ReturnValue;
        	
        	sUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase()  + sUrl;
        	obj_PopupWindow.opener.destinationUrl = sUrl;

        	// function setting 
        	obj_CallbackFun = funCallback;
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
