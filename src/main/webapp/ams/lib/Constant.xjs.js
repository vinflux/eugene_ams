﻿//XJS=Constant.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name         : Constant.xjs                                                                                                          
        * Title        : 상수 선언 부                                                                            
        * @desc        :                                                                                                                       
        * 작성자      :                                                                                                                                 
        * 작성일      : 2017-04-18                                                                                                                           
        * 변경사항    :                                                                                                        
        ********************************************************************************/ 
        /*********************************************************
        	Common 
        **********************************************************/
        this.MAIN = "Main";
        this.MIDDLE = "Middle"; // 업무영역 내 조회
        this.DETAIL = "Detail"; // 업무영역 내 조회

        this.MESSAGE = "";	
        this.MESSAGE_CODE = "";
        /*********************************************************
        	XMS PATH 
        **********************************************************/
        this.XMS_PATH= "http://172.22.12.43:8080/img/wms/img" ,

        /*********************************************************
        	시리얼 타입
        **********************************************************/
        this.SERIALINPUTTYPE_NORMAL = "10" ,
        this.SERIALINPUTTYPE_EANTYPE = "20" ,

        /*********************************************************
        	어플리케이션 코드 
        **********************************************************/
        this.APP_ADM = "ADM",
        this.APP_ICOM = "ICOM",
        this.APP_OMS = "OMS",
        this.APP_TMS = "TMS",
        this.APP_WMS = "WMS",
        this.APP_VIMS = "VIMS",
        this.APP_PMS = "PMS",
        this.APP_BBS = "BBS",
        this.APP_PORTAL = "PORTAL",
        this.APP_TMSBPS = "TMSBPS",

        /**********************************************************
        	국가 코드 / COUNTRY CODE 
        **********************************************************/
        this.COUNTRYCODE_KOR = "KOR",
        this.COUNTRYCODE_ENG = "ENG",

        /**********************************************************
        	DLCOMPANY 택배사
        **********************************************************/
        this.DLCOMPANY_CJ_KOREA_EXPRESS = "10", 		// CJ 대한통운 
        this.DLCOMPANY_POST_EXPRESS     = "20", 		// 우체국 
        this.DLCOMPANY_LOGEN_EXPRESS    = "30", 		// 로젠 택배 
        this.DLCOMPANY_ILYANG_EXPRESS   = "40", 		// 일양 택배 
        this.DLCOMPANY_WELLLOGI_EXPRESS = "50", 		// 웰로지 택배 
        this.DLCOMPANY_DONGBU_EXPRESS = "60", 		// 동부 택배
        this.DLCOMPANY_DONBBURETURN_EXPRESS = "61", 		// 동부 택배 (반품)
        this.DLCOMPANY_GTL_EXPRESS = "70", 	// 굿트럭 택배 
        this.DLCOMPANY_OTHER_EXPRESS    = "90", 		// 기타 택배 	

        /**********************************************************
        	차트 종
        **********************************************************/
        this.CHART_TYPE_GAUGE = "gauge",
        this.CHART_TYPE_COLUMN = "column",
        this.CHART_TYPE_BAR = "bar",
        this.CHART_TYPE_LINE = "line",

        /**********************************************************
        	팝업 검색 ID
        **********************************************************/
        this.SEARCH_ID_APPLICATION = "10",
        this.SEARCH_ID_CODE = "20",
        this.SEARCH_ID_MESSAGE = "30",
        this.SEARCH_ID_ACCOUNT = "40",
        this.SEARCH_ID_OWNER = "50",
        this.SEARCH_ID_CENTER = "60",
        this.SEARCH_ID_USERGROUP = "70",
        this.SEARCH_ID_USER = "80",
        this.SEARCH_ID_ICGRKEY = "90",
        this.SEARCH_ID_ICUTKEY = "100",
        this.SEARCH_ID_ICKEY = "110",
        this.SEARCH_ID_SUBSTITUTION = "120",
        this.SEARCH_ID_ZCKEY = "230",
        this.SEARCH_ID_WAHDKEY = "240",
        this.SEARCH_ID_UOM = "270",
        this.SEARCH_ID_STORE = "720",

        /**********************************************************
        	화주 
        **********************************************************/
        this.OWNER_COOK = "COOK", // 애터미
        this.OWNER_VINFLUX = "VINFLUX",

        /**********************************************************
        	업로드 유형
        **********************************************************/
        this.ULTYPE_COOK_INBOUND = "10",
        this.ULTYPE_COOK_OUTBOUND = "20",

        this.ULTYPE_VINFLUX_INBOUND = "10",
        this.ULTYPE_VINFLUX_OUTBOUND_BTOC = "20",
        this.ULTYPE_VINFLUX_OUTBOUND_BTOB = "30",

        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_MESSAGE   	 = "40",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_ITEMGROUP 	 = "50",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_ITEMUNIT  	 = "60",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_ITEM      	 = "70",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_SUBSTITUTION = "80",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_AC 		     = "90",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_STORE 		 = "100",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_PRIVATEORDER = "120",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_STOCKINVESTIMODIFY = "130", 
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_CENTERSTOCKTRANS = "140",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_STOCKINVESTIGATION = "150",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_LOCXITEMCODE = "160",
        this.WMS_ULTYPE_VINFLUX_EXCELUPLOAD_ACCOUNT = "170",
        this.WMS_ULTYPE_VINFLUX_EXCELUPLOAD_ITEMCODE = "180",
        this.WMS_ULTYPE_VINFLUX_EXCELUPLOAD_LOCATION  = "190",
        this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_YMDIDXHIST = "200",
        this.OMS_ULTYPE_VINFLUX_EXCELUPLOAD_DAYIDXHIST = "210",
        this.TMS_ULTYPE_VINFLUX_EXCELUPLOAD_ROUTE = "220",
        this.OMS_ULTYPE_VINFLUX_EXCELUPLOAD_SEASONIDXHIST = "230",
        this.PTL_ULTYPE_VINFLUX_OUTBOUND_RETURN= "250",
        this.PTL_ULTYPE_VINFLUX_OUTBOUND_MULTI = "260",
        /**********************************************************
        	출고 업무 상태 코드 
        **********************************************************/

        this.OB_WKSTATUS_CREATED = "00", // 작업 생성 
        this.OB_WKSTATUS_PART_ALLOCATION = "35", // 부분할당
        this.OB_WKSTATUS_ALLOCATION = "40", // 할당 완료
        this.OB_WKSTATUS_PART_ALLOCATION_PART_PICKED = "41", // 부분 할당/부분 피킹
        this.OB_WKSTATUS_PART_PICKED = "45", // 부분 피킹
        this.OB_WKSTATUS_PART_ALLOCATION_PICKED = "46", // 부분 할당/피킹 완료
        this.OB_WKSTATUS_PICKED = "49", // 피킹 완료
        this.OB_WKSTATUS_PART_ALLOCATION_PART_PACKED = "51", // 부분 할당/부분 패킹
        this.OB_WKSTATUS_PART_PACKED = "55", // 부분 패킹
        this.OB_WKSTATUS_PART_ALLOCATION_PACKED = "56", // 부분 할당/패킹 완료
        this.OB_WKSTATUS_PACKED = "58", // 패킹 완료
        this.OB_WKSTATUS_PART_ALLOCATION_PART_LOADED = "61", // 부분 할당/부분 상차
        this.OB_WKSTATUS_PART_LOADED = "65", // 부분 상차
        this.OB_WKSTATUS_PART_ALLOCATION_LOADED = "66", // 부분 할당/상차 완료 
        this.OB_WKSTATUS_LOADED = "67", // 상차 완료 
        this.OB_WKSTATUS_PART_ALLOCATION_PART_SHIPPED = "81", // 부분 할당/부분 출고
        this.OB_WKSTATUS_PART_SHIPPED = "85", // 부분 출고
        this.OB_WKSTATUS_PART_ALLOCATION_SHIPPED = "86", // 부분 할당 /출고 완료
        this.OB_WKSTATUS_SHIPPED = "90", // 출고 완료
        this.OB_WKSTATUS_WORK_CANCELED = "99", // 작업 취소
        this.OB_WKSTATUS_UNKNOWN = "UNK", // UNKNOWN	

        /**********************************************************
        	출고 타입 코드
        **********************************************************/
        this.OBHDTYPE_NORMAL = "10",				//입반 출고
        this.OBHDTYPE_INDIVIDUAL_OUTBOUND       = "11", //개별 출고   
        this.OBHDTYPE_CENTER_OUTBOUND           = "12", //센터 출고   
        this.OBHDTYPE_NORMAL_OTHER_OUTBOUND     = "13", //일반 후 출고
        this.OBHDTYPE_INDIVIDUAL_OTHER_OUTBOUND = "14", //개별 후 출고
        this.OBHDTYPE_CENTER_OTHER_OUTBOUND     = "15", //센터 후 출고
        this.OBHDTYPE_EXCHANGE_NORMAL = "35",				//입반 출고(교환)
        this.OBHDTYPE_EXCHANGE_INDIVIDUAL_OUTBOUND       = "36", //개별 출고   (교환)
        this.OBHDTYPE_EXCHANGE_NORMAL_OTHER_OUTBOUND     = "37", //일반 후 출고(교환)
        this.OBHDTYPE_EXCHANGE_INDIVIDUAL_OTHER_OUTBOUND = "38", //개별 후 출고(교환)
        this.OBHDTYPE_EMERGENCY = "50",			//긴급 출고
        this.OBHDTYPE_WH_TRANSFER = "60",			//센터간 이동 출고
        this.OBHDTYPE_OTHERL = "90",				//기타 출고

        
        /**********************************************************
        	주문 타입 코드
        **********************************************************/
        this.ORHDTYPE_RETURN_EXCHANGE_INBOUND = "30" ,

        /**********************************************************
        	리포트 타입 
        **********************************************************/
        this.PRTYPE_WAYBILL = "10",	// 운송장 
        this.PRTYPE_CONTRACT = "20",	// 상품구매계약서
        this.PRTYPE_ULC_BARCODE = "30", // ULC BARCODE 
        this.PRTYPE_REPORT = "40", // 일반 (A4) 리포트 
        this.PRTYPE_MEMBERSHIP = "50", // 회원등록증 
        this.PRTYPE_APPLELABEL = "60", // 애플라벨

        /**********************************************************
        	출력 하위 유형 
        **********************************************************/
        this.PRSUBTYPE_PALLET = "3010",		 		// 파렛트
        this.PRSUBTYPE_BOX = "3020",				// 박스
        this.PRSUBTYPE_INBOUND_ORDER = "4010",		// 입고예정지시서
        this.PRSUBTYPE_INBOUND_STATEMENT = "4020",	// 입고전표

        this.PRSUBTYPE_OUTBOUND_ORDER = "5010",		// 출고예정지시서
        this.PRSUBTYPE_OUTBOUND_RETURN_AC = "5040",		// 매입처 반출
        this.PRSUBTYPE_OUTBOUND_CENTER_MOVE = "5080",		// 센터간 이동 출고
        this.PRSUBTYPE_OUTBOUND_DISUSE = "5120",		// 폐기 출고
        this.PRSUBTYPE_OUTBOUND_SALE = "5130",		// 매각 출고
        this.PRSUBTYPE_OUTBOUND_DISUSE = "5120",		// 폐기 출고
        this.PRSUBTYPE_OUTBOUND_DISUSE_IMPORT = "5430",		// 폐기 출고(수입)

        

        
        /**********************************************************
        	장비타입
        **********************************************************/
        this.EQTYPE_PRINT = "80",	// 일반프리트
        this.EQTYPE_LABELPRINT = "90",	// 라벨프린트

        /**********************************************************
        	프린트 로그 타입 
        **********************************************************/
        this.WKPRHSTYPE_PICKING_ORDER = "10", 			// 피킹리스트 
        this.WKPRHSTYPE_WAYBILL = "20", 					// 운송장
        this.WKPRHSTYPE_PACKING_LIST= "30", 				// 패킹리스트 
        this.WKPRHSTYPE_PICKINGORDER_PACKAGE = "40",		// 피킹리스트 (합포)
        this.WKPRHSTYPE_PICKINGORDER_CART = "50",		// 피킹리스트 (카트)
        this.WKPRHSTYPE_PICKIN_ORDER_PACKAGEORDER_AREA = "60",	// 피킹리스트 (합포+구역)
        this.WKPRHSTYPE_ULC_BARCODE = "70", 				// ULC BARCODE
        this.WKPRHSTYPE_CONTRACT = "80", 				// 상품구매계약서
        this.WKPRHSTYPE_MEMBERSHIP= "90", 				// 회원등록증
        this.WKPRHSTYPE_APPLELABEL = "100" ,				// 애플라벨
        this.WKPRHSTYPE_PICKINGORDER_WAVETT = "110", 		// 피킹리스트 웨이브 단 (총량.개별)

        this.TA_DTWKSTATUS_CREATED	= "00",	//작업생성	
        this.TA_DTWKSTATUS_WORKING	= "50",	//작업중
        this.TA_DTWKSTATUS_COMPLETED	= "90",	//작업완료
        this.TA_DTWKSTATUS_CANCELD	= "99"	//작업취소
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
