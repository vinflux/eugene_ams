﻿//XJS=SchedulerData.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /**
         * @fileoverview
         * SchedulerData.xjs at nexacro® software.<br>
         *<br>
         * Eco.Scheduler.Data is used to involve property of Eco.Scheduler which be called "_data".<br>
         * caculate date range in visible area.<br>
         * load event data with dataset.<br>
         * Copyright (c) 2015 EcoSystem of nexacro.<br>
         * Licensed Free under nexacro® software.
        */

        if ( !JsNamespace.exist("Eco.Scheduler.Data")  )
        {
        	JsNamespace.declareClass("Eco.Scheduler.Data",{
        		/**
        		 * Eco.Scheduler.Data constructor
        		 * @class Eco.Scheduler.Data
        		 * @classdesc This is used as value of property of Eco.Scheduler. The name of property in Eco.Scheduler is "_data".<br>
        		 * caculate date region in visible area by mode(month, week , day).<br>
        		 * load event data with dataset and delete, insert, modify event data.
        		 * @constructor Eco.Scheduler.Data
        		*/
        		initialize: function()
        		{
        			this._calendar = new Eco.GregorianCalendar();
        			this._sortedItems = new Eco.KeySortedHashMap();

        			this._sortedItemsByEnddate = new Eco.KeySortedHashMap(function(a, b)
        				{
        					if ( a.key > b.key ) return -1;
        					if ( a.key < b.key ) return 1;
        					return 0;
        				});
        			this._dsMap = new Eco.DatasetMap();
        			this._weekOffset = 0;
        		},
        		_keyField: "cd",
        		statics: {
        			modeKind: {
        				"month": true,
        				"week": true,
        				"day": true,
        				"workweek": true
        			},
        			dateFormatKind: {
        			}
        		},
        		properties: {
        			binddataset: {
        				"set": function(val)
        				{
        					if ( val != this.binddataset )
        					{
        						this.binddataset = val;
        						if ( val && val instanceof Dataset )
        						{
        							this.clearEventItems();
        							this._dsMap.buildDataWithDataset(val, this._keyField, this.createEventItem, this);
        							this._changeddata = true;
        						}
        						else
        						{
        							this.clearEventItems();
        							this._changeddata = true;
        						}
        					}
        				}
        			},
        			workWeekRange: {
        				value: [1, 5]
        			},
        			hourHeight: {
        				memberName: "_hourHeight",
        				value: 50
        			}
        		},
        		_convertDate: function(dsVal)
        		{
        			var dt;
        			if ( dsVal && dsVal.date )
        			{
        				dt = new Date(dsVal.date.getTime());
        			}
        			else
        			{
        				dt = new Date();
        			}
        			return dt;
        		},
        		_convertNexacroDate: function(dt)
        		{
        			var nexacroDt = new nexacro.Date();
        			nexacroDt.date = new Date(dt.getTime());
        			return nexacroDt;
        		},
        		_dateToString: function(dt)
        		{
        			return Eco.date.getMaskFormatString(dt, "yyyyMMddHHmmss");
        		},
        		clearEventItems: function()
        		{
        			this._sortedItems.clear();
        			this._sortedItemsByEnddate.clear();
        			this._dsMap.clear();
        		},
        		getEventItem: function(key)
        		{
        			return this._dsMap.getByKey(key);
        		},
        		updateEventItemTimeValue: function(evtItem,startdate,enddate)
        		{
        			if ( evtItem.startdate.getTime() != startdate.getTime() )
        			{
        				var sortedItems = this._sortedItems;
        				var orgSortkey = this._dateToString(evtItem.startdate);
        				var orgSortValue = sortedItems.get(orgSortkey);
        				if ( orgSortValue )
        				{
        					if ( orgSortValue.length <= 1 )
        					{
        						sortedItems.remove(orgSortkey);
        					}
        					else
        					{
        						var idx = Eco.array.indexOf(orgSortValue, evtItem);
        						if ( idx > -1 ) orgSortValue.splice(idx, 1);
        					}
        				}
        				var sortkey = this._dateToString(startdate);
        				var sortValue = sortedItems.get(sortkey);
        				if ( sortValue )
        				{
        					sortValue.push(evtItem);
        				}
        				else
        				{
        					sortedItems.put(sortkey, [evtItem]);
        				}
        				evtItem.startdate = startdate;
        				this._changeddata = true;
        			}
        			if ( evtItem.enddate.getTime() != enddate.getTime() )
        			{
        				var sortedItems = this._sortedItemsByEnddate;
        				var orgSortkey = this._dateToString(evtItem.enddate);
        				var orgSortValue = sortedItems.get(orgSortkey);
        				if ( orgSortValue )
        				{
        					if ( orgSortValue.length <= 1 )
        					{
        						sortedItems.remove(orgSortkey);
        					}
        					else
        					{
        						var idx = Eco.array.indexOf(orgSortValue, evtItem);
        						if ( idx > -1 ) orgSortValue.splice(idx, 1);
        					}
        				}
        				var sortkey = this._dateToString(enddate);
        				var sortValue = sortedItems.get(sortkey);
        				if ( sortValue )
        				{
        					sortValue.push(evtItem);
        				}
        				else
        				{
        					sortedItems.put(sortkey, [evtItem]);
        				}
        				evtItem.enddate = enddate;
        				this._changeddata = true;
        			}
        		},
        		getRowToDs: function(key)
        		{
        			return this._dsMap.findRowByKey(key);
        		},
        		applyToDS: function(key)
        		{
        			var value = this._dsMap.getByKey(key);
        			if ( value )
        			{
        				var ds = this.binddataset,
        					row = this._dsMap.findRowByKey(key);

        				ds.setColumn(row, "startTime", this._convertNexacroDate(value.startdate));
        				ds.setColumn(row, "endTime", this._convertNexacroDate(value.enddate));
        				ds.setColumn(row, "summary", value.summary);
        				ds.setColumn(row, "description", value.description);
        				ds.setColumn(row, "location", value.location);
        				ds.setColumn(row, "status", value.status);
        				ds.setColumn(row, "categories", value.categories);
        			}
        		},
        		updateEventItemFromDs: function(row)
        		{
        			var ds = this.binddataset;
        			var key = ds.getColumn(row, this._keyField);
        			var value = this._dsMap.getByKey(key);
        			if ( value )
        			{
        				var startdt = this._convertDate(ds.getColumn(row, "startTime"));
        				var enddt = this._convertDate(ds.getColumn(row, "endTime"));
        				if ( value.startdate.getTime() != startdt.getTime() )
        				{
        					var orgSortkey = this._dateToString(value.startdate);
        					var orgSortValue = this._sortedItems.get(orgSortkey);
        					if ( orgSortValue )
        					{
        						if ( orgSortValue.length <= 1 )
        						{
        							this._sortedItems.remove(orgSortkey);
        						}
        						else
        						{
        							var idx = Eco.array.indexOf(orgSortValue, value);
        							if ( idx > -1 ) orgSortValue.splice(idx, 1);
        						}
        					}
        					var sortkey = this._dateToString(startdt);
        					var sortValue = this._sortedItems.get(sortkey);
        					if ( sortValue )
        					{
        						sortValue.push(value);
        					}
        					else
        					{
        						this._sortedItems.put(sortkey, [value]);
        					}
        				}
        				if ( value.enddate.getTime() != enddt.getTime() )
        				{
        					var orgSortkey = this._dateToString(value.enddate);
        					var orgSortValue = this._sortedItemsByEnddate.get(orgSortkey);
        					if ( orgSortValue )
        					{
        						if ( orgSortValue.length <= 1 )
        						{
        							this._sortedItemsByEnddate.remove(orgSortkey);
        						}
        						else
        						{
        							var idx = Eco.array.indexOf(orgSortValue, value);
        							if ( idx > -1 ) orgSortValue.splice(idx, 1);
        						}
        					}
        					var sortkey = this._dateToString(enddt);
        					var sortValue = this._sortedItemsByEnddate.get(sortkey);
        					if ( sortValue )
        					{
        						sortValue.push(value);
        					}
        					else
        					{
        						this._sortedItemsByEnddate.put(sortkey, [value]);
        					}
        				}
        				value.summary = ds.getColumn(row, "summary");
        				value.description = ds.getColumn(row, "description");
        				value.location = ds.getColumn(row, "location");
        				value.status = ds.getColumn(row, "status");
        				value.categories = ds.getColumn(row, "categories");
        				value.startdate = startdt;
        				value.enddate = enddt;
        				this._changeddata = true;
        			}
        		},
        		insertEventItem: function(key,row)
        		{
        			var value = this._dsMap.getByKey(key);
        			if ( !value )
        			{
        				value = this.createEventItem(this.binddataset, row, key);
        				this._dsMap.insert(row, key, value);
        				this._changeddata = true;
        			}
        		},
        		deleteEventItem: function(key)
        		{
        			var value = this._dsMap.getByKey(key);
        			if ( value )
        			{
        				this._dsMap.removeAtKey(key);
        				var startdt = value.startdate;
        				var sortkey = this._dateToString(startdt);
        				var sortValue = this._sortedItems.get(sortkey);
        				if ( sortValue && sortValue.length > 1 )
        				{
        					var index = Eco.array.indexOf(sortValue, value);
        					if ( index > -1 )
        					{
        						sortValue.splice(index, 1);
        					}
        				}
        				else
        				{
        					this._sortedItems.remove(sortkey);
        				}
        				var sortendkey = this._dateToString(value.enddate);
        				var sortEndValue = this._sortedItemsByEnddate.get(sortendkey);
        				if ( sortEndValue && sortEndValue.length > 1 )
        				{
        					var index = Eco.array.indexOf(sortEndValue, value);
        					if ( index > -1 )
        					{
        						sortEndValue.splice(index, 1);
        					}
        				}
        				else
        				{
        					this._sortedItemsByEnddate.remove(sortendkey);
        				}
        				this._changeddata = true;
        			}
        		},
        		createEventItem: function(ds,row,key)
        		{
        			var startdt = this._convertDate(ds.getColumn(row, "startTime"));
        			var enddt = this._convertDate(ds.getColumn(row, "endTime"));
        			var sortkey = this._dateToString(startdt);
        			var sortValue = this._sortedItems.get(sortkey);
        			var sortendkey = this._dateToString(enddt);
        			var sortEndValue = this._sortedItemsByEnddate.get(sortendkey);
        			value = {
        				summary: ds.getColumn(row, "summary"),
        				description: ds.getColumn(row, "description"),
        				location: ds.getColumn(row, "location"),
        				status: ds.getColumn(row, "status"),
        				categories: ds.getColumn(row, "categories"),
        				startdate: startdt,
        				enddate: enddt
        			};
        			if ( sortValue )
        			{
        				sortValue.push(value);
        			}
        			else
        			{
        				this._sortedItems.put(sortkey, [value]);
        			}
        			if ( sortEndValue )
        			{
        				sortEndValue.push(value);
        			}
        			else
        			{
        				this._sortedItemsByEnddate.put(sortendkey, [value]);
        			}
        			return value;
        		},
        		_calcVisibleEventItems: function()
        		{
        			if ( this.binddataset == null )
        			{
        				this._timeEvents = [];
        				this._dayEvents = [];
        				this._visibleEventsCache = null;
        				return;
        			}

        			var start = this._visibleStartdate;
        			var end = this._visibleEnddate;
        			var calendar = this._calendar,
        				visibleEventsCache = this._visibleEventsCache,
        				resTime, resDay,
        				evtItem;

        			if ( this._changeddata ) visibleEventsCache = null;

        			end = calendar.addUnits(end, Eco.TimeUnit.DAY, 1);

        			if ( visibleEventsCache && visibleEventsCache.startdate == start &&
        				 visibleEventsCache.enddate == end )
        			{
        				resTime = visibleEventsCache.timeEvents;
        				resDay = visibleEventsCache.dayEvents;
        			}
        			else
        			{
        				resTime = [];
        				resDay = [];
        				var cached = false;
        				if ( visibleEventsCache )
        				{
        					var cacheStart = visibleEventsCache.startdate,
        						cacheEnd = visibleEventsCache.enddate;

        					if ( this._isInRange(start, cacheStart, cacheEnd) && 
        						this._isInRange(end, cacheStart, cacheEnd) )
        					{
        						var cacheTimeEvents = visibleEventsCache.timeEvents,
        							cacheDayEvents = visibleEventsCache.dayEvents;

        						for ( var i = 0, len = cacheTimeEvents.length ; i < len ; i++ )
        						{
        							evtItem = cacheTimeEvents[i];
        							if (this.isEventInRange(evtItem, start, end))
        							{
        								resTime.push(evtItem);
        							}
        						}
        						for ( var i = 0, len = cacheDayEvents.length ; i < len ; i++ )
        						{
        							evtItem = cacheDayEvents[i];
        							if (this.isEventInRange(evtItem, start, end))
        							{
        								resDay.push(evtItem);
        							}
        						}
        						cached = true;
        					}
        				}

        				if ( !cached )
        				{
        					var startkey = this._dateToString(start);
        					var sortedItems = this._sortedItems;
        					var pos = sortedItems.findPos(startkey),
        						orgPos = pos,
        						checkDuplicate = {},
        						evtItems, stopLoop = false;
        					while ( true )
        					{
        						evtItems = sortedItems.getAt(pos);
        						if ( evtItems )
        						{
        							for ( var i = 0, len = evtItems.length ; i < len ; i++ )
        							{
        								evtItem = evtItems[i];
        								if (this.isEventInRange(evtItem, start, end))
        								{
        									if (this.isDayEvent(evtItem))
        									{
        										resDay.push(evtItem);
        									}
        									else
        									{
        										resTime.push(evtItem);
        									}
        								}
        								checkDuplicate[evtItem.key] = true;
        								if ( evtItem.startdate > end )
        								{
        									stopLoop = true;
        									break;
        								}
        							}
        							if ( stopLoop )
        							{
        								break;
        							}
        						}
        						else
        						{
        							break;
        						}
        						pos++;
        					}

        					pos = orgPos;
        					pos--;
        					stopLoop = false;
        					while ( true )
        					{
        						evtItems = sortedItems.getAt(pos);
        						if ( evtItems )
        						{
        							for ( var i = 0, len = evtItems.length ; i < len ; i++ )
        							{
        								evtItem = evtItems[i];
        								if ( evtItem.enddate > end )
        								{
        									if (this.isEventInRange(evtItem, start, end))
        									{
        										if (this.isDayEvent(evtItem))
        										{
        											resDay.push(evtItem);
        										}
        										else
        										{
        											resTime.push(evtItem);
        										}
        									}
        									checkDuplicate[evtItem.key] = true;
        								}
        								else
        								{
        									stopLoop = true;
        									break;
        								}
        							}
        							if ( stopLoop )
        							{
        								break;
        							}
        						}
        						else
        						{
        							break;
        						}
        						pos--;
        					}

        					sortedItems = this._sortedItemsByEnddate;
        					var endkey = this._dateToString(end);
        					pos = sortedItems.findPos(endkey);
        					evtItems = null;
        					stopLoop = false;
        					while ( true )
        					{
        						evtItems = sortedItems.getAt(pos);
        						if ( evtItems )
        						{
        							for ( var i = 0, len = evtItems.length ; i < len ; i++ )
        							{
        								evtItem = evtItems[i];
        								if ( !checkDuplicate[evtItem.key] )
        								{
        									if (this.isEventInRange(evtItem, start, end))
        									{
        										if (this.isDayEvent(evtItem))
        										{
        											resDay.push(evtItem);
        										}
        										else
        										{
        											resTime.push(evtItem);
        										}
        									}
        								}
        								if ( evtItem.enddate < start )
        								{
        									stopLoop = true;
        									break;
        								}
        							}
        							if ( stopLoop )
        							{
        								break;
        							}
        						}
        						else
        						{
        							break;
        						}
        						pos++;
        					}

        					this._visibleEventsCache = {
        						startdate: this._cloneDate(start),
        						enddate: this._cloneDate(end),
        						timeEvents: resTime,
        						dayEvents: resDay
        					};
        				}
        				this._changeEventItems = 3;
        			}
        			this._timeEvents = resTime;
        			this._dayEvents = resDay;
        		},
        		isEventInRange: function(evtItem,startdt,enddt)
        		{
        			var itemStart = evtItem.startdate,
        				itemEnd = evtItem.enddate;

        			if (itemEnd != startdt && itemStart != enddt && itemStart <= enddt)
        			{
        				return startdt <= itemEnd;
        			}
        			return false;
        		},
        		isDayEvent: function(evtItem,startdt,enddt)
        		{
        			var sdate, edate;
        			if ( evtItem )
        			{
        				sdate = evtItem.startdate;
        				edate = evtItem.enddate;
        			}
        			else
        			{
        				sdate = startdt;
        				edate = enddt;
        			}
        			var tmpdt = this._calendar.addUnits(sdate, Eco.TimeUnit.DAY, 1);
        			return tmpdt <= edate;
        		},
        		setVisibleArea: function(dt,mode)
        		{
        			var oldstart = this._visibleStartdate,
        				oldend = this._visibleEnddate,
        				oldmode = this._mode;

        			var isRange = false;
        			if ( Eco.isDate(dt) )
        			{
        				if ( Eco.isDate(mode) )
        				{
        					this._calcFromDateRange(dt, mode, this.workWeekRange);
        					isRange = true;
        				}
        				else if ( Eco.isString(mode) && ( mode in Eco.Scheduler.Data.modeKind ) )
        				{
        					this._calcFromBaseDate(dt, mode, this.workWeekRange);
        				}
        				else
        				{
        					mode = "week";
        					this._calcFromBaseDate(dt, mode, this.workWeekRange);
        				}
        			}
        			else
        			{
        				Eco.Logger.error("arguments are not correct! =>" + Eco.Logger.inspect(arguments, false, 0, false));
        			}

        			if ( oldstart != this._visibleStartdate ||
        				oldend != this._visibleEnddate ||
        				oldmode != this._mode )
        			{
        				this._changedVisibleRange = true;
        				if ( oldmode != this._mode )
        				{
        					this._changedMode = true;
        				}
        			}

        			if ( this._changeddata || this._changedVisibleRange )
        			{
        				this._calcVisibleEventItems();
        				this._changeddata = false;
        			}
        			return isRange;
        		},
        		_calcFromDateRange: function(sDt,eDt,workweekrange)
        		{
        			if (sDt > eDt )
        			{
        				var tmpDate = eDt;
        				eDt = sDt;
        				sDt = tmpDate;
        			}
        			sDt.setHours(0,0,0,0);
        			eDt.setHours(0,0,0,0);
        			
        			this._startdate = sDt;
        			this._enddate = eDt;
        			var calendar = this._calendar;
        			var nDay = calendar.getDays(sDt, eDt) + 1;
        			if ( nDay == 1)
        			{
        				this._visibleStartdate = calendar.floor(sDt, Eco.TimeUnit.DAY, 1);
        				this._visibleEnddate = eDt;
        				this._rowCount = 1;
        				this._columnCount = 1;
        				this._mode = "day";
        			}
        			else if ( nDay <= 7)
        			{
        				this._visibleStartdate = calendar.floor(sDt, Eco.TimeUnit.DAY, 1);
        				this._visibleEnddate = calendar.floor(eDt, Eco.TimeUnit.DAY, 1);
        				if (this._visibleStartdate.getDay() == workweekrange[0] && this._visibleEnddate.getDay() == workweekrange[1])
        				{
        					this._mode = "workweek";
        				}
        				else
        				{
        					this._mode = "week";
        				}
        				this._rowCount = 1;
        				this._columnCount = nDay;
        			}
        			else
        			{
        				var startWeekDate = this._getFirstDayOfWeek(sDt);
        				var endWeekDate = this._getFirstDayOfWeek(eDt);
        				endWeekDate = calendar.addUnits(endWeekDate, Eco.TimeUnit.DAY, 6);
        				var nDay = calendar.getDays(startWeekDate, endWeekDate) + 1;
        				if ( nDay > 42)
        				{
        					endWeekDate = calendar.addUnits(startWeekDate, Eco.TimeUnit.DAY, 41);
        					nDay = 42;
        				}
        				this._visibleStartdate = startWeekDate;
        				if ( this._weekOffset != 0)
        				{
        					this._visibleStartdate = calendar.addUnits(this._visibleStartdate, Eco.TimeUnit.WEEK, this._weekOffset, true);
        				}
        				this._visibleEnddate = endWeekDate;
        				if ( this._weekOffset != 0)
        				{
        					this._visibleEnddate = calendar.addUnits(this._visibleEnddate, Eco.TimeUnit.WEEK, this._weekOffset, true);
        				}
        				this._columnCount = 7;
        				this._rowCount = Math.ceil(nDay / 7);
        				this._mode = "month";
        			}
        		},
        		_calcFromBaseDate: function(baseDt,mode,workweekrange)
        		{
        			var tmpDt;
        			var nRange,
        				calendar = this._calendar;
        			baseDt.setHours(0,0,0,0);

        			switch(mode)
        			{
        				case "day":
        				{
        					tmpDt = calendar.floor(baseDt, Eco.TimeUnit.DAY, 1);
        					this._visibleStartdate = tmpDt;
        					this._visibleEnddate = tmpDt;
        					this._rowCount = 1;
        					this._columnCount = 1;
        					this._mode = mode;
        					this._startdate = baseDt;
        					this._enddate = baseDt;
        					break;
        				}
        				case "workweek":
        				{
        					tmpDt = calendar.floor(baseDt, Eco.TimeUnit.DAY, 1);
        					nRange = workweekrange[1] - workweekrange[0];
        					tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, 0-(tmpDt.getDay() - workweekrange[0]));
        					this._visibleStartdate = tmpDt;
        					this._visibleEnddate = calendar.addUnits(this._visibleStartdate, Eco.TimeUnit.DAY, nRange);
        					this._rowCount = 1;
        					this._columnCount = nRange + 1;
        					this._mode = mode;
        					this._startdate = this._cloneDate(baseDt);
        					this._enddate = this._visibleEnddate;
        					break;
        				}
        				case "week":
        				{
        					this._visibleStartdate = this._getFirstDayOfWeek(baseDt);
        					this._visibleEnddate = calendar.addUnits(this._visibleStartdate, Eco.TimeUnit.DAY, 6);
        					this._rowCount = 1;
        					this._columnCount = 7;
        					this._mode = mode;
        					this._startdate = this._cloneDate(baseDt);
        					this._enddate = this._visibleEnddate;
        					break;
        				}
        				case "month":
        				{
        					this._columnCount = 7;
        					tmpDt = this._getFirstDayOfMonth(baseDt);
        					this._visibleStartdate = this._getFirstDayOfWeek(tmpDt);
        					if (this._weekOffset != 0)
        					{
        						this._visibleStartdate = calendar.addUnits(this._visibleStartdate, Eco.TimeUnit.WEEK, this._weekOffset, true);
        					}
        					this._startdate = this._visibleStartdate;
        					tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.MONTH, 1);
        					tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, -1, true);
        					tmpDt = this._getFirstDayOfWeek(tmpDt);
        					this._visibleEnddate = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, 6, true);
        					if (this._weekOffset != 0)
        					{
        						this._visibleEnddate = calendar.addUnits(this._visibleEnddate, Eco.TimeUnit.WEEK, this._weekOffset, true);
        					}
        					this._enddate = this._visibleEnddate;
        					nRange = calendar.getDays(this._visibleStartdate, this._visibleEnddate) + 1;
        					this._rowCount = Math.ceil(nRange / 7);
        					this._mode = mode;
        					break;
        				}
        				default:
        				{
        					break;
        				}
        			}
        		},
        		_getFirstDayOfWeek: function(dt)
        		{
        			return this._calendar.floor(dt, Eco.TimeUnit.WEEK, 1);
        		},
        		_getFirstDayOfMonth: function(dt)
        		{
        			return this._calendar.floor(dt, Eco.TimeUnit.MONTH, 1);
        		},
        		_cloneDate: function(dt)
        		{
        			return dt == null ? null : (new Date(dt.getTime()));
        		},
        		_isInRange: function(dt,startdt,enddt)
        		{
        			var startdtTime = (startdt == null ? -1 : startdt.getTime()),
        				enddtTime = (enddt == null ? -1 : enddt.getTime());
        			var dtTime = dt.getTime();
        			var bStart = (startdtTime == -1 ? true : ( dtTime >= startdtTime ));
        			var bEnd = (enddtTime == -1 ? true : ( dtTime <= enddtTime ));
        			if ( bStart )
        			{
        				return bEnd;
        			}
        			return false;
        		},
        		//일정 data가 보여주는 column들(기간) 내에 서로 중접되는 일정에 대한 데이터를 관리한다.
        		createOverlapLayout: function(isDayMode,isExpandPass)
        		{
        			var val = {
        				dayMode: isDayMode,
        				startdate: null,
        				expandPass: isExpandPass,
        				enddate: null,
        				calcMax: null,
        				layoutInfo: null,
        				maxColumn: [],
        				columns: [-1],
        				columnslength: null
        			};
        			return val;
        		},
        		calcOverlapping: function(overLapLayout,evtItems,startdt,enddt)
        		{
        			overLapLayout.layoutInfo = {};
        			overLapLayout.maxColumn = [];
        			overLapLayout.columns = [];
        			var seq = 0, evtItem,
        				cnt = evtItems.length;
        			while ( seq < cnt )
        			{
        				evtItem = evtItems[seq];
        				this._overlapLayoutPass1(overLapLayout, evtItem);
        				seq++;
        			}
        			overLapLayout.maxColumn[0] = overLapLayout.columns.length;
        			if ( overLapLayout.expandPass )
        			{
        				this._overlapLayoutLayoutPass2(overLapLayout);
        			}
        			if ( startdt != null && !overLapLayout.dayMode )
        			{
        				//this._overlapLayoutCalcMaxPass(overLapLayout, startdt, enddt);
        			}
        		},
        		_overlapLayoutPass1: function(overLapLayout,evtItem)
        		{
        			var evtItems, isOverlap, subSeq,
        				seq = 0,
        				tmplayoutinfo = {offset:0,nb:1},
        				columns = overLapLayout.columns,
        				cnt = columns.length;

        			overLapLayout.layoutInfo[evtItem.key] = tmplayoutinfo;

        			while ( seq < cnt )
        			{
        				evtItems = columns[seq];
        				isOverlap = false;
        				subSeq = 0;
        				while ( subSeq < evtItems.length && !isOverlap )
        				{
        					if ( evtItems[subSeq].startdate < evtItem.enddate && evtItem.startdate < evtItems[subSeq].enddate )
        					{
        						isOverlap = true;
        					}
        					subSeq++;
        				}
        				if ( !isOverlap )
        				{
        					tmplayoutinfo.offset = seq;
        					evtItems.push(evtItem);
        					return;
        				}
        				seq++;
        			}
        			columns.push([evtItem]);
        			tmplayoutinfo.offset = columns.length - 1;
        			overLapLayout.columnslength = columns.length - 1;
        		},
        		_overlapLayoutLayoutPass2: function(overLapLayout)
        		{
        			var columns = overLapLayout.columns,
        				cnt = columns.length,
        				layoutInfo = overLapLayout.layoutInfo,
        				dsMap = this._dsMap,
        				seq = 0,
        				evtItem1, evtItem2, overflag, evtItems1, evtItems2,
        				tmplayoutInfo, subSeq, subSeq0, subSeq1, subSeq2;

        			while ( seq < cnt )
        			{
        				evtItems1 = columns[seq];
        				subSeq = 0;
        				while ( subSeq < evtItems1.length)
        				{
        					evtItem1 = evtItems1[subSeq];
        					subSeq0 = 0;
        					overflag = false;
        					subSeq1 = seq + 1;
        					while ( subSeq1 < cnt && !overflag )
        					{
        						evtItems2 = columns[subSeq1];
        						subSeq2 = 0;
        						while ( subSeq2 < evtItems2.length && !overflag )
        						{
        							evtItem2 = evtItems2[subSeq2];
        							if ( evtItem1.startdate < evtItem2.enddate && evtItem2.startdate < evtItem1.enddate )
        							{
        								overflag = true;
        							}
        							subSeq2++;
        						}
        						if ( !overflag )
        						{
        							subSeq0++;
        						}
        						subSeq1++;
        					}	
        					tmplayoutInfo = layoutInfo[evtItem1.key];
        					tmplayoutInfo.nb = tmplayoutInfo.nb + subSeq0;
        					subSeq++;
        				}
        				seq++;
        				cnt--;
        			}
        			overLapLayout.columnslength = cnt;
        		},
        		_overlapLayoutCalcMaxPass: function(overLapLayout,startdt,enddt)
        		{
        			var calendar = this._calendar,
        				columns = overLapLayout.columns,
        				maxColumn = [],
        				overflag, seq, evtItem;

        			overLapLayout.maxColumn = maxColumn;

        			var tmpSdate = this._cloneDate(startdt);
        			var tmpEdate = calendar.addUnits(tmpSdate, Eco.TimeUnit.DAY, 1);
        			tmpEdate = calendar.floor(tmpEdate, Eco.TimeUnit.DAY, 1);

        			while ( tmpSdate < enddt )
        			{
        				overflag = false;
        				seq = columns.length - 1;
        				while ( seq >= 0 && !overflag )
        				{
        					var evtItems = columns[seq];
        					for ( var i = 0, len = evtItems.length ; i < len ; i++ )
        					{
        						evtItem = evtItems[i];
        						overflag = this.isOverlapping(tmpSdate, tmpEdate, evtItem.startdate, evtItem.enddate, false);
        						if (overflag)
        						{
        							maxColumn[tmpSdate.getDate()] = seq + 1;
        							break;
        						}
        					}
        					seq--;
        				} 
        				if ( !overflag )
        				{
        					maxColumn[tmpSdate.getDate()] = 0;
        				}
        				tmpSdate = calendar.addUnits(tmpSdate, Eco.TimeUnit.DAY, 1, true);
        				tmpSdate = calendar.floor(tmpSdate, Eco.TimeUnit.DAY, 1);
        				tmpEdate = calendar.addUnits(tmpEdate, Eco.TimeUnit.DAY, 1, true);
        				tmpEdate = calendar.floor(tmpEdate, Eco.TimeUnit.DAY, 1);
        				tmpEdate.setHours(0,0,0);
        			}
        		},
        		getOverlapLayoutInfo: function(overLapLayout,evtItem)
        		{
        			if ( evtItem == null || overLapLayout.layoutInfo == null)
        			{
        				return null;
        			}
        			//return overLapLayout.layoutInfo[this._dsMap.getIndex(evtItem.key)];
        			return overLapLayout.layoutInfo[evtItem.key];
        		},
        		getOverlapLayoutCount: function(overLapLayout,nDay)
        		{
        			if ( nDay == null ) nDay = -1;
        			if ( nDay != -1)
        			{
        				return overLapLayout.maxColumn[nDay];
        			}
        			if ( overLapLayout.columns == null)
        			{
        				return 0;
        			}
        			return overLapLayout.columns.length;
        		},
        		isOverlapping: function(startdt1,enddt1,startdt2,enddt2,allowEquals)
        		{
        			if ( startdt1 == null || enddt1 == null ||
        				startdt2 == null || enddt2 == null )
        			{
        				return false;
        			}
        			if ( allowEquals )
        			{
        				if ( startdt1 > enddt2 || startdt2 > enddt1 )
        				{
        					return false;
        				}
        			}
        			else if (startdt1 >= enddt2 || startdt2 >= enddt1)
        			{
        				return false;
        			}
        			return true;
        		},
        		calcHProjection: function(sDt,eDt)
        		{
        			if (sDt >= eDt)
        			{
        				return 0;
        			}
        			var calendar = this._calendar,
        				columnWidth = this._columnWidth,
        				nDays = calendar.getDays(sDt, eDt);
        			var nMinute = eDt.getHours() * 60 + eDt.getMinutes();
        			var startPos = parseInt(columnWidth * nDays);	
        			//var startPos = parseInt(columnWidth * nDays - nDays);	
        			return startPos + Math.round(columnWidth * nMinute / 1440); // 1440 ==> 24 * 60
        		},
        		calcVProjection: function(baseDt,dt,dayHeight)
        		{
        			var tmpDt = this._calendar.addUnits(baseDt, Eco.TimeUnit.DAY, 1);
        			var nMinute = (dt.getHours() - baseDt.getHours()) * 60 + (dt.getMinutes() - baseDt.getMinutes());
        			return dt >= tmpDt ? dayHeight : Math.round(nMinute * dayHeight / 1440);
        		},
        		calcDateRangeOverlap: function(sdt1,edt1,sdt2,edt2,notEquals)
        		{
        			if (sdt1 == null || edt1 == null || sdt2 == null || edt2 == null)
        			{
        				return null;
        			}
        			var nsTime1 = sdt1.getTime();
        			var neTime1 = edt1.getTime();
        			var nsTime2 = sdt2.getTime();
        			var neTime2 = edt2.getTime();
        	
        			if (notEquals)
        			{
        				if (nsTime1 > neTime2 || nsTime2 > neTime1)
        				{
        					return null;
        				}
        			}
        			else if (nsTime1 >= neTime2 || nsTime2 >= neTime1)
        			{
        				return null;
        			}
        			return [new Date(Math.max(nsTime1, nsTime2)), new Date(Math.min(neTime1, neTime2))];
        		},
        		calcDateRangeXOR : function(sdt1,edt1,sdt2,edt2)
        		{
        			var firstFlag = true;
        			if (sdt1 != null)
        			{
        				firstFlag = edt1 == null;
        			}
        			var secondFlag = true;
        			if (sdt2 != null)
        			{
        				secondFlag = edt1 == null;
        			}
        			if (firstFlag && secondFlag )
        			{
        				return [];
        			}
        			if (firstFlag || secondFlag )
        			{
        				return [
        					{
        						start: (firstFlag ? sdt2 : sdt1),
        						end: (firstFlag ? edt2 : edt1)
        					}
        				];
        			}
        			if ( sdt1.getTime() == sdt2.getTime() && 
        				 edt1.getTime() == edt2.getTime() )
        			{
        				return [];
        			}

        			if (sdt1 >= edt2 || sdt2 >= edt1 )
        			{
        				return [
        					{
        						start:sdt1,
        						end:edt1
        					},
        					{
        						start:sdt2,
        						end:edt2
        					}
        				];
        			}
        			var bStart = sdt1 > sdt2;
        			var bEnd = edt1 > edt2;
        			return [
        				{
        					start: (bStart ? sdt2 : sdt1),
        					end: (bStart ? sdt1 : sdt2)
        				},
        				{
        					start: (bEnd ? edt2 : edt1),
        					end: (bEnd ? edt1 : edt2)
        				}
        			];
        		}
        	});
        }
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
