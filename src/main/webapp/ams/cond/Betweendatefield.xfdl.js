﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Betweendatefield");
                this.set_classname("style01");
                this.set_titletext("기간 달력");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,377,28);
            }
            this.style.set_background("transparent");

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Calendar("cal_to", "absolute", "254", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_value("null");
            obj.style.set_cursor("hand");

            obj = new Static("Static14", "absolute", "243", "5", "10", "18", null, null, this);
            obj.set_taborder("6");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_from", "absolute", "116", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_value("null");
            obj.style.set_cursor("hand");

            obj = new Static("label", "absolute", "18", "0", "98", "28", null, null, this);
            obj.set_taborder("8");
            obj.set_cssclass("sta_WF_label");
            obj.set_wordwrap("char");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_use", "absolute", "0", "3", "15", "21", null, null, this);
            obj.set_taborder("9");
            obj.set_value("true");
            obj.set_cssclass("chk_WF_Srh");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 377, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("기간 달력");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Betweendatefield.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Betweendatefield.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;
        this.gv_fr_date_value = 0;
        this.gv_to_date_value = 0;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	var sToday = this.parent.parent.gfn_today();
        	var fromdate = "";
        	var todate = "";
        	
        	if(this.gv_fr_date_value != 0) fromdate = this.parent.parent.gfn_addDate(sToday, this.gv_fr_date_value);
        	else fromdate = sToday;
        	if(this.gv_to_date_value != 0) todate = this.parent.parent.gfn_addDate(sToday, this.gv_to_date_value);
        	else todate = sToday;
        	
        	this.cal_from.set_value(fromdate);
        	this.cal_to.set_value(todate);
        	
        	if( !Eco.isEmpty(this.parent.parent.p_fr_date_value ) ) {
                fromdate = Eco.date.addDate(sToday, nexacro.toNumber( this.parent.parent.p_fr_date_value)  );
        		this.cal_from.set_value(fromdate); 
        	}
        	if( !Eco.isEmpty(this.parent.parent.p_to_date_value ) ) {
                todate = Eco.date.addDate(sToday, nexacro.toNumber( this.parent.parent.p_to_date_value)  );
        		this.cal_to.set_value(todate); 
        	}
        }

        /* cal_from_onkeydown 실행 */
        this.cal_from_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_to.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        /* cal_to_onkeydown 실행 */
        this.cal_to_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_from.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        /* cal_from_canchange 실행 */
        this.cal_from_canchange = function(obj,e)
        {
        	var calFormValue = e.postvalue;
        	var calToValue = this.cal_to.value;
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* cal_to_canchange 실행 */
        this.cal_to_canchange = function(obj,e)
        {
        	var calFormValue = this.cal_from.value;
        	var calToValue = e.postvalue;
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* label_onclick 실행 */
        this.label_onclick = function(obj,e)
        {
        	if(this.chk_use.readonly) return;
        	
        	if(this.chk_use.value) this.chk_use.set_value(false);
        	else this.chk_use.set_value(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.cal_to.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_from.addEventHandler("onkeydown", this.cal_from_onkeydown, this);
            this.label.addEventHandler("onclick", this.label_onclick, this);

        };

        this.loadIncludeScript("Betweendatefield.xfdl", true);

       
    };
}
)();
