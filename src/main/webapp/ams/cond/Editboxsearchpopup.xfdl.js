﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Editboxsearchpopup");
                this.set_classname("style01");
                this.set_titletext("입력 검색");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,377,28);
            }
            this.style.set_background("transparent");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_operator", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_codeValue", this);
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row/></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_find", "absolute", "353", "3", "24", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_value", "absolute", "208", "3", "143", "22", null, null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_combo", "absolute", "116", "3", "90", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_innerdataset("@ds_operator");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("label", "absolute", "18", "0", "98", "28", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("sta_WF_label");
            obj.set_wordwrap("char");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_use", "absolute", "0", "3", "15", "21", null, null, this);
            obj.set_taborder("4");
            obj.set_value("true");
            obj.set_cssclass("chk_WF_Srh");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 377, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("입력 검색");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_value","value","ds_codeValue","code");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Editboxsearchpopup.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Editboxsearchpopup.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;
        this.gv_searchValue;
        this.gv_divId;
        this.gv_label;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	this.parent.parent.gfn_getCode("SEARCHOPERATOR", this.ds_operator);
        }

        /* cbo_combo_onkeydown 실행 */
        this.cbo_combo_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.parent.parent.btn_search.click();
        }

        /* edt_value_onkeydown 실행 */
        this.edt_value_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.parent.parent.btn_search.click();
        }

        this.btn_find_onclick = function(obj,e)
        {
        	var sList = "";
        	var sListKey = "";
        	var workTab = this.parent.parent.gv_workTab;
        	
        	if(this.gv_label == "ICKEY" || this.gv_label == "ICUTKEY" || this.gv_label == "SBKEY" || this.gv_label == "ICGRKEY" ||
        	   this.gv_label == "CTGKEY" || this.gv_label == "ACKEY" || this.gv_label == "SEICKEY" || this.gv_label == "IBHDKEY" ||
        	   this.gv_label == "OBHDKEY" || this.gv_label == "WAHDKEY" || this.gv_label == "WAHDKEY" || this.gv_label == "PTN_ACKEY" ||
        	   this.gv_label == "H_LEV_CTGNAME" || this.gv_label == "M_LEV_CTGNAME" || this.gv_label == "B_LEV_CTGNAME" || 
        	   this.gv_label == "POGRPKEY" || this.gv_label == "REP_ACKEY" || this.gv_label == "STORE_KEY" || this.gv_label == "STORE_NAME"){
        		if(workTab == 0){
        			if(this.parent.parent.gfn_isNull(this.parent.parent.owObjId)) sList = "";
        			else sList = this.parent.parent.owObjId.edt_value.value;
        		}else{
        			if(this.parent.parent.gfn_isNull(this.parent.parent.owObjId2)) sList = "";
        			else sList = this.parent.parent.owObjId2.edt_value.value;
        		}
        		
        		if(this.parent.parent.gfn_isNotNull(sList)) sListKey = "OWKEY_SELECTION_LIST";
        	}else if(this.gv_label == "MEKEY" || this.gv_label == "USKEY"){
        		if(workTab == 0){
        			if(this.parent.parent.gfn_isNull(this.parent.parent.acObjId)) sList = "";
        			else sList = this.parent.parent.acObjId.edt_value.value;
        		}else{
        			if(this.parent.parent.gfn_isNull(this.parent.parent.acObjId2)) sList = "";
        			else sList = this.parent.parent.acObjId2.edt_value.value;
        		}
        		
        		if(this.parent.parent.gfn_isNotNull(sList)) sListKey = "APKEY_SELECTION_LIST";
        	}
        	
        	var sSearchId = this.gv_searchValue.split(";");
        	this.parent.parent.gv_divId = this.gv_divId;
        		
        	var sHdkey = "";
        	
        	if(application.gv_activeApp == "WMS") sHdkey = "ctcd_hdkey";
        	else if(application.gv_activeApp == "ICOM") sHdkey = "orcd_hdkey";
        	else sHdkey = "adcd_hdkey";
        	
        	if(sSearchId.length > 1){
        		sListKey += "|" + sHdkey;
        		sList += "|" + sSearchId[1];
        	}
        	
        	if(application.gv_activeApp == "TMS" && sSearchId.length == 3){
        		sListKey += "|type";
        		sList += "|" + sSearchId[2];
        	}
        	
        	var oArg = { divId:this.gv_divId
        				,searchId:sSearchId[0]
        				,putObj:this.edt_value
        				,putKey:sListKey
        				,putValue:sList
        				,putCallback:"fn_callBackSearchPop"
        			   };
        	
        	this.parent.parent.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 547, "");
        }

        /* label_onclick 실행 */
        this.label_onclick = function(obj,e)
        {
        	if(this.chk_use.readonly) return;
        	
        	if(this.chk_use.value) this.chk_use.set_value(false);
        	else this.chk_use.set_value(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_find.addEventHandler("onclick", this.btn_find_onclick, this);
            this.edt_value.addEventHandler("onkeydown", this.edt_value_onkeydown, this);
            this.edt_value.addEventHandler("onchanged", this.edt_value_onchanged, this);
            this.cbo_combo.addEventHandler("onkeydown", this.cbo_combo_onkeydown, this);
            this.label.addEventHandler("onclick", this.label_onclick, this);

        };

        this.loadIncludeScript("Editboxsearchpopup.xfdl", true);

       
    };
}
)();
