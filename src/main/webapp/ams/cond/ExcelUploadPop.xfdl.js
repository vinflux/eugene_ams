﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("WorkForm");
                this.set_classname("frame_top");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,1230,670);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_split_h", "absolute", "20", "684", null, "6", "20", null, this);
            obj.set_taborder("3");
            obj.set_visible("false");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Static("sta_gubun", "absolute", "20", "44", null, "3", "20", null, this);
            obj.set_taborder("7");
            obj.style.set_background("#454545ff");
            this.addChild(obj.name, obj);

            obj = new Static("sta_title", "absolute", "23", "19", "600", "20", null, null, this);
            obj.set_taborder("8");
            obj.set_text("재고 이동");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_showHideH", "absolute", null, "684", "35", "11", "20", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_st_open");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new FileDownload("fdw_excelFile", "absolute", "20", "710", "99", "28", null, null, this);
            obj.set_taborder("13");
            obj.getSetter("retry").set("0");
            obj.set_text("fdw_excelFile");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new WebBrowser("web_excelFile", "absolute", "10.81%", "712", null, "23", "79.76%", null, this);
            obj.set_taborder("14");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new PopupDiv("popDiv_ExButton", "absolute", "265", "710", "150", "24", null, null, this);
            obj.set_text("popDiv_ExButton");
            obj.set_visible("true");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_ExButton", "absolute", "0", "0", null, null, "0", "0", this.popDiv_ExButton);
            obj.set_taborder("0");
            obj.style.set_padding("0 5 0 5");
            obj.set_scrollbars("autovert");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"150\"/></Columns><Rows><Row size=\"24\"/></Rows><Band id=\"body\"><Cell text=\"bind:name\"/></Band></Format></Formats>");
            this.popDiv_ExButton.addChild(obj.name, obj);

            obj = new Button("btn_bookmark", "absolute", "128", "20", "26", "20", null, null, this);
            obj.set_taborder("10");
            obj.set_cssclass("btn_WF_mymenu_add");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_help", "absolute", "98", "20", "26", "20", null, null, this);
            obj.set_taborder("11");
            obj.set_cssclass("btn_WF_about");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_changeSearch", "absolute", "158", "20", "76", "20", null, null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_SrhChange");
            obj.set_text("  검색영역");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_split_v", "absolute", "423", "84", "6", null, null, "20", this);
            obj.set_taborder("16");
            obj.set_cssclass("btn_LF_SHide");
            obj.set_visible("false");
            obj.style.set_cursor("e-resize");
            this.addChild(obj.name, obj);

            obj = new Button("btn_showHideW", "absolute", "423", "84", "11", "62", null, null, this);
            obj.set_taborder("17");
            obj.set_cssclass("btn_WF_sl_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Div("div_cond", "absolute", "20", "84", "403", null, null, "20", this);
            obj.set_taborder("18");
            obj.set_scrollbars("none");
            obj.set_cssclass("btn_LF_SHide");
            this.addChild(obj.name, obj);

            obj = new Div("div_work", "absolute", "451", "54", null, null, "20", "20", this);
            obj.set_taborder("19");
            obj.set_async("false");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Tab("tab_header", "absolute", "0", "0", null, null, "0", "0", this.div_work);
            obj.set_taborder("0");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.div_work.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.div_work.tab_header);
            obj.set_text("업로드");
            this.div_work.tab_header.addChild(obj.name, obj);
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this.div_work.tab_header.tabpage1);
            obj.set_taborder("0");
            obj.set_scrollbars("none");
            this.div_work.tab_header.tabpage1.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_work.tab_header.tabpage1.div_splitTop);
            obj.set_taborder("31");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_work.tab_header.tabpage1.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "34", null, null, "0", "28", this.div_work.tab_header.tabpage1.div_splitTop);
            obj.set_taborder("32");
            obj.set_binddataset("ds_order_temp");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellsizebandtype("allband");
            obj.set_cellmovingtype("col,band");
            obj.set_treeusecheckbox("false");
            obj.set_treeinitstatus("expand,all");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/></Band></Format></Formats>");
            this.div_work.tab_header.tabpage1.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "5", "33", "24", "0", null, this.div_work.tab_header.tabpage1.div_splitTop);
            obj.set_taborder("33");
            obj.set_cssclass("btn_WF_save");
            obj.set_enable("false");
            this.div_work.tab_header.tabpage1.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_excelUp", "absolute", null, "5", "33", "24", "37", null, this.div_work.tab_header.tabpage1.div_splitTop);
            obj.set_taborder("34");
            obj.set_cssclass("btn_WF_excelup");
            obj.set_enable("false");
            this.div_work.tab_header.tabpage1.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_sampleDown", "absolute", null, "5", "33", "24", "74", null, this.div_work.tab_header.tabpage1.div_splitTop);
            obj.set_taborder("35");
            obj.set_cssclass("btn_WF_sampledown");
            this.div_work.tab_header.tabpage1.div_splitTop.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.div_work.tab_header);
            obj.set_text("관리");
            this.div_work.tab_header.addChild(obj.name, obj);
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this.div_work.tab_header.tabpage2);
            obj.set_taborder("0");
            obj.set_scrollbars("none");
            this.div_work.tab_header.tabpage2.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "5", "33", "24", "0", null, this.div_work.tab_header.tabpage2.div_splitTop);
            obj.set_taborder("26");
            obj.set_cssclass("btn_WF_excel");
            this.div_work.tab_header.tabpage2.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_work.tab_header.tabpage2.div_splitTop);
            obj.set_taborder("27");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_work.tab_header.tabpage2.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "34", null, null, "0", "28", this.div_work.tab_header.tabpage2.div_splitTop);
            obj.set_taborder("28");
            obj.set_binddataset("ds_header");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_treeusecheckbox("false");
            obj.set_treeinitstatus("expand,all");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/></Band></Format></Formats>");
            this.div_work.tab_header.tabpage2.div_splitTop.addChild(obj.name, obj);

            obj = new Static("sta_navi", "absolute", null, "21", "400", "20", "23", null, this);
            obj.set_taborder("20");
            obj.set_text("navi");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_navi");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "38", "20", "22", null, this);
            obj.set_taborder("21");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 150, 24, this.popDiv_ExButton,
            	//-- Layout function
            	function(p) {
            		p.set_text("popDiv_ExButton");
            		p.set_visible("true");

            	}
            );
            this.popDiv_ExButton.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work.tab_header.tabpage1.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_scrollbars("none");

            	}
            );
            this.div_work.tab_header.tabpage1.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work.tab_header.tabpage1,
            	//-- Layout function
            	function(p) {
            		p.set_text("업로드");

            	}
            );
            this.div_work.tab_header.tabpage1.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work.tab_header.tabpage2.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_scrollbars("none");

            	}
            );
            this.div_work.tab_header.tabpage2.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work.tab_header.tabpage2,
            	//-- Layout function
            	function(p) {
            		p.set_text("관리");

            	}
            );
            this.div_work.tab_header.tabpage2.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("19");
            		p.set_async("false");
            		p.set_scrollbars("none");

            	}
            );
            this.div_work.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 670, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_top");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_help","tooltiptext","gds_lang","MENU SCRREEN SYSTEM  INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_work.tab_header.tabpage1","text","gds_lang","UPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_work.tab_header.tabpage2","text","gds_lang","MANAGE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_work.tab_header.tabpage1.div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_work.tab_header.tabpage2.div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_work.tab_header.tabpage1.div_splitTop.btn_excelUp","tooltiptext","gds_lang","EXCELUPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_work.tab_header.tabpage1.div_splitTop.btn_sampleDown","tooltiptext","gds_lang","SAMPLE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("ExcelUploadPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ExcelUploadPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : XXAMC10005.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 메뉴관리 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_menuId;
        this.gv_menuNm;
        this.gv_navi;
        this.gv_menuParam;
        this.gv_split = "";
        this.gv_searchValue = "L";
        this.viewSpace = 20;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        this.form_onload = function(obj,e)
        {
            //var sParam = obj.getOwnerFrame().arguments;
            //console.log(sParam);

        //    if(this.gfn_isNotNull(sParam)){
        //         this.gv_menuId = obj.getOwnerFrame().arguments["menuId"];
        //         this.gv_menuNm = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.gv_menuId), "mename");
        //         this.gv_navi = this.gfn_getMenuNavi(this.gv_menuId);
        //         this.gv_menuParam = obj.getOwnerFrame().arguments["menuParam"];

        		this.gv_menuId = this.parent.menuId;
        		this.gv_menuNm = this.parent.menuNm;

                
                this.sta_title.set_text(this.gv_menuNm);
                //this.sta_navi.set_text(this.gv_navi);
                
                //var sMenuUrl = obj.getOwnerFrame().arguments["pageUrl"];
                //this.div_cond.set_url("svc::ams/ams/cond/SearchLeft.xfdl");
                this.div_cond.set_url("cond::SearchLeftPop.xfdl");
                //this.div_work.set_url(sMenuUrl);
        //    }
            
            var langCnt = this.gv_menuNm.length;
            var spaceCnt = langCnt - nexacro.replaceAll(this.gv_menuNm, " ", "").length;
            var langSize = 0;
            
            if(this.gfn_getCommon("LAKEY") == "KOR") langSize = (langCnt - spaceCnt) * 15;
            else langSize = (langCnt - spaceCnt) * 10;
            
            var spaceSize = spaceCnt * 5;
            var nextSpace = 30;
        	
        	this.btn_help.set_left(10 + this.viewSpace + langSize + spaceSize);
        	this.btn_bookmark.set_left(10 + this.viewSpace + langSize + spaceSize + nextSpace);
        	this.btn_changeSearch.set_left(10 + this.viewSpace + langSize + spaceSize + nextSpace*2);
        	
        	this.gfn_setLocale(this); // 시스템 언어 설정
        	
        	this.fn_gridSearch();
        	
        }

        this.form_onactivate = function(obj,e)
        {
            this.gfn_showMenu(this.gv_menuId);
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        this.fn_gridSearch = function(){
        	var tabIndex = this.div_work.tab_header.tabindex;
        	
        	console.log("===index===");
        	console.log(tabIndex);
        	
        	if(tabIndex != 1){
        	
        		this.owkey = this.ds_searchList.getColumn(0,"value");
        		this.ultypeCode = this.ds_searchList.getColumn(1,"value");
        		
        		for(var i=0; i<this.ds_ultype.rowcount; i++){
        			var code = this.ds_ultype.getColumn(i,"field1");
        			var value = this.ds_ultype.getColumn(i,"field2");
        			if(this.ultypeCode == code){
        				this.ultype = value;
        				break;
        			}
        		}
        	
        		this.gfn_getScroll(this.gv_header1); // 스크롤 위치 저장
        		this.gv_header1.set_nodatatext("");
        		this.ds_column1.clearData();
        		this.ds_column2.clearData();
        		this.gfn_grdSortSet(this.gv_header1); // sort 조회 셋팅
        		this.gfn_setCommon("BEANID", "orderUploadController");
        		this.gfn_setCommon("METHODNM", "selectGridInfo");
        		
        		var sSvcId   = "selectUp";
        		var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "ds_column1=OUT_grdColList ds_column2=OUT_grdColList2 ds_paramUp=OUT_PARAM";
        		var sParam   =  "ctkey="+this.gfn_getUserInfo("ctKey");
        	}else{
        		this.tab_header.set_tabindex(1);
        		this.gfn_getScroll(this.gv_header2); // 스크롤 위치 저장
        		this.gv_header2.set_nodatatext("");
        		this.ds_header.clearData();
        		this.gfn_grdSortSet(this.gv_header2); // sort 조회 셋팅
        		this.gfn_setCommon("BEANID", "orderUploadController");
        		this.gfn_setCommon("METHODNM", "selectOrderUploadInfo");
        		
        		var sSvcId   = "select";
        		var sSvcUrl  = application.gv_oms + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        		var sParam   = "ctkey="+this.gfn_getUserInfo("ctKey");
        	}
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        
        this.fn_search = function(){
        }

        /* 조회 후 검색영역 숨기기 */
        this.fn_searchAfter = function()
        {
        	if(this.gv_searchValue == "L"){
        		this.btn_showHideW.set_cssclass("btn_WF_sl_close");
        		this.btn_showHideW.click();
        	}else{
        		this.btn_showHideH.set_cssclass("btn_WF_st_close");
        		this.btn_showHideH.click();
        	}
        }

        /* 검색영역 위치 변경 */
        this.fn_searchChange = function()
        {
        	var searchCnt = 0;
        	var srchCondDiv;
        	var srchCondDs;
        	
        	if(this.div_cond.gv_workTab == 0){
        		srchCondDs = this.div_cond.ds_srchCondition;
        		srchCondDiv = this.div_cond.div_seachCond;
        	}else if(this.div_cond.gv_workTab == 1){
        		srchCondDs = this.div_cond.ds_srchCondition2;
        		srchCondDiv = this.div_cond.div_seachCond2;
        	}
        	
        	searchCnt = srchCondDs.rowcount;
        	
        	var nHeight = 0;
        	var nSize = true;
        	var nCnt = 0;
        	var nGap = 30;
        	
        	var serchCondWidth = 0;
        	var condWidth = 403; // 검색 영역 넒이
        	var workWidth = 451; // 업무 영역 넒이
        	var topSpaceSizeL = nexacro.toNumber(this.btn_showHideW.top); // 검색영역 left 위쪽 여백
        	var topSpaceSizeW = nexacro.toNumber(topSpaceSizeL - nGap) + 14; // 검색영역 top 위쪽 여백
        	var spaceSize = this.viewSpace; // 화면 여백
        	var splitBarSize = nexacro.toNumber(this.btn_split_h.height); // 스플릿 바 사이즈
        	var buttonSpace = 13; // 조회 버튼 여백
        	var btnWidthPlus = 80;
            var nHcal = 0;
        	var ussc_dttype = "";
        	var divObj;
        	
            if(this.gv_searchValue == "L"){
        		this.gv_searchValue = "T";
        		
        		this.div_cond.sta_back.set_cssclass("sta_search_t");
        		this.div_cond.sta_guide.set_visible(false);
        		this.btn_split_v.set_visible(false);
        		this.btn_split_h.set_visible(true);
        		this.btn_showHideW.set_visible(false);
        		this.btn_showHideH.set_visible(true);
        		this.btn_showHideH.set_cssclass("btn_WF_st_close");

        		this.div_cond.btn_search.set_right(17);
        		this.div_cond.btn_search.set_bottom(this.div_cond.div_top2);
        		
        		this.div_cond.set_top(topSpaceSizeL - nGap);
        		this.div_cond.set_right(spaceSize);
        		this.div_work.set_left(spaceSize);
        		
        		srchCondDiv.set_width(this.div_cond.getOffsetWidth() - btnWidthPlus);
        		serchCondWidth = this.div_cond.getOffsetWidth() - btnWidthPlus;
        		
        		while(nSize){
        			if(serchCondWidth > (this.div_cond.div_firstLeft + this.div_cond.div_left2*(nCnt+1))){
        				nSize = true;
        				nCnt++;
        			}else{
        				nSize = false;
        			}
        		}
        		
        		var nRowCnt = parseInt(searchCnt/(nCnt+1));
        		var nRowPlus = searchCnt%(nCnt+1);
        		
        		if(searchCnt == 1){
        			nRowCnt = 1;
        			nRowPlus = 0;
        		}
        		
        		if(nRowPlus == 0){
        			nHeight = nRowCnt*this.div_cond.div_height + this.div_cond.div_top2*2 + (nRowCnt-1)*this.div_cond.div_space2;
        		}else{
        			nHeight = (nRowCnt+1)*this.div_cond.div_height + this.div_cond.div_top2*2 + nRowCnt*this.div_cond.div_space2;
        		}
        		
        		if(searchCnt == 1) nHeight = this.div_cond.div_height + this.div_cond.div_top2*2;
        		
        		this.btn_split_h.set_top(nHeight+topSpaceSizeL - nGap);
        		this.btn_showHideH.set_top(nHeight+topSpaceSizeL - nGap);
        		this.div_cond.set_height(nHeight);
        		this.div_work.set_top(nHeight+topSpaceSizeW+splitBarSize);
        		
        		var nLeft = 0;
        		
        		for(var i = 0 ; i < srchCondDs.rowcount ; i++){
        			ussc_dttype = srchCondDs.getColumn(i, "ussc_dttype");
        			divObj = eval("srchCondDiv." + this.div_cond.gv_condId + i);
        			
        			divObj.set_left(this.div_cond.div_left + ((this.div_cond.div_left2 - 10) * nLeft));
        			divObj.set_top(this.div_cond.div_top2 + nHcal*this.div_cond.div_space2 + nHcal*this.div_cond.div_height);
        			
        			if(nLeft == nCnt){
        				nHcal++;
        				nLeft = 0;
        			}else{
        				nLeft++;
        			}
        		}
        		
        		srchCondDiv.set_height(nHeight);
            }else{
        		this.gv_searchValue = "L";
        		
        		this.div_cond.sta_back.set_cssclass("sta_search_l");
        		this.div_cond.sta_guide.set_visible(true);
        		this.btn_split_v.set_visible(true);
        		this.btn_split_h.set_visible(false);
        		this.btn_showHideH.set_visible(false);
        		this.btn_showHideW.set_visible(true);
        		this.btn_showHideW.set_cssclass("btn_WF_sl_close");
        		
        		this.div_cond.btn_search.set_right(buttonSpace);
        		this.div_cond.btn_search.set_bottom(buttonSpace);
        		
        		this.btn_split_v.set_left(this.div_cond.div_left2);
        		this.btn_showHideW.set_left(this.div_cond.div_left2);
        		this.div_cond.set_top(topSpaceSizeL);
        		this.div_cond.set_width(condWidth);
        		this.div_cond.set_bottom(spaceSize);
        		this.div_work.set_left(workWidth);
        		this.div_work.set_top(topSpaceSizeL - nGap);
        		
        		for(var i = 0 ; i < srchCondDs.rowcount ; i++){
        			ussc_dttype = srchCondDs.getColumn(i, "ussc_dttype");
        			divObj = eval("srchCondDiv." + this.div_cond.gv_condId + i);
        			
        			divObj.set_left(this.div_cond.div_left);
        			divObj.set_top(this.div_cond.div_top + i*this.div_cond.div_space + i*this.div_cond.div_height);
        		}
        		
        		srchCondDiv.set_width(condWidth);
        		srchCondDiv.set_bottom(this.div_cond.div_bottom);
            }
        	
        	if(system.navigatorname == "nexacro"){
        		application.setPrivateProfile("searchSet", this.gv_searchValue);
        	}else{
        		window.localStorage.setItem("searchSet", this.gv_searchValue);
        	}
        }

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.WorkForm_onbeforeclose = function(obj,e)
        {
        	var nRow = 0;
        	var uCnt = 0;
        	var grdCnt = 0;
        	var uskey = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.gv_menuId), "uskey");
        	var grdObj;
        	
        // 	if(obj == e.fromobject){
        // 		var sOldInitString = ""; // 저장된 초기 데이타
        // 		var sOldFormatString = ""; // 그리드 초기 데이타
        // 		var sNewFormatString = ""; // 변경한 그리드 데이타
        // 		
        // 		if(this.gfn_isNull(application.gds_userGrid.getColumn("delYn"))) application.gds_userGrid.addColumn("delYn");
        // 		if(this.gfn_isNotNull(this.div_work.gv_grdList)) grdCnt = this.div_work.gv_grdList.length;
        // 		
        // 		for(var i = 0 ; i < grdCnt ; i++){
        // 			grdObj = this.div_work.gv_grdList[i];
        // 			sOldFormatString = this.div_work.ds_grdInit.getColumn(0, grdObj.name + "Init");
        // 			sNewFormatString = grdObj.getCurFormatString();
        // 			
        // 			nRow = application.gds_userGrid.findRowExpr("urKey == '"+this.gfn_getUserInfo("urKey")+"' && usKey == '"+uskey+"' && usgridId == '"+grdObj.name+"'");
        // 			if(nRow != -1) sOldInitString = application.gds_userGrid.getColumn(nRow, "urdfusInfoInit");
        // 			
        // 			if(nRow != -1){
        // 				if(sOldFormatString == sOldInitString){
        // 					if(sNewFormatString != application.gds_userGrid.getColumn(nRow, "urdfusInfo")){
        // 						application.gds_userGrid.setColumn(nRow, "urdfusInfo", grdObj.getCurFormatString());
        // 						uCnt++;
        // 					}
        // 				}else{
        // 					application.gds_userGrid.setColumn(nRow, "delYn", "Y");
        // 					uCnt++;
        // 				}
        // 			}else{
        // 				if(sOldFormatString != sNewFormatString){
        // 					nRow = application.gds_userGrid.addRow();
        // 					application.gds_userGrid.setColumn(nRow, "urKey", this.gfn_getUserInfo("urKey"));
        // 					application.gds_userGrid.setColumn(nRow, "usKey", uskey);
        // 					application.gds_userGrid.setColumn(nRow, "usgridId", grdObj.name);
        // 					application.gds_userGrid.setColumn(nRow, "urdfusInfo", grdObj.getCurFormatString());
        // 					application.gds_userGrid.setColumn(nRow, "urdfusInfoInit", this.div_work.ds_grdInit.getColumn(0, grdObj.name+"Init"));
        // 					
        // 					uCnt++;
        // 				}
        // 			}
        // 		}
        // 		
        // 		if(uCnt > 0) application.gv_TopFrame.form.fn_saveGrid();
        // 	}
        }

        this.WorkForm_onclose = function(obj,e)
        {
        	if(obj == e.fromobject){
        		var nRow = application.gds_openMenu.findRow("WINID", "M_" + this.gv_menuId);
        		
        		if(nRow != -1) application.gds_openMenu.deleteRow(nRow);
        		application.gv_TabFrame.form.fn_delTab(this.gv_menuId);
        		
        		var sMenuId = "";
        		
        		if(application.gds_openMenu.rowcount == 0){
        			application.gfn_setFrame("M");
        		}else if(application.gds_openMenu.rowcount == 1){
        			sMenuId = application.gds_openMenu.getColumn(0,"mekey");
        			this.gfn_showMenu(sMenuId);
        		}else{
        			sMenuId = application.gds_openMenu.getColumn(application.gds_openMenu.rowposition,"mekey");
        			this.gfn_showMenu(sMenuId);
        		}
        		
        		application.gv_TabFrame.form.fn_setActive(application.gv_TabFrame.form.ds_Tab.getColumn(application.gv_TabFrame.form.ds_Tab.findRow("WINID", sMenuId),"TAB_ID"));
        	}
        }

        this.WorkForm_ondrag = function(obj,e)
        {
        	if(e.fromreferenceobject == this.btn_split_h){
        		this.gv_split = "height";
        		return true;
        	}else if(e.fromreferenceobject == this.btn_split_v){
        		this.gv_split = "width";
        		return true;
        	}
        }

        this.WorkForm_ondragmove = function(obj,e)
        {
        	var spaceSize = this.viewSpace; // 화면 여백
        	var splitBarSize = nexacro.toNumber(this.btn_split_h.height); // 스플릿 바 사이즈
        	var workSpace = 14; // 검색조건 상단일 때 업무영역 여백
        	var topSpace = nexacro.toNumber(this.div_cond.top) + this.div_cond.div_height + this.div_cond.div_top2*2; // top limit
        	
        	var heightL = (this.getOffsetHeight() - (spaceSize + splitBarSize));
        	var heightW = (this.getOffsetWidth() - (spaceSize + splitBarSize));
        	
        	if(this.gv_split == "height"){
        		this.style.set_cursor("n-resize");
        		
        		if(e.clientY < topSpace){
        			this.btn_split_h.set_top(topSpace);
        			this.btn_showHideH.set_top(topSpace);
        			this.div_cond.set_height(this.btn_split_h.top - this.div_cond.top);
        			this.div_work.set_top(this.btn_split_h.top + splitBarSize + workSpace);
        			this.btn_showHideH.set_cssclass("btn_WF_st_open");
        		}else if(e.clientY > heightL){
        			this.btn_split_h.set_top(heightL);
        			this.btn_showHideH.set_top(heightL);
        			this.div_cond.set_height(this.btn_split_h.top - this.div_cond.top);
        			this.div_work.set_top(this.btn_split_h.top + splitBarSize + workSpace);
        			this.btn_showHideH.set_cssclass("btn_WF_st_close");
        		}else{
        			this.btn_split_h.set_top(e.clientY);
        			this.btn_showHideH.set_top(e.clientY);
        			this.div_cond.set_height(this.btn_split_h.top - this.div_cond.top);
        			this.div_work.set_top(this.btn_split_h.top + splitBarSize + workSpace);
        			this.btn_showHideH.set_cssclass("btn_WF_st_close");
        		}
        		
        		return;
        	}else if(this.gv_split == "width"){
        		this.style.set_cursor("e-resize");
        		
        		if(e.clientX < spaceSize){
        			this.btn_split_v.set_left(spaceSize);
        			this.btn_showHideW.set_left(spaceSize);
        			this.div_cond.set_width(this.btn_split_v.left - this.div_cond.left);
        			this.div_work.set_left(this.btn_split_v.left + splitBarSize + spaceSize);
        			this.btn_showHideW.set_cssclass("btn_WF_sl_open");
        		}else if(e.clientX > heightW){
        			this.btn_split_v.set_left(heightW);
        			this.btn_showHideW.set_left(heightW);
        			this.div_cond.set_width(this.btn_split_v.left - this.div_cond.left);
        			this.div_work.set_left(this.btn_split_v.left + splitBarSize + spaceSize);
        			this.btn_showHideW.set_cssclass("btn_WF_sl_close");
        		}else{
        			this.btn_split_v.set_left(e.clientX);
        			this.btn_showHideW.set_left(e.clientX);
        			this.div_cond.set_width(this.btn_split_v.left - this.div_cond.left);
        			this.div_work.set_left(this.btn_split_v.left + splitBarSize + spaceSize);
        			this.btn_showHideW.set_cssclass("btn_WF_sl_close");
        		}
        		
        		return;
        	}else{
        		return;
        	}
        }

        this.WorkForm_ondrop = function(obj,e)
        {
        	this.gv_split = "";
        	this.style.set_cursor("");
        }

        this.btn_changeSearch_onclick = function(obj,e)
        {
        	this.fn_searchChange();
        }

        this.btn_showHideH_onclick = function(obj,e)
        {
        	var topSpace = nexacro.toNumber(this.div_cond.top) + this.div_cond.div_height + this.div_cond.div_top2*2; // top limit
        	var splitBarSize = nexacro.toNumber(this.btn_split_h.height); // 스플릿 바 사이즈
        	var workSpace = 14; // 검색조건 상단일 때 업무영역 여백
        	
        	if(this.btn_showHideH.cssclass == "btn_WF_st_open"){
        		this.btn_showHideH.set_cssclass("btn_WF_st_close");
        		
        		this.gv_searchValue = "L";
        		this.fn_searchChange();
        	}else{
        		this.btn_showHideH.set_cssclass("btn_WF_st_open");
        		
        		this.btn_split_h.set_top(topSpace);
        		this.btn_showHideH.set_top(topSpace);
        		this.div_cond.set_height(nexacro.toNumber(this.btn_split_h.top) - nexacro.toNumber(this.div_cond.top));
        		this.div_work.set_top(nexacro.toNumber(this.btn_split_h.top) + splitBarSize + workSpace);
        	}
        	
        	this.sta_title.setFocus();
        }

        this.btn_showHideW_onclick = function(obj,e)
        {
        	var spaceSize = this.viewSpace; // 화면 여백
        	var splitBarSize = nexacro.toNumber(this.btn_split_h.height); // 스플릿 바 사이즈
        	
        	if(this.btn_showHideW.cssclass == "btn_WF_sl_open"){
        		this.btn_showHideW.set_cssclass("btn_WF_sl_close");
        		
        		this.gv_searchValue = "T";
        		this.fn_searchChange();
        	}else{
        		this.btn_showHideW.set_cssclass("btn_WF_sl_open");
        		
        		this.btn_split_v.set_left(spaceSize);
        		this.btn_showHideW.set_left(spaceSize);
        		this.div_cond.set_width(nexacro.toNumber(this.btn_split_v.left) - nexacro.toNumber(this.div_cond.left));
        		this.div_work.set_left(nexacro.toNumber(this.btn_split_v.left) + splitBarSize + spaceSize);
        	}
        }

        this.WorkForm_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        this.WorkForm_onsize = function(obj,e)
        {
        	var searchCnt = 0;
        	var srchCondDiv;
        	var srchCondDs;
        	
        	if(this.div_cond.gv_workTab == 0){
        		srchCondDs = this.div_cond.ds_srchCondition;
        		srchCondDiv = this.div_cond.div_seachCond;
        	}else if(this.div_cond.gv_workTab == 1){
        		srchCondDs = this.div_cond.ds_srchCondition2;
        		srchCondDiv = this.div_cond.div_seachCond2;
        	}
        	
        	searchCnt = srchCondDs.rowcount;
        	
        	var nHeight = 0;
        	var nSize = true;
        	var nCnt = 0;
        	var nRowCnt = 0;
        	var nRowPlus = 0;
        	
        	var showHideHTop = 0; // 버튼 top
        	var showHideHheight = 0; // 버튼 높이 + 여백
        	var splitBarSize = nexacro.toNumber(this.btn_split_h.height); // 스플릿 바 사이즈
        	var topSpaceSizeL = nexacro.toNumber(this.div_cond.top); // 검색영역 left 위쪽 여백
        	var btnWidthPlus = 80;
            var nHcal = 0;
        	var ussc_dttype = "";
        	var divObj;
        	
        	if(this.gv_searchValue == "T"){
        		if(this.btn_showHideH.cssclass == "btn_WF_st_close"){
        			srchCondDiv.set_width(this.div_cond.getOffsetWidth() - btnWidthPlus);
        			serchCondWidth = this.div_cond.getOffsetWidth() - btnWidthPlus;
        			
        			while(nSize){
        				if(serchCondWidth > (this.div_cond.div_firstLeft + this.div_cond.div_left2*(nCnt+1))){
        					nSize = true;
        					nCnt++;
        				}else{
        					nSize = false;
        				}
        			}
        			
        			nRowCnt = parseInt(searchCnt/(nCnt+1));
        			nRowPlus = searchCnt%(nCnt+1);
        			
        			if(searchCnt == 1){
        				nRowCnt = 1;
        				nRowPlus = 0;
        			}
        			
        			if(nRowPlus == 0){
        				nHeight = nRowCnt*this.div_cond.div_height + this.div_cond.div_top2*2 + (nRowCnt-1)*this.div_cond.div_space2;
        			}else{
        				nHeight = (nRowCnt+1)*this.div_cond.div_height + this.div_cond.div_top2*2 + nRowCnt*this.div_cond.div_space2;
        			}
        			
        			if(searchCnt == 1) nHeight = this.div_cond.div_height + this.div_cond.div_top2*2;
        			
        			this.btn_split_h.set_top(nHeight+topSpaceSizeL);
        			this.btn_showHideH.set_top(nHeight+topSpaceSizeL);
        			this.div_cond.set_height(nHeight);
        			showHideHTop = nexacro.toNumber(this.btn_showHideH.top); // 버튼 top
        			showHideHheight = nexacro.toNumber(this.btn_showHideH.height) + 10; // 버튼 높이 + 여백
        			this.div_work.set_top(showHideHTop+showHideHheight);
        			
        			var nLeft = 0;
        			
        			for(var i = 0 ; i < srchCondDs.rowcount ; i++){
        				ussc_dttype = srchCondDs.getColumn(i, "ussc_dttype");
        				divObj = eval("srchCondDiv." + this.div_cond.gv_condId + i);
        				
        				divObj.set_left(this.div_cond.div_left + ((this.div_cond.div_left2 - 10) * nLeft));
        				divObj.set_top(this.div_cond.div_top2 + nHcal*this.div_cond.div_space2 + nHcal*this.div_cond.div_height);
        				
        				if(nLeft == nCnt){
        					nHcal++;
        					nLeft = 0;
        				}else{
        					nLeft++;
        				}
        			}
        			
        			srchCondDiv.set_height(nHeight);
        		}else{
        			srchCondDiv.set_width(this.div_cond.getOffsetWidth() - btnWidthPlus);
        			serchCondWidth = this.div_cond.getOffsetWidth() - btnWidthPlus;
        			
        			while(nSize){
        				if(serchCondWidth > (this.div_cond.div_firstLeft + this.div_cond.div_left2*(nCnt+1))){
        					nSize = true;
        					nCnt++;
        				}else{
        					nSize = false;
        				}
        			}
        			
        			nRowCnt = parseInt(searchCnt/(nCnt+1));
        			nRowPlus = searchCnt%(nCnt+1);
        			
        			if(searchCnt == 1){
        				nRowCnt = 1;
        				nRowPlus = 0;
        			}
        			
        			if(nRowPlus == 0){
        				nHeight = nRowCnt*this.div_cond.div_height + this.div_cond.div_top2*2 + (nRowCnt-1)*this.div_cond.div_space2;
        			}else{
        				nHeight = (nRowCnt+1)*this.div_cond.div_height + this.div_cond.div_top2*2 + nRowCnt*this.div_cond.div_space2;
        			}
        			
        			if(searchCnt == 1) nHeight = this.div_cond.div_height + this.div_cond.div_top2*2;
        			
        			showHideHTop = nexacro.toNumber(this.btn_showHideH.top); // 버튼 top
        			showHideHheight = nexacro.toNumber(this.btn_showHideH.height) + 10; // 버튼 높이 + 여백
        			this.div_work.set_top(showHideHTop+showHideHheight);
        			
        			var nLeft = 0;
        			
        			for(var i = 0 ; i < srchCondDs.rowcount ; i++){
        				ussc_dttype = srchCondDs.getColumn(i, "ussc_dttype");
        				divObj = eval("srchCondDiv." + this.div_cond.gv_condId + i);
        				
        				divObj.set_left(this.div_cond.div_left + ((this.div_cond.div_left2 - 10) * nLeft));
        				divObj.set_top(this.div_cond.div_top2 + nHcal*this.div_cond.div_space2 + nHcal*this.div_cond.div_height);
        				
        				if(nLeft == nCnt){
        					nHcal++;
        					nLeft = 0;
        				}else{
        					nLeft++;
        				}
        			}
        			
        			srchCondDiv.set_height(nHeight);
        		}
        	}
        }

        this.btn_help_onclick = function(obj,e)
        {
        	var oArg = { menuId:this.gv_menuId
        	           };
        	this.gfn_popup("SystemInfoPop", "comm::SystemInfoPop.xfdl", oArg, 580, 248, "");
        }

        this.btn_bookmark_onclick = function(obj,e)
        {
        	this.gfn_alert("개발 중인 기능 입니다.");
        	return;		
        }

        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        this.sta_title_onclick = function(obj,e)
        {
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onbeforeclose", this.WorkForm_onbeforeclose, this);
            this.addEventHandler("onclose", this.WorkForm_onclose, this);
            this.addEventHandler("ondrag", this.WorkForm_ondrag, this);
            this.addEventHandler("ondragmove", this.WorkForm_ondragmove, this);
            this.addEventHandler("ondrop", this.WorkForm_ondrop, this);
            this.addEventHandler("onkeydown", this.WorkForm_onkeydown, this);
            this.addEventHandler("onsize", this.WorkForm_onsize, this);
            this.sta_title.addEventHandler("onclick", this.sta_title_onclick, this);
            this.btn_showHideH.addEventHandler("onclick", this.btn_showHideH_onclick, this);
            this.fdw_excelFile.addEventHandler("onclick", this.fdw_excelFile_onclick, this);
            this.btn_bookmark.addEventHandler("onclick", this.btn_bookmark_onclick, this);
            this.btn_help.addEventHandler("onclick", this.btn_help_onclick, this);
            this.btn_changeSearch.addEventHandler("onclick", this.btn_changeSearch_onclick, this);
            this.btn_showHideW.addEventHandler("onclick", this.btn_showHideW_onclick, this);
            this.div_work.tab_header.addEventHandler("onchanged", this.tab_header_onchanged, this);
            this.div_work.tab_header.tabpage1.div_splitTop.btn_save.addEventHandler("onclick", this.tab_header_tabpage1_div_splitTop_btn_save_onclick, this);
            this.div_work.tab_header.tabpage1.div_splitTop.btn_excelUp.addEventHandler("onclick", this.tab_header_tabpage1_div_splitTop_btn_excelUp_onclick, this);
            this.div_work.tab_header.tabpage1.div_splitTop.btn_sampleDown.addEventHandler("onclick", this.tab_header_tabpage1_div_splitTop_btn_sampleDown_onclick, this);
            this.div_work.tab_header.tabpage2.div_splitTop.btn_excel.addEventHandler("onclick", this.tab_header_tabpage2_div_splitTop_btn_excel_onclick, this);
            this.div_work.tab_header.tabpage2.div_splitTop.grd_header.addEventHandler("onheadclick", this.tab_header_tabpage2_div_splitTop_grd_header_onheadclick, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("ExcelUploadPop.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
