<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	String address = request.getParameter("address");
%>
<html>
    <head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <title>TmapRoute</title>
		<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=uLNt_F9MOcMJkzdYaeQ9&submodules=geocoder">
		</script>
		<script type="text/javascript">
			
			var address = "<%=address%>";
			var map = new Map();
			
			//위도,경도 및 보정된 주소 가져오기.
			function searchAddressToCoordinate(address) {
				naver.maps.Service.geocode({
					address: address
				}, function(status, response) {
					if (status === naver.maps.Service.Status.ERROR) {
						return alert('잘못된 주소 정보 입니다. 주소를 확인 해주세요.');
					}

					var item = response.result.items[0],
						addrType = item.isRoadAddress ? '[도로명 주소]' : '[지번 주소]',
						point = new naver.maps.Point(item.point.x, item.point.y);

					map.put("lon",point.x);
					map.put("lat",point.y);
					map.put("newaddress",item.address);
										
					callNexacro(map);					
				});
			}
			
			//초기화.
			function initGeocoder() {
					searchAddressToCoordinate(address);
			}
			
			naver.maps.onJSContentLoaded = initGeocoder;
			
			//맵 객체 만들기.
			function Map() {

				 this.elements = {};
				 this.length = 0;

			}
			
			Map.prototype.put = function(key,value) {

			 this.length++;
			 this.elements[key] = value;

			}
			
			Map.prototype.get = function(key) {

			 return this.elements[key];

			}
			
			//넥사크로로 결과값 객체 리턴.
			function callNexacro(userdata)
			{
				var wb = window.NEXACROWEBBROWSER;

				if ( wb )
				{
					// HTML
					wb.on_fire_onusernotify(wb, userdata);
				}
				else
				{
					// Runtime
					window.document.title = userdata;
				}
			}			
			
		</script>					
    </head>
</html>
