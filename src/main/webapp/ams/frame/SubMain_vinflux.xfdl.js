﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SubMain");
                this.set_classname("SubMain");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new ImageViewer("img_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("SubMain");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("SubMain_vinflux.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SubMain.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : SubMain 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        this.form_onload = function(obj,e)
        {
        	var utl = "";
        	
        	if(application.gv_activeApp == "AMS") utl = "url('img::main_ams_img.png')";
        	else if(application.gv_activeApp == "WMS") utl = "url('img::main_wms_img.png')";
        	else if(application.gv_activeApp == "TMS") utl = "url('img::main_tms_img.png')";
        	else if(application.gv_activeApp == "VIMS") utl = "url('img::main_vms_img.png')";
        	else if(application.gv_activeApp == "ICOM")  utl = "url('img::main_oms_img.png')";
        	else if(application.gv_activeApp == "PMS") utl = "url('img::main_pms_img.png')";
        	
        	this.img_back.set_image(utl);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);

        };

        this.loadIncludeScript("SubMain_vinflux.xfdl", true);

       
    };
}
)();
