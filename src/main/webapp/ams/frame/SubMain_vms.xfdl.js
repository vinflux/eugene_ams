﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("InboundDailyReport");
                this.set_classname("frmDivChart");
                this.set_titletext("입고 시간별 현황");
                this._setFormPosition(0,0,1230,627);
            }
            this.style.set_color("#33333333");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"rnum\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"cfulldate\" type=\"STRING\" size=\"256\"/><Column id=\"cfullhour\" type=\"STRING\" size=\"256\"/><Column id=\"cfulldate_c\" type=\"STRING\" size=\"256\"/><Column id=\"ib_exqty\" type=\"STRING\" size=\"256\"/><Column id=\"ib_compqty\" type=\"STRING\" size=\"256\"/><Column id=\"ib_miasnqty\" type=\"STRING\" size=\"256\"/><Column id=\"ib_rtnqty\" type=\"STRING\" size=\"256\"/><Column id=\"ib_cnlqty\" type=\"STRING\" size=\"256\"/><Column id=\"ib_comprate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_piechart", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_linechart", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_center", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("1");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("2");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_ibchart1", "absolute", "10", "33", "320", null, null, "10", this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("piechart");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_ibchart2", "absolute", "340", "33", null, null, "10", "10", this.div_splitTop);
            obj.set_taborder("20");
            obj.set_text("linechart");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("3");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "-1", "28", null, null, "0", "30", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"45\" band=\"left\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"60\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"normal\" text=\"NO\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"CTNAME\"/><Cell col=\"3\" text=\"CFULLDATE\"/><Cell col=\"4\" text=\"HOUR\"/><Cell col=\"5\" text=\"IB_EXQTY\"/><Cell col=\"6\" text=\"IB_COMPQTY\"/><Cell col=\"7\" text=\"IB_MIASNQTY\"/><Cell col=\"8\" text=\"IB_CNLQTY\"/><Cell col=\"9\" text=\"WORKINGQTY\"/><Cell col=\"10\" text=\"IB_COMPRATE\"/></Band><Band id=\"body\"><Cell displaytype=\"normal\" edittype=\"none\" style=\"padding: ;\" text=\"bind:rnum\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" displaytype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ctkey\" combodataset=\"ds_center\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"3\" displaytype=\"date\" style=\"align:center;padding: ;\" text=\"bind:cfulldate\" mask=\"yyyy-MM-dd\"/><Cell col=\"4\" displaytype=\"normal\" style=\"align:center;padding: ;\" text=\"bind:cfullhour\"/><Cell col=\"5\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:ib_exqty\"/><Cell col=\"6\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:ib_compqty\"/><Cell col=\"7\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:ib_miasnqty\"/><Cell col=\"8\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:ib_cnlqty\"/><Cell col=\"9\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:ib_rtnqty\"/><Cell col=\"10\" displaytype=\"number\" text=\"bind:ib_comprate\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Div("div_RowCount", "absolute", "0", null, null, "29", "0", "0", this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_scrollbars("none");
            obj.set_url("comm::RowCount.xfdl");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("3");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frmDivChart");
            		p.set_titletext("입고 시간별 현황");
            		p.style.set_color("#33333333");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::RowCount.xfdl");
        };
        
        // User Script
        this.addIncludeScript("SubMain_vms.xfdl", "lib::Comm.xjs");
        this.addIncludeScript("SubMain_vms.xfdl", "utils::chartUtil.xjs");
        this.registerScript("SubMain_vms.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : InboundDailyReport.xfdl
        * PROGRAMMER  : HCKIM
        * DATE        : 2018.09.20
        * DESCRIPTION : InboundDailyReport
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("utils::chartUtil.xjs", null, exports); }	//include "utils::chartUtil.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_menuId = "";
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.grdList;

        this.detail = this.div_splitBottom.grd_detail;

        var status = "";

        var elem = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정

        	//그리드초기설정세팅
        	this.grdList = [this.detail]; // 그리드 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	var keyField = "";
        //	var page = [this.gv_page];
        	var page = "";
        	this.fn_dsComboInit();
        	this.gfn_gridInit(this.grdList, "", page, searchFunc, keyField);
        	
        	this.parent.div_cond.btn_search.click();
        //	this.fn_search();
        	
        //	this.parent.fn_searchAfter();
        }

        this.fn_dsComboInit = function()
        {
        	this.gfn_getCode("CTKEY", this.ds_center);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	status = "loaded";
        	
        	// dataset 데이터 삭제
        	this.ds_header.clearData();
        	
        	// Controller명
        	this.gfn_setCommon("BEANID", "inboundDailyReportController");
        	// method명
        	this.gfn_setCommon("METHODNM", "selectInboundDailyReport");
        	
        	// sSvcId는 보통 callback 함수에서 이후 처리하기위해 사용.
        	var sSvcId = "selectInboundDailyReport";
        	// application의 GlobalVariables에 설정된 해당 값으로 설정.
        	var sSvcUrl = application.gv_vms + application.gv_sUrl;
        	// 조회 시 서버로 보낼 dataset 셋팅( 서버=화면 ex)IN_?????=ds_???? )
        	var sInData = "";
        	// 리턴 시 서버에서 받을 dataset 셋팅( 화면=서버 ex) ds_????=OUT_????? )
        	var sOutData = "ds_header=OUT_rtnGrid"
        				 + " ds_param=OUT_PARAM";
        	// 조회 시 필요한 파라미터 name=value
        	// parameter가 많을 경우 빈문자열로 구분"name1=value1 name2=value2 ..."
        	var sParam = "status=" + status;
        	
        	// 트랜젝션(sSvcId, sSvcUrl, sInData, sOutData, sParam, 콜백함수)
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	
        }

        // piechart Data
        this.fn_searchPieChartData = function(ctkey,yyyymmdd,hh)
        {
        	// dataset 데이터 삭제
        	this.ds_piechart.clearData();
        	
        	// Controller명
        	this.gfn_setCommon("BEANID", "inboundDailyReportController");
        	// method명
        	this.gfn_setCommon("METHODNM", "selectPieChartData");
        	
        	// sSvcId는 보통 callback 함수에서 이후 처리하기위해 사용.
        	var sSvcId = "selectPieChartData";
        	// application의 GlobalVariables에 설정된 해당 값으로 설정.
        	var sSvcUrl = application.gv_vms + application.gv_sUrl;
        	// 조회 시 서버로 보낼 dataset 셋팅( 서버=화면 ex)IN_?????=ds_???? )
        	var sInData = "";
        	// 리턴 시 서버에서 받을 dataset 셋팅( 화면=서버 ex) ds_????=OUT_????? )
        	var sOutData = "ds_piechart=OUT_rtnGrid";
        	// 조회 시 필요한 파라미터 name=value
        	// parameter가 많을 경우 빈문자열로 구분"name1=value1 name2=value2 ..."
        	var sParam = "CTKEY=" + ctkey + " YYYYMMDD=" + yyyymmdd + " HH=" + hh;
        	
        	// 트랜젝션(sSvcId, sSvcUrl, sInData, sOutData, sParam, 콜백함수)
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        // 콜백 function
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        // 	if(nErrCd != 0) {
        // 		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        // 		return;
        // 	}
        	
        	if(sSvcId == "selectInboundDailyReport") {
        		this.gfn_constDsSet(this.detail);
        		
        		if(this.ds_header.rowcount > 0){
        			// 검색조건 숨김
        			this.parent.fn_searchAfter();
        		}else{
        			// dataset 데이터 삭제
        			this.ds_piechart.clearData();
        			document.getElementById("dib_piechart").hasNoData(false);

        			// 조회된 데이터가 없을 때 화면에 보여줄 메세지
        			this.gfn_alert("MSG_NO_DATA");
        		}

        		// RowCount처리
        		this.div_splitBottom.div_RowCount.fn_setRowCount(this.ds_header.rowcount);
        		
        		this.fn_initLineChart();
        	} else if(sSvcId == "selectPieChartData") {
        		this.fn_initPieChart();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        // 그리드 rowselectchanged 이벤트
        this.div_splitBottom_grd_detail_onselectchanged = function(obj,e)
        {
        	// 그리드 변경시 cell index 수정
        	var ctkey = obj.getCellValue(e.row, 1);
        	var yyyymmdd = obj.getCellValue(e.row, 3);
        	var hh = obj.getCellValue(e.row, 4);

        	this.fn_searchPieChartData(ctkey, yyyymmdd, hh);
        }

        // piechart 초기화
        this.fn_initPieChart = function()
        {
        	this.ds_piechart.addColumn("data_type_nm", "STRING");
        	
        	var row = this.ds_piechart.rowcount;
        	
        	for(var i=0; i<row; i++) {
        		var type = this.ds_piechart.getColumn(i, "data_type");
        		var str = "";
        		
        		switch(type) {
        			case "IB_COMPQTY" :
        				str = "입고 완료수량";
        				break;
        			case "IB_MIASNQTY" :
        				str = "입고 미처리수량";
        				break;
        			case "IB_CNLQTY" :
        				str = "입고 취소수량";
        				break;
        			default :
        				str = "";
        				break;
        		}
        		
        		this.ds_piechart.setColumn(i, "data_type_nm", str);
        	}

        	elem = "";
        	
        	elem += '<rMateChart backgroundColor="#FFFFFF"  borderStyle="none">'
        		+ '<Options>'
        			+ '<Caption text="시간별 입고실적"/>'
        			+ '<Legend useVisibleCheck="true"/>'
        		+ '</Options>'
        		+ '<Pie2DChart showDataTips="true">'
        			+ '<series>'
        				+ '<Pie2DSeries nameField="data_type_nm" field="data_cnt" perWedgeExplodeRadius="[0,0,0,0]" labelPosition="inside" color="#ffffff" renderDirection="counterClockwise" startAngle="90">'
        					+ '<showDataEffect>'
        						+ '<SeriesInterpolate/>'
        					+ '</showDataEffect>'
        				+ '</Pie2DSeries>'
        			+ '</series>'
        		+ '</Pie2DChart>'
        	+ '</rMateChart>';
        	
        	window.gv_layout_str = elem;
        	
        	eval("window.gv_chart_data=" + this.gfn_makeJsonString(this.ds_piechart,null) + ";");
        	
        	var chartVars = "&rMateOnLoadCallFunction=window.gfn_chartReadyHandler";
        	
        	rMateChartH5.create("dib_piechart", this.div_splitTop.div_ibchart1._unique_id, chartVars, "100%", "100%");
        }

        // linechart 초기화
        this.fn_initLineChart = function()
        {
        	this.ds_linechart.copyData(this.ds_header);

        	elem = "";
        	
        	
        	elem += '<rMateChart backgroundColor="#ffffff" borderStyle="none">'
        		+ '<Options>'
        			+ '<Caption text="일별 실적" />'
        //			+ '<SubCaption text="World Bank Commodity Price Data in 2013" textAlign="center" />'
        			+ '<Legend useVisibleCheck="true"/>'
        		+ '</Options>'
        		+ '<DateFormatter id="dateFmt" formatString="YYYY년 MM월 DD일 HH시"/>'
        		+ '<NumberFormatter id="numFmt" precision="0"/>'
        		+ '<Line2DChart showDataTips="true" dataTipDisplayMode="axis" paddingTop="0">'
        			+ '<horizontalAxis>'
        				+ '<DateTimeAxis id="hAxis" dataUnits="hours" labelUnits="hours" interval="3" formatter="{dateFmt}" alignLabelsToUnits="false" displayLocalTime="true"/>'
        //				+ '<CategoryAxis categoryField="cfulldate_c" padding="0.2"/>'
        			+ '</horizontalAxis>'
        			+ '<verticalAxis>'
        				+ '<LinearAxis minimum="0"/>'
        			+ '</verticalAxis>'
        			+ '<series>'
        				+ '<Line2DSeries xField="cfulldate_c" yField="ib_compqty" fill="#ffffff" radius="5" displayName="입고완료" itemRenderer="RectangleItemRenderer">'
        //				+ '<Line2DSeries yField="ib_compqty" fill="#ffffff" radius="5" displayName="입고완료" itemRenderer="RectangleItemRenderer">'
        					+ '<showDataEffect>'
        						+ '<SeriesInterpolate/>'
        					+ '</showDataEffect>'
        				+ '</Line2DSeries>'
        				+ '<Line2DSeries xField="cfulldate_c" yField="ib_miasnqty" fill="#ffffff" radius="5" displayName="입고미처리" itemRenderer="CircleItemRenderer">'
        //				+ '<Line2DSeries labelPosition="up" yField="ib_miasnqty" fill="#ffffff" radius="5" displayName="입고미처리" itemRenderer="CircleItemRenderer">'
        					+ '<showDataEffect>'
        						+ '<SeriesInterpolate/>'
        					+ '</showDataEffect>'
        				+ '</Line2DSeries>'
        				+ '<Line2DSeries xField="cfulldate_c" yField="ib_cnlqty" fill="#ffffff" radius="6" displayName="입고취소" itemRenderer="TriangleItemRenderer">'
        //				+ '<Line2DSeries yField="ib_cnlqty" fill="#ffffff" radius="6" displayName="입고취소" itemRenderer="TriangleItemRenderer">'
        					+ '<showDataEffect>'
        						+ '<SeriesInterpolate/>'
        					+ '</showDataEffect>'
        				+ '</Line2DSeries>'
        			+ '</series>'
        			+ '<annotationElements>'
        				+ '<CrossRangeZoomer zoomType="horizontal" fontSize="11" color="#FFFFFF" horizontalLabelFormatter="{numFmt}" verticalLabelFormatter="{dateFmt}" verticalLabelPlacement="bottom" horizontalLabelPlacement="left" enableZooming="false" enableCrossHair="true"/>'
        			+ '</annotationElements>'
        		+ '</Line2DChart>'
        	+ '</rMateChart>';
        	
        	
        	window.gv_layout_str = elem;
        	
        	eval("window.gv_chart_data=" + this.gfn_makeJsonString(this.ds_linechart,null) + ";");
        	
        	var chartVars = "&rMateOnLoadCallFunction=window.gfn_chartReadyHandler";

        	rMateChartH5.create("dib_linechart", this.div_splitTop.div_ibchart2._unique_id, chartVars, "100%", "100%");
        }
        this.InboundDailyReport_onsetfocus = function(obj,e)
        {
        // 	this.cancelTransaction();
        // 	this.fn_search();

        	this.fn_initLineChart();
        	this.fn_initPieChart();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsetfocus", this.InboundDailyReport_onsetfocus, this);
            this.div_splitBottom.addEventHandler("onsize", this.div_splitBottom_onsize, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onselectchanged", this.div_splitBottom_grd_detail_onselectchanged, this);

        };

        this.loadIncludeScript("SubMain_vms.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
