﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("TabForm");
                this.set_classname("TopFrame");
                this.set_dragscrolltype("both");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,1264,28);
            }
            this.style.set_background("#ecedf1ff");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_Tab", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"TAB_ID\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE_KO\" type=\"STRING\" size=\"256\"/><Column id=\"WINID\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", "100%", "28", null, null, this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_TBF_TabMenuBg");
            this.addChild(obj.name, obj);

            obj = new Div("div_Tab", "absolute", "80", "0", null, "28", "76", null, this);
            obj.set_taborder("1");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "1", "21", "26", "0", null, this);
            obj.set_taborder("2");
            obj.set_cssclass("btn_TaF_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_PreMdi", "absolute", null, "1", "21", "26", "42", null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_TaF_prev");
            this.addChild(obj.name, obj);

            obj = new Button("btn_NexMdi", "absolute", null, "1", "21", "26", "21", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_TaF_next");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer00", "absolute", "29", "6", "37", "18", null, null, this);
            obj.set_taborder("5");
            obj.set_image("URL('img::ico_page.png')");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #e5e5e5ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1264, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("TopFrame");
            		p.set_dragscrolltype("both");
            		p.set_scrollbars("none");
            		p.style.set_background("#ecedf1ff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Tab.xfdl", "lib::Comm.xjs");
        this.registerScript("Tab.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Tab.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 탭 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.FIRST_GAP = 1;
        this.BTN_GAP = -1;
        this.TAB_HEIGHT = 28;
        this.TAB_EXTRA_RIGHT_GAP = 30;
        this.EXTRA_WIDTH = 17;
        this.EXTRA_TOP = 6;
        this.EXTRA_HEIGHT = 17;
        this.EXTRA_BTN_PREFIX = "EXTRA_";
        this.TAB_BTN_PREFIX = "TAB_";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/

        /***********************************************************************************
        * user function
        ***********************************************************************************/
         /**
         * 메뉴 tab 삭제
         * @param {string} 메뉴key값
         * @param
         * @return
         * @example
         * @memberOf 
         */
        this.fn_delTab = function (winID)
        {
            var nRow = this.ds_Tab.findRow("WINID", winID);
            if (nRow == -1) return false;

            var tabID = this.TAB_BTN_PREFIX + winID;
            // Removing Tab button.
            this.fn_delTabBtn(tabID);
            this.ds_Tab.deleteRow(nRow);
            this.fn_setTabSpinBtnShow();
            
            if(this.ds_Tab.rowcount == 0) this.btn_closeAll.set_visible(false);
            
            return true;
        }

        /**
         * 메뉴 tab Button 동적생성
         * @public
         * @param {string} 메뉴키값
         * @param {string} 메뉴명
         * @return
         * @example
         * @memberOf 
         */
         
        this.fn_addTab = function (winID,name)
        {
            var nRow = this.ds_Tab.findRow("WINID", winID);
            
            if (nRow > -1) return nRow;
            
            var tabID = this.TAB_BTN_PREFIX + winID;
                nRow = this.ds_Tab.addRow();
                this.ds_Tab.setColumn(nRow, "TAB_ID", tabID);
                this.ds_Tab.setColumn(nRow, "WINID", winID);
                this.ds_Tab.setColumn(nRow, "TITLE", name);
            
            this.fn_addTabBtn(tabID, name, winID);
            this.fn_setTabSpinBtnShow();
            this.btn_closeAll.set_visible(true);
            return nRow;
        }

        /**
         * 메뉴 tab 신규 동적 생성
         * @public
         * @param {string} 메뉴키값
         * @param {string} 메뉴명
         * @return
         * @example
         * @memberOf 
         */
        this.fn_addTabBtn = function (tabID,tabName,winID)
        {
            var tabObj;
            var BtnObj;
            var exBtnId = this.EXTRA_BTN_PREFIX + tabID;   //extra button id
            var objTextWidth =  "";

            // Tab 버튼 생성
            if (this.fn_findObj(tabID) == null){
                tabObj = new Button();
                tabObj.init(tabID, "absolute", this.fn_getLeft(tabID), 0, 0, this.TAB_HEIGHT, null, null);
                this.div_Tab.addChild(tabObj.name, tabObj);
            }
            
            tabObj.style.set_align("left middle");
        //    tabObj.style.set_padding_left("15"); // 여백설정
            tabObj.set_tooltiptext(tabName);
            tabObj.setEventHandler("onclick", this.btn_Tab_OnClick, this);
            tabObj.setEventHandler("onlbuttonup", this.btn_Tab_OnLButtonUp, this);
            tabObj.set_visible(true);
            tabObj.show();
            var nCompWidth = this.fn_getWidth(tabObj, tabName)+30; // 30:Extra-Button
            
            // 탭버튼명에 말줄임 처리
            if(nCompWidth > 150){
                if(this.gfn_getUserInfo("laKey") == "KOR") tabObj.set_text( this.gfn_left(tabName, 9)+" . . ." );
                else tabObj.set_text( this.gfn_left(tabName, 15)+" . . ." );
            }else{
                tabObj.set_text(tabName);
            }
            
            nCompWidth = 150;
            
            tabObj.set_cssclass("btn_TaF_tab_on");
            tabObj.set_width(nCompWidth+this.TAB_EXTRA_RIGHT_GAP);
            
            // 닫기("x") 버튼 생성
            if (this.fn_findObj(exBtnId) == null && tabName != "MAIN" ){
                BtnObj = new Button();        
                BtnObj.init(exBtnId, "absolute", tabObj.getOffsetRight()-this.TAB_EXTRA_RIGHT_GAP, this.EXTRA_TOP, tabObj.getOffsetRight() - this.TAB_EXTRA_RIGHT_GAP + this.EXTRA_WIDTH - (tabObj.getOffsetRight() - this.TAB_EXTRA_RIGHT_GAP), this.EXTRA_TOP + this.EXTRA_HEIGHT - this.EXTRA_TOP);
                
                this.div_Tab.addChild(exBtnId, BtnObj);
            
                BtnObj.style.set_cursor("hand");
                BtnObj.setEventHandler("onclick", this.btn_Extra_OnClick, this);
                BtnObj.set_visible(true);
                BtnObj.show();    
                BtnObj.set_cssclass("btn_TaF_tab_close");
            }
            
            if(this.div_Tab.getOffsetWidth() < tabObj.getOffsetRight()){
                this.btn_NexMdi.click();
            }
            
            this.isActiveFrame(winID);
        }
          
        /**
         * 메뉴 tab 이동
         * @public
         * @param {string} 메뉴키값
         * @return
         * @example
         * @memberOf 
         */
        this.fn_moveTab = function (winID)
        {
            var nRow = this.ds_Tab.findRow("WINID", winID);
            if (nRow < 0) return nRow;
            
            var tabID = this.TAB_BTN_PREFIX + winID;    
            this.fn_setActive(tabID);
            this.fn_setTabSpinBtnShow();
        }

        /**
         * 메뉴 tab 변경 처리 함수
         * @public
         * @param {string}WINID fromID
         * @param {string} WINID toID
         * @return
         * @example
         * @memberOf 
         */
        this.fn_changeTab = function (fromID,toID)
        {
            var nfromRow = this.ds_Tab.findRow("WINID", fromID);
            var ntoRow = this.ds_Tab.findRow("WINID", toID);
            this.ds_Tab.moveRow(nfromRow, ntoRow);
            this.fn_setTabSpinBtnShow();
        }

        /**
         * 메뉴 tab id가져오는 함수
         * @public
         * @param {string} 메뉴키값
         * @return
         * @example
         * @memberOf 
         */
        this.fn_getTab = function (winID)
        {
            return this.ds_Tab.findRow("WINID", winID);
        }

        /**
         * 메뉴 현재 tab id가져오는 함수
         * @public
         * @param
         * @return  {string} winid
         * @example
         * @memberOf 
         */
        this.fn_getCurTab = function ()
        {
            if (this.ds_Tab.rowposition < 0){
                return false;
            }
            
            return this.ds_Tab.getColumn(this.ds_Tab.rowposition, "WINID");
        }

        /**
         * 메뉴 현재 tab 정보 가져오는 함수
         * @public
         * @param
         * @return  {string} winid
         * @example
         * @memberOf 
         */
        this.fn_getTabInfo = function (winID,sCol)
        {
            var nRow = this.ds_Tab.findRow("WINID", winID);
            
            if (nRow < 0){
                return "";
            }
            
            return this.ds_Tab.getColumn(nRow, sCol);
        }

        /**
         * 메뉴 현재 tab title 가져오는 함수
         * @public
         * @param    {string} winID
         * @return  {string} TITLE
         * @example
         * @memberOf 
         */
        this.fn_getTitle = function (winID)
        {
            var curRow = this.ds_Tab.findRow("WINID", winID);
            
            if (this.lookup("nRow") < 0){
                return "";
            }
            
            return this.ds_Tab.getColumn(curRow, "TITLE");
        }

        /**
         *  메뉴 tab title setting
         * @public
         * @param    {string} winID
         * @return  {string} TITLE
         * @example
         * @memberOf 
         */
        this.fn_setTitle = function (winID,sTitle)
        {
            var nRow = this.ds_Tab.findRow("WINID", winID);
            
            if (nRow < 0){
                return "";
            }

            var tabID = this.TAB_BTN_PREFIX + winID;
            var panelObj = this.fn_findObj(tabID);
            
            if ((panelObj == null) || (panelObj == "")){
                return;
            }
            
            panelObj.set_text(sTitle);
        }

        /**
         *  메뉴 tab onclick event 
         * @public
         * @param  
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_Tab_OnClick = function (obj,e)
        {
            var winId = obj.name.split(this.TAB_BTN_PREFIX).join("");
            this.isActiveFrame(winId);
        }

        //라니안
        this.btn_Tab_OnLButtonUp = function(obj,e){
        	this.ImageViewer00.setFocus();
        }

        /**
         * 메뉴 tab extra버튼 클릭시 발생되는 event
         * @public
         * @param  
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_Extra_OnClick = function (obj,e)
        {
            var winId = obj.name.split(this.TAB_BTN_PREFIX).join("").split(this.EXTRA_BTN_PREFIX).join("");
            this.fn_TabOnClose(winId);     
        }
         
        /**
         * 메뉴 tab 닫기 실행 함수
         * @public
         * @param {string} winid
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_TabOnClose = function (winid)
        {
            var nRow = application.gds_openMenu.findRow("WINID", "M_" + winid);
        	application.gv_WorkFrame.removeChild("M_" + winid).form.close();
            
        //    application.gds_openMenu.deleteRow(nRow);
            
            // MDI 탭버튼 삭제
            if(nRow > -1){
                this.fn_delTab(winid);
            }
            
            //window id
            var sMenuId = "";
            
            if(application.gds_openMenu.rowcount == 0){
                application.gfn_setFrame("M");
            }else if(application.gds_openMenu.rowcount == 1){
                sMenuId = application.gds_openMenu.getColumn(0,"mekey");
                this.gfn_showMenu(sMenuId);
            }else{
                sMenuId = application.gds_openMenu.getColumn(application.gds_openMenu.rowposition,"mekey");
                this.gfn_showMenu(sMenuId);
            }

            // 탭버튼을 닫은 후에 next Tab버튼 설정
            this.fn_setActive(this.ds_Tab.getColumn(this.ds_Tab.findRow("WINID", sMenuId),"TAB_ID"));
        }

        /**
         * 메뉴 Mdi 다음  split버튼 
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_NexMdi_onclick = function (obj,e) 
        {
            this.fn_moveFirst(this.fn_getFirstTabIndex() + 1);
            this.fn_setTabSpinBtnShow();
        }
         
        /**
         * 메뉴 Mdi 이전   split버튼 
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_PreMdi_onclick = function (obj,e)
        {
            this.fn_moveFirst(this.fn_getFirstTabIndex() - 1);
            this.fn_setTabSpinBtnShow();
        }

        /**
         * menu tab 버튼 첫번째 index 가져오는 함수
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_getFirstTabIndex = function ()
        {
            for(var i=0; i < this.ds_Tab.rowcount;i++){
                var tabID   = this.ds_Tab.getColumn(i, "TAB_ID");
                var tabObj  = this.fn_findObj(tabID);
                
                if(0 <= tabObj.left){
                    return i;
                }
            }
            
            return -1;
        }

        /**
         * menu tab 버튼 첫번째 이동 함수
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_moveFirst = function (nMoveIdx)
        {
            var nIndex;
            var tabID;
            var tabObj;
            var btnObj;
            var tabFirstObj;

            nIndex = this.fn_getFirstTabIndex();
            
            if (nIndex < 0){
        //        return;
            }else if (nMoveIdx < 0){
        //        return;
            }else if (nMoveIdx > this.ds_Tab.rowcount){
                return;
            }

            tabID = this.ds_Tab.getColumn(nIndex, "TAB_ID");
            var tabFirstObj = this.fn_findObj(tabID);

            tabID = this.ds_Tab.getColumn(nMoveIdx, "TAB_ID");
            tabObj = this.fn_findObj(tabID);
        	
        	if(nIndex < 0) var nShiftPos = -179;
            else var nShiftPos = tabObj.getOffsetLeft() - tabFirstObj.getOffsetLeft();

            for (var i = 0; i < this.ds_Tab.rowcount; i++){
                tabID = this.ds_Tab.getColumn(i, "TAB_ID");
                tabObj = this.fn_findObj(tabID);
                btnObj = this.fn_findObj(this.EXTRA_BTN_PREFIX + tabID);        
                tabObj.move(tabObj.getOffsetLeft() - nShiftPos, tabObj.getOffsetTop());
                
                if(this.gfn_isNull(btnObj) == false )
                    btnObj.move(btnObj.getOffsetLeft() - nShiftPos, btnObj.getOffsetTop());
            }
        }

        /**
         * menu tab 현재 버튼 이동
         * @public
         * @param {string} tabID
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_setActive = function (tabID)
        {
            var nRow = this.ds_Tab.findRow("TAB_ID", tabID);
            
            if (nRow < 0){
                return false;
            }
            
            this.ds_Tab.set_rowposition(nRow);
            this.fn_setActiveBtn(tabID);

            return true;
        }

        /**
         * menu tab 현재 버튼 이동
         * @public
         * @param {string} tabID
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_setActiveBtn = function (tabID)
        {
            var TabObj;
            var BtnObj;
            
            for (var i = 0; i < this.ds_Tab.rowcount; i++){
                TabObj = this.fn_findObj(this.ds_Tab.getColumn(i, "TAB_ID"));
                BtnObj = this.fn_findObj(this.EXTRA_BTN_PREFIX + this.ds_Tab.getColumn(i, "TAB_ID"));
                
                if(tabID == this.ds_Tab.getColumn(i, "TAB_ID")){
                    TabObj.set_cssclass("btn_TaF_tab_on");
                    this.fn_showTabBtn(i);
                }else{
                   TabObj.set_cssclass("btn_TaF_tab");
                }
            }
        }

        /**
         * menu tab 현재 버튼 보여주는 함수
         * @public
         * @param {string} nIdx
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_showTabBtn = function (nIdx)
        {
            var i;
            var nLeft;
            var nRight;

            var tabObj = this.fn_findObj(this.ds_Tab.getColumn(nIdx, this.lookup("_ID")));
            nLeft = tabObj.getOffsetLeft();
            nRight = tabObj.getOffsetRight();

            if(0 <= nLeft && this.div_Tab.getOffsetWidth() >= nRight){
                return;
            }

            nRight = tabObj.getOffsetRight();
            nLeft = tabObj.getOffsetLeft();

            if (nLeft < 0){
                this.fn_moveFirst(nIdx);
                return;
            }

            for (var i = this.fn_getFirstTabIndex() + 1; i < this.ds_Tab.rowcount; i++){
                tabObj = this.fn_findObj(this.ds_Tab.getColumn(i, "TAB_ID"));
                
                if (nRight - tabObj.getOffsetLeft() <= this.div_Tab.getOffsetWidth()){
                    break;
                }
                
                this.fn_moveFirst(i);
            }
        }

        /**
         * menu tab 삭제
         * @public
         * @param {string} tabID
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_delTabBtn = function (tabID)
        {
            var exBtnId = this.EXTRA_BTN_PREFIX + tabID;
            var TabObj = this.fn_findObj(tabID);
            var BtnObj = this.fn_findObj(exBtnId);

            var nShitLeft = TabObj.getOffsetWidth() + this.BTN_GAP;
            var curRow = this.ds_Tab.findRow("TAB_ID", tabID);

            this.fn_removeObj(exBtnId);
            this.fn_removeObj(tabID);

            for (var i = curRow + 1; i < this.ds_Tab.rowcount; i++){
                TabObj = this.fn_findObj(this.ds_Tab.getColumn(i, "TAB_ID"));
                BtnObj = this.fn_findObj(this.EXTRA_BTN_PREFIX + this.ds_Tab.getColumn(i, "TAB_ID"));
                TabObj.move(TabObj.getOffsetLeft() - nShitLeft, TabObj.getOffsetTop());
                BtnObj.move(BtnObj.getOffsetLeft() - nShitLeft, BtnObj.getOffsetTop());
            }
        }

        /**
         * menu tab 찾는 함수
         * @public
         * @param {string} tabID
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_findObj = function (strId)
        {
            return this.div_Tab.components[strId];
        }

        /**
         * menu tab 삭제  함수
         * @public
         * @param {string} strId
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_removeObj = function (strId)
        {
            if (this.fn_findObj(strId) == null){
                return;
            }
            
            var strObj = this.div_Tab.removeChild(strId);
            
            if (strObj != null){
                strObj.destroy();
            }
        }

        /**
         * left size조정 
         * @public
         * @param 
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_getLeft = function (tabID)
        {
            var curRow = this.ds_Tab.findRow("TAB_ID", tabID);
            if (curRow == 0) return this.FIRST_GAP;

            var prevTab = this.fn_findObj(this.ds_Tab.getColumn(curRow - 1, "TAB_ID"));
            return prevTab.getOffsetRight() + this.BTN_GAP;
        }

        /**
         * width size조정 
         * @public
         * @param {object} 버튼
         * @param {name}   버튼명
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_getWidth = function (obj,name)
        {
            var tabID = obj.name;
            var curRow = this.ds_Tab.findRow("TAB_ID", tabID);
            var TabObj = this.fn_findObj(this.ds_Tab.getColumn(curRow, "TAB_ID"));
            
            var objFont = new nexacro.Style_font();
            objFont.set_size(10);
            objFont.set_face("Arial");
            objFont.set_type("normal");
            
            var objSize = nexacro.getTextSize(name, objFont);

            return objSize.nx;
        }

        /**
         * mdi spin 버튼 visible 처리
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_setTabSpinBtnShow = function ()
        {
            var tabObj;
            
            if(this.ds_Tab.rowcount == 0){
                this.btn_PreMdi.set_visible(false);
                this.btn_NexMdi.set_visible(false);
                return;
            }

            tabObj = this.fn_findObj(this.ds_Tab.getColumn(this.ds_Tab.rowcount - 1, "TAB_ID"));

            if(this.div_Tab.getOffsetWidth() < tabObj.getOffsetRight()){
                this.btn_NexMdi.set_visible(true);
            }else{
                this.btn_NexMdi.set_visible(false);
            }

            tabObj = this.fn_findObj(this.ds_Tab.getColumn(0, "TAB_ID"));

            if(tabObj.getOffsetLeft() < 0){
                this.btn_PreMdi.set_visible(true);
            }else{
                this.btn_PreMdi.set_visible(false);
            }
        }

         /**
         * 윈도우 키를 기준으로 열려있는 화면 여부 확인
         * @private
         * @param {string} winid: 윈도우 생성 키
         * @return {boolen}
         * @example
         *
         * @memberOf 
         */
        this.isActiveFrame = function(winid)
        {
            this.gfn_showMenu(winid);
            
            /** hckim 20181025, kpi rmate 차트 관련 문제로 추가 start **/
            // KPI 모듈인 경우
            if( application.gv_activeApp == "KPI" )
            {
        		// work 폼이 이미 존재하는 경우(이미 열려있는 메뉴인 경우)
        		if( application.gv_WorkFrame.frames["M_" + winid].form.div_work )
        			// work 폼 하위의 div_splitBottom 에 포커스를 준다.
        			// work 폼에 포커스 이벤트를 발생시키기 위함
        			application.gv_WorkFrame.frames["M_" + winid].form.div_work.div_splitBottom.setFocus();
            }
            /** hckim 20181025, kpi rmate 차트 관련 문제로 추가 end **/
            
        }

        /* btn_closeAll_onclick */
        this.btn_closeAll_onclick = function(obj,e)
        {
        	this.gfn_confirm("MSG_ALERT_ALLMENUCLOSE", "", function(msg, flag){
        		if(flag){
        			/** hckim 20181025, kpi rmate 차트 객체 destroy start **/
        			/** 전체 닫기 선택시 생성되어 있는 rMateChartH5 객체를 모두 destroy 해준다. **/
        			if( application.gv_activeApp == "KPI" )
        			{
        				// rMateChartH5 생성되어있는 객체
        				var obj = rMateChartH5.instances;
        				// 객체의 key를 배열로 keys 변수에 담는다
        				var keys = Object.keys(obj);

        				for(var i=0; i < keys.length; i++) {
        					var key = keys[i];
        					// key에 해당하는 rMateChartH5 객체를 destroy 한다.
        					rMateChartH5.instances[key].destroy();
        				}
        			}
        			/** hckim 20181025, kpi rmate 차트 객체 destroy end **/
        		
        			for(var i = this.ds_Tab.rowcount-1 ; i >= 0 ; i--){
        				this.fn_TabOnClose(this.ds_Tab.getColumn(i, "WINID"));
        			}
        			
        			application.gds_openMenu.clearData();
        			application.gfn_setFrame("M");
        		}
        	});
        }

        /* TabForm_onsize */
        this.TabForm_onsize = function(obj,e)
        {
            this.fn_setTabSpinBtnShow();
        }

        this.TabForm_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        this.fn_reload = function()
        {
        	var tabObj;
        	var tabName = "";
        	var nCompWidth = 0;
        	
        	for(var i = 0 ; i < this.ds_Tab.rowcount ; i++){
        		tabName = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.ds_Tab.getColumn(i, "WINID")), "mename");
        		tabObj = this.fn_findObj(this.ds_Tab.getColumn(i, "TAB_ID"));
        		tabObj.set_tooltiptext(tabName);
        		nCompWidth = this.fn_getWidth(tabObj, tabName)+30; // 30:Extra-Button
        		
        		// 탭버튼명에 말줄임 처리
        		if(nCompWidth > 150){
        			nCompWidth = 150;
        			if(application.gv_language == "KOR") tabObj.set_text( this.gfn_left(tabName, 9)+" . . ." );
        			else tabObj.set_text( this.gfn_left(tabName, 15)+" . . ." );
        		}else{
        			nCompWidth = 150;
        			tabObj.set_text(tabName);
        		}
            }
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.TabForm_onsize, this);
            this.addEventHandler("onkeydown", this.TabForm_onkeydown, this);
            this.div_Tab.addEventHandler("onclick", this.div_Tab_onclick, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.btn_PreMdi.addEventHandler("onclick", this.btn_PreMdi_onclick, this);
            this.btn_NexMdi.addEventHandler("onclick", this.btn_NexMdi_onclick, this);

        };

        this.loadIncludeScript("Tab.xfdl", true);

       
    };
}
)();
