﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SubMain");
                this.set_classname("SubMain");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new ImageViewer("img_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            obj.set_stretch("fit");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("SubMain");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("SubMain.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SubMain.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : SubMain 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        this.form_onload = function(obj,e)
        {
        	var utl = "";
        	
        	if(application.gv_activeApp == "AMS") utl = "url('img::main_ams_img_lotte.png')";
        	else if(application.gv_activeApp == "WMS") utl = "url('img::main_wms_img_lotte.png')";
        	else if(application.gv_activeApp == "TMS") utl = "url('img::main_tms_img_lotte.png')";
        	else if(application.gv_activeApp == "VIMS") utl = "url('img::main_vms_img_lotte.png')";
        	else if(application.gv_activeApp == "ICOM")  utl = "url('img::main_oms_img_lotte.png')";
        	else if(application.gv_activeApp == "PMS") utl = "url('img::main_pms_img.png')";
        	else if(application.gv_activeApp == "MDM") utl = "url('img::main_mdm_img.png')";
        	else if(application.gv_activeApp == "FIS") utl = "url('img::main_fis_img.png')";
        	else if(application.gv_activeApp == "CMS") utl = "url('img::main_cms_img.png')";
        	else if(application.gv_activeApp == "IBS") utl = "url('img::main_ibs_img.png')";
        	else if(application.gv_activeApp == "KPI") utl = "url('img::main_kpi_img_lotte.png')";
        	else if(application.gv_activeApp == "PTL") utl = "url('img::main_wms_img_lotte.png')";
        	
        	this.img_back.set_image(utl);
        	
        	// wms 모듈 진입시 센터를 변경할 수 있는 팝업을 호출한다.
        	if(application.gv_activeApp == "WMS") {
        		//this.fn_changeCenterPop();
        	}
        }

        // 센터 변경 팝업창 호출
        this.fn_changeCenterPop = function()
        {
        	var nLeft = -1;
            var nTop = -1;
            var bShowTitle = false;
            var bShowStatus = false;
            var bLayered = true;
            var nOpacity = 100;
            var bAutoSize = false;
            var bRound = false;
            var sPopupId = "ChangeCenterPop";
            
            var sUrl = "comm::ChangeCenterPop.xfdl";
            var oArg = "";
            var nWidth = 380;
            var nHeight = 160;
            var sPopupCallback = "";
            //var sPopupCallback = "fn_popupAfter";
            var bModeless = false;
            var bResizable = false;
            
            var oPopup = application.popupframes[sPopupId];
            
            if(oPopup != null) {
                trace(sPopupId + " === Popup ID 중복 ");
                oPopup.setFocus();
                return;
            }
            
            var sTitleText = "";
            var sOpenalign = "";
            
            if (nLeft == -1 && nTop == -1) {
                sOpenalign = "center middle";
                nLeft = (application.mainframe.width / 2) - Math.round(nWidth / 2);
                nTop  = (application.mainframe.height / 2) - Math.round(nHeight / 2);
            }
            
            var objParentFrame = this.getOwnerFrame();    
            
        	newChild = new nexacro.ChildFrame;
        	newChild.init(sPopupId,"absolute", nLeft, nTop, nWidth, nHeight, null, null, sUrl);
        	newChild.set_dragmovetype("all");
        	newChild.set_showtitlebar(bShowTitle);
        	newChild.set_autosize(bAutoSize);
        	newChild.set_resizable(bResizable);
        	newChild.set_titletext(sTitleText);
        	newChild.set_showstatusbar(bShowStatus);
        	newChild.set_openalign(sOpenalign);
        	newChild.showModal(objParentFrame, oArg, this, sPopupCallback, true);
        	
        	//this.gfn_setLocale(application.popupframes[sPopupId].form); // 시스템 언어 설정
        	var laKey = application.gds_userInfo.getColumn(0, "laKey");
        	
        	if(laKey == "KOR") application.popupframes[sPopupId].form.set_locale("ko_KR");
        	else if(laKey == "ENG") application.popupframes[sPopupId].form.set_locale("en_US");
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);

        };

        this.loadIncludeScript("SubMain.xfdl", true);

       
    };
}
)();
