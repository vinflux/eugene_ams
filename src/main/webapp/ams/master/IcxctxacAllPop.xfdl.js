﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("IcxctxacAllPop");
                this.set_classname("style01");
                this.set_titletext("센터별거래처별 상품 일괄수정 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,300);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"ibptkey\" type=\"STRING\" size=\"256\"/><Column id=\"poclosdate\" type=\"STRING\" size=\"256\"/><Column id=\"ibtime\" type=\"STRING\" size=\"256\"/><Column id=\"pohdtype\" type=\"STRING\" size=\"256\"/><Column id=\"poicuttype\" type=\"STRING\" size=\"256\"/><Column id=\"chk_ibptkey\" type=\"STRING\" size=\"256\"/><Column id=\"chk_poclosdate\" type=\"STRING\" size=\"256\"/><Column id=\"chk_ibtime\" type=\"STRING\" size=\"256\"/><Column id=\"chk_pohdtype\" type=\"STRING\" size=\"256\"/><Column id=\"chk_poicuttype\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"ibptkey\"/><Col id=\"poclosdate\"/><Col id=\"ibtime\"/><Col id=\"pohdtype\"/><Col id=\"poicuttype\"/><Col id=\"chk_ibptkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_pohdtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_poicuttype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static09", "absolute", "25", "136", null, "31", "25", null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "25", "105", null, "31", "25", null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "25", "74", null, "31", "25", null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            obj.style.set_border("1 solid #e6e6e6ff,0 none #808080ff");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "57", "74", "159", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_border("1 solid #e6e6e6ff,0 none #808080ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "57", "136", "159", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "57", "105", "159", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_ibptkey", "absolute", "32", "79", "19", "20", null, null, this);
            obj.set_taborder("0");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_poclosdate", "absolute", "32", "110", "19", "20", null, null, this);
            obj.set_taborder("2");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_ibtime", "absolute", "32", "141", "19", "20", null, null, this);
            obj.set_taborder("4");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "25", "167", null, "31", "25", null, this);
            obj.set_taborder("235");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_pohdtype", "absolute", "224", "171", "319", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("236");
            obj.set_innerdataset("@ds_pohdtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("static3", "absolute", "57", "167", "159", "31", null, null, this);
            obj.set_taborder("237");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_pohdtype", "absolute", "32", "172", "19", "20", null, null, this);
            obj.set_taborder("238");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "25", "198", null, "31", "25", null, this);
            obj.set_taborder("239");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_poicuttype", "absolute", "224", "202", "319", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("240");
            obj.set_innerdataset("@ds_poicuttype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("static4", "absolute", "57", "198", "159", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_poicuttype", "absolute", "32", "203", "19", "20", null, null, this);
            obj.set_taborder("242");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ibptkey", "absolute", "224", "78", "319", "22", null, null, this);
            obj.set_taborder("243");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_poclosdate", "absolute", "224", "109", "319", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("244");

            obj = new Combo("cbo_ibtime", "absolute", "224", "140", "319", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("245");

            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("247");
            obj.set_text("TITLE");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#000000ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("249");
            obj.set_cssclass("btn_P_close");
            obj.style.set_background("transparent");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "230", "15", this);
            obj.set_taborder("251");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "240", null, "52", "28", null, "15", this);
            obj.set_taborder("252");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "228", null, "1", "22", null, this);
            obj.set_taborder("253");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "73", null, "1", "22", null, this);
            obj.set_taborder("254");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "56", null, "2", "22", null, this);
            obj.set_taborder("255");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 300, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("센터별거래처별 상품 일괄수정 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","cbo_ibtime","value","ds_header","ibtime");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","POCLOSDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","IBTIME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","IBPTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","chk_ibptkey","value","ds_header","chk_ibptkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","chk_poclosdate","value","ds_header","chk_poclosdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","chk_ibtime","value","ds_header","chk_ibtime");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","cbo_pohdtype","value","ds_header","pohdtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","static3","text","gds_lang","POHDTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","chk_pohdtype","value","ds_header","chk_pohdtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","cbo_poicuttype","value","ds_header","poicuttype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","static4","text","gds_lang","POICUTTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","chk_poicuttype","value","ds_header","chk_poicuttype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_ibptkey","value","ds_header","ibptkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","cbo_poclosdate","value","ds_header","poclosdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","BATCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("IcxctxacAllPop.xfdl", "lib::Comm.xjs");
        this.registerScript("IcxctxacAllPop.xfdl", function(exports) {
        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("POHDTYPE", this.ds_pohdtype, "");
        	this.gfn_getCode("POICUTTYPE", this.ds_poicuttype, "");
            this.gv_flag = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.ds_searchList.copyData(this.parent.searchDs);
            
            this.time_combo_setting();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "icxctxacController");
        	this.gfn_setCommon("METHODNM", "saveIcxctxac");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   =  "ibptkey="+this.ds_header.getColumn(0, "ibptkey")
                         + " chk_ibptkey="+this.ds_header.getColumn(0, "chk_ibptkey")
                         + " poclosdate="+this.ds_header.getColumn(0, "poclosdate")
                         + " chk_poclosdate="+this.ds_header.getColumn(0, "chk_poclosdate")
                         + " ibtime="+this.ds_header.getColumn(0, "ibtime")
                         + " chk_ibtime="+this.ds_header.getColumn(0, "chk_ibtime")
                         + " pohdtype="+this.ds_header.getColumn(0, "pohdtype")
                         + " chk_pohdtype="+this.ds_header.getColumn(0, "chk_pohdtype")
                         + " poicuttype="+this.ds_header.getColumn(0, "poicuttype")
                         + " chk_poicuttype="+this.ds_header.getColumn(0, "chk_poicuttype")
                         + " workType="+this.parent.workType
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var arrChk = new Array();
        	var arrVal = new Array();
        	
        	arrChk.push(this.chk_ibptkey.value);
        	arrChk.push(this.chk_poclosdate.value);
        	arrChk.push(this.cbo_ibtime.value);
        	arrChk.push(this.cbo_pohdtype.value);
        	arrChk.push(this.cbo_poicuttype.value);
        	
        	arrVal.push(this.edt_ibptkey.value);
        	arrVal.push(this.cbo_poclosdate.value);
        	arrVal.push(this.cbo_ibtime.value);
        	arrVal.push(this.cbo_pohdtype.value);
        	arrVal.push(this.cbo_poicuttype.value);
        		
        	var chkRslt = this.fn_checkValue(arrChk, arrVal);
        	
        	if(!chkRslt){
        		this.gfn_alert("체크항목 중 값이 들어있지 않은 항목이 있습니다.");
        	}else{this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        		if(flag){
        			this.fn_Update();
        		}
        	});
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        this.fn_checkValue = function(arrChk,arrVal)
        {
        	var size = arrChk.length; 
        	var i=0;
        	for(i=0; i<size; i++){
        		if(arrChk[i] && (arrVal[i] == null || arrVal == "" || arrVal[i].length == 0)){
        		 return false;
        		}
        	}
        	return true;
        	
        }

        this.time_combo_setting = function()
        {
        	//15분 간격 시간 콤보 설정
            var ds_time = new Dataset();
            ds_time.addColumn("field1","string");
            ds_time.addColumn("field2","string");
            var init = 0;
            for(var i=0; i<96; i++){
        		ds_time.addRow();
        		if(Math.floor(init/60) < 10){
        			if(init%60==0){
        				ds_time.setColumn(i,"field1","0"+Math.floor(init/60)+""+init%60+"000");
        				ds_time.setColumn(i,"field2","0"+Math.floor(init/60)+":"+init%60+"0");
        			}else{
        				ds_time.setColumn(i,"field1","0"+Math.floor(init/60)+""+init%60+"00");
        				ds_time.setColumn(i,"field2","0"+Math.floor(init/60)+":"+init%60);
        			}
        		}else{
        			if(init%60==0){
        				ds_time.setColumn(i,"field1",Math.floor(init/60)+""+init%60+"000");
        				ds_time.setColumn(i,"field2",Math.floor(init/60)+":"+init%60+"0");
        			}else{
        				ds_time.setColumn(i,"field1",Math.floor(init/60)+""+init%60+"00");
        				ds_time.setColumn(i,"field2",Math.floor(init/60)+":"+init%60);
        			}
        		}
        		init = init+15;
            }
            this.cbo_ibtime.set_innerdataset(ds_time);
            this.cbo_ibtime.set_codecolumn("field1");
            this.cbo_ibtime.set_datacolumn("field2");
            
            this.cbo_poclosdate.set_innerdataset(ds_time);
            this.cbo_poclosdate.set_codecolumn("field1");
            this.cbo_poclosdate.set_datacolumn("field2");

        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);

        };

        this.loadIncludeScript("IcxctxacAllPop.xfdl", true);

       
    };
}
)();
