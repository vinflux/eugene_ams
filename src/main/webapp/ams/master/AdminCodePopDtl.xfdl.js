﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AdminCodePopDtl");
                this.set_classname("style01");
                this.set_titletext("관리 기준 코드 상세 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,342);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_adminDetailCode", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"adcd_dtkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtvalue\" type=\"STRING\" size=\"256\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtorder\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"adcd_dtname\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_hdkey\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"adcd_dtkey\"/><Col id=\"adcd_dtvalue\"/><Col id=\"comments\"/><Col id=\"lakey\"/><Col id=\"adcd_dtorder\"/><Col id=\"adcd_dtname\"/><Col id=\"adcd_hdkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("10");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_detcode", "absolute", "222", "93", "261", "22", null, null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_code", "absolute", "222", "62", "321", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "275", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("11");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_detname", "absolute", "222", "155", "321", "22", null, null, this);
            obj.set_taborder("6");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchLang", "absolute", "334", "124", "24", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_lang", "absolute", "222", "124", "110", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("235");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("237");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "213", "342", "31", null, null, this);
            obj.set_taborder("238");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_comments", "absolute", "222", "217", "321", "22", null, null, this);
            obj.set_taborder("8");
            this.addChild(obj.name, obj);

            obj = new Static("static5", "absolute", "22", "213", "194", "31", null, null, this);
            obj.set_taborder("240");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "244", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_detvalue", "absolute", "222", "248", "321", "22", null, null, this);
            obj.set_taborder("9");
            this.addChild(obj.name, obj);

            obj = new Static("static6", "absolute", "22", "244", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "93", "57", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_langname", "absolute", "360", "124", "183", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_detorder", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("244");
            obj.set_limitbymask("integer");
            obj.set_mask("##################################");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 342, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("관리 기준 코드 상세 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_detcode","value","ds_adminDetailCode","adcd_dtkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_code","value","ds_adminDetailCode","adcd_hdkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","ADMIN CODE DETAIL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_detname","value","ds_adminDetailCode","adcd_dtname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_lang","value","ds_adminDetailCode","lakey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_comments","value","ds_adminDetailCode","comments");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_detvalue","value","ds_adminDetailCode","adcd_dtvalue");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static","text","gds_lang","ADCD_HDKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static1","text","gds_lang","ADCD_DTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static2","text","gds_lang","LAKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ADCD_DTNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","static4","text","gds_lang","ADCD_DTORDER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","static5","text","gds_lang","ADCD_DTDESC");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static6","text","gds_lang","ADCD_DTVALUE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","mdt_detorder","value","ds_adminDetailCode","adcd_dtorder");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("AdminCodePopDtl.xfdl", "lib::Comm.xjs");
        this.registerScript("AdminCodePopDtl.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AdminCodePopDtl.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 관리 기준 코드 상세 팝업 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_code = "";
        this.gv_detcode = "";
        this.gv_lang = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_code     = this.gfn_isNullEmpty(this.parent.adcd_hdkey);
            this.gv_detcode  = this.gfn_isNullEmpty(this.parent.adcd_dtkey);
            this.gv_lang     = this.gfn_isNullEmpty(this.parent.lakey);
            
        	this.ds_adminDetailCode.setColumn(0, "adcd_hdkey", this.parent.adcd_hdkey);
        	
        	//this.gfn_decimalPointSet("mdt_detorder");

        	this.ds_adminDetailCode.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.edt_detcode.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.edt_lang.set_enable(false);
        		this.btn_searchLang.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_adminDetailCode.clearData();
        	
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "selectDetailAdminCodeInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_adminDetailCode=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "adcd_hdkey="+this.gv_code+" adcd_dtkey="+this.gv_detcode+" lakey="+this.gv_lang;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "checkDetailAdminCodeInfo");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   =  "adcd_hdkey="+this.gv_code+" adcd_dtkey="+this.ds_adminDetailCode.getColumn(0, "adcd_dtkey")+" lakey="+this.ds_adminDetailCode.getColumn(0, "lakey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "insertDetailAdminCode");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_adminDetailCode";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "updateDetailAdminCodeInfo");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_adminDetailCode";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_adminDetailCode.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "adcd_hdkey";
        		var value = "LAKEY";
        		
        		if(this.gfn_isNotNull(this.edt_lang.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_lang.value, "20", application.gv_ams, "", "", this.edt_lang, this.edt_langname);
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        //				this.edt_detcode.set_value("");
        				this.edt_detcode.setFocus();
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_work.fn_searchDetail();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_work.fn_searchDetail();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_adminDetailCode;
        	var dsCol = "adcd_dtvalue|lakey";
        	var sComp = "edt_detcode|edt_lang";
        	
        	if((this.ds_param.rowcount == 0 || this.ds_param.getColumn(0, "isDuplicate") == "true")||this.ds_param.getColumn(0, "isDuplicate") && this.gv_flag == "D"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_code.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_langname.value) && !this.gfn_isNull(this.edt_lang.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_lang.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_adminDetailCode)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					if(this.gv_flag == "D") this.fn_Insert();
        					else if(this.gv_flag == "U") this.fn_Update();
        				}
        			});
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_adminDetailCode)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* edt_detcode_onchanged 실행 */
        this.edt_detcode_onchanged = function(obj,e)
        {
        	if(this.ds_param.rowcount > 0) this.ds_param.setColumn(0, "isDuplicate", "1");
        }

        /* edt_lang_onchanged 실행 */
        this.edt_lang_onchanged = function(obj,e)
        {
        	if(this.ds_param.rowcount > 0) this.ds_param.setColumn(0, "isDuplicate", "1");
        	
        	var key = "adcd_hdkey";
        	var value = "LAKEY";
        	
        	if(this.gfn_isNotNull(this.edt_lang.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_lang.value, "20", application.gv_ams, "", "", this.edt_lang, this.edt_langname);
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.ds_param.getColumn(0, "isDuplicate") == "false"){
        		return;
        	}
        	
        	if(this.gfn_isNull(this.edt_detcode.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_detcode.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_lang.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_lang.setFocus();
        		});
        	}else{
        		this.fn_CheckCode();
        	}
        }

        /* btn_searchLang_onclick 실행 */
        this.btn_searchLang_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_lang
        				,putKey:"adcd_hdkey"
        				,putValue:"LAKEY"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_adminDetailCode_oncolumnchanged 실행 */
        this.ds_adminDetailCode_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "lakey"){
        		this.edt_langname.set_value(this.gv_Pvalue[1]);
        		this.edt_langname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        	
        	this.gv_Pvalue = "";
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_adminDetailCode.addEventHandler("oncolumnchanged", this.ds_adminDetailCode_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_detcode.addEventHandler("onchanged", this.edt_detcode_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchLang.addEventHandler("onclick", this.btn_searchLang_onclick, this);
            this.edt_lang.addEventHandler("onchanged", this.edt_lang_onchanged, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);

        };

        this.loadIncludeScript("AdminCodePopDtl.xfdl", true);

       
    };
}
)();
