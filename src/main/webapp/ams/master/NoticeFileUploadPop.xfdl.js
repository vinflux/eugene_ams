﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("noticeCreate");
                this.set_classname("notice");
                this.set_titletext("공지사항 등록");
                this._setFormPosition(0,0,420,39);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"INSERTURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEDATE\" type=\"STRING\" size=\"256\"/><Column id=\"PARENT_MSG_NO\" type=\"STRING\" size=\"16\"/><Column id=\"TITLE\" type=\"STRING\" size=\"32\"/><Column id=\"INSERTDATE\" type=\"STRING\" size=\"32\"/><Column id=\"CONTENT\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"UPPER_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"MSG_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"START_DT\" type=\"STRING\" size=\"256\"/><Column id=\"END_DT\" type=\"STRING\" size=\"256\"/><Column id=\"TARGET\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row/></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_close", "absolute", null, null, "52", "28", "8", "5", this);
            obj.set_taborder("20");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "8", "4", null, "36", "64", null, this);
            obj.set_taborder("21");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new FileUpload("FileUpload00", "absolute", "2", "10", "348", "22", null, null, this);
            obj.set_taborder("2");
            obj.getSetter("retry").set("0");
            obj.set_enable("true");
            obj.style.set_buttontext("파일");
            obj.set_index("0");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 420, 39, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("공지사항 등록");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","FileUpload00","","filedataset","");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("NoticeFileUploadPop.xfdl", "lib::Comm.xjs");
        this.registerScript("NoticeFileUploadPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : boardPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 게시판 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        this.gv_index = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
                   
            this.gv_index     = this.gfn_isNullEmpty(this.parent.index); // 추가/수정 구분
        	this.FileUpload00.setFocus();
        }

        
        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        
        this.FileUpload00_onitemchanged = function(obj,e)
        {

        	this.file = e.newvalue;
        	var sUrl = application.services["svc"].url+application.gv_ams + "/boardController/fileUpload.do";

        	var bSuccess = obj.upload(sUrl);
        	this.opener.fileSetup(this.FileUpload00.value,this.gv_index);

        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.FileUpload00.addEventHandler("onitemchanged", this.FileUpload00_onitemchanged, this);

        };

        this.loadIncludeScript("NoticeFileUploadPop.xfdl", true);

       
    };
}
)();
