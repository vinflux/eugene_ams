﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("StorePop");
                this.set_classname("style01");
                this.set_titletext("매출처(점포) 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,516);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"tel1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"fctype\" type=\"STRING\" size=\"32\"/><Column id=\"tel2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"cntrycd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"urkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck_desc\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"ktg_area_cd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"ktg_area_cd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"startdate\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"store_chg_date\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"store_name\" type=\"STRING\" size=\"32\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"store_key\" type=\"STRING\" size=\"32\"/><Column id=\"old_store_key\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"scmcheck\" type=\"STRING\" size=\"32\"/><Column id=\"regno\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"owner_name\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"closedate\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"address2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"address1\" type=\"STRING\" size=\"32\"/><Column id=\"stacd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"fctype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"store_type\" type=\"STRING\" size=\"32\"/><Column id=\"zipcode\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"new_store_key\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"scmurkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"regdate\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"tel1\"/><Col id=\"fctype\"/><Col id=\"tel2\"/><Col id=\"cntrycd_desc\"/><Col id=\"urkey\"/><Col id=\"inserturkey\"/><Col id=\"scmcheck_desc\"/><Col id=\"cntrycd\"/><Col id=\"ktg_area_cd\"/><Col id=\"lon\"/><Col id=\"ktg_area_cd_desc\"/><Col id=\"stacd\"/><Col id=\"startdate\"/><Col id=\"ctynm\"/><Col id=\"delyn_desc\"/><Col id=\"store_chg_date\"/><Col id=\"delyn\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"store_name\"/><Col id=\"closingdate\"/><Col id=\"lat\"/><Col id=\"store_key\"/><Col id=\"old_store_key\"/><Col id=\"scmcheck\"/><Col id=\"regno\"/><Col id=\"addnt\"/><Col id=\"owner_name\"/><Col id=\"closedate\"/><Col id=\"address2\"/><Col id=\"addrsttcd\"/><Col id=\"address1\"/><Col id=\"stacd_desc\"/><Col id=\"shpglocnm\"/><Col id=\"fctype_desc\"/><Col id=\"zipcd\"/><Col id=\"store_type\"/><Col id=\"zipcode\"/><Col id=\"unit\"/><Col id=\"new_store_key\"/><Col id=\"scmurkey\"/><Col id=\"regdate\"/><Col id=\"insertdate\"/><Col id=\"addrid\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_store_type", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_fctype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_addr", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static12", "absolute", "142", "152", "490", "31", null, null, this);
            obj.set_taborder("480");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "142", "429", "490", "31", null, null, this);
            obj.set_taborder("479");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("23");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "15", this);
            obj.set_taborder("21");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "90", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("출고처 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "90", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "125", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "121", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "121", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("출고처 명");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "327", "90", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("출고처 가맹점 계약 유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "214", "490", "30", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_store_key", "absolute", "148", "94", "105", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "187", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "183", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "183", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사용자 ID");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "183", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("출고처 소유주명");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "148", "402", "373", "22", null, null, this);
            obj.set_taborder("387");
            this.addChild(obj.name, obj);

            obj = new Static("Static36", "absolute", "142", "398", "490", "31", null, null, this);
            obj.set_taborder("388");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static37", "absolute", "22", "398", "120", "31", null, null, this);
            obj.set_taborder("389");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("반영일자");
            this.addChild(obj.name, obj);

            obj = new Static("Static38", "absolute", "327", "429", "120", "31", null, null, this);
            obj.set_taborder("390");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("전환일자");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owner_name", "absolute", "453", "187", "165", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_enable("true");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "15", this);
            obj.set_taborder("22");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_store_name", "absolute", "148", "125", "165", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            obj.set_maxlength("100");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "305", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("지역코드(KTG)");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "305", "490", "31", null, null, this);
            obj.set_taborder("439");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "148", "340", "373", "22", null, null, this);
            obj.set_taborder("441");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "142", "336", "490", "31", null, null, this);
            obj.set_taborder("442");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "22", "336", "120", "31", null, null, this);
            obj.set_taborder("443");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("신출고처 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "327", "398", "120", "31", null, null, this);
            obj.set_taborder("444");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("오픈일시");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ktg_area_cd", "absolute", "148", "309", "470", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_enable("true");
            obj.set_maxlength("3");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "141", "367", "490", "31", null, null, this);
            obj.set_taborder("455");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "22", "429", "120", "31", null, null, this);
            obj.set_taborder("456");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("종료일");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "367", "120", "31", null, null, this);
            obj.set_taborder("457");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("운영 구분");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_store_type", "absolute", "148", "371", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("16");
            obj.set_innerdataset("ds_store_type");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Button("btn_check", "absolute", "256", "94", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_fctype", "absolute", "453", "94", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_innerdataset("ds_fctype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Edit("edt_new_store_key", "absolute", "148", "340", "138", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_enable("true");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchNewStoreKey", "absolute", "289", "340", "24", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_store_chg_date", "absolute", "453", "433", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("20");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.style.set_cursor("hand");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "55", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_new_store_name", "absolute", "327", "340", "291", "22", null, null, this);
            obj.set_taborder("15");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "453", "125", "165", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_limitbymask("integer");
            obj.set_maskchar("_");
            obj.set_mask("###-##-#####");
            obj.set_type("string");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "696", "62", "165", "22", null, null, this);
            obj.set_taborder("482");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_unit", "absolute", "696", "86", "165", "22", null, null, this);
            obj.set_taborder("483");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "696", "110", "165", "22", null, null, this);
            obj.set_taborder("484");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "696", "134", "165", "22", null, null, this);
            obj.set_taborder("485");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "696", "158", "165", "22", null, null, this);
            obj.set_taborder("486");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "696", "182", "165", "22", null, null, this);
            obj.set_taborder("487");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "696", "206", "165", "22", null, null, this);
            obj.set_taborder("488");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "696", "230", "165", "22", null, null, this);
            obj.set_taborder("489");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAddr", "absolute", "290", "218", "24", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "327", "121", "120", "31", null, null, this);
            obj.set_taborder("494");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사업자등록 번호");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urkey", "absolute", "148", "187", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_startdate", "absolute", "453", "402", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("18");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.style.set_cursor("hand");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Calendar("cal_regdate", "absolute", "148", "402", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("17");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.style.set_cursor("hand");
            obj.set_value("null");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Calendar("cal_closedate", "absolute", "148", "433", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("19");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.style.set_cursor("hand");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Static("Static02", "absolute", "22", "59", "120", "31", null, null, this);
            obj.set_taborder("495");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "142", "59", "490", "31", null, null, this);
            obj.set_taborder("496");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "138", "22", null, null, this);
            obj.set_taborder("497");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "289", "62", "24", "22", null, null, this);
            obj.set_taborder("498");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "214", "120", "31", null, null, this);
            obj.set_taborder("499");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "148", "218", "138", "22", null, null, this);
            obj.set_taborder("500");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "245", "120", "30", null, null, this);
            obj.set_taborder("501");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "141", "245", "490", "30", null, null, this);
            obj.set_taborder("502");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "249", "469", "22", null, null, this);
            obj.set_taborder("503");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "22", "275", "120", "30", null, null, this);
            obj.set_taborder("504");
            obj.set_text("상세 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "141", "275", "490", "30", null, null, this);
            obj.set_taborder("505");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "279", "470", "22", null, null, this);
            obj.set_taborder("506");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "813", "305", "490", "31", null, null, this);
            obj.set_taborder("507");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "999", "305", "120", "31", null, null, this);
            obj.set_taborder("509");
            obj.set_text("경도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "694", "275", "120", "31", null, null, this);
            obj.set_taborder("511");
            obj.set_text("위도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lat", "absolute", "820", "309", "165", "22", null, null, this);
            obj.set_taborder("512");
            obj.set_mask("##.####");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lon", "absolute", "1125", "309", "165", "22", null, null, this);
            obj.set_taborder("513");
            obj.set_mask("###.####");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_old_store_key", "absolute", "148", "156", "165", "22", null, null, this);
            obj.set_taborder("514");
            obj.set_maxlength("5");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "22", "152", "120", "31", null, null, this);
            obj.set_taborder("515");
            obj.set_text("구출고처 코드");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 516, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("매출처(점포) 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item9","Static19","text","gds_lang","OLD_STORE_KEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_old_store_key","value","ds_header","old_store_key");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_store_key","value","ds_header","store_key");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_owner_name","value","ds_header","owner_name");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","STORE_KEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","STORE_NAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","FCTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","URKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","OWNER_NAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","Static37","text","gds_lang","REGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item40","Static38","text","gds_lang","STORE_CHG_DATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_store_name","value","ds_header","store_name");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","KTG_AREA_CD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","NEW_STORE_KEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","STARTDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_ktg_area_cd","value","ds_header","ktg_area_cd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static43","text","gds_lang","CLOSEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item59","Static44","text","gds_lang","STORE_TYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_store_type","value","ds_header","store_type");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","STORE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","cbo_fctype","value","ds_header","fctype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","edt_new_store_key","value","ds_header","new_store_key");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cal_store_chg_date","value","ds_header","store_chg_date");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","edt_unit","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","edt_ctynm","value","ds_header","ctynm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static01","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_urkey","value","ds_header","urkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","cal_startdate","value","ds_header","startdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","cal_regdate","value","ds_header","regdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","cal_closedate","value","ds_header","closedate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","Static02","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Static05","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static06","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","Static13","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","Static14","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","Static17","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","mdt_lat","value","ds_header","lat");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","mdt_lon","value","ds_header","lon");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("StorePop_old.xfdl", "lib::Comm.xjs");
        this.registerScript("StorePop_old.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : StorePop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 매출처(점포) 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_store_key = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";
        this.gv_owkey = "";
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("FCTYPE", this.ds_fctype);
        	this.gfn_getCode("STORETYPE", this.ds_store_type, "");
        	
            this.gv_flag      = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId    = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_store_key = this.gfn_isNullEmpty(this.parent.store_key);
            this.gv_owkey     = this.gfn_isNullEmpty(this.parent.owkey);
        	this.ds_header.setColumn(0, "lon", 0);
        	this.ds_header.setColumn(0, "lat", 0);
        	this.ds_header.applyChange();

            if(this.gv_flag == "U"){
        		
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_store_key.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.edt_store_key.setFocus();
        		this.fn_search();
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "selectStore");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "store_key="+this.gv_store_key
        				 + " owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchCode = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "scxstoreController");
        	this.gfn_setCommon("METHODNM", "searchStoreInfo");
        	
            var sSvcId   = "selectCode";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "searchValue="+this.edt_new_store_key.value
                         + " opval=eq"
        				 ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "selectStore");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   =  "store_key="+this.ds_header.getColumn(0, "store_key")
                         +  " owkey="+this.ds_header.getColumn(0, "owkey") 
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "saveStore");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "saveStore");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		if(this.gfn_isNotNull(this.edt_new_store_key.value)){
        			this.gv_code = "Y";
        			this.fn_searchCode();
        		}
        	}else if(sSvcId == "selectCode"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_new_store_name.set_value(this.ds_code.getColumn(0, "store_name"));
        			this.edt_new_store_name.set_tooltiptext(this.ds_code.getColumn(0, "store_name"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_new_store_name.set_value("");
        					this.edt_new_store_key.setFocus();
        				});
        			}else{
        				this.edt_new_store_name.set_value("");
        				this.edt_new_store_key.setFocus();
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cbo_icuttype.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        this.fn_calCutValue = function(sValue,colId)
        {
        	this.ds_header.setColumn(0, colId, sValue.substr(0, 14));
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "store_key|fctype|store_name|zipNo|address2|store_type";
        	var sComp = "edt_store_key|cbo_fctype|edt_store_name|edt_zipcode|text_address2|cbo_store_type";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.cbo_icuttype.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_new_store_name.value) && !this.gfn_isNull(this.edt_new_store_key.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_new_store_key.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "H")	this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchNewStoreKey_onclick 실행 */
        this.btn_searchNewStoreKey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { putObj:this.edt_new_store_key
        			   };
        	this.gfn_popup("StoreKeySearchPop", "master::StoreKeySearchPop.xfdl", oArg, 700, 497, "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|store_key";
        	var sComp = "edt_owkey|edt_store_key";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "store_key"){
        		this.gv_dupChk = false;
        	}else if(e.columnid == "new_store_key"){
        		this.edt_new_store_name.set_value(this.gv_Pvalue[1]);
        		this.edt_new_store_name.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "regdate"){
        		this.fn_calCutValue(""+e.newvalue, "regdate");
        	}else if(e.columnid == "startdate"){
        		this.fn_calCutValue(""+e.newvalue, "startdate");
        	}else if(e.columnid == "closedate"){
        		this.fn_calCutValue(""+e.newvalue, "closedate");
        	}else if(e.columnid == "store_chg_date"){
        		this.fn_calCutValue(""+e.newvalue, "store_chg_date");
        	}
        // 	else if(e.columnid == "addr" && this.gfn_isNotNull(this.ds_addr.getColumn(0, "zipcd"))){
        // 		obj.setColumn(0, "lat", this.ds_addr.getColumn(0, "lat"));
        // 		obj.setColumn(0, "lon", this.ds_addr.getColumn(0, "lon"));
        // 		obj.setColumn(0, "addrid", this.ds_addr.getColumn(0, "addrid"));
        // 		obj.setColumn(0, "unit", this.ds_addr.getColumn(0, "unit"));
        // 		obj.setColumn(0, "stacd", this.ds_addr.getColumn(0, "stacd"));
        // 		obj.setColumn(0, "ctynm", this.ds_addr.getColumn(0, "ctynm"));
        // 		obj.setColumn(0, "stnm", this.ds_addr.getColumn(0, "stnm"));
        // 		obj.setColumn(0, "cntrycd", this.ds_addr.getColumn(0, "cntrycd"));
        // 		obj.setColumn(0, "zipcd", this.ds_addr.getColumn(0, "zipcd"));
        // 		obj.setColumn(0, "zipcode", this.ds_addr.getColumn(0, "zipcd"));
        // 		obj.setColumn(0, "addrsttcd", this.ds_addr.getColumn(0, "addrsttcd"));
        // 	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_new_store_key_onchanged 실행 */
        this.edt_new_store_key_onchanged = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_new_store_key.value)){
        		this.gv_code = "";
        		this.fn_searchCode();
        	}
        }

        /* btn_searchAddr_onclick 실행 */
        this.btn_searchAddr_onclick = function(obj,e)
        {
        	
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }

        /* edt_store_key_onchanged 실행 */
        this.edt_store_key_onchanged = function(obj,e)
        {
        	if(e.postvalue != ""){
        		if(this.gfn_isNotNull(this.edt_new_store_key.value)){
        			this.gv_code = "";
        			this.fn_searchCode();
        		}
        	}
        }

        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_store_key.addEventHandler("onchanged", this.edt_store_key_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_store_name.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.edt_new_store_key.addEventHandler("onchanged", this.edt_new_store_key_onchanged, this);
            this.btn_searchNewStoreKey.addEventHandler("onclick", this.btn_searchNewStoreKey_onclick, this);
            this.cal_store_chg_date.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_store_chg_date.addEventHandler("canchange", this.cal_to_canchange, this);
            this.btn_searchAddr.addEventHandler("onclick", this.btn_searchAddr_onclick, this);
            this.cal_startdate.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_startdate.addEventHandler("canchange", this.cal_to_canchange, this);
            this.cal_regdate.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_regdate.addEventHandler("canchange", this.cal_to_canchange, this);
            this.cal_closedate.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_closedate.addEventHandler("canchange", this.cal_to_canchange, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);

        };

        this.loadIncludeScript("StorePop_old.xfdl", true);

       
    };
}
)();
