﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("StorePop");
                this.set_classname("style01");
                this.set_titletext("매출처(점포) 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,360);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"tel1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"tel2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"cntrycd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"urkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck_desc\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"ktg_area_cd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"ktg_area_cd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"store_name\" type=\"STRING\" size=\"32\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"store_key\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck\" type=\"STRING\" size=\"32\"/><Column id=\"regno\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"owner_name\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"closedate\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"address2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"address1\" type=\"STRING\" size=\"32\"/><Column id=\"stacd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"zipcode\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"scmurkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"fn_get_master_impos\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"tel1\"/><Col id=\"tel2\"/><Col id=\"cntrycd_desc\"/><Col id=\"urkey\"/><Col id=\"inserturkey\"/><Col id=\"scmcheck_desc\"/><Col id=\"cntrycd\"/><Col id=\"ktg_area_cd\"/><Col id=\"lon\"/><Col id=\"ktg_area_cd_desc\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn_desc\"/><Col id=\"delyn\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"store_name\"/><Col id=\"closingdate\"/><Col id=\"lat\"/><Col id=\"store_key\"/><Col id=\"scmcheck\"/><Col id=\"regno\"/><Col id=\"addnt\"/><Col id=\"owner_name\"/><Col id=\"closedate\"/><Col id=\"address2\"/><Col id=\"addrsttcd\"/><Col id=\"address1\"/><Col id=\"stacd_desc\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"zipcode\"/><Col id=\"unit\"/><Col id=\"scmurkey\"/><Col id=\"insertdate\"/><Col id=\"addrid\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_addr", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static07", "absolute", "142", "273", "490", "31", null, null, this);
            obj.set_taborder("479");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("23");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "15", this);
            obj.set_taborder("12");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "836", "348", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("출고처 코드");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "182", "490", "30", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_store_key", "absolute", "842", "373", "105", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("출고처 소유주명");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owner_name", "absolute", "453", "93", "165", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "15", this);
            obj.set_taborder("13");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "22", "273", "120", "31", null, null, this);
            obj.set_taborder("456");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("종료일");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "928", "342", "57", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_text("중복체크");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "55", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_limitbymask("integer");
            obj.set_maskchar("_");
            obj.set_mask("###-##-#####");
            obj.set_type("string");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "696", "62", "165", "22", null, null, this);
            obj.set_taborder("482");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_unit", "absolute", "696", "86", "165", "22", null, null, this);
            obj.set_taborder("483");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "696", "110", "165", "22", null, null, this);
            obj.set_taborder("484");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "696", "134", "165", "22", null, null, this);
            obj.set_taborder("485");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "696", "158", "165", "22", null, null, this);
            obj.set_taborder("486");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "696", "182", "165", "22", null, null, this);
            obj.set_taborder("487");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "696", "206", "165", "22", null, null, this);
            obj.set_taborder("488");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "696", "230", "165", "22", null, null, this);
            obj.set_taborder("489");
            obj.set_enable("false");
            obj.set_visible("false");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAddr", "absolute", "290", "186", "24", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "23", "89", "120", "31", null, null, this);
            obj.set_taborder("494");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사업자등록 번호");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_closedate", "absolute", "148", "277", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("11");
            obj.set_dateformat("yyyy-MM-dd ");
            obj.style.set_cursor("hand");
            obj.set_editformat("yyyy-MM-dd ");
            obj.set_value("null");

            obj = new Static("Static02", "absolute", "22", "59", "120", "31", null, null, this);
            obj.set_taborder("495");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "142", "59", "490", "31", null, null, this);
            obj.set_taborder("496");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "138", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_maxlength("20");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "289", "62", "24", "22", null, null, this);
            obj.set_taborder("498");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "182", "120", "31", null, null, this);
            obj.set_taborder("499");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "148", "186", "138", "22", null, null, this);
            obj.set_taborder("500");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "213", "120", "30", null, null, this);
            obj.set_taborder("501");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "141", "213", "490", "30", null, null, this);
            obj.set_taborder("502");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "217", "469", "22", null, null, this);
            obj.set_taborder("503");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "22", "243", "120", "30", null, null, this);
            obj.set_taborder("504");
            obj.set_text("상세 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "141", "243", "490", "30", null, null, this);
            obj.set_taborder("505");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "247", "470", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "813", "305", "490", "31", null, null, this);
            obj.set_taborder("507");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "999", "305", "120", "31", null, null, this);
            obj.set_taborder("509");
            obj.set_text("경도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "814", "275", "120", "31", null, null, this);
            obj.set_taborder("511");
            obj.set_text("위도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lat", "absolute", "820", "309", "165", "22", null, null, this);
            obj.set_taborder("512");
            obj.set_mask("##.####");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lon", "absolute", "1125", "309", "165", "22", null, null, this);
            obj.set_taborder("513");
            obj.set_mask("###.####");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "23", "120", "120", "31", null, null, this);
            obj.set_taborder("514");
            obj.set_text("담당자");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "152", "120", "31", null, null, this);
            obj.set_taborder("516");
            obj.set_text("전화 번호1");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "152", "490", "31", null, null, this);
            obj.set_taborder("518");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "152", "120", "31", null, null, this);
            obj.set_taborder("519");
            obj.set_text("전화 번호2");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel2", "absolute", "453", "156", "165", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_inputtype("number,sign");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel1", "absolute", "148", "155", "165", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_inputtype("number,sign");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "141", "120", "490", "31", null, null, this);
            obj.set_taborder("523");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_contact", "absolute", "148", "125", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_maxlength("10");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fn_get_master_impos", "absolute", "880", "50", "67", "18", null, null, this);
            obj.set_taborder("525");
            obj.set_maxlength("10");
            obj.set_enable("false");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "687", "22", "291", "22", null, null, this);
            obj.set_taborder("526");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "327", "59", "120", "31", null, null, this);
            obj.set_taborder("527");
            obj.set_text("출고처 명");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_store_name", "absolute", "453", "63", "165", "22", null, null, this);
            obj.set_taborder("528");
            obj.set_maxlength("100");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 360, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("매출처(점포) 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_store_key","value","ds_header","store_key");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_owner_name","value","ds_header","owner_name");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","STORE_KEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","OWNER_NAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static43","text","gds_lang","CLOSEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","STORE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","edt_unit","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","edt_ctynm","value","ds_header","ctynm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static01","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","cal_closedate","value","ds_header","closingdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","Static02","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Static05","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static06","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","edt_address111","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","Static13","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","Static14","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","Static17","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","mdt_lat","value","ds_header","lat");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","mdt_lon","value","ds_header","lon");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static31","text","gds_lang","CONTACT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","TEL1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","TEL2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_tel2","value","ds_header","tel2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","edt_tel1","value","ds_header","tel1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_contact","value","ds_header","sc_nm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_fn_get_master_impos","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","STORE_NAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_store_name","value","ds_header","store_name");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("StorePop.xfdl", "lib::Comm.xjs");
        this.registerScript("StorePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : StorePop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 매출처(점포) 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_store_key = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";
        this.gv_owkey = "";
        this.gv_zipcode = "";
        this.gv_address2 = "";
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag      = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId    = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_store_key = this.gfn_isNullEmpty(this.parent.store_key);
            this.gv_owkey     = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_zipcode    = this.gfn_isNullEmpty(this.parent.zipcode);
            this.gv_address2    = this.gfn_isNullEmpty(this.parent.address2);
            var owkey = this.gfn_getUserInfo("owkeym").split(",");
        	this.ds_header.setColumn(0, "owkey", owkey[0]);
        	this.ds_header.setColumn(0, "lon", 0);
        	this.ds_header.setColumn(0, "lat", 0);
        	this.ds_header.applyChange();

            if(this.gv_flag == "U"){
        		
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_store_key.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.edt_store_key.setFocus();
        		this.fn_search();
        	} else{
        	
        		this.edt_owkey.setFocus();
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "selectStore");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "store_key="+this.gv_store_key
        				 + " owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "selectCeckStore");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   =  "store_key="+this.ds_header.getColumn(0, "store_key")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "saveStore");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "storeController");
        	this.gfn_setCommon("METHODNM", "saveStore");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        	
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cbo_icuttype.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        this.fn_calCutValue = function(sValue,colId)
        {
        	this.ds_header.setColumn(0, colId, sValue.substr(0, 14));
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "store_name|zipNo|address2";
        	var sComp = "edt_store_name|edt_zipcode|text_address2";
        	
        // 	if(!this.gv_dupChk && this.gv_flag == "H"){
        // 		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        // 			this.cbo_icuttype.setFocus();
        // 		});
        // 	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "H")	this.fn_Insert();
        							else if(this.gv_flag == "U") {
        								//주소가 변경되면 새로운 주소ID 생성하기 위한 로직
        								if(this.gv_zipcode != this.edt_zipcode.value || this.gv_address2 != this.text_address2.value){
        									this.ds_header.setColumn(0, "addrid", "");
        								}
        								this.fn_Update();
        							}
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	//}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        // this.btn_check_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        // {
        // 	if(this.gv_dupChk){
        // 		return;
        // 	}
        // 	
        // 	var dsObj = this.ds_header;
        // 	var dsCol = "store_key";
        // 	var sComp = "edt_store_key";
        // 	
        // 	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        // 		this.fn_CheckCode();
        // 	}
        // }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {

        	if(e.columnid == "owkey"){
        		this.gv_dupChk = false;
        		this.edt_fn_get_master_impos.set_value(this.gv_Pvalue[2]);
        		this.edt_zipcode.set_value("");
        		this.edt_address1.set_value("");
        		
        		if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        	}
        		
        	
        	if(e.columnid == "store_key"){
        		this.gv_dupChk = false;
        	}else if(e.columnid == "startdate"){
        		this.fn_calCutValue(""+e.newvalue, "startdate");
        	}else if(e.columnid == "closedate"){
        		this.fn_calCutValue(""+e.newvalue, "closedate");
        	}else if(e.columnid == "store_chg_date"){
        		this.fn_calCutValue(""+e.newvalue, "store_chg_date");
        	}
        // 	else if(e.columnid == "addr" && this.gfn_isNotNull(this.ds_addr.getColumn(0, "zipcd"))){
        // 		obj.setColumn(0, "lat", this.ds_addr.getColumn(0, "lat"));
        // 		obj.setColumn(0, "lon", this.ds_addr.getColumn(0, "lon"));
        // 		obj.setColumn(0, "addrid", this.ds_addr.getColumn(0, "addrid"));
        // 		obj.setColumn(0, "unit", this.ds_addr.getColumn(0, "unit"));
        // 		obj.setColumn(0, "stacd", this.ds_addr.getColumn(0, "stacd"));
        // 		obj.setColumn(0, "ctynm", this.ds_addr.getColumn(0, "ctynm"));
        // 		obj.setColumn(0, "stnm", this.ds_addr.getColumn(0, "stnm"));
        // 		obj.setColumn(0, "cntrycd", this.ds_addr.getColumn(0, "cntrycd"));
        // 		obj.setColumn(0, "zipcd", this.ds_addr.getColumn(0, "zipcd"));
        // 		obj.setColumn(0, "zipcode", this.ds_addr.getColumn(0, "zipcd"));
        // 		obj.setColumn(0, "addrsttcd", this.ds_addr.getColumn(0, "addrsttcd"));
        // 	}
        	
        	this.gv_Pvalue = "";
        }

        
        /* btn_searchAddr_onclick 실행 */
        this.btn_searchAddr_onclick = function(obj,e)
        {
        	
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }

        
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_store_key.addEventHandler("onchanged", this.edt_store_key_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.btn_searchAddr.addEventHandler("onclick", this.btn_searchAddr_onclick, this);
            this.cal_closedate.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_closedate.addEventHandler("canchange", this.cal_to_canchange, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.edt_tel2.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_store_name.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);

        };

        this.loadIncludeScript("StorePop.xfdl", true);

       
    };
}
)();
