﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Address");
                this.set_classname("style01");
                this.set_titletext("주소");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"addrid\" type=\"STRING\" size=\"256\"/><Column id=\"ctry\" type=\"STRING\" size=\"256\"/><Column id=\"addnt\" type=\"STRING\" size=\"256\"/><Column id=\"unit\" type=\"STRING\" size=\"256\"/><Column id=\"stnm\" type=\"STRING\" size=\"256\"/><Column id=\"ctynm\" type=\"STRING\" size=\"256\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"256\"/><Column id=\"zipcd\" type=\"STRING\" size=\"256\"/><Column id=\"sta\" type=\"STRING\" size=\"256\"/><Column id=\"stacd\" type=\"STRING\" size=\"256\"/><Column id=\"stry\" type=\"STRING\" size=\"256\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"256\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"addrid\" type=\"STRING\" size=\"256\"/><Column id=\"shpgloctycd\" type=\"STRING\" size=\"256\"/><Column id=\"shpgloccd\" type=\"STRING\" size=\"256\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"256\"/><Column id=\"shpglocid\" type=\"STRING\" size=\"256\"/><Column id=\"shpgloctypnm\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_stacd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_show", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_inputrestrict", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ymd_format", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_paramDetail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "-94", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "-57", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"180\"/><Column size=\"140\"/><Column size=\"300\"/><Column size=\"220\"/><Column size=\"120\"/><Column size=\"86\"/><Column size=\"140\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/><Cell col=\"1\" text=\"ADDRID\"/><Cell col=\"2\" text=\"CNTRYCD\"/><Cell col=\"3\" text=\"CNTRYNM\"/><Cell col=\"4\" text=\"ZIPCD\"/><Cell col=\"5\" text=\"CTYNM\"/><Cell col=\"6\" text=\"STNM\"/><Cell col=\"7\" text=\"ADDNT\"/><Cell col=\"8\" text=\"DELYN\"/><Cell col=\"9\" text=\"INSERTDATE\"/><Cell col=\"10\" text=\"INSERTURKEY\"/><Cell col=\"11\" text=\"UPDATEDATE\"/><Cell col=\"12\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"1\" displaytype=\"text\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:addrid\" editlimit=\"10\" editdisplay=\"edit\" editlimitbymask=\"none\"/><Cell col=\"2\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:cntrycd\" editlimit=\"100\" editdisplay=\"edit\" expandshow=\"hide\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" text=\"bind:ctry\"/><Cell col=\"4\" edittype=\"none\" text=\"bind:zipcd\" expandshow=\"hide\"/><Cell col=\"5\" edittype=\"none\" text=\"bind:ctynm\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" text=\"bind:stnm\"/><Cell col=\"7\" edittype=\"none\" text=\"bind:addnt\"/><Cell col=\"8\" text=\"bind:delyn_desc\"/><Cell col=\"9\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"11\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"12\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "-131", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"120\"/><Column size=\"180\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"200\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"SHPGLOCTYP\"/><Cell col=\"2\" text=\"SHPGLOCID\"/><Cell col=\"3\" text=\"SHPGLOCNM\"/><Cell col=\"4\" text=\"INSERTDATE\"/><Cell col=\"5\" text=\"INSERTURKEY\"/><Cell col=\"6\" text=\"UPDATEDATE\"/><Cell col=\"7\" text=\"UPDATEURKEY\"/><Cell col=\"8\" text=\"ADDRID\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"1\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:shpgloctypnm\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"2\" displaytype=\"normal\" edittype=\"none\" style=\"align:right;padding: ;\" text=\"bind:shpglocid\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:shpglocnm\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"4\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"5\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"6\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"7\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/><Cell col=\"8\" displaytype=\"text\" style=\"align:left;padding: ;\" text=\"bind:addrid\" editlimitbymask=\"none\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_text("주소별 거점 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("21");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this);
            obj.set_taborder("6");
            obj.set_text("주소 내역");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("주소");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitBottom.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","Static00","text","gds_lang","ME00105490_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.Static06","text","gds_lang","ME00105490_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Address.xfdl", "lib::Comm.xjs");
        this.registerScript("Address.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Address.xfdl
        * PROGRAMMER  : 장호진
        * DATE        : 2017.11.27
        * DESCRIPTION : 주소
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";
        var dtlsearchcnt = 0;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header, this.gv_detail]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	//그리드 숫자입력 자리수 제한
        	var grdObj = this.gv_grdList[0];
        	var decimalPointValue = ".00";
        	for(var j = 0 ; j < grdObj.getCellCount("head") ; j++){
        		editType = this.gfn_isNullEmpty(grdObj.getCellProperty("body", j, "edittype"));
        		dispType = grdObj.getCellProperty("body", j, "displaytype");
        		cellName = grdObj.getCellProperty("head",j,"text");
        		
        		if(editType == "masknumber" || dispType == "number" && cellName == "LAT"){
        			if(grdObj.getCellText(-1, j) != "NO"){
        				grdObj.setCellProperty("body", j, "editautoselect", true);
        				grdObj.setCellProperty("body", j, "mask", "##0" + decimalPointValue);
        				grdObj.setCellProperty("summary", j, "mask", "##0" + decimalPointValue);
        			}else{
        				grdObj.setCellProperty("body", j, "mask", "##0");
        			}
        			
        			grdObj.setCellProperty("body", j, "align", "right");
        		}
        	}

        	this.gfn_getCode("STACD", this.ds_stacd);
        	this.gfn_getCode("SHOW_VALUE", this.ds_show);
        	this.gfn_getCode("INPUTRESTRICT", this.ds_inputrestrict);
        	this.gfn_getCode("YMD_FORMAT", this.ds_ymd_format);
        	this.gfn_getCode("YESORNO", this.ds_yesno);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "addrController");
        	this.gfn_setCommon("METHODNM", "selectAddrInfo");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	if((this.gfn_isUpdate(this.ds_header) || this.gfn_isUpdate(this.ds_detail)) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				this.ds_detail.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		this.ds_detail.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchDetail = function()
        {
        	this.gfn_getScroll(this.gv_detail); // 스크롤 위치 저장
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.gv_detail.set_nodatatext("");
        	this.ds_detail.clearData();
        	
        	this.gfn_setCommon("BEANID", "addrController");
        	this.gfn_setCommon("METHODNM", "selectAddrInfoDetail");
        	
        	var sSvcId   = "selectDetail"+dtlsearchcnt++;
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_detail=OUT_rtnGrid ds_paramDetail=OUT_PARAM";
        	var sParam   = "addrid="+this.ds_header.getColumn(this.gv_nRow, "addrid");
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        // this.fn_save = function(sAll)
        // {
        // 
        // 	this.gfn_setCommon("BEANID", "addrController");
        // 	this.gfn_setCommon("METHODNM", "saveAddr");
        // 	
        //     var sSvcId   = "save";
        //     var sSvcUrl  = application.gv_ams + application.gv_saveUrl; 
        //     var sInData  = "IN_input_LIST=ds_header:U";
        //     var sOutData = "";
        //     var sParam   = "chkAll="+sAll
        // 					+" ctkey="+this.gfn_getUserInfo("ctKey");
        //     
        //     this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        // }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			this.ds_header.addColumn("CHK");
        			this.ds_header.addColumn("STATUS");
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId.substr(0, 12) == "selectDetail"){
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_detail);
        		this.searchFalg = "";
        	}
        // 	else if(sSvcId == "save"){
        // 		this.searchFalg = "save";
        // 		this.gfn_alert("MSG_ALERT_COMPLETE");
        // 		this.parent.div_cond.btn_search.click();
        // 	}else if(sSvcId == "saveDetail"){
        // 		this.searchFalg = "save";
        // 		this.gfn_alert("MSG_ALERT_COMPLETE");
        // 		this.fn_searchDetail();
        // 	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.rowChg = "Y";
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        
        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_detail.rowcount > 0){
        		this.gfn_sortDataset(obj, e);
        	}else if(colName == "CHK"){
        		this.ds_detail.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_detail.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("addrController/excelDownAddr.do", this.gv_header, this, oValue);
        }

        /* div_splitBottom_btn_excel_onclick 실행 */
        this.div_splitBottom_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        		addrid : this.ds_detail.getColumn(0, "addrid")
        	};
        	
        	this.parent.div_cond.fn_excelSearch("addrController/excelDownAddrDetail.do", this.gv_detail, this, oValue);
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        // this.div_splitTop_btn_delete_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        // {	
        // 	var selZipcd = this.ds_header.getColumn(this.ds_header.rowposition, 'delyn_desc');
        // 
        // 	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 	var nChkCnt = this.ds_header.getCaseCount("CHK == '1'");
        // 	var sAddr = this.ds_header.getColumn(nRow,"addrid");
        // 	
        // 	if(nRow == -1){
        // 		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        // 	}else{
        // 			if(selZipcd == '네'){
        // 			this.gfn_alert("ALRD_DELTED");
        // 			return;
        // 		};
        // 	
        // 		if(this.gfn_isNotNull(this.ds_detail.getColumn(0,"addrid"))){
        // 			this.gfn_confirm("해당 주소ID("+sAddr+") 는 이미 거점 정보가 존재 합니다. \n 그래도 계속 진행하시겠습니까?", "", function(msg, flag){
        // 				if(flag){
        // 					while(nRow != -1){
        // 						if(this.ds_header.getRowType(nRow) == "2"){
        // 							this.ds_header.deleteRow(nRow);
        // 							nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 						}else{
        // 							this.ds_header.setColumn(nRow, "STATUS", "D");
        // 							nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        // 						}
        // 					}
        // 					
        // 					this.div_splitTop_btn_save_onclick ();
        // 				}		
        // 			});
        // 		}else{
        // 			while(nRow != -1){
        // 				if(this.ds_header.getRowType(nRow) == "2"){
        // 					this.ds_header.deleteRow(nRow);
        // 					nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 				}else{
        // 					this.ds_header.setColumn(nRow, "STATUS", "D");
        // 					nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        // 				}
        // 			}	
        // 			this.div_splitTop_btn_save_onclick ();
        // 		}
        // 	}
        // }

        /* div_splitTop_btn_save_onclick 실행 */
        // this.div_splitTop_btn_save_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        // {
        // 	var dsCol = "zipNo|lnmAdres";
        // 	var nChkCnt = this.ds_header.getCaseCount("STATUS == 'D'");
        // 	
        // 	if(this.gfn_isUpdate(this.ds_header)){
        // 		if(nChkCnt == this.ds_header.rowcount){
        // 			var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        // 			var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        // 			var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        // 			
        // 			this.gfn_confirm3Btn (
        // 				arrBtnText,
        // 				sMsgCode,
        // 				sMsgText,
        // 				function(msg, flag) {
        // 					if (flag != "0") {
        // 						if (flag == "1") this.fn_save("");
        // 						if (flag == "2") this.fn_save("ALL");
        // 					} else { // 취소시 변경내용 reset
        // 						if(obj == null){
        // 							this.ds_header.reset();
        // 						}
        // 					}
        // 				}
        // 			);
        // 		}else{
        // 			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        // 				if(flag){
        // 					if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header, dsCol)){
        // 						this.fn_save("");
        // 					}
        // 				} else { // 취소시 변경내용 reset
        // 					if(obj == null){
        // 						this.ds_header.reset();
        // 					}
        // 				}
        // 			});
        // 		}
        // 	}else{
        // 		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        // 	}
        // }

        /* div_splitTop_btn_add_onclick 실행 */
        // this.div_splitTop_btn_add_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        // {
        // 	//this.ds_detail.clearData();
        // 	
        // 	var nRow = this.ds_header.addRow();
        // 	this.ds_header.setColumn(nRow, "addrsttcd", "1");
        // 	this.ds_header.setColumn(nRow, "STATUS", "C");
        // 	this.ds_header.setColumn(nRow, "CHK", 1);
        // 	this.ds_header.setColumn(nRow, "PAGING_NUM", nRow+1);
        // 	this.ds_header.setColumn(nRow,"lat",0);
        // 	this.ds_header.setColumn(nRow,"lon",0);
        // 	//this.ds_header.setColumn(nRow,"cntrycd","KOR");
        // 	this.ds_header.set_rowposition(nRow);
        // 	//this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "addrid"));
        // 	this.gv_header.setFocus();
        // }

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	 
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        				if(this.gfn_isNull(this.ds_header.getColumn(e.newrow,"addrid"))){
        					this.gv_header.setFocus();
        					
        				}else{
        					this.gv_header.setFocus();
        					this.fn_searchDetail();
        				}
        				
        		}
        	}
        	this.rowChg = "";
        	
        	
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	
        	var key = "";
        	var value = "";
        	this.gv_code = "";

        	if(e.columnid != "CHK"){

        	if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        /* ds_detail_cancolumnchange 실행 */
        this.ds_detail_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_detail_oncolumnchanged 실행 */
        this.ds_detail_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") obj.set_updatecontrol(true);
        	else if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        }

        /* div_splitBottom_grd_detail_oncellclick 실행 */
        this.div_splitBottom_grd_detail_oncellclick = function(obj,e)
        {
        	// 한 번 클릭으로 동작
        	obj.dropdownCombo();
        	obj.dropdownCalendar();
        }

        /* div_splitBottom_grd_detail_oncelldblclick 실행 */
        this.div_splitBottom_grd_detail_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        // this.div_splitTop_grd_header_onexpanddown = function(obj:Grid, e:nexacro.GridMouseEventInfo)
        // {
        // 	this.gv_Pvalue = "";
        // 
        // 	if(obj.getBindCellIndex("body", "cntrycd") == e.col){
        // 		var oArg = { divId:"GridMulti"
        // 					,searchId:1380
        // 					,putObj:this.ds_header
        // 					,putCol:"cntrycd|ctry"
        // 					,putRow:""+e.row
        // 				   };
        // 		this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        // 	} 
        // 
        // 	if(obj.getBindCellIndex("body", "zipNo") == e.col){
        // 		var oArg = { oRow:e.row
        // 				    ,putDs:this.ds_header
        // 				    ,putKey:"zipNo|lnmAdres"
        // 				    };	
        // 		this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        // 	
        // 	
        // 	}
        // 
        // }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_detail.addEventHandler("cancolumnchange", this.ds_detail_cancolumnchange, this);
            this.ds_detail.addEventHandler("oncolumnchanged", this.ds_detail_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_header.addEventHandler("onexpanddown", this.div_splitTop_grd_header_onexpanddown, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.div_splitBottom_grd_detail_oncelldblclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onsummclick", this.div_splitBottom_grd_detail_onsummclick, this);
            this.div_splitBottom.btn_excel.addEventHandler("onclick", this.div_splitBottom_btn_excel_onclick, this);
            this.Static03.addEventHandler("onclick", this.Static03_onclick, this);

        };

        this.loadIncludeScript("Address.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
