﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("notice");
                this.set_classname("notice");
                this.set_titletext("공지사항");
                this._setFormPosition(0,0,1230,560);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"FILE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"INSERTURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"UPDATEDATE\" type=\"STRING\" size=\"32\"/><Column id=\"MSG_NO\" type=\"STRING\" size=\"16\"/><Column id=\"MSG_TYPE_DESC\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE\" type=\"STRING\" size=\"32\"/><Column id=\"INSERTDATE\" type=\"STRING\" size=\"32\"/><Column id=\"CONTENT\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"START_DT\" type=\"STRING\" size=\"256\"/><Column id=\"END_DT\" type=\"STRING\" size=\"256\"/><Column id=\"TARGET\" type=\"STRING\" size=\"256\"/><Column id=\"PARENT_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"UPPER_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"OPEN_YN\" type=\"STRING\" size=\"256\"/><Column id=\"RIPPLE_CNT\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_YN\" type=\"STRING\" size=\"256\"/><Column id=\"FILE1\" type=\"STRING\" size=\"256\"/><Column id=\"FILE2\" type=\"STRING\" size=\"256\"/><Column id=\"FILE3\" type=\"STRING\" size=\"256\"/><Column id=\"MSG_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE_D\" type=\"STRING\" size=\"256\"/><Column id=\"URGRKEY\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG1\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG2\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG3\" type=\"STRING\" size=\"256\"/><Column id=\"UDYN\" type=\"STRING\" size=\"256\"/><Column id=\"HITS_CNT\" type=\"STRING\" size=\"256\"/><Column id=\"URNAME\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_noticegr", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_urgr", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"URGRKEY\" type=\"STRING\" size=\"256\"/><Column id=\"URGRNAME\" type=\"STRING\" size=\"256\"/><Column id=\"NM_COMPANY\" type=\"STRING\" size=\"256\"/><Column id=\"URNAME\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"GR_KEY\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_msgtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_copyheader", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"FILE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"INSERTURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"UPDATEDATE\" type=\"STRING\" size=\"32\"/><Column id=\"MSG_NO\" type=\"STRING\" size=\"16\"/><Column id=\"MSG_TYPE_DESC\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE\" type=\"STRING\" size=\"32\"/><Column id=\"INSERTDATE\" type=\"STRING\" size=\"32\"/><Column id=\"CONTENT\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"START_DT\" type=\"STRING\" size=\"256\"/><Column id=\"END_DT\" type=\"STRING\" size=\"256\"/><Column id=\"TARGET\" type=\"STRING\" size=\"256\"/><Column id=\"PARENT_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"UPPER_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"OPEN_YN\" type=\"STRING\" size=\"256\"/><Column id=\"RIPPLE_CNT\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_YN\" type=\"STRING\" size=\"256\"/><Column id=\"FILE1\" type=\"STRING\" size=\"256\"/><Column id=\"FILE2\" type=\"STRING\" size=\"256\"/><Column id=\"FILE3\" type=\"STRING\" size=\"256\"/><Column id=\"MSG_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE_D\" type=\"STRING\" size=\"256\"/><Column id=\"URGRKEY\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG1\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG2\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG3\" type=\"STRING\" size=\"256\"/><Column id=\"UDYN\" type=\"STRING\" size=\"256\"/><Column id=\"HITS_CNT\" type=\"STRING\" size=\"256\"/><Column id=\"URNAME\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_file", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"SEQ\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_ID\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_NM\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG\" type=\"STRING\" size=\"256\"/><Column id=\"BE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"BE_FILE_NM\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_fileView", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"F0\" type=\"STRING\" size=\"256\"/><Column id=\"F1\" type=\"STRING\" size=\"256\"/><Column id=\"F2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_filecopy", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"SEQ\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_ID\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_NM\" type=\"STRING\" size=\"256\"/><Column id=\"FILECHG\" type=\"STRING\" size=\"256\"/><Column id=\"BE_URL\" type=\"STRING\" size=\"256\"/><Column id=\"BE_FILE_NM\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_fileViewcopy", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"F0\" type=\"STRING\" size=\"256\"/><Column id=\"F1\" type=\"STRING\" size=\"256\"/><Column id=\"F2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detailcopy", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"URGRKEY\" type=\"STRING\" size=\"256\"/><Column id=\"URGRNAME\" type=\"STRING\" size=\"256\"/><Column id=\"NM_COMPANY\" type=\"STRING\" size=\"256\"/><Column id=\"URNAME\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"GR_KEY\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop01", "absolute", "0", "0", null, "229", "0", null, this);
            obj.set_taborder("17");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop01);
            obj.set_taborder("24");
            obj.set_scrollbars("none");
            obj.style.set_gradation("linear 0,0 #ffffffff 0,100 #fafafaff");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, "171", "0", null, this.div_splitTop01);
            obj.set_taborder("25");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_treeuseline("false");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"99\"/><Column size=\"198\"/><Column size=\"112\"/><Column size=\"398\"/><Column size=\"34\"/><Column size=\"40\"/><Column size=\"76\"/><Column size=\"60\"/><Column size=\"60\"/><Column size=\"82\"/><Column size=\"87\"/><Column size=\"130\"/><Column size=\"93\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"28\"/></Rows><Band id=\"head\"><Cell displaytype=\"normal\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"NOTICE_TYPE\"/><Cell col=\"3\" text=\"WRITEDATE\"/><Cell col=\"4\" text=\"SEQ\"/><Cell col=\"5\" text=\"NOTICE_TITLE\"/><Cell col=\"6\" text=\"ATTACHFILE\"/><Cell col=\"7\" text=\"NOTICEYN\"/><Cell col=\"8\" text=\"WRITEUR\"/><Cell col=\"9\" text=\"HITS\"/><Cell col=\"10\" text=\"RIPPLE_CNT\"/><Cell col=\"11\" text=\"NOTICE_START\"/><Cell col=\"12\" text=\"NOTICE_END\"/><Cell col=\"13\" text=\"UPDATEDATE\"/><Cell col=\"14\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:center;\" text=\"bind:MSG_TYPE_DESC\"/><Cell col=\"3\" displaytype=\"date\" style=\"align:center;\" text=\"bind:INSERTDATE\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"4\" style=\"align:center;\" text=\"bind:MSG_NO\"/><Cell col=\"5\" displaytype=\"normal\" style=\"align:left;\" text=\"bind:TITLE_D\"/><Cell col=\"6\" style=\"align:center middle;backgroundimage:EXPR(FILE_YN==&quot;Y&quot;?&quot;img::icon_file.png&quot;:&quot;&quot;);\"/><Cell col=\"7\" style=\"align:center;\" text=\"bind:OPEN_YN\"/><Cell col=\"8\" style=\"align:center middle;\" text=\"bind:URNAME\"/><Cell col=\"9\" style=\"align:right;\" text=\"bind:HITS_CNT\"/><Cell col=\"10\" style=\"align:right;\" text=\"bind:RIPPLE_CNT\"/><Cell col=\"11\" style=\"align:center middle;\" text=\"bind:START_DT\" mask=\"####-##-##\"/><Cell col=\"12\" style=\"align:center middle;\" text=\"bind:END_DT\" mask=\"####-##-##\"/><Cell col=\"13\" displaytype=\"date\" text=\"bind:UPDATEDATE\" mask=\"yyyy-MM-dd HH:mm:ss\"/><Cell col=\"14\" style=\"align:center;\" text=\"bind:UPDATEURKEY\"/></Band></Format></Formats>");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "147", null, this.div_splitTop01);
            obj.set_taborder("26");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "73", null, this.div_splitTop01);
            obj.set_taborder("27");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "36", null, this.div_splitTop01);
            obj.set_taborder("28");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "110", null, this.div_splitTop01);
            obj.set_taborder("29");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Static("Noticemanage01", "absolute", "3", "1", "117", "20", null, null, this.div_splitTop01);
            obj.set_taborder("30");
            obj.set_text("◈공지 사항");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop01.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("18");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "194", "327", "511", "132", null, null, this);
            obj.set_taborder("19");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "194", "296", null, "31", "524", null, this);
            obj.set_taborder("20");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Noticemanage02", "absolute", "0", "235", "194", "20", null, null, this);
            obj.set_taborder("21");
            obj.set_text("◈공지 내용");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "194", "460", "512", "31", null, null, this);
            obj.set_taborder("22");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "0", "460", "193", "95", null, null, this);
            obj.set_taborder("23");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "0", "264", "194", "31", null, null, this);
            obj.set_taborder("24");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static07", "absolute", "0", "328", "194", "131", null, null, this);
            obj.set_taborder("25");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_notice_title", "absolute", "200", "299", "506", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_maxlength("200");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("CLEAR01", "absolute", "686", "464", "21", "19", null, null, this);
            obj.set_taborder("13");
            obj.style.set_image("URL('img::btn_delete_on.png')");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new TextArea("edt_notice_content", "absolute", "200", "329", "506", "127", null, null, this);
            obj.set_taborder("4");
            obj.set_maxlength("3000");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "194", "492", "512", "31", null, null, this);
            obj.set_taborder("26");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("CLEAR02", "absolute", "686", "496", "22", "19", null, null, this);
            obj.set_taborder("14");
            obj.style.set_image("URL('img::btn_delete_on.png')");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "194", "524", "512", "31", null, null, this);
            obj.set_taborder("27");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("CLEAR03", "absolute", "686", "528", "22", "19", null, null, this);
            obj.set_taborder("15");
            obj.style.set_image("URL('img::btn_delete_on.png')");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Noticemanage04", "absolute", "728", "235", "194", "20", null, null, this);
            obj.set_taborder("28");
            obj.set_text("◈공지 대상");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "728", "262", "71", "24", null, null, this);
            obj.set_taborder("29");
            obj.set_text("대상 선택");
            obj.style.set_color("#ff5a00ff");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_noticegr", "absolute", "829", "264", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_innerdataset("@ds_noticegr");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");

            obj = new Static("Static11", "absolute", "968", "264", "114", "24", null, null, this);
            obj.set_taborder("31");
            obj.set_text("사용자 그룹");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_urgr", "absolute", "1085", "264", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("16");
            obj.set_innerdataset("@ds_urgr");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");

            obj = new Button("btn_right", "absolute", "1141", "231", "75", "28", null, null, this);
            obj.set_taborder("33");
            obj.set_text("Load");
            obj.set_enable("false");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this);
            obj.set_taborder("34");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "0", "296", "194", "31", null, null, this);
            obj.set_taborder("35");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "194", "264", "512", "31", null, null, this);
            obj.set_taborder("38");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_start_dt", "absolute", "200", "268", "117", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("1");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_enable("false");
            obj.set_value("null");

            obj = new Calendar("cal_end_dt", "absolute", "355", "268", "117", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_enable("false");
            obj.set_value("null");

            obj = new Static("lab_title01", "absolute", "331", "269", "27", "20", null, null, this);
            obj.set_taborder("41");
            obj.set_text("~");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new CheckBox("edt_openYn", "absolute", "671", "235", "18", "18", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("false");
            obj.set_truevalue("Y");
            obj.set_falsevalue("N");
            this.addChild(obj.name, obj);

            obj = new Static("Noticemanage03", "absolute", "608", "235", "64", "20", null, null, this);
            obj.set_taborder("43");
            obj.set_text("공지 여부");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop02", "absolute", "728", "294", null, "261", "0", null, this);
            obj.set_taborder("45");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "0", null, "261", "1", null, this.div_splitTop02);
            obj.set_taborder("32");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_enable("false");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"27\" band=\"left\"/><Column size=\"33\"/><Column size=\"121\"/><Column size=\"135\"/><Column size=\"144\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"28\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" text=\"NO\"/><Cell col=\"3\" text=\"URGRNAME\"/><Cell col=\"4\" text=\"COMPANY_NM\"/><Cell col=\"5\" text=\"URNAME\"/></Band><Band id=\"body\"><Cell style=\"background: ;backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"padding: ;\" expr=\"currow+1\" calendardisplaynulltype=\"none\"/><Cell col=\"3\" style=\"align: ;\" text=\"bind:URGRNAME\"/><Cell col=\"4\" displaytype=\"normal\" style=\"align:left;\" text=\"bind:NM_COMPANY\"/><Cell col=\"5\" text=\"bind:URNAME\"/></Band></Format></Formats>");
            this.div_splitTop02.addChild(obj.name, obj);

            obj = new Button("btn_reply", "absolute", "602", "264", "99", "28", null, null, this);
            obj.set_taborder("47");
            obj.set_text("댓글 달기");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_msgtype", "absolute", "469", "234", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("0");
            obj.set_innerdataset("ds_msgtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");

            obj = new Edit("edt_file", "absolute", "201", "464", "343", "22", null, null, this);
            obj.set_taborder("55");
            obj.set_maxlength("20");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_file02", "absolute", "201", "496", "343", "22", null, null, this);
            obj.set_taborder("58");
            obj.set_maxlength("20");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_file03", "absolute", "201", "528", "343", "22", null, null, this);
            obj.set_taborder("60");
            obj.set_maxlength("20");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Noticemanage00", "absolute", "400", "235", "69", "20", null, null, this);
            obj.set_taborder("64");
            obj.set_text("◈유형");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_upload03", "absolute", "616", "527", "60", "25", null, null, this);
            obj.set_taborder("12");
            obj.set_text("업로드");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_upload02", "absolute", "616", "494", "60", "25", null, null, this);
            obj.set_taborder("10");
            obj.set_text("업로드");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_upload01", "absolute", "617", "463", "60", "25", null, null, this);
            obj.set_taborder("8");
            obj.set_text("업로드");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new FileDownload("FileDownload01", "absolute", "547", "463", "60", "25", null, null, this);
            obj.set_taborder("7");
            obj.getSetter("retry").set("0");
            obj.set_text("다운로드");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #ccccccff");
            obj.style.set_color("#555555ff");
            obj.style.set_bordertype("normal 0 0");
            obj.style.set_cursor("hand");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new FileDownload("FileDownload02", "absolute", "547", "494", "60", "25", null, null, this);
            obj.set_taborder("9");
            obj.getSetter("retry").set("0");
            obj.set_text("다운로드");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #ccccccff");
            obj.style.set_color("#555555ff");
            obj.style.set_bordertype("normal 0 0");
            obj.style.set_cursor("hand");
            this.addChild(obj.name, obj);

            obj = new FileDownload("FileDownload03", "absolute", "547", "527", "60", "25", null, null, this);
            obj.set_taborder("11");
            obj.getSetter("retry").set("0");
            obj.set_text("다운로드");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #ccccccff");
            obj.style.set_color("#555555ff");
            obj.style.set_bordertype("normal 0 0");
            obj.style.set_cursor("hand");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 229, this.div_splitTop01,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("17");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop01.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 261, this.div_splitTop02,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("45");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop02.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 560, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("공지사항");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","Noticemanage02","text","gds_lang","NOTICE_CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ATTACH FILE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","NOTICE_TERM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static07","text","gds_lang","NOTICE_CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_notice_title","value","ds_header","TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_notice_content","value","ds_header","CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Noticemanage04","text","gds_lang","NOTICETARGET");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","Static10","text","gds_lang","TARGET_SELECT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","Static11","text","gds_lang","USER_GROUPS");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","div_splitTop.Static06","text","gds_lang","NOTICEBOARD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","static12","text","gds_lang","NOTICE_TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","cal_start_dt","value","ds_header","START_DT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","cal_end_dt","value","ds_header","END_DT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Noticemanage03","text","gds_lang","NOTICEYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_reply","text","gds_lang","RIPPLE_WRITE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_openYn","value","ds_header","OPEN_YN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","cbo_noticegr","value","ds_header","TARGET");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","cbo_msgtype","value","ds_header","MSG_TYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","cbo_urgr","value","ds_header","URGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","edt_file","value","ds_fileView","F0");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item30","edt_file02","value","ds_fileView","F1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","edt_file03","value","ds_fileView","F2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","Noticemanage00","text","gds_lang","NOTICE_TYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","btn_upload03","text","gds_lang","UPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","btn_upload02","text","gds_lang","UPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","btn_upload01","text","gds_lang","UPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","FileDownload01","text","gds_lang","DOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","FileDownload02","text","gds_lang","DOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","FileDownload03","text","gds_lang","DOWN");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("NoticeManage.xfdl", "lib::Comm.xjs");
        this.registerScript("NoticeManage.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : notice.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 공지사항  
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;
        this.gv_self = ""; //수정시 본인클릭
        this.gv_header = this.div_splitTop01.grd_header;
        this.gv_detail = this.div_splitTop02.grd_detail;
        this.gv_page = this.div_splitTop01.div_Paging;
        this.gv_addRow = false;
        this.gv_rowPix = true;
        this.gv_nRow = 0;
        this.gv_urgr = "";//개인그룹키
        this.searchFalg = "";
        this.rowChg = "Y"; 
        var dtlsearchcnt = 0;
        this.ds_header.set_updatecontrol(false);
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {	
        	this.gfn_getCode("PTL_NOTICE_TG_GB", this.ds_noticegr, "");
        	this.gfn_getCode("PTL_URGR", this.ds_urgr, "");
        	this.gfn_getCode("MSG_TYPE", this.ds_msgtype, "");
        	this.ds_urgr.filter("field1 != '*'&&field1 != 'USER'");
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        // 	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        // 	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        // 	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        //	var gridMenuSet = ["grd_header^1|0|0|0|0|0|1|1", "grd_detail^0|0|0|0|0|0|1|1"];
        	this.gv_grdList = [this.gv_header,this.gv_detail]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "selectNoticeInfoList");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
        	
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        								
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        				
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        		
        	}
        }

        this.fn_searchDetail = function()
        {
        	var nRow = this.ds_header.rowposition;	
        	this.ds_detail.clearData();
        	this.gfn_setCommon("BEANID", "boardController");	
        	var sParam ="";	
        	if(this.cbo_noticegr.value == "3" && this.cbo_urgr.index == 0){
        	 return;
        	}
        	if(this.cbo_noticegr.value == "3"){
        		this.gfn_setCommon("METHODNM", "selectNoticeUrGrList");
        		sParam   =  "urgr="+this.gv_urgr +  " MSG_NO="+this.ds_header.getColumn(nRow, "MSG_NO");
        	}
        	else{
        		sParam   =  "MSG_NO="+this.ds_header.getColumn(nRow, "MSG_NO");
        		this.gfn_setCommon("METHODNM", "selectNoticeGrList");
        	}
            var sSvcId   = "selectDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid";
            
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_searchFile = function()
        {
        	var nRow = this.ds_header.rowposition;	
        	this.ds_file.clearData();
        	this.ds_fileView.clearData();
        	this.ds_file.addRow();
        	this.ds_file.addRow();
        	this.ds_file.addRow();
        	this.ds_fileView.addRow();
        	this.gfn_setCommon("BEANID", "boardController");	
        	this.gfn_setCommon("METHODNM", "selectFileList");

        	var sParam   =  "MSG_NO="+this.ds_header.getColumn(nRow, "MSG_NO");
        	
            var sSvcId   = "selectFile";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_file=OUT_rtnGrid";
            
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_save = function()
        {
           
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "saveNotice");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_input_LIST=ds_header:U IN_gr_LIST=ds_detail IN_file_LIST=ds_file";
            var sOutData = "";
        	var sParam   = "";
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        this.fn_enable = function(obj)
        {
        	this.cal_start_dt.set_enable(obj);
        	this.cal_end_dt.set_enable(obj);
        	this.edt_notice_title.set_enable(obj);
        	this.edt_notice_content.set_enable(obj);
        	this.btn_upload01.set_enable(obj);
        	this.btn_upload02.set_enable(obj);
        	this.btn_upload03.set_enable(obj);
        	this.CLEAR01.set_enable(obj);
        	this.CLEAR02.set_enable(obj);
        	this.CLEAR03.set_enable(obj);
        	this.cbo_noticegr.set_enable(obj);
        	this.cbo_msgtype.set_enable(obj);
        	this.div_splitTop02.grd_detail.set_enable(obj);
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "PARENT_MSG_NO") != "0"&&this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") != "C"){
        		this.edt_openYn.set_enable(false);
        		this.ds_header.setColumn(this.ds_header.rowposition, "OPEN_YN","Y");
        	}
        	else{
        		this.edt_openYn.set_enable(obj);
        	}

        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_constDsSet(this.gv_header);
        	
        		if(this.ds_header.rowcount > 0){
        			for(var i= 0; i <this.ds_header.rowcount; i++){		
        				if(this.ds_header.getColumn(i, "FILE_URL")==undefined){
        					this.ds_header.setColumn(i,"FILE_URL","");
        				}
        			}
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			this.ds_header.setRowType(this.ds_header.rowposition,1);
        		    this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","");
        		    this.cbo_urgr.set_enable(false);
        			this.btn_right.set_enable(false);
        			this.fn_enable(false);
        			this.btn_reply.set_enable(true);

        		
        			this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        			
        			this.ds_copyheader.mergeData(this.ds_header);
        			if(this.ds_header.getColumn(this.ds_header.rowposition, "TARGET") != "1"){
        				this.gv_urgr = this.ds_header.getColumn(this.ds_header.rowposition, "URGRKEY");
        				
        					//디테일 컬럼 사이즈 조정
        		
        				if(this.cbo_noticegr.value == "3"){
        					this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        					this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",135);
        					this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",135);
        				}
        				else if(this.cbo_noticegr.value == "2"){
        					this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",135);
        					this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        					this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);

        				}
        				else{
        					this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        					this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        					this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);
        				}
        				this.fn_searchDetail();
        				this.fn_searchFile();
        			}else{
        				this.ds_detail.clearData();
        				this.fn_searchFile();
        			}
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        	}else if(sSvcId == "selectDetail"){
        		//this.gfn_constDsSet(this.gv_detail);
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}else{
        					
        		}
        	}
        	else if(sSvcId == "selectFile"){
        		for(var i= 0; i <this.ds_file.rowcount; i++){		
        			if(i==0)this.ds_fileView.setColumn(0,"F0",this.ds_file.getColumn(0,"FILE_NM"));
        			if(i==1)this.ds_fileView.setColumn(0,"F1",this.ds_file.getColumn(1,"FILE_NM"));
        			if(i==2)this.ds_fileView.setColumn(0,"F2",this.ds_file.getColumn(2,"FILE_NM"));
        		}
        	}
        	else if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.ds_copyheader.clearData();
        		this.parent.div_cond.btn_search.click();
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop01_grd_header_onheadclick = function(obj,e)
        {
        	//var colName = this.gfn_gridBindName(obj, e.cell);
        //	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        //	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        //		this.rowChg = "Y";
        //		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        //		this.parent.div_cond.btn_search.click();
        //	}
        	
        }

        
        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop01_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") == "C"){
        			this.gfn_confirm("NOTICE_DELETE", "", function(msg, flag){
        				if(flag){
        					this.ds_detail.clearData();
        					this.ds_file.clearData();
        					this.ds_fileView.clearData();
        					this.ds_header.deleteRow(this.ds_header.rowposition);		
        					this.fn_enable(false);
        				}else{

        				}
        			});
        		}else{
        			this.ds_header.setColumn(this.ds_header.rowposition, "STATUS", "D");
        			this.ds_header.setRowType(this.ds_header.rowposition,Dataset.ROWTYPE_UPDATE);
        			this.div_splitTop01_btn_save_onclick ();
        		}
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop01_btn_add_onclick = function(obj,e)
        {
        	
        	var nRow = this.ds_header.findRowExpr("STATUS == 'C'");

        	if(nRow != -1)return;
        		
        	this.cbo_msgtype.set_index(1);
        	this.gv_addRow =true;
        	this.ds_header.copyRow(this.ds_header.rowposition,this.ds_copyheader,this.ds_header.rowposition);
        	this.ds_header.setRowType(this.ds_header.rowposition,2);
        	this.gv_self = "";
        	this.gv_header.setFocus();
        	nRow = this.ds_header.addRow();
        	this.ds_header.setColumn(nRow, "CHK", 1);
        	this.ds_header.setColumn(nRow, "STATUS", "C");	
        	this.ds_header.set_rowposition(nRow);
        	//this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "mename"));
        	
        	this.btn_reply.set_enable(false);
        	this.fn_enable(true);	
        	this.ds_file.clearData();
        	this.ds_fileView.clearData();
        	this.ds_file.addRow();
        	this.ds_file.addRow();
        	this.ds_file.addRow();
        	this.ds_fileView.addRow();
        	this.ds_fileView.setColumn(0,"F0","");
        	this.ds_fileView.setColumn(0,"F2","");
        	this.ds_fileView.setColumn(0,"F3","");
        	this.ds_detail.clearData();
        	this.cbo_msgtype.setFocus();
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop01_btn_update_onclick = function(obj,e)
        {
        	var userGrKey = this.gfn_getUserInfo("urGrKey");
        	var userId = this.gfn_getUserInfo("urKey");
        	var iUrKey = this.ds_header.getColumn(this.ds_header.rowposition, "INSERTURKEY");
        	if(this.ds_header.rowcount > 0){
        		if(userGrKey == "SYSADM" || userId == iUrKey){
        			this.btn_reply.set_enable(false);
        			this.fn_enable(true);
        			//this.btn_right.set_enable(true);
        			this.ds_header.setColumn(this.ds_header.rowposition, "STATUS", "U");
        			if(this.cbo_noticegr.value == "3"){
        				this.cbo_urgr.set_enable(true);
        				this.btn_right.set_enable(true);
        			}
        		}
        	}
        }

        
        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop01_btn_save_onclick = function(obj,e)
        {
        	

        	//var dsObj = this.ds_header;
        	var dsCol = "TITLE|CONTENT|START_DT|END_DT";
        	//var sComp = "edt_notice_title|edt_notice_content|cal_start_dt|cal_end_dt";
        	//trace(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp));
        	

        	if(this.gfn_isUpdate(this.ds_header)||this.gfn_isUpdate(this.ds_detail)){
        	var objDate = new Date();
        	

        		if((this.ds_header.getColumn(this.ds_header.rowposition, "TARGET") == "2"||this.ds_header.getColumn(this.ds_header.rowposition, "TARGET") == "3") && this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") != "D"){	
        			var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        		
        			if(nRow == -1){
        				this.gfn_alert("MSG_CHKDATA_NOSELECT");
        				return;
        			}
        		}	
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "UDYN") != "Y" && this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") != "C"){
        				
        				this.gfn_alert("MSG_CHK_UPDATE_STATUS");
        				return;
        		}
        		if(this.dateChk(this.cal_end_dt.value,Eco.date.getMaskFormatString(objDate, "yyyyMMdd"),"<")){
        			this.gfn_alert("MSG_NOT_DATE");
        			return;
        		}
        		if(!this.edt_notice_title.enable&&this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") != "D")return; //신규,수정,삭제 상태만 저장가능
        		
        			if(!this.fn_nullChk(this.cbo_msgtype)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("MSGTYPE"));
        			}else if(!this.fn_nullChk(this.cal_start_dt)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("NOTICE_START"));
        			}else if(!this.fn_nullChk(this.cal_end_dt)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("NOTICE_END"));
        			}else if(!this.fn_nullChk(this.edt_notice_title)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("NOTICE_TITLE"));
        			}else if(!this.fn_nullChk(this.edt_notice_content)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("NOTICE_CONTENT"));
        			}else if(!this.fn_nullChk(this.cbo_noticegr)){
        				this.gfn_alert("MSG_WRITE_INPUT","ITEM,"+this.gfn_getLang("NOTICETARGET"));
        			}else{
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				
        					if(flag){
        								if(this.ds_header.getColumn(nRow, "STATUS") ==""){
        									this.ds_header.setRowType(this.ds_header.rowposition,4);
        									this.ds_header.setColumn(nRow, "STATUS","U");
        								}
        								this.fn_save();
        					} else { 
        						if (obj == null ){ //삭제 취소시 변경내역 reset
        							this.ds_header.reset();
        							this.ds_header.setColumn(this.ds_header.rowposition, "STATUS", "");
        						}
        					}
        				});
        		
        			}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        
        /* ds_detail_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        	//	obj.set_updatecontrol(false);
        		
        	//	trace("B"+this.ds_header.getRowType(e.row)+"="+e.row);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        
        this.div_splitTop01_grd_header_oncellclick = function(obj,e)
        {
        	this.ds_fileViewcopy.clearData();
        	this.ds_detailcopy.clearData();
        	this.ds_filecopy.clearData();
        	this.ds_detailcopy.mergeData(this.ds_detail);		
        	this.ds_filecopy.mergeData(this.ds_file);		
        	this.ds_fileViewcopy.mergeData(this.ds_fileView);			

        	
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "PARENT_MSG_NO") !="0"){
        	
        	}
        	
        	if(this.gv_self == this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO")){
        		this.gv_self = this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO");
        	}
        	else if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") == "C"){
        		this.ds_detail.clearData();
        		
        		this.gv_self = "";
        		this.btn_reply.set_enable(false);
        		this.fn_enable(true);
        	}
        	else{
        		this.ds_detail.clearData();
        		
        		this.gv_self = this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO");
        		this.btn_reply.set_enable(true);
        		this.fn_enable(false);
        		this.cbo_urgr.set_enable(false);
        		this.btn_right.set_enable(false);
        	}

        	var colName = this.gfn_gridBindName(obj, e.cell);
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClickChangeRowType(obj);
        	} 
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "TARGET") == "1"||this.cbo_noticegr.value == undefined||this.cbo_noticegr.value == ""||(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") =="C" && this.cbo_noticegr.value != undefined&&this.cbo_noticegr.value != "")){
        		this.ds_detail.clearData();
        	}else{
        		this.gv_urgr = this.ds_header.getColumn(this.ds_header.rowposition, "URGRKEY");
        		this.fn_searchDetail();
        	}
        	
        	//디테일 컬럼 사이즈 조정
        	
        	if(this.cbo_noticegr.value == "3"){
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",135);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",135);
        	}
        	else if(this.cbo_noticegr.value == "2"){
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",135);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);

        	}
        	else{
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);
        	}
        	this.fn_searchFile();
        }
        //2번그리드 사이즈 조정과 사용자 그룹 활성화
        this.cbo_icgrkey_onitemchanged = function(obj,e)
        {
            
            this.ds_detail.clearData();
        	if(this.cbo_noticegr.value == "3"){
        	    this.btn_right.set_enable(true);
        		this.cbo_urgr.set_enable(true);
        		
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",135);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",135);
        	}
        	else if(this.cbo_noticegr.value == "2"){
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",135);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);
        		this.cbo_urgr.set_index(0);
        		this.cbo_urgr.set_enable(false);
        		this.btn_right.set_enable(true);
        	}
        	else{
        		this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        		this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);
        		this.cbo_urgr.set_index(0);
        		this.cbo_urgr.set_enable(false);
        		this.btn_right.set_enable(false);
        	}
        	if(this.cbo_noticegr.value != "1"&&!this.gfn_isNull(this.cbo_noticegr.value)){
        		this.fn_searchDetail();
        	}
        }

        

        
        this.btn_reply_onclick = function(obj,e)
        {
        	var objDate = new Date();
        	if(this.dateChk(this.cal_end_dt.value,Eco.date.getMaskFormatString(objDate, "yyyyMMdd"),"<")){
        			this.gfn_alert("MSG_NOTICE_DATE");
        			return;
        	}
        	if(this.ds_header.rowcount > 0){
        		var parentMsgNo = this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO");
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "PARENT_MSG_NO") != "0"){
        			parentMsgNo = this.ds_header.getColumn(this.ds_header.rowposition, "PARENT_MSG_NO");
        		}
        		var oArg = { argFlag:"U"
        					,menuId:this.parent.gv_menuId
        					,parent_msg_no:parentMsgNo
        					,msg_no:this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO")
        					,msg_type:this.ds_header.getColumn(this.ds_header.rowposition, "MSG_TYPE")
        					,start_dt:this.ds_header.getColumn(this.ds_header.rowposition, "START_DT")
        					,end_dt:this.ds_header.getColumn(this.ds_header.rowposition, "END_DT")
        					,target:this.ds_header.getColumn(this.ds_header.rowposition, "TARGET")
        				   };
        		this.gfn_popup("noticePop", "master::NoticeRepplePop.xfdl", oArg, 600, 550, "");

        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}
        }

        /***********************************************************************************
        * FILE 파일 관련 컨트롤
        ***********************************************************************************/

        this.fileSetup = function(obj,fileIndex)
        {
        	//trace(obj.fileName);
        	//trace("obj.saveFile"+obj.fileid);
        	
        	//trace("ds_file"+this.ds_file.rowcount);
        	//trace("ds_fileView"+this.ds_fileView.rowcount);
        	
        	this.ds_file.setColumn(fileIndex,"MSG_NO",this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO"));
        	this.ds_file.setColumn(fileIndex,"FILE_ID",obj.fileid);
        	this.ds_file.setColumn(fileIndex,"FILE_NM",this.fn_clearFilePath(obj.fileName));
        	this.ds_fileView.setColumn(0,"F"+fileIndex,this.fn_clearFilePath(obj.fileName));
        	this.ds_file.setColumn(fileIndex,"FILECHG","Y");	
        	trace("첨부"+this.ds_header.getColumn(this.ds_header.rowposition, "STATUS"));
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") !="C"){
        		this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","U");
        		this.ds_header.setRowType(this.ds_header.rowposition,4);
        	}
        	else{
        		this.ds_header.setRowType(this.ds_header.rowposition,2);
        	}
        }
        var sCall = this;
        this.btn_upload01_onclick = function(obj,e)
        {
        	//if(this.ds_header.rowcount > 0){
        //		var parentMsgNo = this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO");
        	//	var oArg = {index:0};
        	//	this.gfn_popup("noticeFilePop", "master::NoticeFileUploadPop.xfdl", oArg, 422, 41, "");
        	//}else{
        	//	this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	//}

        	
        	
        	var fn_uploadCallback = function (oResponseData){
        		sCall.fileSetup(oResponseData,0);
        		return;
        	}

           this.gtn_uploadPopShow(null , "/boardController/fileUpload.do" , fn_uploadCallback);
        }

        this.btn_upload02_onclick = function(obj,e)
        {
        	var fn_uploadCallback = function (oResponseData){
        		sCall.fileSetup(oResponseData,1);
        		return;
        	}

           this.gtn_uploadPopShow(null , "/boardController/fileUpload.do" , fn_uploadCallback);
        }

        this.btn_upload03_onclick = function(obj,e)
        {
        	var fn_uploadCallback = function (oResponseData){
        		sCall.fileSetup(oResponseData,2);
        		return;
        	}

           this.gtn_uploadPopShow(null , "/boardController/fileUpload.do" , fn_uploadCallback);
        }
        this.FileDownload01_onclick = function(obj,e)
        {
        	if (this.ds_file.getColumn(0,"FILE_URL")!="") {
        	
        		var path = this.ds_file.getColumn(0,"FILE_URL");		

        		var filename = this.ds_file.getColumn(0,"FILE_NM");
        		var fileid = this.ds_file.getColumn(0,"FILE_ID");
        		var sUrl = application.services["svc"].url+application.gv_ams + "/boardController/fileDown.do";
        		
        		if(filename == "" || filename==null){
        			this.gfn_alert("MSG_CHKFILE");
        		}
        		else{
        			this.FileDownload01.set_downloadurl(sUrl+"?filename="+filename+"&fileid="+fileid+"&fileurl="+path+"/");
        		}
        	}
        }

        this.FileDownload02_onclick = function(obj,e)
        {
        	if (this.ds_file.getColumn(1,"FILE_URL")!="") {
        	
        		var path = this.ds_file.getColumn(1,"FILE_URL");		

        		var filename = this.ds_file.getColumn(1,"FILE_NM");
        		var fileid = this.ds_file.getColumn(1,"FILE_ID");
        		var sUrl = application.services["svc"].url+application.gv_ams + "/boardController/fileDown.do";
        		
        		if(filename == "" || filename==null){
        			this.gfn_alert("MSG_CHKFILE");
        		}
        		else{
        			this.FileDownload02.set_downloadurl(sUrl+"?filename="+filename+"&fileid="+fileid+"&fileurl="+path+"/");
        		}
        	}
        }

        this.FileDownload03_onclick = function(obj,e)
        {
        	if (this.ds_file.getColumn(2,"FILE_URL")!="") {
        	
        		var path = this.ds_file.getColumn(2,"FILE_URL");		

        		var filename = this.ds_file.getColumn(2,"FILE_NM");
        		var fileid = this.ds_file.getColumn(2,"FILE_ID");
        		var sUrl = application.services["svc"].url+application.gv_ams + "/boardController/fileDown.do";
        		
        		if(filename == "" || filename==null){
        			this.gfn_alert("MSG_CHKFILE");
        		}
        		else{
        			this.FileDownload03.set_downloadurl(sUrl+"?filename="+filename+"&fileid="+fileid+"&fileurl="+path+"/");
        		}
        	}
        }

        
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") {
        	//	obj.set_updatecontrol(true);
        		//trace("F"+this.ds_header.getRowType(e.row)+"="+e.row);
        	}
        	else if(e.columnid != "STATUS" && e.columnid != "") { 
        		this.ds_header.setRowType(this.ds_header.rowposition,4);
        		this.gfn_statusChk(obj, e.row); 
        	}
        }

        
        this.div_splitTop02_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        this.div_splitTop02_grd_detail_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.ds_detail.setColumn(this.ds_detail.rowposition, "STATUS","U");
        		this.ds_detail.setRowType(this.ds_detail.rowposition,4);
        		
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") !="C"){
        			this.ds_header.setRowType(this.ds_header.rowposition,4);
        			this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","U");
        		}
        	}
        	
        	//var nRow = this.ds_detail.findRow("CHK", "1");
        	//alert(this.ds_detail.getColumn(this.ds_detail.rowposition, "CHK"));
        	//var colName = this.gfn_gridBindName(obj, e.cell);
        	//if(colName == "CHK"&&this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") != "C"){
        		
        	//}
        }

        this.fn_clearFilePath = function(val){
            var tmpStr = val;
            
            var cnt = 0;
            while(true){
                cnt = tmpStr.indexOf("/");
                if(cnt == -1) break;
                tmpStr = tmpStr.substring(cnt+1);
            }
            while(true){
                cnt = tmpStr.indexOf("\\");
                if(cnt == -1) break;
                tmpStr = tmpStr.substring(cnt+1);
            }
            
            return tmpStr;
        }
        this.cbo_urgr_onitemchanged = function(obj,e)
        {
        	this.gv_urgr = this.cbo_urgr.value;
        	this.fn_searchDetail();
        }

        this.CLEAR01_onclick = function(obj,e)
        {
        	this.ds_file.setColumn(0,"MSG_NO",this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO"));
        	this.ds_file.setColumn(0,"FILE_ID","");
        	this.ds_file.setColumn(0,"FILE_NM","");
        	this.ds_fileView.setColumn(0,"F0","");
        	this.ds_file.setColumn(0,"FILECHG","Y");
        	this.ds_header.setRowType(this.ds_header.rowposition,4);
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") !="C"){
        		this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","U");
        	}
        }

        this.CLEAR02_onclick = function(obj,e)
        {
        	this.ds_file.setColumn(1,"MSG_NO",this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO"));
        	this.ds_file.setColumn(1,"FILE_ID","");
        	this.ds_file.setColumn(1,"FILE_NM","");
        	this.ds_fileView.setColumn(0,"F1","");
        	this.ds_file.setColumn(1,"FILECHG","Y");
        	this.ds_header.setRowType(this.ds_header.rowposition,4);
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") !="C"){
        		this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","U");
        	}
        }

        this.CLEAR03_onclick = function(obj,e)
        {
        	this.ds_file.setColumn(2,"MSG_NO",this.ds_header.getColumn(this.ds_header.rowposition, "MSG_NO"));
        	this.ds_file.setColumn(2,"FILE_ID","");
        	this.ds_file.setColumn(2,"FILE_NM","");
        	this.ds_fileView.setColumn(0,"F2","");
        	this.ds_file.setColumn(2,"FILECHG","Y");
        	this.ds_header.setRowType(this.ds_header.rowposition,4);
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "STATUS") !="C"){
        		this.ds_header.setColumn(this.ds_header.rowposition, "STATUS","U");
        	}
        }

        this.div_splitTop01_btn_excel_onclick = function(obj,e)
        {

        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("boardController/excelDown.do", this.gv_header, this, oValue);

        }

        this.cbo_msgtype_onitemchanged = function(obj,e)
        {
        	//if(this.cbo_msgtype.value == 1){
        		
        		
        	//}
        	this.cbo_noticegr.set_index(1);
        	this.btn_right.set_enable(false);
        	this.cbo_urgr.set_enable(false);
        	this.cbo_urgr.set_index(0);
        	this.ds_detail.clearData();
        }

        this.fn_nullChk = function(obj){
        	if(obj.value =="" || obj.value == undefined){
        		eval(obj).setFocus();
        		return false;
        	}else{
        		return true;
        	}
        }

        this.ds_header_onrowposchanged = function(obj,e)
        {
        //trace("들어온다:"+ e.newrow);
        	//if(this.gv_rowPix){
        	//전 로우가 수정중이었으면
        		if(this.ds_header.getRowType(e.oldrow) == 4){
        			this.gfn_confirm("NOTICE_EDT_STOP", "", function(msg, flag){
        				if(flag){
        					if(this.gfn_isUpdate(this.ds_header)&&this.ds_header.getColumn(e.oldrow, "STATUS") != "C"){
        						trace("로우수정 후 전 로우 수정 안해서 인서트:"+ e.oldrow);
        						this.ds_header.copyRow(e.oldrow,this.ds_copyheader,e.oldrow);
        						this.ds_header.setRowType(e.oldrow,1);
        						this.ds_header.setColumn(e.oldrow,"CHK",0);
        						this.ds_header.setColumn(e.oldrow, "STATUS","");
        					
        					}else if(this.ds_header.getColumn(e.oldrow, "STATUS") == "C"){
        						//if(!this.gv_addRow)this.ds_header.deleteRow(this.ds_header.rowposition);	
        						this.ds_header.deleteRow(e.oldrow);
        					}
        					this.gv_addRow = false;
        				}else{
        					trace("로우수정 후 전 로우 수정함 현 선택로우 상태 되돌리기:"+ e.newrow);
        					this.ds_header.setColumn(e.newrow, "STATUS","");
        					this.ds_header.setColumn(e.newrow,"CHK",0);
        					this.ds_header.setRowType(e.newrow,1);
        					this.ds_header.set_rowposition(e.oldrow);
        					this.gv_header.onsetfocus
        					this.fn_enable(true);
        					this.ds_detail.clearData();
        					this.ds_file.clearData();
        					this.ds_fileView.clearData();
        					this.ds_detail.mergeData(this.ds_detailcopy);
        					this.ds_file.mergeData(this.ds_filecopy);
        					this.ds_fileView.mergeData(this.ds_fileViewcopy);
        					
        					//디테일 컬럼 사이즈 조정
        					
        					if(this.cbo_noticegr.value == "3"){
        						this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        						this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",135);
        						this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",135);
        						this.cbo_urgr.set_enable(true);
        					}
        					else if(this.cbo_noticegr.value == "2"){
        						this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",135);
        						this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        						this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);

        					}
        					else{
        						this.div_splitTop02.grd_detail.setFormatColProperty(3,"size",0);
        						this.div_splitTop02.grd_detail.setFormatColProperty(4,"size",0);
        						this.div_splitTop02.grd_detail.setFormatColProperty(5,"size",0);
        					}
        				}
        			});
        		}
        		else{
        			if(this.gfn_isUpdate(this.ds_header)&&this.ds_header.getColumn(e.oldrow, "STATUS") != "C"){
        				trace(e.oldrow);
        			//	this.ds_header.copyRow(e.oldrow,this.ds_copyheader,e.oldrow);
        			//	this.ds_header.setRowType(e.oldrow,1);
        			//	this.ds_header.setColumn(e.oldrow,"CHK",0);
        			//	this.ds_header.setColumn(e.oldrow, "STATUS","");
        			
        			}else if(this.ds_header.getColumn(e.oldrow, "STATUS") == "C"){
        				//if(!this.gv_addRow)this.ds_header.deleteRow(this.ds_header.rowposition);	
        				this.ds_header.deleteRow(e.oldrow);
        			}else{
        				this.ds_header.setColumn(e.oldrow,"CHK",0);
        			}
        			this.gv_addRow = false;
        		}
        	//	rowPix = false;
        	//}
        	///else{
        //		rowPix = true;
        //	}
        }

        this.dateChk = function(dateA,dateB,obj){
        	if(eval(dateA+obj+dateB)){
        		return true;
        	}
        	else{
        		return false
        	}
        }

        this.FileDownload_onmouseenter = function(obj,e)
        {
        	obj.set_style("background:#555555ff");
        	obj.set_style("color:ghostwhite");
        }
        this.FileDownload_onmouseleave = function(obj,e)
        {
        	obj.set_style("background:#ffffffff");
        	obj.set_style("color:#555555ff");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_copyheader.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_copyheader.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop01.grd_header.addEventHandler("oncellclick", this.div_splitTop01_grd_header_oncellclick, this);
            this.div_splitTop01.grd_header.addEventHandler("onheadclick", this.div_splitTop01_grd_header_onheadclick, this);
            this.div_splitTop01.btn_add.addEventHandler("onclick", this.div_splitTop01_btn_add_onclick, this);
            this.div_splitTop01.btn_delete.addEventHandler("onclick", this.div_splitTop01_btn_delete_onclick, this);
            this.div_splitTop01.btn_save.addEventHandler("onclick", this.div_splitTop01_btn_save_onclick, this);
            this.div_splitTop01.btn_update.addEventHandler("onclick", this.div_splitTop01_btn_update_onclick, this);
            this.Static08.addEventHandler("onclick", this.Static08_onclick, this);
            this.CLEAR01.addEventHandler("onclick", this.CLEAR01_onclick, this);
            this.CLEAR02.addEventHandler("onclick", this.CLEAR02_onclick, this);
            this.CLEAR03.addEventHandler("onclick", this.CLEAR03_onclick, this);
            this.cbo_noticegr.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cbo_urgr.addEventHandler("onitemchanged", this.cbo_urgr_onitemchanged, this);
            this.btn_right.addEventHandler("onclick", this.fn_searchDetail, this);
            this.btn_excel.addEventHandler("onclick", this.div_splitTop01_btn_excel_onclick, this);
            this.cal_start_dt.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_start_dt.addEventHandler("canchange", this.cal_to_canchange, this);
            this.cal_end_dt.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_end_dt.addEventHandler("canchange", this.cal_to_canchange, this);
            this.edt_openYn.addEventHandler("onclick", this.CheckBox00_onclick, this);
            this.div_splitTop02.grd_detail.addEventHandler("oncellclick", this.div_splitTop02_grd_detail_oncellclick, this);
            this.div_splitTop02.grd_detail.addEventHandler("onheadclick", this.div_splitTop02_grd_detail_onheadclick, this);
            this.btn_reply.addEventHandler("onclick", this.btn_reply_onclick, this);
            this.cbo_msgtype.addEventHandler("onitemchanged", this.cbo_msgtype_onitemchanged, this);
            this.btn_upload03.addEventHandler("onclick", this.btn_upload03_onclick, this);
            this.btn_upload02.addEventHandler("onclick", this.btn_upload02_onclick, this);
            this.btn_upload01.addEventHandler("onclick", this.btn_upload01_onclick, this);
            this.FileDownload01.addEventHandler("onclick", this.FileDownload01_onclick, this);
            this.FileDownload01.addEventHandler("onmouseenter", this.FileDownload_onmouseenter, this);
            this.FileDownload01.addEventHandler("onmouseleave", this.FileDownload_onmouseleave, this);
            this.FileDownload02.addEventHandler("onclick", this.FileDownload02_onclick, this);
            this.FileDownload02.addEventHandler("onmouseenter", this.FileDownload_onmouseenter, this);
            this.FileDownload02.addEventHandler("onmouseleave", this.FileDownload_onmouseleave, this);
            this.FileDownload03.addEventHandler("onclick", this.FileDownload03_onclick, this);
            this.FileDownload03.addEventHandler("onmouseenter", this.FileDownload_onmouseenter, this);
            this.FileDownload03.addEventHandler("onmouseleave", this.FileDownload_onmouseleave, this);

        };

        this.loadIncludeScript("NoticeManage.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
