﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Owner");
                this.set_classname("style01");
                this.set_titletext("화주");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            obj.getSetter("authProps").set("excel");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("14");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            obj.getSetter("authProps").set("update");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"200\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"80\"/><Column size=\"320\"/><Column size=\"220\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"140\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"21\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"OWKEY\"/><Cell col=\"3\" text=\"OWNAME\"/><Cell col=\"4\" text=\"BILLTOCUSTCD\"/><Cell col=\"5\" text=\"REGNO\"/><Cell col=\"6\" text=\"BUSINESSTYPE\"/><Cell col=\"7\" text=\"BUSINESSCATEGORY\"/><Cell col=\"8\" text=\"ZIPCODE\"/><Cell col=\"9\" text=\"ADDRESS1\"/><Cell col=\"10\" text=\"ADDRESS2\"/><Cell col=\"11\" text=\"TEL1\"/><Cell col=\"12\" displaytype=\"normal\" text=\"TEL2\"/><Cell col=\"13\" displaytype=\"normal\" text=\"FAX\"/><Cell col=\"14\" text=\"CONTACT\"/><Cell col=\"15\" text=\"CEO\"/><Cell col=\"16\" text=\"UDF1ORDEND\"/><Cell col=\"17\" text=\"RELEASE_DUEDATE\"/><Cell col=\"18\" text=\"MASTER_UPDATE_IMPOS\"/><Cell col=\"19\" text=\"CARTYPE\"/><Cell col=\"20\" text=\"DELYN\"/><Cell col=\"21\" text=\"INSERTDATE\"/><Cell col=\"22\" text=\"INSERTURKEY\"/><Cell col=\"23\" text=\"UPDATEDATE\"/><Cell col=\"24\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:owkey\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:owname\" editdisplay=\"edit\"/><Cell col=\"4\" text=\"bind:billtocustcd\"/><Cell col=\"5\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:regno\" mask=\"expr:comp.parent.parent.gfn_isNull(regno) ? &quot;&quot; : &quot;###-##-#####&quot;\" maskchar=\"_\"/><Cell col=\"6\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:businesstype\"/><Cell col=\"7\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:businesscategory\"/><Cell col=\"8\" displaytype=\"normal\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:zipcode\"/><Cell col=\"9\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:address1\"/><Cell col=\"10\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:address2\"/><Cell col=\"11\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:tel1\"/><Cell col=\"12\" displaytype=\"normal\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:tel2\"/><Cell col=\"13\" displaytype=\"normal\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:fax\"/><Cell col=\"14\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:contact\"/><Cell col=\"15\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:ceo\"/><Cell col=\"16\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:udf1\" mask=\"expr:comp.parent.parent.gfn_isNull(udf1)||udf1=='      ' ? &quot;&quot; : &quot;##:##:##&quot;\"/><Cell col=\"17\" text=\"bind:udf3_desc\"/><Cell col=\"18\" text=\"bind:master_update_impos_desc\"/><Cell col=\"19\" text=\"bind:cartype_desc\"/><Cell col=\"20\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:delyn_desc\"/><Cell col=\"21\" displaytype=\"date\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"22\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:inserturkey\"/><Cell col=\"23\" displaytype=\"date\" style=\"background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"24\" style=\"align:left;background:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);background2:EXPR(dataset.getColumn(currow,'delyn') == &quot;Y&quot; ? &quot;tomato&quot; :&quot;#ffffff&quot;);\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("20");
            obj.set_text("화주 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("화주");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.Static06","text","gds_lang","ME00000090_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Owner.xfdl", "lib::Comm.xjs");
        this.registerScript("Owner.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Owner.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 화주
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        * 2018.10.18    JHKIM2       화면에서 삭제버튼만 제거
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        //	var gridMenuSet = ["grd_header^1|0|0|0|0|0|1|1", "grd_detail^0|0|0|0|0|0|1|1"];
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	this.gfn_setUsobXMenu(obj, this.parent.gv_menuId);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gv_header.set_nodatatext("");
        	this.ds_header.clearData();
        	
        	this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        	
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "selectOwnerInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "saveOwner");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        		}else{
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("ownerController/excelDown.do", this.gv_header, this, oValue);
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});
        	}
        }
        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	if(this.ds_header.rowcount > 0){
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "delyn") == "Y"){			
        		 this.gfn_alert("MSG_DEL_READY");
        		 return;
        		 }
        		var oArg = { argFlag:"U"
        					,menuId:this.parent.gv_menuId
        					,owkey:this.ds_header.getColumn(this.ds_header.rowposition, "owkey")
        				   };
        		this.gfn_popup("OwnerPop", "master::OwnerPop.xfdl", oArg, 657, 468, "");
        	}else{
        		this.gfn_alert("MSG_90001");
        	}
        }

        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClickChangeRowType(obj);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);

        };

        this.loadIncludeScript("Owner.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
