﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AddrPop");
                this.set_classname("style01");
                this.set_titletext("신 주소 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,330);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipNo\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addr\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipNo\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"lnmAdres\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ctgLevel", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("10");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "64", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("11");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "95", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "64", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("우편번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "157", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "157", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("국가");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "126", "342", "31", null, null, this);
            obj.set_taborder("249");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "95", "194", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("기본 주소");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "622", "20", "321", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_maxlength("48");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctry", "absolute", "222", "161", "294", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCtry", "absolute", "518", "161", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "222", "99", "321", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_maxlength("90");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "216", "188", "342", "31", null, null, this);
            obj.set_taborder("258");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static10", "absolute", "22", "188", "194", "31", null, null, this);
            obj.set_taborder("259");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("위도");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "216", "219", "342", "31", null, null, this);
            obj.set_taborder("260");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "22", "219", "194", "31", null, null, this);
            obj.set_taborder("261");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("경도");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "77", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "597", "90", "234", "22", null, null, this);
            obj.set_taborder("262");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "597", "58", "234", "22", null, null, this);
            obj.set_taborder("263");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "597", "82", "234", "22", null, null, this);
            obj.set_taborder("264");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "126", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_text("상세 주소");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "222", "130", "321", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_enable("true");
            obj.set_maxlength("90");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "597", "162", "234", "22", null, null, this);
            obj.set_taborder("265");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lat", "absolute", "222", "192", "321", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_mask("##.####");
            obj.set_limitbymask("both");
            obj.set_type("number");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lon", "absolute", "222", "223", "321", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_mask("###.####");
            obj.set_limitbymask("both");
            obj.set_type("number");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "223", "66", "138", "22", null, null, this);
            obj.set_taborder("266");
            obj.set_maxlength("10");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchZipcd", "absolute", "364", "66", "24", "22", null, null, this);
            obj.set_taborder("267");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 330, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("신 주소 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","ADDR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","CTRY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static06","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_ctry","value","ds_header","ctry");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_ctynm","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","static10","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","static12","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","mdt_lat","value","ds_header","lat");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","mdt_lon","value","ds_header","lon");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_ctry","","ds_header","");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("AddrPop_New.xfdl", "lib::Comm.xjs");
        this.registerScript("AddrPop_New.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AddrPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 주소 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_obj = "";
        this.gv_addrid = "";
        this.gv_objDs = "";

        this.gv_Pvalue = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {   
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_obj    = this.gfn_isNullEmpty(this.parent.putObj);
            this.gv_addrid = this.gfn_isNullEmpty(this.parent.addrid);
            this.gv_objDs  = this.gfn_isNullEmpty(this.parent.putDs);
            this.edt_addrsttcd.set_value(1);
            this.ds_header.setColumn(0, "ctry", "KOR");
        	
        	//this.gfn_decimalPointSet("mdt_lat|mdt_lon");

        	this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "addrController");
        	this.gfn_setCommon("METHODNM", "selectAddr");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_FORM_DATA_MAP ds_param=OUT_PARAM";
            var sParam   =  "addrid="+this.gv_addrid
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0) this.ds_header.addRow();
        		this.edt_unit.setFocus();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "zipNo|lnmAdres|stnm|ctry|lat|lon";
        	var sComp = "edt_zipcode|edt_ctynm|edt_stnm|btn_searchCtry|mdt_lat|mdt_lon";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        					var sAddr = this.edt_ctynm.value + "\n" + this.edt_stnm.value;
        					this.gv_objDs.copyData(dsObj);
        					this.gv_obj.set_value(sAddr);
        					
        					this.close();
        				}
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchSta_onclick 실행 */
        this.btn_searchSta_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:10
        				,putObj:this.edt_stacd
        				,putKey:"adcd_hdkey|cntrycd"
        				,putValue:"STACD|KOR"
        				,putBean:"commonController"
        				,putMethod:"selectCommonSearch"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchCtry_onclick 실행 */
        this.btn_searchCtry_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:10
        				,putObj:this.edt_cntrycd
        				,putKey:"adcd_hdkey"
        				,putValue:"CNTRYCD"
        				,putBean:"commonController"
        				,putMethod:"selectCommonSearch"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "stacd"){
        		obj.setColumn(0, "sta", this.gv_Pvalue[1]);
        	}else if(e.columnid == "cntrycd"){
        		obj.setColumn(0, "ctry", this.gv_Pvalue[1]);
        	}
        }

        this.btn_searchZipcd_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchCtry.addEventHandler("onclick", this.btn_searchCtry_onclick, this);
            this.btn_searchZipcd.addEventHandler("onclick", this.btn_searchZipcd_onclick, this);

        };

        this.loadIncludeScript("AddrPop_New.xfdl", true);

       
    };
}
)();
