﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Carrier");
                this.set_classname("style01");
                this.set_titletext("운송사");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"cntrycd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrcd\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"distuom\" type=\"STRING\" size=\"32\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"extcd2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"loggrpcd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"wgtuom\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrnm\" type=\"STRING\" size=\"32\"/><Column id=\"telno1\" type=\"STRING\" size=\"32\"/><Column id=\"telno2\" type=\"STRING\" size=\"32\"/><Column id=\"carrtyp\" type=\"STRING\" size=\"32\"/><Column id=\"lakey_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"shpglocnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"autotndryn\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"brcdtyp_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"alwshpmtndryn\" type=\"STRING\" size=\"32\"/><Column id=\"currtyp\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ADDNT\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"loggrpcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute5\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute4\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute3\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"distuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"attribute2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"currtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"intcarryn\" type=\"STRING\" size=\"32\"/><Column id=\"tndrresphrs\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"lenuom\" type=\"STRING\" size=\"32\"/><Column id=\"lenuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"lakey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"faxno\" type=\"STRING\" size=\"32\"/><Column id=\"weburl\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addr\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"email\" type=\"STRING\" size=\"32\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd\" type=\"STRING\" size=\"32\"/><Column id=\"autotndryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"wgtuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"stacd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"uomtyp\" type=\"STRING\" size=\"32\"/><Column id=\"alwshpmtndryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"intcarryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"brcdtyp\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"ceo\" type=\"STRING\" size=\"256\"/><Column id=\"regno\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"seq\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">adcd_hdkey</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 코드</Col></Row><Row><Col id=\"DATA_FILED\">adcd_hdname</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_MessageCenter", this);
            obj._setContents("<ColumnInfo><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_apkey", this);
            obj._setContents("<ColumnInfo><Column id=\"appKey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_lakey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_msgtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"carrcd\" type=\"STRING\" size=\"32\"/><Column id=\"extcd1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lakey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"distuom\" type=\"STRING\" size=\"32\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"memoid\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"extcd2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"wgtuom\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"regno\" type=\"STRING\" size=\"32\"/><Column id=\"carrnm\" type=\"STRING\" size=\"32\"/><Column id=\"telno1\" type=\"STRING\" size=\"32\"/><Column id=\"telno2\" type=\"STRING\" size=\"32\"/><Column id=\"carrtyp\" type=\"STRING\" size=\"32\"/><Column id=\"autotndryn\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"stacdname\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"alwshpmtndryn\" type=\"STRING\" size=\"32\"/><Column id=\"attribute9\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute8\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"currtyp\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"loggrpcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute5\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute4\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute7\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute6\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute14\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute13\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute3\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute12\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute11\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute10\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"intcarryn\" type=\"STRING\" size=\"32\"/><Column id=\"attribute19\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute18\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute17\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"MMOFWDTYPE\" type=\"STRING\" size=\"32\"/><Column id=\"attribute16\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute15\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"tndrresphrs\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ceo\" type=\"STRING\" size=\"32\"/><Column id=\"ctynm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lenuom\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"prtbtxt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"Column38\" type=\"STRING\" size=\"32\"/><Column id=\"faxno\" type=\"STRING\" size=\"32\"/><Column id=\"weburl\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addr\" type=\"STRING\" size=\"32\"/><Column id=\"nonprtbtxt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute20\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"email\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycdname\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"uomtyp\" type=\"STRING\" size=\"32\"/><Column id=\"brcdtyp\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"seq\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_returnValue", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_sttcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_headerCopySaveProc", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"cntrycd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrcd\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"distuom\" type=\"STRING\" size=\"32\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"extcd2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"loggrpcd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"wgtuom\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"carrnm\" type=\"STRING\" size=\"32\"/><Column id=\"telno1\" type=\"STRING\" size=\"32\"/><Column id=\"telno2\" type=\"STRING\" size=\"32\"/><Column id=\"carrtyp\" type=\"STRING\" size=\"32\"/><Column id=\"lakey_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"shpglocnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"autotndryn\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"brcdtyp_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"alwshpmtndryn\" type=\"STRING\" size=\"32\"/><Column id=\"currtyp\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ADDNT\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"loggrpcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute5\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute4\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute1\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"attribute3\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"distuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"attribute2\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"currtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"intcarryn\" type=\"STRING\" size=\"32\"/><Column id=\"tndrresphrs\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"lenuom\" type=\"STRING\" size=\"32\"/><Column id=\"lenuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"lakey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"faxno\" type=\"STRING\" size=\"32\"/><Column id=\"weburl\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addr\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"email\" type=\"STRING\" size=\"32\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd\" type=\"STRING\" size=\"32\"/><Column id=\"autotndryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"wgtuom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"stacd_desc\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"uomtyp\" type=\"STRING\" size=\"32\"/><Column id=\"alwshpmtndryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"intcarryn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"brcdtyp\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "77", null, this.div_splitTop);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_modify", "absolute", null, "0", "33", "24", "114", null, this.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "151", null, this.div_splitTop);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_header");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"134\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/><Cell col=\"1\" text=\"CARRCD\"/><Cell col=\"2\" text=\"CARRNM\"/><Cell col=\"3\" text=\"REGNO\"/><Cell col=\"4\" text=\"ADDR\"/><Cell col=\"5\" text=\"LAT\"/><Cell col=\"6\" text=\"LON\"/><Cell col=\"7\" text=\"TELNO1\"/><Cell col=\"8\" text=\"FAXNO\"/><Cell col=\"9\" text=\"EMAIL\"/><Cell col=\"10\" text=\"WEBURL\"/><Cell col=\"11\" text=\"CEO\"/><Cell col=\"12\" text=\"STTCD\"/><Cell col=\"13\" text=\"LAKEY\"/><Cell col=\"14\" text=\"CARRTYP\"/><Cell col=\"15\" text=\"CURRTYP\"/><Cell col=\"16\" text=\"DELYN\"/><Cell col=\"17\" text=\"INSERTDATE\"/><Cell col=\"18\" text=\"INSERTURKEY\"/><Cell col=\"19\" text=\"UPDATEDATE\"/><Cell col=\"20\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding:0 2 0 2;\" text=\"bind:PAGING_NUM\"/><Cell col=\"1\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:carrcd\"/><Cell col=\"2\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:carrnm\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:regno\"/><Cell col=\"4\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:addr\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lat\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lon\"/><Cell col=\"7\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:telno1\"/><Cell col=\"8\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:faxno\"/><Cell col=\"9\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:email\"/><Cell col=\"10\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:weburl\"/><Cell col=\"11\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:ceo\"/><Cell col=\"12\" displaytype=\"combo\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:sttcd\" combodataset=\"ds_combo_sttcd\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"13\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lakey_desc\"/><Cell col=\"14\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:carrtypname\"/><Cell col=\"15\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:currtyp\"/><Cell col=\"16\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:delyn_desc\"/><Cell col=\"17\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:insertdate\"/><Cell col=\"18\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"19\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updatedate\"/><Cell col=\"20\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("수정");
            obj.set_visible("false");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("7");
            obj.set_text("운송사 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("운송사");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_modify","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00103280_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Carrier.xfdl", "lib::Comm.xjs");
        this.registerScript("Carrier.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Carrier.xfdl
        * PROGRAMMER  : eungheon.kim
        * DATE        : 2017.01.06
        * DESCRIPTION : 운송사
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* 폼 초기설정 */
        this.form_oninit = function(obj,e)
        {

        }

        /* 폼 로드 */
        this.form_onload = function(obj,e)
        { 
        	this.gv_grdList = [this.div_splitTop.grd_header]; // 그리드 오브젝트
        	var divPaging = [this.div_splitTop.div_Paging]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	//그리드 데이터 추가시 입력가능 수정시 수정불가능 목록 정의
        	var keyField = ["carrcd"];
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc, keyField);
        	
        	this.gfn_getCode("STTCD", this.ds_combo_sttcd, ""); // 그리드에 상태칼럼 콤보박스 로드
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.set_keystring(""); // 필터 초기화
        	this.ds_header.clearData();
        	this.ds_detail.set_keystring(""); // 필터 초기화
        	this.ds_detail.clearData();
        	
        	this.gfn_setCommon("BEANID",   "carrierController");
        	this.gfn_setCommon("METHODNM", "selectCarrInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        // 저장 전 데이터가 유효한지 디비에서 실제 먼저 체크
        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "carrierController");
        	this.gfn_setCommon("METHODNM", "checkValidCarrList");
        	
        	var sSvcId   = "save";
        	var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
        	var sInData  = "IN_APPROVAL_LIST=ds_header:U";
        	var sOutData = "ds_returnValue=OUT_PARAM";
        	var sParam   = "workType=APPROVE";
        	this.ds_returnValue.clearData();
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        // 실제저장
        this.fn_save_proc = function()
        {
        	this.gfn_setCommon("BEANID", "carrierController");
        	this.gfn_setCommon("METHODNM", "approveCarrList");
        	
            var sSvcId   = "save_proc";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_APPROVAL_LIST=ds_headerCopySaveProc";    
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /***********************************************************************************
        * 그리드 해더
        ***********************************************************************************/
        /* 그리드 해더 클릭(Grid Header Click)  */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	this.gfn_grdHeaderClick(obj, e, this);
        }

        /* 그리드 해더 셀 클릭(Grid Header Cell Click)  */
        this.div_splitTop_grd_header_oncellclick = function(obj,e)
        {
        	// 한 번 클릭으로 동작
        	obj.dropdownCombo();
        	obj.dropdownCalendar();
        }

        /* 그리드 해더 셀 더블클릭(Grid Header Cell DoubleClick)  */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	this.gfn_grdDoubleClick(obj);
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	if(colName != "CHK"){
        		this.div_splitTop_btn_modify_onclick();
        	}
        }

        /* 추가 버튼 클릭(Add Button Click) */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	            ,ds_header:this.ds_header
        	            ,grd_header:this.div_splitTop.grd_header
        	           };
        	this.gfn_popup("CarrierPop", "master::CarrierPop.xfdl", oArg, 657, 761, "");
        }

        /* 수정 버튼 클릭(modify Button Click) */
        this.div_splitTop_btn_modify_onclick = function(obj,e)
        {
        	if(this.gfn_grdSingleRowCheck(this.div_splitTop.grd_header)){
        		var oArg = { argFlag:"U"
        					,menuId:this.parent.gv_menuId
        					,ds_header:this.ds_header
        					};
        		this.gfn_popup("CarrierPop", "master::CarrierPop.xfdl", oArg, 657, 761, "");	
        	}
        }

        /* 삭제 버튼 클릭(Delete Button Click) */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	if(this.gfn_grdMultiRowCheck(this.div_splitTop.grd_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});		
        	}
        }

        this.fn_delete = function()
        {	
        	var checkList = this.gfn_getCheckedIndex(this.div_splitTop.grd_header);
        	for(var i=0; i<checkList.length ; i++){
        		this.ds_header_save.copyRow(this.ds_header_save.addRow(), this.ds_header, checkList[i]);
        	}
        				
        	this.gfn_setCommon("BEANID"  , "entitiesCarrController");
        	this.gfn_setCommon("METHODNM", "saveCarr");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_tms + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_header_save";
            var sOutData = "";
            var sParam   = "workType=DELETE";

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /* 엑셀 버튼 클릭(Excel Button Click) */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	//this.gfn_exportExcel(this, this.div_splitTop.grd_header, "Message");
        	
        	// 엑셀 다운로드 1
        	this.gfn_Excel(this.div_splitTop.grd_header, this, "carrierController/excelDown.do", null);	
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			//this.ds_header.addColumn("CHK");
        		}else{
        			this.div_splitTop.grd_header.set_nodatatext(application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage"));
        		}
        		
        		this.gfn_setParam("sortValue", "");
        		this.gfn_setParam("colName", "");
        		
        		this.div_splitTop.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        		//헤더쪽 다국어 때문에 사용 ( DataSet에서 useclientlayout : false 일때 사용 조회시 서버로부터 데이터 받을시 그리드 다국어 적용
        		this.gfn_constDsSet(this.div_splitTop.grd_header);

        	}else if(sSvcId == "save"){
        		if(this.ds_returnValue.getRowCount() == 0){
        			this.gfn_alert("MSG_ALERT_COMPLETE");
        		}else{
        			if (this.ds_returnValue.getColumn(0, "result") == false) {
        				this.gfn_alert("MSG_CHK_APPROVAL_STATUS");
        				return;
        			} else {						
        				this.fn_save_proc();
        			}
        // 			var keyValue = "";
        // 			var apkey = "";
        // 			for(var i = 0 ; i < this.ds_returnValue.getRowCount() ; i++){
        // 				if(i != 0){
        // 					keyValue +="\n";
        // 				}
        // 				keyValue += this.ds_returnValue.getColumn(i, "carrcd")+"_";
        // 				keyValue += this.ds_returnValue.getColumn(i, "sttcd")+"_";
        // 			}
        // 			 
        // 			var arg = "KEYVALUE,"+keyValue;
        // 			this.gfn_alert("MSG_DUPLICATE_KEY", arg);
        		}
        //		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "save_proc"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_search();
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_search();
        	}else if(sSvcId == "exceldown"){
        		this.gfn_excelDownload(this.ds_excelFile);
        	}
        }

        /*******************************************************************************
        * 사용자 정의 함수 (User Definition Function)
        ********************************************************************************/

        
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	//var dsCol = "mename|meorder|apkey|uskey,usname|uppermekey,uppermename|eqtype";
        	var dsCol = "carrcd|sttcd";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				if(this.gfn_isDataNullCheck(this.div_splitTop.grd_header, this.ds_header, dsCol)){
        					var nRow = this.ds_header.findRowExpr("STATUS == 'U'");
        					
        					// 변경된 데이터만 저장하는 데이터셋 초기화
        					this.ds_headerCopySaveProc.clearData();
        					
        					// 변경된 데이터셋만 복사하는 로직
        					var i = 0;
        					
        					while(nRow != -1){
        						this.ds_headerCopySaveProc.addRow();
        						this.ds_headerCopySaveProc.copyRow(i, this.ds_header, nRow);
        						nRow = this.ds_header.findRowExpr("STATUS == 'U'", nRow+1);
        						i++;
        					}
        					
        					this.fn_save();
        				}
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1 && obj.getRowType(e.row) != 2){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        		
        		if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					//this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			//this.fn_searchDetail();
        		}
        	}
        	
        	this.rowChg = "";
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.form_onsize, this);
            this.addEventHandler("oninit", this.form_oninit, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_modify.addEventHandler("onclick", this.div_splitTop_btn_modify_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);

        };

        this.loadIncludeScript("Carrier.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
