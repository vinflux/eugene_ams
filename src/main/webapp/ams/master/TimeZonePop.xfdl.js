﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("TimeZonePop");
                this.set_classname("style01");
                this.set_titletext("표준 시간대 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,187);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"tzkey\" type=\"string\" size=\"32\"/><Column id=\"utc_offset\" type=\"string\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"utc_offset\"/><Col id=\"tzkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("121");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tzkey", "absolute", "222", "62", "260", "22", null, null, this);
            obj.set_taborder("161");
            obj.set_maxlength("8");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_utc_offset", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("180");
            obj.set_maxlength("5");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("226");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "62", "57", "22", null, null, this);
            obj.set_taborder("228");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 187, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("표준 시간대 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_tzkey","value","ds_header","tzkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_utc_offset","value","ds_header","utc_offset");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","TIMEZONE HEADER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","static","text","gds_lang","TZKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static1","text","gds_lang","UTC_OFFSET");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("TimeZonePop.xfdl", "lib::Comm.xjs");
        this.registerScript("TimeZonePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : TimeZonePop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 표준 시간대 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_tzkey = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_tzkey  = this.gfn_isNullEmpty(this.parent.tzkey);
        	
        	this.ds_header.applyChange();
        	
        	if(this.gv_flag == "U"){
        		this.edt_tzkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "timeZoneController");
        	this.gfn_setCommon("METHODNM", "selectTimezoneHd");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "tzkey="+this.gv_tzkey;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "timeZoneController");
        	this.gfn_setCommon("METHODNM", "checkTimezoneInfoHd");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "tzkey="+this.ds_header.getColumn(0, "tzkey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "timeZoneController");
        	this.gfn_setCommon("METHODNM", "insertTimezoneInfoHd");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_TZDATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "timeZoneController");
        	this.gfn_setCommon("METHODNM", "updateTimezoneInfoHd");
        	
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_TZDATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_tzkey.setFocus();
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			if(this.gfn_isNull(this.gfn_setParam("colName"))){
        				this.gfn_setParam("sortValue", "DESC");
        				this.gfn_setParam("colName", "UPDATEDATE");
        			}
        			
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			if(this.gfn_isNull(this.gfn_setParam("colName"))){
        				this.gfn_setParam("sortValue", "DESC");
        				this.gfn_setParam("colName", "UPDATEDATE");
        			}
        			
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "tzkey|utc_offset";
        	var sComp = "edt_tzkey|edt_utc_offset";
        	
        	if((this.ds_param.rowcount == 0 || this.ds_param.getColumn(0, "isDuplicate") == "true") && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_tzkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						var utc_offset = this.edt_utc_offset.value;
        						var delimeter = utc_offset.substr(0,1);
        						var time = parseInt(utc_offset.substr(1,4));
        						var str = utc_offset.substr(1,4).valueOf();
        						
        						for(var i = 0 ; i < str.length ; i++){
        							if(str.charAt(i) < '0' || str.charAt(i) > '9'){
        								this.gfn_alert("MSG_ALERT_WRONG_TIMEZONE", "", function(msg, flag){
        									this.edt_utc_offset.setFocus();
        								});
        								
        								return;
        							}
        						}
        						
        						if(delimeter != "-" && delimeter != "+"){
        							this.gfn_alert("MSG_ALERT_SYMBOLIC_ERROR", "", function(msg, flag){
        								this.edt_utc_offset.setFocus();
        							});
        						}else if(delimeter == "-" && time > 1200){
        							this.gfn_alert("MSG_ALERT_OUT_OF_RANGE", "", function(msg, flag){
        								this.edt_utc_offset.setFocus();
        							});
        						}else if(delimeter == "+" && time > 1400){
        							this.gfn_alert("MSG_ALERT_OUT_OF_RANGE", "", function(msg, flag){
        								this.edt_utc_offset.setFocus();
        							});
        						}else{
        							if(this.gv_flag == "H") this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        						}
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* edt_tzkey_onchanged 실행 */
        this.edt_tzkey_onchanged = function(obj,e)
        {
        	if(this.ds_param.rowcount > 0) this.ds_param.setColumn(0, "isDuplicate", "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.ds_param.getColumn(0, "isDuplicate") == "false"){
        		return;
        	}
        	
        	if(this.gfn_isNull(this.edt_tzkey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_tzkey.setFocus();
        		});
        	}else{
        		var tzkey = this.edt_tzkey.value;
        		var delimeter = tzkey.substr(3,1);
        		var time = parseInt(tzkey.substr(4,4));
        		var str = tzkey.substr(4,4).valueOf();
        		
        		for(var i = 0 ; i < str.length ; i++){
        			if(str.charAt(i) < '0' || str.charAt(i) > '9'){
        				this.gfn_alert("MSG_ALERT_WRONG_TIMEZONE", "", function(msg, flag){
        					this.edt_tzkey.setFocus();
        				});
        				
        				return;
        			}
        		}
        		
        		if(tzkey.length != 8){
        			this.gfn_alert("MSG_CHKDATA_TOOLONG", "", function(msg, flag){
        				this.edt_tzkey.setFocus();
        			});
        		}else if(delimeter != "-" && delimeter != "+"){
        			this.gfn_alert("MSG_ALERT_SYMBOLIC_ERROR", "", function(msg, flag){
        				this.edt_tzkey.setFocus();
        			});
        		}else if(delimeter == "-" && time > 1200){
        			this.gfn_alert("MSG_ALERT_OUT_OF_RANGE", "", function(msg, flag){
        				this.edt_tzkey.setFocus();
        			});
        		}else if(delimeter == "+" && time > 1400){
        			this.gfn_alert("MSG_ALERT_OUT_OF_RANGE", "", function(msg, flag){
        				this.edt_tzkey.setFocus();
        			});
        		}else{
        			this.fn_CheckCode();
        		}
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_tzkey.addEventHandler("onchanged", this.edt_tzkey_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);

        };

        this.loadIncludeScript("TimeZonePop.xfdl", true);

       
    };
}
)();
