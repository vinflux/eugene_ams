﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("IcxctxacPop");
                this.set_classname("style01");
                this.set_titletext("센터별거래처별 상품 수정 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,422,276);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_poicuttype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_pohdtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static26", "absolute", "142", "151", "255", "31", null, null, this);
            obj.set_taborder("385");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "265", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#000000ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("35");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            obj.style.set_background("transparent");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "160", null, "52", "28", null, "15", this);
            obj.set_taborder("33");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "25", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.style.set_border("1 solid #e6e6e6ff,0 none #808080ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "257", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            obj.style.set_border("1 solid #e6e6e6ff,0 none #808080ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "258", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "25", "89", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "182", "259", "31", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "25", "182", "120", "31", null, null, this);
            obj.set_taborder("325");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "258", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "25", "120", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "25", "151", "120", "31", null, null, this);
            obj.set_taborder("384");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ackey", "absolute", "153", "124", "210", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_enable("false");
            obj.set_cssclass("edt_pop");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "152", "15", this);
            obj.set_taborder("34");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ickey", "absolute", "153", "155", "156", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("false");
            obj.set_cssclass("edt_pop");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "153", "62", "210", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("false");
            obj.set_cssclass("edt_pop");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctkey", "absolute", "153", "93", "210", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("false");
            obj.set_cssclass("edt_pop");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIckey", "absolute", "312", "155", "24", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", null, "62", "24", "22", "25", null, this);
            obj.set_taborder("458");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAckey", "absolute", null, "124", "24", "22", "25", null, this);
            obj.set_taborder("459");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCtkey", "absolute", null, "93", "24", "22", "25", null, this);
            obj.set_taborder("460");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_CheckKey", "absolute", "82.94%", "155", null, "22", "25", null, this);
            obj.set_taborder("461");
            obj.set_text("CHECK");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Spin("spn_costprice", "absolute", "153", "185", null, "22", "25", null, this);
            obj.set_taborder("465");
            obj.set_value("0");
            obj.set_max("9999999");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("468");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("469");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "212", null, "1", "22", null, this);
            obj.set_taborder("470");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 422, 276, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("센터별거래처별 상품 수정 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","edt_ackey","value","ds_header","ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","CTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static05","text","gds_lang","COSTPRICE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","Static25","text","gds_lang","ICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_ickey","value","ds_header","ickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_ctkey","value","ds_header","ctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","CTXACXIC");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","spn_costprice","value","ds_header","costprice");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("IcxctxacPop.xfdl", "lib::Comm.xjs");
        this.registerScript("IcxctxacPop.xfdl", function(exports) {
        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ickey = "";
        this.gv_ctkey = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	this.gfn_getCode("POICUTTYPE", this.ds_poicuttype, "");
        	this.gfn_getCode("POHDTYPE", this.ds_pohdtype, "");
        	
        	this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_ickey    = this.gfn_isNullEmpty(this.parent.ickey);
            this.gv_ctkey    = this.gfn_isNullEmpty(this.parent.ctkey);
            this.gv_ackey    = this.gfn_isNullEmpty(this.parent.ackey);
            //this.time_combo_setting();
        	this.ds_header.applyChange();
        	this.fn_search();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "icxctxacController");
        	this.gfn_setCommon("METHODNM", "selectIcxctxac");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " ickey="+this.gv_ickey
                         + " ctkey="+this.gv_ctkey
                         + " ackey="+this.gv_ackey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "icxctxacController");
        	this.gfn_setCommon("METHODNM", "saveIcxctxac");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType="+this.parent.workType;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        			
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }
        this.btn_save_onclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.edt_ackey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ackey.setFocus();
        		});
        		
        		return;
        	}
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_Update();
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.time_combo_setting = function()
        {
        	//15분 간격 시간 콤보 설정
            var ds_time = new Dataset();
            ds_time.addColumn("field1","string");
            ds_time.addColumn("field2","string");
            var init = 0;
            for(var i=0; i<96; i++){
        		ds_time.addRow();
        		if(Math.floor(init/60) < 10){
        			if(init%60==0){
        				ds_time.setColumn(i,"field1","0"+Math.floor(init/60)+""+init%60+"000");
        				ds_time.setColumn(i,"field2","0"+Math.floor(init/60)+":"+init%60+"0");
        			}else{
        				ds_time.setColumn(i,"field1","0"+Math.floor(init/60)+""+init%60+"00");
        				ds_time.setColumn(i,"field2","0"+Math.floor(init/60)+":"+init%60);
        			}
        		}else{
        			if(init%60==0){
        				ds_time.setColumn(i,"field1",Math.floor(init/60)+""+init%60+"000");
        				ds_time.setColumn(i,"field2",Math.floor(init/60)+":"+init%60+"0");
        			}else{
        				ds_time.setColumn(i,"field1",Math.floor(init/60)+""+init%60+"00");
        				ds_time.setColumn(i,"field2",Math.floor(init/60)+":"+init%60);
        			}
        		}
        		init = init+15;
            }
            this.cbo_ibtime.set_innerdataset(ds_time);
            this.cbo_ibtime.set_codecolumn("field1");
            this.cbo_ibtime.set_datacolumn("field2");
            
            this.cbo_poclosdate.set_innerdataset(ds_time);
            this.cbo_poclosdate.set_codecolumn("field1");
            this.cbo_poclosdate.set_datacolumn("field2");

        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_ctkey.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_searchIckey.addEventHandler("onclick", this.btn_searchPkcttkey_onclick, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchPkcttkey_onclick, this);
            this.btn_searchAckey.addEventHandler("onclick", this.btn_searchPkcttkey_onclick, this);
            this.btn_searchCtkey.addEventHandler("onclick", this.btn_searchPkcttkey_onclick, this);

        };

        this.loadIncludeScript("IcxctxacPop.xfdl", true);

       
    };
}
)();
