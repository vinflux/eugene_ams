﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UserGroup");
                this.set_classname("style01");
                this.set_titletext("사용자그룹");
                this._setFormPosition(0,0,1230,627);
            }
            this.getSetter("menuId").set("");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_userGroup", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"1\"/><Column id=\"urgrkey\" type=\"string\" size=\"32\"/><Column id=\"updatedate\" type=\"string\" size=\"32\"/><Column id=\"inserturkey\" type=\"string\" size=\"32\"/><Column id=\"insertdate\" type=\"string\" size=\"32\"/><Column id=\"updateurkey\" type=\"string\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"bigdecimal\" size=\"8\"/><Column id=\"urgrname\" type=\"string\" size=\"32\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_adminDetailCode", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"200\"/><Column id=\"adcd_dtkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtvalue\" type=\"STRING\" size=\"256\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"DATE\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtorder\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtname\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_hdkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">urgrkey</Col><Col id=\"FILED_NAME1\">사용자 그룹 코드</Col><Col id=\"FILED_NAME2\">사용자 그룹 코드</Col><Col id=\"FILED_NAME3\">사용자 그룹 코드</Col></Row><Row><Col id=\"DATA_FILED\">urgrname</Col><Col id=\"FILED_NAME1\">사용자 그룹 명</Col><Col id=\"FILED_NAME2\">사용자 그룹 명</Col><Col id=\"FILED_NAME3\">사용자 그룹 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_userGroup", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_userGroup");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("none");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj.getSetter("userdata").set("0");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/><Column size=\"40\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\"/><Cell col=\"3\"/><Cell col=\"4\"/><Cell col=\"5\"/><Cell col=\"6\"/><Cell col=\"7\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding:0 2 0 2;\" text=\"bind:PAGING_NUM\"/><Cell col=\"2\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:urgrkey\"/><Cell col=\"3\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:urgrname\"/><Cell col=\"4\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"6\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"7\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자그룹");
            		p.getSetter("menuId").set("");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("UserGroup.xfdl", "lib::Comm.xjs");
        this.registerScript("UserGroup.xfdl", function() {
        /***********************************************************************************
        * FILE NAME   : AdminCode.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 관리 기준 코드 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        //include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        //this.gv_split = "";

        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_initForm(obj);
        	
        	var gridList = [this.div_splitTop.grd_userGroup];
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var gridMenuSet = ["1|0|0|0|0|0|1|1", "0|0|0|0|0|0|1|1"];
        	var divPaging = [this.div_splitTop.div_Paging];
        	var searchFunc = [this.gv_main];
        	
        	this.gfn_gridInit(gridList, gridMenuSet, divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_userGroup.set_keystring(""); // 필터 초기화
        	this.ds_userGroup.clearData();
        	
        	this.gfn_setCommon("BEANID", "userGroupController");
        	this.gfn_setCommon("METHODNM", "selectUserGroupInfoList");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_userGroup=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        
        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "deleteAdminCode");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_AdminCode=ds_adminCode:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.pageSet(this.div_splitTop.div_Paging, this.ds_param, true);
        		
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "deleteDetail"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_SearchDetail();
        	}else if(sSvcId == "exceldown"){
        		this.gfn_excelDownload(this.ds_excelFile);
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /* div_splitTop_grd_adminCode_oncelldblclick 실행 */
        this.div_splitTop_grd_adminCode_oncelldblclick = function(obj,e)
        {
        	this.fn_SearchDetail();
        }

        /* div_splitTop_grd_adminCode_onrbuttonup 실행 */
        this.div_splitTop_grd_adminCode_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "0|0|0|0|0|0|1|1";
        	this.gfn_openGridMenu(obj, e, enable_list);
        }

        /* div_splitBottom_grd_adminDetailCode_onrbuttonup 실행 */
        this.div_splitBottom_grd_adminDetailCode_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "0|0|0|0|0|0|1|1";
        	this.gfn_openGridMenu(obj, e, enable_list);
        }

        /* div_splitTop_grd_adminCode_onkeydown 실행 */
        this.div_splitTop_grd_adminCode_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_adminCode, obj.currentcell);
        }

        
        /* div_splitTop_grd_adminCode_onheadclick 실행 */
        this.div_splitTop_grd_adminCode_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_adminCode.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitBottom_grd_adminDetailCode_onheadclick 실행 */
        this.div_splitBottom_grd_adminDetailCode_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.div_splitTop.grd_adminCode, "AdminCode");
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_adminCode.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("UserGroupPop", "master::UserGroupPop.xfdl", oArg, 580, 187, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	var oArg = "";
        	var nRow = this.ds_adminCode.findRow("CHK", "1");
        	var nRowCnt = this.ds_adminCode.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else if(nRowCnt > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        	}else{
        		oArg = { argFlag:"U"
        				,menuId:this.parent.gv_menuId
        				,adcd_hdkey:this.ds_adminCode.getColumn(nRow, "adcd_hdkey")
        				,adcd_hdname:this.ds_adminCode.getColumn(nRow, "adcd_hdname")
        			   };
        		this.gfn_popup("AdminCodePop", "master::AdminCodePop.xfdl", oArg, 580, 187, "");
        	}
        }

        /* div_splitTop_grd_adminCode_oncellclick 실행 */
        this.div_splitTop_grd_adminCode_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_adminCode.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_adminCode.setColumn(e.row, "CHK", "");
        		else this.ds_adminCode.setColumn(e.row, "CHK", "1");
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("onheadclick", this.div_splitTop_grd_adminCode_onheadclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("oncelldblclick", this.div_splitTop_grd_adminCode_oncelldblclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("oncellclick", this.div_splitTop_grd_adminCode_oncellclick, this);

        };

        this.loadIncludeScript("UserGroup.xfdl");
        this.loadPreloadList();
       
    };
}
)();
