﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("StorexctPop");
                this.set_classname("style01");
                this.set_titletext("매출처별 센터(점포) 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,249);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_newyn", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("14");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("4");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_store_key", "absolute", "222", "93", "258", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_enable("false");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("5");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCtKey", "absolute", "518", "62", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctkey", "absolute", "222", "62", "290", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "486", "93", "57", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_text("중복체크");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "794", "94", "342", "31", null, null, this);
            obj.set_taborder("252");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static10", "absolute", "600", "94", "194", "31", null, null, this);
            obj.set_taborder("253");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_arr_time_applydate", "absolute", "800", "98", "106", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("11");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.set_value("null");
            obj.set_type("normal");

            obj = new Combo("time_arr_time", "absolute", "616", "67", "320", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("10");
            obj.set_innerdataset("ds_prtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_newyn", "absolute", "222", "155", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_combo_newyn");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("static05", "absolute", "912", "94", "137", "31", null, null, this);
            obj.set_taborder("12");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_time_regdate", "absolute", "182.41%", "98", null, "22", "-88.28%", null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("13");
            obj.set_dateformat("HH:MM");
            obj.set_value("null");
            obj.set_editformat("HH:MM");

            obj = new MaskEdit("mask_arr_time", "absolute", "38.28%", "124", "321", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_mask("##:##");
            obj.set_limitbymask("integer");
            obj.set_type("string");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 249, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("매출처별 센터(점포) 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_store_key","value","ds_header","store_key");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","STOREXCT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_ctkey","value","ds_header","ctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","STORE_KEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","CTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ARR_TIME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","NEWYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","static10","text","gds_lang","ARR_TIME_APPLYDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","cal_arr_time_applydate","value","ds_header","arr_time_applydate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","time_arr_time","value","ds_header","arr_time");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","cbo_newyn","value","ds_header","newyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","ARR_TIME_APPLYDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","mask_arr_time","value","ds_header","arr_time");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("StorexctPop.xfdl", "lib::Comm.xjs");
        this.registerScript("StorexctPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : StorexctPop.xfdl
        * PROGRAMMER  : 황성무
        * DATE        : 2017.05.17
        * DESCRIPTION : 센터별 매출처(점포) 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_ctkey = "";
        this.gv_store_key = "";
        this.gv_owkey = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	//this.gfn_getCode("LOGGRPCD", this.ds_combo_loggrpcd, "");
        	this.gfn_getCode("YESORNO", this.ds_combo_newyn, "");
        		
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_ctkey  = this.gfn_isNullEmpty(this.parent.ctkey);
            this.gv_store_key  = this.gfn_isNullEmpty(this.parent.store_key);
            this.gv_owkey  = this.gfn_isNullEmpty(this.parent.owkey);
            
            this.ds_header.applyChange();
            if(this.gv_flag == "U"){
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "storexctController");
        	this.gfn_setCommon("METHODNM", "selectStorexct");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "ctkey="+this.gv_ctkey
                         + " store_key="+this.gv_store_key
                         + " owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        // this.fn_CheckCode = function()
        // {
        // 	this.ds_param.clearData();
        // 	
        // 	this.gfn_setCommon("BEANID", "equipmentController");
        // 	this.gfn_setCommon("METHODNM", "selectEquipment");
        // 	
        //     var sSvcId   = "check";
        //     var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        //     var sInData  = "";
        //     var sOutData = "ds_param=OUT_PARAM";
        //     var sParam   = "ctkey="+this.ds_header.getColumn(0, "ctkey")
        //                  + " prtype="+this.ds_header.getColumn(0, "prtype")
        //                  ;
        //     
        //     this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        // }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "storexctController");
        	this.gfn_setCommon("METHODNM", "saveStorexct");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT"
                         + " owkey="+this.gv_owkey
                         ;
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "storexctController");
        	this.gfn_setCommon("METHODNM", "saveStorexct");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE"
                         + " owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		//this.cbo_loggrpcd.setFocus();
        // 		var key = "";
        // 		var value = "";
        // 		
        // 		if(this.gfn_isNotNull(this.edt_ctkey.value)){
        // 			this.gv_code = "Y";
        // 			this.gfn_codeSearch(key, value, this.edt_ctkey.value, "10", application.gv_ams, "", "", this.edt_ctkey, this.edt_ctname);
        // 		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "COUNT") != 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_ctkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "ctkey|store_key";
        	var sComp = "edt_ctkey|edt_store_key";
        	
        	/*var regex= /^[0-2][0-3][0-5][0-9]$/;*/
        	var regex= /^([0-1]?[0-9]|2[0-3])[0-5][0-9]$/;
        	
        	if (!regex.test(this.mask_arr_time.value.trim()) && !this.gfn_isNull(this.mask_arr_time.value.trim())) {
        		this.gfn_alert("시분 형식에 맞지 않습니다.");
        		this.mask_arr_time.setFocus()
        		return;
        	}
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_ctkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_ctkey.value) && !this.gfn_isNull(this.edt_ctkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ctkey.setFocus();
        		});
        	}else if(!this.gfn_isNull(this.edt_store_key.value) && this.gfn_isNull(this.edt_store_key.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_store_key.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") this.fn_Update();
        					}
        				}
        			});
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "ctkey|eqtype|prtype";
        	var sComp = "edt_ctkey|cbo_eqtype|cbo_prtype";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* btn_searchctkey_onclick 실행 */
        this.btn_searchctkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:10
        				,putObj:this.edt_ctkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "ctkey" || e.columnid == "eqtype" || e.columnid == "prtype"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "ctkey"){
        			this.edt_apname.set_value(this.gv_Pvalue[1]);
        			this.edt_apname.set_tooltiptext(this.gv_Pvalue[1]);
        		}
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_ctkey_onchanged 실행 */
        this.edt_ctkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_ctkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_ctkey.value, "10", application.gv_ams, "", "", this.edt_ctkey, this.edt_apname);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchCtKey.addEventHandler("onclick", this.btn_searchApkey_onclick, this);
            this.edt_ctkey.addEventHandler("onchanged", this.edt_apkey_onchanged, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.time_arr_time.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.cbo_newyn.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);

        };

        this.loadIncludeScript("StorexctPop.xfdl", true);

       
    };
}
)();
