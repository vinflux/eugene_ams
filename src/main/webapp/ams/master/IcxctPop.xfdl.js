﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("IcxctPop");
                this.set_classname("style01");
                this.set_titletext("상품별센터 수정 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,526);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_pktype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_voltype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ordstoptype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ordtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_obptkey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static10", "absolute", "141", "212", "490", "31", null, null, this);
            obj.set_taborder("433");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "142", "428", "490", "31", null, null, this);
            obj.set_taborder("464");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "142", "151", "490", "31", null, null, this);
            obj.set_taborder("385");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("35");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "23", this);
            obj.set_taborder("33");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("상품코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("상품명");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "89", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("센터코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("센터명");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icname", "absolute", "453", "62", "165", "22", null, null, this);
            obj.set_taborder("18");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static50", "absolute", "327", "151", "120", "31", null, null, this);
            obj.set_taborder("323");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("크로스독킹여부");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "22", "397", "120", "31", null, null, this);
            obj.set_taborder("331");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("상품높이(mm)");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "148", "401", "373", "22", null, null, this);
            obj.set_taborder("336");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "142", "397", "490", "31", null, null, this);
            obj.set_taborder("337");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "22", "428", "120", "31", null, null, this);
            obj.set_taborder("339");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("매입처코드");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "124", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("물류그룹코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "120", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("회차구분");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "141", "181", "490", "31", null, null, this);
            obj.set_taborder("369");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "22", "181", "120", "31", null, null, this);
            obj.set_taborder("381");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("피킹센터코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "22", "151", "120", "31", null, null, this);
            obj.set_taborder("384");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("상품그룹코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "141", "366", "490", "31", null, null, this);
            obj.set_taborder("391");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static55", "absolute", "22", "366", "120", "31", null, null, this);
            obj.set_taborder("401");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("상품가로(mm)");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_dckey", "absolute", "453", "124", "165", "22", null, null, this);
            obj.set_taborder("22");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_dtgid", "absolute", "148", "124", "165", "22", null, null, this);
            obj.set_taborder("21");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "23", this);
            obj.set_taborder("34");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icgrkey", "absolute", "148", "155", "165", "22", null, null, this);
            obj.set_taborder("23");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ickey", "absolute", "148", "62", "165", "22", null, null, this);
            obj.set_taborder("17");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_loggrpcd", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("19");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctname", "absolute", "453", "93", "165", "22", null, null, this);
            obj.set_taborder("20");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pkctkey", "absolute", "148", "185", "138", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchPkcttkey", "absolute", "289", "185", "24", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "327", "212", "120", "31", null, null, this);
            obj.set_taborder("419");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("출고패턴관리");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ackey_desc", "absolute", "327", "432", "291", "22", null, null, this);
            obj.set_taborder("31");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_cdockyn", "absolute", "453", "155", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static06", "absolute", "327", "366", "120", "31", null, null, this);
            obj.set_taborder("424");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("상품세로(mm)");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pkctname", "absolute", "327", "185", "291", "22", null, null, this);
            obj.set_taborder("24");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "212", "120", "31", null, null, this);
            obj.set_taborder("435");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("피킹형태");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_pktype", "absolute", "148", "216", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_innerdataset("ds_pktype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static15", "absolute", "22", "243", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH3 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "243", "490", "31", null, null, this);
            obj.set_taborder("439");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "243", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH3");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "148", "278", "373", "22", null, null, this);
            obj.set_taborder("441");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "142", "274", "490", "31", null, null, this);
            obj.set_taborder("442");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "22", "274", "120", "31", null, null, this);
            obj.set_taborder("443");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH2 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "327", "274", "120", "31", null, null, this);
            obj.set_taborder("444");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH2");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_hcdname", "absolute", "453", "247", "165", "22", null, null, this);
            obj.set_taborder("26");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "148", "309", "373", "22", null, null, this);
            obj.set_taborder("446");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "142", "305", "490", "31", null, null, this);
            obj.set_taborder("447");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "22", "305", "120", "31", null, null, this);
            obj.set_taborder("448");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH1 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "327", "305", "120", "31", null, null, this);
            obj.set_taborder("449");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("MCH1");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_bcdname", "absolute", "453", "309", "165", "22", null, null, this);
            obj.set_taborder("30");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_bcode", "absolute", "148", "309", "165", "22", null, null, this);
            obj.set_taborder("29");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_hcode", "absolute", "148", "247", "165", "22", null, null, this);
            obj.set_taborder("25");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_mcode", "absolute", "148", "278", "165", "22", null, null, this);
            obj.set_taborder("27");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_mcdname", "absolute", "453", "278", "165", "22", null, null, this);
            obj.set_taborder("28");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "141", "336", "490", "31", null, null, this);
            obj.set_taborder("455");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "327", "336", "120", "31", null, null, this);
            obj.set_taborder("456");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("신규구분");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "336", "120", "31", null, null, this);
            obj.set_taborder("457");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("상품(용적)구분");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_newyn", "absolute", "453", "340", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("8");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_voltype", "absolute", "148", "340", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_innerdataset("ds_voltype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_obptkey", "absolute", "453", "216", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("32");
            obj.set_innerdataset("@ds_obptkey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ackey", "absolute", "148", "432", "163", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_enable("false");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_unit_width", "absolute", "148", "370", "165", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_unit_height", "absolute", "148", "401", "165", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_unit_length", "absolute", "453", "370", "165", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 526, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품별센터 수정 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_icname","value","ds_header","icname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","edt_dtgid","value","ds_header","loggrpcd_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_dckey","value","ds_header","dckey_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","ICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","ICNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","CTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","CTNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","Static50","text","gds_lang","CDOCK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","Static11","text","gds_lang","UNIT_HEIGHT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item30","Static19","text","gds_lang","ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","LOGGRPCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","DCKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item35","Static34","text","gds_lang","PKCTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","Static25","text","gds_lang","ICGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","Static55","text","gds_lang","UNIT_WIDTH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_icgrkey","value","ds_header","icgrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_ickey","value","ds_header","ickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_loggrpcd","value","ds_header","ctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","edt_ctname","value","ds_header","ctname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","edt_pkctkey","value","ds_header","pkctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","Static03","text","gds_lang","OBPTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item45","edt_ackey_desc","value","ds_header","ackey_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item47","cbo_cdockyn","value","ds_header","cdockyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Static06","text","gds_lang","UNIT_LENGTH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","edt_pkctname","value","ds_header","pkctname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","PKTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item36","cbo_pktype","value","ds_header","pktype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","HCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","HCDNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","MCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","MCDNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_hcdname","value","ds_header","h_lev_ctgname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static31","text","gds_lang","BCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item52","Static32","text","gds_lang","BCDNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item53","edt_bcdname","value","ds_header","b_lev_ctgname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item54","edt_bcode","value","ds_header","b_lev_ctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_hcode","value","ds_header","h_lev_ctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item56","edt_mcode","value","ds_header","m_lev_ctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","edt_mcdname","value","ds_header","m_lev_ctgname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static43","text","gds_lang","NEWYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item59","Static44","text","gds_lang","VOLTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_newyn","value","ds_header","newyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item61","cbo_voltype","value","ds_header","voltype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","cbo_obptkey","value","ds_header","obptkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","ICXCT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_ackey","value","ds_header","ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","mdt_unit_width","value","ds_header","unit_width");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","mdt_unit_height","value","ds_header","unit_height");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","mdt_unit_length","value","ds_header","unit_length");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("IcxctPop.xfdl", "lib::Comm.xjs");
        this.registerScript("IcxctPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : IcxctPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품별센터 수정 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ickey = "";
        this.gv_ctkey = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	this.gfn_getCode("PKTYPE", this.ds_pktype, "");
        	this.gfn_getCode("VOLTYPE", this.ds_voltype, "");
        	this.gfn_getCode("ORDSTOPTYPE", this.ds_ordstoptype, "");
        	this.gfn_getCode("ORDTYPE", this.ds_ordtype, "");
        	this.gfn_getCode("OBPTKEY", this.ds_obptkey, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_ickey    = this.gfn_isNullEmpty(this.parent.ickey);
            this.gv_ctkey    = this.gfn_isNullEmpty(this.parent.ctkey);
        	
        	this.gfn_decimalPointSet("mdt_unit_width|mdt_unit_height|mdt_unit_length");

        	
        	this.fn_search();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "icxctController");
        	this.gfn_setCommon("METHODNM", "selectIcxct");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " ickey="+this.gv_ickey
                         + " ctkey="+this.gv_ctkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "icxctController");
        	this.gfn_setCommon("METHODNM", "saveIcxct");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "OWKEY_SELECTION_LIST";
        		var value = this.gfn_getUserInfo("owkeym");
        		
        		if(this.gfn_isNotNull(this.edt_ackey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_ackey.value, "40", application.gv_ams, "", "", this.edt_ackey, this.edt_ackey_desc);
        		}
        		this.ds_header.applyChange();
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.edt_pkctname.value) && !this.gfn_isNull(this.edt_pkctkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_pkctkey.setFocus();
        		});
        		
        		return;
        	}else if(this.gfn_isNull(this.edt_ackey_desc.value) && !this.gfn_isNull(this.edt_ackey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ackey.setFocus();
        		});
        		
        		return;
        	}
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_Update();
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchPkcttkey_onclick 실행 */
        this.btn_searchPkcttkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:60
        				,putObj:this.edt_pkctkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "pkctkey"){
        		this.edt_pkctname.set_value(this.gv_Pvalue[1]);
        		this.edt_pkctname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "ackey"){
        		this.edt_ackey_desc.set_value(this.gv_Pvalue[1]);
        		this.edt_ackey_desc.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        }

        /* edt_pkctkey_onchanged 실행 */
        this.edt_pkctkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_pkctkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_pkctkey.value, "60", application.gv_ams, "", "", this.edt_pkctkey, this.edt_pkctname);
        	}
        }

        /* edt_ackey_onchanged 실행 */
        this.edt_ackey_onchanged = function(obj,e)
        {
        	var key = "OWKEY_SELECTION_LIST";
        	var value = this.gfn_getUserInfo("owkeym");
        	
        	if(this.gfn_isNotNull(this.edt_ackey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_ackey.value, "40", application.gv_ams, "", "", this.edt_ackey, this.edt_ackey_desc);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_icname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_loggrpcd.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_ctname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_pkctkey.addEventHandler("onchanged", this.edt_pkctkey_onchanged, this);
            this.btn_searchPkcttkey.addEventHandler("onclick", this.btn_searchPkcttkey_onclick, this);
            this.edt_hcdname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_mcode.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_mcdname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_ackey.addEventHandler("onchanged", this.edt_ackey_onchanged, this);

        };

        this.loadIncludeScript("IcxctPop.xfdl", true);

       
    };
}
)();
