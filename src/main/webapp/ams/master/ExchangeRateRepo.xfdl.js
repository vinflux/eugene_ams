﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ExchangeRateRepo");
                this.set_classname("ExchangeRateRepo");
                this.set_titletext("환율정보");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            obj.set_visible("true");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("22");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("23");
            obj.set_binddataset("ds_header");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"0\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"YYMMDD\"/><Cell col=\"3\" text=\"CURR_SOUR\"/><Cell col=\"4\" text=\"CURR_DEST\"/><Cell col=\"5\" text=\"NO_SEQ\"/><Cell col=\"6\" text=\"RATE_BASE\"/><Cell col=\"7\" text=\"RATE_SALE\"/><Cell col=\"8\" text=\"RATE_BUY\"/><Cell col=\"9\" text=\"INSERTDATE\"/><Cell col=\"10\" text=\"INSERTURKEY\"/><Cell col=\"11\" text=\"UPDATEDATE\"/><Cell col=\"12\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"2\" displaytype=\"date\" edittype=\"none\" style=\"align: ;\" text=\"bind:yymmdd\" mask=\"yyyy-MM-dd\" editlimit=\"50\" editdisplay=\"edit\" calendardisplaynulltype=\"none\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:curr_sour\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"4\" text=\"bind:curr_dest\"/><Cell col=\"5\" displaytype=\"number\" text=\"bind:no_seq\"/><Cell col=\"6\" displaytype=\"number\" style=\"align:right;\" text=\"bind:rate_base\"/><Cell col=\"7\" displaytype=\"number\" style=\"align:right;\" text=\"bind:rate_sale\"/><Cell col=\"8\" displaytype=\"number\" style=\"align:right;\" text=\"bind:rate_buy\"/><Cell col=\"9\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"11\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"12\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("25");
            obj.set_text("환율정보 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("1");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("ExchangeRateRepo");
            		p.set_titletext("환율정보");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00110941_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("ExchangeRateRepo.xfdl", "lib::Comm.xjs");
        this.registerScript("ExchangeRateRepo.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ExchangeRateRepo.xfdl
        * PROGRAMMER  : yachoi
        * DATE        : 2018.06.18
        * DESCRIPTION : 환율정보
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        this.searchFalg = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역 

        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gv_header.set_nodatatext("");
        	this.ds_header.clearData();
        	
        	this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        	this.gfn_setCommon("BEANID", "exchangeRateRepoController");
        	this.gfn_setCommon("METHODNM", "selectExchangeRateRepoList");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }	

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_constDsSet(this.gv_header);
        		
        		if(this.ds_header.rowcount > 0){
        			
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        		}else{
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {};
        	this.parent.div_cond.fn_excelSearch("exchangeRateRepoController/excelDown.do", this.gv_header, this, oValue);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);

        };

        this.loadIncludeScript("ExchangeRateRepo.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
