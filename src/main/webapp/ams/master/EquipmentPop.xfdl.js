﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("EquipmentPop");
                this.set_classname("style01");
                this.set_titletext("장비 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,373);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"eqname\" type=\"STRING\" size=\"256\"/><Column id=\"prtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"ipaddress\" type=\"STRING\" size=\"256\"/><Column id=\"dlcompany\" type=\"STRING\" size=\"256\"/><Column id=\"updateurKey\" type=\"STRING\" size=\"256\"/><Column id=\"inserturKey\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"dlcompany_desc\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"eqkey\" type=\"STRING\" size=\"256\"/><Column id=\"prtype\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"apkey\"/><Col id=\"eqname\"/><Col id=\"prtype_desc\"/><Col id=\"eqtype\"/><Col id=\"ipaddress\"/><Col id=\"dlcompany\"/><Col id=\"updateurKey\"/><Col id=\"inserturKey\"/><Col id=\"insertdate\"/><Col id=\"eqtype_desc\"/><Col id=\"dlcompany_desc\"/><Col id=\"closingdate\"/><Col id=\"eqkey\"/><Col id=\"prtype\"/><Col id=\"updatedate\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_eqtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_prtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_dlcompany", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("13");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "32", this);
            obj.set_taborder("11");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_eqkey", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("false");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "32", this);
            obj.set_taborder("12");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchApkey", "absolute", "334", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_apkey", "absolute", "222", "62", "110", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "213", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "213", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "155", "57", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_eqtype", "absolute", "222", "124", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_innerdataset("ds_eqtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("249");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "96", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_eqname", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_apname", "absolute", "360", "62", "183", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "832", "275", "342", "31", null, null, this);
            obj.set_taborder("252");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static10", "absolute", "638", "275", "194", "31", null, null, this);
            obj.set_taborder("253");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "216", "244", "342", "31", null, null, this);
            obj.set_taborder("254");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "22", "244", "194", "31", null, null, this);
            obj.set_taborder("255");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_closingdate", "absolute", "838", "279", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("10");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Edit("edt_ctgname00", "absolute", "222", "248", "321", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_prtype", "absolute", "222", "155", "257", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_innerdataset("ds_prtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_dlcompany", "absolute", "222", "217", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("8");
            obj.set_innerdataset("ds_dlcompany");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 373, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("장비 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_eqkey","value","ds_header","eqkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","EQUIPMENT HEADER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_apkey","value","ds_header","apkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","EQKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","EQTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","APKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","PRTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","DLCOMPANY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cbo_eqtype","value","ds_header","eqtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static06","text","gds_lang","EQNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_eqname","value","ds_header","eqname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","static10","text","gds_lang","CLOSINGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static12","text","gds_lang","IPADDRESS");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","cal_closingdate","value","ds_header","closingdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","edt_ctgname00","value","ds_header","ipaddress");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","cbo_prtype","value","ds_header","prtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","cbo_dlcompany","value","ds_header","dlcompany");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("EquipmentPop.xfdl", "lib::Comm.xjs");
        this.registerScript("EquipmentPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : EquipmentPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 장비 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_apkey = "";
        this.gv_eqkey = "";
        this.gv_prtype = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("EQTYPE", this.ds_eqtype, "");
        	this.gfn_getCode("PRTYPE", this.ds_prtype, "");
        	this.gfn_getCode("DLCOMPANY", this.ds_dlcompany, "");
        	
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_apkey  = this.gfn_isNullEmpty(this.parent.apkey);
            this.gv_eqkey  = this.gfn_isNullEmpty(this.parent.eqkey);
            this.gv_prtype = this.gfn_isNullEmpty(this.parent.prtype);
            
            this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.edt_apkey.set_enable(false);
        		this.btn_searchApkey.set_enable(false);
        		this.edt_eqkey.set_enable(false);
        		this.cbo_eqtype.set_enable(false);
        		this.cbo_prtype.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "equipmentController");
        	this.gfn_setCommon("METHODNM", "selectEquipmentForUpdate");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "apkey="+this.gv_apkey
                         + " eqkey="+this.gv_eqkey
                         + " prtype="+this.gv_prtype
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "equipmentController");
        	this.gfn_setCommon("METHODNM", "selectEquipment");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "apkey="+this.ds_header.getColumn(0, "apkey")
                         + " prtype="+this.ds_header.getColumn(0, "prtype")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "equipmentController");
        	this.gfn_setCommon("METHODNM", "saveEquipment");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "equipmentController");
        	this.gfn_setCommon("METHODNM", "saveEquipment");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_apkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_apkey.value, "10", application.gv_ams, "", "", this.edt_apkey, this.edt_apname);
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "COUNT") != 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_apkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "apkey|eqtype|prtype";
        	var sComp = "edt_apkey|cbo_eqtype|cbo_prtype";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_apkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_apname.value) && !this.gfn_isNull(this.edt_apkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_apkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") this.fn_Update();
        					}
        				}
        			});
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "apkey|eqtype|prtype";
        	var sComp = "edt_apkey|cbo_eqtype|cbo_prtype";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* btn_searchApkey_onclick 실행 */
        this.btn_searchApkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:10
        				,putObj:this.edt_apkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "apkey" || e.columnid == "eqtype" || e.columnid == "prtype"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "apkey"){
        			this.edt_apname.set_value(this.gv_Pvalue[1]);
        			this.edt_apname.set_tooltiptext(this.gv_Pvalue[1]);
        		}
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_apkey_onchanged 실행 */
        this.edt_apkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_apkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_apkey.value, "10", application.gv_ams, "", "", this.edt_apkey, this.edt_apname);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchApkey.addEventHandler("onclick", this.btn_searchApkey_onclick, this);
            this.edt_apkey.addEventHandler("onchanged", this.edt_apkey_onchanged, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.cbo_eqtype.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.cbo_prtype.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.cbo_dlcompany.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);

        };

        this.loadIncludeScript("EquipmentPop.xfdl", true);

       
    };
}
)();
