﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AddrTransPop");
                this.set_classname("style01");
                this.set_titletext("주소 전환 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,290);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_apiSelect", this);
            obj._setContents("<ColumnInfo><Column id=\"naver\" type=\"STRING\" size=\"256\"/><Column id=\"google\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "22", "120", "536", "55", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addr", "absolute", "222", "62", "221", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_maxlength("24");
            obj.set_lengthunit("utf8");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "216", "176", "342", "31", null, null, this);
            obj.set_taborder("258");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "216", "207", "342", "31", null, null, this);
            obj.set_taborder("260");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "22", "207", "194", "31", null, null, this);
            obj.set_taborder("261");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "24", "240", null, "1", "20", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lon", "absolute", "222", "212", "321", "22", null, null, this);
            obj.set_taborder("9");
            this.addChild(obj.name, obj);

            obj = new Static("static10", "absolute", "22", "176", "194", "31", null, null, this);
            obj.set_taborder("262");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lat", "absolute", "222", "180", "321", "22", null, null, this);
            obj.set_taborder("263");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "20", "174", null, "1", "22", null, this);
            obj.set_taborder("264");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "15", this);
            obj.set_taborder("265");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "15", this);
            obj.set_taborder("266");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "451", "62", null, "22", "32", null, this);
            obj.set_taborder("267");
            obj.set_text("검색");
            this.addChild(obj.name, obj);

            obj = new Radio("rdo_apiuseSel", "absolute", "29", "146", "529", "22", null, null, this);
            this.addChild(obj.name, obj);
            var rdo_apiuseSel_innerdataset = new Dataset("rdo_apiuseSel_innerdataset", this.rdo_apiuseSel);
            rdo_apiuseSel_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">1</Col><Col id=\"datacolumn\">네이버(NAVER) API</Col></Row><Row><Col id=\"codecolumn\">2</Col><Col id=\"datacolumn\">구글(GOOGLE) API</Col></Row></Rows>");
            obj.set_innerdataset(rdo_apiuseSel_innerdataset);
            obj.set_taborder("268");
            obj.set_columncount("2");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_value("1");
            obj.style.set_align("left middle");
            obj.set_index("0");

            obj = new WebBrowser("googlemap", "absolute", "4.83%", "242", null, "33", "68.79%", null, this);
            obj.set_taborder("269");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("270");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "90", "342", "31", null, null, this);
            obj.set_taborder("271");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_newAddress", "absolute", "222", "94", "321", "22", null, null, this);
            obj.set_taborder("272");
            obj.set_maxlength("24");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "4.83%", "124", null, "17", "49.83%", null, this);
            obj.set_taborder("273");
            obj.set_text("※ 원하는 API를 선택 하세요.");
            obj.style.set_color("orangered");
            obj.style.set_font("9 dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 290, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("주소 전환 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item8","static10","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","ADDR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","ADDR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_addr","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","static12","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","static05","text","gds_lang","CORRECTADDR");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("AddrTransPop.xfdl", "lib::Comm.xjs");
        this.registerScript("AddrTransPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AddrTransPop.xfdl
        * PROGRAMMER  : 장호진
        * DATE        : 2017.11.30
        * DESCRIPTION : 주소 전환 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_obj = "";
        this.gv_addrid = "";
        this.gv_objDs = "";

        this.gv_Pvalue = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_obj    = this.gfn_isNullEmpty(this.parent.putObj);
            this.gv_addrid = this.gfn_isNullEmpty(this.parent.addrid);
            this.gv_objDs  = this.gfn_isNullEmpty(this.parent.putDs);
            this.gv_lon = this.gfn_isNullEmpty(this.parent.lon);
            this.gv_lat = this.gfn_isNullEmpty(this.parent.lat);
        	
        	this.edt_addr.set_value(this.gv_addrid);
        	this.mdt_lon.set_value(this.gv_lon);
        	this.mdt_lat.set_value(this.gv_lat);
        	this.gfn_decimalPointSet("mdt_lat|mdt_lon");

        	//this.ds_header.applyChange();
        	
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var nRow = this.gv_objDs.rowposition;
        	this.gv_objDs.setColumn(nRow, "lat", this.mdt_lat.value);
        	this.gv_objDs.setColumn(nRow, "lon", this.mdt_lon.value);
        	
        	this.close();
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        /*주소변환 API호출*/
        this.btn_search_onclick = function(obj,e)
        {

        	var jspName = "";
        	
        	if(this.rdo_apiuseSel.value == 1){
        		jspName = "naverGeo.jsp";
        	}else{
        		jspName = "googleGeo.jsp";
        	}
        	
        	var sParam = "address="+this.edt_addr.value;

        	var sUrl = application.services["svc"].url + "ams/ams/addrtrans/"+jspName+"?";
        	
        	sUrl+=sParam;
        	
        	this.googlemap.set_url(sUrl);
        }

        /*결과값 callback*/
        this.googlemap_onusernotify = function(obj,e)
        {
        	
        	var nLat = e.userdata.get("lat");
        	var nLon = e.userdata.get("lon");
        	var nAddress = e.userdata.get("newaddress");
        	
        	this.edt_newAddress.set_value(nAddress);
        	this.mdt_lat.set_value(nLat);
        	this.mdt_lon.set_value(nLon);
        }

        this.edt_addr_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.btn_search_onclick();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_addr.addEventHandler("onkeydown", this.edt_addr_onkeydown, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.rdo_apiuseSel.addEventHandler("onitemclick", this.rdo_asnuseynComp_onitemclick, this);
            this.googlemap.addEventHandler("onusernotify", this.googlemap_onusernotify, this);

        };

        this.loadIncludeScript("AddrTransPop.xfdl", true);

       
    };
}
)();
