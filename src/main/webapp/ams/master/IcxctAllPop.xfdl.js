﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("IcxctAllPop");
                this.set_classname("style01");
                this.set_titletext("상품별센터 일괄수정 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,217);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"cdock\" type=\"STRING\" size=\"256\"/><Column id=\"pkctkey\" type=\"STRING\" size=\"256\"/><Column id=\"pktype\" type=\"STRING\" size=\"256\"/><Column id=\"chk_cdock\" type=\"STRING\" size=\"256\"/><Column id=\"chk_pkctkey\" type=\"STRING\" size=\"256\"/><Column id=\"chk_pktype\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"cdock\"/><Col id=\"pkctkey\"/><Col id=\"pktype\"/><Col id=\"chk_cdock\"/><Col id=\"chk_pkctkey\"/><Col id=\"chk_pktype\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_cdock", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_pktype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("6");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "22", "120", null, "31", "22", null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "22", "89", null, "31", "22", null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("7");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "22", "58", null, "31", "22", null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchPkctkey", "absolute", "518", "93", "24", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pkctkey", "absolute", "222", "93", "294", "22", null, null, this);
            obj.set_taborder("234");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_pktype", "absolute", "222", "124", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_innerdataset("ds_pktype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_cdock", "absolute", "222", "62", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("1");
            obj.set_innerdataset("ds_cdock");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("static", "absolute", "54", "58", "162", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "54", "120", "162", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "54", "89", "162", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_cdock", "absolute", "29", "63", "19", "20", null, null, this);
            obj.set_taborder("0");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_pkctkey", "absolute", "29", "94", "19", "20", null, null, this);
            obj.set_taborder("2");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_pktype", "absolute", "29", "125", "19", "20", null, null, this);
            obj.set_taborder("4");
            obj.set_value("true");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 217, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품별센터 일괄수정 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","ALL_UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_pkctkey","value","ds_header","pkctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","PKCTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","PKTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","CDOCK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cbo_pktype","value","ds_header","pktype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","cbo_cdock","value","ds_header","cdock");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","chk_cdock","value","ds_header","chk_cdock");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","chk_pkctkey","value","ds_header","chk_pkctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","chk_pktype","value","ds_header","chk_pktype");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("IcxctAllPop.xfdl", "lib::Comm.xjs");
        this.registerScript("IcxctAllPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : IcxctAllPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품별센터 일괄수정 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("YESORNO", this.ds_cdock, "");
        	this.gfn_getCode("PKTYPE", this.ds_pktype, "");
        	
            this.gv_flag = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.ds_searchList.copyData(this.parent.searchDs);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "icxctController");
        	this.gfn_setCommon("METHODNM", "updateChkIcxct");
            
            var chkList = []; 
            for(var i = 0; i < this.parent.chkDs.rowcount; i++) {
        		if(this.parent.chkDs.getColumn(i,'CHK') == 1) {
        			var chkKey = this.parent.chkDs.getColumn(i, "ordtype")+this.parent.chkDs.getColumn(i, "ickey")+
        			this.parent.chkDs.getColumn(i, "owkey")+this.parent.chkDs.getColumn(i, "ctkey")+
        			this.parent.chkDs.getColumn(i, "newyn");
        			 chkList.push(chkKey);
        		}
        	}
        	//TADMIN_MST_ICXCT 의 PK를 넘기지만 기존 params 혹시몰라 놔둠
        	//trace(chkList);
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   =  "chk_cdock="+this.ds_header.getColumn(0, "chk_cdock")
                         + " cdock="+this.ds_header.getColumn(0, "cdock")
                         + " chk_pkctkey="+this.ds_header.getColumn(0, "chk_pkctkey")
                         + " pkctkey="+this.ds_header.getColumn(0, "pkctkey")
                         + " chk_pktype="+this.ds_header.getColumn(0, "chk_pktype")
                         + " pktype="+this.ds_header.getColumn(0, "pktype")
                         + " chkList="+chkList
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        		if(flag){
        			this.fn_Update();
        		}
        	});
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	var oArg = { divId:"Single"
        				,searchId:60
        				,putObj:this.edt_pkctkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchPkctkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);

        };

        this.loadIncludeScript("IcxctAllPop.xfdl", true);

       
    };
}
)();
