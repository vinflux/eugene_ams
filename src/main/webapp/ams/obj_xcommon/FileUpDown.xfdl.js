﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("FileUpDown");
                this.set_classname("com_ftpFileUpload");
                this.set_titletext("파일관리");
                this.set_scrollbars("autoboth");
                this.set_dragscrolltype("none");
                this._setFormPosition(0,0,733,193);
            }
            this.style.set_background("transparent");
            this.style.set_font("9 Arial");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_cond", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"FILE_GROUP_ID\" size=\"100\" type=\"STRING\"/><Column id=\"FILE_ID\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_temp", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"FILE_GROUP_ID\" size=\"256\" type=\"BIGDECIMAL\"/><Column id=\"FILE_ID\" type=\"BIGDECIMAL\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("grd_ftp", "absolute", "0", "24", null, null, "0", "0", this);
            obj.set_taborder("4");
            obj.set_autofittype("col");
            obj.style.set_border("0 solid #d5d5d5ff,1 solid #d5d5d5ff,1 solid #d5d5d5ff,1 solid #d5d5d5ff");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"30\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"205\"/><Column size=\"56\"/><Column size=\"0\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\" style=\"border:1 solid #d5d5d5ff,0 solid #d5d5d5ff,0 solid #d5d5d5ff,0 solid #d5d5d5ff;\"><Cell displaytype=\"checkbox\" style=\"align:center;\"/><Cell col=\"1\" text=\"No.\"/><Cell col=\"2\" displaytype=\"normal\" text=\"파일명\"/><Cell col=\"3\" text=\"크기\"/><Cell col=\"4\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"align:center;\" text=\"bind:CHK\" suppressalign=\"first\"/><Cell col=\"1\" text=\"expr:currow+1\"/><Cell col=\"2\" text=\"bind:FILE_NAME\"/><Cell col=\"3\" text=\"bind:FILE_SIZE\"/><Cell col=\"4\" text=\"bind:SERVER_FILE_NM\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "21", null, "3", "0", null, this);
            obj.set_taborder("18");
            obj.set_text("H3");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ftpSelect", "absolute", null, "0", "21", "21", "48", null, this);
            obj.set_taborder("20");
            obj.set_cssclass("btn_WF_FileAdd");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ftpDelete", "absolute", null, "0", "21", "21", "24", null, this);
            obj.set_taborder("21");
            obj.set_cssclass("btn_WF_FileDelete");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ftpDownload", "absolute", null, "0", "21", "21", "0", null, this);
            obj.set_taborder("22");
            obj.set_cssclass("btn_WF_FileDownload");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", null, "0", "3", "21", "45", null, this);
            obj.set_taborder("25");
            obj.set_text("H3");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", null, "0", "3", "21", "21", null, this);
            obj.set_taborder("26");
            obj.set_text("H3");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new FileDownload("ftpFileDownload", "absolute", "152", "3", "15", "15", null, null, this);
            obj.set_taborder("8");
            obj.getSetter("retry").set("0");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new FileUpload("ftpFileUpload", "absolute", "129", "3", "15", "15", null, null, this);
            obj.set_taborder("5");
            obj.getSetter("retry").set("0");
            obj.set_multiselect("true");
            obj.set_itemcount("0");
            obj.set_visible("false");
            obj.style.set_border("1 solid #808080ff");
            obj.set_index("0");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ftpOpen", "absolute", null, "0", "21", "21", "72", null, this);
            obj.set_taborder("27");
            obj.set_cssclass("btn_WF_FileExecute");
            obj.style.set_image("URL('img::btn_WF_FileExecute.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", null, "0", "3", "21", "69", null, this);
            obj.set_taborder("28");
            obj.set_text("H3");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 733, 193, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("com_ftpFileUpload");
            		p.set_titletext("파일관리");
            		p.style.set_background("transparent");
            		p.style.set_font("9 Arial");
            		p.set_scrollbars("autoboth");
            		p.set_dragscrolltype("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("FileUpDown.xfdl", "lib::Comm.xjs");
        this.registerScript("FileUpDown.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : FileUpDown.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.08.01
        * DESCRIPTION : FileUpDown 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.fv_dsFile;
        this.fv_oCallBack;
        this.fv_oForm;
        this.fv_moduleId;
        this.fv_fileGroupId = 0;

        this.fv_allowTypes = ["jpg","jpeg","gif","png","bmp","txt","zip","7z","gzip","doc","docx","ppt","pptx","xls","xlsx","pdf"];

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e){
        	this.gfn_initForm(obj);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_searchFile = function(file_group_id)
        {
        	if(this.gfn_isNull(file_group_id)){
        		this.fv_dsFile.clearData();
        		return false;
        	}else{
        		this.fv_fileGroupId = file_group_id;
        	}
        	
        	this.ds_cond.clearData();
        	this.ds_cond.addRow();
        	this.ds_cond.setColumn(0, "FILE_GROUP_ID", this.fv_fileGroupId);
        	
        	var sSvcId = "searchFile";
        	var sSvcUrl = "actID=BRS_AMF_RetrieveFile&baRq=IN_FILE&baRs=OUT_FILE";
        	var sInData = "IN_FILE=ds_cond";
        	var sOutData = this.fv_dsFile.name+"=OUT_FILE";
        	var sParam = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        //파일 저장
        this.fn_fileSave = function()
        {
        	var strUrl = application.services["Svc"].url;
        	var strCmdUrl = "/service/fileSave/nexacro.ncd?";
        	var url = strUrl + strCmdUrl;
        	
        	var sSvcId = "saveFile";
        	var sSvcUrl = url + "actID=BRS_AMF_InsertFile&baRq=IN_FILE&baRs=OUT_FILE";
        	var sInData = "IN_FILE="+this.fv_dsFile.name+":U";
        	var sOutData = "ds_save=OUT_FILE";
        	var sParam = "";
        	
        	this.transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, 2, false);
        }

        //파일다운
        this.fn_fileDown = function()
        {
        	var nChkCnt = this.fv_dsFile.getCaseCount("CHK=='1'");
        	
        	if(nChkCnt < 1){
        		this.gfn_alert("selectFile");
        		return;
        	}
        	
        	var strUrl = application.services["Svc"].url;
        	var strCmdUrl = "service/fileDownload.ncd?";
        	var strParam = "";
        	var url = "";
        	
        	for(var i = 0 ; i < this.fv_dsFile.getRowCount() ; i++){
        		if(this.fv_dsFile.getColumn(i,"CHK") == '1' && !this.gfn_isNull(this.fv_dsFile.getColumn(i, "FILE_PATH")) && !this.gfn_isNull(this.fv_dsFile.getColumn(i, "FILE_NAME"))){
        			strParam = "FILE_PATH=" + this.fv_dsFile.getColumn(i, "FILE_PATH")
        					+"&FILE_NAME=" + this.fv_dsFile.getColumn(i, "FILE_NAME")
        					;
        			
        			url = strUrl + strCmdUrl + strParam;
        			
        			this.ftpFileDownload.set_downloadfilename(this.fv_dsFile.getColumn(i, "FILE_NAME"));
        			this.ftpFileDownload.set_downloadurl(url);
        			this.ftpFileDownload.download();
        		}
        	}
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(svcId,errorCode,errorMsg)
        {
        	if(errorCode < 0){
        		return;
        	}
        	
        	if(svcId == "searchFile"){
        		
        	}else if(svcId == "saveFile"){
        		if(this.fv_dsFile.getRowCount() != 0){
        			for(var i = 0 ; i < this.fv_dsFile.getRowCount() ; i++){
        				this.fv_dsFile.setColumn(i, "FILE_GROUP_ID", this.ds_save.getColumn(0, "FILE_GROUP_ID"));
        			}
        		}
        		
        		this.fv_oCallBack.call(this.fv_oForm, "file_save", true);
        	}else if(svcId == "saveDownAll"){
        		
        	}else if(svcId == "saveDown"){
        		
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /**
         * @class 폼경로로 업/다운로드 경로를 설정하여 파일매니저 초기셋팅
         * @param objForm: 파일매니저가 존재하는 폼의 ChildFrame , 파일매니저를 사용활 화면에서 this.getOwnerFrame() 를 넘기면된다.
         * @param objDsFile: 파일과 연동시킬 데이터셋
         * @return None
         */
        this.fn_initFile = function(objForm,objDsFile,moduleId,oCallback)
        {	
        	this.fv_oForm 	 = objForm;
        	this.fv_dsFile 	 = objDsFile;
        	this.fv_moduleId  = moduleId;
        	this.fv_oCallBack = oCallback;
        	
        	this.grd_ftp.set_binddataset(this.fv_dsFile.name);
        }

        //파일업로드
        this.fn_uploadFile = function()
        {
        	var strUrl = application.services["Svc"].url;
        	var strCmdUrl = "service/fileUpload.ncd?";
        	var strParam = "moduleId=" + this.fv_moduleId  // Setup, Basic, Fixed, Inven, Arcon
        	            ;
        	
        	var url = strUrl + strCmdUrl + strParam;
        	
        	trace("url = "+url);
        	//파일이 추가된 경우 업로드수행
        	if (this.fv_dsFile.findRowExpr("this.getRowType(rowidx)==Dataset.ROWTYPE_INSERT") > -1){
        		var bSucc = this.ftpFileUpload.upload(url);
        	}else{
        		this.fn_fileSave();
        	}
        }

        //바로열기
        this.fn_DirectOpen = function()
        {
        	var nChkCnt = this.fv_dsFile.getCaseCount("CHK=='1'");
        	
        	if(nChkCnt < 1){
        		this.gfn_alert("selectFile");
        		return;
        	}
        	
        	var strUrl = application.services["Svc"].url;
        	var strCmdUrl = "service/fileDownload.ncd?";
        	var strParam = "";
        	var url = "";
        	
        	for(var i = 0 ; i < this.fv_dsFile.getRowCount() ; i++){
        		if(this.fv_dsFile.getColumn(i,"CHK") == '1' && !this.gfn_isNull(this.fv_dsFile.getColumn(i, "FILE_PATH")) && !this.gfn_isNull(this.fv_dsFile.getColumn(i, "FILE_NAME"))){
        			strParam = "FILE_PATH=" + this.fv_dsFile.getColumn(i, "FILE_PATH")
        					+"&FILE_NAME=" + this.fv_dsFile.getColumn(i, "FILE_NAME")
        					;
        			
        			url = strUrl + strCmdUrl + strParam;
        			
        			try{
        				system.execBrowser(encodeURI(url));
        			} catch (e){
        				trace(e);
        				this.gfn_alert("failDownloadFile");
        			}
        		}
        	}
        }

        this.fn_fileClear = function()
        {
        	//파일업로드컴포넌트 초기화
        	var nLoopCnt = this.ftpFileUpload.getItemCount();
        	
        	for(var i = nLoopCnt-1; i >= 0; i--){
        		this.ftpFileUpload.deleteItem(i);
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_upload_onclick 실행 */
        this.btn_upload_onclick = function(obj,e)
        {
        	this.fn_uploadFile();
        }

        /* grd_ftp_onheadclick 실행 */
        this.grd_ftp_onheadclick = function(obj,e)
        {
        	if(e.col == 1) this.gfn_gridProc(obj, e);
        }

        //파일선택
        this.btn_ftpSelect_onclick = function(obj,e)
        {
        	//trace("btn_ftpSelect_onclick = "+obj.name);
        	this.ftpFileUpload.appendItem();
        	var nFileCnt = this.ftpFileUpload.getItemCount() - 1;
        	this.ftpFileUpload._items[nFileCnt].fileitembutton.click();
        }

        //파일업로드 아이템 변경
        this.ftpFileUpload_onitemchanged = function(obj,e)
        {
        	trace("ftpFileUpload_onitemchanged = "+obj.value);
        	
        	var sFiles = obj.value;
        	
        	// 파일 삭제 처리
        	if(sFiles == null || sFiles == "" ){
        		obj.deleteItem(obj.index);
        		return;
        	}
        	
        	var aFiles = sFiles.split(",");
        	var sFileName;
        	var sDirExpt;
        	var nLastIdx;
        	var sFilePath;
        	var sFileExt;
        	var bContain = false;
        	
        	for(var i = 0; i < aFiles.length; i++){
        		sFilePath = aFiles[i];
        		bContain = false;
        		
        		//확장자 체크
        		nLastIdx = sFilePath.lastIndexOf(".");
        		sFileExt = sFilePath.substring(nLastIdx+1);
        		sFileExt = sFileExt.toLowerCase();
        		
        		for(var i = 0 ; i < this.fv_allowTypes.length ; i++){
        			if(this.fv_allowTypes[i] == sFileExt){
        				bContain = true;
        				break;
        			}
        		}
        		
        		if(!bContain){
        			this.gfn_alert("The File extension must be jpg,jpeg,gif,png,bmp,txt,zip,7z,gzip,doc,docx,ppt,pptx,xls,xlsx,pdf");
        			obj.deleteItem(obj.index);
        			return;
        		}
        		
        		nLastIdx = sFilePath.lastIndexOf("\\");	
        		
        		trace("sFilePath = " + sFilePath);
        		
        		//로컬경로가 있는 경우
        		if(nLastIdx >= 0){
        			sFileName = sFilePath.substring(nLastIdx+1);
        			trace("FilePath exist = "+sFileName);
        		}else{//로컬경로가 없는 경우
        			sFileName = sFilePath;
        			trace("FilePath not exist = "+sFileName);
        		}
        		
        		//파일명이 동일한경우 업로드 불가
        		if(this.fv_dsFile.findRow("FILE_NAME", sFileName) > -1){
        			this.gfn_alert("checkFileDuplicate");
        			obj.deleteItem(obj.index);
        			return;
        		}else{
        			var nRow = this.fv_dsFile.addRow();
        			this.fv_dsFile.setColumn(nRow, "CUD", '1');
        			this.fv_dsFile.setColumn(nRow, "FILE_NAME", sFileName);
        		}
        	}
        }

        // 파일업로드 전송성공
        this.ftpFileUpload_onsuccess = function(obj,e)
        {
        	//file upload error
        	if( e.datasets == null || e.errorcode == '500' ){
        		this.gfn_alert("failUploadFile");
        		this.fv_oCallBack.call(this.fv_oForm, "file_save", false);
        	}else{
        		this.ds_temp.copyData(e.datasets[0]);
        		
        //		trace(this.ds_temp.saveXML());
        		
        		var sFileNm;
        		var nRow;
        		
        		for(var i = 0; i < this.ds_temp.rowcount; i++){
        			sFileNm = this.ds_temp.getColumn(i, "CLIENT_FILENAME");
        			nRow = this.fv_dsFile.findRow("FILE_NAME", sFileNm);
        			
        			if(nRow > -1){
        				this.fv_dsFile.setColumn(nRow, "FILE_GROUP_ID", this.fv_fileGroupId);
        				this.fv_dsFile.setColumn(nRow, "FILE_ID", 0);
        				this.fv_dsFile.setColumn(nRow, "MODULE_CODE", this.fv_moduleId);
        				this.fv_dsFile.setColumn(nRow, "SERVER_FILE_NM", this.ds_temp.getColumn(i, "SERVER_FILENAME"));
        				this.fv_dsFile.setColumn(nRow, "FILE_PATH", this.ds_temp.getColumn(i, "SERVER_PATH"));
        				this.fv_dsFile.setColumn(nRow, "FILE_SIZE", this.ds_temp.getColumn(i, "FILE_SIZE"));
        			}
        		}
        		
        //		trace(this.fv_dsFile.saveXML());
        		
        		this.ds_temp.clear();
        		
        		//파일업로드컴포넌트 초기화
        		var nLoopCnt = this.ftpFileUpload.getItemCount();
        		
        		for(var i = nLoopCnt-1; i >= 0; i--){
        			this.ftpFileUpload.deleteItem(i);
        		}
        		
        		this.fn_fileSave();
        	}
        }

        //오픈
        this.btn_ftpOpen_onclick = function(obj,e){
        	this.fn_DirectOpen();
        }

        //파일다운로드
        this.btn_ftpDownload_onclick = function(obj,e)
        {
        	this.fn_fileDown();
        }

        //파일삭제
        this.btn_ftpDelete_onclick = function(obj,e){
        	var nDelCnt = this.fv_dsFile.getCaseCount("CHK=='1'");
        	
        	if(nDelCnt < 1){
        		this.gfn_alert("Check deleteFile");
        		return;
        	}

        	for(var i = this.fv_dsFile.rowcount - 1 ; i >= 0 ; i--){
        		if(this.fv_dsFile.getColumn(i, "CHK") == '1'){
        			this.ftpFileUpload.deleteItem(i);
        			this.fv_dsFile.deleteRow(i);
        		}
        	}
        }

        //파일 다운로드
        this.grd_ftp_oncelldblclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.fv_dsFile.getColumn(e.row, "FILE_PATH")) || this.gfn_isNull(this.fv_dsFile.getColumn(e.row, "FILE_NAME"))){
        		return;
        	}
        	
        	var strUrl = application.services["Svc"].url;
        	var strCmdUrl = "service/fileDownload.ncd?";
        	var strParam = "FILE_PATH=" + this.fv_dsFile.getColumn(e.row, "FILE_PATH")
        	            +"&FILE_NAME=" + this.fv_dsFile.getColumn(e.row, "FILE_NAME")
        	            ;
        	
        	var url = strUrl + strCmdUrl + strParam;
        	
        	this.ftpFileDownload.set_downloadfilename(this.fv_dsFile.getColumn(e.row, "FILE_NAME"));
        	this.ftpFileDownload.set_downloadurl(url);
        	this.ftpFileDownload.download();
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.grd_ftp.addEventHandler("onheadclick", this.grd_ftp_onheadclick, this);
            this.grd_ftp.addEventHandler("oncelldblclick", this.grd_ftp_oncelldblclick, this);
            this.btn_ftpSelect.addEventHandler("onclick", this.btn_ftpSelect_onclick, this);
            this.btn_ftpDelete.addEventHandler("onclick", this.btn_ftpDelete_onclick, this);
            this.btn_ftpDownload.addEventHandler("onclick", this.btn_ftpDownload_onclick, this);
            this.ftpFileDownload.addEventHandler("onerror", this.ftpFileDownload_onerror, this);
            this.ftpFileUpload.addEventHandler("onitemchanged", this.ftpFileUpload_onitemchanged, this);
            this.ftpFileUpload.addEventHandler("onsuccess", this.ftpFileUpload_onsuccess, this);
            this.btn_ftpOpen.addEventHandler("onclick", this.btn_ftpOpen_onclick, this);

        };

        this.loadIncludeScript("FileUpDown.xfdl", true);

       
    };
}
)();
