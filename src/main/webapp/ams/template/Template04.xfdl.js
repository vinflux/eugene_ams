﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"256\"/><Column id=\"Head1\" type=\"STRING\" size=\"256\"/><Column id=\"Head2\" type=\"STRING\" size=\"256\"/><Column id=\"Head3\" type=\"STRING\" size=\"256\"/><Column id=\"Head4\" type=\"DATE\" size=\"256\"/><Column id=\"Head5\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("Button02", "absolute", null, "322", "33", "24", "35", null, this);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "30", "351", null, null, "35", "54", this);
            obj.set_taborder("18");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "33", "325", "350", "20", null, null, this);
            obj.set_taborder("42");
            obj.set_text("상세 현황");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Button("Button10", "absolute", null, "322", "33", "24", "72", null, this);
            obj.set_taborder("43");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.addChild(obj.name, obj);

            obj = new Button("Button11", "absolute", null, "322", "33", "24", "109", null, this);
            obj.set_taborder("44");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Tab("Tab00", "absolute", "30", "10", null, "302", "34", null, this);
            obj.set_taborder("65");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.Tab00);
            obj.set_text("로케이션별");
            obj.set_url("template::Template04_tab01.xfdl");
            obj.set_scrollbars("none");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.Tab00);
            obj.set_text("상품별");
            obj.set_scrollbars("none");
            this.Tab00.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", null, "327", "55", "18", "254", null, this);
            obj.set_taborder("66");
            obj.set_text("·상품 코드");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", null, "324", "86", "22", "163", null, this);
            obj.set_taborder("67");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", null, "324", "86", "22", "321", null, this);
            obj.set_taborder("68");
            this.addChild(obj.name, obj);

            obj = new Static("label01", "absolute", null, "327", "80", "18", "411", null, this);
            obj.set_taborder("69");
            obj.set_text("·로케이션 코드");
            this.addChild(obj.name, obj);

            obj = new Div("Paging", "absolute", "30", null, null, "29", "35", "26", this);
            obj.set_taborder("70");
            obj.set_scrollbars("none");
            obj.set_url("template::Paging.xfdl");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "30", "0", "200", "10", null, null, this);
            obj.set_taborder("72");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "30", "341", "200", "10", null, null, this);
            obj.set_taborder("73");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "995", "34", "200", "5", null, null, this);
            obj.set_taborder("75");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "994", "63", "200", "5", null, null, this);
            obj.set_taborder("76");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "994", "346", "200", "5", null, null, this);
            obj.set_taborder("77");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "30", "312", "1168", "10", null, null, this);
            obj.set_taborder("78");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "601", "34", "7", "55", null, null, this);
            obj.set_taborder("79");
            obj.set_text("w7");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static76", "absolute", "0", "0", "30", "627", null, null, this);
            obj.set_taborder("80");
            obj.set_text("w30");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "30", "601", "200", "26", null, null, this);
            obj.set_taborder("81");
            obj.set_text("H26");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "template::Template04_tab01.xfdl");
            this._addPreloadList("fdl", "template::Paging.xfdl");
        };
        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Static11.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static76.addEventHandler("onclick", this.Static76_onclick, this);

        };

        this.loadIncludeScript("Template04.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
