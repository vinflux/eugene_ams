﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,614,590);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"256\"/><Column id=\"Head1\" type=\"STRING\" size=\"256\"/><Column id=\"Head2\" type=\"STRING\" size=\"256\"/><Column id=\"Head3\" type=\"STRING\" size=\"256\"/><Column id=\"Head4\" type=\"DATE\" size=\"256\"/><Column id=\"Head5\" type=\"STRING\" size=\"256\"/><Column id=\"Head6\" type=\"STRING\" size=\"256\"/><Column id=\"Head7\" type=\"INT\" size=\"256\"/><Column id=\"Head8\" type=\"STRING\" size=\"256\"/><Column id=\"Head9\" type=\"INT\" size=\"256\"/><Column id=\"Head10\" type=\"INT\" size=\"256\"/><Column id=\"Head11\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0009</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0010</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0011</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0012</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0013</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0014</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0015</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0016</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0017</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0018</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "0", "5", null, null, "0", "166", this);
            obj.set_taborder("24");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("paging00", "absolute", "0", null, null, "129", "0", "0", this);
            obj.set_taborder("29");
            obj.set_cssclass("sta_WF_sum");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "13", null, "95", "20", null, "101", this);
            obj.set_taborder("30");
            obj.set_text("Summary");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("label02", "absolute", "13", null, "85", "18", null, "72", this);
            obj.set_taborder("31");
            obj.set_text("출고공통코드");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "96", null, "180", "22", null, "70", this);
            obj.set_taborder("32");
            this.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", "315", null, "110", "18", null, "72", this);
            obj.set_taborder("33");
            obj.set_text("작업생성/부분할당");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "422", null, "180", "22", null, "70", this);
            obj.set_taborder("34");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "422", null, "180", "22", null, "41", this);
            obj.set_taborder("35");
            this.addChild(obj.name, obj);

            obj = new Static("label01", "absolute", "315", null, "110", "18", null, "43", this);
            obj.set_taborder("36");
            obj.set_text("피킹");
            this.addChild(obj.name, obj);

            obj = new Static("label03", "absolute", "13", null, "85", "18", null, "43", this);
            obj.set_taborder("38");
            obj.set_text("할당/부분피킹");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit04", "absolute", "422", null, "180", "22", null, "13", this);
            obj.set_taborder("39");
            this.addChild(obj.name, obj);

            obj = new Static("label04", "absolute", "315", null, "110", "18", null, "14", this);
            obj.set_taborder("40");
            obj.set_text("출고 완료");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "96", null, "180", "22", null, "13", this);
            obj.set_taborder("41");
            this.addChild(obj.name, obj);

            obj = new Static("label05", "absolute", "13", null, "85", "18", null, "14", this);
            obj.set_taborder("42");
            obj.set_text("배차계획");
            this.addChild(obj.name, obj);

            obj = new Div("Paging", "absolute", "0", null, null, "29", "0", "137", this);
            obj.set_taborder("43");
            obj.set_scrollbars("none");
            obj.set_url("template::Paging.xfdl");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "96", null, "180", "22", null, "41", this);
            obj.set_taborder("44");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 614, 590, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "template::Paging.xfdl");
        };
        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("Template05_tab01.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
