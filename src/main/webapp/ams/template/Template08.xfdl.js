﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,500,170);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/><Column id=\"Head1\" type=\"STRING\" size=\"300\"/><Column id=\"Head2\" type=\"STRING\" size=\"300\"/><Column id=\"Head3\" type=\"STRING\" size=\"300\"/><Column id=\"Head4\" type=\"DATE\" size=\"300\"/><Column id=\"Head5\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("FILE UPLOAD");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", null, "114", "62", "28", "219", null, this);
            obj.set_taborder("121");
            obj.set_text("업로드");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "55", "456", "44", null, null, this);
            obj.set_taborder("122");
            obj.style.set_border("3 solid #f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "83", "66", "320", "22", null, null, this);
            obj.set_taborder("123");
            this.addChild(obj.name, obj);

            obj = new Button("Button03", "absolute", "409", "66", "59", "22", null, null, this);
            obj.set_taborder("124");
            obj.set_text(" 검색");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("label01", "absolute", "35", "68", "45", "18", null, null, this);
            obj.set_taborder("125");
            obj.set_text("· FILE");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 500, 170, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);

        };

        this.loadIncludeScript("Template08.xfdl", true);

       
    };
}
)();
