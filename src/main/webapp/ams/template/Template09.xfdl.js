﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,900,400);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/><Column id=\"Head1\" type=\"STRING\" size=\"300\"/><Column id=\"Head2\" type=\"STRING\" size=\"300\"/><Column id=\"Head3\" type=\"STRING\" size=\"300\"/><Column id=\"Head4\" type=\"DATE\" size=\"300\"/><Column id=\"Head5\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static21", "absolute", "148", "241", "731", "31", null, null, this);
            obj.set_taborder("289");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "148", "210", "731", "31", null, null, this);
            obj.set_taborder("287");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "148", "179", "731", "31", null, null, this);
            obj.set_taborder("286");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "79", "736", "31", null, null, this);
            obj.set_taborder("246");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", null, "344", "52", "28", "409", null, this);
            obj.set_taborder("121");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "78", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "79", "125", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_text("ASN코드<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "22", "110", "125", "31", null, null, this);
            obj.set_taborder("167");
            obj.set_text("전표번호");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "147", "110", "731", "31", null, null, this);
            obj.set_taborder("237");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit09", "absolute", "153", "114", "143", "22", null, null, this);
            obj.set_taborder("238");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "153", "83", "253", "22", null, null, this);
            obj.set_taborder("242");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "28", "53", "350", "20", null, null, this);
            obj.set_taborder("245");
            obj.set_text("입고지시");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "435", "79", "125", "31", null, null, this);
            obj.set_taborder("247");
            obj.set_text("화주코드<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "566", "83", "301", "22", null, null, this);
            obj.set_taborder("248");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "441", "114", "143", "22", null, null, this);
            obj.set_taborder("249");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "320", "110", "115", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_text("전표순번");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "608", "110", "115", "31", null, null, this);
            obj.set_taborder("252");
            obj.set_text("작업상태<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo04", "absolute", "728", "114", "139", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("253");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Static("Static06", "absolute", "22", "140", null, "1", "22", null, this);
            obj.set_taborder("254");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "23", "178", null, "1", "22", null, this);
            obj.set_taborder("257");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "23", "210", "125", "31", null, null, this);
            obj.set_taborder("259");
            obj.set_text("상품대체코드");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "154", "214", "158", "22", null, null, this);
            obj.set_taborder("261");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "28", "153", "350", "20", null, null, this);
            obj.set_taborder("263");
            obj.set_text("품목");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "321", "210", "115", "31", null, null, this);
            obj.set_taborder("267");
            obj.set_text("초기수량<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "609", "210", "115", "31", null, null, this);
            obj.set_taborder("268");
            obj.set_text("예정수량<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "23", "179", "125", "31", null, null, this);
            obj.set_taborder("271");
            obj.set_text("상품코드<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit04", "absolute", "154", "183", "132", "22", null, null, this);
            obj.set_taborder("272");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "321", "179", "115", "31", null, null, this);
            obj.set_taborder("273");
            obj.set_text("상품단위코드<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "609", "179", "115", "31", null, null, this);
            obj.set_taborder("275");
            obj.set_text("단위(UOM)<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo01", "absolute", "729", "183", "138", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("276");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Button("Button03", "absolute", "288", "183", "24", "22", null, null, this);
            obj.set_taborder("278");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "576", "183", "24", "22", null, null, this);
            obj.set_taborder("279");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "442", "183", "132", "22", null, null, this);
            obj.set_taborder("280");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin00", "absolute", "442", "214", "160", "22", null, null, this);
            obj.set_taborder("281");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin01", "absolute", "729", "214", "138", "22", null, null, this);
            obj.set_taborder("282");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin02", "absolute", "154", "245", "160", "22", null, null, this);
            obj.set_taborder("283");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin03", "absolute", "442", "245", "160", "22", null, null, this);
            obj.set_taborder("284");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit06", "absolute", "729", "245", "138", "22", null, null, this);
            obj.set_taborder("285");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "23", "241", "125", "31", null, null, this);
            obj.set_taborder("288");
            obj.set_text("확정수량");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "321", "241", "115", "31", null, null, this);
            obj.set_taborder("290");
            obj.set_text("미납");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "609", "241", "115", "31", null, null, this);
            obj.set_taborder("291");
            obj.set_text("상품단위수량");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "148", "303", "731", "31", null, null, this);
            obj.set_taborder("292");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "148", "272", "731", "31", null, null, this);
            obj.set_taborder("293");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "23", "303", "125", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_text("UDF");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "321", "303", "115", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_text("생산일자");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "609", "303", "115", "31", null, null, this);
            obj.set_taborder("298");
            obj.set_text("유통기한");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "23", "272", "125", "31", null, null, this);
            obj.set_taborder("299");
            obj.set_text("TO로케이션코드<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit08", "absolute", "154", "276", "132", "22", null, null, this);
            obj.set_taborder("300");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "321", "272", "115", "31", null, null, this);
            obj.set_taborder("301");
            obj.set_text("TO ULC코드");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "609", "272", "115", "31", null, null, this);
            obj.set_taborder("302");
            obj.set_text("생산 LOT");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo00", "absolute", "729", "276", "138", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("303");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Button("Button01", "absolute", "288", "276", "24", "22", null, null, this);
            obj.set_taborder("304");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "576", "276", "24", "22", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit10", "absolute", "442", "276", "132", "22", null, null, this);
            obj.set_taborder("306");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin06", "absolute", "154", "307", "160", "22", null, null, this);
            obj.set_taborder("309");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            this.addChild(obj.name, obj);

            obj = new Calendar("Calendar07", "absolute", "442", "307", "158", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("310");

            obj = new Calendar("Calendar00", "absolute", "729", "307", "138", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("311");

            obj = new Static("Static19", "absolute", "23", "333", null, "1", "22", null, this);
            obj.set_taborder("270");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 900, 400, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Template09.xfdl", function(exports) {

        this.Static19_onclick = function(obj,e)
        {
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.Static19.addEventHandler("onclick", this.Static19_onclick, this);

        };

        this.loadIncludeScript("Template09.xfdl", true);

       
    };
}
)();
