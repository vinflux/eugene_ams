﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,915,800);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\"/></Row><Row><Col id=\"NO\"/></Row><Row><Col id=\"NO\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static21", "absolute", "22", "345", "405", "381", null, null, this);
            obj.set_taborder("362");
            obj.set_cssclass("sta_WF_bg2");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "22", "79", "405", "231", null, null, this);
            obj.set_taborder("246");
            obj.set_cssclass("sta_WF_bg2");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "153", "213", "253", "56", null, null, this);
            obj.set_taborder("322");
            this.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", "384", null, "52", "28", null, "28", this);
            obj.set_taborder("121");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "153", "89", "253", "22", null, null, this);
            obj.set_taborder("242");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "28", "53", "350", "20", null, null, this);
            obj.set_taborder("245");
            obj.set_text("전략 정보");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo01", "absolute", "153", "355", "150", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("276");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Button("Button03", "absolute", "382", "182", "24", "22", null, null, this);
            obj.set_taborder("278");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "23", "86", "125", "31", null, null, this);
            obj.set_taborder("313");
            obj.set_text("전략 유형");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "153", "120", "253", "22", null, null, this);
            obj.set_taborder("314");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "23", "117", "125", "31", null, null, this);
            obj.set_taborder("315");
            obj.set_text("전략 코드");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "153", "151", "253", "22", null, null, this);
            obj.set_taborder("316");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "23", "148", "125", "31", null, null, this);
            obj.set_taborder("317");
            obj.set_text("전략 설명");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "23", "179", "125", "31", null, null, this);
            obj.set_taborder("318");
            obj.set_text("전략 마스터 코드");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit07", "absolute", "153", "182", "227", "22", null, null, this);
            obj.set_taborder("319");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "23", "210", "125", "31", null, null, this);
            obj.set_taborder("320");
            obj.set_text("전략 상세 설명");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "23", "275", "125", "31", null, null, this);
            obj.set_taborder("323");
            obj.set_text("프로세스 순번");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "153", "278", null, "22", "509", null, this);
            obj.set_taborder("324");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "447", "79", "447", "231", null, null, this);
            obj.set_taborder("325");
            obj.set_cssclass("sta_WF_bg2");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "452", "53", "350", "20", null, null, this);
            obj.set_taborder("328");
            obj.set_text("체크 속성");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static76", "absolute", "0", "0", "22", "627", null, null, this);
            obj.set_taborder("340");
            obj.set_text("w22");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "427", "79", "20", "231", null, null, this);
            obj.set_taborder("341");
            obj.set_text("w20");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", null, "0", "22", "627", "0", null, this);
            obj.set_taborder("342");
            obj.set_text("w22");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox00", "absolute", "459", "86", "143", "25", null, null, this);
            obj.set_taborder("343");
            obj.set_text("Product Date Check");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "618", "89", "34", "22", null, null, this);
            obj.set_taborder("344");
            obj.set_text("MIN:");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "714", "89", "34", "22", null, null, this);
            obj.set_taborder("345");
            obj.set_text("Day ~");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "854", "89", "28", "22", null, null, this);
            obj.set_taborder("346");
            obj.set_text("Day");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "752", "89", "34", "22", null, null, this);
            obj.set_taborder("348");
            obj.set_text("MAX:");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox01", "absolute", "459", "117", "143", "25", null, null, this);
            obj.set_taborder("350");
            obj.set_text("Expiry Date Check");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "618", "120", "34", "22", null, null, this);
            obj.set_taborder("351");
            obj.set_text("MIN:");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "714", "120", "34", "22", null, null, this);
            obj.set_taborder("352");
            obj.set_text("Day ~");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "854", "120", "28", "22", null, null, this);
            obj.set_taborder("353");
            obj.set_text("Day");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "752", "120", "34", "22", null, null, this);
            obj.set_taborder("355");
            obj.set_text("MAX:");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox02", "absolute", "459", "148", "143", "25", null, null, this);
            obj.set_taborder("357");
            obj.set_text("Use Attribute 1");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox03", "absolute", "459", "179", "143", "25", null, null, this);
            obj.set_taborder("358");
            obj.set_text("Use Attribute 2");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox04", "absolute", "459", "210", "143", "25", null, null, this);
            obj.set_taborder("359");
            obj.set_text("Use Attribute 3");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox05", "absolute", "459", "241", "143", "25", null, null, this);
            obj.set_taborder("360");
            obj.set_text("Use Attribute 4");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox06", "absolute", "459", "272", "143", "25", null, null, this);
            obj.set_taborder("361");
            obj.set_text("Use Attribute 5");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "28", "319", "350", "20", null, null, this);
            obj.set_taborder("365");
            obj.set_text("일반 로직");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "23", "352", "125", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_text("최소 할당 규격");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "447", "345", "447", "381", null, null, this);
            obj.set_taborder("377");
            obj.set_cssclass("sta_WF_bg2");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "452", "319", "350", "20", null, null, this);
            obj.set_taborder("379");
            obj.set_text("Allocation in specific areas");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "427", "345", "20", "231", null, null, this);
            obj.set_taborder("380");
            obj.set_text("w20");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox09", "absolute", "34", "389", "155", "25", null, null, this);
            obj.set_taborder("394");
            obj.set_text("규격 단위 수량 할당");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox10", "absolute", "199", "389", "155", "25", null, null, this);
            obj.set_taborder("395");
            obj.set_text("패킹 보존 여부");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "23", "420", "143", "31", null, null, this);
            obj.set_taborder("396");
            obj.set_text("로케이션 유형 적용");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00", "absolute", "32", "451", null, "168", "645", null, this);
            obj.set_taborder("399");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\"/><Column size=\"198\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" text=\"로케이션 타입\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("Button10", "absolute", "199", "422", "33", "24", null, null, this);
            obj.set_taborder("400");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "237", "422", "33", "24", null, null, this);
            obj.set_taborder("401");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "23", "628", "125", "31", null, null, this);
            obj.set_taborder("402");
            obj.set_text("할당 순서1");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo00", "absolute", "153", "631", "150", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("403");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Combo("Combo02", "absolute", "305", "631", "103", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("404");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Combo("Combo03", "absolute", "305", "662", "103", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("405");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Combo("Combo04", "absolute", "153", "662", "150", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("406");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Static("Static26", "absolute", "23", "659", "125", "31", null, null, this);
            obj.set_taborder("407");
            obj.set_text("할당 순서2");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo05", "absolute", "305", "693", "103", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("408");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Combo("Combo06", "absolute", "153", "693", "150", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("409");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Static("Static27", "absolute", "23", "690", "125", "31", null, null, this);
            obj.set_taborder("410");
            obj.set_text("할당 순서3");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "450", "352", "131", "31", null, null, this);
            obj.set_taborder("411");
            obj.set_text("특정 구역 포함");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "591", "354", "33", "24", null, null, this);
            obj.set_taborder("412");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "629", "354", "33", "24", null, null, this);
            obj.set_taborder("413");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "459", "383", null, "198", "252", null, this);
            obj.set_taborder("414");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\"/><Column size=\"170\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" text=\"구역\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid02", "absolute", "674", "383", null, "198", "37", null, this);
            obj.set_taborder("415");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\"/><Column size=\"170\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" text=\"구역\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("Button04", "absolute", "845", "354", "33", "24", null, null, this);
            obj.set_taborder("416");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Button("Button05", "absolute", "807", "354", "33", "24", null, null, this);
            obj.set_taborder("417");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "677", "352", "131", "31", null, null, this);
            obj.set_taborder("418");
            obj.set_text("특정 구역 포함");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head2");
            this.addChild(obj.name, obj);

            obj = new Button("Button06", "absolute", "442", null, "52", "28", null, "28", this);
            obj.set_taborder("419");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit00", "absolute", "649", "120", "61", "22", null, null, this);
            obj.set_taborder("420");
            obj.set_value("0");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit01", "absolute", "789", "120", "61", "22", null, null, this);
            obj.set_taborder("421");
            obj.set_value("0");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit02", "absolute", "649", "89", "61", "22", null, null, this);
            obj.set_taborder("422");
            obj.set_value("0");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit03", "absolute", "789", "89", "61", "22", null, null, this);
            obj.set_taborder("423");
            obj.set_value("0");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 915, 800, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.Static76.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static17.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static18.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static31.addEventHandler("onclick", this.Static76_onclick, this);

        };

        this.loadIncludeScript("Template12.xfdl", true);

       
    };
}
)();
