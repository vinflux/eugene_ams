﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,465);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/><Column id=\"Head1\" type=\"STRING\" size=\"300\"/><Column id=\"Head2\" type=\"STRING\" size=\"300\"/><Column id=\"Head3\" type=\"STRING\" size=\"300\"/><Column id=\"Head4\" type=\"DATE\" size=\"300\"/><Column id=\"Head5\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("웨이브");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", null, "414", "52", "28", "251", null, this);
            obj.set_taborder("121");
            obj.set_text("실행");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "130", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_text("화주코드<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "152", "58", "406", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("Button03", "absolute", "518", "62", "24", "22", null, null, this);
            obj.set_taborder("160");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "158", "62", "358", "22", null, null, this);
            obj.set_taborder("161");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "158", "93", "373", "22", null, null, this);
            obj.set_taborder("162");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "152", "89", "406", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "89", "130", "31", null, null, this);
            obj.set_taborder("164");
            obj.set_text("상/저온 구분");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "158", "124", "373", "22", null, null, this);
            obj.set_taborder("165");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "152", "120", "406", "31", null, null, this);
            obj.set_taborder("166");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "22", "120", "130", "31", null, null, this);
            obj.set_taborder("167");
            obj.set_text("회차 구분");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit04", "absolute", "158", "155", "373", "22", null, null, this);
            obj.set_taborder("168");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "152", "151", "406", "31", null, null, this);
            obj.set_taborder("169");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "22", "151", "130", "31", null, null, this);
            obj.set_taborder("170");
            obj.set_text("출고처코드");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "158", "186", "373", "22", null, null, this);
            obj.set_taborder("171");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "152", "182", "406", "31", null, null, this);
            obj.set_taborder("172");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "22", "182", "130", "31", null, null, this);
            obj.set_taborder("173");
            obj.set_text("웨이브 공통일자");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit06", "absolute", "158", "217", "373", "22", null, null, this);
            obj.set_taborder("174");
            this.addChild(obj.name, obj);

            obj = new Static("Static35", "absolute", "152", "213", "406", "31", null, null, this);
            obj.set_taborder("175");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static36", "absolute", "22", "213", "130", "31", null, null, this);
            obj.set_taborder("176");
            obj.set_text("납품예정일자<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit07", "absolute", "158", "248", "373", "22", null, null, this);
            obj.set_taborder("177");
            this.addChild(obj.name, obj);

            obj = new Static("Static37", "absolute", "152", "244", "406", "31", null, null, this);
            obj.set_taborder("178");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static38", "absolute", "22", "244", "130", "31", null, null, this);
            obj.set_taborder("179");
            obj.set_text("출고공통코드");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit08", "absolute", "158", "93", "358", "22", null, null, this);
            obj.set_taborder("180");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "518", "93", "24", "22", null, null, this);
            obj.set_taborder("181");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit09", "absolute", "158", "124", "358", "22", null, null, this);
            obj.set_taborder("182");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "518", "124", "24", "22", null, null, this);
            obj.set_taborder("183");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit10", "absolute", "158", "155", "358", "22", null, null, this);
            obj.set_taborder("184");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "518", "155", "24", "22", null, null, this);
            obj.set_taborder("185");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "305", "182", "110", "31", null, null, this);
            obj.set_taborder("186");
            obj.set_text("출고 공통유형");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo02", "absolute", "421", "186", "121", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("187");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Calendar("Calendar07", "absolute", "158", "186", "108", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("188");

            obj = new Calendar("Calendar00", "absolute", "158", "217", "108", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("189");

            obj = new Combo("Combo00", "absolute", "268", "217", "75", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("190");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Static("Static40", "absolute", "347", "219", "10", "18", null, null, this);
            obj.set_taborder("191");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Calendar("Calendar06", "absolute", "357", "217", "108", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("192");

            obj = new Combo("Combo01", "absolute", "467", "217", "75", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("193");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Edit("Edit11", "absolute", "158", "248", "384", "22", null, null, this);
            obj.set_taborder("194");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit12", "absolute", "158", "310", "373", "22", null, null, this);
            obj.set_taborder("195");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "152", "306", "406", "31", null, null, this);
            obj.set_taborder("196");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "22", "306", "130", "31", null, null, this);
            obj.set_taborder("197");
            obj.set_text("웨이브 공통명");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit13", "absolute", "158", "341", "373", "22", null, null, this);
            obj.set_taborder("198");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "152", "337", "406", "31", null, null, this);
            obj.set_taborder("199");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "337", "130", "31", null, null, this);
            obj.set_taborder("200");
            obj.set_text("최대출고주문건수<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "158", "372", "373", "22", null, null, this);
            obj.set_taborder("201");
            this.addChild(obj.name, obj);

            obj = new Static("Static45", "absolute", "152", "368", "406", "31", null, null, this);
            obj.set_taborder("202");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static46", "absolute", "22", "368", "130", "31", null, null, this);
            obj.set_taborder("203");
            obj.set_text("당일배송여부");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo04", "absolute", "158", "372", "384", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("208");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Edit("Edit16", "absolute", "158", "279", "373", "22", null, null, this);
            obj.set_taborder("213");
            this.addChild(obj.name, obj);

            obj = new Static("Static47", "absolute", "152", "275", "406", "31", null, null, this);
            obj.set_taborder("214");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "22", "275", "130", "31", null, null, this);
            obj.set_taborder("215");
            obj.set_text("도크 번호");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit17", "absolute", "158", "279", "185", "22", null, null, this);
            obj.set_taborder("216");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit15", "absolute", "357", "279", "185", "22", null, null, this);
            obj.set_taborder("217");
            this.addChild(obj.name, obj);

            obj = new Static("Static48", "absolute", "347", "281", "10", "18", null, null, this);
            obj.set_taborder("218");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit18", "absolute", "158", "310", "265", "22", null, null, this);
            obj.set_taborder("219");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin00", "absolute", "158", "341", "264", "22", null, null, this);
            obj.set_taborder("220");
            obj.set_value("100");
            this.addChild(obj.name, obj);

            obj = new Static("Static50", "absolute", null, "343", "66", "19", "40", null, this);
            obj.set_taborder("223");
            obj.set_text("(1~3,000)");
            obj.style.set_align("right middle");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox02", "absolute", "451", "310", "90", "21", null, null, this);
            obj.set_taborder("224");
            obj.set_text("접두어 사용");
            obj.set_value("true");
            obj.style.set_buttonalign("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "398", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 465, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);

        };

        this.loadIncludeScript("Template_popup02.xfdl", true);

       
    };
}
)();
