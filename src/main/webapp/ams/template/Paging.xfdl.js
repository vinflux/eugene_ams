﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1165,29);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">1</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">2</Col></Row><Row><Col id=\"code\">3</Col><Col id=\"data\">3</Col></Row><Row><Col id=\"code\">4</Col><Col id=\"data\">4</Col></Row><Row><Col id=\"code\">5</Col><Col id=\"data\">5</Col></Row><Row><Col id=\"code\">6</Col><Col id=\"data\">6</Col></Row><Row><Col id=\"code\">7</Col><Col id=\"data\">7</Col></Row><Row><Col id=\"code\">8</Col><Col id=\"data\">8</Col></Row><Row><Col id=\"code\">9</Col><Col id=\"data\">9</Col></Row><Row><Col id=\"code\">10</Col><Col id=\"data\">50</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("paging", "absolute", "0", "0", "100%", "29", null, null, this);
            obj.set_taborder("97");
            obj.set_cssclass("sta_WF_paging");
            this.addChild(obj.name, obj);

            obj = new Static("sta_totcnt", "absolute", "13", "6", "100", "19", null, null, this);
            obj.set_taborder("113");
            obj.set_text("Total <fc v='#ff1414ff'><b v='true'>18131</b></fc>건");
            obj.set_usedecorate("true");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", null, "6", "61", "19", "64", null, this);
            obj.set_taborder("114");
            obj.set_text("페이지 이동");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", null, "5", "19", "19", "166", null, this);
            obj.set_taborder("116");
            obj.set_text(">");
            obj.set_cssclass("btn_WF_page");
            this.addChild(obj.name, obj);

            obj = new Button("Button17", "absolute", null, "5", "19", "19", "323", null, this);
            obj.set_taborder("122");
            obj.set_text("<");
            obj.set_cssclass("btn_WF_page");
            this.addChild(obj.name, obj);

            obj = new Button("Button20", "absolute", null, "5", "22", "19", "344", null, this);
            obj.set_taborder("125");
            obj.set_text("<<");
            obj.set_cssclass("btn_WF_page");
            this.addChild(obj.name, obj);

            obj = new Button("Button21", "absolute", null, "5", "22", "19", "142", null, this);
            obj.set_taborder("126");
            obj.set_text(">>");
            obj.set_cssclass("btn_WF_page");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", null, "6", "31", "19", "282", null, this);
            obj.set_taborder("127");
            obj.set_text("Page");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", null, "6", "51", "19", "182", null, this);
            obj.set_taborder("128");
            obj.set_text("of <fc v='#ff1414ff'><b v='true'>363</b></fc>");
            obj.style.set_font("8 dotum");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin00", "absolute", null, "5", "45", "19", "236", null, this);
            obj.set_taborder("129");
            obj.set_value("1");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo01", "absolute", null, "5", "50", "19", "13", null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("130");
            obj.set_innerdataset("ds_list");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("1");
            obj.set_text("1");
            obj.set_index("-1");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1165, 29, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("Paging.xfdl", true);

       
    };
}
)();
