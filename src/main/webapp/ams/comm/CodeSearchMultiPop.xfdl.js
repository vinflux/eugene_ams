﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CodeSearchMultiPop");
                this.set_classname("style01");
                this.set_titletext("코드선택 멀티");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,700,547);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_code", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_search", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/><Column id=\"field3\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_choice", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grdInit", this);
            obj._setContents("<ColumnInfo><Column id=\"grd_codeInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_codeSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_codeIndex\" type=\"STRING\" size=\"256\"/><Column id=\"grd_choiceInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_choiceSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_choiceIndex\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"grd_codeIndex\"/><Col id=\"grd_codeSize\"/><Col id=\"grd_codeInit\"/><Col id=\"grd_choiceInit\"/><Col id=\"grd_choiceSize\"/><Col id=\"grd_choiceIndex\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_selectValue", this);
            obj._setContents("<ColumnInfo><Column id=\"grd_codeInit\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"grd_codeInit\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_headText", this);
            obj._setContents("<ColumnInfo><Column id=\"headText\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_owkey", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ctkey", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "22", "110", null, null, "22", "208", this);
            obj.set_taborder("136");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_code", "absolute", "0", "0", null, null, "0", "29", this.div_splitTop);
            obj.set_taborder("0");
            obj.set_binddataset("ds_code");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.style.set_padding("0 2 0 2");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            obj.set_url("comm::PopupPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("검색");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "55", "656", "44", null, null, this);
            obj.set_taborder("117");
            obj.style.set_border("3 solid #f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_search", "absolute", "33", "66", null, "22", "97", null, this);
            obj.set_taborder("119");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "609", "66", "59", "22", null, null, this);
            obj.set_taborder("130");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_text(" 검색");
            this.addChild(obj.name, obj);

            obj = new Button("btn_complete", "absolute", null, null, "52", "28", "323", "21", this);
            obj.set_taborder("133");
            obj.set_text("완료");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_work_split_h", "absolute", "22", null, null, "8", "22", "200", this);
            obj.set_taborder("134");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "347", null, "10", "22", null, this);
            obj.set_taborder("135");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "323", "21", this);
            obj.set_taborder("139");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "22", null, null, "127", "22", "64", this);
            obj.set_taborder("140");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_choice", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("0");
            obj.set_binddataset("ds_choice");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.style.set_padding("0 2 0 2");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "0", "24", null, "5", "0", null, this.div_splitBottom);
            obj.set_taborder("1");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "40", "24", "279", null, this.div_splitBottom);
            obj.set_taborder("2");
            obj.set_text("∧");
            obj.set_cssclass("btn_p");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_select", "absolute", null, "0", "40", "24", "329", null, this.div_splitBottom);
            obj.set_taborder("3");
            obj.set_text("∨");
            obj.set_tooltiptext("선택");
            this.div_splitBottom.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("136");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 127, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("140");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 700, 547, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("코드선택 멀티");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","btn_complete","text","gds_lang","COMPLETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_search","tooltiptext","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","label00","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::PopupPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("CodeSearchMultiPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CodeSearchMultiPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CodeSearchMultiPop.xfdl
        * PROGRAMMER  : bgheo 
        * DATE        : 2016.06.08
        * DESCRIPTION : 코드 선택 멀티
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;
        this.gv_edtValue;
        this.gv_colId;
        this.gv_obj;
        this.gv_col;
        this.gv_row;
        this.gv_key;
        this.gv_value;
        this.gv_callback;
        this.gv_edtval;

        this.gv_main = "Main";
        this.gv_middle = "Middle";
        this.gv_detral = "Detail";

        this.gv_first = "Y";
        this.gv_colTemp = "";

        

        
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	this.div_splitTop.div_Paging.gv_flag = this.gv_main;
        	this.div_splitTop.div_Paging.gv_popId = obj;
        	
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	
        	this.gv_searchId = this.gfn_isNullEmpty(this.parent.searchId);
        	this.gv_divId = this.gfn_isNullEmpty(this.parent.divId);
        	this.gv_obj = this.gfn_isNullEmpty(this.parent.putObj);
        	this.gv_col = this.gfn_isNullEmpty(this.parent.putCol);
        	this.gv_row = this.gfn_isNullEmpty(this.parent.putRow);
        	this.gv_key = ""+this.gfn_isNullEmpty(this.parent.putKey);
        	this.gv_value = ""+this.gfn_isNullEmpty(this.parent.putValue);
        	this.gv_bean = this.gfn_isNullEmpty(this.parent.putBean);
        	this.gv_method = this.gfn_isNullEmpty(this.parent.putMethod);
        	this.gv_appkey = this.gfn_isNullEmpty(this.parent.putAppkey);
        	this.gv_callback = this.gfn_isNullEmpty(this.parent.putCallback);
        	
        	/*팝업이 열리자마자 조회할 값 세팅*/
        	this.gv_edtval = this.gfn_isNullEmpty(this.parent.putEdtvalue); 
        	this.edt_search.set_value(this.gv_edtval);
        	
        	if(this.gv_bean == "") this.gv_bean = "commonController";
        	if(this.gv_method == "") this.gv_method = "selectCommonCode";
        	if(this.gv_appkey == "") this.gv_appkey = application.gv_ams;
        	// SingleOwChangeSave 화주변경 마스터 데이터 저장( 매입처, 배출처, 상품, 상품그룹, 상품대중소)
        	if(this.gv_divId == "Single" || this.gv_divId == "Grid" || this.gv_divId == "GridMulti" || this.gv_divId == "Single_Ibs" || this.gv_divId == "SingleOwChangeSave"){
        		this.btn_work_split_h.set_visible(false);
        		this.div_splitBottom.set_visible(false);
        		this.btn_complete.set_visible(false);
        		this.btn_close.set_visible(true);
        		this.div_splitTop.set_height(229);
        		this.div_splitTop.grd_code.setCellProperty("head", 0, "text", "NO");
        		this.div_splitTop.grd_code.setCellProperty("body", 0, "text", "NO");
        		this.div_splitTop.grd_code.setCellProperty("head", 0, "displaytype", "normal");
        		this.div_splitTop.grd_code.setCellProperty("body", 0, "displaytype", "number");
        		this.div_splitTop.grd_code.setCellProperty("body", 0, "edittype", "none");
        		this.div_splitTop.grd_code.setCellProperty("body", 0, "align", "right");
        		this.div_splitTop.grd_code.setCellProperty("body", 0, "expr", "currow+1");
        		if(application.gv_activeApp == "PTL"){
        			this.parent.set_height(405);
        			var nTop  = (application.mainframe.height / 2) - Math.round(405 / 2);
        			this.parent.set_top(nTop);
        		}
        	}
        	
        	this.fn_search();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.div_splitTop.grd_code.set_nodatatext("");
        	this.ds_param.clearData();
        	this.ds_search.clearData();
        //	this.ds_code.set_keystring("");
        	this.ds_code.clearData();
        	

         
        	  this.gfn_setParam("ctkey" , "");
         
        	this.gfn_setCommon("BEANID"  , this.gv_bean);
        	this.gfn_setCommon("METHODNM", this.gv_method);
        	
        	var sKey = "";
        	var sCtkey = "";
        	var cnt = "";
        	var smKey = this.gv_key.split("|");
        	
        	for(var i=0; i < smKey.length;i++ ){
        		if(smKey[i] == "OWKEY_SELECTION_LIST"){
        			cnt++;
        		}
        		
        	}//넘어온 gv_key 키값 중 OWKEY_SELECTION_LIST 값이 유무 체크. cnt가 0인 경우 화주키값이 null.

        	//화주키값이 null인 경우 Role별 화주 목록을 리스트로 생성.
        	if(this.gfn_isNull(smKey) || cnt == 0){
        		this.ds_owkey.copyData(application.gds_rtnOwnerRole, true);
        		
        		var owArr = new Array();
        		
        		for(var i=0; i< this.ds_owkey.getRowCount();i++){
        			owArr[i] = this.ds_owkey.getColumn(i,"ownerKey");
        		}

        		sKey += " OWKEY_SELECTION_LIST="+owArr;
        		
        		if(application.gv_activeApp == "TMS"){
        		sKey = " ";	
        		}
        			
        	}

        	this.ds_ctkey.copyData(application.gds_rtnWHRole, true);
        	
        	var ctArr = new Array();
        	
        	for(var i=0; i< this.ds_ctkey.rowcount;i++){
        		ctArr[i] = this.ds_ctkey.getColumn(i,"centerKey");
        	}

        	sKey += " CTKEY_SELECTION_LIST="+ctArr;

        	
        	if(this.gfn_isNotNull(this.gv_key)){
        		var mKey = this.gv_key.split("|");
        		var mValue = this.gv_value.split("|");
        		
        		for(var i = 0 ; i < mKey.length ; i++){
        			sKey += " " + mKey[i] + "=" + mValue[i];
        			if(mKey[i] == "ctkey") sCtkey = mKey[i];
        		}
        	}	
         
        	if(sCtkey == "" && application.gv_activeApp == "WMS"){
        		sKey += " ctkey=" + nexacro.wrapQuote(this.gfn_getUserInfo("ctKey"));
        	}
        	
        	var sValue = "";
        	if(this.gfn_isNull(this.gv_list)){
        		sValue = this.gfn_isNullEmpty(this.gfn_getUserInfo("owkeym"));
        	}else sValue = this.gv_list;
        		
            var sSvcId   = "select";
            var sSvcUrl  = this.gv_appkey + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_search=OUT_COMMON_CODE_LIST ds_param=OUT_PARAM";
            var sParam   = "searchid="+this.gv_searchId
                         +sKey
                         +" value="+nexacro.wrapQuote(this.gfn_isNullEmpty(this.edt_search.value))
                         ;

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_search.rowcount == 0){
        			this.div_splitTop.grd_code.set_nodatatext(application.gv_nodataMsg);
        			return;
        		}
        		
        		var utcHour = this.gfn_getUserInfo("utcHour");
        		
        		if(this.gv_first == "Y"){
        			var nCol = 0;
        			
        			this.gv_colId = "";
        			this.gv_colId = this.ds_param.getColumn(0, "COMMON_CODE_FIELD").split(";");
        			
        			for(var i = 0 ; i < this.gv_colId.length ; i++){
        				nCol = this.div_splitTop.grd_code.appendContentsCol();
        				
        				if(this.ds_headText.rowcount == 0){
        					this.div_splitTop.grd_code.setCellProperty("head", nCol, "text", this.gv_colId[i]);
        				}else{
        					this.div_splitTop.grd_code.setCellProperty("head", nCol, "text", this.ds_headText.getColumn(nCol, "headText"));
        				}
        				
        				if(this.gv_colId[i].indexOf("DATE") >= 0){
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "displaytype"            , "date");
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "mask"                   , "yyyy-MM-dd HH:mm:ss");
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "calendardisplaynulltype", "none");
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "align"                  , "center middle");
        				}
        				
        				if(this.gv_colId.length > 2){
        					this.div_splitTop.grd_code.setFormatColProperty(nCol, "size", 200);
        				}else{
        					this.div_splitTop.grd_code.setFormatColProperty(nCol, "size", 309);
        				}
        				
        				if(this.ds_search.rowcount > 0){
        					var sValueDesc = this.ds_search.getColumn(0, "field"+(i+1)+"_desc");
        					var sValue = this.ds_search.getColumn(0, "field"+(i+1));
        					
        					this.ds_code.addColumn(this.gv_colId[i]);
        					this.ds_choice.addColumn(this.gv_colId[i]);
        					
        					if(this.gv_colTemp == "") this.gv_colTemp = "field"+(i+1);
        					else this.gv_colTemp += "|field"+(i+1);
        					
        					// Grid Layout Setting & Code Description  & <> Code data Setting
        					if (this.gfn_isNull(sValueDesc)) {
        						this.div_splitTop.grd_code.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i]);
        					} else { 
        						this.ds_code.addColumn(this.gv_colId[i] +"_DESC");
        						this.ds_choice.addColumn(this.gv_colId[i] +"_DESC");
        						
        						this.div_splitTop.grd_code.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i] + "_DESC"); 
        					}
        					
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "align", "left");
        					this.div_splitTop.grd_code.setCellProperty("body", nCol, "padding", "0px 2px 0px 2px");
        				}
        				
        				nCol = this.div_splitBottom.grd_choice.appendContentsCol();
        				this.div_splitBottom.grd_choice.setCellProperty("head", nCol, "text", this.gv_colId[i]);
        				
        				if(this.gv_colId.length > 2){
        					this.div_splitBottom.grd_choice.setFormatColProperty(nCol, "size", 200);
        				}else{
        					this.div_splitBottom.grd_choice.setFormatColProperty(nCol, "size", 309);
        				}
        				
        				if(this.ds_search.rowcount > 0){
        					var sValueDesc = this.ds_search.getColumn(0, "field"+(i+1) + "_desc");
        					var sValue = this.ds_search.getColumn(0, "field"+(i+1));
        					
        					if (this.gfn_isNull(sValueDesc )) { 
        						this.div_splitBottom.grd_choice.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i]);
        					} else { 
        						this.div_splitBottom.grd_choice.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i] + "_DESC"); 
        					}
        					
        					this.div_splitBottom.grd_choice.setCellProperty("body", nCol, "align", "left");
        					this.div_splitBottom.grd_choice.setCellProperty("body", nCol, "padding", "0px 2px 0px 2px");
        				}
        			}
        			
        			if(this.div_splitTop.grd_code.getCellText(-1, 0) == "NO") this.div_splitTop.grd_code.setFormatColProperty(0, "size", 40);
        			else this.div_splitTop.grd_code.setFormatColProperty(0, "size", 25);
        			this.div_splitBottom.grd_choice.setFormatColProperty(0, "size", 25);
        			
        			this.gfn_gridHeadSet(this.div_splitTop.grd_code);
        			this.gfn_gridHeadSet(this.div_splitBottom.grd_choice);
        			
        			if(this.ds_search.rowcount > 0){
        				var dateVal = "";
        				
        				this.div_splitTop.grd_code.set_enableredraw(false);
        				for(var i = 0 ; i < this.ds_search.rowcount ; i++){
        					this.ds_code.addRow();
        					
        					for(var j = 0 ; j < this.gv_colId.length ; j++){
        						var sValueDesc = this.ds_search.getColumn(i, "field"+(j+1) + "_desc");
        						var sValue = this.ds_search.getColumn(i, "field"+(j+1));
        						
        						if(this.gv_colId[j].indexOf("DATE") >= 0 && this.gfn_isNotNull(sValue)){
        							if(sValue.length == 8) dateVal = sValue.substr(0, 4) + "/" + sValue.substr(4, 2) + "/" + sValue.substr(6, 2) + " 00:00:00";
        							else dateVal = sValue.substr(0, 4) + "/" + sValue.substr(4, 2) + "/" + sValue.substr(6, 2) + " " + sValue.substr(8, 2) + ":" + sValue.substr(10, 2) + ":" + sValue.substr(12, 2);
        							var dt = new Date(dateVal);
        							
        							if(dt != "Invalid Date"){
        								var hour = dt.getHours() + nexacro.toNumber(utcHour);
        								dt.setHours(hour);
        								
        								var year = dt.getFullYear().toString();
        								var month = (dt.getMonth() + 1).toString();
        								var day = dt.getDate().toString();
        								var hours = dt.getHours().toString();
        								var minute = dt.getMinutes().toString();
        								var seconds = dt.getSeconds().toString();
        								
        								if(month.length < 2) month = "0" + month;
        								if(day.length < 2) day = "0" + day;
        								if(hours.length < 2) hours = "0" + hours;
        								if(minute.length < 2) minute = "0" + minute;
        								if(seconds.length < 2) seconds = "0" + seconds;
        								
        								dateVal = year+month+day+hours+minute+seconds
        								
        								this.ds_code.setColumn(i, this.gv_colId[j], dateVal);
        							}
        						}else{
        							this.ds_code.setColumn(i, this.gv_colId[j], sValue);
        						}
        						
        						// Code Description  & <> Code data Setting
        						if (this.gfn_isNotNull(sValueDesc)) {
        							this.ds_code.setColumn(i, this.gv_colId[j] + "_DESC", sValueDesc);
        						}
        					}
        				}
        				this.div_splitTop.grd_code.set_enableredraw(true);
        				
        				this.ds_code.set_rowposition(0);
        			}
        			
        			this.div_splitTop.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        			
        			// 그리드 초기 값 셋팅
        			var sSize = "";
        			var sIndex = "";
        			
        			this.ds_grdInit.setColumn(0, "grd_codeInit", this.div_splitTop.grd_code.getFormatString());
        			this.ds_grdInit.setColumn(0, "grd_choiceInit", this.div_splitBottom.grd_choice.getFormatString());
        			
        			for(var i = 0 ; i < this.div_splitTop.grd_code.getCellCount("head") ; i++){
        				if(i == 0){
        					sSize = this.div_splitTop.grd_code.getRealColSize(i);
        					sIndex = this.div_splitTop.grd_code.getCellText(-1, i);
        				}else{
        					sSize += "|" + this.div_splitTop.grd_code.getRealColSize(i);
        					sIndex += "|" + this.div_splitTop.grd_code.getCellText(-1, i);
        				}
        			}
        			
        			this.ds_grdInit.setColumn(0, "grd_codeSize", sSize);
        			this.ds_grdInit.setColumn(0, "grd_codeIndex", sIndex);
        			
        			for(var i = 0 ; i < this.div_splitBottom.grd_choice.getCellCount("head") ; i++){
        				if(i == 0){
        					sSize = this.div_splitBottom.grd_choice.getRealColSize(i);
        					sIndex = this.div_splitBottom.grd_choice.getCellText(-1, i);
        				}else{
        					sSize += "|" + this.div_splitBottom.grd_choice.getRealColSize(i);
        					sIndex += "|" + this.div_splitBottom.grd_choice.getCellText(-1, i);
        				}
        			}
        			
        			this.ds_grdInit.setColumn(0, "grd_choiceSize", sSize);
        			this.ds_grdInit.setColumn(0, "grd_choiceIndex", sIndex);
        		}else{
        			if(this.ds_search.rowcount > 0){
        				var dateVal = "";
        				
        				this.div_splitTop.grd_code.set_enableredraw(false);
        				for(var i = 0 ; i < this.ds_search.rowcount ; i++){
        					this.ds_code.addRow();
        					
        					for(var j = 0 ; j < this.gv_colId.length ; j++){
        						var sValueDesc = this.ds_search.getColumn(i, "field"+(j+1) + "_desc");
        						var sValue = this.ds_search.getColumn(i, "field"+(j+1));
        						
        						if(this.gv_colId[j].indexOf("DATE") >= 0 && this.gfn_isNotNull(sValue)){
        							if(sValue.length == 8) dateVal = sValue.substr(0, 4) + "/" + sValue.substr(4, 2) + "/" + sValue.substr(6, 2) + " 00:00:00";
        							else dateVal = sValue.substr(0, 4) + "/" + sValue.substr(4, 2) + "/" + sValue.substr(6, 2) + " " + sValue.substr(8, 2) + ":" + sValue.substr(10, 2) + ":" + sValue.substr(12, 2);
        							var dt = new Date(dateVal);
        							
        							if(dt != "Invalid Date"){
        								var hour = dt.getHours() + nexacro.toNumber(utcHour);
        								dt.setHours(hour);
        								
        								var year = dt.getFullYear().toString();
        								var month = (dt.getMonth() + 1).toString();
        								var day = dt.getDate().toString();
        								var hours = dt.getHours().toString();
        								var minute = dt.getMinutes().toString();
        								var seconds = dt.getSeconds().toString();
        								
        								if(month.length < 2) month = "0" + month;
        								if(day.length < 2) day = "0" + day;
        								if(hours.length < 2) hours = "0" + hours;
        								if(minute.length < 2) minute = "0" + minute;
        								if(seconds.length < 2) seconds = "0" + seconds;
        								
        								dateVal = year+month+day+hours+minute+seconds
        								
        								this.ds_code.setColumn(i, this.gv_colId[j], dateVal);
        							}
        						}else{
        							this.ds_code.setColumn(i, this.gv_colId[j], sValue);
        						}
        						
        						// Code Description  & <> Code data Setting
        						if (this.gfn_isNotNull(sValueDesc)) { 
        							this.ds_code.setColumn(i, this.gv_colId[j] + "_DESC", sValueDesc);
        						}
        					}
        				}
        				this.div_splitTop.grd_code.set_enableredraw(true);
        				
        				this.ds_code.set_rowposition(0);
        			}
        			
        			this.div_splitTop.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        		}
        		
        		this.gv_first = "";
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* pageSearch 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */
        this.fn_mainSearch = function(currentPage,pagingLimit,count)
        {
        	this.gfn_setParam("currentPage", currentPage);
        	this.gfn_setParam("pagingLimit", pagingLimit);
        	this.gfn_setParam("COUNT", count);
        	this.gfn_setParam("colName", this.ds_param.getColumn(0, "colName"));
        	this.gfn_setParam("sortValue", this.ds_param.getColumn(0, "sortValue"));
        	
        	this.fn_search();
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_search_onclick 실행 */
        this.btn_search_onclick = function(obj,e)
        {
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	this.gfn_setParam("colName", this.ds_param.getColumn(0, "colName"));
        	this.gfn_setParam("sortValue", this.ds_param.getColumn(0, "sortValue"));

        	this.fn_search();
        }

        /* div_splitBottom_btn_select_onclick 실행 */
        this.div_splitBottom_btn_select_onclick = function(obj,e)
        {
        	var nRow = this.ds_code.findRowExpr("CHK == '1'");
        	
        	while(nRow != -1){
        		if(this.ds_choice.getCaseCount(this.gv_colId[0] + " == '" + this.ds_code.getColumn(nRow, this.gv_colId[0]) + "' && " + this.gv_colId[1] + " == '" + this.ds_code.getColumn(nRow, this.gv_colId[1]) + "'") != 0){
        			nRow = this.ds_code.findRowExpr("CHK == '1'", nRow+1);
        		}else{
        			this.ds_choice.addRow();
        			
        			for(var j = 0 ; j < this.gv_colId.length ; j++){
        				var sValue = this.ds_code.getColumn(nRow, this.gv_colId[j]);
        				var sValueDesc = this.ds_code.getColumn(nRow, this.gv_colId[j] + "_DESC");
        				
        				if (this.gfn_isNull(sValueDesc)) { 
        					this.ds_choice.setColumn(this.ds_choice.rowposition, this.gv_colId[j], sValue);
        				} else { 
        					this.ds_choice.setColumn(this.ds_choice.rowposition, this.gv_colId[j], sValue);
        					this.ds_choice.setColumn(this.ds_choice.rowposition, this.gv_colId[j]+ "_DESC", sValueDesc);
        				}
        			}
        			
        			nRow = this.ds_code.findRowExpr("CHK == '1'", nRow+1);
        		}
        	}
        }

        /* grd_code_onheadclick 실행 */
        this.grd_code_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sValueDesc = this.ds_search.getColumn(i, "field"+e.cell+"_desc");
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}else if(colName != "PAGING_NUM" && this.ds_code.rowcount > 0){
        		this.gfn_sortSearchPopup(obj, e.cell, colName, sortValue);
        		
        		if(this.gfn_isNull(sValueDesc))	this.gfn_setParam("colName", "FIELD"+e.cell);
        		else this.gfn_setParam("colName", "FIELD"+e.cell+"_DESC");
        		
        		this.ds_headText.clearData();
        		
        		for(var i = 0 ; i < obj.getCellCount("head") ; i++){
        			this.ds_headText.addRow();
        			this.ds_headText.setColumn(i, "headText", obj.getCellText(-1, i));
        		}
        		
        		this.gfn_setParam("currentPage", 1);
        		this.fn_search();
        	}
        }

        /* grd_code_onkeydown 실행 */
        this.grd_code_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_code, obj.currentcell);
        }

        /* div_splitBottom_grd_choice_onrbuttonup 실행 */
        this.div_splitBottom_grd_choice_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "1|1|0|0|0|0|1|0";
        	this.gfn_openGridMenu(obj, e, enable_list, "Pop");
        }

        /* div_splitBottom_grd_choice_onkeydown 실행 */
        this.div_splitBottom_grd_choice_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_choice, obj.currentcell);
        }

        /* grd_choice_onrbuttonup 실행 */
        this.grd_choice_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "1|1|0|0|0|0|1|0";
        	this.gfn_openGridMenu(obj, e,enable_list);
        }

        /* grd_code_oncellclick 실행 */
        this.grd_code_oncellclick = function(obj,e)
        {
        // 	var colName = this.gfn_gridBindName(obj, e.cell);
        // 	var chkValue = this.ds_code.getColumn(e.row, "CHK");
        // 	
        // 	if(colName != "CHK"){
        // 		if(chkValue == "1") this.ds_code.setColumn(e.row, "CHK", "");
        // 		else this.ds_code.setColumn(e.row, "CHK", "1");
        // 	}
        }

        /* div_splitBottom_grd_choice_oncellclick 실행 */
        this.div_splitBottom_grd_choice_oncellclick = function(obj,e)
        {
        // 	var colName = this.gfn_gridBindName(obj, e.cell);
        // 	var chkValue = this.ds_choice.getColumn(e.row, "CHK");
        // 	
        // 	if(colName != "CHK"){
        // 		if(chkValue == "1") this.ds_choice.setColumn(e.row, "CHK", "");
        // 		else this.ds_choice.setColumn(e.row, "CHK", "1");
        // 	}
        }

        /* div_splitBottom_grd_choice_onheadclick 실행 */
        this.div_splitBottom_grd_choice_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* btn_complete_onclick 실행 */
        this.btn_complete_onclick = function(obj,e)
        {
        	var allValue = "";
        	var valueArr = [];
        	
        	if(this.ds_choice.rowcount == 0){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return;
        	}
        	
        	for(var i = 0 ; i < this.ds_choice.rowcount ; i++){
        		if(i == 0){
        			allValue = this.ds_choice.getColumn(i, this.gv_colId[0]);
        		}else{
        			allValue = allValue+","+this.ds_choice.getColumn(i, this.gv_colId[0]);
        		}
        	}
        	if(this.gv_divId == "GridList"){
        		var sDsColId = this.gv_col.split("|");
        		
        		for(var j = 0 ; j < this.ds_choice.rowcount ; j++){
        			var insetRow = this.gv_obj.addRow();
        			
        			for(var i = 0 ; i < sDsColId.length ; i++){
        				this.gv_obj.setColumn(insetRow, sDsColId[i], this.ds_choice.getColumn(j, this.gv_colId[i]));
        			}
        		}
        	}else{
        		this.gv_obj.set_value(allValue);
        	}

        	valueArr = allValue.split(",");

        	var typeCheck = "single";
        	if( this.ds_choice.rowcount > 1 ){
        		typeCheck = "multi";
        	}
        	
        	if(this.gv_callback.length > 0){
        		eval("this.opener."+this.gv_callback+"(this.gv_obj, valueArr, typeCheck)");
        	}
        	
        	this.close();
        }

        /* div_splitBottom_btn_delete_onclick 실행 */
        this.div_splitBottom_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_choice.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return;
        	}
        	
        	while(nRow != -1){
        		this.ds_choice.deleteRow(nRow);
        		nRow = this.ds_choice.findRowExpr("CHK == '1'", nRow);
        	}
        }

        /* grd_code_oncelldblclick 실행 */
        this.grd_code_oncelldblclick = function(obj,e)
        {
        	var valueArr = [];
        	var sValue = this.ds_code.getColumn(e.row, this.gv_colId[0]);
        	var sResult = "";
        	var nRow = 0;
        	
        	this.opener.gv_Pvalue = "";
        	//if(this.opener.gv_Pvalue == ""){
        		nRow = this.ds_search.findRowExpr("field1 == '" + sValue + "'");

        		for(var i = 0 ; i < this.gv_colId.length ; i++){
        			if(i == 0) sResult = this.ds_code.getColumn(e.row, this.gv_colId[i]);
        			else sResult += "|" + this.ds_code.getColumn(e.row, this.gv_colId[i]);
        		}
        		if(nRow != -1){
        			for(var i = 0 ; i < this.ds_search.getColCount() ; i++){
        				if(this.gv_colTemp.indexOf(this.ds_search.getColID(i)) == -1) {
        					sResult += "|" + this.ds_search.getColumn(nRow, this.ds_search.getColID(i));
        				}
        			}
        		}
        		
        		valueArr = sResult.split("|");
        		this.opener.gv_Pvalue = sResult.split("|");
        	//}
        	if(this.gv_divId == "Grid"){
        		this.gv_obj.setColumn(this.gv_row, this.gv_col, sValue);
        	}else if(this.gv_divId == "GridMulti"){
        		var sDsColId = this.gv_col.split("|");
        		
        		for(var i = 0 ; i < sDsColId.length ; i++){
        			this.gv_obj.setColumn(this.gv_row, sDsColId[i], this.ds_code.getColumn(e.row, this.gv_colId[i]));
        		}	
        	}else if(this.gv_divId == "GridList"){
        		var sDsColId = this.gv_col.split("|");
        		var insetRow = this.gv_obj.addRow();
        		
        		for(var i = 0 ; i < sDsColId.length ; i++){
        			this.gv_obj.setColumn(insetRow, sDsColId[i], this.ds_code.getColumn(e.row, this.gv_colId[i]));
        		}
        	}else if(this.gv_divId == "Single_Ibs"){
        		this.gv_obj.set_value(sValue);
        		this.opener.ds_temp.copyData(this.ds_search, false);
        	}else if(this.gv_divId == "SingleOwChangeSave"){
        		// SingleOwChangeSave 화주변경 마스터 데이터 저장( 매입처, 배출처, 상품, 상품그룹, 상품대중소 사용)
        		this.opener.fn_SingleOwChangeSaveCallBack(sValue);
        		this.close();
        	}else{
        		this.gv_obj.set_value(sValue);
        	}
        	if(this.gv_callback.length > 0){
        		eval("this.opener."+this.gv_callback+"(this.gv_obj, valueArr)");
        	}
        	
        	this.close();
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }

        /* edt_search_onkeydown 실행 */
        this.edt_search_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.btn_search.click();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.grd_code.addEventHandler("onrbuttonup", this.grd_code_onrbuttonup, this);
            this.div_splitTop.grd_code.addEventHandler("onkeydown", this.grd_code_onkeydown, this);
            this.div_splitTop.grd_code.addEventHandler("oncellclick", this.grd_code_oncellclick, this);
            this.div_splitTop.grd_code.addEventHandler("onheadclick", this.grd_code_onheadclick, this);
            this.div_splitTop.grd_code.addEventHandler("oncelldblclick", this.grd_code_oncelldblclick, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_search.addEventHandler("onkeydown", this.edt_search_onkeydown, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.btn_complete.addEventHandler("onclick", this.btn_complete_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.div_splitBottom.grd_choice.addEventHandler("oncellclick", this.div_splitBottom_grd_choice_oncellclick, this);
            this.div_splitBottom.grd_choice.addEventHandler("onheadclick", this.div_splitBottom_grd_choice_onheadclick, this);
            this.div_splitBottom.grd_choice.addEventHandler("onkeydown", this.div_splitBottom_grd_choice_onkeydown, this);
            this.div_splitBottom.grd_choice.addEventHandler("onrbuttonup", this.div_splitBottom_grd_choice_onrbuttonup, this);
            this.div_splitBottom.btn_delete.addEventHandler("onclick", this.div_splitBottom_btn_delete_onclick, this);
            this.div_splitBottom.btn_select.addEventHandler("onclick", this.div_splitBottom_btn_select_onclick, this);

        };

        this.loadIncludeScript("CodeSearchMultiPop.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
