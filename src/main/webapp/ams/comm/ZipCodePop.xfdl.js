﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ZipCodePop");
                this.set_classname("ZipCodePop");
                this.set_titletext("우편번호 조회 팝업");
                this._setFormPosition(0,0,580,768);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grdInit", this);
            obj._setContents("<ColumnInfo><Column id=\"grd_codeInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_codeSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_codeIndex\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"grd_codeIndex\"/><Col id=\"grd_codeSize\"/><Col id=\"grd_codeInit\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("0");
            obj.set_text("Title");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("1");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("2");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addr", "absolute", "140", "91", "221", "20", null, null, this);
            obj.set_taborder("3");
            obj.set_lengthunit("utf8");
            obj.set_maxlength("24");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "369", "91", null, "22", "114", null, this);
            obj.set_taborder("4");
            obj.set_text("검색");
            this.addChild(obj.name, obj);

            obj = new Static("lab_search", "absolute", "81", "93", "77", "20", null, null, this);
            obj.set_taborder("5");
            obj.set_text("검색어 : ");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("lab_search00", "absolute", "129", "53", "151", "20", null, null, this);
            obj.set_taborder("6");
            obj.set_text("[도로명/건물명] 또는");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "12.07%", "53", null, "20", "76.55%", null, this);
            obj.set_taborder("7");
            obj.set_text("도로명주소");
            obj.style.set_color("green");
            obj.style.set_font("9 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("lab_search01", "absolute", "319", "53", "151", "20", null, null, this);
            obj.set_taborder("9");
            obj.set_text("[동/읍/면/리] 입력");
            obj.set_cssclass("sta_WF_title");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_header", "absolute", "1", "121", null, null, "1", "29", this);
            obj.set_taborder("11");
            obj.set_binddataset("ds_header");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_scrollbars("autoboth");
            obj.set_autosizingtype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"77\"/><Column size=\"250\"/><Column size=\"250\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"ZIPCD\"/><Cell col=\"1\" text=\"LNMADDR\"/><Cell col=\"2\" text=\"RNADDR\"/></Band><Band id=\"body\"><Cell style=\"align:center;padding: ;\" text=\"bind:zipNo\"/><Cell col=\"1\" displaytype=\"text\" style=\"align:left;color:dodgerblue;color2:dodgerblue;\" text=\"bind:lnmAdres\" wordwrap=\"char\" autosizerow=\"default\"/><Cell col=\"2\" style=\"align:left;\" text=\"bind:rnAdres\" wordwrap=\"char\" autosizerow=\"default\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Div("div_Paging", "absolute", "1", null, null, "29", "3", "0", this);
            obj.set_taborder("12");
            obj.set_scrollbars("none");
            obj.set_url("comm::PopupPagingMAX50.xfdl");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "46.72%", "53", null, "20", "41.9%", null, this);
            obj.set_taborder("13");
            obj.set_text("지번주소");
            obj.style.set_color("green");
            obj.style.set_font("9 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "32.24%", "72", null, "17", "22.59%", null, this);
            obj.set_taborder("14");
            obj.set_text("※ 도로명 주소를 입력해주세요.");
            obj.style.set_color("orangered");
            obj.style.set_font("9 dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 768, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("ZipCodePop");
            		p.set_titletext("우편번호 조회 팝업");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","ADDR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::PopupPagingMAX50.xfdl");
        };
        
        // User Script
        this.addIncludeScript("ZipCodePop.xfdl", "lib::Comm.xjs");
        this.registerScript("ZipCodePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ZipCodePop.xfdl
        * PROGRAMMER  : yachoi
        * DATE        : 2018.06.011
        * DESCRIPTION : 우편번호 조회 공통 팝업 
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /*******************************************************************************
            1. 공통 라이브러리 INCLUDE 영역
        ********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /*******************************************************************************
            2. FORM 변수 선언 영역
        ********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_grdList;

        this.gv_header = this.grd_header;
        this.gv_page=this.div_Paging;
        this.gv_Row;
        this.gv_ds;
        this.gv_callback;
        this.gv_obj;
        this.gv_key;
        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/

        this.form_onload = function(obj,e)
        {
        	var sSize = "";
        	var sIndex = "";
        	this.gv_obj = this.gfn_isNullEmpty(this.parent.putObj);
        	this.gv_key = this.gfn_isNullEmpty(this.parent.putKey);
        	this.gv_Row  = this.gfn_isNullEmpty(this.parent.oRow);
        	this.gv_ds  = this.gfn_isNullEmpty(this.parent.putDs);
        	this.gv_callback = this.gfn_isNullEmpty(this.parent.putCallback);
        	this.div_Paging.gv_flag = this.gv_main;
        	this.div_Paging.gv_popId = obj;
        	this.gfn_gridHeadSet(this.grd_header);
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 50);

        	
        	// 그리드 초기 값 셋팅
        	this.ds_grdInit.setColumn(0, "grd_codeInit", this.grd_header.getFormatString());
        	
        	for(var i = 0 ; i < this.grd_header.getCellCount("head") ; i++){
        		if(i == 0){
        			sSize = this.grd_header.getRealColSize(i);
        			sIndex = this.grd_header.getCellText(-1, i);
        		}else{
        			sSize += "|" + this.grd_header.getRealColSize(i);
        			sIndex += "|" + this.grd_header.getCellText(-1, i);
        		}
        	}
        	
        	this.ds_grdInit.setColumn(0, "grd_codeSize", sSize);
        	this.ds_grdInit.setColumn(0, "grd_codeIndex", sIndex);

        	this.fn_search();
        	
        }

        /*******************************************************************************

            4. TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.grd_header.set_nodatatext("");
        	//this.ds_search.clearData();
        	this.ds_param.clearData();
        	this.ds_header.set_keystring("");
        	this.ds_header.clearData();
        	
        	var addr = this.edt_addr.value;
        	if(this.gfn_isNull(addr)) {
        		addr = "";
        	} else {
        		addr = addr.replace(/ /g, "-");
        	}
        	
        	this.gfn_setCommon("BEANID", "commonController");
        	this.gfn_setCommon("METHODNM", "selectZipCode");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "search="+ addr;
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	
        }

        /*******************************************************************************
        * 5. Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	 //this.ds_param.clearData();
        	if(sSvcId == "select") {
        		this.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));		
        		trace(this.div_Paging.cbo_pageMove.innerdataset.value);
        	}

        	
        }
        /*******************************************************************************
            6. 개발자 FUNCTION 영역
        ********************************************************************************/
        this.fn_mainSearch = function(currentPage,pagingLimit,count)
        {
        	if(this.div_Paging.spn_cnt.value == 0) {
        	this.gfn_setParam("currentPage", this.div_Paging.spn_cnt.value + 1);
        	} else {
        	this.gfn_setParam("currentPage", this.div_Paging.spn_cnt.value);
        	}
        	this.gfn_setParam("pagingLimit", this.div_Paging.cbo_pageMove.value);
        	this.gfn_setParam("COUNT", count);
        	
        	this.fn_search();
        }
        /*******************************************************************************
            7. 각 COMPONENT 별 EVENT 영역
        ********************************************************************************/
        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        
        this.btn_search_onclick = function(obj,e)
        {
        	if(this.edt_addr.value != null && this.edt_addr.value != ""){
        		this.fn_mainSearch();
        	}else{
        		alert("주소를 입력해주세요.");
        	}
        }

        this.edt_addr_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.btn_search.click();
        }

        
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	
        	if(this.gv_ds != ""){
        		var keys =this.gv_key.split("|");
        		for(var i=0; i<keys.length; i++){
        			this.gv_ds.setColumn(this.gv_Row, keys[i], this.ds_header.getColumn(e.row, keys[i]));
        		}	
        	}else if(this.gv_obj != ""){
        		var objs =this.gv_obj.split("|");
        		var keys =this.gv_key.split("|");
        		for(var i=0; i<objs.length; i++){
        			eval("this.opener."+objs[i]+".set_value(this.ds_header.getColumn(e.row, '"+keys[i]+"'))");
        		}
        	}
        	
        	
        	if(this.gv_callback.length > 0){ 
        		eval("this.opener."+this.gv_callback+"(this.ds_header.getColumn(e.row,'zipNo'),this.ds_header.getColumn(e.row, 'lnmAdres'))");
        	}
        	
        	this.close();
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_addr.addEventHandler("onkeydown", this.edt_addr_onkeydown, this);
            this.edt_addr.addEventHandler("oneditclick", this.edt_addr_oneditclick, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.Static01.addEventHandler("onclick", this.Static01_onclick, this);

        };

        this.loadIncludeScript("ZipCodePop.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
