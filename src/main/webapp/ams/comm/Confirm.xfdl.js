﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Confirm");
                this.set_classname("Template10");
                this.set_titletext("confirm");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,380,208);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static02", "absolute", "22", "14", "250", "20", null, null, this);
            obj.set_taborder("1");
            obj.set_text("확인");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ok", "absolute", null, null, "52", "28", "191", "23", this);
            obj.set_taborder("6");
            obj.set_text("OK");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_cancel", "absolute", null, null, "52", "28", "134", "23", this);
            obj.set_taborder("7");
            obj.set_text("Cancel");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_msg", "absolute", "22", "57", "336", "85", null, null, this);
            obj.set_taborder("8");
            obj.set_scrollbars("autovert");
            obj.set_wordwrap("char");
            obj.set_dragscrolltype("vert");
            obj.set_cssclass("txt_WF_Alert");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("9");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_msg", "absolute", "472", "83", "336", "85", null, null, this);
            obj.set_taborder("10");
            obj.set_readonly("true");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 380, 208, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Template10");
            		p.set_titletext("confirm");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_ok","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_cancel","text","gds_lang","CANCEL");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Confirm.xfdl", "lib::Comm.xjs");
        this.registerScript("Confirm.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Confirm.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Confirm msg 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            var sMsgCd = this.gfn_isNullEmpty(this.parent.argMsgCd);
            var sTrans = this.gfn_isNullEmpty(this.parent.argTrans);
            
            var aMsg = this.gfn_getMsgToArray(sMsgCd);
            var sMsg = "";
            
            if(this.gfn_isNotNull(aMsg)) {
                if(sMsgCd == "MSG_WELCOME") this.txa_msg.set_value(sTrans + this.gfn_getTransMsg(aMsg[1]));
                else if(sMsgCd == "MSG_CHK_ENABLEMAXUSERSCREEN") this.txa_msg.set_value(this.gfn_getTransMsg(aMsg[1]) + sTrans);
                else this.txa_msg.set_value(this.gfn_getTransMsg(aMsg[1], sTrans));
            }else{
                sMsg = this.gfn_replaceAll(sMsgCd, "\n", String.fromCharCode(10) + String.fromCharCode(13));
                this.txa_msg.set_value(sMsg + sTrans); // 메시지 코드가 없을 경우
            }
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* cancel Button 실행 */
        this.btn_cancel_onclick = function(obj,e)
        {
            var flag = false;
            this.close(flag);
        }

        /* ok Button 실행 */
        this.btn_ok_onclick = function(obj,e)
        {
            var flag = true;
            this.close(flag);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_close.addEventHandler("onclick", this.btn_cancel_onclick, this);
            this.btn_ok.addEventHandler("onclick", this.btn_ok_onclick, this);
            this.btn_cancel.addEventHandler("onclick", this.btn_cancel_onclick, this);
            this.txa_msg.addEventHandler("oneditclick", this.TextArea00_oneditclick, this);

        };

        this.loadIncludeScript("Confirm.xfdl", true);

       
    };
}
)();
