﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UbiReport");
                this.set_classname("Template10");
                this.set_titletext("UbiReport");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,800,700);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("7");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "14", "250", "20", null, null, this);
            obj.set_taborder("9");
            obj.set_text("UbiReport");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Div("div_Ubi", "absolute", "0", "45", null, null, "0", "0", this);
            obj.set_taborder("11");
            obj.set_text("div_Ubi");
            this.addChild(obj.name, obj);
            obj = new UbiReport("ubi_report", "absolute", "20", "20", null, null, "20", "20", this.div_Ubi);
            obj.set_taborder("0");
            this.div_Ubi.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_Ubi,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("11");
            		p.set_text("div_Ubi");

            	}
            );
            this.div_Ubi.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 800, 700, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Template10");
            		p.set_titletext("UbiReport");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("UbiReportPop.xfdl", "lib::Comm.xjs");
        this.registerScript("UbiReportPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Alert.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Alert msg 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";
        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.svcURL = application.services.get_item("svc").url;

        this.div_Ubi.ubi_report.resource = this.svcURL+this.gfn_getActiveApp()+"/ubireport/ajax/js4";
        this.div_Ubi.ubi_report.gatewayurl = this.svcURL+this.gfn_getActiveApp()+"/UbiGateway";
        this.div_Ubi.ubi_report.resid = "UBIAJAX";
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.div_Ubi.ubi_report.scrollpage = "false";
        	this.div_Ubi.ubi_report.dataurl = "";
        	this.div_Ubi.ubi_report.fileurl = this.parent.appModule+this.parent.fileUrl;

        	this.div_Ubi.ubi_report.jrffile = this.parent.jrfFile;

         	if(this.gfn_isNotNull(this.parent.dsObjectList)){
        		for(var i = 0 ; i < this.parent.dsObjectList.length ; i++){
        			this.div_Ubi.ubi_report.setDataset(this.parent.dsObjectList[i].sqlID, this.parent.dsObjectList[i].dsObject);
        		}
        	}

         	if(system.navigatorname == "nexacro"){
        		this.div_Ubi.ubi_report.jrffile = this.svcURL+this.parent.appModule+this.parent.fileUrl+this.parent.jrfFile;
         	}else{
        		this.div_Ubi.ubi_report.jrffile = this.parent.jrfFile;
         	}
         
        	if(this.parent.printType == "DIRECT"){
        	 	this.div_Ubi.ubi_report.retrieve(this, "retrieveEnd");
        		this.div_Ubi.ubi_report.print();
        		this.btn_ok_onclick();
        	}else{
        		this.div_Ubi.ubi_report.retrieve(this, "retrieveEnd");
        	}
        }

        this.btn_ok_onclick = function(obj,e)
        {
            this.close(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_ok_onclick, this);

        };

        this.loadIncludeScript("UbiReportPop.xfdl", true);

       
    };
}
)();
