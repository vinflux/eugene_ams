﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("RowCount");
                this.set_classname("style01");
                this.set_titletext("RowCount");
                this._setFormPosition(0,0,1165,29);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("paging", "absolute", "0", "0", "100%", "29", null, null, this);
            obj.set_taborder("97");
            obj.set_cssclass("sta_WF_paging");
            this.addChild(obj.name, obj);

            obj = new Static("sta_totcnt", "absolute", null, "6", "148", "19", "13", null, this);
            obj.set_taborder("113");
            obj.set_text("Total <fc v='#ff1414ff'><b v='true'>0</b></fc> Row");
            obj.set_usedecorate("true");
            obj.style.set_align("right middle");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "252", "6", "61", "19", null, null, this);
            obj.set_taborder("114");
            obj.style.set_align("right middle");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1165, 29, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("RowCount");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","Static05","text","gds_lang","");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("RowCount.xfdl", "lib::Comm.xjs");
        this.registerScript("RowCount.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : RowCount.xfdl
        * PROGRAMMER  : JUNGDOS
        * DATE        : 2018.10.01
        * DESCRIPTION : VIMS 화면 RowCount 추가
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_menuId;
        this.gv_flag; // Main, Middle, Detail

        this.gv_count;
        this.gv_totPage;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* page 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */

        this.fn_setRowCount = function(count)
        {
        	try
        	{
        		this.sta_totcnt.set_text("Total <fc v='#ff1414ff'><b v='true'>"+this.gfn_formatComma(count.toString())+"</b></fc> Row");
        	}
        	catch(e)
        	{
        		this.sta_totcnt.set_text("Total <fc v='#ff1414ff'><b v='true'>0</b></fc> Row");
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);

        };

        this.loadIncludeScript("RowCount.xfdl", true);

       
    };
}
)();
