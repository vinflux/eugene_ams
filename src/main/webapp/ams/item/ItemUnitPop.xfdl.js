﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ItemUnitPop");
                this.set_classname("style01");
                this.set_titletext("상품 단위 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,311);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"icutkey\" type=\"STRING\" size=\"256\"/><Column id=\"icname\" type=\"STRING\" size=\"256\"/><Column id=\"icuttype\" type=\"STRING\" size=\"256\"/><Column id=\"icutqty\" type=\"STRING\" size=\"25\"/><Column id=\"icutorder\" type=\"STRING\" size=\"25\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"fn_get_master_impos\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"owkey\"/><Col id=\"icutkey\"/><Col id=\"icuttype\"/><Col id=\"icutqty\"/><Col id=\"closingdate\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/><Col id=\"updatedate\"/><Col id=\"inserturkey\"/><Col id=\"icutorder\"/><Col id=\"icname\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_icuttype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("10");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("8");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_text("상품 단위 유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icutkey", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            obj.set_maxlength("50");
            obj.set_inputtype("number,english");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("9");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("상품 단위 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("상품 단위 입수");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("화주 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "334", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "222", "62", "110", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "213", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "213", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("종료 일시");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "124", "57", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_icuttype", "absolute", "222", "124", "257", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_icuttype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Calendar("cal_close", "absolute", "222", "217", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("249");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("상품 단위 순서");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_icutqty", "absolute", "222", "155", "321", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_displaynulltext("1");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_icutorder", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_displaynulltext("1");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "360", "62", "183", "22", null, null, this);
            obj.set_taborder("251");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fn_get_master_impos", "absolute", "616", "54", "183", "22", null, null, this);
            obj.set_taborder("252");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 311, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품 단위 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_icutkey","value","ds_header","icutkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","ITEMCODE UNIT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","ICUTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","ICUTTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ICUTQTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","CLOSINGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cbo_icuttype","value","ds_header","icuttype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cal_close","value","ds_header","closingdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static06","text","gds_lang","ICUTORDER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","mdt_icutqty","value","ds_header","icutqty");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","mdt_icutorder","value","ds_header","icutorder");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ItemUnitPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ItemUnitPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ItemUnitPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품 단위 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_icutkey = "";
        this.gv_icuttype = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("ICUTTYPE", this.ds_icuttype, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_icutkey  = this.gfn_isNullEmpty(this.parent.icutkey);
            this.gv_icuttype = this.gfn_isNullEmpty(this.parent.icuttype);
            
            this.ds_header.setColumn(0, "owkey", this.gfn_getUserInfo("owkeym"));
            this.ds_header.setColumn(0, "icutqty", "1");
            this.ds_header.setColumn(0, "icutorder", "1");
        	this.ds_header.applyChange();
        	
        	this.gfn_decimalPointSet("mdt_icutqty");

            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_icutkey.set_enable(false);
        		this.cbo_icuttype.set_enable(false);
        		this.btn_check.set_enable(false);
        		if(this.parent.icuttype == "LAYER" || this.parent.icuttype == "PL"){
        			this.mdt_icutorder.set_enable(true);
        			this.mdt_icutqty.set_enable(true);
        		}
        		this.fn_search();
        	}else{
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "itemUnitController");
        	this.gfn_setCommon("METHODNM", "selectItemUnit");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " icutkey="+this.gv_icutkey
                         + " icuttype="+this.gv_icuttype
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "itemUnitController");
        	this.gfn_setCommon("METHODNM", "selectItemUnit");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   =  "owkey="+this.ds_header.getColumn(0, "owkey")
                         + " icutkey="+this.ds_header.getColumn(0, "icutkey")
                         + " icuttype="+this.ds_header.getColumn(0, "icuttype")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "itemUnitController");
        	this.gfn_setCommon("METHODNM", "saveItemUnit");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "itemUnitController");
        	this.gfn_setCommon("METHODNM", "saveItemUnit");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        				this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cbo_icuttype.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	
        	if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|icutkey|icuttype|icutqty|icutorder";
        	var sComp = "edt_owkey|edt_icutkey|cbo_icuttype|mdt_icutqty|mdt_icutorder";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.cbo_icuttype.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") this.fn_Update();
        					}
        				}
        			});
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|icutkey|icuttype|icutqty|icutorder";
        	var sComp = "edt_owkey|edt_icutkey|cbo_icuttype|mdt_icutqty|mdt_icutorder";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey" || e.columnid == "icutkey" || e.columnid == "icuttype"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "owkey"){
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        			this.edt_fn_get_master_impos.set_value(this.gv_Pvalue[2]);
        			
        			if(this.edt_fn_get_master_impos.value=='Y'){
        				this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        				return;
        			}

        		}
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_owkey_onchanged 실행 */
        this.edt_owkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        	}
        }

        this.cbo_icuttype_onitemchanged = function(obj,e)
        {
        	if(this.cbo_icuttype.value == "LAYER" || this.cbo_icuttype.value == "PL"){
        		this.mdt_icutqty.set_enable(true);
        		this.mdt_icutorder.set_enable(true);
        	}else{
        		this.mdt_icutqty.set_enable(false);
        		this.mdt_icutorder.set_enable(false);
        	}
        }

        
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_icuttype.addEventHandler("oncolumnchanged", this.ds_icuttype_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.edt_owkey.addEventHandler("onchanged", this.edt_owkey_onchanged, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.cbo_icuttype.addEventHandler("onitemchanged", this.cbo_icuttype_onitemchanged, this);

        };

        this.loadIncludeScript("ItemUnitPop.xfdl", true);

       
    };
}
)();
