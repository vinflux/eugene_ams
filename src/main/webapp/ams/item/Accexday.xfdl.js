﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Accexday");
                this.set_classname("style01");
                this.set_titletext("체화예상재고");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"level1\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm1\" type=\"STRING\" size=\"256\"/><Column id=\"level2\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm2\" type=\"STRING\" size=\"256\"/><Column id=\"level3\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm3\" type=\"STRING\" size=\"256\"/><Column id=\"accexday\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header2", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"level1\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm1\" type=\"STRING\" size=\"256\"/><Column id=\"level2\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm2\" type=\"STRING\" size=\"256\"/><Column id=\"level3\" type=\"STRING\" size=\"256\"/><Column id=\"level_nm3\" type=\"STRING\" size=\"256\"/><Column id=\"ickey\" type=\"STRING\" size=\"256\"/><Column id=\"icname\" type=\"STRING\" size=\"256\"/><Column id=\"accexday\" type=\"INT\" size=\"256\"/><Column id=\"accexday_flag\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param2", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_accexdayFlag", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_accexdayFlagDisp", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "8", null, null, "0", "0", this);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("58");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("59");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("60");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("61");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("62");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "28", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("63");
            obj.set_binddataset("ds_header2");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"170\"/><Column size=\"130\"/><Column size=\"170\"/><Column size=\"130\"/><Column size=\"170\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"80\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"OWKEY\"/><Cell col=\"4\" text=\"LEVEL1\"/><Cell col=\"5\" text=\"LEVEL_NM1\"/><Cell col=\"6\" text=\"LEVEL2\"/><Cell col=\"7\" text=\"LEVEL_NM2\"/><Cell col=\"8\" text=\"LEVEL3\"/><Cell col=\"9\" text=\"LEVEL_NM3\"/><Cell col=\"10\" text=\"ICKEY\"/><Cell col=\"11\" text=\"ICNAME\"/><Cell col=\"12\" text=\"SPEC\"/><Cell col=\"13\" text=\"UOM\"/><Cell col=\"14\" text=\"CHGUOM\"/><Cell col=\"15\" text=\"ACCEXDAY\"/><Cell col=\"16\" text=\"ACCEXDAY_FLAG\"/><Cell col=\"17\" text=\"INSERTDATE\"/><Cell col=\"18\" text=\"INSERTURKEY\"/><Cell col=\"19\" text=\"UPDATEDATE\"/><Cell col=\"20\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" style=\"align:left;\" text=\"bind:owkey\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level1\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"5\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level_nm1\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"6\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level2\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"7\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level_nm2\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"8\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level3\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"9\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:level_nm3\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"10\" style=\"align:left;background:EXPR(dataset.getRowType(currow) == &quot;2&quot; ? &quot;#fff8d5&quot; : &quot;&quot;);background2:EXPR(dataset.getRowType(currow) == &quot;2&quot; ? &quot;#fff5c2&quot; : &quot;&quot;);selectbackground:EXPR(dataset.getRowType(currow) == &quot;2&quot; ? &quot;#e1e0d9ff&quot; : &quot;&quot;);\" text=\"bind:ickey\" expandshow=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;show&quot; : &quot;hide&quot;\"/><Cell col=\"11\" style=\"align:left;\" text=\"bind:icname\"/><Cell col=\"12\" text=\"bind:spec\"/><Cell col=\"13\" text=\"bind:uom_desc\"/><Cell col=\"14\" displaytype=\"number\" text=\"bind:udf3\"/><Cell col=\"15\" displaytype=\"number\" edittype=\"masknumber\" text=\"bind:accexday\" editlimitbymask=\"decimal\"/><Cell col=\"16\" displaytype=\"combo\" edittype=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;combo&quot; : (dataset.getColumn(currow, &quot;accexday_flag&quot;) != &quot;1&quot; ? &quot;combo&quot; : &quot;none&quot;)\" style=\"align:left;\" text=\"bind:accexday_flag\" combodataset=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;ds_accexdayFlag&quot; : (dataset.getColumn(currow, &quot;accexday_flag&quot;) == &quot;1&quot; ? &quot;ds_accexdayFlagDisp&quot; : &quot;ds_accexdayFlag&quot; )\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"17\" displaytype=\"date\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"18\" text=\"bind:inserturkey\"/><Cell col=\"19\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"20\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("64");
            obj.set_text("체화예정재고 상품 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("5");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("체화예상재고");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","Tab00.tabpage1.div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","Tab00.tabpage1.div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","Tab00.tabpage2.div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","Tab00.tabpage2.div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.Static00","text","gds_lang","ME00108190_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Accexday.xfdl", "lib::Comm.xjs");
        this.registerScript("Accexday.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Accexday.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 체화예상재고
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        * 2018-06-14	CMJ					수정
        * 2018-10-03    KJH               카테고리탭 삭제
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.isFirst = true;

        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_accexdayFlag = '2';

        this.gv_pagingLimit;
        this.gv_dsCount;

        this.searchFalg = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	//그리드 초기화함수, tab화면이라서 tab 수 대로 Init 시켜준다.
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);

        	this.initCombo();
        	
        }

        this.initCombo = function(){
        	this.gfn_getCode("ACCEXDAY_FLAG", this.ds_accexdayFlag);
        	this.gfn_getCode("ACCEXDAY_FLAG", this.ds_accexdayFlagDisp);
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역 
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "accexdayController");
        	this.gfn_setCommon("METHODNM", "selectAccexday");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header2=OUT_rtnGrid ds_param2=OUT_PARAM";
        	var sParam   = "tabType=IC";
        	
        	if(this.gfn_isUpdate(this.ds_header2) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.gv_header.set_nodatatext("");
        				this.ds_header2.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.gv_header.set_nodatatext("");
        		this.ds_header2.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "accexdayController");
        	this.gfn_setCommon("METHODNM", "updateAccexday");
        	
        	var sSvcId   = "save";
        	var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
        		sInData  = "IN_list=ds_header2:U";
        	var sOutData = "";
        		sParam   = "tabType=IC";
        		
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		if(nErrCd == -30) this.gfn_alert("MSG_DUPLICATE_KEY", "KEYVALUE," + sErrMsg);
        		else this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        			if(this.ds_header2.rowcount > 0){
        				var nRow = this.ds_accexdayFlag.findRowExpr("field1 == '1'");
        				if(nRow != -1){ this.ds_accexdayFlag.deleteRow(nRow); }
        				
        				this.ds_header2.set_rowposition(0);
        				this.parent.fn_searchAfter(this.gv_header, this); // 검색조건 숨김
        				
        				//체화예정구분이 카테고리면 체크컬럼을 생성하지 않는다.
        				this.gv_accexdayFlag = this.ds_param2.getColumn(0, "accexday_flag");
        				this.ds_header2.addColumn("CHK");
        				this.ds_header2.addColumn("STATUS");
        				
        				this.gv_page.fn_pageSet(this.ds_param2.getColumn(0, "pagingLimit"), this.ds_param2.getColumn(0, "currentPage"), this.ds_param2.getColumn(0, "COUNT"));
        			}else{
        				this.gv_header.set_nodatatext(application.gv_nodataMsg);
        				this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), 1 ,0 );
        			}
        			
        			this.searchFalg = "";
        			this.gfn_constDsSet(this.gv_header);
        		
        	} else if(sSvcId == "save"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		
        		if(this.ds_param.rowcount == 0) this.parent.div_cond.btn_search.click();
        		else this.parent.div_cond.fn_pageSearch(this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "COUNT"));
        		
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sType = obj.getCellProperty("head", e.cell, "displaytype");
        	
        	if(sType != "checkbox" && colName != "" && colName != "CHK" && colName != "PAGING_NUM" && this.ds_header2.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(sType == "checkbox"){
        		this.ds_header2.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header2.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	
        		var oValue = { tabType : 'IC' };
        		this.parent.div_cond.fn_excelSearch("accexdayController/excelDown.do", this.gv_header, this, oValue);

        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {	
        	//조회조건이 카테고리일때, 신규생성한 row가 아니면 return
        	if(this.gv_accexdayFlag == '1' && this.ds_header2.getRowType(e.row) != '2') { return false; }
        	
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") {
        		obj.set_updatecontrol(true);
        	}
        	else if(e.columnid != "STATUS") { this.gfn_statusChk(obj, e.row); }
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "icname|accexday";
        	
        	if(this.gfn_isUpdate(this.ds_header2)){
        		if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header2, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        					this.fn_save();
        				}
        			})
        		};
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	if(this.gv_accexdayFlag == '1') { return; }
        	
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {

        		
        		nRow = this.ds_header2.addRow();
        		this.ds_header2.setColumn(nRow, "STATUS", "C");

        		var owkey = this.gfn_getUserInfo("owkeym").split(",");
        		this.ds_header2.setColumn(nRow, "owkey", owkey[0]);
        		this.ds_header2.setColumn(nRow, "accexday_flag", '2');
        		
        		this.ds_header2.set_rowposition(nRow);
        		this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "accexday"));

        		this.gv_header.setFocus();
        }

        /* div_splitTop_grd_header_onexpanddown 실행 */
        this.div_splitTop_grd_header_onexpanddown = function(obj,e)
        {
        	this.gv_Pvalue = "";

        	if(obj.getBindCellIndex("body", "ickey") == e.col){
        		var oArg = { putRow:""+e.row
        					, putObj:this.ds_header2 
        					, putTabFlag:'IC'
        		};
        		this.gfn_popup("AccexdayPop", "item::AccexdayPop.xfdl", oArg, 700, 402, "");
        	}
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header2.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        					while(nRow != -1){
        						if(this.ds_header2.getRowType(nRow) == "2"){
        							this.ds_header2.deleteRow(nRow);
        							nRow = this.ds_header2.findRowExpr("CHK == '1'");
        						}else{
        							this.ds_header2.setColumn(nRow, "STATUS", "D");
        							nRow = this.ds_header2.findRowExpr("CHK == '1'", nRow+1);
        						}
        					}
        					if( this.ds_header2.findRowExpr("STATUS == 'D'") > -1 )  this.fn_save();
        				}
        			})
        		
        	}
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header2.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header2.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_header.addEventHandler("onexpanddown", this.div_splitTop_grd_header_onexpanddown, this);

        };

        this.loadIncludeScript("Accexday.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
