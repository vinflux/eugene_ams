﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Substitution");
                this.set_classname("style01");
                this.set_titletext("상품 대체 코드");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"ickey\" type=\"STRING\" size=\"256\"/><Column id=\"sbkey\" type=\"STRING\" size=\"256\"/><Column id=\"udf1\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("14");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "148", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"200\"/><Column size=\"150\"/><Column size=\"220\"/><Column size=\"146\"/><Column size=\"80\"/><Column size=\"140\"/><Column size=\"100\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"80\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"OWKEY\"/><Cell col=\"3\" text=\"OWNAME\"/><Cell col=\"4\" text=\"ICKEY\"/><Cell col=\"5\" text=\"ICNAME\"/><Cell col=\"6\" text=\"SPEC\"/><Cell col=\"7\" text=\"CHGUOM\"/><Cell col=\"8\" text=\"ICUTTYPE\"/><Cell col=\"9\" text=\"SBKEY\"/><Cell col=\"10\" text=\"UDF1\"/><Cell col=\"11\" text=\"UDF2\"/><Cell col=\"12\" text=\"UDF3\"/><Cell col=\"13\" text=\"DELYN\"/><Cell col=\"14\" text=\"CLOSINGDATE\"/><Cell col=\"15\" text=\"INSERTDATE\"/><Cell col=\"16\" text=\"INSERTURKEY\"/><Cell col=\"17\" text=\"UPDATEDATE\"/><Cell col=\"18\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:owkey\"/><Cell col=\"3\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:owname\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ickey\" editdisplay=\"edit\"/><Cell col=\"5\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:icname\"/><Cell col=\"6\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:spec\"/><Cell col=\"7\" displaytype=\"number\" text=\"bind:chguom\"/><Cell col=\"8\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:icuttype_desc\"/><Cell col=\"9\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:sbkey\"/><Cell col=\"10\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:udf1\"/><Cell col=\"11\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:udf2\"/><Cell col=\"12\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:udf3\"/><Cell col=\"13\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:delyn_desc\"/><Cell col=\"14\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:closingdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"15\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"16\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:inserturKey\"/><Cell col=\"17\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"18\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:updateurKey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("상품 대체 코드 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", "580", null, null, "0", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"180\"/><Column size=\"300\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"CTNAME\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ctname\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_text("상품 대체 코드별 센터 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Grid("grd_detail_save", "absolute", null, "29", "580", null, "0", "0", this.div_splitBottom);
            obj.set_taborder("22");
            obj.set_binddataset("ds_detail_save");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_scrollbars("autoboth");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"180\"/><Column size=\"300\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"CTNAME\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ctname\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_left", "absolute", "590", "110", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("23");
            obj.set_text(">");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_right", "absolute", "590", "143", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("24");
            obj.set_text("<");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("25");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_excelUpload", "absolute", null, "0", "33", "24", "0", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_excelup");
            obj.set_tooltiptext("엑셀 업로드");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품 대체 코드");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","btn_excelUpload","tooltiptext","gds_lang","EXCELUPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","div_splitTop.Static00","text","gds_lang","ME00000440_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitBottom.Static06","text","gds_lang","ME00000440_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Substitution.xfdl", "lib::Comm.xjs");
        this.registerScript("Substitution.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Substitution.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품 대체 코드
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_detail_save = this.div_splitBottom.grd_detail_save;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";
        var dtlsearchcnt = 0;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        //	var gridMenuSet = ["grd_header^1|0|0|0|0|0|1|1", "grd_detail^0|0|0|0|0|0|1|1"];
        	this.gv_grdList = [this.gv_header, this.gv_detail, this.gv_detail_save]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	this.fn_calculateDetailPosition();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "substitutionController");
        	this.gfn_setCommon("METHODNM", "selectListSubstitution");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				this.ds_detail.clearData();
        				this.ds_detail_save.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		this.ds_detail.clearData();
        		this.ds_detail_save.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchDetail = function()
        {
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.ds_detail.clearData();
        	this.ds_detail_save.clearData();

        	this.gfn_setCommon("BEANID", "substitutionController");
        	this.gfn_setCommon("METHODNM", "selectSubstitutionCenter");
        	
            var sSvcId   = "selectDetail"+dtlsearchcnt++;
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid1 ds_detail_save=OUT_rtnGrid2";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " ickey="+this.ds_header.getColumn(this.gv_nRow, "ickey")
                         + " sbkey="+this.ds_header.getColumn(this.gv_nRow, "sbkey")
                         + " udf1="+this.ds_header.getColumn(this.gv_nRow, "udf1")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "substitutionController");
        	this.gfn_setCommon("METHODNM", "saveSubstitution");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_saveDetail = function()
        {
        	this.gfn_setCommon("BEANID", "substitutionController");
        	this.gfn_setCommon("METHODNM", "saveSubstitutionCenter");
        	
            var sSvcId   = "saveDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_list=ds_detail_save";
            var sOutData = "";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " ickey="+this.ds_header.getColumn(this.gv_nRow, "ickey")
                         + " sbkey="+this.ds_header.getColumn(this.gv_nRow, "sbkey")
                         + " udf1="+this.ds_header.getColumn(this.gv_nRow, "udf1")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_constDsSet(this.gv_header);
        		
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        			this.ds_header.addColumn("STATUS");
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId.substr(0, 12) == "selectDetail"){
        		this.ds_detail.addColumn("CHK");
        		this.ds_detail_save.addColumn("CHK");
        		
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		if(this.ds_detail_save.rowcount == 0){
        			this.gv_detail_save.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_detail);
        		this.gfn_constDsSet(this.gv_detail_save);
        		this.searchFalg = "";
        	}else if(sSvcId == "delete"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "saveDetail"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        // bottom 크기 조절
        this.fn_calculateDetailPosition = function(){
        	var btn_Gap = 10;
        	var btn_HGap = 47;
        	
        	var v_nX = nexacro.round(this.div_splitBottom.getOffsetWidth() / 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetWidth() / 2) - btn_Gap;
        	var v_nY = nexacro.round(this.gv_detail.getOffsetHeight() / 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetHeight() / 2);
        	
        	this.gv_detail.set_width(v_nX);
        	this.gv_detail_save.set_width(v_nX);
        	this.div_splitBottom.btn_left.set_left(v_nX+btn_Gap);
        	this.div_splitBottom.btn_right.set_left(v_nX+btn_Gap);

        	this.div_splitBottom.btn_left.set_top(v_nY+btn_Gap);
        	this.div_splitBottom.btn_right.set_top(v_nY+btn_HGap);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.rowChg = "Y";
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.grd_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_detail.rowcount > 0){
        		this.gfn_sortDataset(obj, e);
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("substitutionController/selectListSubstitutionExcel.do", this.gv_header, this, oValue);
        }

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("dataset.getRowType(currow) == '2' || dataset.getRowType(currow) == '8'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			this.fn_searchDetail();
        		}
        	}
        	
        	this.rowChg = "";
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK")	obj.set_updatecontrol(false);
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("dataset.getRowType(currow) == '2' || dataset.getRowType(currow) == '8'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var nChkCnt = this.ds_header.getCaseCount("STATUS == 'D'");
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(nChkCnt == this.ds_header.rowcount){
        			var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        			var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        			var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        			
        			this.gfn_confirm3Btn (
        				arrBtnText,
        				sMsgCode,
        				sMsgText,
        				function(msg, flag) {
        					if (flag != "0") {
        						if (flag == "1") this.fn_delete("");
        						if (flag == "2") this.fn_delete("ALL");
        					} else { //최소시 ds_header reset
        						this.ds_header.reset();
        					}
        				}
        			);
        		}else{
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.fn_delete("");
        				} else { //최소시 ds_header reset
        						this.ds_header.reset();
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			this.ds_header.setColumn(nRow, "STATUS", "D");
        			nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        		}
        		this.div_splitTop_btn_save_onclick();
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("SubstitutionPop", "item::SubstitutionPop.xfdl", oArg, 580, 373, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	if(this.ds_header.rowcount > 0){
        		var oArg = { argFlag:"U"
        					,menuId:this.parent.gv_menuId
        					,owkey:this.ds_header.getColumn(this.ds_header.rowposition, "owkey")
        					,ickey:this.ds_header.getColumn(this.ds_header.rowposition, "ickey")
        					,sbkey:this.ds_header.getColumn(this.ds_header.rowposition, "sbkey")
        					,udf1:this.ds_header.getColumn(this.ds_header.rowposition, "udf1")
        					,icutkey:this.ds_header.getColumn(this.ds_header.rowposition, "icutkey")
        				   };
        		this.gfn_popup("SubstitutionPop", "item::SubstitutionPop.xfdl", oArg, 580, 373, "");
        	}else{
        		this.gfn_alert("MSG_90001");
        	}
        }

        /* div_splitBottom_onsize 실행 */
        this.div_splitBottom_onsize = function(obj,e)
        {
        	this.fn_calculateDetailPosition();
        }

        /* div_splitBottom_btn_left_onclick 실행 */
        this.div_splitBottom_btn_left_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	var addRow = 0;
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			if(this.ds_detail_save.findRow("ctkey", this.ds_detail.getColumn(nRow, "ctkey")) == -1){
        				addRow = this.ds_detail_save.addRow();
        				this.ds_detail_save.copyRow(addRow, this.ds_detail, nRow);
        				this.ds_detail_save.setColumn(addRow, "owkey", this.ds_header.getColumn(this.gv_nRow, "owkey"));
        				this.ds_detail_save.setColumn(addRow, "ickey", this.ds_header.getColumn(this.gv_nRow, "ickey"));
        				this.ds_detail_save.setColumn(addRow, "sbkey", this.ds_header.getColumn(this.gv_nRow, "sbkey"));
        				this.ds_detail_save.setColumn(addRow, "udf1", this.ds_header.getColumn(this.gv_nRow, "udf1"));
        			}
        			
        			nRow = this.ds_detail.findRowExpr("CHK == '1'", nRow+1);
        		}
        	}
        	
        	this.ds_detail_save.set_keystring("S:+ctkey");
        }

        /* div_splitBottom_btn_right_onclick 실행 */
        this.div_splitBottom_btn_right_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			this.ds_detail_save.deleteRow(nRow);
        			nRow = this.ds_detail_save.findRowExpr("CHK == '1'");
        		}
        	}
        }

        /* div_splitBottom_btn_save_onclick 실행 */
        this.div_splitBottom_btn_save_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_detail_save)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_saveDetail();
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* oncelldblclick 실행 */
        this.oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        this.btn_excelUpload_onclick = function(obj,e)
        {
        	var oArg = {
        		menuId:"ME00000460",
        		uskey: "US00000620",
        		menuNm:"SUBSTITUTION EXCELUPLOAD",
        		menu: "comm::ExcelUploadPop.xfdl",
        		ultype : this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_SUBSTITUTION,
        		apkey : application.gv_activeApp,
        		controllerService : "substitutionUploadController",
        		uploadAfterSearch : "selectSubstitutionTemp",
        		tab2SearchService : "selectSubstitutionUploadInfo",
        		uploadService : "uploadSubstitutionExcel",
        		saveService : "saveSubstitution",
        		samplePath : "item",
        		sampleDownName : "Item_ICSBUP_v1.0.xls",
        		excelDownService : "excelDown"
        	};
        	
        	this.gfn_popup("substitutionExcelUploadPop", "comm::ExcelUploadPop.xfdl", oArg, 1918, 948, "");		
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitBottom.addEventHandler("onsize", this.div_splitBottom_onsize, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.grd_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.oncelldblclick, this);
            this.div_splitBottom.grd_detail_save.addEventHandler("onheadclick", this.grd_onheadclick, this);
            this.div_splitBottom.grd_detail_save.addEventHandler("oncelldblclick", this.oncelldblclick, this);
            this.div_splitBottom.btn_left.addEventHandler("onclick", this.div_splitBottom_btn_left_onclick, this);
            this.div_splitBottom.btn_right.addEventHandler("onclick", this.div_splitBottom_btn_right_onclick, this);
            this.div_splitBottom.btn_save.addEventHandler("onclick", this.div_splitBottom_btn_save_onclick, this);
            this.btn_excelUpload.addEventHandler("onclick", this.btn_excelUpload_onclick, this);

        };

        this.loadIncludeScript("Substitution.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
