﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ItemCodePop");
                this.set_classname("style01");
                this.set_titletext("상품 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,959,734);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"ictype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"icutkey\" type=\"STRING\" size=\"32\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"32\"/><Column id=\"rfidbottle\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"icgrtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"bottleprice\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"ctgkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflife\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"spec\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"taxyn\" type=\"STRING\" size=\"32\"/><Column id=\"uom\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"rfidbox\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"boxyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelfday\" type=\"STRING\" size=\"32\"/><Column id=\"uom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"icgrtype\" type=\"STRING\" size=\"32\"/><Column id=\"returnor_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"height\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"returnor\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"accstockdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"accstocktype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"serialinputtype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifecodetype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"weight\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"btlickey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"foodyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"volume\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"operstatype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lengthpi\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"heightpi\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"boxyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"accexday\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ictype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"stc_apply_grade_cd_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"taxyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ordstopdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"scmcheck_desc\" type=\"STRING\" size=\"32\"/><Column id=\"minordqty\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"foodyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"widthpi\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"boxickey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"maxordqty\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lengthbox\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"dispshelfkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck\" type=\"STRING\" size=\"32\"/><Column id=\"releasedays\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"operstatype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ordstartdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lo_non_stock_type_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf5\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"chittype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lo_non_stock_type\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"32\"/><Column id=\"ordqty\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"udf4\" type=\"STRING\" size=\"256\"/><Column id=\"udf1_desc\" type=\"STRING\" size=\"256\"/><Column id=\"udf1\" type=\"STRING\" size=\"32\"/><Column id=\"udf2\" type=\"STRING\" size=\"32\"/><Column id=\"brandcode\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"btlyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"stc_apply_grade_cd\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"boxprice\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"ickey\" type=\"STRING\" size=\"32\"/><Column id=\"scmurkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"btlyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifecodetype\" type=\"STRING\" size=\"32\"/><Column id=\"icname\" type=\"STRING\" size=\"32\"/><Column id=\"width\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"ackey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"regdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifeindicator_desc\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"shelflifeindicator\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"chittype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf6\" type=\"STRING\" size=\"256\"/><Column id=\"fn_get_master_impos\" type=\"STRING\" size=\"256\"/><Column id=\"udf6_desc\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"ictype\"/><Col id=\"icutkey\"/><Col id=\"icgrkey\"/><Col id=\"rfidbottle\"/><Col id=\"inserturkey\"/><Col id=\"icgrtype_desc\"/><Col id=\"bottleprice\"/><Col id=\"ctgkey\"/><Col id=\"shelflife\"/><Col id=\"spec\"/><Col id=\"delyn_desc\"/><Col id=\"taxyn\"/><Col id=\"uom\"/><Col id=\"updatedate\"/><Col id=\"rfidbox\"/><Col id=\"boxyn_desc\"/><Col id=\"shelfday\"/><Col id=\"uom_desc\"/><Col id=\"icgrtype\"/><Col id=\"returnor_desc\"/><Col id=\"height\"/><Col id=\"returnor\"/><Col id=\"accstockdate\"/><Col id=\"owkey\"/><Col id=\"accstocktype\"/><Col id=\"serialinputtype\"/><Col id=\"shelflifecodetype_desc\"/><Col id=\"weight\"/><Col id=\"btlickey\"/><Col id=\"foodyn\"/><Col id=\"volume\"/><Col id=\"operstatype\"/><Col id=\"lengthpi\"/><Col id=\"heightpi\"/><Col id=\"boxyn\"/><Col id=\"accexday\"/><Col id=\"ictype_desc\"/><Col id=\"stc_apply_grade_cd_desc\"/><Col id=\"taxyn_desc\"/><Col id=\"ordstopdate\"/><Col id=\"scmcheck_desc\"/><Col id=\"minordqty\"/><Col id=\"delyn\"/><Col id=\"foodyn_desc\"/><Col id=\"widthpi\"/><Col id=\"boxickey\"/><Col id=\"maxordqty\"/><Col id=\"lengthbox\"/><Col id=\"dispshelfkey\"/><Col id=\"closingdate\"/><Col id=\"scmcheck\"/><Col id=\"releasedays\"/><Col id=\"operstatype_desc\"/><Col id=\"ordstartdate\"/><Col id=\"lo_non_stock_type_desc\"/><Col id=\"udf5\"/><Col id=\"chittype\"/><Col id=\"lo_non_stock_type\"/><Col id=\"udf3\"/><Col id=\"ordqty\"/><Col id=\"udf4\"/><Col id=\"udf1\"/><Col id=\"udf2\"/><Col id=\"brandcode\"/><Col id=\"btlyn_desc\"/><Col id=\"stc_apply_grade_cd\"/><Col id=\"boxprice\"/><Col id=\"ickey\"/><Col id=\"scmurkey\"/><Col id=\"btlyn\"/><Col id=\"shelflifecodetype\"/><Col id=\"icname\"/><Col id=\"width\"/><Col id=\"ackey\"/><Col id=\"regdate\"/><Col id=\"shelflifeindicator_desc\"/><Col id=\"insertdate\"/><Col id=\"shelflifeindicator\"/><Col id=\"updateurkey\"/><Col id=\"chittype_desc\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ictype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_uom", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field1_desc\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/><Column id=\"field3\" type=\"STRING\" size=\"256\"/><Column id=\"field4\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_foodyn", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_lo_non_stock_type", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_shelflifecodetype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_serialinputtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_stc_apply_grade_cd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_run_item_cd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_icgrkey", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"field1\">AA3</Col><Col id=\"field2\">A동/창고/냉동</Col></Row><Row><Col id=\"field1\">AA4</Col><Col id=\"field2\">A동/창고/초저온</Col></Row><Row><Col id=\"field1\">AB3</Col><Col id=\"field2\">A동/보세/냉동</Col></Row><Row><Col id=\"field1\">AC3</Col><Col id=\"field2\">A동/</Col></Row><Row><Col id=\"field1\">BA2</Col></Row><Row><Col id=\"field1\">BB2</Col></Row><Row><Col id=\"field1\">CA1</Col></Row><Row><Col id=\"field1\">CB1</Col></Row><Row><Col id=\"field1\">DD1</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_rel_duedate", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static77", "absolute", "22", "89", null, "31", "22", null, this);
            obj.set_taborder("602");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static76", "absolute", "22", "182", null, "31", "22", null, this);
            obj.set_taborder("596");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "22", "213", null, "31", "22", null, this);
            obj.set_taborder("385");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "22", "58", null, "31", "22", null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "22", "120", null, "31", "22", null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "22", "151", null, "31", "22", null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static36", "absolute", "22", "647", null, "31", "22", null, this);
            obj.set_taborder("388");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "22", "337", null, "31", "22", null, this);
            obj.set_taborder("61");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("71");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "400", null, "77", "28", null, "15", this);
            obj.set_taborder("69");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "998", "89", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("상품코드");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("상품 대/중/소 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "328", "89", "122", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("상품구분");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ickey", "absolute", "1124", "93", "105", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            obj.set_inputmode("upper");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "151", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("상품단위코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "22", "213", "120", "31", null, null, this);
            obj.set_taborder("384");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("박스금액(원)");
            this.addChild(obj.name, obj);

            obj = new Static("Static35", "absolute", "327", "213", "120", "31", null, null, this);
            obj.set_taborder("386");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("공병금액(원)");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "482", null, "52", "28", null, "15", this);
            obj.set_taborder("70");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "138", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icutkey", "absolute", "148", "155", "138", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIcutkey", "absolute", "335", "155", "24", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "337", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("과세구분");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "289", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "1232", "93", "57", "22", null, null, this);
            obj.set_taborder("60");
            obj.set_text("중복체크");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_ictype", "absolute", "454", "93", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_innerdataset("ds_ictype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Edit("edt_ctgkey", "absolute", "148", "124", "138", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCtgkey", "absolute", "289", "124", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", "679", null, "1", "22", null, this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "244", null, "31", "22", null, this);
            obj.set_taborder("488");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "22", "182", "120", "31", null, null, this);
            obj.set_taborder("489");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("매입처코드");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ackey", "absolute", "148", "186", "138", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAckey", "absolute", "289", "186", "24", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "632", "213", "122", "31", null, null, this);
            obj.set_taborder("494");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("부피");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "22", "275", null, "31", "22", null, this);
            obj.set_taborder("497");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static46", "absolute", "327", "244", "120", "31", null, null, this);
            obj.set_taborder("499");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("박스세로");
            this.addChild(obj.name, obj);

            obj = new Static("Static47", "absolute", "632", "244", "122", "31", null, null, this);
            obj.set_taborder("502");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("박스가로");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "22", "306", null, "31", "22", null, this);
            obj.set_taborder("506");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "327", "275", "120", "31", null, null, this);
            obj.set_taborder("508");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("단품세로");
            this.addChild(obj.name, obj);

            obj = new Static("Static48", "absolute", "632", "275", "122", "31", null, null, this);
            obj.set_taborder("509");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("단품가로");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_taxyn", "absolute", "453", "341", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("28");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static20", "absolute", "22", "368", null, "31", "22", null, this);
            obj.set_taborder("516");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static50", "absolute", "632", "368", "122", "31", null, null, this);
            obj.set_taborder("519");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("식분구분");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_foodyn", "absolute", "758", "372", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("29");
            obj.set_innerdataset("ds_foodyn");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static33", "absolute", "22", "399", null, "31", "22", null, this);
            obj.set_taborder("62");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "327", "399", "120", "31", null, null, this);
            obj.set_taborder("525");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("입고기준일");
            this.addChild(obj.name, obj);

            obj = new Static("Static51", "absolute", "328", "368", "122", "31", null, null, this);
            obj.set_taborder("526");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("유통기한체크유형");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_shelflifecodetype", "absolute", "454", "372", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("32");
            obj.set_innerdataset("ds_shelflifecodetype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_enable("true");

            obj = new Static("Static52", "absolute", "22", "430", null, "31", "22", null, this);
            obj.set_taborder("530");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "22", "461", null, "31", "22", null, this);
            obj.set_taborder("59");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static57", "absolute", "22", "430", "120", "31", null, null, this);
            obj.set_taborder("541");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("RFID_박스");
            this.addChild(obj.name, obj);

            obj = new Static("Static58", "absolute", "327", "430", "120", "31", null, null, this);
            obj.set_taborder("544");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("RFID_병");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "22", "492", null, "31", "22", null, this);
            obj.set_taborder("547");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "22", "461", "120", "31", null, null, this);
            obj.set_taborder("548");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("브랜드코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static59", "absolute", "327", "461", "120", "31", null, null, this);
            obj.set_taborder("549");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("매출처반품구분");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_brandcode", "absolute", "148", "465", "165", "22", null, null, this);
            obj.set_taborder("39");
            obj.set_inputtype("number,sign");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Static("Static60", "absolute", "632", "399", "122", "31", null, null, this);
            obj.set_taborder("551");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("출고가능일수");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_returnor", "absolute", "453", "465", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("40");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static01", "absolute", "22", "523", null, "31", "22", null, this);
            obj.set_taborder("555");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "22", "554", null, "31", "22", null, this);
            obj.set_taborder("563");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "492", "120", "31", null, null, this);
            obj.set_taborder("564");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("재고등급코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static63", "absolute", "327", "492", "120", "31", null, null, this);
            obj.set_taborder("565");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("체화예정소진일수");
            this.addChild(obj.name, obj);

            obj = new Static("Static64", "absolute", "632", "430", "122", "31", null, null, this);
            obj.set_taborder("566");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("매출처발주입수");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_stc_apply_grade_cd", "absolute", "148", "496", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("42");
            obj.set_innerdataset("@ds_stc_apply_grade_cd");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static65", "absolute", "22", "585", null, "31", "22", null, this);
            obj.set_taborder("571");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static69", "absolute", "22", "616", null, "31", "22", null, this);
            obj.set_taborder("579");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static70", "absolute", "22", "554", "120", "31", null, null, this);
            obj.set_taborder("580");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("공박스상품코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static71", "absolute", "22", "585", "120", "31", null, null, this);
            obj.set_taborder("581");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("공병상품코드");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_boxickey", "absolute", "148", "558", "138", "22", null, null, this);
            obj.set_taborder("48");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchBoxickey", "absolute", "289", "558", "24", "22", null, null, this);
            obj.set_taborder("49");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static72", "absolute", "632", "461", "122", "31", null, null, this);
            obj.set_taborder("584");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("보관수명(Day)");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_btlickey", "absolute", "148", "589", "138", "22", null, null, this);
            obj.set_taborder("51");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchBtlickey", "absolute", "289", "589", "24", "22", null, null, this);
            obj.set_taborder("52");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_ordstartdate", "absolute", "148", "620", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("54");
            obj.set_dateformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_value("null");
            obj.set_editformat("yyyy-MM-dd");

            obj = new Static("Static74", "absolute", "22", "647", "120", "31", null, null, this);
            obj.set_taborder("591");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("반영일자");
            this.addChild(obj.name, obj);

            obj = new Static("Static75", "absolute", "327", "647", "120", "31", null, null, this);
            obj.set_taborder("592");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("종료일시");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_closingdate", "absolute", "453", "651", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("66");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.set_editformat("yyyy-MM-dd HH:mm");
            obj.style.set_cursor("hand");

            obj = new Calendar("cal_regdate", "absolute", "148", "651", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("57");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_value("null");

            obj = new Edit("edt_icgrkey", "absolute", "1019", "14", "138", "22", null, null, this);
            obj.set_taborder("593");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIcgrkey", "absolute", "1160", "14", "24", "22", null, null, this);
            obj.set_taborder("594");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctgname", "absolute", "327", "124", "291", "22", null, null, this);
            obj.set_taborder("595");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static45", "absolute", "22", "244", "120", "31", null, null, this);
            obj.set_taborder("498");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("박스높이");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "275", "120", "31", null, null, this);
            obj.set_taborder("507");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("단품높이");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "22", "337", "120", "31", null, null, this);
            obj.set_taborder("517");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("배수상한상품최소수량");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "22", "368", "120", "31", null, null, this);
            obj.set_taborder("524");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("유통기한체크여부");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_shelflifeindicator", "absolute", "148", "372", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("30");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static53", "absolute", "22", "399", "120", "31", null, null, this);
            obj.set_taborder("531");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("시리얼입력유형");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_serialinputtype", "absolute", "148", "403", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("33");
            obj.set_innerdataset("ds_serialinputtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static15", "absolute", "22", "306", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("규격");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_spec", "absolute", "148", "310", "165", "22", null, null, this);
            obj.set_taborder("24");
            obj.set_enable("true");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icutname", "absolute", "367", "155", "251", "22", null, null, this);
            obj.set_taborder("597");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_acname", "absolute", "327", "186", "291", "22", null, null, this);
            obj.set_taborder("598");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static62", "absolute", "632", "647", "122", "31", null, null, this);
            obj.set_taborder("558");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("출고마감일자");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "632", "553", "122", "31", null, null, this);
            obj.set_taborder("556");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("상품 추가 설명");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_add_expln", "absolute", "758", "557", "165", "22", null, null, this);
            obj.set_taborder("53");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_boxicname", "absolute", "327", "558", "291", "22", null, null, this);
            obj.set_taborder("600");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_btlicname", "absolute", "327", "589", "291", "22", null, null, this);
            obj.set_taborder("601");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_boxprice", "absolute", "148", "217", "165", "22", null, null, this);
            obj.set_taborder("15");
            obj.set_mask("#,###");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_height", "absolute", "148", "248", "165", "22", null, null, this);
            obj.set_taborder("18");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_heightpi", "absolute", "148", "279", "165", "22", null, null, this);
            obj.set_taborder("21");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_bottleprice", "absolute", "453", "217", "165", "22", null, null, this);
            obj.set_taborder("16");
            obj.set_mask("#,###");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lengthbox", "absolute", "453", "248", "165", "22", null, null, this);
            obj.set_taborder("19");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lengthpi", "absolute", "453", "279", "165", "22", null, null, this);
            obj.set_taborder("22");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_volume", "absolute", "758", "217", "165", "22", null, null, this);
            obj.set_taborder("17");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_width", "absolute", "758", "248", "165", "22", null, null, this);
            obj.set_taborder("20");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_widthpi", "absolute", "758", "279", "165", "22", null, null, this);
            obj.set_taborder("23");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_minordqty", "absolute", "148", "341", "165", "22", null, null, this);
            obj.set_taborder("27");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_rfidbox", "absolute", "148", "434", "165", "22", null, null, this);
            obj.set_taborder("36");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_shelflife", "absolute", "453", "403", "165", "22", null, null, this);
            obj.set_taborder("34");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_rfidbottle", "absolute", "453", "434", "165", "22", null, null, this);
            obj.set_taborder("37");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_accexday", "absolute", "453", "496", "165", "22", null, null, this);
            obj.set_taborder("43");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_releasedays", "absolute", "758", "403", "165", "22", null, null, this);
            obj.set_taborder("35");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_ordqty", "absolute", "758", "434", "165", "22", null, null, this);
            obj.set_taborder("38");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_shelfday", "absolute", "758", "465", "165", "22", null, null, this);
            obj.set_taborder("41");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "327", "62", "291", "22", null, null, this);
            obj.set_taborder("603");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static37", "absolute", "22", "616", "120", "31", null, null, this);
            obj.set_taborder("604");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("최초발주일자(매출처)");
            this.addChild(obj.name, obj);

            obj = new Static("Static38", "absolute", "327", "616", "120", "31", null, null, this);
            obj.set_taborder("605");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("발주정지예정일자");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_ordstopdate", "absolute", "453", "620", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("55");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_value("null");

            obj = new Static("Static61", "absolute", "632", "616", "122", "31", null, null, this);
            obj.set_taborder("607");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("수주마감시간");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "327", "307", "120", "31", null, null, this);
            obj.set_taborder("490");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("중량");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_weight", "absolute", "453", "311", "165", "22", null, null, this);
            obj.set_taborder("25");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "992", "151", "122", "31", null, null, this);
            obj.set_taborder("613");
            obj.set_text("상품그룹유형");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icgrtype", "absolute", "1118", "155", "165", "22", null, null, this);
            obj.set_taborder("614");
            obj.set_maxlength("16");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static73", "absolute", "632", "522", "122", "31", null, null, this);
            obj.set_taborder("615");
            obj.set_text("입수수량");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_chguom", "absolute", "758", "526", "165", "22", null, null, this);
            obj.set_taborder("50");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Static("Static54", "absolute", "632", "492", "122", "31", null, null, this);
            obj.set_taborder("532");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("품목타입");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_itemtype", "absolute", "758", "496", "165", "22", null, null, this);
            obj.set_taborder("44");
            obj.set_maxlength("33");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("medt_order_duetime", "absolute", "758", "621", "165", "22", null, null, this);
            obj.set_taborder("56");
            obj.set_limitbymask("both");
            obj.set_type("string");
            obj.set_mask("99:99:99");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static67", "absolute", "632", "336", "122", "31", null, null, this);
            obj.set_taborder("573");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("공박스유무");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_boxyn", "absolute", "758", "340", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("14");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static07", "absolute", "327", "523", "120", "31", null, null, this);
            obj.set_taborder("479");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전표생성구분");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_chittype", "absolute", "453", "527", "165", "22", null, null, this);
            obj.set_taborder("46");
            obj.set_maxlength("33");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fn_get_master_impos", "absolute", "992", "54", "183", "22", null, null, this);
            obj.set_taborder("625");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_release_duedate0", "absolute", "977", "651", "165", "22", null, null, this);
            obj.set_taborder("626");
            obj.set_inputfilter("dot,comma,alpha,space");
            obj.set_inputtype("number,sign");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Combo("edt_release_duedate", "absolute", "758", "653", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("627");
            obj.set_innerdataset("@ds_rel_duedate");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.style.set_background("#edededff");

            obj = new Button("btn_insert", "absolute", "290", "155", "41", "22", null, null, this);
            obj.set_taborder("628");
            obj.set_text("추가");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "632", "151", "122", "31", null, null, this);
            obj.set_taborder("631");
            obj.set_text("단위(UOM)");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_uom", "absolute", "758", "155", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("632");
            obj.set_innerdataset("ds_uom");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field1_desc");

            obj = new Static("Static55", "absolute", "22", "89", "121", "31", null, null, this);
            obj.set_taborder("635");
            obj.set_text("상품그룹코드");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_ichrkey", "absolute", "148", "92", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("636");
            obj.set_innerdataset("@ds_icgrkey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static12", "absolute", "632", "58", "120", "30", null, null, this);
            obj.set_taborder("637");
            obj.set_text("상품명");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icname", "absolute", "758", "61", "165", "22", null, null, this);
            obj.set_taborder("638");
            obj.set_maxlength("33");
            this.addChild(obj.name, obj);

            obj = new Static("Static56", "absolute", "632", "89", "122", "30", null, null, this);
            obj.set_taborder("639");
            obj.set_text("물류그룹코드");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("edt_loggrpcd", "absolute", "758", "92", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("640");
            obj.set_innerdataset("@ds_loggrpcd");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static68", "absolute", "632", "306", "122", "31", null, null, this);
            obj.set_taborder("641");
            obj.set_text("공병유무");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_btlyn", "absolute", "758", "310", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("642");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static49", "absolute", "632", "119", "122", "31", null, null, this);
            obj.set_taborder("643");
            obj.set_text("무재고상품구분");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_lo_non_stock_type", "absolute", "758", "123", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("644");
            obj.set_innerdataset("ds_lo_non_stock_type");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static66", "absolute", "632", "182", "120", "31", null, null, this);
            obj.set_taborder("645");
            obj.set_text("운영상태");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_operstatype", "absolute", "758", "186", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("646");
            obj.set_innerdataset("@ds_run_item_cd");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static22", "absolute", "23", "523", "120", "31", null, null, this);
            obj.set_taborder("647");
            obj.set_text("배수상한상품최대수량");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_maxordqty", "absolute", "149", "527", "165", "22", null, null, this);
            obj.set_taborder("648");
            this.addChild(obj.name, obj);

            obj = new Static("Static78", "absolute", "632", "584", "122", "31", null, null, this);
            obj.set_taborder("649");
            obj.set_text("팔렛 입수 수량");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pltqty", "absolute", "758", "588", "165", "22", null, null, this);
            obj.set_taborder("650");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 959, 734, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item89","edt_release_duedate0","value","ds_header","udf6");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_icgrtype","value","ds_header","icgrtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","ICGRTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_ickey","value","ds_header","ickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","ICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","CTGKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","ICTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","ICUTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","Static25","text","gds_lang","BOXPRICE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item41","Static35","text","gds_lang","BOTTLEPRICE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","ITEM CODE COPY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item45","edt_icutkey","value","ds_header","icutkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","SPEC");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","TAXYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_spec","value","ds_header","spec");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","ITEMCODE HEADER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","cbo_ictype","value","ds_header","ictype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","edt_ctgkey","value","ds_header","ctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","Static07","text","gds_lang","CHITTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","edt_chittype","value","ds_header","chittype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Static24","text","gds_lang","ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static30","text","gds_lang","WEIGHT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item32","edt_ackey","value","ds_header","ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item35","Static32","text","gds_lang","VOLUME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item47","Static45","text","gds_lang","HEIGHTBOX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","Static46","text","gds_lang","LENGTHBOX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item53","Static47","text","gds_lang","WIDTHBOX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","Static14","text","gds_lang","HEIGHTPI");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static31","text","gds_lang","LENGTHPI");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static48","text","gds_lang","WIDTHPI");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","cbo_taxyn","value","ds_header","taxyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","MINORDQTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item56","Static50","text","gds_lang","FOODYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","cbo_foodyn","value","ds_header","foodyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","Static43","text","gds_lang","SHELFLIFEINDICATOR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static44","text","gds_lang","SHELFLIFE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item59","Static51","text","gds_lang","SHELFLIFECODETYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_shelflifecodetype","value","ds_header","shelflifecodetype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item72","cbo_shelflifeindicator","value","ds_header","shelflifeindicator");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item73","Static53","text","gds_lang","SERIALINPUTTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item74","Static54","text","gds_lang","ITEMTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item78","cbo_serialinputtype","value","ds_header","serialinputtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","Static57","text","gds_lang","RFIDBOX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item80","Static58","text","gds_lang","RFIDBOTTLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","Static17","text","gds_lang","BRANDCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","Static59","text","gds_lang","RETURNOR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item83","edt_brandcode","value","ds_header","brandcode");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item84","Static60","text","gds_lang","RELEASEDAYS");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item86","cbo_returnor","value","ds_header","returnor");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static13","text","gds_lang","ADD_EXPLN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item87","Static62","text","gds_lang","RELEASE_DUEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item88","edt_add_expln","value","ds_header","udf4");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","Static06","text","gds_lang","STC_APPLY_GRADE_CD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","Static63","text","gds_lang","ACCEXDAY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item91","Static64","text","gds_lang","ORDQTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item94","cbo_stc_apply_grade_cd","value","ds_header","stc_apply_grade_cd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item96","Static67","text","gds_lang","BOXYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item100","cbo_boxyn","value","ds_header","boxyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item101","Static70","text","gds_lang","BOXICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item102","Static71","text","gds_lang","BTLICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item103","edt_boxickey","value","ds_header","boxickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item104","Static72","text","gds_lang","SHELFDAY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item105","edt_btlickey","value","ds_header","btlickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item107","cal_ordstartdate","value","ds_header","ordstartdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item108","Static74","text","gds_lang","REGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item109","Static75","text","gds_lang","CLOSINGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item110","cal_closingdate","value","ds_header","closingdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item111","cal_regdate","value","ds_header","regdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item112","edt_icgrkey","value","ds_header","icgrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item113","mdt_boxprice","value","ds_header","boxprice");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item114","mdt_height","value","ds_header","height");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item115","mdt_heightpi","value","ds_header","heightpi");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","mdt_bottleprice","value","ds_header","bottleprice");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item52","mdt_weight","value","ds_header","weight");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","mdt_lengthbox","value","ds_header","lengthbox");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item116","mdt_lengthpi","value","ds_header","lengthpi");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","mdt_volume","value","ds_header","volume");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item30","mdt_width","value","ds_header","width");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item54","mdt_widthpi","value","ds_header","widthpi");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item36","mdt_minordqty","value","ds_header","minordqty");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item61","mdt_rfidbox","value","ds_header","rfidbox");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","mdt_shelflife","value","ds_header","shelflife");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","mdt_rfidbottle","value","ds_header","rfidbottle");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","mdt_accexday","value","ds_header","accexday");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","mdt_releasedays","value","ds_header","releasedays");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item81","mdt_ordqty","value","ds_header","ordqty");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item82","mdt_shelfday","value","ds_header","shelfday");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","Static37","text","gds_lang","ORDSTARTDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item40","Static38","text","gds_lang","ORDSTOPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cal_ordstopdate","value","ds_header","ordstopdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","Static61","text","gds_lang","ORDER_DUETIME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item85","Static73","text","gds_lang","CHGUOM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item92","edt_chguom","value","ds_header","udf3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item76","edt_itemtype","value","ds_header","udf1_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item77","medt_order_duetime","value","ds_header","udf5");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item90","edt_release_duedate","value","ds_header","udf6");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item106","btn_insert","text","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","Static19","text","gds_lang","UOM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","cbo_uom","value","ds_header","uom");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Static55","text","gds_lang","ICGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item79","cbo_ichrkey","value","ds_header","icgrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","Static12","text","gds_lang","ICNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","edt_icname","value","ds_header","icname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item75","Static56","text","gds_lang","LOGGRPCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item93","edt_loggrpcd","value","ds_header","udf2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item98","Static68","text","gds_lang","BTLYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item99","cbo_btlyn","value","ds_header","btlyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","Static49","text","gds_lang","LO_NON_STOCK_TYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","cbo_lo_non_stock_type","value","ds_header","lo_non_stock_type");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item95","Static66","text","gds_lang","OPERSTATYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item97","cbo_operstatype","value","ds_header","operstatype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","MAXORDQTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","mdt_maxordqty","value","ds_header","maxordqty");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item117","Static78","text","gds_lang","PLTQTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item118","edt_pltqty","value","ds_header","pltqty");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ItemCodeCopyPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ItemCodeCopyPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ItemCodePop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ickey = "";
        this.gv_icutkey = "";
        this.gv_ttqty="";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("RELEASE_DUEDATE", this.ds_rel_duedate, "");
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	this.gfn_getCode("ICTYPE", this.ds_ictype, "");
        	this.gfn_getCode("LO_NON_STOCK_TYPE", this.ds_lo_non_stock_type, "");
        	this.gfn_getCode("FOODYN", this.ds_foodyn, "");
        	this.gfn_getCode("STC_APPLY_GRADE_CD", this.ds_stc_apply_grade_cd);
        	this.gfn_getCode("RUN_ITEM_ID", this.ds_run_item_cd, "");
        	this.gfn_getCode("SHELFLIFECODETYPE", this.ds_shelflifecodetype, "");
        	this.gfn_getCode("SERIALINPUTTYPE", this.ds_serialinputtype, "");
        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd, "");
        	this.gfn_getCode("UOM", this.ds_uom, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_ickey    = this.gfn_isNullEmpty(this.parent.ickey);
            this.gv_icutkey  = this.gfn_isNullEmpty(this.parent.icutkey);
            this.gv_ttqty	 = this.gfn_isNullEmpty(this.parent.ttqty);
        	this.gv_owkey	 = this.gfn_isNullEmpty(this.parent.owkey);
            if(this.gv_flag == "U"){
        		if(this.gv_owkey == "EUGENESF") {
        			this.Static24.set_cssclass("sta_WF_head");
        		}else {
        			this.Static24.set_cssclass("sta_WF_head_duty");
        		}
            
        		// Search cond, searchId, use Dataset, combo Name, insert value, default Value, callback
        		this.gfn_getCommCodeTran( "icutkey="+this.gv_icutkey+'|'+"owkey="+this.gv_owkey
        								, "270"
        								, "ds_uom"
        								, "cbo_uom"
        								, ""
        								, ""
        								, "fn_search"
        								);
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_ickey.set_enable(false);
        		//this.edt_loggrpcd.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_searchIcgr(this.gv_owkey);
        		//wms 가용재고 존재하면 필수값 수정불가
        		if(this.parent.shelflifeindicator == 'Y') {
        			this.cbo_shelflifecodetype.set_enable(true);
        		
        		}else {
        			this.cbo_shelflifecodetype.set_value('');
        			this.cbo_shelflifecodetype.set_enable(false);
        		}
        		
        //		this.fn_search();
            }else{
        		var owkey = this.gfn_getUserInfo("owkeym").split(",");
        		this.gv_owkey = owkey[0];
        		
        		//this.ds_header.setColumn(0, "owkey", owkey[0]);
        		this.ds_header.setColumn(0, "taxyn", "N");
        		this.ds_header.setColumn(0, "boxprice", "0");
        		this.ds_header.setColumn(0, "bottleprice", "0");
        		this.ds_header.setColumn(0, "weight", "0");
        		this.ds_header.setColumn(0, "volume", "0");
        		this.ds_header.setColumn(0, "height", "0");
        		this.ds_header.setColumn(0, "lengthbox", "0");
        		this.ds_header.setColumn(0, "width", "0");
        		this.ds_header.setColumn(0, "heightpi", "0");
        		this.ds_header.setColumn(0, "lengthpi", "0");
        		this.ds_header.setColumn(0, "widthpi", "0");
        		this.ds_header.setColumn(0, "releasedays", "0");
        		this.ds_header.setColumn(0, "accexday", "0");
        		this.ds_header.setColumn(0, "ordqty", "0");
        		this.ds_header.setColumn(0, "shelfday", "0");
        		
        		this.fn_searchIcgr(owkey[0]);
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
            }

        	this.ds_header.applyChange();
        	
        	this.gfn_decimalPointSet("mdt_height|mdt_heightpi|mdt_weight|mdt_lengthbox|mdt_lengthpi|mdt_volume|mdt_width|mdt_widthpi|mdt_minordqty|mdt_rfidbox|mdt_maxordqty|mdt_shelflife|mdt_rfidbottle|mdt_ordqty|mdt_shelfday");
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "selectItemCode");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " ickey="+this.gv_ickey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchCode = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "ctgController");
        	this.gfn_setCommon("METHODNM", "searchCtgInfo");
        	
            var sSvcId   = "selectCode";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "owkey="+this.edt_owkey.value
                         + " opval=eq"
                         ;
        	
            if(this.gfn_isNotNull(this.edt_ctgkey.value)) sParam += " searchValue="+this.edt_ctgkey.value;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_searchCodeBoxic = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "cntrController");
        	this.gfn_setCommon("METHODNM", "searchCntrKeySearchInfo");
        	
            var sSvcId   = "selectCodeBoxic";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "value="+this.edt_boxickey.value
                         + " opval=eq"
                         ;

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_searchCodeBtlic = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "cntrController");
        	this.gfn_setCommon("METHODNM", "searchCntrKeySearchInfo");
        	
            var sSvcId   = "selectCodeBtlic";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "value="+this.edt_btlickey.value
                         + " opval=eq"
                         ;

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_CheckCode = function()
        {

        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "selectItemCode");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   =  "owkey="+this.ds_header.getColumn(0, "owkey")
                         + " ickey="+this.ds_header.getColumn(0, "ickey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchIcgr = function(owkey)
        {
        	this.ds_icgrkey.clearData();

        	this.gfn_setCommon("BEANID", "itemGroupController");
        	this.gfn_setCommon("METHODNM", "selectItemGroupCode");
        	
            var sSvcId   = "selectIcgr";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_icgrkey=OUT_rtnGrid";
            var sParam   =  "owkey="+owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "saveItemCode");
        	
        	this.ds_header.setColumn(0,'icgrtype','CATEGORY');
        	this.ds_header.setColumn(0, "ickey", "");
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        
        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        		
        		if(this.gfn_isNotNull(this.edt_ctgkey.value)){
        			this.gv_code = "Y";
        			this.fn_searchCode();
        		}
        		
        		key = "owkey";
        		value = this.edt_owkey.value;
        		
        		if(this.gfn_isNotNull(this.edt_icutkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_icutkey.value, "100", application.gv_ams, "", "", this.edt_icutkey, this.edt_icutname);
        		}
        		
        		if(this.gfn_isNotNull(this.edt_ackey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_ackey.value, "40", application.gv_ams, "", "", this.edt_ackey, this.edt_acname);
        		}
        		
        		if(this.gfn_isNotNull(this.edt_boxickey.value)){
        			this.gv_code = "Y";
        			this.fn_searchCodeBoxic();
        		}
        		
        		if(this.gfn_isNotNull(this.edt_btlickey.value)){
        			this.gv_code = "Y";
        			this.fn_searchCodeBtlic();
        		}
        	}else if(sSvcId == "selectCode"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_ctgname.set_value(this.ds_code.getColumn(0, "ctgname"));
        			this.edt_ctgname.set_tooltiptext(this.ds_code.getColumn(0, "ctgname"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_ctgname.set_value("");
        					this.edt_ctgkey.setFocus();
        				});
        			}else{
        				this.edt_ctgname.set_value("");
        				this.edt_ctgkey.setFocus();
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "selectCodeBoxic"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_boxicname.set_value(this.ds_code.getColumn(0, "field2"));
        			this.edt_boxicname.set_tooltiptext(this.ds_code.getColumn(0, "field2"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_boxicname.set_value("");
        					this.edt_boxickey.setFocus();
        				});
        			}else{
        				this.edt_boxicname.set_value("");
        				this.edt_boxickey.setFocus();
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "selectCodeBtlic"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_btlicname.set_value(this.ds_code.getColumn(0, "field2"));
        			this.edt_btlicname.set_tooltiptext(this.ds_code.getColumn(0, "field2"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_btlicname.set_value("");
        					this.edt_btlickey.setFocus();
        				});
        			}else{
        				this.edt_btlicname.set_value("");
        				this.edt_btlickey.setFocus();
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cbo_icuttype.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        this.fn_calCutValue = function(sValue,colId)
        {
        	this.ds_header.setColumn(0, colId, sValue.substr(0, 14));
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	
        	if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        	}
        	var dsObj = "";
        	var dsCol = "";
        	var sComp = "";
        	var owkey = this.edt_owkey.value;
        	
        	if(owkey == "EUGENESF") {
        		dsObj = this.ds_header;
        		dsCol = "owkey|ctgkey|icutkey|icname|icgrkey|ictype|uom|operstatype|shelflifeindicator|lo_non_stock_type|foodyn|udf2";
        		sComp = "edt_owkey|edt_ickey|edt_ctgkey|edt_icutkey|edt_icname|cbo_ichrkey|cbo_ictype|cbo_uom|cbo_operstatype|cbo_shelflifeindicator|cbo_lo_non_stock_type|cbo_foodyn|edt_loggrpcd";
        	}else {
        		dsObj = this.ds_header;
        		dsCol = "owkey|ctgkey|icutkey|ackey|icname|icgrkey|ictype|uom|operstatype|shelflifeindicator|lo_non_stock_type|foodyn|udf2";
        		sComp = "edt_owkey|edt_ickey|edt_ctgkey|edt_icutkey|edt_ackey|edt_icname|cbo_ichrkey|cbo_ictype|cbo_uom|cbo_operstatype|cbo_shelflifeindicator|cbo_lo_non_stock_type|cbo_foodyn|edt_loggrpcd";
        	}
        	
        	
        	//var shelflifeindicator = this.ds_header.getColumn(0,"shelflifeindicator");
         	if(this.cbo_shelflifeindicator.value == 'Y' ){
        		if(this.gfn_isNull(this.cbo_shelflifecodetype.value)){
        			this.gfn_alert("MSG_10001", "", function(msg, flag){
        				this.cbo_shelflifecodetype.setFocus();
        			});
        			return;
        		}
        	}
        	
        	/*수주마감시간 시간 체크 루틴*/
        	var nDtChk = this.medt_order_duetime.text;
        	if(nDtChk.length > 0 && Number(nDtChk) > 0) {
        		if(nDtChk.substring(0,1) == "0"){
        			if(nDtChk.substring(1,2) == "0")
        				nDtChk = "00"+Number(this.medt_order_duetime.value)+"";
        			else
        				nDtChk = "0"+Number(this.medt_order_duetime.value)+"";
        		} else 
        		  nDtChk = Number(this.medt_order_duetime.text)+"";
        		if( nDtChk.length != 6 || 
        		   ( Number(nDtChk.substring(0,2)) > 23  ||  Number(nDtChk.substring(0,2)) < 0)||
        		   ( Number(nDtChk.substring(2,4)) < 0 || Number(nDtChk.substring(2,4)) > 59)||
        		   ( Number(nDtChk.substring(4,6)) < 0 || Number(nDtChk.substring(4,6)) > 59)){
        		  this.gfn_alert("MSG_CHKDATA_NOTIME", "", function(msg, flag){
        					this.medt_order_duetime.setFocus();
        				});
        		  return;
        		 }
        	}
        	if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_ctgname.value) && !this.gfn_isNull(this.edt_ctgkey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_ctgkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_icutname.value) && !this.gfn_isNull(this.edt_icutkey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_icutkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_boxicname.value) && !this.gfn_isNull(this.edt_boxickey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_boxickey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_btlicname.value) && !this.gfn_isNull(this.edt_btlickey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_btlickey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_icname.value) && !this.gfn_isNull(this.edt_icname.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_icname.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_ackey.value) && !this.gfn_isNull(this.edt_ackey.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_ackey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.cbo_operstatype.value) && !this.gfn_isNull(this.cbo_operstatype.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.cbo_operstatype.setFocus();
        		});
        	}else if(this.gfn_isNull(this.cbo_lo_non_stock_type.value) && !this.gfn_isNull(this.cbo_lo_non_stock_type.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.cbo_lo_non_stock_type.setFocus();
        		});
        	}else if(this.gfn_isNull(this.cbo_foodyn.value) && !this.gfn_isNull(this.cbo_foodyn.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.cbo_foodyn.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						this.fn_Insert();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey" || e.columnid == "ickey"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "owkey"){
        			// 화주 키가 바뀌면 입력화면 초기화
        			if(e.oldvalue != "" &&e.oldvalue != e.newvalue){
        			
        			this.cbo_ichrkey.set_value('');	
        			this.edt_ickey.set_value('');	
        			this.edt_icname.set_value('');	
        			this.cbo_ictype.set_value('');	
        			this.edt_ctgkey.set_value('');	
        			this.edt_ctgname.set_value('');	
        			this.cbo_uom.set_value('');	
        			this.edt_icutkey.set_value('');	
        			this.edt_icutname.set_value('');	
        			this.edt_ackey.set_value('');	
        			this.edt_acname.set_value('');	
        			this.cbo_ichrkey.set_value('');	

        			}
        			
        			if(e.newvalue == "EUGENESF"){
        				this.Static24.set_cssclass("sta_WF_head");	
        			}else{
        				this.Static24.set_cssclass("sta_WF_head_duty");
        			}
        			
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        			this.edt_fn_get_master_impos.set_value(this.gv_Pvalue[2]);
        			
        			if(this.edt_fn_get_master_impos.value=='Y'){
        				this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        				return;
        			}
        			
        				var key = "";
        				var value = "";
        				
        				if(this.gfn_isNotNull(e.newvalue)){
        					this.gv_code = "";
        					this.gfn_codeSearch(key, value, e.newvalue, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        					
        					//상품그룹코드 새로 불러오기
        					this.fn_searchIcgr(e.newvalue);
        				
        				} else {
        					this.ds_icgrkey.clearData();
        				}
        		}
        	}else if(e.columnid == "icutkey"){
        		this.edt_icutname.set_value(this.gv_Pvalue[1]);
        		this.edt_icutname.set_tooltiptext(this.gv_Pvalue[1]);
        		
        		// Search cond, searchId, use Dataset, combo Name, insert value, default Value, callback
        		this.gfn_getCommCodeTran( "icutkey="+e.newvalue+'|'+"owkey="+this.edt_owkey.value
        								, "270"
        								, "ds_uom"
        								, "cbo_uom"
        								, ""
        								, ""
        								);
        	}else if(e.columnid == "ctgkey"){
        		this.edt_ctgname.set_value(this.gv_Pvalue[1]);
        		this.edt_ctgname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "ackey"){
        		this.edt_acname.set_value(this.gv_Pvalue[1]);
        		this.edt_acname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "boxickey"){
        		this.edt_boxicname.set_value(this.gv_Pvalue[1]);
        		this.edt_boxicname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "btlickey"){
        		this.edt_btlicname.set_value(this.gv_Pvalue[1]);
        		this.edt_btlicname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "ordstartdate"){
        		this.fn_calCutValue(""+e.newvalue, "ordstartdate");
        	}else if(e.columnid == "ordstopdate"){
        		this.fn_calCutValue(""+e.newvalue, "ordstopdate");
        	}else if(e.columnid == "regdate"){
        		this.fn_calCutValue(""+e.newvalue, "regdate");
        	}else if(e.columnid == "closingdate"){
        		this.fn_calCutValue(""+e.newvalue, "closingdate");
        	}
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchCtgkey_onclick 실행 */
        this.btn_searchCtgkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { putObj:this.edt_ctgkey
        	            ,owkey:this.edt_owkey.value
        	            ,searchLevel:'3'
        			   };
        	this.gfn_popup("CtgKeySearchPop", "master::CtgKeySearchPop.xfdl", oArg, 700, 497, "");
        }

        /* btn_searchIcutkey_onclick 실행 */
        this.btn_searchIcutkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:100
        				,putObj:this.edt_icutkey
        				,putKey:"owkey"
        				,putValue:this.edt_owkey.value
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchAckey_onclick 실행 */
        this.btn_searchAckey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:40
        				,putObj:this.edt_ackey
        				,putKey:"owkey"
        				,putValue:this.edt_owkey.value
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchBoxickey_onclick 실행 */
        this.btn_searchBoxickey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { putObj:this.edt_boxickey
        			   };
        	this.gfn_popup("CntrKeySearchPop", "master::CntrKeySearchPop.xfdl", oArg, 700, 497, "");
        }

        /* btn_searchBtlickey_onclick 실행 */
        this.btn_searchBtlickey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { putObj:this.edt_btlickey
        			   };
        	this.gfn_popup("CntrKeySearchPop", "master::CntrKeySearchPop.xfdl", oArg, 700, 497, "");
        }

        /* edt_ctgkey_onchanged 실행 */
        this.edt_ctgkey_onchanged = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_ctgkey.value)){
        		this.gv_code = "";
        		this.fn_searchCode();
        	}
        }

        /* edt_icutkey_onchanged 실행 */
        this.edt_icutkey_onchanged = function(obj,e)
        {
        	var key = "owkey";
        	var value = this.edt_owkey.value;
        	
        	if(this.gfn_isNotNull(this.edt_icutkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_icutkey.value, "100", application.gv_ams, "", "", this.edt_icutkey, this.edt_icutname);
        	}
        }

        /* edt_ackey_onchanged 실행 */
        this.edt_ackey_onchanged = function(obj,e)
        {
        	var key = "owkey";
        	var value = this.edt_owkey.value;
        	
        	if(this.gfn_isNotNull(this.edt_ackey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_ackey.value, "40", application.gv_ams, "", "", this.edt_ackey, this.edt_acname);
        	}
        }

        /* edt_boxickey_onchanged 실행 */
        this.edt_boxickey_onchanged = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_boxickey.value)){
        		this.gv_code = "";
        		this.fn_searchCodeBoxic();
        	}
        }

        /* edt_btlickey_onchanged 실행 */
        this.edt_btlickey_onchanged = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_btlickey.value)){
        		this.gv_code = "";
        		this.fn_searchCodeBtlic();
        	}
        }

        
        this.cbo_shelflifeindicator_onitemchanged = function(obj,e)
        {	
        	//this.cbo_shelflifecodetype.set_value('');

        	if(e.postvalue == 'Y') {
        		this.cbo_shelflifecodetype.set_enable(true);
        		
        	}else {
        		this.cbo_shelflifecodetype.set_value('');
        		this.cbo_shelflifecodetype.set_enable(false);
        	}
        		
        }

        /*수주마감시간 시간 체크 루틴*/
        this.mdt_udf1ordend_onchanged = function(obj,e)
        {
        	var nDtChk = this.mdt_udf1ordend.value;
        	if(nDtChk.length > 0 && Number(nDtChk) > 0) {
        		if(nDtChk.substring(0,1) == "0"){
        			if(nDtChk.substring(1,2) == "0")
        				nDtChk = "00"+Number(this.mdt_udf1ordend.value)+"";
        			else
        				nDtChk = "0"+Number(this.mdt_udf1ordend.value)+"";
        		} else 
        		  nDtChk = Number(this.mdt_udf1ordend.value)+"";
        		if( nDtChk.length != 6 || 
        		   ( Number(nDtChk.substring(0,2)) > 23  ||  Number(nDtChk.substring(0,2)) < 0)||
        		   ( Number(nDtChk.substring(2,4)) < 0 || Number(nDtChk.substring(2,4)) > 59)||
        		   ( Number(nDtChk.substring(4,6)) < 0 || Number(nDtChk.substring(4,6)) > 59)){
        		  this.gfn_alert("MSG_CHKDATA_NOTIME", "", function(msg, flag){
        					this.mdt_udf1ordend.setFocus();
        				});
        		  return;
        		 }
        	} else {
        		this.mdt_udf1ordend.value = null;
        	}
        }

        
        this.btn_insert_onclick = function(obj,e)
        {
        	var gv_menuId="ME00000430";
        	var oArg = { argFlag:"H"
        	            ,menuId:gv_menuId
        	           };
        	this.gfn_popup("ItemCod_ItemUnitPop", "item::ItemCod_ItemUnitPop.xfdl", oArg, 580, 342, "");
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_ickey.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_icutkey.addEventHandler("onchanged", this.edt_icutkey_onchanged, this);
            this.btn_searchIcutkey.addEventHandler("onclick", this.btn_searchIcutkey_onclick, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.edt_ctgkey.addEventHandler("onchanged", this.edt_ctgkey_onchanged, this);
            this.btn_searchCtgkey.addEventHandler("onclick", this.btn_searchCtgkey_onclick, this);
            this.edt_ackey.addEventHandler("onchanged", this.edt_ackey_onchanged, this);
            this.btn_searchAckey.addEventHandler("onclick", this.btn_searchAckey_onclick, this);
            this.edt_boxickey.addEventHandler("onchanged", this.edt_boxickey_onchanged, this);
            this.btn_searchBoxickey.addEventHandler("onclick", this.btn_searchBoxickey_onclick, this);
            this.edt_btlickey.addEventHandler("onchanged", this.edt_btlickey_onchanged, this);
            this.btn_searchBtlickey.addEventHandler("onclick", this.btn_searchBtlickey_onclick, this);
            this.cbo_shelflifeindicator.addEventHandler("onitemchanged", this.cbo_shelflifeindicator_onitemchanged, this);
            this.edt_btlicname.addEventHandler("oneditclick", this.edt_btlicname_oneditclick, this);
            this.edt_itemtype.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_chittype.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_insert.addEventHandler("onclick", this.btn_insert_onclick, this);
            this.edt_icname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);

        };

        this.loadIncludeScript("ItemCodeCopyPop.xfdl", true);

       
    };
}
)();
