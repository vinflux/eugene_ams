﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CntrPop");
                this.set_classname("style01");
                this.set_titletext("용기 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,491,423);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"owkey\" type=\"STRING\" size=\"20\"/><Column id=\"owkey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"cntrkey\" type=\"STRING\" size=\"20\"/><Column id=\"cntrname\" type=\"STRING\" size=\"50\"/><Column id=\"ickey\" type=\"STRING\" size=\"50\"/><Column id=\"ickey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"icname\" type=\"STRING\" size=\"256\"/><Column id=\"cntrtype\" type=\"STRING\" size=\"1\"/><Column id=\"cntrtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"cntrprice\" type=\"BIGDECIMAL\" size=\"25\"/><Column id=\"rout_fee\" type=\"BIGDECIMAL\" size=\"9\"/><Column id=\"rin_fee\" type=\"BIGDECIMAL\" size=\"9\"/><Column id=\"regdate\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"14\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"14\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"1\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"cntrkey\"/><Col id=\"cntrname\"/><Col id=\"ickey\"/><Col id=\"cntrtype\"/><Col id=\"cntrprice\"/><Col id=\"rout_fee\"/><Col id=\"rin_fee\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_cntrtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static01", "absolute", "142", "184", "334", "31", null, null, this);
            obj.set_taborder("39");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "140", "244", "334", "31", null, null, this);
            obj.set_taborder("37");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "140", "275", "334", "31", null, null, this);
            obj.set_taborder("38");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "142", "215", "334", "31", null, null, this);
            obj.set_taborder("36");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static50", "absolute", "140", "306", "334", "31", null, null, this);
            obj.set_taborder("34");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("16");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "14", null, this);
            obj.set_taborder("17");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "14", null, this);
            obj.set_taborder("15");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "201", null, "52", "28", null, "15", this);
            obj.set_taborder("13");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "14", null, this);
            obj.set_taborder("18");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "20", "58", "120", "31", null, null, this);
            obj.set_taborder("19");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "294", "31", null, null, this);
            obj.set_taborder("20");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "152", "31", null, null, this);
            obj.set_taborder("21");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "140", "89", "334", "31", null, null, this);
            obj.set_taborder("22");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "20", "89", "120", "31", null, null, this);
            obj.set_taborder("23");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "20", "275", "120", "31", null, null, this);
            obj.set_taborder("24");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "140", "120", "334", "31", null, null, this);
            obj.set_taborder("26");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "20", "120", "120", "31", null, null, this);
            obj.set_taborder("27");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrname", "absolute", "146", "124", "165", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "258", null, "52", "28", null, "15", this);
            obj.set_taborder("14");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrkey", "absolute", "146", "93", "105", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "139", "152", "335", "31", null, null, this);
            obj.set_taborder("28");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "20", "152", "120", "31", null, null, this);
            obj.set_taborder("29");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "20", "213", "120", "31", null, null, this);
            obj.set_taborder("30");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "20", "183", "120", "31", null, null, this);
            obj.set_taborder("31");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "20", "306", "120", "31", null, null, this);
            obj.set_taborder("32");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "254", "93", "57", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "20", "244", "120", "31", null, null, this);
            obj.set_taborder("33");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", null, null, "1", "14", "58", this);
            obj.set_taborder("25");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "146", "62", "121", "22", null, null, this);
            obj.set_taborder("0");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "269", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "295", "62", "175", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_cntrtype", "absolute", "147", "189", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("8");
            obj.set_innerdataset("@ds_cntrtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Calendar("cal_regdate", "absolute", "147", "309", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("12");
            obj.style.set_cursor("hand");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.set_value("null");

            obj = new Edit("edt_icname", "absolute", "295", "156", "175", "22", null, null, this);
            obj.set_taborder("35");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ickey", "absolute", "146", "156", "121", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIckey", "absolute", "269", "156", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_cntrprice", "absolute", "147", "219", "165", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_limitbymask("both");
            obj.set_mask("##############.##");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_rout_fee", "absolute", "147", "249", "165", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_mask("#######.##");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_rin_fee", "absolute", "147", "279", "165", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_mask("#######.##");
            obj.set_limitbymask("both");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 491, 423, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("용기 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","OWNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","CNTRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","Static11","text","gds_lang","RIN_FEE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","CNTRNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","ICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","CNTRPRICE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","CNTRTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","REGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","CNTR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","Static24","text","gds_lang","ROUT_FEE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_cntrname","value","ds_header","cntrname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_cntrkey","value","ds_header","cntrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","edt_ickey","value","ds_header","ickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","cbo_cntrtype","value","ds_header","cntrtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item111","cal_regdate","value","ds_header","regdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_owname","value","ds_header","owkey_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_icname","value","ds_header","icname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","mdt_cntrprice","value","ds_header","cntrprice");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","mdt_rout_fee","value","ds_header","rout_fee");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","mdt_rin_fee","value","ds_header","rin_fee");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CntrPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CntrPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CntrPop.xfdl
        * PROGRAMMER  : kclee
        * DATE        : 2017.04.20
        * DESCRIPTION : 용기 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
         	this.gfn_getCode("CNTRTYPE", this.ds_cntrtype, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_cntrkey  = this.gfn_isNullEmpty(this.parent.cntrkey);

            this.ds_header.setColumn(0, "owkey", this.gfn_getUserInfo("owkeym"));
            //this.ds_header.setColumn(0, "asnuseyn", "N");
            this.ds_header.setColumn(0, "delyn", "N");
            
           // this.gfn_decimalPointSet("mdt_cntrprice|mdt_rin_fee|mdt_rout_fee");

        	this.ds_header.applyChange();
        	

            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.edt_cntrkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.edt_cntrkey.setFocus();
        		this.fn_search();
            } else {	
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "cntrController");
        	this.gfn_setCommon("METHODNM", "selectCntr");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                          + " cntrkey="+this.gv_cntrkey;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "cntrController");
        	this.gfn_setCommon("METHODNM", "checkCntrCount");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "cntrkey="+this.ds_header.getColumn(0, "cntrkey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "cntrController");
        	this.gfn_setCommon("METHODNM", "saveCntr");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "cntrController");
        	this.gfn_setCommon("METHODNM", "saveCntr");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		// 삭제 데이터 변경불가하도록 수정
        		if(this.ds_header.getColumn(this.ds_header.rowposition,"delyn") == "Y"){
        			this.btn_save.set_enable(false);
        		}
        		   
        		var key = "adcd_hdkey";
        		var value = "OWKEY";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        		this.ds_header.applyChange();
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_cntrkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|owkey_desc|cntrkey|cntrname|ickey|cntrtype|cntrprice|rout_fee|rin_fee";
        	var sComp = "edt_owkey|edt_owname|edt_cntrkey|edt_cntrname|edt_ickey|cbo_cntrtype|mdt_cntrprice|mdt_rout_fee|mdt_rin_fee";

        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_cntrkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_icname.value) && !this.gfn_isNull(this.edt_ickey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ickey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "H")	this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        						}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchIckey_onclick 실행 */
        this.btn_searchIckey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:110
        				,putKey:"owkey"
        				,putValue:this.ds_header.getColumn(0, "owkey")
        				,putObj:this.edt_ickey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey" || e.columnid == "ickey"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "owkey"){
        			//코드명과 툴팁에 코드명 설정		
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        		} else {
        			//코드명과 툴팁에 코드명 설정
        			this.edt_icname.set_value(this.gv_Pvalue[1]);
        			this.edt_icname.set_tooltiptext(this.gv_Pvalue[1]);		
        		}
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "cntrkey";
        	var sComp = "edt_cntrkey";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* edt_owkey_onchanged 실행 */
        this.edt_owkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        	}
        }

        /* edt_ickey_onchanged 실행 */
        this.edt_ickey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";

        	if(this.gfn_isNotNull(this.edt_ickey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, e.postvalue, this.edt_ickey.value, "110", application.gv_ams, "", "", this.edt_ickey, this.edt_icname);
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.edt_owkey.addEventHandler("onchanged", this.edt_owkey_onchanged, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.cal_regdate.addEventHandler("onchanged", this.cal_regdate_onchanged, this);
            this.edt_ickey.addEventHandler("onchanged", this.edt_ickey_onchanged, this);
            this.btn_searchIckey.addEventHandler("onclick", this.btn_searchIckey_onclick, this);

        };

        this.loadIncludeScript("CntrPop.xfdl", true);

       
    };
}
)();
