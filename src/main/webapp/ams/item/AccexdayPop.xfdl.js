﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AccexdayPop");
                this.set_classname("style01");
                this.set_titletext("체화예상재고 대/중/소분류 선택 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,700,497);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_search", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grdInit", this);
            obj._setContents("<ColumnInfo><Column id=\"grd_codeInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_codeSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_codeIndex\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"grd_codeIndex\"/><Col id=\"grd_codeSize\"/><Col id=\"grd_codeInit\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("검색");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "55", "656", "44", null, null, this);
            obj.set_taborder("117");
            obj.style.set_border("3 solid #f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_search", "absolute", "33", "66", null, "22", "97", null, this);
            obj.set_taborder("119");
            this.addChild(obj.name, obj);

            obj = new Button("btn_select", "absolute", null, "104", "42", "24", "22", null, this);
            obj.set_taborder("124");
            obj.set_text("선택");
            obj.set_tooltiptext("선택");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_code", "absolute", "22", "133", null, null, "22", "50", this);
            obj.set_taborder("126");
            obj.set_binddataset("ds_search");
            obj.set_cellmovingtype("col,band");
            obj.set_cellsizingtype("col");
            obj.set_autofittype("none");
            obj.set_scrollbars("autovert");
            obj.style.set_padding("0 2 0 2");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"153\"/><Column size=\"153\"/><Column size=\"153\"/><Column size=\"153\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"LEVEL_NM1\"/><Cell col=\"2\" text=\"LEVEL_NM2\"/><Cell col=\"3\" text=\"LEVEL_NM3\"/><Cell col=\"4\" text=\"ICNAME\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:level_nm1\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:level_nm2\"/><Cell col=\"3\" style=\"align:left;padding: ;\" text=\"bind:level_nm3\"/><Cell col=\"4\" style=\"align:left;\" text=\"bind:icname\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Div("div_Paging", "absolute", "22", null, null, "29", "22", "21", this);
            obj.set_taborder("129");
            obj.set_scrollbars("none");
            obj.set_url("comm::PopupPaging.xfdl");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "609", "66", "59", "22", null, null, this);
            obj.set_taborder("130");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_text(" 검색");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 700, 497, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("체화예상재고 대/중/소분류 선택 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_select","text","gds_lang","SELECT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","btn_search","tooltiptext","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","label00","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::PopupPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("AccexdayPop.xfdl", "lib::Comm.xjs");
        this.registerScript("AccexdayPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AccexdayPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 체화예상재고 대/중/소 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main";

        this.gv_Row;
        this.gv_Obj;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 그리드 초기 값 셋팅
        	//this.ds_grdInit.setColumn(0, "grd_codeInit", this.grd_code.getFormatString());
            this.gv_Row  = this.gfn_isNullEmpty(this.parent.putRow);
            this.gv_Obj  = this.gfn_isNullEmpty(this.parent.putObj);
            this.gv_Flag = this.gfn_isNullEmpty(this.parent.putTabFlag);
            	
        	if(this.gv_Flag == 'CTG') {
        		this.grd_code.setFormatColProperty(4, "size", 0);
        		this.grd_code.setFormatColProperty(3, "size", 205);
        		this.grd_code.setFormatColProperty(2, "size", 205);
        		this.grd_code.setFormatColProperty(1, "size", 205);
        	}
        	
        	this.div_Paging.gv_flag = this.gv_main;
        	this.div_Paging.gv_popId = obj;
        	
        	this.gfn_gridHeadSet(this.grd_code);
        	
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
            
        	var divPaging = [this.div_Paging]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역

        	this.gfn_gridInit(this.grd_code, "", divPaging, searchFunc);
        	
        	this.ds_search.clearData();
        	this.ds_param.clearData();
        	
        	this.fn_search();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {	
        	this.gfn_setCommon("BEANID"  , "accexdayController");
        	this.gfn_setCommon("METHODNM", "selectAccexdayPop");
        	
        	var owkey = this.gfn_getUserInfo("owkeym").split(",");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_search=OUT_CTG_LEVEL_LIST ds_param=OUT_PARAM";
            var sParam   = "searchValue="+this.gfn_isNullEmpty(this.edt_search.value)
        				+ " tabType="+this.gv_Flag
        				+ " owkey="+owkey[0];
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        	
        		if(this.ds_search.rowcount > 0){
        			this.ds_search.set_rowposition(0);
        		}else{
        			this.grd_code.set_nodatatext(application.gv_nodataMsg);
        		}
        		this.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* pageSearch 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */
        this.fn_mainSearch = function(currentPage,pagingLimit,count)
        {
        	this.gfn_setParam("currentPage", currentPage);
        	this.gfn_setParam("pagingLimit", pagingLimit);
        	this.gfn_setParam("COUNT", count);
        	
        	this.fn_search();
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_search_onclick 실행 */
        this.btn_search_onclick = function(obj,e)
        {
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	
        	this.fn_search();
        }

        /* btn_select_onclick 실행 */
        this.btn_select_onclick = function(obj,e)
        {

        	this.gv_Obj.setColumn(this.gv_Row, 'level1', this.ds_search.getColumn(this.ds_search.rowposition,'level1'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm1', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm1'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level2', this.ds_search.getColumn(this.ds_search.rowposition,'level2'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm2', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm2'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level3', this.ds_search.getColumn(this.ds_search.rowposition,'level3'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm3', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm3'));
        	
        	if(this.gv_Flag == 'IC') {
        		this.gv_Obj.setColumn(this.gv_Row, 'ickey', this.ds_search.getColumn(this.ds_search.rowposition,'ickey'));
        		this.gv_Obj.setColumn(this.gv_Row, 'icname', this.ds_search.getColumn(this.ds_search.rowposition,'icname'));
        	}
        	
        	this.close();
        }

        /* grd_code_onheadclick 실행 */
        this.grd_code_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "PAGING_NUM" && this.ds_search.rowcount > 0){
        		this.gfn_sortSearchPopup(obj, e.cell, colName, sortValue);
        		this.fn_search();
        	}
        }

        /* btn_closeAll_onclick 실행 */
        this.btn_closeAll_onclick = function(obj,e)
        {
        	this.close();
        }

        /* grd_code_oncelldblclick 실행 */
        this.grd_code_oncelldblclick = function(obj,e)
        {
        	this.gv_Obj.setColumn(this.gv_Row, 'level1', this.ds_search.getColumn(this.ds_search.rowposition,'level1'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm1', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm1'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level2', this.ds_search.getColumn(this.ds_search.rowposition,'level2'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm2', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm2'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level3', this.ds_search.getColumn(this.ds_search.rowposition,'level3'));
        	this.gv_Obj.setColumn(this.gv_Row, 'level_nm3', this.ds_search.getColumn(this.ds_search.rowposition,'level_nm3'));
        	
        	if(this.gv_Flag == 'IC') {
        		this.gv_Obj.setColumn(this.gv_Row, 'ickey', this.ds_search.getColumn(this.ds_search.rowposition,'ickey'));
        		this.gv_Obj.setColumn(this.gv_Row, 'icname', this.ds_search.getColumn(this.ds_search.rowposition,'icname'));
        	}
        	
        	this.close();
        }

        /* edt_search_onkeydown 실행 */
        this.edt_search_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.btn_search.click();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.edt_search.addEventHandler("onkeydown", this.edt_search_onkeydown, this);
            this.btn_select.addEventHandler("onclick", this.btn_select_onclick, this);
            this.grd_code.addEventHandler("onheadclick", this.grd_code_onheadclick, this);
            this.grd_code.addEventHandler("oncelldblclick", this.grd_code_oncelldblclick, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);

        };

        this.loadIncludeScript("AccexdayPop.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
