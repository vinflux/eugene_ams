﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("sampleUser");
                this.set_classname("xampleUser");
                this.set_titletext("가이드 샘플");
                this._setFormPosition(0,0,982,602);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_user", this);
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"USER_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"USER_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMP_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"ENG_EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMAIL_ADDR\" type=\"STRING\" size=\"256\"/><Column id=\"STRT_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"END_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"COMP_CD\" type=\"STRING\" size=\"256\"/><Column id=\"LANG_CD\" type=\"STRING\" size=\"256\"/><Column id=\"TIME_ZONE_CD\" type=\"STRING\" size=\"256\"/><Column id=\"DATE_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"NUM_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"CURR_CD\" type=\"STRING\" size=\"256\"/><Column id=\"CUD\" type=\"STRING\" size=\"256\"/><Column id=\"PASSWORD\" type=\"STRING\" size=\"256\"/><Column id=\"ATTR1\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_userCond", this);
            obj.set_useclientlayout("true");
            obj._setContents("<ColumnInfo><Column id=\"USER_NM\" type=\"String\" size=\"256\"/><Column id=\"EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"ENG_EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMAIL_ADDR\" type=\"STRING\" size=\"256\"/><Column id=\"COMP_CD\" type=\"STRING\" size=\"256\"/><Column id=\"ROLE_CD\" type=\"STRING\" size=\"256\"/><Column id=\"ROLE_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EFFCT_FLAG\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_role", this);
            obj.set_useclientlayout("true");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"USER_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"USER_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMP_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"ENG_EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMAIL_ADDR\" type=\"STRING\" size=\"256\"/><Column id=\"STRT_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"END_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"COMP_CD\" type=\"STRING\" size=\"256\"/><Column id=\"LANG_CD\" type=\"STRING\" size=\"256\"/><Column id=\"TIME_ZONE_CD\" type=\"STRING\" size=\"256\"/><Column id=\"DATE_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"NUM_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"CURR_CD\" type=\"STRING\" size=\"256\"/><Column id=\"duistate\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_userTran", this);
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"USER_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"USER_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMP_ID\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"ENG_EMP_NM\" type=\"STRING\" size=\"256\"/><Column id=\"EMAIL_ADDR\" type=\"STRING\" size=\"256\"/><Column id=\"STRT_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"END_DATE\" type=\"STRING\" size=\"256\"/><Column id=\"COMP_CD\" type=\"STRING\" size=\"256\"/><Column id=\"LANG_CD\" type=\"STRING\" size=\"256\"/><Column id=\"TIME_ZONE_CD\" type=\"STRING\" size=\"256\"/><Column id=\"DATE_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"NUM_FORMAT_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"CURR_CD\" type=\"STRING\" size=\"256\"/><Column id=\"CUD\" type=\"STRING\" size=\"256\"/><Column id=\"PASSWORD\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_file", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_GROUP_ID\" type=\"bigdecimal\" size=\"256\"/><Column id=\"FILE_ID\" type=\"bigdecimal\" size=\"256\"/><Column id=\"MODULE_CODE\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_NAME\" type=\"string\" size=\"256\"/><Column id=\"SERVER_FILE_NM\" type=\"string\" size=\"256\"/><Column id=\"FILE_PATH\" type=\"STRING\" size=\"256\"/><Column id=\"FILE_SIZE\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"CUD\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_excelImport", "absolute", null, "20", "47", "27", "252", null, this);
            obj.set_taborder("1");
            obj.set_text("exIm");
            this.addChild(obj.name, obj);

            obj = new Button("btn_excelExport", "absolute", null, "20", "47", "27", "204", null, this);
            obj.set_taborder("2");
            obj.set_text("exEx");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, "20", "46", "27", "60", null, this);
            obj.set_taborder("3");
            obj.set_text("저장");
            this.addChild(obj.name, obj);

            obj = new Button("btn_del", "absolute", null, "20", "48", "27", "107", null, this);
            obj.set_taborder("4");
            obj.set_text("삭제");
            this.addChild(obj.name, obj);

            obj = new Button("btn_add", "absolute", null, "20", "47", "27", "156", null, this);
            obj.set_taborder("5");
            obj.set_text("추가");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_user", "absolute", "30", "55", null, "270", "60", null, this);
            obj.set_taborder("6");
            obj.set_binddataset("ds_user");
            obj.set_autoenter("select");
            obj.set_selecttype("row");
            obj.set_cellmovingtype("col,band");
            obj.set_cellsizingtype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"20\" band=\"left\"/><Column size=\"30\" band=\"left\"/><Column size=\"80\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"120\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"80\"/><Column size=\"100\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\" style=\"align:center;\"/><Cell col=\"2\" text=\"USER_ID\"/><Cell col=\"3\" text=\"USER_NM\"/><Cell col=\"4\" text=\"EMP_ID\"/><Cell col=\"5\" text=\"EMP_NM\"/><Cell col=\"6\" text=\"ENG_EMP_NM\"/><Cell col=\"7\" text=\"EMAIL_ADDR\"/><Cell col=\"8\" text=\"STRT_DATE\"/><Cell col=\"9\" text=\"END_DATE\"/><Cell col=\"10\" text=\"COMP_CD\"/><Cell col=\"11\" text=\"LANG_CD\"/><Cell col=\"12\" text=\"TIME_ZONE_CD\"/><Cell col=\"13\" text=\"DATE_FORMAT_TYPE\"/><Cell col=\"14\" text=\"NUM_FORMAT_TYPE\"/><Cell col=\"15\" text=\"CURR_CD\"/><Cell col=\"16\" text=\"PASSWORD\"/></Band><Band id=\"body\"><Cell style=\"align:center;backgroundimage:EXPR(CUD==&quot;1&quot;?&quot;img::icon_Insert.png&quot;:(CUD==&quot;2&quot;?&quot;Images::icon_Update.png&quot;:(CUD==&quot;3&quot;?&quot;Images::icon_Delete.png&quot;:&quot;&quot;)));\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"align:center;\" text=\"bind:CHK\"/><Cell col=\"2\" edittype=\"expr:CUD==&quot;1&quot;?&quot;masknumber&quot;:&quot;none&quot;\" text=\"bind:USER_ID\" editlimit=\"22\" editdisplay=\"expr:CUD==&quot;1&quot;?&quot;display&quot;:&quot;&quot;\"/><Cell col=\"3\" edittype=\"text\" text=\"bind:USER_NM\" editlimit=\"100\" editdisplay=\"display\" combodisplay=\"edit\"/><Cell col=\"4\" edittype=\"text\" text=\"bind:EMP_ID\" editlimit=\"22\" editdisplay=\"display\"/><Cell col=\"5\" edittype=\"text\" text=\"bind:EMP_NM\" editlimit=\"100\" editdisplay=\"display\"/><Cell col=\"6\" text=\"bind:ENG_EMP_NM\"/><Cell col=\"7\" text=\"bind:EMAIL_ADDR\"/><Cell col=\"8\" displaytype=\"date\" edittype=\"date\" style=\"align:center;\" text=\"bind:STRT_DATE\" editdisplay=\"display\" calendardisplay=\"display\" calendardisplaynulltype=\"none\"/><Cell col=\"9\" displaytype=\"date\" edittype=\"date\" style=\"align:center;\" text=\"bind:END_DATE\" editdisplay=\"display\" calendardisplay=\"display\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" style=\"align:center;\" text=\"bind:COMP_CD\"/><Cell col=\"11\" style=\"align:center;\" text=\"bind:LANG_CD\"/><Cell col=\"12\" style=\"align:center;\" text=\"bind:TIME_ZONE_CD\"/><Cell col=\"13\" style=\"align:center;\" text=\"bind:DATE_FORMAT_TYPE\"/><Cell col=\"14\" style=\"align:center;\" text=\"bind:NUM_FORMAT_TYPE\"/><Cell col=\"15\" style=\"align:center;\" text=\"bind:CURR_CD\"/><Cell col=\"16\" text=\"bind:PASSWORD\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Div("cal_period", "absolute", "55", "27", "200", "21", null, null, this);
            obj.set_taborder("7");
            obj.set_scrollbars("none");
            obj.set_url("obj_xcommon::PeriodCalendar.xfdl");
            this.addChild(obj.name, obj);

            obj = new Div("div_file", "absolute", "30", "356", null, null, "60", "42", this);
            obj.set_taborder("8");
            obj.set_scrollbars("none");
            obj.set_dragscrolltype("none");
            obj.set_url("obj_xcommon::FileUpDown.xfdl");
            this.addChild(obj.name, obj);

            obj = new Button("btn_popup", "absolute", "277", "22", "51", "26", null, null, this);
            obj.set_taborder("9");
            obj.set_text("popup");
            this.addChild(obj.name, obj);

            obj = new Button("btn_split_in", "absolute", "30", "336", null, "10", "60", null, this);
            obj.set_taborder("10");
            obj.style.set_cursor("auto");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 982, 602, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("xampleUser");
            		p.set_titletext("가이드 샘플");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "obj_xcommon::PeriodCalendar.xfdl");
            this._addPreloadList("fdl", "obj_xcommon::FileUpDown.xfdl");
        };
        
        // User Script
        this.addIncludeScript("sampleUser.xfdl", "lib::Comm.xjs");
        this.registerScript("sampleUser.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : sampleUser.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 샘플 CRUD 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_initForm(obj);
        //	this.gfn_addCondi(this.parent.gv_menuId, "sample::sampleCond");
        	
        	// (선택) Period Calendar 초기화
        	this.fn_initCalendar();
        	
            // Search CODE, searchId, use Dataset, insert Row, combo Name, combo Value
            this.gfn_getCommCodeTran( "CM_SYSTEM|CM_MENU_FLAG|CM_ANSWER|CM_LANG"
        							, "20|50|1200|800"
        							, "ds_systemCd|ds_menuGbCd|ds_useYnCd|ds_langCd"
        							, "||"
        							, "||"
        							, "||"
        							);
        	
        	/**	
        	* fn_initFile 파일 초기 셋팅
        	* @param objForm : 현재 form Object
        	* @param objDsFile : 파일 데이타셋
        	* @param moduleId : Setup, Basic, Fixed, Inven, Arcon
        	* @param oCallback : upload 후 callback함수
        	* @return None
        	* @comment :  
        	**/
        	this.div_file.fn_initFile(obj, this.ds_file, "Setup", this.fn_callbackFile);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_Search = function(aCond)
        {
        	this.gv_userNm = aCond[0];
        	
        	this.ds_user.clearData();
        	this.ds_userCond.clearData();
        	this.ds_userCond.addRow();
        	
        	this.ds_userCond.setColumn(0, "USER_NM", this.gv_userNm);
        	this.ds_userCond.setColumn(0, "EMP_NM", "");
        	this.ds_userCond.setColumn(0, "ENG_EMP_NM", "");
        	this.ds_userCond.setColumn(0, "EMAIL_ADDR", "");
        	this.ds_userCond.setColumn(0, "COMP_CD", "");
        	this.ds_userCond.setColumn(0, "ROLE_CD", "");
        	this.ds_userCond.setColumn(0, "ROLE_NM", "");
        	
        	this.gfn_clearSortAndMark(this.grd_user);
        	
        	var sSvcId = "selectUser";
        	var sSvcUrl = "actID=BRS_CM_RetrieveUserList&baRq=IN_SEARCH&baRs=OUT_USER,OUT_ROLE";
        	var sInData = "IN_SEARCH=ds_userCond";
        	var sOutData = "ds_user=OUT_USER ds_role=OUT_ROLE";
        	var sParam = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Save = function()
        {
        	var sSvcId = "saveUser";
        	var sSvcUrl = "actID=SAMPLE_SaveUserList&baRq=IN_USER&baRs=";
        	var sInData = "IN_USER=ds_user:U";
        	var sOutData = "";
        	var sParam = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		return;
        	}
        	
        	if(sSvcId == "selectUser") { // user 조회
        		if(this.ds_user.rowcount > 0){
        		}else{
        		}
        	}else if(sSvcId == "saveUser"){ // user 저장
        //		application.gv_LeftFrame.form["div_Cond_" + this.parent.gv_menuId].btn_search.click();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* Period Calendar 초기화
        * @return
        * @param
        */
        this.fn_initCalendar = function()
        {
        	this.cal_period.fn_initComp(this, "period_calendar", this.fn_callbackPeriodCalendar, null);
        	var oParam = {};
        	oParam.fromdate = this.gfn_today();
        	oParam.todate   = this.gfn_today();	
        	this.cal_period.fn_setValue(oParam);	
        }

        /**
         * 파일 처리후 callback
         * @return 
         * @example
         * @memberOf this
         */
        this.fn_callbackFile = function(sMsgId,sValue)
        {
        	if(sMsgId == "file_save"){
        		if(sValue){
        			if(this.ds_file.rowcount == 0){
        				this.ds_user.setColumn(this.ds_user.rowposition, "ATTR1", "");
        			}else{
        				if(this.ds_user.getColumn(this.ds_user.rowposition, "ATTR1") == ""){
        					this.ds_user.setColumn(this.ds_user.rowposition, "ATTR1", this.ds_file.getColumn(0, "FILE_GROUP_ID"));
        					this.ds_user.setColumn(this.ds_user.rowposition, "CUD", "2");
        				}
        			}
        			
        			if(!this.gfn_isUpdate(this.ds_user)){ // update 여부 확인
        				this.div_file.fn_fileClear();
        				this.div_file.fn_searchFile(this.ds_user.getColumn(this.ds_user.rowposition, "ATTR1"));
        			}else{
        				this.fn_Save();
        			}
        		}
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* 추가 Button 실행 */
        this.btn_add_onclick = function(obj,e)
        {
        	var nRow = 0;
        	
        	if(this.ds_user.rowposition == this.ds_user.rowcount-1){
        		nRow = this.ds_user.addRow();
        	}else{
        		nRow = this.ds_user.insertRow(this.ds_user.rowposition+1);
        	}
        	
        	this.ds_user.setColumn(nRow, "CUD", "1");
        	this.ds_user.setColumn(nRow, "CHK", "1");
        }

        /* 삭제 Button 실행 */
        this.btn_del_onclick = function(obj,e)
        {
        	var nRowType = "";
        	var nCnt = this.ds_user.rowcount-1;
        	
        	for( ; nCnt >= 0 ; nCnt--){
        		if(this.ds_user.getColumn(nCnt, "CHK")){
        			this.ds_user.deleteRow(nCnt);
        		}
        	}
        }

        /* 저장 Button 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var saveFlag = "";
        	
        	if(this.gfn_isUpdate(this.ds_file)) { // update 여부 확인
        		saveFlag = "F";
        	}else if(this.gfn_isUpdate(this.ds_user)){
        		saveFlag = "U";
        	}else{
        		return;
        	}
        	
        	var ds_col = "USER_ID|USER_NM|EMP_ID|EMP_ID";
        	var grid_col = "USER_ID|USER_NM|EMP_ID|EMP_ID";
        	var bRtn = this.gfn_isDataNullCheck(this.grd_user, this.ds_user, ds_col, grid_col);
        	
        	if(bRtn){
        		return;
        	}
        	
        	this.gfn_confirm("Are you sure you want to save?", "", function(msg, flag){
        		if(flag){
        			if(saveFlag == "F") this.div_file.fn_uploadFile();
        			else if(saveFlag == "U") this.fn_Save();
        		}
        	});
        }

        /* dataSet 변경시 실행 */
        this.ds_user_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid != "CHK" && e.columnid != "CUD" && this.ds_user.getColumn(e.row, "CUD") != "1"){
        		this.ds_user.setColumn(e.row, "CUD", "2");
        	}
        }

        /* excelExport Button 실행 */
        this.btn_excelExport_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.grd_user, "sampleUser");
        }

        /* excelImport Button 실행 */
        this.btn_excelImport_onclick = function(obj,e)
        {
        	this.ds_user.clearData();
        	this.gfn_importExcel(this, "ds_user", "sampleUser");
        }

        /* 그리드 head click 실행 */
        this.grd_user_onheadclick = function(obj,e)
        {
        	if(e.col > 0) this.gfn_gridProc(obj, e);
        }

        /* 그리드 copy 실행 */
        this.grd_user_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_user);
        }

        /* 그리드 grd_user_oncellclick 실행 */
        this.grd_user_oncellclick = function(obj,e)
        {
        	obj.dropdownCalendar(); // 한 번 클릭으로 칼랜더 호출
        }

        /* 그리드 grd_user_onrbuttonup 실행 */
        this.grd_user_onrbuttonup = function(obj,e)
        {
        	this.gfn_openGridMenu(obj, e);
        }

        /* ds_user_onrowposchanged 실행 */
        this.ds_user_onrowposchanged = function(obj,e)
        {
        	this.div_file.fn_fileClear();
        	this.div_file.fn_searchFile(this.ds_user.getColumn(e.newrow, "ATTR1"));
        }

        /* btn_popup_onclick 실행 */
        this.btn_popup_onclick = function(obj,e)
        {
        	this.gfn_popup("popup_test", "sample::samplePopup.xfdl", "", 380, 194, "");
        	
        	// Xplatform 용
        // 	if( hasPopupFrame() ){ //폼에서 띄운 팝업창이 있는지 체크
        // 		var popup = application.popupframes["popup_test"];
        // 		popup.form.test();
        // 	} else { //팝업창이 없을 경우 팝업창을 호출
        // 		this.gfn_popup("popup_test", "sample::samplePopup.xfdl", oArg, 380, 194, "");
        // 	}
        }

        this.sampleUser_ondrag = function(obj,e)
        {
        	if(e.fromreferenceobject == this.btn_split_in){
        		this.gv_split = "in";
        		return true;
        	}
        }

        this.sampleUser_ondragmove = function(obj,e)
        {
        	if(this.gv_split == "in"){
        		this.btn_split_in.set_top(e.clientY);
        		this.grd_user.set_height(this.btn_split_in.top - 66);
        		this.div_file.set_top(this.btn_split_in.top + 20);
        		return true;
        	}else if(this.gv_split == ""){
        		return;
        	}
        }

        this.sampleUser_ondrop = function(obj,e)
        {
        	this.gv_split = "";
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_user.addEventHandler("oncolumnchanged", this.ds_user_oncolumnchanged, this);
            this.ds_user.addEventHandler("onrowposchanged", this.ds_user_onrowposchanged, this);
            this.ds_userTran.addEventHandler("oncolumnchanged", this.ds_user_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("ondrag", this.sampleUser_ondrag, this);
            this.addEventHandler("ondragmove", this.sampleUser_ondragmove, this);
            this.addEventHandler("ondrop", this.sampleUser_ondrop, this);
            this.btn_excelImport.addEventHandler("onclick", this.btn_excelImport_onclick, this);
            this.btn_excelExport.addEventHandler("onclick", this.btn_excelExport_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_del.addEventHandler("onclick", this.btn_del_onclick, this);
            this.btn_add.addEventHandler("onclick", this.btn_add_onclick, this);
            this.grd_user.addEventHandler("onkeydown", this.grd_user_onkeydown, this);
            this.grd_user.addEventHandler("oncellclick", this.grd_user_oncellclick, this);
            this.grd_user.addEventHandler("onheadclick", this.grd_user_onheadclick, this);
            this.grd_user.addEventHandler("onrbuttonup", this.grd_user_onrbuttonup, this);
            this.btn_popup.addEventHandler("onclick", this.btn_popup_onclick, this);

        };

        this.loadIncludeScript("sampleUser.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
