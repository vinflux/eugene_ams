﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("samplePopup");
                this.set_classname("KpiPopup");
                this.set_titletext("popup");
                this._setFormPosition(0,0,352,351);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_inForMain", this);
            obj._setContents("<ColumnInfo><Column id=\"YYYY\" type=\"string\" size=\"256\"/><Column id=\"RESP_JOB_CD\" type=\"string\" size=\"256\"/><Column id=\"RGN_CD\" type=\"string\" size=\"256\"/><Column id=\"SUBSDR_CD\" type=\"string\" size=\"256\"/><Column id=\"DIV_CD\" type=\"string\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_inForAll", this);
            obj._setContents("<ColumnInfo><Column id=\"YYYY\" type=\"string\" size=\"256\"/><Column id=\"KPI_CAT_CD\" type=\"string\" size=\"256\"/><Column id=\"RESP_JOB_CD\" type=\"string\" size=\"256\"/><Column id=\"RGN_CD\" type=\"string\" size=\"256\"/><Column id=\"SUBSDR_CD\" type=\"string\" size=\"256\"/><Column id=\"DIV_CD\" type=\"string\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_MainKPICat", this);
            obj._setContents("<ColumnInfo><Column id=\"KPI_CAT_CD\" type=\"string\" size=\"256\"/><Column id=\"KPI_CAT_NAME\" type=\"string\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_KPICat", this);
            obj._setContents("<ColumnInfo><Column id=\"CK\" type=\"string\" size=\"256\"/><Column id=\"KPI_CAT_CD\" type=\"string\" size=\"256\"/><Column id=\"KPI_CAT_NAME\" type=\"string\" size=\"256\"/><Column id=\"KPI_SUB_CAT_CD\" type=\"string\" size=\"256\"/><Column id=\"KPI_SUB_CAT_NAME\" type=\"string\" size=\"256\"/><Column id=\"KPI_UNIT_CD\" type=\"string\" size=\"256\"/><Column id=\"KPI_UNIT_NAME\" type=\"string\" size=\"256\"/><Column id=\"INPUT_VLD_EXCPT_CD\" type=\"string\" size=\"256\"/><Column id=\"DISP_FORMTT_TYPE_CD\" type=\"string\" size=\"256\"/><Column id=\"KPI_CAT_SORT_ORDER\" type=\"string\" size=\"256\"/><Column id=\"KPI_SUB_CAT_SORT_ORDER\" type=\"string\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_gridHead", this);
            obj._setContents("<ColumnInfo><Column id=\"GRID_CD\" type=\"STRING\" size=\"256\"/><Column id=\"IDX\" type=\"STRING\" size=\"256\"/><Column id=\"DISP_RESRC_CD\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"GRID_CD\">01</Col><Col id=\"IDX\">0</Col></Row><Row><Col id=\"GRID_CD\">01</Col><Col id=\"IDX\">1</Col><Col id=\"DISP_RESRC_CD\">KPI_CD_01</Col></Row><Row><Col id=\"GRID_CD\">01</Col><Col id=\"IDX\">2</Col><Col id=\"DISP_RESRC_CD\">KPI_UNIT_CD_01</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_bg", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("6");
            obj.set_cssclass("popupBg");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_KpiCat", "absolute", "13", "40", null, null, "13", "50", this);
            obj.set_taborder("0");
            obj.set_autofittype("col");
            obj.set_binddataset("ds_KPICat");
            obj.getSetter("dispCd").set("01");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"30\"/><Column size=\"160\"/><Column size=\"110\"/><Column size=\"80\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" colspan=\"2\" text=\"KPI\"/><Cell col=\"3\" text=\"Unit\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CK\"/><Cell col=\"1\" style=\"align:left;\" text=\"bind:KPI_CAT_NAME\"/><Cell col=\"2\" style=\"align:left;\" text=\"bind:KPI_SUB_CAT_NAME\"/><Cell col=\"3\" style=\"align:left;\" text=\"bind:KPI_UNIT_NAME\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ok", "absolute", "28%", null, "70", "24", null, "15", this);
            obj.set_taborder("1");
            obj.set_text("OK");
            this.addChild(obj.name, obj);

            obj = new Button("btn_cancel", "absolute", null, null, "70", "24", "28%", "15", this);
            obj.set_taborder("2");
            obj.set_text("Cancel");
            this.addChild(obj.name, obj);

            obj = new Static("stc_back", "absolute", "0", "0", null, "41", "299", null, this);
            obj.set_taborder("3");
            obj.set_text("KPI");
            obj.style.set_color("black");
            obj.style.set_padding("0 0 0 20");
            obj.getSetter("dispCd").set("");
            this.addChild(obj.name, obj);

            obj = new Combo("cmb_kpi", "absolute", "15.91%", "7", "160", null, null, "319", this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_innerdataset("@ds_MainKPICat");
            obj.set_codecolumn("KPI_CAT_CD");
            obj.set_datacolumn("KPI_CAT_NAME");

            obj = new Button("Button00", "absolute", null, "8", "80", "23", "13", null, this);
            obj.set_taborder("7");
            obj.set_text("Search");
            obj.set_cssclass("btn_WF_search");
            obj.set_positionstep("-1");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 352, 351, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("KpiPopup");
            		p.set_titletext("popup");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("samplePopup.xfdl", "lib::Comm.xjs");
        this.registerScript("samplePopup.xfdl", function(exports) {
        /**********************************************************************************
         * Program ID    : KpiPopup.xfdl
         * Program Name  : KPI Category (Popup)
         * Author        : 
         * Created On    : 2016.05.11
         * Desc          : 
        ***********************************************************************************
         *     수정일      이  름     사유 
        ***********************************************************************************
         *    2016.05.11               최초 작성 
        ***********************************************************************************/

        /*******************************************************************************
            1. 공통 라이브러리 INCLUDE 영역
        ********************************************************************************/ 
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        
        /*******************************************************************************
            2. FORM 변수 선언 영역
        ********************************************************************************/
        this.aaa     = "";
        this.bbb     = "";

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.KpiPopup_onload = function(obj,e)
        {
            this.gfn_initForm(obj);

            if(this.parent.aaa != ""){
                this.aaa     = this.parent.aaa;
                this.bbb     = this.parent.bbb;
            }
        }

        /*******************************************************************************
            5. Transaction Callback 처리부분
        ********************************************************************************/

        /*******************************************************************************
            6. 개발자 FUNCTION 영역
        ********************************************************************************/
        /* Ok 버튼 클릭시 */
        this.btn_ok_onclick = function(obj,e)
        {
            var selectedData = "aaa|bbb|ccc";
            
            this.close(selectedData);
        }

        /* Cancel 버튼 클릭시 */
        this.btn_cancel_onclick = function(obj,e)
        {
            this.close();
        }

        this.test = function()
        {
        	alert("찾아오냐?");
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.KpiPopup_onload, this);
            this.grd_KpiCat.addEventHandler("oncelldblclick", this.grd_KpiCat_oncelldblclick, this);
            this.grd_KpiCat.addEventHandler("oncellclick", this.grd_KpiCat_oncellclick, this);
            this.btn_ok.addEventHandler("onclick", this.btn_ok_onclick, this);
            this.btn_cancel.addEventHandler("onclick", this.btn_cancel_onclick, this);
            this.Button00.addEventHandler("onclick", this.btn_search_onclick, this);

        };

        this.loadIncludeScript("samplePopup.xfdl", true);

       
    };
}
)();
