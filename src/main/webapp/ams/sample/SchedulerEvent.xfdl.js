﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SchedulerEvent");
                this.set_classname("SchedulerEvent");
                this.set_titletext("Add Events to the Scheduler");
                this._setFormPosition(0,0,503,439);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_hours", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"displayVal\" type=\"STRING\" size=\"256\"/><Column id=\"hour\" type=\"STRING\" size=\"256\"/><Column id=\"min\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"displayVal\">12:00 AM</Col><Col id=\"hour\">0000</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">12:30 AM</Col><Col id=\"hour\">0030</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">01:00 AM</Col><Col id=\"hour\">0100</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">01:30 AM</Col><Col id=\"hour\">0130</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">02:00 AM</Col><Col id=\"hour\">0200</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">02:30 AM</Col><Col id=\"hour\">0230</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">03:00 AM</Col><Col id=\"hour\">0300</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">03:30 AM</Col><Col id=\"hour\">0330</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">04:00 AM</Col><Col id=\"hour\">0400</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">04:30 AM</Col><Col id=\"hour\">0430</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">05:00 AM</Col><Col id=\"hour\">0500</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">05:30 AM</Col><Col id=\"hour\">0530</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">06:00 AM</Col><Col id=\"hour\">0600</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">06:30 AM</Col><Col id=\"hour\">0630</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">07:00 AM</Col><Col id=\"hour\">0700</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">07:30 AM</Col><Col id=\"hour\">0730</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">08:00 AM</Col><Col id=\"hour\">0800</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">08:30 AM</Col><Col id=\"hour\">0830</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">09:00 AM</Col><Col id=\"hour\">0900</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">09:30 AM</Col><Col id=\"hour\">0930</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">10:00 AM</Col><Col id=\"hour\">1000</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">10:30 AM</Col><Col id=\"hour\">1030</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">11:00 AM</Col><Col id=\"hour\">1100</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">11:30 AM</Col><Col id=\"hour\">1130</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">12:00 PM</Col><Col id=\"hour\">1200</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">12:30 PM</Col><Col id=\"hour\">1230</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">01:00 PM</Col><Col id=\"hour\">1300</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">01:30 PM</Col><Col id=\"hour\">1330</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">02:00 PM</Col><Col id=\"hour\">1400</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">02:30 PM</Col><Col id=\"hour\">1430</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">03:00 PM</Col><Col id=\"hour\">1500</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">03:30 PM</Col><Col id=\"hour\">1530</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">04:00 PM</Col><Col id=\"hour\">1600</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">04:30 PM</Col><Col id=\"hour\">1630</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">05:00 PM</Col><Col id=\"hour\">1700</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">05:30 PM</Col><Col id=\"hour\">1730</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">06:00 PM</Col><Col id=\"hour\">1800</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">06:30 PM</Col><Col id=\"hour\">1830</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">07:00 PM</Col><Col id=\"hour\">1900</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">07:30 PM</Col><Col id=\"hour\">1930</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">08:00 PM</Col><Col id=\"hour\">2000</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">08:30 PM</Col><Col id=\"hour\">2030</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">09:00 PM</Col><Col id=\"hour\">2100</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">09:30 PM</Col><Col id=\"hour\">2130</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">10:00 PM</Col><Col id=\"hour\">2200</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">10:30 PM</Col><Col id=\"hour\">2230</Col><Col id=\"min\">30</Col></Row><Row><Col id=\"displayVal\">11:00 PM</Col><Col id=\"hour\">2300</Col><Col id=\"min\">00</Col></Row><Row><Col id=\"displayVal\">11:30 PM</Col><Col id=\"hour\">2330</Col><Col id=\"min\">30</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "14", "19", "49", "33", null, null, this);
            obj.set_taborder("0");
            obj.set_text("Title");
            obj.style.set_font("antialias 11 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Edit("Ed_title", "absolute", "60", "23", "432", "28", null, null, this);
            obj.set_taborder("1");
            obj.style.set_font("antialias 10 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "15", "64", "50", "33", null, null, this);
            obj.set_taborder("2");
            obj.set_text("Start");
            obj.style.set_font("antialias 11 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Combo("Cmb_start", "absolute", "60", "68", "234", "27", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_hours");
            obj.set_datacolumn("displayVal");
            obj.set_codecolumn("hour");

            obj = new Calendar("Cal_start", "absolute", "60", "68", "234", "27", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_dateformat("yyyy-MM-dd HH:mm ddd");
            obj.set_value("20151229125900000");
            obj.set_editformat("yyyy-MM-dd HH:mm");
            obj.style.set_padding("0 30 0 0");
            obj.style.set_font("antialias 10 malgun gothic, Verdana");

            obj = new Static("st_start", "absolute", "269", "74", "19", "17", null, null, this);
            obj.set_taborder("5");
            obj.style.set_background("transparent URL('Images::clock.png')");
            this.addChild(obj.name, obj);

            obj = new Combo("Cmb_end", "absolute", "60", "112", "234", "27", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("6");
            obj.set_innerdataset("@ds_hours");
            obj.set_codecolumn("hour");
            obj.set_datacolumn("displayVal");

            obj = new Calendar("Cal_end", "absolute", "60", "112", "234", "27", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_value("20151229000000000");
            obj.set_dateformat("yyyy-MM-dd HH:mm ddd");
            obj.set_editformat("yyyy-MM-dd HH:mm");
            obj.style.set_padding("0 30 0 0");
            obj.style.set_font("antialias 10 malgun gothic, Verdana");

            obj = new Static("st_end", "absolute", "270", "117", "19", "17", null, null, this);
            obj.set_taborder("8");
            obj.style.set_background("transparent URL('Images::clock.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "17", "110", "49", "33", null, null, this);
            obj.set_taborder("9");
            obj.set_text("End");
            obj.style.set_font("antialias 11 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "15", "150", "102", "33", null, null, this);
            obj.set_taborder("10");
            obj.set_text("Description");
            obj.style.set_font("antialias 11 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "16", "183", "476", "191", null, null, this);
            obj.set_taborder("11");
            obj.style.set_font("antialias 10 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "138", "385", "100", "38", null, null, this);
            obj.set_taborder("12");
            obj.set_text("Save");
            obj.style.set_font("antialias bold 11 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "262", "385", "100", "38", null, null, this);
            obj.set_taborder("13");
            obj.set_text("Cancel");
            obj.style.set_font("antialias bold 11 malgun gothic, Verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 503, 439, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("SchedulerEvent");
            		p.set_titletext("Add Events to the Scheduler");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("SchedulerEvent.xfdl", function(exports) {
        /*
         * SchedulerEvent at nexacro® software.
         *
         * Here is Modal Dialog Form called when modify and add event in Scheduler Sample(Scheduler.xfdl).
         *
         * Copyright (c) 2015 EcoSystem of nexacro.
         * Licensed Free under nexacro® software.
        */

        var jobGbn, evtItem;

        /**
         * when form has been loaded, fire event handler.
        **/
        this.SchedulerEvent_onload = function(obj,e)
        {
        	var arg = Eco.XComp.getPopupArguments(this,["jobGbn", "selectEvent", "timeRange"]);
        	jobGbn = arg.jobGbn;
        	if ( jobGbn == "add" )
        	{
        		var timeRange = arg.timeRange;
        		if ( timeRange && timeRange.length )
        		{
        			this.Cal_start.set_value(Eco.date.getMaskFormatString(timeRange[0], "yyyyMMddHHmm"));
        			this.Cal_end.set_value(Eco.date.getMaskFormatString(timeRange[1], "yyyyMMddHHmm"));
        		}
        		else
        		{
        			var curdt = new Date();
        			this.Cal_start.set_value(Eco.date.getMaskFormatString(curdt, "yyyyMMddHHmm"));
        			this.Cal_end.set_value(Eco.date.getMaskFormatString(curdt, "yyyyMMddHHmm"));
        		}
        		this.set_titletext("Add Events to the Scheduler");
        	}
        	else
        	{
        		evtItem = arg.selectEvent;
        		if ( evtItem )
        		{
        			this.Ed_title.set_value(evtItem.summary);
        			this.TextArea00.set_value(evtItem.description);
        			this.Cal_start.set_value(Eco.date.getMaskFormatString(evtItem.startdate, "yyyyMMddHHmm"));
        			this.Cal_end.set_value(Eco.date.getMaskFormatString(evtItem.enddate, "yyyyMMddHHmm"));
        		}
        		this.set_titletext("Modify Events to the Scheduler");
        	}
        }

        
        /**
         * date and time picker is composed of several components.
         *  → calendar, static(be with background image as a time feature), combo
         * combo component acts as time picker.
         * calendar component acts as date picker.
        **/

        /**
         * itemchange event handler in combo
         * set part of time in value(datetime) of calendar component. 
        **/
        this.Combo00_canitemchange = function(obj,e)
        {
        	var pos = Eco.string.lastIndexOfIgnoreCase(obj.name, "_");
        	var kind = obj.name.substr(pos + 1);
        	var	calobj = this["Cal_" + kind],
        		dt = calobj.value ? calobj.value.date : null;
        	if ( dt )
        	{
        		//var datestr = Eco.date.getMaskFormatString(dt, "yyyyMMdd") + ds.getColumn(row, "hour");// + ds.getColumn(row, "min");
        		var datestr = Eco.date.getMaskFormatString(dt, "yyyyMMdd") + e.postvalue;// + ds.getColumn(row, "min");
        		calobj.set_value(datestr);
        		calobj._editText = null;
        	}
        }

        /**
         * click event handler in static(with time icon)
         * get current time in value(datetime) of calendar component.
         * select matched value in combo' popup listbox.
         * popup combo's listbox.
        **/
        this.st_start_hour_onclick = function(obj,e)
        {
        	var pos = Eco.string.lastIndexOfIgnoreCase(obj.name, "_");
        	var kind = obj.name.substr(pos + 1);
        	var	calobj = this["Cal_" + kind],
        		dt;

        	var combo = this["Cmb_" + kind];
        	if ( calobj._editText )
        	{
        		var keystr = calobj._editText.substr(0, 2) + calobj._editText.substr(3, 2);
        		if ( keystr.length == 4 )
        		{
        			var ds = this.ds_hours;
        			var checkstr = keystr.substr(2), idx;
        			if ( checkstr == "00" || checkstr == "30" )
        			{
        				idx = ds.findRow("hour", keystr);
        			}
        			else if ( checkstr > "30" )
        			{
        				idx = ds.findRow("hour", keystr.substr(0, 2) + "30");
        			}
        			else
        			{
        				idx = ds.findRow("hour", keystr.substr(0, 2) + "00");
        			}
        			combo.set_index(idx);
        		}
        		calobj._editText = null;
        	}
        	else
        	{
        		dt = calobj.value ? calobj.value.date : null;
        		if ( dt )
        		{
        			//var datestr = Eco.date.getMaskFormatString(dt, "yyyyMMdd") + ds.getColumn(row, "hour");// + ds.getColumn(row, "min");
        			var keystr = Eco.date.getMaskFormatString(dt, "HHmm");// + ds.getColumn(row, "min");
        			var ds = this.ds_hours;
        			var checkstr = keystr.substr(2), idx;
        			if ( checkstr == "00" || checkstr == "30" )
        			{
        				idx = ds.findRow("hour", keystr);
        			}
        			else if ( checkstr > "30" )
        			{
        				idx = ds.findRow("hour", keystr.substr(0, 2) + "30");
        			}
        			else
        			{
        				idx = ds.findRow("hour", keystr.substr(0, 2) + "00");
        			}
        			combo.set_index(idx);
        		}
        	}

        	combo.dropdown();
        }

        /**
         * click event handler in static(with time icon)
         * get current time in value(datetime) of calendar component.
         * select matched value in combo' popup listbox.
         * popup combo's listbox.
        **/
        this.Calendar00_oncloseup = function(obj,e)
        {
        	var prevdt = e.prevalue ? e.prevalue.date : null;
        	if ( prevdt )
        	{
        		var postdt = e.postvalue ? e.postvalue.date : null;
        		if ( postdt && (postdt.getHours() != prevdt.getHours() || postdt.getMinutes() != prevdt.getMinutes()) )
        		{
        			//var datestr = Eco.date.getMaskFormatString(postdt, "yyyyMMdd") + Eco.date.getMaskFormatString(prevdt, "HHmm");
        			var dt = new nexacro.Date();
        			dt.date = new Date(postdt.getTime());
        			dt.date.setHours(prevdt.getHours(), prevdt.getMinutes());
        			obj._setValue(dt);
        			//obj.value.date.setHours(prevdt.getHours(), prevdt.getMinutes());
        		}
        	}
        	obj._editText = null;
        }

        /**
         * ontextchanged handler in calendar.
         * The event ocurred after text be typed in calendar component.
        **/
        this.Calendar00_ontextchanged = function(obj,e)
        {
        	obj._editText = (e.posttext ? e.posttext.substr(11) : null);
        }

        /**
         * converting a Date to a nexacro.Date value(given argument of function).
         * @param {nexacro.Date} xpDate nexacro.Date.
         * @return {Date} value converted to Date.
        **/
        this.xpDateToDate = function(xpDate)
        {
        	if ( xpDate.date )
        		return new Date(xpDate.date.getTime());
        }

        /**
         * when "Save" button click, fire event handler.
         * return a object(res) to a parent form by calling the setRetVal function in parent form. 
         * I close this form by calling the close function
        **/
        this.Button00_onclick = function(obj,e)
        {
        	var pForm = this.parent.parent.form;

        	var res = {
        		returnVal: "OK",
        	};

        	if ( jobGbn == "add" )
        	{
        		res.startdate = this.xpDateToDate(this.Cal_start.value);
        		res.enddate = this.xpDateToDate(this.Cal_end.value);
        		res.summary = this.Ed_title.value;
        		res.description = this.TextArea00.value;
        	}
        	else
        	{
        		res.key = evtItem.key;
        		res.startdate = this.xpDateToDate(this.Cal_start.value);
        		res.enddate = this.xpDateToDate(this.Cal_end.value);
        		res.summary = this.Ed_title.value;
        		res.description = this.TextArea00.value;
        	}
        	pForm.setRetVal(res);
        	this.close();
        }

        /**
         * when "Cancel" button click, fire event handler.
         * I only close this form by calling the close function
        **/
        this.Button01_onclick = function(obj,e)
        {
        	this.close();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.SchedulerEvent_onload, this);
            this.Cmb_start.addEventHandler("canitemchange", this.Combo00_canitemchange, this);
            this.Cal_start.addEventHandler("oncloseup", this.Calendar00_oncloseup, this);
            this.Cal_start.addEventHandler("ontextchanged", this.Calendar00_ontextchanged, this);
            this.st_start.addEventHandler("onclick", this.st_start_hour_onclick, this);
            this.Cmb_end.addEventHandler("canitemchange", this.Combo00_canitemchange, this);
            this.Cal_end.addEventHandler("oncloseup", this.Calendar00_oncloseup, this);
            this.Cal_end.addEventHandler("ontextchanged", this.Calendar00_ontextchanged, this);
            this.st_end.addEventHandler("onclick", this.st_start_hour_onclick, this);
            this.Button00.addEventHandler("onclick", this.Button00_onclick, this);
            this.Button01.addEventHandler("onclick", this.Button01_onclick, this);

        };

        this.loadIncludeScript("SchedulerEvent.xfdl", true);

       
    };
}
)();
