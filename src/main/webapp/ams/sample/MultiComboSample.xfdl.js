﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MultiCombo");
                this.set_classname("sample016");
                this.set_titletext("멀티콤보샘플");
                this._setFormPosition(0,0,744,600);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_inner00", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CODE\" type=\"STRING\" size=\"256\"/><Column id=\"NAME\" type=\"STRING\" size=\"256\"/><Column id=\"COL1\" type=\"STRING\" size=\"256\"/><Column id=\"COL2\" type=\"STRING\" size=\"256\"/><Column id=\"COL3\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"CODE\">1</Col><Col id=\"NAME\">aaaa</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">2</Col><Col id=\"NAME\">bbbb</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">3</Col><Col id=\"NAME\">cccc</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">4</Col><Col id=\"NAME\">dddd</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">5</Col><Col id=\"NAME\">eeee</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_inner11", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CODE\" type=\"STRING\" size=\"256\"/><Column id=\"NAME\" type=\"STRING\" size=\"256\"/><Column id=\"COL1\" type=\"STRING\" size=\"256\"/><Column id=\"COL2\" type=\"STRING\" size=\"256\"/><Column id=\"COL3\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"CODE\">1</Col><Col id=\"NAME\">aaaa</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">2</Col><Col id=\"NAME\">bbbb</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">3</Col><Col id=\"NAME\">cccc</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">4</Col><Col id=\"NAME\">dddd</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">5</Col><Col id=\"NAME\">eeee</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">1</Col><Col id=\"NAME\">aaaa</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">2</Col><Col id=\"NAME\">bbbb</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">3</Col><Col id=\"NAME\">cccc</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">4</Col><Col id=\"NAME\">dddd</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">5</Col><Col id=\"NAME\">eeee</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">1</Col><Col id=\"NAME\">aaaa</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">2</Col><Col id=\"NAME\">bbbb</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">3</Col><Col id=\"NAME\">cccc</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">4</Col><Col id=\"NAME\">dddd</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row><Row><Col id=\"CODE\">5</Col><Col id=\"NAME\">eeee</Col><Col id=\"COL1\">A1</Col><Col id=\"COL2\">B1</Col><Col id=\"COL3\">C1</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", "744", "53", null, null, this);
            obj.set_taborder("21");
            obj.set_text("Multi Combo");
            obj.set_cssclass("Guide_header");
            this.addChild(obj.name, obj);

            obj = new Static("Static48", "absolute", "0", "52", "135", "548", null, null, this);
            obj.set_taborder("22");
            obj.set_text("Multi Select");
            obj.style.set_background("#f8f8f8ff");
            obj.style.set_border("1 solid #767676ff");
            obj.style.set_color("#333333ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 Gulim");
            this.addChild(obj.name, obj);

            obj = new Static("Static47", "absolute", "134", "52", "610", "548", null, null, this);
            obj.set_taborder("23");
            obj.style.set_border("1 solid #767676ff");
            obj.style.set_color("#2c4898ff");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_set", "absolute", "269", "63", "72", "25", null, null, this);
            obj.set_taborder("13");
            obj.set_text("SET");
            this.addChild(obj.name, obj);

            obj = new Button("btn_get", "absolute", "351", "63", "72", "25", null, null, this);
            obj.set_taborder("14");
            obj.set_text("GET");
            this.addChild(obj.name, obj);

            obj = new Button("btn_init", "absolute", "445", "63", "72", "25", null, null, this);
            obj.set_taborder("15");
            obj.set_text("초기화");
            this.addChild(obj.name, obj);

            obj = new Button("btn_validation", "absolute", "527", "63", "72", "25", null, null, this);
            obj.set_taborder("17");
            obj.set_text("유효성체크");
            this.addChild(obj.name, obj);

            obj = new Div("div_multiCombo00", "absolute", "269", "130", "154", "21", null, null, this);
            obj.set_taborder("11");
            obj.set_url("obj_xcommon::MultiCombo.xfdl");
            this.addChild(obj.name, obj);

            obj = new Div("div_multiCombo11", "absolute", "445", "130", "154", "21", null, null, this);
            obj.set_taborder("12");
            obj.set_url("obj_xcommon::MultiCombo.xfdl");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "270", "109", "152", "21", null, null, this);
            obj.set_taborder("24");
            obj.set_text("div_multiCombo00");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "446", "109", "152", "21", null, null, this);
            obj.set_taborder("25");
            obj.set_text("div_multiCombo11");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 744, 600, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("sample016");
            		p.set_titletext("멀티콤보샘플");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "obj_xcommon::MultiCombo.xfdl");
            this._addPreloadList("fdl", "obj_xcommon::MultiCombo.xfdl");
        };
        
        // User Script
        this.addIncludeScript("MultiComboSample.xfdl", "lib::Comm.xjs");
        this.registerScript("MultiComboSample.xfdl", function(exports) {
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        //Form onload event
        this.frm_onload = function(obj,e)
        {
            //멀티선택콤보 초기화
            this.fn_initMultiCombo();
        }

        /*************************************************************/
        //멀티콤보 초기설정
        /*************************************************************/
        this.fn_initMultiCombo = function()
        {
            /**
             * @class 멀티콤보 초기설정
             * @method fn_initComp
             * @param oForm{Object} Form Object
             * @param sPopId{String} Callback ID
             * @param oCallback{Object} Callback Function
             * @param oParam{Object} 멀티콤보 설정값
                        innerdataset    - 콤보의 innerdataset으로 사용할 데이타셋(필수)
                        codecolumn        - 콤보의 코드값으로 사용할 컬럼명(필수)
                        datacolumn        - 콤보의 데이타컬럼값으로 사용할 컬럼명(필수)
                        etccolumn        - 추가로 보여줄 컬럼명 ex) "컬럼명1,컬럼명2"
                        checkcolumn        - 체크박스컬럼명(기본값 : CHK)
                        listboxwidth    - 콤보리스트박스의 넓이값(기본값 : 콤보의 width값)
                        displayrowcount - 콤보 리스트박스에 보여줄 건수(기본값 : 10)
                        rowsize            - 콤보 리스트박스의 한로우의 높이값 (기본값 : 24)
                        viewcolumn        - 콤보리스트 선택시 보여줄 값의 컬럼명(기본값 : datacolumn)
                        usecheck        - 체크박스 사용여부
                        delimiter         - 구분자값 (기본값 : ',')    
                        selecttype        - A : ALL, S : SELECT, N : ""
                        initdata        - 초기설정값
             * @return none
             */      
            var oParam = {innerdataset:this.ds_inner00, selecttype:"A"};
            this.div_multiCombo00.fn_initComp(this, "div_multiCombo00", this.fn_callbackMultiCombo, oParam);
            
            var oParam2 = {innerdataset:this.ds_inner11, selecttype:"S", codecolumn:"CODE", datacolumn:"NAME", etccolumn:"COL1,COL2", checkcolumn:"CHK", listboxwidth:250, displayrowcount:10, viewcolumn:"COL1"};
            this.div_multiCombo11.fn_initComp(this, "div_multiCombo11", this.fn_callbackMultiCombo, oParam2);
        }

        // 멀티콤보 콜백함수
        this.fn_callbackMultiCombo = function(sPopId,sValue)
        {
            if(sPopId == "div_multiCombo00")
            {
                trace("Callback : div_multiCombo00~!!!");
            }
            else if(sPopId == "div_multiCombo11")
            {
                trace("Callback : div_multiCombo11~!!!");
            }
        }

        // 멀티콤보 값설정
        this.btn_set_onclick = function(obj,e)
        {
            this.div_multiCombo00.fn_setValue("2,3");    // 기본값은 datacolumn
            this.div_multiCombo11.fn_setValue("4,5");    // 기본값은 datacolumn
        }

        // 멀티콤보 값 얻기
        this.btn_get_onclick = function(obj,e)
        {
            // "CHK"칼럼이 체크된 codecolumn값
            this.alert("div_multiCombo00 : checked codecolumn value = "+this.div_multiCombo00.fn_getValue());
            this.alert("div_multiCombo11 : checked codecolumn value = "+this.div_multiCombo11.fn_getValue());

            // "CHK"칼럼이 체크된 codecolumn값 - Quote 문자열 (IN조건시 사용)
            this.alert("div_multiCombo00 : checked codecolumn(Quote) value = "+this.div_multiCombo00.fn_getValueQuote());
            this.alert("div_multiCombo11 : checked codecolumn(Quote) value = "+this.div_multiCombo11.fn_getValueQuote());
            
            // "CHK"칼럼이 체크된 codecolumn값 이외 경우
            this.alert("div_multiCombo00 : checked etc column value = "+this.div_multiCombo00.fn_getValue("NAME")+" / "+this.div_multiCombo00.fn_getValue("COL1")+" / "+this.div_multiCombo00.fn_getValue("COL2")+" / "+this.div_multiCombo00.fn_getValue("COL3"));    
            this.alert("div_multiCombo11 : checked etc column value = "+this.div_multiCombo11.fn_getValue("NAME")+" / "+this.div_multiCombo11.fn_getValue("COL1")+" / "+this.div_multiCombo11.fn_getValue("COL2")+" / "+this.div_multiCombo11.fn_getValue("COL3"));    
            
            
            // "CHK"칼럼이 체크된 codecolumn값 이외 경우 - Quote 문자열 (IN조건시 사용)
            this.alert("div_multiCombo00 : checked etc column(Quote) value = "+this.div_multiCombo00.fn_getValueQuote("NAME")+" / "+this.div_multiCombo00.fn_getValueQuote("COL1")+" / "+this.div_multiCombo00.fn_getValueQuote("COL2")+" / "+this.div_multiCombo00.fn_getValueQuote("COL3"));
            this.alert("div_multiCombo11 : checked etc column(Quote) value = "+this.div_multiCombo11.fn_getValueQuote("NAME")+" / "+this.div_multiCombo11.fn_getValueQuote("COL1")+" / "+this.div_multiCombo11.fn_getValueQuote("COL2")+" / "+this.div_multiCombo11.fn_getValueQuote("COL3"));
        }

        // 멀티콤보 초기화
        this.btn_init_onclick = function(obj,e)
        {
            this.div_multiCombo00.fn_initValue();
            this.div_multiCombo11.fn_initValue();
        }

        // "CHK"칼럼이 체크된 Row값의 유효성체크
        this.btn_validation_onclick = function(obj,e)
        {
            var rtn00 = this.div_multiCombo00.fn_checkValidation();
            trace("rtn00 = " + rtn00);
            var rtn11 = this.div_multiCombo11.fn_checkValidation();
            trace("rtn11 = " + rtn11);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.frm_onload, this);
            this.btn_set.addEventHandler("onclick", this.btn_set_onclick, this);
            this.btn_get.addEventHandler("onclick", this.btn_get_onclick, this);
            this.btn_init.addEventHandler("onclick", this.btn_init_onclick, this);
            this.btn_validation.addEventHandler("onclick", this.btn_validation_onclick, this);

        };

        this.loadIncludeScript("MultiComboSample.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
