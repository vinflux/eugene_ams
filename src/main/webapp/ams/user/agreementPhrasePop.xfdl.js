﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("agreementPhrasePop");
                this.set_classname("notice");
                this.set_titletext("약관 등록");
                this._setFormPosition(0,0,801,772);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"usegb\" type=\"STRING\" size=\"256\"/><Column id=\"agreegb\" type=\"STRING\" size=\"256\"/><Column id=\"startdt\" type=\"STRING\" size=\"256\"/><Column id=\"content\" type=\"STRING\" size=\"999999\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_agreegb", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_usegb", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_apkey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static08", "absolute", "160", "119", null, "609", "16", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "160", "54", null, "31", "21", null, this);
            obj.set_taborder("9");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("10");
            obj.set_text("약관등록");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "21", "40", null, "2", "23", null, this);
            obj.set_taborder("11");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "394", "8", this);
            obj.set_taborder("5");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "20", "52", null, "1", "24", null, this);
            obj.set_taborder("12");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "337", "8", this);
            obj.set_taborder("6");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "22", "54", "138", "31", null, null, this);
            obj.set_taborder("13");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static07", "absolute", "22", "119", "138", "609", null, null, this);
            obj.set_taborder("14");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new TextArea("edt_content", "absolute", "165", "119", null, "601", "21", null, this);
            obj.set_taborder("4");
            obj.set_maxlength("999999");
            obj.set_enable("true");
            obj.set_acceptstab("true");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "160", "86", null, "31", "21", null, this);
            obj.set_taborder("19");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static02", "absolute", "22", "86", "138", "31", null, null, this);
            obj.set_taborder("20");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static03", "absolute", "406", "54", "138", "31", null, null, this);
            obj.set_taborder("21");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static09", "absolute", "406", "86", "138", "31", null, null, this);
            obj.set_taborder("22");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_apkey", "absolute", "165", "58", "125", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("0");
            obj.set_innerdataset("@ds_apkey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("true");

            obj = new Combo("cbo_useGb", "absolute", "165", "89", "125", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_innerdataset("@ds_usegb");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("true");

            obj = new Combo("cbo_agreeGb", "absolute", "547", "88", "124", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_agreegb");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("true");

            obj = new Calendar("cal_start_dt", "absolute", "547", "58", "124", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("1");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_enable("true");
            obj.style.set_cursor("hand");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 801, 772, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("약관 등록");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","ME00112681_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","ME00112681_02");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static07","text","gds_lang","ME00112681_06");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_content","value","ds_header","content");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","static02","text","gds_lang","ME00112681_04");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","static03","text","gds_lang","ME00112681_03");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static09","text","gds_lang","ME00112681_05");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","cbo_apkey","value","ds_header","apkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","cbo_useGb","value","ds_header","usegb");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cbo_agreeGb","value","ds_header","agreegb");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","cal_start_dt","value","ds_header","startdt");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("agreementPhrasePop.xfdl", "lib::Comm.xjs");
        this.registerScript("agreementPhrasePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : boardPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 게시판 팝업 
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/
         
        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.ds_header.addRow();

        	this.gfn_getCommCodeTran( ""
        					, "11"
        					, "ds_apkey"
        					, ""
        					, ""
        					, ""
        					);
        	this.gfn_getCode("PTL_AGREE_USE_GB", this.ds_usegb, "");
        	this.gfn_getCode("PTL_AGREE_SIGN_GB", this.ds_agreegb, "");
                   
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            if(this.gv_flag == "U"){
            	this.cbo_apkey.set_enable(false);
        		this.cal_start_dt.set_enable(false);
        		this.cbo_useGb.set_enable(false);
        		this.cbo_agreeGb.set_enable(false);
        		this.ds_header.setColumn(0,"apkey",this.parent.apkey);
        		this.ds_header.setColumn(0,"usegb",this.parent.usegb);
        		this.ds_header.setColumn(0,"agreegb",this.parent.agreegb);
        		this.ds_header.setColumn(0,"startdt",this.parent.startdt);
        		this.ds_header.setColumn(0,"content",this.parent.content);
            }else{
               	this.cbo_apkey.set_enable(true);
        		this.cal_start_dt.set_enable(true);
        		this.cbo_useGb.set_enable(true);
        		this.cbo_agreeGb.set_enable(true);
        	}
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        
        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "apController");
        	this.gfn_setCommon("METHODNM", "save");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_SAVE_DATA=ds_header";
            var sOutData = "";
            var sParam   = "";
            if(this.gv_flag == "U"){
        		sParam   = "workType=UPDATE";
            }else{
        		sParam   = "workType=INSERT";
            }
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {	
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "apkey|usegb|agreegb|startdt";
        	var sComp = "cbo_apkey|cbo_useGb|cbo_agreeGb|cal_start_dt";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_Insert();
        				}	else  {
        						trace(this.ds_header.saveXML());
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        	
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.cbo_apkey.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cbo_useGb.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cbo_agreeGb.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cal_start_dt.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_start_dt.addEventHandler("canchange", this.cal_to_canchange, this);

        };

        this.loadIncludeScript("agreementPhrasePop.xfdl", true);

       
    };
}
)();
