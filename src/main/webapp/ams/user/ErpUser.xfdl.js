﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ErpUser");
                this.set_classname("style01");
                this.set_titletext("ERP 사용자관리");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_save", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"140\"/><Column size=\"140\"/><Column size=\"100\"/><Column size=\"140\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"140\"/><Column size=\"140\"/><Column size=\"160\"/><Column size=\"140\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"SCM_USER_YN\"/><Cell col=\"3\" text=\"NO_EMP\"/><Cell col=\"4\" text=\"NM_KOR\"/><Cell col=\"5\" text=\"CD_BIZAREA\"/><Cell col=\"6\" text=\"CD_DEPT\"/><Cell col=\"7\" text=\"NM_DEPT\"/><Cell col=\"8\" text=\"DT_ENTER\"/><Cell col=\"9\" text=\"DT_RETIRE\"/><Cell col=\"10\" text=\"CD_INCOM\"/><Cell col=\"11\" text=\"NO_TEL\"/><Cell col=\"12\" text=\"NO_TEL_EMER\"/><Cell col=\"13\" text=\"NO_EMAIL\"/><Cell col=\"14\" text=\"INSERTDATE\"/><Cell col=\"15\" text=\"INSERTURKEY\"/><Cell col=\"16\" text=\"UPDATEDATE\"/><Cell col=\"17\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;background:bind:background;background2:bind:background;\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:scm_user_yn_desc\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:no_emp\" editdisplay=\"edit\"/><Cell col=\"4\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:nm_kor\"/><Cell col=\"5\" displaytype=\"normal\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:cd_bizarea\" calendardisplaynulltype=\"default\"/><Cell col=\"6\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:cd_dept\"/><Cell col=\"7\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:nm_dept\"/><Cell col=\"8\" displaytype=\"date\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:dt_enter\" mask=\"yyyy-MM-dd\" calendardisplaynulltype=\"none\"/><Cell col=\"9\" displaytype=\"date\" edittype=\"none\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:dt_retire\" mask=\"yyyy-MM-dd\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:cd_incom\"/><Cell col=\"11\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:no_tel\"/><Cell col=\"12\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:no_tel_emer\"/><Cell col=\"13\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:no_email\"/><Cell col=\"14\" displaytype=\"date\" style=\"background:bind:background;background2:bind:background;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"15\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:inserturkey\"/><Cell col=\"16\" displaytype=\"date\" style=\"background:bind:background;background2:bind:background;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"17\" style=\"align:left;background:bind:background;background2:bind:background;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_addUser", "absolute", null, "0", "114", "24", "155", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_tooltiptext("추가");
            obj.set_text("SCM 사용자 등록");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_cancelUser", "absolute", null, "0", "114", "24", "37", null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("SCM 사용자 취소");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this);
            obj.set_taborder("4");
            obj.set_text("ERP 사용자 내역");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("ERP 사용자관리");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_addUser","tooltiptext","gds_lang","SCM_USER_ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitTop.btn_addUser","text","gds_lang","SCM_USER_ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_cancelUser","tooltiptext","gds_lang","SCM_USER_CANCEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitTop.btn_cancelUser","text","gds_lang","SCM_USER_CANCEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","Static00","text","gds_lang","ME00110871_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("ErpUser.xfdl", "lib::Comm.xjs");
        this.registerScript("ErpUser.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ErpUser.xfdl
        * PROGRAMMER  : CMJ
        * DATE        : 2018.06.11
        * DESCRIPTION : ERP 사용자
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gv_header.set_nodatatext("");
        	this.ds_header.clearData();
        	
        	this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        	
        	this.gfn_setCommon("BEANID", "erpUserController");
        	this.gfn_setCommon("METHODNM", "selectErpUserInfoList");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_save= function()
        {
        	this.gfn_setCommon("BEANID", "erpUserController");
        	this.gfn_setCommon("METHODNM", "saveErpUserInfo");
        	
        	this.ds_save.clearData();
        	
        	this.ds_header.set_enableevent(false);
        	this.ds_header.filter("CHK == '1'");
        	this.ds_save.copyData(this.ds_header, true);
        	this.ds_header.filter("");
        	this.ds_header.set_enableevent(true);
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_save";
            var sOutData = "";
            var sParam   = "workType=SAVE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "erpUserController");
        	this.gfn_setCommon("METHODNM", "saveErpUserInfo");
        	
        	this.ds_save.clearData();
        	
        	this.ds_header.set_enableevent(false);
        	this.ds_header.filter("CHK == '1'");
        	this.ds_save.copyData(this.ds_header, true);
        	this.ds_header.filter("");
        	this.ds_header.set_enableevent(true);
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_save";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        		}else{
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_SCMUSER_CANCEL_SUCCESS");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_SCMUSER_ADD_SUCCESS");
        		this.parent.div_cond.btn_search.click();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClickChangeRowType(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {};
        	this.parent.div_cond.fn_excelSearch("erpUserController/excelDown.do", this.gv_header, this, oValue);
        }

        /* div_splitTop_btn_addUser_onclick 실행 */
        this.div_splitTop_btn_addUser_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        	
        		var dFlag = true;
        		for(var i =0; i< this.ds_header.rowcount; i++) {
        			if('1' == this.ds_header.getColumn(i,"CHK") && 'Y' == this.ds_header.getColumn(i,"scm_user_yn"))
        			{
        				dFlag = false;
        				break;
        			}
        		}
        		if(dFlag) {
        			this.gfn_confirm("MSG_ALERT_SCMUSER_ADD", "", function(msg, flag){
        				if(flag){
        					this.fn_save();
        				}
        			});
        		} else {
        			this.gfn_alert("MSG_ALERT_SCMUSER_ADD_NO");
        		}

        	}
        }

        /* div_splitTop_btn_cancelUser_onclick 실행 */
        this.div_splitTop_btn_cancelUser_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		var dFlag = true;
        		for(var i =0; i< this.ds_header.rowcount; i++) {
        			if('1' == this.ds_header.getColumn(i,"CHK") && 'N' == this.ds_header.getColumn(i,"scm_user_yn"))
        			{
        				dFlag = false;
        				break;
        			}
        		}
        		
        		if(dFlag) {
        			this.gfn_confirm("MSG_ALERT_SCMUSER_CANCEL", "", function(msg, flag){
        				if(flag){
        					this.fn_delete();
        				}
        			});
        		} else {
        			this.gfn_alert("MSG_ALERT_SCMUSER_CANCEL_NO");
        		}
        		
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_addUser.addEventHandler("onclick", this.div_splitTop_btn_addUser_onclick, this);
            this.div_splitTop.btn_cancelUser.addEventHandler("onclick", this.div_splitTop_btn_cancelUser_onclick, this);

        };

        this.loadIncludeScript("ErpUser.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
