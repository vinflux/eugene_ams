if (!nexacro.ExtMonthCalendar) 
{
    // ==============================================================================
    // nexacro.ExtMonthCalendar
    // ==============================================================================
   
    nexacro.ExtMonthCalendar = function(id, position, left, top, width, height, right, bottom, parent) 
    {
		//alert("start~~~~~~~~~~~~~~~~~~~~~");
        nexacro.Div.call(this, id, position, left, top, width, height, right, bottom, parent);
        this._draw 		= true;
		this.reDraw();
          
    };

	var _pExtMonthCalendar = nexacro._createPrototype(nexacro.Div);
    nexacro.ExtMonthCalendar.prototype = _pExtMonthCalendar;
    _pExtMonthCalendar._type = "Div";
    _pExtMonthCalendar._type_name = "Div";

	_pExtMonthCalendar.reDraw = function() {
		//maskedit 생성
		var objMaskEdit = new MaskEdit();  
		objMaskEdit.init("mae_calendarMonth","absolute", 0, 0, 76, 19, null, null);

		this.addChild("mae_calendarMonth", objMaskEdit); 
		if( !this.isNull(this._required) && this._required == "true" || this.class == "input_point") {
			objMaskEdit.class = "input_point";
		}

		objMaskEdit.set_value(this._value); //draw가 늦게 될 수가 있다. 지정된 값으로 초기화시키게 하자.
		//objMaskEdit.anchor = "left top right";
		objMaskEdit.set_autoselect(true);
		objMaskEdit.set_type("string");		
		objMaskEdit.set_style("align:left middle");
		objMaskEdit.set_mask("####-##");
		objMaskEdit.set_tooltiptext(this.tooltiptext);
		objMaskEdit.set_trimtype("both");
		objMaskEdit.show();
	
        if(!this.isNull(this._search))  objMaskEdit._search  = this._search;
		//버튼 생성
		var objButton = new Button();  
		objButton.init("btn_calendarMonth","absolute", 56, 0, 18, 18,null,null);
		this.addChild("btn_cal", objButton); 

		//버튼 이벤트 등록
		var nIndex = objButton.addEventHandler("onclick", this.btn_cal_onclick, this);
		objButton.style.set_border("0 double #b6b6b6ff #ffffffff");
		objButton.set_style("background: URL('img::btn_calendar_N.png')");
		//objButton.anchor = "top right";
		objButton.set_tabstop(false);
		objButton.show(); 
       
      if(!this.isNull(this._bindDataset) &&  !this.isNull(this._bindColumn))
      {

           this.setBind(_bindDataset, _bindColumn); 
      } 
	 
	  if(!this.isNull(this._enable))  objMaskEdit._search  = this._search;
	 
     // this.set_enable(this._enable);
	};

	/**
	 * @desc : 기준년도를 원하는 값으로 지정
	 */
	_pExtMonthCalendar.setBaseYear = function( sBaseYear ) {

		sBaseYear += "";
		
		if( this.isNull(sBaseYear) || sBaseYear.length != 4 ) return;
		this._base_year = sBaseYear;
	};
	
	/**
	 * @desc : Calendar 값 셋팅
	 */
	_pExtMonthCalendar.setValue  = function( sValue ) {
		
		 var v_sYYMM = sValue.substr(0, 4);
		var v_sMonth = sValue.substr(4, 2);
		

		if (parseInt(v_sMonth) <= 0 || parseInt(v_sMonth) >= 13  || isUndefined(v_sYYMM))
		{
		   return;	
		}
	

		this._value = sValue;

		try {

			if( this._draw == true ) {
	
				var mae_calendarMonth = this.mae_calendarMonth;
				mae_calendarMonth.set_value(sValue);
			
				//mae_calendarMonth_ontextchanged(); //이벤트 강제로 호출
			}
		} catch(e) {
		}
	};
	
	/**
	 * @desc : Calendar 값 가져오는 함수
	 */
	_pExtMonthCalendar.getValue = function() {
		var mae_calendarMonth = this.mae_calendarMonth;
		trace(mae_calendarMonth.name);

		try {
			if( this._draw == true ) {
				this._value = mae_calendarMonth.value;
				return this._value;
			} else {
				return this._value;
			}
		} catch(e) {
		}
		
	};

	_pExtMonthCalendar.btn_cal_onclick = function(obj,  e){
//		var objDiv;		
//       alert("start");
		var sPudId = "pud_calendarMonth";
//		var sParentPath = getPath(this) + "." + sPudId;		
		//var iBaseMon = (isNull(sCalMonth) || sCalMonth.length != 6) ? 1 : parseInt(sCalMonth.substr(4, 2));

		//없을때만 새로 만들자
		if( this.isValidObject( sPudId ) ) {
			objDiv =  this.pud_calendarMonth ;
		
		} else {
			
			// popupdiv 생성
			objDiv = new PopupDiv();  
			objDiv.init(sPudId, "absolute", 0, 0, 177, 155,null,null);
			this.addChild(sPudId, objDiv); 

			//objDiv.style.border = "1 solid #cececeff";
			objDiv.style.set_border( "2 double #b6b6b6ff #ffffffff");
			//objDiv.style.background = "#f3f3f3ff";
			objDiv.set_style("background:'#ffffffff'");

			//objDiv.style.background = "white";
			objDiv.show(); 

            var objMonth = new MaskEdit();
			objMonth.init("sta_year", "absolute", 57, 12, 59, 17,null,null);
			objDiv.addChild("sta_year", objMonth); 
			objMonth.set_style("align:center middle");
			objMonth.set_style("font:Dotum,9,bold");
			objMonth.set_style("background:'#ffffffff'");
			objMonth.style.set_border( "0 none");
			objMonth.set_mask("####");
			objMonth.set_type("string");				
		    objMonth.set_trimtype("both");
			objMonth.show(); 

			var objPrev = new Button();  
			objPrev.init("btn_prev","absolute", 42, 12, 18, 14,null,null);
			objDiv.addChild("btn_prev", objPrev); 
//			objPrev.set_style("background: URL('img::btn_prev_O.png')");
			var nIndex = objPrev.addEventHandler("onclick", this.btn_prev_onclick, this);
            objPrev.set_text("<");
			objPrev.show(); 
			
			
			var objNext = new Button();  
			objNext.init("btn_next","absolute", 116, 12, 18, 14, null,null);
			objDiv.addChild("btn_next", objNext); 
			
//		    objNext.set_style("background: URL('img::btn_next_O.png')");
			objNext.set_text(">");
			var nIndex = objNext.addEventHandler("onclick", this.btn_next_onclick, this);
			objNext.show(); 


			//달 선택 버튼
			var iTop = 10;
			var iLeft = -23;
			var iWidth = 33;
			var iHeight = 30;
			
			var iLeftMargin = 40;
			var iTopMargin = 35;
			
			for( var i=1; i<=12; i++ ) {
				var objMon = new Button();  
				objMon.init("btn_mon" + i,"absolute", iLeft+iWidth, iTop + iHeight,  iWidth,  iHeight,null,null);
				objDiv.addChild("btn_mon" + i, objMon); 
				
				objMon.set_style("font:'Dotum,9,bold'");			
				objMon.set_text(i);
				objMon.month = i;
				objMon.style.set_border( "2 double #b6b6b6ff #ffffffff");
				objMon.set_style("background:'@gradation'");
				var nIndex = objMon.addEventHandler("onclick", this.btn_mon_onclick, this);
				
				objMon.show(); 
				
				
				if( (i % 4) == 0 ) {
					iLeft = -23;
					iTop += iTopMargin;
				} else {
					iLeft += iLeftMargin;
				}
			}
		}
		
		//입력한 edit 에값이 있다면 값을 기준으로
		var mae_calendarMonth =  this.mae_calendarMonth;
		
		
		var objDate = new Date();
	   var strDate = String(objDate.getFullYear());
	   var sCalMonth = mae_calendarMonth.value;
	   if(this.isNull(sCalMonth) && this.isNull(this._base_year)) this._base_year = strDate +"01";
	
		if( !this.isNull(sCalMonth) && sCalMonth.length >= 4 ) {
	
			if(parseInt(sCalMonth.substr(0, 4)) < 1970)
			{
			    mae_calendarMonth.set_value("197001");
	            this._base_year  = "1970";
			}
			else
			{
				this._base_year = sCalMonth.substr(0, 4);	
				
			}
			
		}
		
		
		//날짜 지정
		this.setCalendar();
		var v_nY = "";		
		var v_nX = this.getOffsetLeft();
		var v_nY = Number(this.getOffsetTop()) +Number(mae_calendarMonth.getOffsetHeight())+Number(mae_calendarMonth.getOffsetHeight());
		var v_sRet = objDiv.trackPopup(v_nX,v_nY);
	};

		/**
	 * @desc : 이전 년도 onclick
	 */
	_pExtMonthCalendar.btn_prev_onclick = function(obj,e) {
		this.setBaseYear( parseInt(this._base_year) - 1 );
		
		this.setCalendar();
	}

	/**
	 * @desc : 이후 년도 onclick
	 */
	_pExtMonthCalendar.btn_next_onclick = function(obj,e) {
		this.setBaseYear( parseInt(this._base_year) + 1 );
		
		this.setCalendar();
	}

     /**
	 * @desc : 선택한 달 onclick
	 */
	 _pExtMonthCalendar.btn_mon_onclick = function(obj,e) {

		var objDate = new Date();
		var strDate = String(objDate.getFullYear());
		var month = String(obj.month).padLeft(2, "0");
		var nDate = this._base_year + month;
	
	    
		var objDiv = this.pud_calendarMonth;
		objDiv.closePopup();
		this.mae_calendarMonth.set_value(nDate);
	}

 this.mae_calendarMonth_ontextchanged = function (obj,e) {

		try {
			var eventNm = getNullToEmpty( this._ontextchanged );
		  
		
			if( isNull(eventNm) ) return;
			
			if (typeof( eval(eventNm) ) == "function") {
				eval(eventNm + " ('"+mae_calendarMonth.value+"' );");
			}
		} catch(e) {
		}
	}

    _pExtMonthCalendar.setCalendar = function() {

		//-------------------------------------------------------------------
		//기준년도 지정

		//지정되어 있는 base_year 기준으로
		if( !this.isNull(this._base_year) && this._base_year.length == 4 ) {
       
		} 

		//그것도 아니라면 현재 날짜 기준으로
		else {
	

			//기준일자를 정합시다.
			var objDate = new Date();
			var strDate = String(objDate.getFullYear());
			
			this._base_year = strDate;
		}
		this.pud_calendarMonth.sta_year.set_value(this._base_year);
		//-------------------------------------------------------------------
	}


	/**
	 * null 값 체크
	 * @param 비교할 값
	 * @return true / false
	 */
	_pExtMonthCalendar.isNull = function(sValue)
	{
		if( new String(sValue).valueOf() == "undefined") 
			return true;
		if( sValue == null )
			return true;
		if( ("x"+sValue == "xNaN") && ( new String(sValue.length).valueOf() == "undefined" ) )
			return true;
		if( sValue.length == 0 )
			return true;
		return false;
	};
	
	//월달력 바인드 셋팅
	 _pExtMonthCalendar.setBind = function(v_sDs, v_sColName) 
	 {
       if(this.isValidObject("EXTMONTHCALENDAR")) this.removeChild("EXTMONTHCALENDAR");
		
		var v_objNewbindItem = new BindItem();  

		v_objNewbindItem.init("EXTMONTHCALENDAR", "mae_calendarMonth", "value", v_sDs, v_sColName);
		
		this.addChild("EXTMONTHCALENDAR", v_objNewbindItem); 
		this.EXTMONTHCALENDAR.bind();
	};
		
		/************************************************************
	 * 그리드와 바인딩해야 할 경우 바인딩될 그리드를 지정한다.
	 * 추가  :  w.s.jeong
	 ************************************************************/
	_pExtMonthCalendar.setBindDataset = function(_bindDataset, _bindColumn)
	{
		this._BindDatasetFlag = true;
		
		this.mae_calendarMonth.enable = false;
		

		this._bindDataset 	= getNullToEmpty(_bindDataset);
		this._bindColumn	= getNullToEmpty(_bindColumn);
			
		if( this._bindDataset == "" || this._bindColumn == "" ) return;
	};

   delete _pExtMonthCalendar;
}