﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UserScreen");
                this.set_classname("style01");
                this.set_titletext("사용자 화면");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"uskey\" type=\"STRING\" size=\"256\"/><Column id=\"usname\" type=\"STRING\" size=\"256\"/><Column id=\"uspath\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_hdname\" type=\"STRING\" size=\"256\"/><Column id=\"isdwus\" type=\"STRING\" size=\"256\"/><Column id=\"nxpath\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"remark\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"field1\">Y</Col><Col id=\"field2\">예</Col></Row><Row><Col id=\"field1\">N</Col><Col id=\"field2\">아니요</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"140\"/><Column size=\"130\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"220\"/><Column size=\"300\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"APKEY\"/><Cell col=\"4\" text=\"USKEY\"/><Cell col=\"5\" text=\"USNAME\"/><Cell col=\"6\" text=\"USPATH\"/><Cell col=\"7\" text=\"USSC_HDKEY\"/><Cell col=\"8\" text=\"USSC_HDNAME\"/><Cell col=\"9\" text=\"ISDWUS\"/><Cell col=\"10\" text=\"NXPATH\"/><Cell col=\"11\" text=\"REMARK\"/><Cell col=\"12\" text=\"INSERTDATE\"/><Cell col=\"13\" text=\"INSERTURKEY\"/><Cell col=\"14\" text=\"UPDATEDATE\"/><Cell col=\"15\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:apkey\" editlimit=\"10\" editdisplay=\"edit\" expandshow=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;show&quot; : &quot;hide&quot;\" expandsize=\"18\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:uskey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"5\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:usname\" editlimit=\"33\" editdisplay=\"edit\"/><Cell col=\"6\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:uspath\" editlimit=\"100\" editdisplay=\"edit\"/><Cell col=\"7\" edittype=\"normal\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdkey\" editlimit=\"-1\" editdisplay=\"edit\" expandshow=\"show\" expandsize=\"18\"/><Cell col=\"8\" style=\"align:left;\" text=\"bind:ussc_hdname\"/><Cell col=\"9\" displaytype=\"combo\" edittype=\"combo\" style=\"padding: ;\" text=\"bind:isdwus\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"10\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:nxpath\" editlimit=\"200\" editdisplay=\"edit\"/><Cell col=\"11\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:remark\" editlimit=\"200\" editdisplay=\"edit\"/><Cell col=\"12\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"13\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"14\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"15\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("사용자 화면 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_headerExcel", "absolute", "232", "284", null, null, "224", "195", this.div_splitTop);
            obj.set_taborder("20");
            obj.set_binddataset("ds_header");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_visible("false");
            obj.set_enable("false");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"140\"/><Column size=\"130\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"220\"/><Column size=\"300\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/><Cell col=\"1\" text=\"APKEY\"/><Cell col=\"2\" text=\"USKEY\"/><Cell col=\"3\" text=\"USNAME\"/><Cell col=\"4\" text=\"USPATH\"/><Cell col=\"5\" text=\"USSC_HDKEY\"/><Cell col=\"6\" text=\"USSC_HDNAME\"/><Cell col=\"7\" text=\"ISDWUS\"/><Cell col=\"8\" text=\"NXPATH\"/><Cell col=\"9\" text=\"REMARK\"/><Cell col=\"10\" text=\"INSERTDATE\"/><Cell col=\"11\" text=\"INSERTURKEY\"/><Cell col=\"12\" text=\"UPDATEDATE\"/><Cell col=\"13\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"1\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:apkey\" editlimit=\"10\" editdisplay=\"edit\" expandshow=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;show&quot; : &quot;hide&quot;\" expandsize=\"18\"/><Cell col=\"2\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:uskey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"3\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:usname\" editlimit=\"33\" editdisplay=\"edit\"/><Cell col=\"4\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:uspath\" editlimit=\"100\" editdisplay=\"edit\"/><Cell col=\"5\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdkey\" editlimit=\"-1\" editdisplay=\"edit\" expandshow=\"show\" expandsize=\"18\"/><Cell col=\"6\" style=\"align:left;\" text=\"bind:ussc_hdname\"/><Cell col=\"7\" displaytype=\"normal\" edittype=\"none\" style=\"padding: ;\" text=\"bind:isdwus_desc\" combodisplay=\"edit\"/><Cell col=\"8\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:nxpath\" editlimit=\"200\" editdisplay=\"edit\"/><Cell col=\"9\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:remark\" editlimit=\"200\" editdisplay=\"edit\"/><Cell col=\"10\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"11\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"12\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"13\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자 화면");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00000620_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("UserScreen.xfdl", "lib::Comm.xjs");
        this.registerScript("UserScreen.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : UserScreen.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 사용자 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        this.searchFalg = "";
        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "userScreenController");
        	this.gfn_setCommon("METHODNM", "selectUserScreen");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "userScreenController");
        	this.gfn_setCommon("METHODNM", "saveUserScreenList");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_input_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.searchFalg = "";
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "save"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		if(this.ds_param.rowcount == 0) this.parent.div_cond.btn_search.click();
        		else this.parent.div_cond.fn_pageSearch(this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "COUNT"));
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sType = obj.getCellProperty("head", e.cell, "displaytype");
        	
        	if(sType != "checkbox" && colName != "" && colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(sType == "checkbox"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("userScreenController/excelDown.do", this.div_splitTop.grd_headerExcel, this, oValue);
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				while(nRow != -1){
        					if(this.ds_header.getRowType(nRow) == "2"){
        						this.ds_header.deleteRow(nRow);
        						nRow = this.ds_header.findRowExpr("CHK == '1'");
        					}else{
        						this.ds_header.setColumn(nRow, "STATUS", "D");
        						nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        					}
        				}
        				this.fn_save();
        			}
        		});
        		
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	nRow = this.ds_header.addRow();
        	this.ds_header.setColumn(nRow, "STATUS", "C");
        	this.ds_header.set_rowposition(nRow);
        	this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "apkey"));
        	this.gv_header.setFocus();
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") {
        		obj.set_updatecontrol(true);
        	}else if(e.columnid != "STATUS"){
        	 	if(e.columnid == "ussc_hdkey"){
        			if(e.newvalue == ""){
        					obj.setColumn(e.row, "ussc_hdname", "");
        			}else{
        					var key = "";
        					var value = "";
        					this.gv_code = "";
        					this.gfn_codeSearch(key, value, e.newvalue, "130", application.gv_ams, "", "", "", "", obj, "ussc_hdname", e.row);
        		}
        	}
        	 this.gfn_statusChk(obj, e.row);
        	}

        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "apkey|ussc_hdkey|ussc_hdname";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_save();
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* div_splitTop_grd_header_oncellclick 실행 */
        this.div_splitTop_grd_header_oncellclick = function(obj,e)
        {
        	// 한 번 클릭으로 동작
        	obj.dropdownCombo();
        	obj.dropdownCalendar();
        }

        /* div_splitTop_grd_header_onexpanddown 실행 */
        this.div_splitTop_grd_header_onexpanddown = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	if(obj.getBindCellIndex("body", "apkey") == e.col){
        		var oArg = { divId:"Grid"
        					,searchId:10
        					,putObj:this.ds_header
        					,putCol:"apkey"
        					,putRow:""+e.row
        				   };
        		this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        	}else if(obj.getBindCellIndex("body", "ussc_hdkey") == e.col){
        		var oArg = { divId:"Grid"
        					,searchId:130
        					,putObj:this.ds_header
        					,putCol:"ussc_hdkey"
        					,putRow:""+e.row
        				   };
        		this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        	}
        }

        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_header.addEventHandler("onexpanddown", this.div_splitTop_grd_header_onexpanddown, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("onexpanddown", this.div_splitTop_grd_header_onexpanddown, this);

        };

        this.loadIncludeScript("UserScreen.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
