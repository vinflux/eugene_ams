﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SearchCondition");
                this.set_classname("style01");
                this.set_titletext("사용자 화면 검색 조건");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"ussc_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_hdname\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_dtkey\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_order\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_dttype\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_dttype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_label\" type=\"STRING\" size=\"256\"/><Column id=\"dbfieldname\" type=\"STRING\" size=\"256\"/><Column id=\"searchid\" type=\"STRING\" size=\"256\"/><Column id=\"defaultoperator\" type=\"STRING\" size=\"256\"/><Column id=\"defaultvalue\" type=\"STRING\" size=\"256\"/><Column id=\"singleoperatoryn\" type=\"STRING\" size=\"256\"/><Column id=\"singleoperatoryn_desc\" type=\"STRING\" size=\"256\"/><Column id=\"inputrestrict\" type=\"STRING\" size=\"256\"/><Column id=\"inputrestrict_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ismandatory\" type=\"STRING\" size=\"256\"/><Column id=\"ismandatory_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_active\" type=\"STRING\" size=\"256\"/><Column id=\"ussc_active_desc\" type=\"STRING\" size=\"256\"/><Column id=\"fr_date_value\" type=\"STRING\" size=\"256\"/><Column id=\"to_date_value\" type=\"STRING\" size=\"256\"/><Column id=\"gmtapply_chk\" type=\"STRING\" size=\"256\"/><Column id=\"gmtapply_chk_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ymd_format\" type=\"STRING\" size=\"256\"/><Column id=\"ymd_format_desc\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_dttype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_defaultoperator", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_inputrestrict", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ymd_format", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_paramDetail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"210\"/><Column size=\"450\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"USSC_HDKEY\"/><Cell col=\"4\" text=\"USSC_HDNAME\"/><Cell col=\"5\" text=\"INSERTDATE\"/><Cell col=\"6\" text=\"INSERTURKEY\"/><Cell col=\"7\" text=\"UPDATEDATE\"/><Cell col=\"8\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"4\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdname\" editlimit=\"100\" editdisplay=\"edit\"/><Cell col=\"5\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"7\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"8\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("사용자 화면 검색 조건 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"210\"/><Column size=\"210\"/><Column size=\"180\"/><Column size=\"210\"/><Column size=\"180\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"90\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" text=\"NO\"/><Cell col=\"3\" text=\"USSC_HDKEY\"/><Cell col=\"4\" text=\"USSC_DTKEY\"/><Cell col=\"5\" text=\"USSC_ORDER\"/><Cell col=\"6\" text=\"USSC_DTTYPE\"/><Cell col=\"7\" text=\"USSC_LABEL\"/><Cell col=\"8\" text=\"DBFIELDNAME\"/><Cell col=\"9\" text=\"SEARCHID\"/><Cell col=\"10\" text=\"DEFAULTOPERATOR\"/><Cell col=\"11\" text=\"DEFAULTVALUE\"/><Cell col=\"12\" text=\"SINGLEOPERATORYN\"/><Cell col=\"13\" text=\"INPUTRESTRICT\"/><Cell col=\"14\" text=\"ISMANDATORY\"/><Cell col=\"15\" text=\"GMTAPPLY_CHK\"/><Cell col=\"16\" text=\"YMD_FORMAT\"/><Cell col=\"17\" text=\"FR_DATE_VALUE\"/><Cell col=\"18\" text=\"TO_DATE_VALUE\"/><Cell col=\"19\" text=\"INSERTDATE\"/><Cell col=\"20\" text=\"INSERTURKEY\"/><Cell col=\"21\" text=\"UPDATEDATE\"/><Cell col=\"22\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdkey\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:ussc_dtkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"5\" displaytype=\"number\" edittype=\"text\" style=\"align:right;padding: ;\" text=\"bind:ussc_order\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"6\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ussc_dttype\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_dttype\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"7\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:ussc_label\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"8\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:dbfieldname\" editlimit=\"65\" editdisplay=\"edit\"/><Cell col=\"9\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:searchid\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"10\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:defaultoperator\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_defaultoperator\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"11\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:defaultvalue\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"12\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:singleoperatoryn\" editlimit=\"10\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"13\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:inputrestrict\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_inputrestrict\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"14\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ismandatory\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"15\" displaytype=\"combo\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;combo&quot; : &quot;none&quot;))\" style=\"align:left;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:gmtapply_chk\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"16\" displaytype=\"combo\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;combo&quot; : &quot;none&quot;))\" style=\"background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:ymd_format\" editautoselect=\"false\" editdisplay=\"edit\" combodataset=\"ds_ymd_format\" combocodecol=\"field1\" combodatacol=\"field2\" combodisplay=\"edit\"/><Cell col=\"17\" displaytype=\"number\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;masknumber&quot; : &quot;none&quot;)\" editfilter=\"integer\" style=\"align:right;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;));\" text=\"bind:fr_date_value\" mask=\"#,##0\" editlimit=\"3\"/><Cell col=\"18\" displaytype=\"number\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;masknumber&quot; : &quot;none&quot;))\" editfilter=\"integer\" style=\"align:right;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:to_date_value\" mask=\"#,##0\" editlimit=\"3\"/><Cell col=\"19\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"20\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"21\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"22\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_text("사용자 화면 검색 조건 상세 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitBottom);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitBottom);
            obj.set_taborder("20");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("21");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitBottom);
            obj.set_taborder("22");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Grid("grd_detailExcel", "absolute", "184", "106", null, null, "240", "51", this.div_splitBottom);
            obj.set_taborder("23");
            obj.set_binddataset("ds_detail");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_visible("false");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"210\"/><Column size=\"210\"/><Column size=\"180\"/><Column size=\"210\"/><Column size=\"180\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"90\"/><Column size=\"120\"/><Column size=\"140\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"NO\"/><Cell col=\"2\" text=\"USSC_HDKEY\"/><Cell col=\"3\" text=\"USSC_DTKEY\"/><Cell col=\"4\" text=\"USSC_ORDER\"/><Cell col=\"5\" text=\"USSC_DTTYPE\"/><Cell col=\"6\" text=\"USSC_LABEL\"/><Cell col=\"7\" text=\"DBFIELDNAME\"/><Cell col=\"8\" text=\"SEARCHID\"/><Cell col=\"9\" text=\"DEFAULTOPERATOR\"/><Cell col=\"10\" text=\"DEFAULTVALUE\"/><Cell col=\"11\" text=\"SINGLEOPERATORYN\"/><Cell col=\"12\" text=\"INPUTRESTRICT\"/><Cell col=\"13\" text=\"ISMANDATORY\"/><Cell col=\"14\" text=\"GMTAPPLY_CHK\"/><Cell col=\"15\" text=\"YMD_FORMAT\"/><Cell col=\"16\" text=\"FR_DATE_VALUE\"/><Cell col=\"17\" text=\"TO_DATE_VALUE\"/><Cell col=\"18\" text=\"INSERTDATE\"/><Cell col=\"19\" text=\"INSERTURKEY\"/><Cell col=\"20\" text=\"UPDATEDATE\"/><Cell col=\"21\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ussc_hdkey\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:ussc_dtkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"4\" displaytype=\"number\" edittype=\"text\" style=\"align:right;padding: ;\" text=\"bind:ussc_order\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"5\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ussc_dttype_desc\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"6\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:ussc_label\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"7\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:dbfieldname\" editlimit=\"65\" editdisplay=\"edit\"/><Cell col=\"8\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:searchid\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"9\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:defaultoperator\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"10\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:defaultvalue\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"11\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:singleoperatoryn_desc\" editlimit=\"10\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"12\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:inputrestrict_desc\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"13\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ismandatory_desc\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"14\" displaytype=\"combo\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;combo&quot; : &quot;none&quot;))\" style=\"align:left;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:gmtapply_chk_desc\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"15\" displaytype=\"combo\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;combo&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;combo&quot; : &quot;none&quot;))\" style=\"background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:ymd_format_desc\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"16\" displaytype=\"number\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;masknumber&quot; : &quot;none&quot;)\" editfilter=\"integer\" style=\"align:right;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;));\" text=\"bind:fr_date_value\" mask=\"#,##0\" editlimit=\"3\"/><Cell col=\"17\" displaytype=\"number\" edittype=\"expr:ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;masknumber&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;masknumber&quot; : &quot;none&quot;))\" editfilter=\"integer\" style=\"align:right;background:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff8d5&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff8d5&quot; : &quot;&quot;)));background2:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#fff5c2&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#fff5c2&quot; : &quot;&quot;)));selectbackground:EXPR(ussc_dttype == &quot;BETWEENDATEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;BETWEENDATETIMEFIELD&quot; ? &quot;#e1e0d9&quot; : (ussc_dttype == &quot;DATEFIELD&quot; ? &quot;#e1e0d9&quot; : &quot;&quot;)));\" text=\"bind:to_date_value\" mask=\"#,##0\" editlimit=\"3\"/><Cell col=\"18\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"19\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"20\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"21\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자 화면 검색 조건");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitBottom.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitBottom.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00000610_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitBottom.Static06","text","gds_lang","ME00000610_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("SearchCondition.xfdl", "lib::Comm.xjs");
        this.registerScript("SearchCondition.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SearchCondition.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 사용자 화면 검색 조건
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";
        var dtlsearchcnt = 0;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header, this.gv_detail]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	this.gfn_getCode("SEARCHCONDITIONTYPE", this.ds_dttype);
        	this.gfn_getCode("SEARCHOPERATOR", this.ds_defaultoperator);
        	this.gfn_getCode("INPUTRESTRICT", this.ds_inputrestrict);
        	this.gfn_getCode("YMD_FORMAT", this.ds_ymd_format);
        	this.gfn_getCode("YESORNO", this.ds_yesno);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "searchConditionMgtController");
        	this.gfn_setCommon("METHODNM", "selectSearchCondition");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	if((this.gfn_isUpdate(this.ds_header) || this.gfn_isUpdate(this.ds_detail)) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				this.ds_detail.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		this.ds_detail.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchDetail = function()
        {
        	this.gfn_getScroll(this.gv_detail); // 스크롤 위치 저장
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.gv_detail.set_nodatatext("");
        	this.ds_detail.clearData();
        	
        	this.gfn_setCommon("BEANID", "searchConditionMgtController");
        	this.gfn_setCommon("METHODNM", "selectSearchConditionDetail");
        	
        	var sSvcId   = "selectDetail"+dtlsearchcnt++;
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_detail=OUT_rtnGrid2 ds_paramDetail=OUT_PARAM";
        	var sParam   = "ussc_hdkey="+this.ds_header.getColumn(this.ds_header.rowposition, "ussc_hdkey");
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_save = function(sAll)
        {
        	this.gfn_setCommon("BEANID", "searchConditionMgtController");
        	this.gfn_setCommon("METHODNM", "saveSearchCondition");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_input_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "chkAll="+sAll;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_saveDetail = function()
        {
        	this.gfn_setCommon("BEANID", "searchConditionMgtController");
        	this.gfn_setCommon("METHODNM", "saveSearchConditionDetail");
        	
            var sSvcId   = "saveDetail";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_input_LIST=ds_detail:U";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_errAlert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId.substr(0, 12) == "selectDetail"){
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_detail);
        		this.searchFalg = "";
        	}else if(sSvcId == "save"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "saveDetail"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sType = obj.getCellProperty("head", e.cell, "displaytype");
        	
        	if(sType != "checkbox" && colName != "" && colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.rowChg = "Y";
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(sType == "checkbox"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_detail.rowcount > 0){
        		this.gfn_sortDataset(obj, e);
        	}else if(colName == "CHK"){
        		this.ds_detail.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_detail.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("searchConditionMgtController/excelDownSearchCondition.do", this.gv_header, this, oValue);
        }

        /* div_splitBottom_btn_excel_onclick 실행 */
        this.div_splitBottom_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        		ussc_hdkey: this.ds_header.getColumn(this.ds_header.rowposition, "ussc_hdkey")
        	}
        	
        	this.parent.div_cond.fn_excelSearch("searchConditionMgtController/excelDownSearchConditionDetail.do", this.div_splitBottom.grd_detailExcel, this, oValue);
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	var nChkCnt = this.ds_header.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        // 	}else if(nChkCnt == this.ds_header.rowcount){
        // 		var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        // 		var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        // 		var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        // 		
        // 		this.gfn_confirm3Btn (
        // 			arrBtnText,
        // 			sMsgCode,
        // 			sMsgText,
        // 			function(msg, flag) {
        // 				if (flag != "0") {
        // 					while(nRow != -1){
        // 						if(this.ds_header.getRowType(nRow) == "2"){
        // 							this.ds_header.deleteRow(nRow);
        // 							nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 						}else{
        // 							this.ds_header.setColumn(nRow, "STATUS", "D");
        // 							nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        // 						}
        // 					}
        // 					
        // 					if (flag == "1") this.fn_delete("");
        // 					if (flag == "2") this.fn_delete("ALL");
        // 				}
        // 			}
        // 		);
        	}else{
        	
        			if(nChkCnt == this.ds_header.rowcount){
        				var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        				var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        				var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        				
        				this.gfn_confirm3Btn (
        					arrBtnText,
        					sMsgCode,
        					sMsgText,
        					function(msg, flag) {
        						if (flag != "0") {
        							while(nRow != -1){
        								if(this.ds_header.getRowType(nRow) == "2"){
        									this.ds_header.deleteRow(nRow);
        									nRow = this.ds_header.findRowExpr("CHK == '1'");
        								}else{
        									this.ds_header.setColumn(nRow, "STATUS", "D");
        									nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        								}
        							}
        							if (flag == "1") this.fn_save("");
        							if (flag == "2") this.fn_save("ALL");
        						}
        					}
        				);
        			}else{
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						while(nRow != -1){
        							if(this.ds_header.getRowType(nRow) == "2"){
        								this.ds_header.deleteRow(nRow);
        								nRow = this.ds_header.findRowExpr("CHK == '1'");
        							}else{
        								this.ds_header.setColumn(nRow, "STATUS", "D");
        								nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        							}
        						}
        						
        						if (this.ds_header.getCaseCount("STATUS == 'D'") > 0) this.fn_save("");
        					}
        				});
        			}
        		
        	}
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "ussc_hdname";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_save("");
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	this.ds_detail.clearData();
        	
        	var nRow = this.ds_header.addRow();
        	this.ds_header.setColumn(nRow, "STATUS", "C");
        	this.ds_header.set_rowposition(nRow);
        	this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "ussc_hdkey"));
        	this.gv_header.setFocus();
        }

        /* div_splitBottom_btn_add_onclick 실행 */
        this.div_splitBottom_btn_add_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(nRow != -1){
        		this.gfn_alert("MSG_80110");
         	}else{
        		nRow = this.ds_detail.addRow();
        		this.ds_detail.setColumn(nRow, "STATUS", "C");
        		this.ds_detail.setColumn(nRow, "ussc_hdkey", this.ds_header.getColumn(this.gv_nRow, "ussc_hdkey"));
        		this.ds_detail.set_rowposition(nRow);
        		this.gv_detail.setCellPos(this.gv_detail.getBindCellIndex("body", "ussc_dtkey"));
        		this.gv_detail.setFocus();
         	}
        }

        /* div_splitBottom_btn_delete_onclick 실행 */
        this.div_splitBottom_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				while(nRow != -1){
        					if(this.ds_detail.getRowType(nRow) == "2"){
        						this.ds_detail.deleteRow(nRow);
        						nRow = this.ds_detail.findRowExpr("CHK == '1'");
        					}else{
        						this.ds_detail.setColumn(nRow, "STATUS", "D");
        						nRow = this.ds_detail.findRowExpr("CHK == '1'", nRow+1);
        					}
        				}
        				if (this.ds_detail.getCaseCount("STATUS == 'D'") > 0) this.fn_saveDetail();
        			}
        		});
        		
        	}
        }

        /* div_splitBottom_btn_save_onclick 실행 */
        this.div_splitBottom_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "ussc_order|ussc_dttype|ussc_label|dbfieldname|defaultoperator";
        	
        	if(this.gfn_isUpdate(this.ds_detail)){
        		if(this.gfn_isDataNullCheck(this.gv_detail, this.ds_detail, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_saveDetail();
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			this.fn_searchDetail();
        		}
        	}
        	
        	this.rowChg = "";
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1 && obj.getRowType(e.row) != 2){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        		
        		if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        /* ds_detail_cancolumnchange 실행 */
        this.ds_detail_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_detail_oncolumnchanged 실행 */
        this.ds_detail_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") obj.set_updatecontrol(true);
        	else if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        }

        /* div_splitBottom_grd_detail_oncellclick 실행 */
        this.div_splitBottom_grd_detail_oncellclick = function(obj,e)
        {
        	// 한 번 클릭으로 동작
        	obj.dropdownCombo();
        	obj.dropdownCalendar();
        }

        /* div_splitBottom_grd_detail_oncelldblclick 실행 */
        this.div_splitBottom_grd_detail_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_detail.addEventHandler("cancolumnchange", this.ds_detail_cancolumnchange, this);
            this.ds_detail.addEventHandler("oncolumnchanged", this.ds_detail_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.div_splitBottom_grd_detail_oncelldblclick, this);
            this.div_splitBottom.btn_add.addEventHandler("onclick", this.div_splitBottom_btn_add_onclick, this);
            this.div_splitBottom.btn_delete.addEventHandler("onclick", this.div_splitBottom_btn_delete_onclick, this);
            this.div_splitBottom.btn_excel.addEventHandler("onclick", this.div_splitBottom_btn_excel_onclick, this);
            this.div_splitBottom.btn_save.addEventHandler("onclick", this.div_splitBottom_btn_save_onclick, this);
            this.div_splitBottom.grd_detailExcel.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detailExcel.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detailExcel.addEventHandler("oncelldblclick", this.div_splitBottom_grd_detail_oncelldblclick, this);

        };

        this.loadIncludeScript("SearchCondition.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
