<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link rel="stylesheet" href="<c:url value='/css/error_style.css' />">
<script language="javascript">
function fncGoAfterErrorPage(){
    //history.back(-2);
    location.href="<c:url value='/' />";
}
</script>
</head>

<body topmargin="0" leftmargin="0">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
<td bgcolor="#f6f6f6" valign="middle">

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td background="<c:url value='/img/line_bg.gif' />" height="2"></td>
  </tr>
  <tr> 
    <td height="113" align="center"><img src="<c:url value='/img/error_img1.png' />"></td>
  </tr>
  <tr>
    <td height="40" align="center" valign="top" class="brown01">페이지를 찾을수 없습니다.</td>
  </tr>
  <tr> 
    <td background="<c:url value='/img/line_bg.gif' />" height="2"></td>
  </tr>
  <tr> 
    <td height="73" align="center" class="gray01"></td>
  </tr>
  <tr> 
    <td background="<c:url value='/img/line_bg.gif' />" height="2"></td>
  </tr>
  <tr> 
    <td height="70" align="center"><a href="#LINK" onClick="fncGoAfterErrorPage();"><img src="<c:url value='/img/btn_confirm.png' />" border="0"></a></td>
  </tr>
</table>

</td>
</tr>
</table>

</body>
</html>