﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CommCodePop");
                this.set_classname("DynamicCondPop");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,550,620);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_codeList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_paramRtn", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_commonRtn", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_rtn", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "80", null, "69", "0", null, this);
            obj.set_taborder("0");
            obj.style.set_background("#c0c5caff");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_list", "absolute", "10", "159", null, null, "10", "10", this);
            obj.set_taborder("1");
            obj.set_binddataset("ds_list");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"150\"/><Column size=\"280\"/></Columns><Rows><Row size=\"40\"/><Row size=\"40\"/><Row size=\"40\"/><Row size=\"40\"/></Rows><Band id=\"body\"><Cell celltype=\"head\" style=\"selectfont: ;\" text=\"bind:Column0\"/><Cell col=\"1\" style=\"align:left middle;selectfont: ;\" text=\"bind:Column1\"/><Cell row=\"1\" celltype=\"head\" style=\"background2: ;\" text=\"bind:Column0\"/><Cell row=\"1\" col=\"1\" style=\"align:left middle;selectfont: ;\" text=\"bind:Column1\"/><Cell row=\"2\" celltype=\"head\" style=\"background2: ;\" text=\"bind:Column0\"/><Cell row=\"2\" col=\"1\" style=\"align:left middle;selectfont: ;\" text=\"bind:Column1\"/><Cell row=\"3\" celltype=\"head\" style=\"background2: ;\" text=\"bind:Column0\"/><Cell row=\"3\" col=\"1\" style=\"align:left middle;selectfont: ;\" text=\"bind:Column1\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("label00", "absolute", "23", "90", "110", "48", null, null, this);
            obj.set_taborder("2");
            obj.set_text("Search");
            obj.set_cssclass("sta_WF_search");
            obj.style.set_color("#ffffffff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_searchText", "absolute", "133", "90", null, "48", "70", null, this);
            obj.set_taborder("3");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", null, "90", "48", "48", "18", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_WF_pop");
            this.addChild(obj.name, obj);

            obj = new Static("stc_bg", "absolute", "0", "0", null, "80", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("Search");
            obj.set_cssclass("sta_pop_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closePopup", "absolute", null, "0", "70", "80", "0", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_pop_close");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "0", "200", "10", "136", null, null, this);
            obj.set_taborder("7");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "8", "208", "10", "136", null, null, this);
            obj.set_taborder("8");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "158", "149", "142", "10", null, null, this);
            obj.set_taborder("9");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 550, 620, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("DynamicCondPop");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CommCodePop.xfdl", "lib::Comm.xjs");
        this.registerScript("CommCodePop.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/
        var fv_searchId;
        var fv_key;
        var fv_labelCode;
        var fv_compPos;
        var fv_rtnColNm;

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        	fv_searchId = this.arguments.searchid;
        	fv_key = this.arguments.key;
        	fv_labelCode = this.arguments.labelCode;
        	fv_compPos = this.arguments.compPos;

        	this.fv_pagingTran = false;
        	this.fn_search();
        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        this.fn_search = function ()
        {
        // 	this.gfn_addTranColumn(application.gds_param, "searchid");
        // 	application.gds_param.setColumn(0, "searchid", fv_searchId);
        // 
        // 	this.gfn_addTranColumn(application.gds_param, "eqtype");
        // 	application.gds_param.setColumn(0, "eqtype", "10");
        // 
        // 	this.gfn_addTranColumn(application.gds_param, "ibhdtype");
        // 	application.gds_param.setColumn(0, "ibhdtype", undefined);
        // 
        	this.gfn_addTranColumn(application.gds_common, "EQTYPE");
        	application.gds_common.setColumn(0, "EQTYPE", "10");

        	this.gfn_addTranColumn(application.gds_common, "URKEY");
        	application.gds_common.setColumn(0, "URKEY", application.gds_userInfo.getColumn(0, "urKey"));

        	this.gfn_addTranColumn(application.gds_common, "ctkey");
        	application.gds_common.setColumn(0, "ctkey", application.gds_userInfo.getColumn(0, "ctKey"));

        	this.gfn_addTranColumn(application.gds_common, "uskey");
        	application.gds_common.setColumn(0, "uskey", application.gds_userInfo.getColumn(0, "urKey"));

        	this.fn_makeParam("S");

        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectCommonCode");

            var sSvcId   = "selectCommonCodeList";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_codeList=OUT_COMMON_CODE_LIST ds_paramRtn=OUT_PARAM ds_commonRtn=OUT_COMMON";
            var sParam   = "";

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_makeParam = function (sDiv,sValue)
        {
        	if (sDiv == "S") {
        		this.ds_param.clear();
        		this.ds_param.copyData(application.gds_param);
        		application.gds_param.clear();

        		application.gds_param.addColumn("currentPage", "BIGDECIMAL", 255);
        		application.gds_param.addColumn("searchid", "STRING", 255);
        		if (String(fv_searchId).indexOf(";") >= 0) application.gds_param.addColumn("ctcd_hdkey", "STRING", 255);
        		application.gds_param.addColumn("value", "STRING", 255);
        		application.gds_param.addColumn("owkey", "STRING", 255);
        		application.gds_param.addColumn("ctkey", "STRING", 255);
        		application.gds_param.addColumn("pushid", "STRING", 255);
        		application.gds_param.addColumn("ENABLEMAXBETWEENDAYS_MOBILE", "BIGDECIMAL", 255);
        		application.gds_param.addColumn("pagingLimit", "STRING", 255);

        		application.gds_param.addRow();
        		application.gds_param.setColumn(0, "currentPage", 1);

        		if (String(fv_searchId).indexOf(";") >= 0) {
        			application.gds_param.setColumn(0, "searchid", String(fv_searchId).split(";")[0]);
        			application.gds_param.setColumn(0, "ctcd_hdkey", String(fv_searchId).split(";")[1]);
        		} else {
        			application.gds_param.setColumn(0, "searchid", fv_searchId);
        		}

        		application.gds_param.setColumn(0, "value", sValue);
        		application.gds_param.setColumn(0, "owkey", application.gds_userInfo.getColumn(0, "owkeym"));
        		application.gds_param.setColumn(0, "ctkey", application.gds_userInfo.getColumn(0, "ctKey"));
        		application.gds_param.setColumn(0, "pushid", "none");
        		application.gds_param.setColumn(0, "ENABLEMAXBETWEENDAYS_MOBILE", 365);
        		application.gds_param.setColumn(0, "pagingLimit", "100");

        		var sValue = this.edt_searchText.value;

        		// 결과내 재검색 처리
        		if (!this.gfn_isNull(sValue)) {
        			this.gfn_addTranColumn(application.gds_param, "value");
        			application.gds_param.setColumn(0, "value", sValue);
        		} else {
        			this.gfn_addTranColumn(application.gds_param, "value");
        			application.gds_param.setColumn(0, "value", "");
        		}

        	} else if (sDiv == "E") {
        		application.gds_param.clear();
        		application.gds_param.copyData(this.ds_param);
        		this.ds_param.clear();
        	}

        }

        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "selectCommonCodeList") {
        // 		var bNewDraw = false;
        // 		if (this.ds_list.getRowCount() == 0) {
        // 			bNewDraw = true;
        // 			this.ds_list.clear();
        // 			this.ds_list.copyData(this.ds_codeList);
        // 			this.ds_list.clearData();
        // 			//this.fn_drawGrid
        // 		}
        // 
        // 
        // // trace(this.ds_codeList.saveXML());
        // // trace(this.ds_paramRtn.saveXML());
        // // trace(this.ds_commonRtn.saveXML());
        // 		this.ds_list.appendData(this.ds_codeList);
        // 
        // //		trace(this.ds_list.saveXML());
         		var bNewDraw = true;
        		this.fn_makeParam("E");
        		this.fn_drawGrid(bNewDraw);
        		this.gfn_setDsData(this.ds_list, this.ds_codeList);

            }
        }

        this.fn_drawGrid = function ()
        {
        	var sColPrefix = "field";
        	var arrHeadColCode = this.ds_paramRtn.getColumn(0, "COMMON_CODE_FIELD");
        	arrHeadColCode = String(arrHeadColCode).split(";");

        	var sGrdFormat = "";
        	sGrdFormat += '<Formats>\n';
        	sGrdFormat += '<Format id="default">\n';
        	sGrdFormat += '</Format>\n';
        	sGrdFormat += '</Formats>\n';

        	this.grd_list.set_formats(sGrdFormat);

        	this.grd_list.set_enableredraw(false);

        	sGrdFormat = '<Formats>\n';
        	sGrdFormat += '<Format id="default">\n';

        	sGrdFormat += '  <Columns>\n';
        	sGrdFormat += '    <Column size="150" />\n';
        	sGrdFormat += '    <Column size="280" />\n';
        	sGrdFormat += '  </Columns>\n';
        	sGrdFormat += '  <Rows>\n';

        	for (var i = 0; i < arrHeadColCode.length; i++) {
        		sGrdFormat += '    <Row size="40" />\n';
        	}

        	sGrdFormat += '  </Rows>\n';
        	sGrdFormat += '  <Band id="body">\n';

        	var sGrdBodyFormat = "";

        	for (var i = 0; i < arrHeadColCode.length; i++) {
        		var sHeadText = this.gfn_getLang(arrHeadColCode[i]);
        		var sColNm = sColPrefix + String(i + 1);

        		if (String(fv_labelCode).toUpperCase() == String(arrHeadColCode[i])) fv_rtnColNm = sColNm;

        		if (i == (arrHeadColCode.length - 1)) {
        			sGrdBodyFormat +=  '    <Cell row="' + String(i) + '" celltype="head" style="line:1&#32;solid&#32;#d5d8deff,1&#32;solid&#32;#d5d8deff,3&#32;solid&#32;#d5d8deff,1&#32;solid&#32;#d5d8deff;background:#888fa2ff;background2:#888fa2ff;color:#ffffffff;color2:#ffffffff;font:bold&#32;17&#32;Dotum;selectfont:bold&#32;17&#32;Dotum;" text="' + sHeadText + '"/>\n';
        			sGrdBodyFormat +=  '    <Cell row="' + String(i) + '" col="1" style="align:left&#32;middle;line:1&#32;solid&#32;#d5d8deff,1&#32;solid&#32;#d5d8deff,3&#32;solid&#32;#d5d8deff,1&#32;solid&#32;#d5d8deff;font:bold&#32;17&#32;Dotum;selectfont:bold&#32;17&#32;Dotum;" text="bind:' + sColNm + '"/>\n';
        		} else {
        			sGrdBodyFormat +=  '    <Cell row="' + String(i) + '" celltype="head" style="background:#888fa2ff;background2:#888fa2ff;color:#ffffffff;color2:#ffffffff;font:bold&#32;17&#32;Dotum;selectfont:bold&#32;17&#32;Dotum;" text="' + sHeadText + '"/>\n';
        			sGrdBodyFormat +=  '    <Cell row="' + String(i) + '" col="1" style="align:left&#32;middle;font:bold&#32;17&#32;Dotum;selectfont:bold&#32;17&#32;Dotum;" text="bind:' + sColNm + '"/>\n';
        		}
        	}

        	sGrdFormat += sGrdBodyFormat;

        	sGrdFormat += '  </Band>\n';
        	sGrdFormat += '</Format>\n';
        	sGrdFormat += '</Formats>\n';

        	this.grd_list.set_formats(sGrdFormat);

        	this.grd_list.set_enableredraw(true);
        }

        this.btn_closePop_onclick = function(obj,e)
        {
        	this.gfn_popupClose();
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        this.fn_setDefValue = function (objComp,sValue)
        {

        }

        this.btn_search_onclick = function(obj,e)
        {
        	this.fv_pagingTran = false;
        	this.fn_search();
        }

        this.grd_list_oncellclick = function(obj,e)
        {
        	if (e.row < 0 || e.cell < 0) return;

        	this.ds_rtn.clear();
        	this.ds_rtn.copyData(this.ds_list);
        	this.ds_rtn.clearData();
        	this.ds_rtn.addRow();
        	this.ds_rtn.copyRow(0, this.ds_list, e.row);

        	var objRtn = new Object();
        	objRtn.rtnDs = this.ds_rtn;
        	objRtn.key = fv_key;
        	objRtn.compPos = fv_compPos;
        	objRtn.valueColNm = fv_rtnColNm;

        	this.gfn_popupClose(objRtn);
        }
        this.edt_searchText_onkeydown = function(obj,e)
        {
        	 if (e.keycode == 13){
        	 this.fn_search();
        	 }
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.grd_list.addEventHandler("oncellclick", this.grd_list_oncellclick, this);
            this.edt_searchText.addEventHandler("onkeydown", this.edt_searchText_onkeydown, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.btn_closePopup.addEventHandler("onclick", this.btn_closePop_onclick, this);

        };

        this.loadIncludeScript("CommCodePop.xfdl", true);

       
    };
}
)();
