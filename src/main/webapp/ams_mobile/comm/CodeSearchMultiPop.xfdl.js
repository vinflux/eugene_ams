﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CodeSearchMultiPop");
                this.set_classname("style01");
                this.set_titletext("코드선택 멀티");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,700,547);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_code", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_search", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/><Column id=\"field3\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_choice", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grdInit", this);
            obj._setContents("<ColumnInfo><Column id=\"grd_codeInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_codeSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_codeIndex\" type=\"STRING\" size=\"256\"/><Column id=\"grd_choiceInit\" type=\"STRING\" size=\"4000\"/><Column id=\"grd_choiceSize\" type=\"STRING\" size=\"256\"/><Column id=\"grd_choiceIndex\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"grd_codeIndex\"/><Col id=\"grd_codeSize\"/><Col id=\"grd_codeInit\"/><Col id=\"grd_choiceInit\"/><Col id=\"grd_choiceSize\"/><Col id=\"grd_choiceIndex\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("검색 (멀티 선택)");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "55", "656", "44", null, null, this);
            obj.set_taborder("117");
            obj.style.set_border("3 solid #f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_search", "absolute", "33", "66", null, "22", "97", null, this);
            obj.set_taborder("119");
            this.addChild(obj.name, obj);

            obj = new Button("btn_select", "absolute", null, "104", "42", "24", "22", null, this);
            obj.set_taborder("124");
            obj.set_text("선택");
            obj.set_tooltiptext("선택");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_code", "absolute", "22", "133", null, null, "22", "246", this);
            obj.set_taborder("126");
            obj.set_binddataset("ds_code");
            obj.set_cellmovingtype("col,band");
            obj.set_cellsizingtype("col");
            obj.set_autofittype("none");
            obj.set_summarytype("default");
            obj.set_fillareatype("none");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Div("div_Paging", "absolute", "22", null, null, "29", "22", "217", this);
            obj.set_taborder("129");
            obj.set_scrollbars("none");
            obj.set_url("comm::PopupPaging.xfdl");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "609", "66", "59", "22", null, null, this);
            obj.set_taborder("130");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_text(" 검색");
            this.addChild(obj.name, obj);

            obj = new Button("btn_delete", "absolute", null, "498", "52", "28", "352", null, this);
            obj.set_taborder("131");
            obj.set_text("삭제");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_choice", "absolute", "22", "340", null, "143", "22", null, this);
            obj.set_taborder("132");
            obj.set_binddataset("ds_choice");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autofittype("none");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("btn_complete", "absolute", null, "498", "52", "28", "296", null, this);
            obj.set_taborder("133");
            obj.set_text("완료");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 700, 547, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("코드선택 멀티");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_select","text","gds_lang","SELECT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","btn_delete","text","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_complete","text","gds_lang","COMPLETE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::PopupPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("CodeSearchMultiPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CodeSearchMultiPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CodeSearchMultiPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 코드 선택 멀티
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;
        this.gv_edtValue;
        this.gv_colId;
        this.gv_headClick = "";

        this.gv_main = "Main";
        this.gv_middle = "Middle";
        this.gv_detral = "Detail";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.div_Paging.gv_flag = this.gv_main;
        	
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	
        	this.gv_searchId = this.gfn_isNullEmpty(this.parent.searchId);
        	this.gv_divId = this.gfn_isNullEmpty(this.parent.divId);
        	
        	this.fn_search();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_code.set_keystring("");
        	this.ds_code.clearData();
        	this.ds_choice.set_keystring("");
        	this.ds_choice.clearData();
        	
        	this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectCommonCode");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_search=OUT_COMMON_CODE_LIST ds_param=OUT_PARAM";
            var sParam   = "searchid="+this.gv_searchId
                         +" value="+this.gfn_isNullEmpty(this.edt_search.value)
                         +" OWKEY_SELECTION_VALUE="+this.gfn_isNullEmpty(this.gfn_getUserInfo("owkeym"))
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_code.rowcount == 0){
        			this.grd_code.set_nodatatext(application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage"));
        		}
        		
        		if(this.gv_headClick == ""){
        			var nCol = 0;
        			
        			this.gv_colId = this.ds_param.getColumn(0, "COMMON_CODE_FIELD").split(";");
        			var bSuccDs = true;
        			
        			while(bSuccDs){
        				bSuccDs = this.ds_code.deleteColumn(1);
        				bSuccDs = this.ds_choice.deleteColumn(1);
        			}
        			
        			for(var i = 0 ; i < this.gv_colId.length ; i++){
        				this.ds_code.addColumn(this.gv_colId[i]);
        				this.ds_choice.addColumn(this.gv_colId[i]);
        				
        				nCol = this.grd_code.appendContentsCol();
        				this.grd_code.setCellProperty("head", nCol, "text", this.gv_colId[i]);
        				this.grd_code.setFormatColProperty(nCol, "size", 200);
        				this.grd_code.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i]);
        				this.grd_code.setCellProperty("body", nCol, "align", "left");
        				this.grd_code.setCellProperty("body", nCol, "padding", "0px 2px 0px 2px");
        				
        				nCol = this.grd_choice.appendContentsCol();
        				this.grd_choice.setCellProperty("head", nCol, "text", this.gv_colId[i]);
        				this.grd_choice.setFormatColProperty(nCol, "size", 200);
        				this.grd_choice.setCellProperty("body", nCol, "text", "bind:" + this.gv_colId[i]);
        				this.grd_choice.setCellProperty("body", nCol, "align", "left");
        				this.grd_choice.setCellProperty("body", nCol, "padding", "0px 2px 0px 2px");
        			}
        			
        			if(this.gv_colId.length > 3){
        				this.grd_code.set_autofittype("none");
        				this.grd_choice.set_autofittype("none");
        			}else{
        				this.grd_code.set_autofittype("col");
        				this.grd_choice.set_autofittype("col");
        			}
        			
        			this.grd_code.setFormatColProperty(0, "size", 25);
        			this.grd_choice.setFormatColProperty(0, "size", 25);
        			
        			this.gfn_gridHeadSet(this.grd_code);
        			this.gfn_gridHeadSet(this.grd_choice);
        		}
        		
        		this.gv_headClick = "";
        		
        		if(this.ds_search.rowcount > 0){
        			for(var i = 0 ; i < this.ds_search.rowcount ; i++){
        				this.ds_code.addRow();
        				
        				for(var j = 0 ; j < this.gv_colId.length ; j++){
        					if(j == 2) this.ds_code.setColumn(i, this.gv_colId[j]  , this.ds_search.getColumn(i, "field"+(j+1)+"_desc"));
        					else this.ds_code.setColumn(i, this.gv_colId[j]  , this.ds_search.getColumn(i, "field"+(j+1)));
        				}
        			}
        			
        			this.ds_code.set_rowposition(0);
        		}
        		
        		this.gfn_setParam( "sortValue", "");
        		this.gfn_setParam( "colName", "");
        		
        		this.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        		
        		// 그리드 초기 값 셋팅
        		var sSize = "";
        		var sIndex = "";
        		
        		this.ds_grdInit.setColumn(0, "grd_codeInit", this.grd_code.getFormatString());
        		this.ds_grdInit.setColumn(0, "grd_choiceInit", this.grd_choice.getFormatString());
        		
        		for(var i = 0 ; i < this.grd_code.getCellCount("head") ; i++){
        			if(i == 0){
        				sSize = this.grd_code.getRealColSize(i);
        				sIndex = this.grd_code.getCellText(-1, i);
        			}else{
        				sSize += "|" + this.grd_code.getRealColSize(i);
        				sIndex += "|" + this.grd_code.getCellText(-1, i);
        			}
        		}
        		
        		this.ds_grdInit.setColumn(0, "grd_codeSize", sSize);
        		this.ds_grdInit.setColumn(0, "grd_codeIndex", sIndex);
        		
        		for(var i = 0 ; i < this.grd_choice.getCellCount("head") ; i++){
        			if(i == 0){
        				sSize = this.grd_choice.getRealColSize(i);
        				sIndex = this.grd_choice.getCellText(-1, i);
        			}else{
        				sSize += "|" + this.grd_choice.getRealColSize(i);
        				sIndex += "|" + this.grd_choice.getCellText(-1, i);
        			}
        		}
        		
        		this.ds_grdInit.setColumn(0, "grd_choiceSize", sSize);
        		this.ds_grdInit.setColumn(0, "grd_choiceIndex", sIndex);
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* pageSearch 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */
        this.fn_mainSearch = function(currentPage,pagingLimit,count)
        {
        	this.gfn_setParam("currentPage", currentPage);
        	this.gfn_setParam("pagingLimit", pagingLimit);
        	this.gfn_setParam("COUNT", count);
        	
        	this.fn_search();
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_search_onclick 실행 */
        this.btn_search_onclick = function(obj,e)
        {
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	
        	this.fn_search();
        }

        /* btn_select_onclick 실행 */
        this.btn_select_onclick = function(obj,e)
        {
        	var nRow = this.ds_code.findRowExpr("CHK == '1'");
        	
        	while(nRow != -1){
        		this.ds_choice.addRow();
        		
        		for(var j = 0 ; j < this.gv_colId.length ; j++){
        			this.ds_choice.setColumn(this.ds_choice.rowposition, this.gv_colId[j]  , this.ds_code.getColumn(nRow, this.gv_colId[j]));
        		}
        		
        		nRow = this.ds_code.findRowExpr("CHK == '1'", nRow+1);
        	}
        }

        /* grd_code_onheadclick 실행 */
        this.grd_code_onheadclick = function(obj,e)
        {
        	this.gv_headClick = "Y";
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	for(var j = 0 ; j < this.gv_colId.length ; j++){
        		if(colName == this.gv_colId[j] && j != 2) colName = "field"+(j+1);
        		else if(colName == this.gv_colId[j] && j == 2) colName = "field"+(j+1)+"_desc";
        	}
        	
        	if(colName != "CHK" && this.ds_code.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.fn_search();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* grd_code_onkeydown 실행 */
        this.grd_code_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_code, obj.currentcell);
        }

        /* grd_code_onrbuttonup 실행 */
        this.grd_code_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|Copy|Paste|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "1|1|1|1|0|0|0|0|1|0";
        	this.gfn_openGridMenu(obj, e,enable_list);
        }

        /* grd_choice_onkeydown 실행 */
        this.grd_choice_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_choice, obj.currentcell);
        }

        /* grd_choice_onrbuttonup 실행 */
        this.grd_choice_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|Copy|Paste|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "1|1|1|1|0|0|0|0|1|0";
        	this.gfn_openGridMenu(obj, e,enable_list);
        }

        /* grd_code_oncellclick 실행 */
        this.grd_code_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_code.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_code.setColumn(e.row, "CHK", "");
        		else this.ds_code.setColumn(e.row, "CHK", "1");
        	}
        }

        /* grd_choice_oncellclick 실행 */
        this.grd_choice_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_choice.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_choice.setColumn(e.row, "CHK", "");
        		else this.ds_choice.setColumn(e.row, "CHK", "1");
        	}
        }

        /* grd_choice_onheadclick 실행 */
        this.grd_choice_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* btn_complete_onclick 실행 */
        this.btn_complete_onclick = function(obj,e)
        {
        	if(this.ds_choice.rowcount == 0){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return;
        	}
        	
        	var allValue = "";
        	
        	for(var i = 0 ; i < this.ds_choice.rowcount ; i++){
        		if(i == 0){
        			allValue = this.ds_choice.getColumn(i, this.gv_colId[0]);
        		}else{
        			allValue = allValue+","+this.ds_choice.getColumn(i, this.gv_colId[0]);
        		}
        	}
        	
        	var edtObj = eval("this.opener.div_seachCond." + this.gv_divId);
        	edtObj.edt_value.set_value(allValue);
        	this.close();
        }

        /* btn_delete_onclick 실행 */
        this.btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_choice.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return;
        	}
        	
        	while(nRow != -1){
        		this.ds_choice.deleteRow(nRow);
        		nRow = this.ds_choice.findRowExpr("CHK == '1'", nRow);
        	}
        }

        /* grd_code_oncelldblclick 실행 */
        this.grd_code_oncelldblclick = function(obj,e)
        {
        	var edtObj = eval("this.opener.div_seachCond." + this.gv_divId);
        	edtObj.edt_value.set_value(this.ds_code.getColumn(this.ds_code.rowposition, this.gv_colId[0]));
        	this.close();
        }

        /* btn_closeAll_onclick 실행 */
        this.btn_closeAll_onclick = function(obj,e)
        {
        	this.close();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.btn_select.addEventHandler("onclick", this.btn_select_onclick, this);
            this.grd_code.addEventHandler("onheadclick", this.grd_code_onheadclick, this);
            this.grd_code.addEventHandler("onkeydown", this.grd_code_onkeydown, this);
            this.grd_code.addEventHandler("onrbuttonup", this.grd_code_onrbuttonup, this);
            this.grd_code.addEventHandler("oncellclick", this.grd_code_oncellclick, this);
            this.grd_code.addEventHandler("oncelldblclick", this.grd_code_oncelldblclick, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.btn_delete.addEventHandler("onclick", this.btn_delete_onclick, this);
            this.grd_choice.addEventHandler("onkeydown", this.grd_choice_onkeydown, this);
            this.grd_choice.addEventHandler("onrbuttonup", this.grd_choice_onrbuttonup, this);
            this.grd_choice.addEventHandler("oncellclick", this.grd_choice_oncellclick, this);
            this.grd_choice.addEventHandler("onheadclick", this.grd_choice_onheadclick, this);
            this.btn_complete.addEventHandler("onclick", this.btn_complete_onclick, this);

        };

        this.loadIncludeScript("CodeSearchMultiPop.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
