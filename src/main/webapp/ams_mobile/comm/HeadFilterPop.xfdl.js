﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("HeadFilterPop");
                this.set_classname("Guide11");
                this.set_titletext("헤더 필터");
                this._setFormPosition(0,0,250,120);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_filter", this);
            obj._setContents("<ColumnInfo><Column id=\"ID\" type=\"STRING\" size=\"255\"/><Column id=\"SIZE\" type=\"STRING\" size=\"255\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"IDX\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_close", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "22", "14", "130", "20", null, null, this);
            obj.set_taborder("1");
            obj.set_text("Head Filter");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_filter", "absolute", "22", "52", null, null, "22", "23", this);
            obj.set_taborder("52");
            obj.set_binddataset("ds_filter");
            obj.set_autoenter("none");
            obj.set_selecttype("row");
            obj.set_autofittype("col");
            obj.set_nodatatext("no data found");
            obj.set_treeusecheckbox("false");
            obj.set_treeinitstatus("collapse,null");
            obj.style.set_font("9 arial");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/><Column size=\"180\"/></Columns><Rows><Row size=\"23\"/></Rows><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"align:center;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:ID\" editdisplay=\"edit\" tooltiptext=\"bind:ID\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("53");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 250, 120, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Guide11");
            		p.set_titletext("헤더 필터");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("HeadFilterPop.xfdl", "lib::Comm.xjs");
        this.registerScript("HeadFilterPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : HeadFilterPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Filter 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_filterGridP;
        this.gv_index;
        this.gv_size;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_filterGridP = this.gfn_isNullEmpty(this.parent.argFilterGrid);
            this.gv_index = this.gfn_isNullEmpty(this.parent.argIndex);
            this.gv_size = this.gfn_isNullEmpty(this.parent.argSize);
            
            this.fn_filter();
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* 데이타셋 초기화
        * @param
        * @return
        */
        this.fn_filter = function()
        {
        	var nRow = 0;
        	var sHeadText = "";
        	
        	for(var i = 0 ; i < this.gv_filterGridP.getCellCount("head") ; i++){
        		if(this.gfn_isNull(this.gv_filterGridP.getCellText(-1, i)) || this.gv_filterGridP.getCellText(-1, i) == "CHK" || this.gv_filterGridP.getCellText(-1, i) == "NO"){
        			continue;
        		}
        		
        		nRow = this.ds_filter.addRow();
        		
        		if(this.gv_filterGridP.getRealColSize(i) > 0) this.ds_filter.setColumn(nRow, "CHK", "1");
        		else this.ds_filter.setColumn(nRow, "CHK", "0");
        		
        		sHeadText = this.gv_filterGridP.getCellText(-1,i);
        		if(sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark || sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark){
        			sHeadText = sHeadText.substr(0, sHeadText.length - 1);
        		}
        		
        		this.ds_filter.setColumn(nRow, "SIZE", this.gv_filterGridP.getRealColSize(i));
        		this.ds_filter.setColumn(nRow, "ID"  , sHeadText);
        		this.ds_filter.setColumn(nRow, "IDX" , i);
        	}
        	
        	this.ds_filter.set_rowposition(0);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        /* btn_close_onclick 실행 */
        this.grd_filter_oncellclick = function(obj,e)
        {
        	var colId = "";
        	var nSize = 0;
        	
        	if(e.col == 1){
        		if(this.ds_filter.getColumn(e.row, "CHK") == 0 || this.gfn_isNull(this.ds_filter.getColumn(e.row, "CHK"))) this.ds_filter.setColumn(e.row, "CHK", 1);
        		else this.ds_filter.setColumn(e.row, "CHK", 0);
        	}
        	
        	if(this.ds_filter.getColumn(e.row,"CHK") == 0){
        		this.gv_filterGridP.setFormatColProperty(this.ds_filter.getColumn(e.row,"IDX"), "size", -1);
        	}else{
        		for(var i = 0 ; i < this.gv_index.length ; i++){
        			if(this.gv_index[i] == this.ds_filter.getColumn(e.row, "ID")){
        				nSize = this.gv_size[i];
        				break;
        			}
        		}
        		
        		this.gv_filterGridP.setFormatColProperty(this.ds_filter.getColumn(e.row,"IDX"), "size", nSize);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.grd_filter.addEventHandler("oncellclick", this.grd_filter_oncellclick, this);

        };

        this.loadIncludeScript("HeadFilterPop.xfdl", true);

       
    };
}
)();
