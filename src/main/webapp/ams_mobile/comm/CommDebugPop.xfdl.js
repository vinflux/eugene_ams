﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CommDebugPop");
                this.set_classname("CommDebug_p");
                this.set_titletext("디비그 팝업");
                this._setFormPosition(0,0,800,600);
            }
            this.style.set_background("#ffffffff");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj._setContents("<ColumnInfo><Column id=\"PAR_NAME\" type=\"STRING\" size=\"256\"/><Column id=\"DS_NAME\" type=\"STRING\" size=\"256\"/><Column id=\"DS_COUNT\" type=\"BIGDECIMAL\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_data", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_bg", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("4");
            obj.set_cssclass("popupBg");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_list", "absolute", "10", "10", null, "200", "10", null, this);
            obj.set_taborder("0");
            obj.set_binddataset("ds_list");
            obj.set_autofittype("col");
            obj.set_cellsizingtype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"294\"/><Column size=\"267\"/><Column size=\"222\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"Form\"/><Cell col=\"1\" text=\"Dataset\"/><Cell col=\"2\" text=\"Row Count\"/></Band><Band id=\"body\"><Cell displaytype=\"text\" style=\"align:left;\" text=\"bind:PAR_NAME\"/><Cell col=\"1\" displaytype=\"text\" style=\"align:left;\" text=\"bind:DS_NAME\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;\" text=\"bind:DS_COUNT\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Tab("tab_data", "absolute", "10", "225", null, null, "10", "40", this);
            obj.set_taborder("1");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tpg_data", this.tab_data);
            obj.set_text("Data");
            obj.style.set_background("#ffffffff");
            this.tab_data.addChild(obj.name, obj);
            obj = new Grid("grd_data", "absolute", "10", "10", null, null, "10", "10", this.tab_data.tpg_data);
            obj.set_taborder("0");
            obj.set_binddataset("ds_data");
            obj.set_cellsizingtype("col");
            obj._setContents("<Formats></Formats>");
            this.tab_data.tpg_data.addChild(obj.name, obj);
            obj = new Tabpage("tpg_xml", this.tab_data);
            obj.set_text("XML");
            obj.style.set_background("#ffffffff");
            this.tab_data.addChild(obj.name, obj);
            obj = new TextArea("txt_xml", "absolute", "10", "40", null, null, "10", "10", this.tab_data.tpg_xml);
            obj.set_taborder("0");
            obj.style.set_color("#000000ff");
            this.tab_data.tpg_xml.addChild(obj.name, obj);
            obj = new Button("btn_upmode", "absolute", "693", "10", "77", "21", null, null, this.tab_data.tpg_xml);
            obj.set_taborder("1");
            obj.set_text("수정정보");
            this.tab_data.tpg_xml.addChild(obj.name, obj);
            obj = new Button("btn_nomode", "absolute", "650", "10", "41", "21", null, null, this.tab_data.tpg_xml);
            obj.set_taborder("2");
            obj.set_text("일반");
            this.tab_data.tpg_xml.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "380", "570", "41", "21", null, null, this);
            obj.set_taborder("2");
            obj.set_text("닫기");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tab_data.tpg_data,
            	//-- Layout function
            	function(p) {
            		p.set_text("Data");
            		p.style.set_background("#ffffffff");

            	}
            );
            this.tab_data.tpg_data.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 150, 72, this.tab_data.tpg_xml,
            	//-- Layout function
            	function(p) {
            		p.set_text("XML");
            		p.style.set_background("#ffffffff");

            	}
            );
            this.tab_data.tpg_xml.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 800, 600, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CommDebug_p");
            		p.set_titletext("디비그 팝업");
            		p.style.set_background("#ffffffff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CommDebugPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CommDebugPop.xfdl", function(exports) {
        /**********************************************************************************
         * Program ID    : CommDebugPop.xfdl
         * Program Name  : 디비그 팝업
         * Author        : 김명성
         * Created On    : 2016.05.01
         * Desc          : 
        ***********************************************************************************
         *     수정일      이  름     사유 
        ***********************************************************************************
         *    2016.05.01    김명성    최초 작성 
        ***********************************************************************************/

        /*******************************************************************************
            1. 공통 라이브러리 INCLUDE 영역
        ********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /*******************************************************************************
            2. FORM 변수 선언 영역
        ********************************************************************************/

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/

        this.form_onload = function(obj,e)
        {
            this.fn_setData();
        }

        /*******************************************************************************
            4. TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
            5. Transaction Callback 처리부분
        ********************************************************************************/

        /*******************************************************************************
            6. 개발자 FUNCTION 영역
        ********************************************************************************/

        this.fn_setData = function()
        {
            this.ds_list.clearData();
            var oDatas;
            var nRow;
            
            if(this.opener.name == "WorkForm"){
        		oDatas = this.opener.div_work.objects;

        		for(var i=0; i<oDatas.length; i++) {
        			if(oDatas[i] instanceof Dataset) {
        				nRow = this.ds_list.addRow();
        				this.ds_list.setColumn(nRow, "PAR_NAME", this.opener.div_work.name);
        				this.ds_list.setColumn(nRow, "DS_NAME", oDatas[i].name);
        				this.ds_list.setColumn(nRow, "DS_COUNT", oDatas[i].getRowCount());
        			}
        		}
        		
        		oDatas = this.opener.div_cond.objects;

        		for(var i=0; i<oDatas.length; i++) {
        			if(oDatas[i] instanceof Dataset) {
        				nRow = this.ds_list.addRow();
        				this.ds_list.setColumn(nRow, "PAR_NAME", this.opener.div_cond.name);
        				this.ds_list.setColumn(nRow, "DS_NAME", oDatas[i].name);
        				this.ds_list.setColumn(nRow, "DS_COUNT", oDatas[i].getRowCount());
        			}
        		}
            }else{
        		oDatas = this.opener.objects;

        		for(var i=0; i<oDatas.length; i++) {
        			if(oDatas[i] instanceof Dataset) {
        				nRow = this.ds_list.addRow();
        				this.ds_list.setColumn(nRow, "PAR_NAME", this.opener.name);
        				this.ds_list.setColumn(nRow, "DS_NAME", oDatas[i].name);
        				this.ds_list.setColumn(nRow, "DS_COUNT", oDatas[i].getRowCount());
        			}
        		}
            }
            
            this.ds_list.applyChange();
            this.ds_list.set_rowposition(0);
        }

        /*******************************************************************************
            7. 각 COMPONENT 별 EVENT 영역
        ********************************************************************************/
        this.grd_list_oncelldblclick = function(obj,e)
        {
            var oData;
            
            if(this.opener.name == "WorkForm"){
        		if(this.ds_list.getColumn(e.row, "PAR_NAME") == "div_work")oData = this.opener.div_work.objects[this.ds_list.getColumn(e.row, "DS_NAME")];
        		else if(this.ds_list.getColumn(e.row, "PAR_NAME") == "div_cond") oData = this.opener.div_cond.objects[this.ds_list.getColumn(e.row, "DS_NAME")];
        	}else{
        		oData = this.opener.objects[this.ds_list.getColumn(e.row, "DS_NAME")];
        	}
            
            this.ds_data.clearData();
            
            if(oData != null) {
                this.ds_data.copyData(oData);
                this.tab_data.tpg_data.grd_data.createFormat();
                this.tab_data.tpg_xml.txt_xml.set_value(oData.saveXML());
            }
        }

        this.tab_data_tpg_xml_btn_nomode_onclick = function(obj,e)
        {
            this.tab_data.tpg_xml.txt_xml.set_value(this.ds_data.saveXML());
        }

        this.tab_data_tpg_xml_btn_upmode_onclick = function(obj,e)
        {
            var oData = this.opener.objects[this.ds_list.getColumn(this.ds_list.rowposition, "DS_NAME")];
            
            if(oData != null) {
                trace(oData.name);
                this.tab_data.tpg_xml.txt_xml.set_value(oData.saveXML("ds_data", "U"));
            }
        }

        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        this.Grid_onkeydown = function(obj,e)
        {
            this.gfn_GridCopy(obj, e);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.grd_list.addEventHandler("oncelldblclick", this.grd_list_oncelldblclick, this);
            this.tab_data.tpg_data.grd_data.addEventHandler("onkeydown", this.Grid_onkeydown, this);
            this.tab_data.tpg_xml.btn_upmode.addEventHandler("onclick", this.tab_data_tpg_xml_btn_upmode_onclick, this);
            this.tab_data.tpg_xml.btn_nomode.addEventHandler("onclick", this.tab_data_tpg_xml_btn_nomode_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("CommDebugPop.xfdl", true);

       
    };
}
)();
