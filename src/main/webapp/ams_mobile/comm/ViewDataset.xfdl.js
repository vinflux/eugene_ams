﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MainMenu");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,989);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_data", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_datasetList", this);
            obj._setContents("<ColumnInfo><Column id=\"winId\" type=\"STRING\" size=\"256\"/><Column id=\"dsName\" type=\"STRING\" size=\"256\"/><Column id=\"owner\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_formList", this);
            obj._setContents("<ColumnInfo><Column id=\"IDX\" type=\"INT\" size=\"256\"/><Column id=\"FORM_OBJ\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("grd_data", "absolute", "148", "143", null, "361", "9", null, this);
            obj.set_taborder("0");
            obj.set_binddataset("ds_data");
            obj.set_cellsizingtype("col");
            obj.style.set_font("antialias 9 Dotum");
            obj._setContents("<Formats></Formats>");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00", "absolute", "9", "143", "134", null, null, "0", this);
            obj.set_taborder("1");
            obj.set_binddataset("ds_datasetList");
            obj.set_cellsizingtype("col");
            obj.style.set_font("antialias 12 Dotum");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"0\"/><Column size=\"122\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"winId\"/><Cell col=\"1\" style=\"font:antialias 9 Dotum;selectfont:antialias 9 Dotum;\" text=\"dsName\"/></Band><Band id=\"body\"><Cell text=\"bind:winId\"/><Cell col=\"1\" style=\"font:antialias 9 Dotum;selectfont:antialias 9 Dotum;\" text=\"bind:dsName\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_dsXml", "absolute", "148", "511", null, null, "9", "0", this);
            obj.set_taborder("2");
            obj.set_readonly("true");
            obj.style.set_font("antialias 9 Dotum");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_forms", "absolute", "180", "7", null, "40", "65", null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_formList");
            obj.set_codecolumn("IDX");
            obj.set_datacolumn("FORM_OBJ");
            obj.style.set_font("antialias 12 Dotum");

            obj = new Static("stc_label01", "absolute", "9", "7", "166", "39", null, null, this);
            obj.set_taborder("4");
            obj.set_text("Form");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, "7", "50", "40", "9", null, this);
            obj.set_taborder("5");
            obj.set_text("Close");
            obj.style.set_font("antialias bold 12 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_label00", "absolute", "9", "52", "166", "39", null, null, this);
            obj.set_taborder("7");
            obj.set_text("KeyString");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_keyString", "absolute", "180", "52", null, "40", "65", null, this);
            obj.set_taborder("8");
            obj.style.set_font("antialias 12 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_applyKeyString", "absolute", null, "52", "50", "40", "9", null, this);
            obj.set_taborder("9");
            obj.set_text("Apply");
            obj.style.set_font("antialias bold 12 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_label02", "absolute", "9", "100", "166", "39", null, null, this);
            obj.set_taborder("10");
            obj.set_text("Filter");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_filter", "absolute", "180", "100", null, "40", "65", null, this);
            obj.set_taborder("11");
            obj.style.set_font("antialias 12 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_applyFilter", "absolute", null, "100", "50", "40", "9", null, this);
            obj.set_taborder("12");
            obj.set_text("Apply");
            obj.style.set_font("antialias bold 12 Dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 989, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ViewDataset.xfdl", "lib::Comm.xjs");
        this.registerScript("ViewDataset.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        var fv_devFlag = true;
        var fv_globalIdx = 9999;
        var fv_arrForms = new Array();

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        	this.fn_createFormList();
        }

        this.fn_createFormList = function ()
        {
        	fv_arrForms = new Array();

        	this.ds_formList.clearData();

        	var nRow = this.ds_formList.addRow();
        	this.ds_formList.setColumn(nRow, "IDX", fv_globalIdx);
        	this.ds_formList.setColumn(nRow, "FORM_OBJ", "application");

        	fv_arrForms[fv_globalIdx] = application.all;

        	var nRow = this.ds_formList.addRow();
        	this.ds_formList.setColumn(nRow, "IDX", 0);
        	this.ds_formList.setColumn(nRow, "FORM_OBJ", "CF_HIDDEN");

        	fv_arrForms[0] = application.gv_CF_HIDDEN.form.objects;

        	var nRow = this.ds_formList.addRow();
        	this.ds_formList.setColumn(nRow, "IDX", 1);
        	this.ds_formList.setColumn(nRow, "FORM_OBJ", "CF_MAIN");

        	fv_arrForms[1] = application.gv_CF_MAIN.form.objects;

        	var nRow = this.ds_formList.addRow();
        	this.ds_formList.setColumn(nRow, "IDX", 2);
        	this.ds_formList.setColumn(nRow, "FORM_OBJ", "CF_MAIN.div_sork");

        	fv_arrForms[2] = application.gv_CF_MAIN.form.div_work.objects;

        	this.cbo_forms.set_enableevent(false);
        	this.cbo_forms.set_index(0);
        	this.cbo_forms.set_enableevent(true);

        	this.fn_getDsList(fv_globalIdx);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        
        this.fn_getDsList = function (nIdx)
        {
        	var sGrdFormatStr = '<Formats><Format id="default"></Format></Formats>';
        	this.grd_data.set_formats(sGrdFormatStr);
        	this.txa_dsXml.set_value("");
        	this.ds_data.clear();

        	this.ds_datasetList.clearData();
        	this.ds_datasetList.set_enableevent(false);
        	var objDsList = fv_arrForms[nIdx];

        // 	if (nIdx == fv_globalIdx) {
        // 		objDsList = application.all;
        // 	} else {
        // 		var objCF = application.mainframe.all[nIdx];trace("objCF : " + objCF);
        // 		if (!this.gfn_isNull(objCF)) {
        // 			var objForm = objCF.form;trace("objForm : " + objForm);
        // 			if (!this.gfn_isNull(objForm)) {
        // 				objDsList = objForm.objects;trace("objDsList : " + objDsList);
        // 			}
        // 		}
        // 	}

        	for (var i = 0; i < objDsList.length; i++) {
        		if (objDsList[i] instanceof Dataset) {
        			var nRow = this.ds_datasetList.addRow();
        			this.ds_datasetList.setColumn(nRow, "winId", nRow);
        			this.ds_datasetList.setColumn(nRow, "dsName", objDsList[i].name);
        		}
        	}

        	this.ds_datasetList.set_rowposition(0);
        	this.ds_datasetList.set_enableevent(true);

        	this.fn_createDsContnets(0);
        }

        this.fn_createDsContnets = function (nIdx)
        {
         	this.grd_data.set_enableredraw(false);

        	var arrRowStatus = new Array("EMPTY", "NORMAL", "INSERT", "", "UPDATE", "", "", "", "DELETE",  "", "", "", "", "", "", "", "GROUP");
        	var nWinId = this.ds_datasetList.getColumn(nIdx, "winId");

        	if (this.gfn_isNull(nWinId)) return;

        	var sDsName = this.ds_datasetList.getColumn(nIdx, "dsName");
        	var objList;
        	var objDs;

        	objList = fv_arrForms[this.cbo_forms.value];
        	objDs = objList[sDsName];

        	var sGrdFormatStr = '<Formats><Format id="default"></Format></Formats>';
        	this.grd_data.set_formats(sGrdFormatStr);
        	this.txa_dsXml.set_value("");
        	this.ds_data.clear();

        	if (objDs instanceof Dataset) {
        		this.ds_data.copyData(objDs);

        		this.ds_data.addColumn("rowType", "STRING", 255);

        		var sFilterStr = objDs.filterstr;

        		if (this.gfn_isNull(sFilterStr)) sFilterStr = "";

        		objDs.filter("");

        		for (var k = 0; k < objDs.getRowCount(); k++) {
        			var sRowTypeNm = arrRowStatus[objDs.getRowType(k)];
        			this.ds_data.setColumn(k, "rowType", sRowTypeNm);
        		}

        		objDs.filter(sFilterStr);

        		this.ds_data.filter(sFilterStr);
        		this.edt_filter.set_value(sFilterStr);

        		var sKeystring = objDs.keystring;

        		if (this.gfn_isNull(sKeystring)) sKeystring = "";

        		this.ds_data.set_keystring(sKeystring);
        		this.edt_keyString.set_value(sKeystring);
        		this.grd_data.createFormat(this.ds_data);

        		var nCellCnt = this.grd_data.getCellCount("body");

        		for (var i = 0; i < nCellCnt; i++) {
        			this.grd_data.setCellProperty("head", i, "font", "antialias 9 Dotum");
        			this.grd_data.setCellProperty("body", i, "font", "antialias 9 Dotum");
        			this.grd_data.setCellProperty("body", i, "selectfont", "antialias 9 Dotum");
        		}

        //  		var sFormatStr = this.grd_data.getCurFormatString();
        // trace("sFormatStr : " + sFormatStr);
        // // 		var sFormatStr = String(this.grd_data.getCurFormatString()).replace(/text=/gi, 'style="font:antialias 9 Dotum;selectfont:antialias 9 Dotum;" text=');
        // 		this.grd_data.set_formats('<Formats>' + sFormatStr + '</Formats>');

         		this.grd_data.set_enableredraw(true);

        		this.txa_dsXml.set_value(this.ds_data.saveXML());
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        this.grd_menu_oncellclick = function(obj,e)
        {
        	if (e.row < 0 || e.cell < 0) return;
        	var nTreeRow = obj.getTreeRow(e.row);
        	var nTreeStatus = obj.getTreeStatus(nTreeRow);

        	if (nTreeStatus == 0) obj.setTreeStatus(nTreeRow, true);
        	if (nTreeStatus == 1) obj.setTreeStatus(nTreeRow, false);
        }

        this.ds_datasetList_onrowposchanged = function(obj,e)
        {
        	if (e.newrow >= 0) this.fn_createDsContnets(e.newrow);
        }

        this.cbo_forms_onitemchanged = function(obj,e)
        {
        	if (e.postvalue >= 0) {
        		this.fn_getDsList(e.postvalue);
        	}
        }

        this.btn_close_onclick = function(obj,e)
        {
        	this.parent.div_tools.set_visible(false);
        }

        this.btn_applyKeyString_onclick = function(obj,e)
        {
        	var sKeystring = this.edt_keyString.value;
        	this.ds_data.set_keystring(sKeystring);
        }

        this.btn_applyFilter_onclick = function(obj,e)
        {
        	var sFilterStr = this.edt_filter.value;
        	this.ds_data.filter(sFilterStr);
        }

        this.grd_data_oncelldblclick = function(obj,e)
        {
        	if (e.row < 0 || e.cell < 0) return;

        	var sValue = obj.getCellText(e.row, e.cell);
        	if (!this.gfn_isNull(sValue)) {
        		system.setClipboard("CF_TEXT", sValue);
        		alert("[" + sValue + "] Copied.");
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_datasetList.addEventHandler("onrowposchanged", this.ds_datasetList_onrowposchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.grd_data.addEventHandler("oncelldblclick", this.grd_data_oncelldblclick, this);
            this.cbo_forms.addEventHandler("onitemchanged", this.cbo_forms_onitemchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_applyKeyString.addEventHandler("onclick", this.btn_applyKeyString_onclick, this);
            this.btn_applyFilter.addEventHandler("onclick", this.btn_applyFilter_onclick, this);

        };

        this.loadIncludeScript("ViewDataset.xfdl", true);

       
    };
}
)();
