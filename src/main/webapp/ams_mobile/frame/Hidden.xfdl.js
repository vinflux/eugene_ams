﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("frm_Hidden");
                this.set_classname("frm_Base");
                this.set_titletext("Hidden");
                this._setFormPosition(0,0,67,46);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_lang", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize

            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 67, 46, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frm_Base");
            		p.set_titletext("Hidden");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Hidden.xfdl", "lib::Comm.xjs");
        this.registerScript("Hidden.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/
        this.fv_args = new Object();

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	application.gds_openMenu.clearData();
        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        this.fn_setArgs = function (sWinId,objArgs)
        {
        	if (this.gfn_isNull(this.fv_args[sWinId])) this.fv_args[sWinId] = new Object();
        	this.fv_args[sWinId] = objArgs;
        }

        this.fn_traceArgs = function ()
        {
        	for (keys in this.fv_args) {
        		trace("this.fv_args[" + keys + "] : " + this.fv_args[keys]);
        	}
        }

        this.fn_getArgs = function (sWinId)
        {
        	return this.fv_args[sWinId];
        }

        this.fn_clearArgs = function (sWinId)
        {
        	var objDsList = this.objects;

        	for (var i = (objDsList.length - 1); i >= 0; i--) {
        		if (String(objDsList[i].name).indexOf("_" + sWinId) >= 0) this.removeChild(objDsList[i].name);
        	}

        // 	var objComps = objForm.components;
        // 
        // 	for (i = (objComps.length - 1); i >= 0; i--) {
        // 		if (String(objComps[i].name).indexOf("_" + sWinId) >= 0) objForm.removeChild(objComps[i].name);
        // 	}

        	this.fv_args[sWinId] = undefined;
        }

        this.fn_addDs = function (sWinId,objDs)
        {
        	var sDsName = objDs.id + "_" + sWinId;
        	var objRtn;

        	if (!this.gfn_isNull(this.objects[sDsName])) this.removeChild(sDsName);
        	objRtn = new Dataset();
        	objRtn.name = sDsName;
        	this.addChild(sDsName, objRtn);
        	objRtn.copyData(objDs);

        	return objRtn;
        }

        this.fn_setPushMsgContents = function ()
        {
        // 	if (application.gds_pushMsg.getRowCount() > 0) {
        // 		var arg = {userID : "01044675520"};
        // 		this.biobj.pushGetMsg(arg);
        // 	} else {trace("========================1");
        // 	}
        }

        this._on_success = function(obj,e)
        {
        //	this.fn_mobile_callback(0, e.returnvalue, e.reason);
        }

        this._on_error = function(obj,e)
        {
        //	this.fn_mobile_callback(-1, e.returnvalue, e.reason);
        }

        // 콜백
        this.fn_mobile_callback = function(nErrorCode,sMessage,nReason)
        {
        // 	// errorCode 0 이 정상 그외에는 오류
        // 	if(nErrorCode != 0) {
        // 		trace("errorCode:"+nErrorCode+"\nerrorMessage:"+sMessage);
        // 		return;
        // 	}
        // 
        // 	trace("sMessage : " + sMessage);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("ondevicebuttonup", this.frm_Base_ondevicebuttonup, this);

        };

        this.loadIncludeScript("Hidden.xfdl", true);

       
    };
}
)();
