﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide05");
                this.set_classname("CompGuide01");
                this.set_titletext("Grid#1");
                this._setFormPosition(0,0,1217,1122);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/><Column id=\"Column1\" type=\"STRING\" size=\"256\"/><Column id=\"Column2\" type=\"STRING\" size=\"256\"/><Column id=\"Column3\" type=\"STRING\" size=\"256\"/><Column id=\"Column4\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"Column0\">1</Col><Col id=\"Column1\">가</Col><Col id=\"Column2\">AAA</Col></Row><Row><Col id=\"Column0\">2</Col><Col id=\"Column1\">가나</Col><Col id=\"Column2\">BBB</Col></Row><Row><Col id=\"Column0\">3</Col><Col id=\"Column1\">가나다</Col><Col id=\"Column2\">CCC</Col></Row><Row><Col id=\"Column0\">4</Col><Col id=\"Column1\">가나다라</Col><Col id=\"Column2\">DDD</Col></Row><Row><Col id=\"Column0\">5</Col><Col id=\"Column1\">가</Col><Col id=\"Column2\">EEE</Col></Row><Row><Col id=\"Column0\">6</Col><Col id=\"Column1\">가나</Col><Col id=\"Column2\">FFF</Col></Row><Row><Col id=\"Column0\">7</Col><Col id=\"Column1\">가나다</Col><Col id=\"Column2\">GGG</Col></Row><Row><Col id=\"Column0\">8</Col><Col id=\"Column1\">가나다라</Col><Col id=\"Column2\">HHH</Col></Row><Row><Col id=\"Column0\">9</Col><Col id=\"Column1\">가</Col><Col id=\"Column2\">III</Col></Row><Row><Col id=\"Column0\">10</Col><Col id=\"Column1\">가나</Col><Col id=\"Column2\">JJJJ</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grd_tree", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"caption\" type=\"STRING\" size=\"256\"/><Column id=\"level\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"id\">1</Col><Col id=\"caption\">style</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">2</Col><Col id=\"caption\">border</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">3</Col><Col id=\"caption\">background</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">4</Col><Col id=\"caption\">gradation</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">5</Col><Col id=\"caption\">font</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">6</Col><Col id=\"caption\">Component</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">7</Col><Col id=\"caption\">TEXT</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">8</Col><Col id=\"caption\">Static</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">9</Col><Col id=\"caption\">Edit</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">10</Col><Col id=\"caption\">MaskEdit</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">11</Col><Col id=\"caption\">TextArea</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">12</Col><Col id=\"caption\">BUTTON</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">13</Col><Col id=\"caption\">Spin</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">14</Col><Col id=\"caption\">CheckBox</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">15</Col><Col id=\"caption\">LIST</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">16</Col><Col id=\"caption\">ListBox</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">17</Col><Col id=\"caption\">Combo</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">18</Col><Col id=\"caption\">Menu</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">19</Col><Col id=\"caption\">DATE</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">20</Col><Col id=\"caption\">Calendar</Col><Col id=\"level\">2</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "135", "70", "899", "474", null, null, this);
            obj.set_taborder("34");
            obj.set_binddataset("ds_grd");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"80\"/><Column size=\"142\"/><Column size=\"109\"/><Column size=\"117\"/><Column size=\"102\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"40\"/><Row size=\"50\" band=\"summ\"/></Rows><Band id=\"head\"><Cell style=\"background:#575c67ff URL('theme://images/img_required.png') left top;\" cssclass=\"headEssential\" text=\"Column0\"/><Cell col=\"1\" cssclass=\"headEssential  \" text=\"Column1\"/><Cell col=\"2\" text=\"Column2\"/><Cell col=\"3\" text=\"Column3\"/><Cell col=\"4\" text=\"Column4\"/></Band><Band id=\"body\"><Cell text=\"bind:Column0\"/><Cell col=\"1\" text=\"bind:Column1\"/><Cell col=\"2\" displaytype=\"normal\"/><Cell col=\"3\" text=\"bind:Column3\"/><Cell col=\"4\" text=\"bind:Column4\"/></Band><Band id=\"summary\"><Cell colspan=\"2\" text=\"합계\"/><Cell col=\"2\"/><Cell col=\"3\"/><Cell col=\"4\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "132", "591", "552", "25", null, null, this);
            obj.set_taborder("36");
            obj.set_text("displaytype =\"tree\" , edittype =\"tree\"");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("37");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("38");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "487", null, null, this);
            obj.set_taborder("39");
            obj.set_text("Grid#1");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "135", "35", "248", "25", null, null, this);
            obj.set_taborder("42");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "127", "624", "560", "473", null, null, this);
            obj.set_taborder("35");
            obj.set_binddataset("ds_grd_tree");
            obj.set_treeinitstatus("expand,null");
            obj.set_treeusecheckbox("false");
            obj.set_tabstop("false");
            obj.set_treeuseline("false");
            obj.set_autofittype("col");
            obj.set_treeuseexpandkey("true");
            obj.set_treeusebutton("no");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"223\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"50\"/></Rows><Band id=\"head\"><Cell text=\"caption\"/></Band><Band id=\"body\"><Cell displaytype=\"tree\" edittype=\"tree\" text=\"bind:caption\" treelevel=\"bind:level\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1217, 1122, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Grid#1");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("ontimer", this.CompGuide05_ontimer, this);
            this.Static22.addEventHandler("onclick", this.Static22_onclick, this);

        };

        this.loadIncludeScript("CompGuide08.xfdl", true);

       
    };
}
)();
