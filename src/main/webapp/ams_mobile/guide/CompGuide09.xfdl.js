﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide05");
                this.set_classname("CompGuide01");
                this.set_titletext("Grid#2");
                this._setFormPosition(0,0,1049,793);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/><Column id=\"Column1\" type=\"STRING\" size=\"256\"/><Column id=\"Column2\" type=\"STRING\" size=\"256\"/><Column id=\"Column3\" type=\"STRING\" size=\"256\"/><Column id=\"Column4\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"Column4\">1</Col><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column4\"/><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column4\"/><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column1\">20141101</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grd_cmb", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">Static</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">MaskEdit</Col></Row><Row><Col id=\"code\">3</Col><Col id=\"data\">Edit</Col></Row><Row><Col id=\"code\">4</Col><Col id=\"data\">Combo</Col></Row><Row><Col id=\"code\">5</Col><Col id=\"data\">ListBox</Col></Row><Row><Col id=\"code\">6</Col><Col id=\"data\">Button</Col></Row><Row><Col id=\"code\">7</Col><Col id=\"data\">Spin</Col></Row><Row><Col id=\"code\">8</Col><Col id=\"data\">Image</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grd02", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/><Column id=\"Column1\" type=\"STRING\" size=\"256\"/><Column id=\"Column2\" type=\"STRING\" size=\"256\"/><Column id=\"Column3\" type=\"STRING\" size=\"256\"/><Column id=\"Column4\" type=\"STRING\" size=\"256\"/><Column id=\"Column5\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"Column2\"/><Col id=\"Column4\">가나다라</Col><Col id=\"Column5\">넥사크로플랫폼</Col><Col id=\"Column1\"/></Row><Row><Col id=\"Column2\"/><Col id=\"Column4\">ABCD</Col><Col id=\"Column5\">넥사&lt;fc v='red'&gt;크로&lt;/fc&gt;플랫폼</Col></Row><Row><Col id=\"Column2\"/><Col id=\"Column5\">넥사&lt;fs v='15'&gt;크로&lt;/fs&gt;플랫폼</Col></Row><Row><Col id=\"Column5\">넥사&lt;b v='true'&gt;크로&lt;/b&gt;플랫폼</Col></Row><Row><Col id=\"Column5\">넥사&lt;s v='true'&gt;크로&lt;/s&gt;플랫폼</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grd03", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/><Column id=\"Column1\" type=\"STRING\" size=\"256\"/><Column id=\"Column2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row/><Row/><Row/><Row/></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("Dataset00", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "135", "68", "859", "341", null, null, this);
            obj.set_taborder("34");
            obj.set_binddataset("ds_grd");
            obj.set_autofittype("col");
            obj.set_useselcolor("true");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"63\"/><Column size=\"170\"/><Column size=\"164\"/><Column size=\"109\"/><Column size=\"67\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"50\"/></Rows><Band id=\"head\"><Cell text=\"normal\"/><Cell col=\"1\" text=\"date\"/><Cell col=\"2\" text=\"mask\"/><Cell col=\"3\" text=\"combo\"/><Cell col=\"4\" text=\"checkbox\"/></Band><Band id=\"body\"><Cell text=\"bind:Column0\"/><Cell col=\"1\" displaytype=\"date\" edittype=\"date\" style=\"padding: ;\" text=\"bind:Column1\" calendardisplay=\"display\"/><Cell col=\"2\" edittype=\"mask\" style=\"padding: ;\" text=\"bind:Column2\" mask=\"####-##-##\" editdisplay=\"display\"/><Cell col=\"3\" displaytype=\"combo\" edittype=\"combo\" style=\"padding: ;\" text=\"bind:Column3\" combodataset=\"ds_grd_cmb\" combocodecol=\"code\" combodatacol=\"data\" combodisplay=\"display\"/><Cell col=\"4\" displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:Column4\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "132", "430", "859", "338", null, null, this);
            obj.set_taborder("35");
            obj.set_binddataset("ds_grd02");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"66\"/><Column size=\"90\"/><Column size=\"53\"/><Column size=\"80\"/><Column size=\"98\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"50\"/></Rows><Band id=\"head\"><Cell text=\"Button\"/><Cell col=\"1\" edittype=\"none\" text=\"TextArea\" editdisplay=\"edit\"/><Cell col=\"2\" text=\"expand\"/><Cell col=\"3\" edittype=\"text\" text=\"text\"/><Cell col=\"4\" text=\"decoratetext\"/></Band><Band id=\"body\"><Cell displaytype=\"button\" edittype=\"button\" style=\"padding: ;\"/><Cell col=\"1\" edittype=\"textarea\" text=\"bind:Column1\" wordwrap=\"char\" editscrollbar=\"autovert\" editdisplay=\"display\"/><Cell col=\"2\" edittype=\"expand\" style=\"padding: ;\" text=\"bind:Column3\" expandshow=\"show\" expandsize=\"38\"/><Cell col=\"3\" edittype=\"text\" style=\"padding: ;\" text=\"bind:Column4\" editautoselect=\"true\" editdisplay=\"display\"/><Cell col=\"4\" displaytype=\"decoratetext\" style=\"padding: ;\" text=\"bind:Column5\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("38");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "487", null, null, this);
            obj.set_taborder("39");
            obj.set_text("Grid#2");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "135", "35", "397", "25", null, null, this);
            obj.set_taborder("42");
            obj.set_text("displaytype, edittype");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1049, 793, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Grid#2");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("CompGuide09.xfdl", function(exports) {

        this.btntest_onclick = function(obj,e){
        	this.Pbr00.set_pos(0);

        	this.setTimer(0, 50);		
        }

        this.CompGuide05_ontimer = function(obj,e)
        {
        	if(e.timerid == 0)
        	{
        		if(this.Pbr00.pos == 100)
        		{
        			this.killTimer(0);		
        		}
        		this.Pbr00.getNumSetter("pos").postInc();
        			
        	}	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("ontimer", this.CompGuide05_ontimer, this);
            this.Static22.addEventHandler("onclick", this.Static22_onclick, this);

        };

        this.loadIncludeScript("CompGuide09.xfdl", true);

       
    };
}
)();
