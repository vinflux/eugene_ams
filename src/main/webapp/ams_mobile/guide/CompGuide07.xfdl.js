﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide05");
                this.set_classname("CompGuide01");
                this.set_titletext("Tab#2");
                this._setFormPosition(0,0,765,600);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Tab("Tab00", "absolute", "135", "70", "579", "134", null, null, this);
            obj.set_taborder("34");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            obj.set_tabposition("top");
            obj.set_tabjustify("true");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.Tab00);
            obj.set_text("tabpage1");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.Tab00);
            obj.set_text("tabpage2");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage3", this.Tab00);
            obj.set_text("tabpage3");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage4", this.Tab00);
            obj.set_text("tabpage4");
            this.Tab00.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("35");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("36");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "269", "124", "69", null, null, this);
            obj.set_taborder("37");
            obj.set_text("Left Tab");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "135", "35", "248", "25", null, null, this);
            obj.set_taborder("38");
            obj.set_text("tabjustify = \"true\"");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "241", "765", "1", null, null, this);
            obj.set_taborder("39");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Tab("tab_userInfo", "absolute", "135", "276", null, "280", "51", null, this);
            obj.set_taborder("40");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            obj.set_cssclass("tab_LF_user");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tpg_user", this.tab_userInfo);
            obj.set_text("text1");
            obj.set_cssclass("tabp_LF_user");
            this.tab_userInfo.addChild(obj.name, obj);
            obj = new Tabpage("tpg_center", this.tab_userInfo);
            obj.set_text("text2");
            this.tab_userInfo.addChild(obj.name, obj);
            obj = new Button("btn_centerSave", "absolute", "10", "300", null, "62", "10", null, this.tab_userInfo.tpg_center);
            obj.set_taborder("6");
            obj.set_text("저장");
            obj.set_cssclass("btn_LF_save");
            this.tab_userInfo.tpg_center.addChild(obj.name, obj);
            obj = new Static("Static01", "absolute", "0", "224", "10", "105", null, null, this.tab_userInfo.tpg_center);
            obj.set_taborder("7");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_center.addChild(obj.name, obj);
            obj = new Grid("grd_WHRole", "absolute", "10", "10", null, "283", "10", null, this.tab_userInfo.tpg_center);
            obj.set_taborder("8");
            obj.set_binddataset("ds_WHRole");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"60\"/><Column size=\"270\"/><Column size=\"270\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"58\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" style=\"selectfont: ;\" text=\"센터명\"/><Cell col=\"2\" style=\"selectfont: ;\" text=\"센터코드\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:sel\"/><Cell col=\"1\" style=\"selectfont: ;\" text=\"bind:centerName\"/><Cell col=\"2\" style=\"selectfont: ;\" text=\"bind:centerKey\"/></Band></Format></Formats>");
            this.tab_userInfo.tpg_center.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "16", "0", "369", "10", null, null, this.tab_userInfo.tpg_center);
            obj.set_taborder("9");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_center.addChild(obj.name, obj);
            obj = new Static("Static02", "absolute", null, "50", "10", "291", "0", null, this.tab_userInfo.tpg_center);
            obj.set_taborder("10");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_center.addChild(obj.name, obj);
            obj = new Tabpage("tpg_ownm", this.tab_userInfo);
            obj.set_text("text3");
            this.tab_userInfo.addChild(obj.name, obj);
            obj = new Button("btn_owSave", "absolute", "10", "300", null, "62", "10", null, this.tab_userInfo.tpg_ownm);
            obj.set_taborder("5");
            obj.set_text("저장");
            obj.set_cssclass("btn_LF_save");
            this.tab_userInfo.tpg_ownm.addChild(obj.name, obj);
            obj = new Static("Static01", "absolute", "0", "266", "10", "105", null, null, this.tab_userInfo.tpg_ownm);
            obj.set_taborder("6");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_ownm.addChild(obj.name, obj);
            obj = new Static("Static02", "absolute", null, "266", "10", "105", "0", null, this.tab_userInfo.tpg_ownm);
            obj.set_taborder("7");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_ownm.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "16", "0", "369", "10", null, null, this.tab_userInfo.tpg_ownm);
            obj.set_taborder("8");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.tab_userInfo.tpg_ownm.addChild(obj.name, obj);
            obj = new Grid("grd_ownerRole", "absolute", "10", "10", null, "283", "10", null, this.tab_userInfo.tpg_ownm);
            obj.set_taborder("9");
            obj.set_binddataset("ds_ownerRole");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"60\"/><Column size=\"275\"/><Column size=\"275\"/></Columns><Rows><Row size=\"50\" band=\"head\"/><Row size=\"58\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" style=\"selectfont: ;\" text=\"화주명\"/><Cell col=\"2\" style=\"selectfont: ;\" text=\"화주코드\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:sel\"/><Cell col=\"1\" style=\"selectfont: ;\" text=\"bind:owname\"/><Cell col=\"2\" style=\"selectfont: ;\" text=\"bind:ownerKey\"/></Band></Format></Formats>");
            this.tab_userInfo.tpg_ownm.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tab_userInfo.tpg_user,
            	//-- Layout function
            	function(p) {
            		p.set_text("text1");
            		p.set_cssclass("tabp_LF_user");

            	}
            );
            this.tab_userInfo.tpg_user.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tab_userInfo.tpg_center,
            	//-- Layout function
            	function(p) {
            		p.set_text("text2");

            	}
            );
            this.tab_userInfo.tpg_center.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tab_userInfo.tpg_ownm,
            	//-- Layout function
            	function(p) {
            		p.set_text("text3");

            	}
            );
            this.tab_userInfo.tpg_ownm.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 765, 600, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Tab#2");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("CompGuide07.xfdl", function(exports) {

        this.btntest_onclick = function(obj,e){
        	this.Pbr00.set_pos(0);

        	this.setTimer(0, 50);		
        }

        this.CompGuide05_ontimer = function(obj,e)
        {
        	if(e.timerid == 0)
        	{
        		if(this.Pbr00.pos == 100)
        		{
        			this.killTimer(0);		
        		}
        		this.Pbr00.getNumSetter("pos").postInc();
        			
        	}	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("ontimer", this.CompGuide05_ontimer, this);
            this.tab_userInfo.tpg_center.btn_centerSave.addEventHandler("onclick", this.tab_userInfo_tpg_center_btn_centerSave_onclick, this);
            this.tab_userInfo.tpg_ownm.btn_owSave.addEventHandler("onclick", this.tab_userInfo_tpg_ownm_btn_owSave_onclick, this);

        };

        this.loadIncludeScript("CompGuide07.xfdl", true);

       
    };
}
)();
