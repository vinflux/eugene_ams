﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("Button, Spin, CheckBox, Radio");
                this._setFormPosition(0,0,858,763);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_radio", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">남</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">여</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Spin("Spin00", "absolute", "139", "397", "120", "50", null, null, this);
            obj.set_taborder("35");
            obj.set_value("100");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin01", "absolute", "422", "397", "120", "50", null, null, this);
            obj.set_taborder("36");
            obj.set_value("0");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("48");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("49");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "185", null, null, this);
            obj.set_taborder("50");
            obj.set_text("Button");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "363", "831", "1", null, null, this);
            obj.set_taborder("52");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "140", "37", "140", "25", null, null, this);
            obj.set_taborder("55");
            obj.set_text("Normal & Mouseover");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "140", "66", "94", "60", null, null, this);
            obj.set_taborder("56");
            obj.set_text("가나");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "140", "136", "126", "60", null, null, this);
            obj.set_taborder("58");
            obj.set_text("가나다");
            this.addChild(obj.name, obj);

            obj = new Button("Button04", "absolute", "139", "206", "158", "60", null, null, this);
            obj.set_taborder("60");
            obj.set_text("가나다라");
            this.addChild(obj.name, obj);

            obj = new Button("Button06", "absolute", "139", "276", "190", "60", null, null, this);
            obj.set_taborder("62");
            obj.set_text("가나다라마");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "677", "0", "192", "84", null, null, this);
            obj.set_taborder("64");
            obj.set_text("## 최소 2글자부터 시작하며 \r\n한 글자가 늘어날 때마다\r\n버튼 가로크기는 약 32px씩\r\n증가한다.");
            obj.style.set_linespace("3");
            obj.style.set_color("lightcoral");
            obj.style.set_font("10 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "0", "363", "124", "90", null, null, this);
            obj.set_taborder("65");
            obj.set_text("Spin");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "140", "367", "100", "25", null, null, this);
            obj.set_taborder("66");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "423", "367", "100", "25", null, null, this);
            obj.set_taborder("67");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "0", "469", "831", "1", null, null, this);
            obj.set_taborder("68");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "462", "124", "135", null, null, this);
            obj.set_taborder("69");
            obj.set_text("CheckBox");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "140", "481", "170", "25", null, null, this);
            obj.set_taborder("70");
            obj.set_text("Normal & Mouseover");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "423", "481", "100", "25", null, null, this);
            obj.set_taborder("71");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox00", "absolute", "139", "512", "227", "50", null, null, this);
            obj.set_taborder("72");
            obj.set_text("체크박스");
            obj.set_value("false");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox01", "absolute", "422", "512", "270", "50", null, null, this);
            obj.set_taborder("73");
            obj.set_text("체크박스");
            obj.set_enable("false");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "0", "636", "831", "1", null, null, this);
            obj.set_taborder("74");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "636", "124", "90", null, null, this);
            obj.set_taborder("75");
            obj.set_text("Radio");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "140", "654", "170", "25", null, null, this);
            obj.set_taborder("76");
            obj.set_text("Normal & Mouseover");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "423", "654", "100", "25", null, null, this);
            obj.set_taborder("77");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio00", "absolute", "139", "687", "280", "50", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("78");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_direction("vertical");
            obj.set_value("2");
            obj.set_index("1");

            obj = new Radio("Radio01", "absolute", "418", "687", "270", "50", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("79");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_direction("vertical");
            obj.set_enable("false");
            obj.set_value("2");
            obj.set_index("1");

            obj = new CheckBox("CheckBox02", "absolute", "139", "568", "227", "50", null, null, this);
            obj.set_taborder("83");
            obj.set_text("체크박스");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Button("Button12", "absolute", "486", "158", "222", "80", null, null, this);
            obj.set_taborder("89");
            obj.set_text("검색");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "342", "84", "80", "25", null, null, this);
            obj.set_taborder("90");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            obj.set_text("W : 94px");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "342", "152", "80", "25", null, null, this);
            obj.set_taborder("91");
            obj.set_text("W : 126px");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "342", "222", "80", "25", null, null, this);
            obj.set_taborder("92");
            obj.set_text("W : 158px");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "342", "293", "80", "25", null, null, this);
            obj.set_taborder("93");
            obj.set_text("W : 190px");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "728", "188", "80", "25", null, null, this);
            obj.set_taborder("97");
            obj.set_text("H : 80px");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin02", "absolute", "702", "397", "120", "50", null, null, this);
            obj.set_taborder("98");
            obj.set_value("0");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "703", "367", "100", "25", null, null, this);
            obj.set_taborder("99");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "490", "125", "140", "25", null, null, this);
            obj.set_taborder("100");
            obj.set_text("POPUP Button");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 Verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 858, 763, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Button, Spin, CheckBox, Radio");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("CompGuide02.xfdl", true);

       
    };
}
)();
