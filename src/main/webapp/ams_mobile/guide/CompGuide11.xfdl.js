﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("etc 버튼");
                this._setFormPosition(0,0,944,1301);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static03", "absolute", "175", "201", "640", "100", null, null, this);
            obj.set_taborder("144");
            obj.style.set_background("#2e333fff");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("32");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "14", "203", "130", "25", null, null, this);
            obj.set_taborder("108");
            obj.set_text("메뉴 리스트 정렬 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "24", "419", "130", "25", null, null, this);
            obj.set_taborder("111");
            obj.set_text("팝업 닫기 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button16", "absolute", "169", "498", "48", "48", null, null, this);
            obj.set_taborder("126");
            obj.set_cssclass("btn_WF_cal");
            this.addChild(obj.name, obj);

            obj = new Button("Button17", "absolute", "544", "498", "48", "48", null, null, this);
            obj.set_taborder("127");
            obj.set_cssclass("btn_WF_cal");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "24", "508", "130", "25", null, null, this);
            obj.set_taborder("128");
            obj.set_text("달력 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "14", "602", "130", "25", null, null, this);
            obj.set_taborder("129");
            obj.set_text("검색 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button19", "absolute", "544", "596", "48", "48", null, null, this);
            obj.set_taborder("131");
            obj.set_cssclass("btn_WF_pop");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_logout", "absolute", null, "60", "128", "43", "646", null, this);
            obj.set_taborder("132");
            obj.set_text("LOGOUT >");
            obj.set_cssclass("btn_LF_logout");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "14", "68", "130", "25", null, null, this);
            obj.set_taborder("133");
            obj.set_text("로그아웃 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "170", "25", "100", "25", null, null, this);
            obj.set_taborder("134");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "548", "26", "140", "25", null, null, this);
            obj.set_taborder("135");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("btn_logout00", "absolute", "548", "60", "128", "43", null, null, this);
            obj.set_taborder("136");
            obj.set_text("LOGOUT >");
            obj.set_cssclass("btn_LF_logout");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "14", "130", "130", "25", null, null, this);
            obj.set_taborder("137");
            obj.set_text("Left 닫기 버튼");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("right middle");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", null, "596", "48", "48", "727", null, this);
            obj.set_taborder("140");
            obj.set_cssclass("btn_WF_pop");
            this.addChild(obj.name, obj);

            obj = new Button("btn_menu", "absolute", "175", "201", "92", "100", null, null, this);
            obj.set_taborder("141");
            obj.set_cssclass("btn_Top_menu");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, "126", "51", "48", "723", null, this);
            obj.set_taborder("145");
            obj.set_cssclass("btn_LF_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close00", "absolute", null, "126", "51", "48", "336", null, this);
            obj.set_taborder("146");
            obj.set_cssclass("btn_LF_close");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "267", "201", "1", "99", null, null, this);
            obj.set_taborder("147");
            obj.style.set_background("#ffffff30");
            this.addChild(obj.name, obj);

            obj = new Button("btn_prevScreen", "absolute", "268", "201", "62", "100", null, null, this);
            obj.set_taborder("148");
            obj.set_cssclass("btn_Top_back");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "176", "359", "100", "25", null, null, this);
            obj.set_taborder("149");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "554", "360", "140", "25", null, null, this);
            obj.set_taborder("150");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closePopup", "absolute", null, "396", "70", "80", "717", null, this);
            obj.set_taborder("151");
            obj.set_cssclass("btn_pop_close");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", null, "201", "1", "99", "223", null, this);
            obj.set_taborder("152");
            obj.style.set_background("#ffffff30");
            this.addChild(obj.name, obj);

            obj = new Button("btn_condition", "absolute", null, "201", "92", "100", "130", null, this);
            obj.set_taborder("153");
            obj.set_cssclass("btn_Top_search");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 944, 1301, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("etc 버튼");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.btn_menu.addEventHandler("onclick", this.btn_menu_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_close00.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_prevScreen.addEventHandler("onclick", this.btn_prevScreen_onclick, this);
            this.btn_closePopup.addEventHandler("onclick", this.btn_closePop_onclick, this);
            this.btn_condition.addEventHandler("onclick", this.btn_condition_onclick, this);

        };

        this.loadIncludeScript("CompGuide11.xfdl", true);

       
    };
}
)();
