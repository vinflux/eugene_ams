﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("com_period_calendar");
                this.set_classname("com_calendar_month");
                this.set_titletext("기간달력");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,198,21);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Calendar("cal_to", "absolute", "104", "0", "94", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("8");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_value("null");
            obj.style.set_daysize("22 22");
            obj.style.set_buttonsize("15");
            obj.style.set_padding("0 0 0 0");
            obj.style.set_cursor("auto");
            obj.set_type("normal");

            obj = new Static("stc_bar", "absolute", "89", "0", "18", "21", null, null, this);
            obj.set_taborder("3");
            obj.set_text("~");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new PopupDiv("pdv_cal", "absolute", "0", "53", "368", "237", null, null, this);
            obj.set_cssclass("div_WF_CalpopBg");
            this.addChild(obj.name, obj);
            obj = new Calendar("cal_fr", "absolute", "10", "10", "168", "182", null, null, this.pdv_cal);
            this.pdv_cal.addChild(obj.name, obj);
            obj.set_taborder("0");
            obj.set_type("monthonly");
            obj.set_dateformat("yyyy-MM-dd");
            obj.style.set_daysize("22 22");
            obj.style.set_border("1 solid #808080ff");
            obj.set_locale("ko_KR");
            obj.set_value("20160520");
            obj = new Calendar("cal_to", "absolute", "188", "10", "168", "182", null, null, this.pdv_cal);
            this.pdv_cal.addChild(obj.name, obj);
            obj.set_taborder("1");
            obj.set_type("monthonly");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_locale("ko_KR");
            obj.style.set_border("1 solid #bfbfbfff");
            obj.set_value("20160520");
            obj = new Button("btn_WF_Popup", "absolute", null, null, "52", "25", "11", "10", this.pdv_cal);
            obj.set_taborder("2");
            obj.set_text("OK");
            obj.set_cssclass("btn_WF_PopupBtn");
            this.pdv_cal.addChild(obj.name, obj);

            obj = new Calendar("cal_fr", "absolute", "0", "0", "94", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_value("null");
            obj.style.set_buttonsize("15");
            obj.style.set_padding("0 0 0 0");
            obj.style.set_cursor("auto");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 368, 237, this.pdv_cal,
            	//-- Layout function
            	function(p) {
            		p.set_cssclass("div_WF_CalpopBg");

            	}
            );
            this.pdv_cal.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 198, 21, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("com_calendar_month");
            		p.set_titletext("기간달력");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("PeriodCalendar.xfdl", "lib::Comm.xjs");
        this.registerScript("PeriodCalendar.xfdl", function(exports) {
        /**************************************************************************************************
         * 01. 업무구분                : 
         * 02. 화 면 명                : 
         * 03. 화면설명                :  
         * 04. 관련 화면 및 서비스    : 
         * 05. 관련 테이블명            : 
         * 06. 작 성 자                : 
         * 07. 작 성 일                : 
         * 08. 수정이력
         **************************************************************************************************
         *    수정일            이름        사유
         **************************************************************************************************
         *    
         *************************************************************************************************/

        //-------------------------------------------------------------------------------------------------
        // 공통 함수 모음: 공통 스크립트를 include 처리한다.
        //-------------------------------------------------------------------------------------------------
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        //-------------------------------------------------------------------------------------------------
        // [ PART 1 ]
        // Form에서 사용될 전역변수를 선언 
        //-------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------
        // [ PART 2 ]
        // Form event 정의
        //-------------------------------------------------------------------------------------------------

        this.form_onload = function(obj,e)
        {
            
        }

        //-------------------------------------------------------------------------------------------------
        // [ PART 3 ]
        // Form 공통 버튼 이벤트 함수 정의
        //-------------------------------------------------------------------------------------------------

        //닫기 공통버튼 클릭 이벤트
        this.fn_close = function()
        {
            return true;
        }
        //-------------------------------------------------------------------------------------------------
        // [ PART 4 ]
        // transaction 함수 정의
        //-------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------
        // [ PART 5]
        // 사용자 정의 함수
        // ex) fn_함수명
        //-------------------------------------------------------------------------------------------------

        /**
         * @class 기간달력 초기설정
         * @method fn_initComp
         * @param oForm{Object} Form Object
         * @param sPopId{String} Callback ID
         * @param oCallback{Object} Callback Function
         * @param oParam{Object} 기간달력 설정값
                fromdate - 시작날짜 초기값
                todate - 종료날짜 초기값
         * @return none
         */  
        this.fn_initComp = function(oForm,sPopId,oCallback,oParam,oInitFuc)
        {
            this.fv_oForm = oForm;
            this.fv_sPopId = sPopId;
            this.fv_oCallback = oCallback;
            this.fv_oInitFuc = oInitFuc;  /* 초기화 처리가 필용한 경우 */
            
            if(!this.gfn_isNull(oParam))
            {
                this.fn_initValue(oParam);
            }
            else
            {
                this.fn_initValue();
            }
        }

        //값설정
        //viewcolumn에 해당하는값들을 보여줌(기본값은 datacolumn)
        this.fn_setValue = function(oParam)
        {
            this.fn_initValue(oParam);    
        }

        //값얻기
        //기본값은 codecolumn값
        //codecolumn값 이외 경우 컬럼명을 설정해줌
        this.fn_getValue = function()
        {
            var oRtn = {};
            oRtn.fromdate = this.cal_fr.value;
            oRtn.todate = this.cal_to.value;
            
            return oRtn;
        }

        this.fn_initValue = function(oParam)
        {
            this.fv_oParam = oParam;
            
            if(this.gfn_isNull(oParam))
            {
                this.cal_fr.set_value("");
                this.cal_to.set_value("");
                this.pdv_cal.cal_fr.set_value(this.gfn_today());
                this.pdv_cal.cal_to.set_value(this.gfn_today());
                return;
            }
            
            if(!this.gfn_isNull(oParam.fromdate))
            {
                this.cal_fr.set_value(oParam.fromdate);
                this.pdv_cal.cal_fr.set_value(oParam.fromdate);
            }
            else
            {
                this.cal_fr.set_value(oParam.fromdate);
                this.pdv_cal.cal_fr.set_value(this.gfn_today());
            }
            
            if(!this.gfn_isNull(oParam.todate))
            {
                this.cal_to.set_value(oParam.todate);
                this.pdv_cal.cal_to.set_value(oParam.todate);
            }
            else
            {
                this.cal_to.set_value(oParam.todate);
                this.pdv_cal.cal_to.set_value(this.gfn_today());
            }    
        }

        //class 설정
        this.fn_setClass = function(sClass)
        {    
            if(this.gfn_isNull(sClass))
            {
                sClass = "essential";
            }
            
            this.cal_fr.set_cssclass(sClass);
            this.cal_to.set_cssclass(sClass);
        }

        this.fn_checkValidation = function(sMsgCd)
        {
            if(this.gfn_isNull(this.cal_fr.value))
            {
                this.Iject.showAlert(this, "VALID_REQUIRED", [this.gfn_getWord(sMsgCd)]);
                this.cal_fr.setFocus();
                return false;
            }
            if(this.gfn_isNull(this.cal_to.value))
            {
                this.Iject.showAlert(this, "VALID_REQUIRED", [this.gfn_getWord(sMsgCd)]);
                this.cal_to.setFocus();
                return false;
            }
             return true;
        }

        
        //popupdiv오픈
        this.fn_openPopupDiv = function()
        {
            
            this.pdv_cal.cal_fr.set_enable(this.cal_fr.enable);
            this.pdv_cal.cal_to.set_enable(this.cal_to.enable);
            
            this.pdv_cal.cal_fr.set_value(this.cal_fr.value);
            this.pdv_cal.cal_to.set_value(this.cal_to.value);
            
            if(this.gfn_isNull(this.pdv_cal.cal_fr.value)) this.pdv_cal.cal_fr.set_value(this.gfn_today());
            if(this.gfn_isNull(this.pdv_cal.cal_to.value)) this.pdv_cal.cal_to.set_value(this.gfn_today());
            
            this.pdv_cal.trackPopupByComponent(this.cal_fr, 0, 28);
        }
        //-------------------------------------------------------------------------------------------------
        // [ PART 6]
        // 각 component 의 event 정의
        // event 가 있는 component 는 UI 명명규칙에 의하여 id 부여. 자동생성되는 function 명 사용
        //------------------------------------------------------------------------------------------------

        this.btn_expand_onclick = function(obj,e)
        {
            this.fn_openPopupDiv();
        }

        this.cal_fr_ondropdown = function(obj,e)
        {    
            this.fn_openPopupDiv();
            return false;
        }

        this.cal_to_ondropdown = function(obj,e)
        {    
            this.fn_openPopupDiv();
            return false;    
        }

        this.pdv_cal_btn_WF_Popup_onclick = function(obj,e)
        {
            this.closeCalendar();
        }

        this.isSelectedFr = false;
        this.isSelectedTo = false;

        this.closeCalendar = function()
        {    
            var fromdt = this.gfn_strToDate2(this.cal_fr.value.toString());
            var todt = this.gfn_strToDate2(this.cal_to.value.toString());
            var day = this.gfn_getDiffDay(fromdt, todt);

            if(day < 0)
            {
                this.gfn_alert("COLLABO_CODE_89");
                
                if(this.cal_fr.enable) this.cal_fr.set_value("");
                if(this.cal_to.enable) this.cal_to.set_value("");
                
                this.isSelectedFr = false;
                this.isSelectedTo = false;
                
                return false;
            }    

            if(this.isSelectedFr || this.isSelectedTo)
            {
                if(this.gfn_isNull(this.cal_fr.value)) this.cal_fr.set_value(this.pdv_cal.cal_fr.value);
                if(this.gfn_isNull(this.cal_to.value)) this.cal_to.set_value(this.pdv_cal.cal_to.value);
            }    
            
            var oRtn = {};
            oRtn.fromdate = this.cal_fr.value;
            oRtn.todate = this.cal_to.value;
            try{
                this.fv_oCallback.call(this.fv_oForm, this.fv_sPopId, oRtn);
            }catch(e){}
            this.isSelectedFr = false;
            this.isSelectedTo = false;
                
            this.pdv_cal.closePopup();
        }

        this.pdv_cal_cal_fr_ondayclick = function(obj,e)
        {
            if(this.cal_fr.enable && !this.cal_fr.readonly)
            {
                obj.set_value(e.date);
                this.cal_fr.set_value(this.pdv_cal.cal_fr.value);
                
                if(this.fv_oInitFuc != null)
                {
                    this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행
                }
            }
            
            this.isSelectedFr = true;
        }

        this.pdv_cal_cal_to_ondayclick = function(obj,e)
        {
            
            if(this.cal_to.enable && !this.cal_to.readonly)
            {
                obj.set_value(e.date);
                this.cal_to.set_value(this.pdv_cal.cal_to.value);
                
                if(this.fv_oInitFuc != null)
                {
                    this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행
                }
            }
            
            this.isSelectedTo = true;
        }

        this.cal_fr_canchange = function(obj,e)
        {    
            return true;
        }

        this.cal_to_canchange = function(obj,e)
        {    
            return true;
        }

        this.cal_fr_onchanged = function(obj,e)
        {
            if(this.cal_fr.value.toString().indexOf("0000") > -1){
                this.cal_fr.set_value("");
            }

            if(this.gfn_isNull(this.cal_to.value)) return true;
            
            var fromdt = this.gfn_strToDate2(this.cal_fr.value.toString());
            var todt = this.gfn_strToDate2(this.cal_to.value.toString());
            var day = this.gfn_getDiffDay(fromdt, todt);
            
            if(day < 0)
            {
                this.gfn_alert("COLLABO_CODE_89");
                obj.set_value(e.prevalue);
            }
            
            if(this.fv_oInitFuc != null)
            {
                this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행    
            }
            
        }

        this.cal_to_onchanged = function(obj,e)
        {    
            if(this.cal_to.value.toString().indexOf("0000") > -1){
                this.cal_to.set_value("");
            }
            
            if(this.gfn_isNull(this.cal_fr.value)) return true;
            
            var fromdt = this.gfn_strToDate2(this.cal_fr.value.toString());
            var todt = this.gfn_strToDate2(this.cal_to.value.toString());
            var day = this.gfn_getDiffDay(fromdt, todt);

            if(day < 0)
            {
                this.gfn_alert("COLLABO_CODE_89");
                obj.set_value(e.prevalue);
            }
            
            if(this.fv_oInitFuc != null)
            {
                this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행
            }
        }

        this.cal_to_onchar = function(obj,e)
        {
            if(this.fv_oInitFuc != null)
            {
                this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행
            }
        }

        this.cal_fr_onchar = function(obj,e)
        {
            if(this.fv_oInitFuc != null)
            {
                this.fv_oInitFuc.call(this.fv_oForm, this.fv_sPopId); // 화면 내의 초기화 함수 실행
            }
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.periodcalendar_onsize, this);
            this.cal_to.addEventHandler("ondropdown", this.cal_to_ondropdown, this);
            this.cal_to.addEventHandler("canchange", this.cal_to_canchange, this);
            this.cal_to.addEventHandler("onchanged", this.cal_to_onchanged, this);
            this.cal_to.addEventHandler("onchar", this.cal_to_onchar, this);
            this.pdv_cal.cal_fr.addEventHandler("onchanged", this.pdv_cal_cal_fr_onchanged, this);
            this.pdv_cal.cal_fr.addEventHandler("ondayclick", this.pdv_cal_cal_fr_ondayclick, this);
            this.pdv_cal.cal_to.addEventHandler("onchanged", this.pdv_cal_cal_to_onchanged, this);
            this.pdv_cal.cal_to.addEventHandler("ondayclick", this.pdv_cal_cal_to_ondayclick, this);
            this.pdv_cal.btn_WF_Popup.addEventHandler("onclick", this.pdv_cal_btn_WF_Popup_onclick, this);
            this.cal_fr.addEventHandler("canchange", this.cal_fr_canchange, this);
            this.cal_fr.addEventHandler("onchanged", this.cal_fr_onchanged, this);
            this.cal_fr.addEventHandler("onchar", this.cal_fr_onchar, this);
            this.cal_fr.addEventHandler("ondropdown", this.cal_to_ondropdown, this);

        };

        this.loadIncludeScript("PeriodCalendar.xfdl", true);

       
    };
}
)();
