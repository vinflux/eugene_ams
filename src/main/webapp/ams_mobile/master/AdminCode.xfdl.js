﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AdminCode");
                this.set_classname("style01");
                this.set_titletext("관리 기준 코드");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"1\"/><Column id=\"adcd_hdname\" type=\"string\" size=\"32\"/><Column id=\"updatedate\" type=\"string\" size=\"32\"/><Column id=\"inserturkey\" type=\"string\" size=\"32\"/><Column id=\"insertdate\" type=\"string\" size=\"32\"/><Column id=\"updateurkey\" type=\"string\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"bigdecimal\" size=\"8\"/><Column id=\"adcd_hdkey\" type=\"string\" size=\"32\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"200\"/><Column id=\"adcd_dtkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtvalue\" type=\"STRING\" size=\"256\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"DATE\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtorder\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtname\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_hdkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">adcd_hdkey</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 코드</Col></Row><Row><Col id=\"DATA_FILED\">adcd_hdname</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("45");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_excelAll", "absolute", null, "0", "33", "24", "-37", null, this.div_splitTop);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            obj.set_visible("true");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_header");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("none");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"adcd_hdkey\"/><Cell col=\"3\" text=\"adcd_hdname\"/><Cell col=\"4\" text=\"insertdate\"/><Cell col=\"5\" text=\"inserturkey\"/><Cell col=\"6\" text=\"updatedate\"/><Cell col=\"7\" text=\"updateurkey\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding:0 2 0 2;\" text=\"bind:PAGING_NUM\"/><Cell col=\"2\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_hdkey\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_hdname\" editdisplay=\"edit\"/><Cell col=\"4\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"6\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"7\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("47");
            obj.set_scrollbars("none");
            obj.style.set_background("transparent");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("1");
            obj.set_binddataset("ds_detail");
            obj.set_autoenter("none");
            obj.set_cellmovingtype("col,band");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"180\"/><Column size=\"180\"/><Column size=\"120\"/><Column size=\"180\"/><Column size=\"180\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"NO\"/><Cell col=\"2\" text=\"adcd_hdkey\"/><Cell col=\"3\" text=\"adcd_dtkey\"/><Cell col=\"4\" text=\"lakey\"/><Cell col=\"5\" text=\"adcd_dtorder\"/><Cell col=\"6\" text=\"adcd_dtname\"/><Cell col=\"7\" text=\"adcd_dtvalue\"/><Cell col=\"8\" text=\"comments\"/><Cell col=\"9\" text=\"insertdate\"/><Cell col=\"10\" text=\"inserturkey\"/><Cell col=\"11\" text=\"updatedate\"/><Cell col=\"12\" text=\"updateurkey\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:right;padding:0 2 0 2;\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_hdkey\"/><Cell col=\"3\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_dtkey\"/><Cell col=\"4\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lakey\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_dtorder\"/><Cell col=\"6\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_dtname\"/><Cell col=\"7\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:adcd_dtvalue\"/><Cell col=\"8\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:comments\"/><Cell col=\"9\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"11\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"12\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("5");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Detail");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitBottom);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "74", null, this.div_splitBottom);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitBottom);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("9");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("51");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("52");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("47");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("관리 기준 코드");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitBottom.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitBottom.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.btn_excelAll","tooltiptext","gds_lang","ALLEXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("AdminCode.xfdl", "lib::Comm.xjs");
        this.registerScript("AdminCode.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AdminCode.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 관리 기준 코드 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|Copy|Paste|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        //	var gridMenuSet = ["grd_header^1|0|1|1|0|0|0|0|1|1", "grd_detail^0|0|1|1|0|0|0|0|1|1"];
        	this.gv_grdList = [this.div_splitTop.grd_header, this.div_splitBottom.grd_detail]; // 그리드 오브젝트
        	var divPaging = [this.div_splitTop.div_Paging]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.set_keystring(""); // 필터 초기화
        	this.ds_header.clearData();
        	this.ds_detail.set_keystring(""); // 필터 초기화
        	this.ds_detail.clearData();
        	
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "selectAdminCodeInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchDetail = function()
        {
        	this.ds_detail.set_keystring(""); // 필터 초기화
        	this.ds_detail.clearData();

        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "selectDetailAdminCodeInfo");
        	
            var sSvcId   = "selectDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "adcd_hdkey="+this.ds_header.getColumn(this.ds_header.rowposition, "adcd_hdkey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "deleteAdminCode");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_AdminCode=ds_header:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_deleteDetail = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "deleteDetailAdminCodeInfo");
        	
            var sSvcId   = "deleteDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_AdminCode=ds_detail:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.div_splitTop.grd_header.set_nodatatext(application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage"));
        		}
        		
        		this.gfn_setParam("sortValue", "");
        		this.gfn_setParam("colName", "");
        		
        		this.div_splitTop.div_Paging.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "selectDetail"){
        		if(this.ds_detail.rowcount == 0){
        			this.div_splitBottom.grd_detail.set_nodatatext(application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage"));
        		}
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "deleteDetail"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	}else if(sSvcId == "exceldown"){
        		this.gfn_excelDownload(this.ds_excelFile);
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	this.fn_searchDetail();
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.div_splitTop.grd_header, "AdminCode");
        }

        /* div_splitBottom_btn_excel_onclick 실행 */
        this.div_splitBottom_btn_excel_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.div_splitBottom.grd_detail, "AdminCodeDetail");
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("AdminCodePop", "master::AdminCodePop.xfdl", oArg, 580, 187, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	var oArg = "";
        	var nRow = this.ds_header.findRow("CHK", "1");
        	var nRowCnt = this.ds_header.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else if(nRowCnt > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        	}else{
        		oArg = { argFlag:"U"
        				,menuId:this.parent.gv_menuId
        				,adcd_hdkey:this.ds_header.getColumn(nRow, "adcd_hdkey")
        				,adcd_hdname:this.ds_header.getColumn(nRow, "adcd_hdname")
        			   };
        		this.gfn_popup("AdminCodePop", "master::AdminCodePop.xfdl", oArg, 580, 187, "");
        	}
        }

        /* div_splitTop_grd_header_oncellclick 실행 */
        this.div_splitTop_grd_header_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_header.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_header.setColumn(e.row, "CHK", "");
        		else this.ds_header.setColumn(e.row, "CHK", "1");
        	}
        }

        /* div_splitBottom_btn_add_onclick 실행 */
        this.div_splitBottom_btn_add_onclick = function(obj,e)
        {
        	var oArg = "";
        	var nRow = this.ds_header.findRow("CHK", "1");
        	var nRowCnt = this.ds_header.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else if(nRowCnt > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        	}else{
        		oArg = { argFlag:"D"
        				,adcd_hdkey:this.ds_detail.getColumn(nRow, "adcd_hdkey")
        		       };
        		this.gfn_popup("AdminCodePopDtl", "master::AdminCodePopDtl.xfdl", oArg, 580, 187, "");
        	}
        }

        /* div_splitBottom_btn_update_onclick 실행 */
        this.div_splitBottom_btn_update_onclick = function(obj,e)
        {
        	var oArg = "";
        	var nRow = this.ds_detail.findRow("CHK", "1");
        	var nRowCnt = this.ds_detail.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else if(nRowCnt > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        	}else{
        		oArg = { argFlag:"U"
        				,adcd_hdkey:this.ds_detail.getColumn(nRow, "adcd_hdkey")
        				,adcd_dtkey:this.ds_detail.getColumn(nRow, "adcd_dtkey")
        				,lakey:this.ds_detail.getColumn(nRow, "lakey")
        				,adcd_dtname:this.ds_detail.getColumn(nRow, "adcd_dtname")
        				,adcd_dtorder:this.ds_detail.getColumn(nRow, "adcd_dtorder")
        				,comments:this.ds_detail.getColumn(nRow, "comments")
        				,adcd_dtvalue:this.ds_detail.getColumn(nRow, "adcd_dtvalue")
        			   };
        		this.gfn_popup("AdminCodePopDtl", "master::AdminCodePopDtl.xfdl", oArg, 580, 187, "");
        	}
        }

        /* div_splitBottom_btn_delete_onclick 실행 */
        this.div_splitBottom_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_deleteDetail();
        			}
        		});
        	}
        }

        /* div_splitBottom_grd_detail_oncellclick 실행 */
        this.div_splitBottom_grd_detail_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_detail.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_detail.setColumn(e.row, "CHK", "");
        		else this.ds_detail.setColumn(e.row, "CHK", "1");
        	}
        }

        this.div_splitTop_btn_excelAll_onclick = function(obj,e)
        {
        	this.excelDownloadByJsp();
        }

        this.excelDownloadByComponent = function(){
        	this.gfn_setParam("EXCEL_FORMAT", "EXCEL_FORMAT");
        	this.gfn_setParam("EXCEL_FILENAME", "TEST_EXCEL");

        	this.gfn_setCommon("FILE_BEANID", "masterAdminCodeService");
        	this.gfn_setCommon("FILE_METHODNM", "selectExcelDownAdminCode");
        	
            var sSvcId   = "exceldown";
            var sSvcUrl  = application.gv_ams + application.gv_fUrl;
            var sInData  = "IN_EXCEL_FORMAT=ds_excelSetting";
            var sOutData = "ds_excelFile=OUT_FILE_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.excelDownloadByJsp = function(){
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var jspName = "excelSetting.jsp";
        	var paramExcelFormat = "EXCEL_FORMAT=EXCEL_FORMAT";
        	var paramExcelFileName = "EXCEL_FILENAME=jspExcelDown";
        	var paramBeanID = "BEANID=masterAdminCodeController";
        	var paramMethodNm = "METHODNM=excelDown";
        	var paramXlsData = "xlsParam=%7B%22PARAM%22%3A%7B%22pagingLimit%22%3A50%2C%22currentPage%22%3A%221%22%2C%22EXCEL_FILENAME%22%3A%22%5BAdminCode%5D_%5B20161209%5D_%5B151512%5D%22%7D%2C%22MAP%22%3A%7B%7D%2C%22LIST%22%3A%7B%22EXCEL_FORMAT%22%3A%5B%7B%22DATA_FILED%22%3A%22No%22%2C%22FILED_NAME1%22%3A%22NO%22%2C%22FILED_NAME2%22%3A%22NO%22%2C%22FILED_NAME3%22%3A%22NO%22%7D%2C%7B%22DATA_FILED%22%3A%22adcd_hdkey%22%2C%22FILED_NAME1%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%2C%22FILED_NAME2%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%2C%22FILED_NAME3%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cucf54%5Cub4dc%22%7D%2C%7B%22DATA_FILED%22%3A%22adcd_hdname%22%2C%22FILED_NAME1%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%2C%22FILED_NAME2%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%2C%22FILED_NAME3%22%3A%22%5Cuad00%5Cub9ac%5Cuc790+%5Cuae30%5Cuc900+%5Cucf54%5Cub4dc+%5Cuacf5%5Cud1b5+%5Cuba85%22%7D%2C%7B%22DATA_FILED%22%3A%22insertdate%22%2C%22FILED_NAME1%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME2%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME3%22%3A%22%5Cuc785%5Cub825+%5Cuc77c%5Cuc2dc%22%7D%2C%7B%22DATA_FILED%22%3A%22inserturkey%22%2C%22FILED_NAME1%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME2%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME3%22%3A%22%5Cuc785%5Cub825+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%7D%2C%7B%22DATA_FILED%22%3A%22updatedate%22%2C%22FILED_NAME1%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME2%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%2C%22FILED_NAME3%22%3A%22%5Cuc218%5Cuc815+%5Cuc77c%5Cuc2dc%22%7D%2C%7B%22DATA_FILED%22%3A%22updateurkey%22%2C%22FILED_NAME1%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME2%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22FILED_NAME3%22%3A%22%5Cuc218%5Cuc815+%5Cuc0ac%5Cuc6a9%5Cuc790+ID%22%2C%22HEADER_CNT%22%3A%221%22%7D%5D%7D%2C%22COMMON%22%3A%7B%22DEVICE%22%3A%22PC%22%2C%22EQTYPE%22%3A%2210%22%2C%22TIMEZONE%22%3A%229%22%2C%22uskey%22%3A%22US00000080%22%2C%22USER_INFO%22%3A%7B%22urKey%22%3A%22eungheon.kim%22%2C%22rstclgst%22%3A%22N%22%2C%22loginStatus%22%3A%22SUCCESS%22%2C%22owkeym%22%3A%22HANSOL%22%2C%22ctKey_desc%22%3A%22%5Cud55c%5Cuc194+%5Cuc13c%5Cud130+2%22%2C%22icgrkey%22%3A%22%22%2C%22dst%22%3A%22N%22%2C%22rstcdiv%22%3A%22N%22%2C%22urGrKey%22%3A%22SYSADM%22%2C%22ctKey%22%3A%22802%22%2C%22SESSION_USERINFO%22%3A%22SESSION_USERINFO%22%2C%22rep_ackey%22%3A%220000000%22%2C%22ipAddress%22%3A%22%22%2C%22expireDateStatus%22%3A%22VALID%22%2C%22usKey%22%3A%22%22%2C%22divcd%22%3A%22%22%2C%22urName%22%3A%22%5Cuae40%5Cuc751%5Cud5cc%22%2C%22usrtyp%22%3A%22%22%2C%22urliloh_key%22%3A%221600009718%22%2C%22loggrpcd%22%3A%22%22%2C%22duplLog%22%3A%22N%22%2C%22utcHour%22%3A%229%22%2C%22laKey%22%3A%22KOR%22%2C%22utcOffset%22%3A%22%2B0900%22%2C%22utcMinute%22%3A%220%22%7D%2C%22ACTIVE_APP%22%3A%22ADM%22%2C%22OWKEY_AUTH%22%3A%22BGF%2CHANSOL%2CWINFOOD%2C%5Cuc9c0%5Cuc624%5Cuc601%2C11ST%22%2C%22CTKEY_AUTH%22%3A%22801%2C802%2C901%2C902%2C903%2C904%2C905%2C906%2C907%2C908%2C909%2C912%2C913%2C916%2C917%2C918%2C920%2C921%2C923%2C924%2C925%2C926%2C927%2C928%2C929%2C930%2C998%2C999%22%7D%2C%22SEARCHLIST%22%3A%5B%7B%22dbColoum%22%3A%22ADCD_HDKEY%22%2C%22operator%22%3A%22%3D%22%2C%22value%22%3A%22%22%7D%2C%7B%22dbColoum%22%3A%22ADCD_HDNAME%22%2C%22operator%22%3A%22LIKE%22%2C%22value%22%3A%22%22%7D%5D%7D";
        	
        	var fullsUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/"+this.gfn_getActiveApp()+"/"
        	    +jspName
        	    +"?"+paramExcelFormat
        	    +"&"+paramExcelFileName
        	    +"&"+paramBeanID
        	    +"&"+paramMethodNm
        	    +"&"+paramXlsData;
        	    
        	trace("fullsUrl : "+fullsUrl);
        	trace("paramXlsData : "+paramXlsData);
        	this.web_excelFile.set_url(fullsUrl);
        	this.web_excelFile.updateWindow
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.btn_excelAll.addEventHandler("onclick", this.div_splitTop_btn_excelAll_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.btn_add.addEventHandler("onclick", this.div_splitBottom_btn_add_onclick, this);
            this.div_splitBottom.btn_update.addEventHandler("onclick", this.div_splitBottom_btn_update_onclick, this);
            this.div_splitBottom.btn_delete.addEventHandler("onclick", this.div_splitBottom_btn_delete_onclick, this);
            this.div_splitBottom.btn_excel.addEventHandler("onclick", this.div_splitBottom_btn_excel_onclick, this);

        };

        this.loadIncludeScript("AdminCode.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
