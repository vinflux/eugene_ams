﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("DynamicCondPop");
                this.set_classname("DynamicCondPop");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this.set_visible("false");
                this._setFormPosition(0,0,550,590);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchVal", this);
            obj._setContents("<ColumnInfo><Column id=\"key\" type=\"STRING\" size=\"256\"/><Column id=\"sqlType\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static33", "absolute", "9", "146", null, "52", "9", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("stc_ibexpectdateBg", "absolute", "9", "86", null, "52", "9", null, this);
            obj.set_taborder("1");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ibexpectdateSel", "absolute", null, "151", "52", "50", "20", null, this);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_pop");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "241", "151", null, "50", "76", null, this);
            obj.set_taborder("3");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "9", "201", null, "52", "9", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "241", "208", null, "50", "20", null, this);
            obj.set_taborder("5");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "9", "253", null, "52", "9", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "241", "265", null, "50", "76", null, this);
            obj.set_taborder("7");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ackey", "absolute", null, "265", "52", "50", "20", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_pop");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "9", "306", null, "52", "9", null, this);
            obj.set_taborder("9");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "241", "322", null, "50", "20", null, this);
            obj.set_taborder("10");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "9", "358", null, "52", "9", null, this);
            obj.set_taborder("11");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "9", "403", null, "52", "9", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "241", "435", null, "50", "20", null, this);
            obj.set_taborder("13");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchPop", "absolute", "0", null, null, "80", "0", "0", this);
            obj.set_taborder("14");
            obj.set_text("검색");
            obj.set_cssclass("btn_RM_cartAll");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "20", "322", "214", "50", null, null, this);
            obj.set_taborder("15");
            obj.set_text("매입처명");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "20", "265", "214", "50", null, null, this);
            obj.set_taborder("16");
            obj.set_text("매입처 코드");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "20", "435", "214", "50", null, null, this);
            obj.set_taborder("17");
            obj.set_text("전표번호");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "20", "151", "214", "50", null, null, this);
            obj.set_taborder("18");
            obj.set_text("상품코드");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("stc_ibexpectdate", "absolute", "20", "94", "214", "50", null, null, this);
            obj.set_taborder("19");
            obj.set_text("입고 예정 일자");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "20", "208", "214", "50", null, null, this);
            obj.set_taborder("20");
            obj.set_text("상품명");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit04", "absolute", "241", "379", null, "50", "20", null, this);
            obj.set_taborder("21");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "20", "379", "214", "50", null, null, this);
            obj.set_taborder("22");
            obj.set_text("ASN 코드");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("stc_bg", "absolute", "0", "0", null, "80", "0", null, this);
            obj.set_taborder("24");
            obj.set_text("Search");
            obj.set_cssclass("sta_pop_title");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_ibexpectdate", "absolute", "241", "94", null, "50", "20", null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("25");
            obj.set_dateformat("yyyy-MM-dd");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");

            obj = new Static("Static04", "absolute", "447", "144", "137", "7", null, null, this);
            obj.set_taborder("26");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closePopup", "absolute", null, "0", "70", "80", "0", null, this);
            obj.set_taborder("23");
            obj.set_cssclass("btn_pop_close");
            this.addChild(obj.name, obj);

            obj = new Static("stc_topGap", "absolute", "199", "80", "137", "14", null, null, this);
            obj.set_taborder("27");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("stc_topGap00", "absolute", "199", null, "137", "25", null, "80", this);
            obj.set_taborder("28");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 550, 590, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("DynamicCondPop");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");
            		p.set_visible("false");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("DynamicCondPop.xfdl", "lib::Comm.xjs");
        this.registerScript("DynamicCondPop.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/
        var fv_searchCompDs;
        var fv_searchValDs;

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        	fv_searchCompDs = obj.arguments.dsCompList;

        	this.ds_searchVal.clearData();
        	this.ds_searchVal.copyData(obj.arguments.dsSearchVal);

        	this.fn_clearComp(obj);
        	this.fn_createComp();

        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        this.btn_searchPop_onclick = function(obj,e)
        {
        	if (this.gfn_chkCompNull(this, this)) {
        		return;
        	}

        	this.ds_searchVal.clearData();

        	var objComps = this.components;

        	for (var i = 0; i < objComps.length; i++) {
        		if (objComps[i] instanceof Div) {
        			var sKey = objComps[i].fv_key;
        			var sValue = objComps[i].objValueComp.value;

        			if (!this.gfn_isNull(sValue)) {
        				var nRow = this.ds_searchVal.addRow();
        				this.ds_searchVal.setColumn(nRow, "key", this.fn_getProp(sKey, "dbfieldname"));
        				this.ds_searchVal.setColumn(nRow, "sqlType", this.fn_getProp(sKey, "defaultoperator"));
        				this.ds_searchVal.setColumn(nRow, "value", sValue);
        			}
        		}
        	}

        	trace("this.arguments.bAtLeastSelOne : " + this.arguments.bAtLeastSelOne);

        	if (this.arguments.bAtLeastSelOne) {
        		var objParam = new Object();
        		objParam.objDs = this.ds_searchVal;
        		objParam.colNm = "value";

        		var objRtn = this.gfn_chkDSNull(objParam);

        		if (this.ds_searchVal.getRowCount() == 0 || objRtn.result) {
        			alert("검색조건 중 한가지는 입력해야 됩니다."); // MSG_CODE_NONE
        			return;
        		}
        	}

        	this.gfn_popupClose(this.ds_searchVal);
        }

        this.btn_closePop_onclick = function(obj,e)
        {
        	this.gfn_popupClose();
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        this.fn_createComp = function ()
        {
        	var nGap = 7;
        	var nDefTop = this.stc_topGap.getOffsetTop() + this.stc_topGap.getOffsetHeight();
        	var nLeft = 10;
        	var nRight = 10;
        	var nWidth = null;
        	var nHeight = 50;
        	var nBottom = null;
        	var nTop = nDefTop;

        	for (var i = 0 ; i < fv_searchCompDs.getRowCount(); i++) {
        		var sKey = fv_searchCompDs.getColumn(i, "ussc_dtkey");
        		var sKeyNm = fv_searchCompDs.getColumn(i, "ussc_label");
        		var sUsscDtType = fv_searchCompDs.getColumn(i, "ussc_dttype");
        		var sNotNull = fv_searchCompDs.getColumn(i, "ismandatory");
        		var ismandatory     = fv_searchCompDs.getColumn(i, "ismandatory");
        		var searchid         = fv_searchCompDs.getColumn(i, "searchid");

        		if (this.gfn_isNull(sUsscDtType)) continue;

        		var sObjName = "div_" + String(sKey).toLowerCase() + "_" + String(i);
        		var objCondDiv = new Div();
        		objCondDiv.init(sObjName, "absolute", nLeft, nTop, nWidth, nHeight, nRight, nBottom);
        		this.addChild(sObjName, objCondDiv);
        		objCondDiv.set_async(false);
        		objCondDiv.show();
        		objCondDiv.style.set_background("transparent");
        		objCondDiv.isCreated = "Y";

        		nTop = nDefTop + ((i + 1) * nHeight) + ((i + 1) * nGap);

         		if (sUsscDtType == "EDITBOXSEARCHPOPUP") {
        			var sSearchId = fv_searchCompDs.getColumn(i, "searchid");
        			objCondDiv.searchid = sSearchId;
        			objCondDiv.set_url("cond::CommSearchCondEditSearch.xfdl");
        			objCondDiv.gv_divId = sObjName;
        			objCondDiv.gv_label = sKeyNm;
        		}

         		if (sUsscDtType == "EDITBOX") {
        			objCondDiv.set_url("cond::CommSearchCondEdit.xfdl");
        			objCondDiv.gv_divId = sObjName;
        			objCondDiv.gv_label = sKeyNm;
        		}

         		if (sUsscDtType == "DATEFIELD") {
        			objCondDiv.set_url("cond::CommSearchCondCalendar.xfdl");
        			objCondDiv.gv_divId = sObjName;
        			objCondDiv.gv_label = sKeyNm;
        		}
        //Div00
        		objCondDiv.sendToBack();
        		objCondDiv.set_scrollbars("none");
        		
        		objCondDiv.gv_searchId = sSearchId;
        		
        		if(this.gfn_isNotNull(application.gds_lang.getColumn(0, sKeyNm)) && this.gfn_isNotNull(objCondDiv.stc_label)){
        			objCondDiv.stc_label.set_text(application.gds_lang.getColumn(0, sKeyNm));
        		}

        		if(ismandatory == "Y" && this.gfn_isNotNull(objCondDiv.stc_label)){
        			objCondDiv.stc_label.set_cssclass("sta_WF_detail_duty");
        		}

        
        // 		if (sUsscDtType == "COMBOBOX") sCompPrefix = "cbo_";
        // 		if (sUsscDtType == "BETWEENDATEFIELD") sCompPrefix = "cal_";
        // 		if (sUsscDtType == "BETWEENDATETIMEFIELD") sCompPrefix = "cal_";
        // 		if (sUsscDtType == "BETWEENMONTHFIELD") sCompPrefix = "cal_";

        //Div(strName, position, nLeft, nTop[, nWidth, nHeight[, nRight, nBottom]])
        	}

        	this.set_visible(true);

        }

        this.fn_getKey = function (objDiv)
        {
        	var sDivName = objDiv.name;
        	var sRowNum = Number(String(sDivName).substr(String(sDivName).lastIndexOf("_") + 1));
        	var sKey = fv_searchCompDs.getColumn(sRowNum, "ussc_dtkey");

        	return sKey;
        }

        this.fn_getLabelCode = function (objDiv)
        {
        	var sDivName = objDiv.name;
        	var sRowNum = Number(String(sDivName).substr(String(sDivName).lastIndexOf("_") + 1));
        	var sKey = fv_searchCompDs.getColumn(sRowNum, "ussc_label");

        	return sKey;
        }

        this.fn_getProp = function (sKey,sPropNm)
        {
        	var sRtn = "";
        	var nFindRow = fv_searchCompDs.findRowExpr("ussc_dtkey == '" + sKey + "'");

        	if (nFindRow >= 0) sRtn = fv_searchCompDs.getColumn(nFindRow, sPropNm);

        	return sRtn;
        }

        this.fn_setDefValue = function (objComp,sValue)
        {
        	var objDiv = objComp.parent;
        	var sKey = objDiv.key;trace("sKey : " + sKey);
        	var nFindRow = this.ds_searchVal.findRowExpr("key == '" + sKey + "'");
        	var sSavedValue = "";

        	if (nFindRow >= 0) sSavedValue = this.ds_searchVal.getColumn(nFindRow, "value");

        	if (!this.gfn_isNull(sSavedValue)) sValue = sSavedValue;
        trace("sSavedValue : " + sSavedValue + ", sValue : " + sValue);
        	objComp.set_value(sValue);
        }

        this.fn_clearComp = function (objTarget)
        {
        	var bReturn = true;

        	if (objTarget instanceof Form) bReturn = false;
        	if (objTarget instanceof Div) bReturn = false;
        	if (objTarget instanceof Tabpage) bReturn = false;

        	if (bReturn) return;

        	var objComps = objTarget.components;
        	var nLength = objComps.length - 1;

        	for (var i = nLength ; i >= 0; i--) {
        		if (this.gfn_isNull(objComps[i].isCreated)) continue;
        		if (objComps[i].isCreated == "Y") {
        			objTarget.removeChild(objComps[i].name);
        		}
        	}
        }

        this.fn_popupAfter = function (sPopupId,objRtn)
        {
        	if (sPopupId == "selCommCodePop") {
        		if (!this.gfn_isNull(objRtn)) {
        			var objRtnDs = objRtn.rtnDs;
        			var sKey = objRtn.key;
        			var sValueColNm = objRtn.valueColNm;
        			var sValue = objRtnDs.getColumn(0, sValueColNm);
        			var sCompName = "div_" + String(sKey).toLowerCase() + "_" + objRtn.compPos;
        			var objCompDiv = this.components[sCompName];

        			if (!this.gfn_isNull(objCompDiv) && !this.gfn_isNull(objCompDiv.edt_value)) {
        				if (!this.gfn_isNull(sValue)) objCompDiv.edt_value.set_value(sValue);
        			}
        		}
        	}
        }

        this.fn_commCodePopup = function (sPopupId,sKey,sCompPos,sLabelCode)
        {
        	if (this.gfn_isNull(sKey)) return;

        	var sUrl = "comm::CommCodePop.xfdl";

        	var oArg = new Object();
        	var nWidth = 550;
        	var nHeight = 620;
        	var sPopupCallback = "";
        	var bModeless = false;

        
        	var nFindRow = fv_searchCompDs.findRowExpr("ussc_dtkey == '" + sKey + "'");

        	if (nFindRow < 0) return;

        	var sSearchId = fv_searchCompDs.getColumn(nFindRow, "searchid");

        	oArg.key = sKey;
        	oArg.labelCode = sLabelCode;
        	oArg.compPos = sCompPos;
        	oArg.searchid = sSearchId;

        	this.gfn_popup(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_ibexpectdateSel.addEventHandler("onclick", this.btn_ibexpectdateSel_onclick, this);
            this.btn_ackey.addEventHandler("onclick", this.btn_ackey_onclick, this);
            this.btn_searchPop.addEventHandler("onclick", this.btn_searchPop_onclick, this);
            this.btn_closePopup.addEventHandler("onclick", this.btn_closePop_onclick, this);

        };

        this.loadIncludeScript("DynamicCondPop.xfdl", true);

       
    };
}
)();
