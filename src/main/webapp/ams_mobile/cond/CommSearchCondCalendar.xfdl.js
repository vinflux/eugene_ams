﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CommSearchCondEdit");
                this.set_classname("CommSearchCondEdit");
                this.set_titletext("Comm Cond Calendar");
                this._setFormPosition(0,0,530,50);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static04", "absolute", "226", "4", "7", "76", null, null, this);
            obj.set_taborder("3");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_value", "absolute", "233", "0", null, "50", "0", null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("5");
            obj.set_dateformat("yyyy-MM-dd");
            obj.getSetter("isCreated").set("Y");
            obj.set_autoskip("true");
            obj.set_readonly("true");

            obj = new Static("stc_label", "absolute", "0", "0", "226", "50", null, null, this);
            obj.set_taborder("6");
            obj.getSetter("isCreated").set("Y");
            obj.set_cssclass("sta_WF_detail");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 530, 50, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CommSearchCondEdit");
            		p.set_titletext("Comm Cond Calendar");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CommSearchCondCalendar.xfdl", "lib::Comm.xjs");
        this.registerScript("CommSearchCondCalendar.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/
        this.fv_key;
        this.fv_labelCode;
        this.objValueComp = this.cal_value;

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);

        	this.fv_key = this.parent.fn_getKey(obj);
        	this.fv_labelCode = this.parent.fn_getLabelCode(obj);
        	//this.cal_value.set_autoselect(true);
        	var objBind = new BindItem("bindLabel", "stc_label", "text", "gds_lang", this.fv_labelCode);
        	this.addChild("bindLabel", objBind); 
        	objBind.bind();

        	var sNotNull = this.parent.fn_getProp(this.fv_labelCode, "ismandatory");
        	if (sNotNull == "Y") this.cal_value.set_cssclass("essential");

         	var sDBColNm = this.parent.fn_getProp(this.fv_key, "dbfieldname");
         	var sDefValRow = this.parent.ds_searchVal.findRowExpr("key == '" + sDBColNm + "'");
        	var sDefVal = this.parent.ds_searchVal.getColumn(sDefValRow, "value");

        	if (this.gfn_isNull(sDefVal)) {
        		this.parent.fn_setDefValue(this.cal_value, this.gfn_today());
        	} else {
        		this.parent.fn_setDefValue(this.cal_value, sDefVal);
        	}
        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        this.cal_value_ondropdown = function(obj,e)
        {
        	var sPopupId = "selCalendarPop";
        	var sUrl = "comm::CalendarPop.xfdl";

        	var oArg = new Object();
        	var nWidth = 589;
        	var nHeight = 699;
        	var sPopupCallback = "";
        	var bModeless = false;

        	oArg.selDate = obj.value;

        	this.gfn_popup(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless);

        	return false;
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        this.fn_popupAfter = function (sPopupId,objRtn)
        {
        	if (sPopupId == "selCalendarPop") {
        		if (!this.gfn_isNull(objRtn)) {
        			this.cal_value.set_value(objRtn);
        		}
        	}
        }
        this.cal_value_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.parent.btn_searchPop_onclick();
        	}
        }

        this.cal_value_oneditclick = function(obj,e)
        {
        	var sPopupId = "selCalendarPop";
        	var sUrl = "comm::CalendarPop.xfdl";

        	var oArg = new Object();
        	var nWidth = 589;
        	var nHeight = 699;
        	var sPopupCallback = "";
        	var bModeless = false;

        	oArg.selDate = obj.value;

        	this.gfn_popup(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless);

        	return false;
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.cal_value.addEventHandler("ondropdown", this.cal_value_ondropdown, this);
            this.cal_value.addEventHandler("onkeydown", this.cal_value_onkeydown, this);
            this.cal_value.addEventHandler("oneditclick", this.cal_value_oneditclick, this);

        };

        this.loadIncludeScript("CommSearchCondCalendar.xfdl", true);

       
    };
}
)();
