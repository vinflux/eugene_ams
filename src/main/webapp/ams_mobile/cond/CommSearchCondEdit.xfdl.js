﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CommSearchCondEdit");
                this.set_classname("CommSearchCondEdit");
                this.set_titletext("Comm Cond Edit");
                this._setFormPosition(0,0,530,50);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static13", "absolute", "0", "0", null, "52", "0", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_WF_bg");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_value", "absolute", "233", "0", null, "50", "0", null, this);
            obj.set_taborder("1");
            obj.style.set_font("antialias bold 15 Dotum");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("stc_label", "absolute", "0", "0", "226", "50", null, null, this);
            obj.set_taborder("2");
            obj.set_cssclass("sta_WF_detail");
            obj.getSetter("isCreated").set("Y");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "226", "4", "7", "76", null, null, this);
            obj.set_taborder("3");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 530, 50, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CommSearchCondEdit");
            		p.set_titletext("Comm Cond Edit");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CommSearchCondEdit.xfdl", "lib::Comm.xjs");
        this.registerScript("CommSearchCondEdit.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/
        this.fv_key;
        this.fv_labelCode;
        this.objValueComp = this.edt_value;

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        	this.edt_value.set_autoselect(true);
        	this.fv_key = this.parent.fn_getKey(obj);
        	this.fv_labelCode = this.parent.fn_getLabelCode(obj);
        	
        	var objBind = new BindItem("bindLabel", "stc_label", "text", "gds_lang", this.fv_labelCode);
        	this.addChild("bindLabel", objBind); 
        	objBind.bind();

        	var sNotNull = this.parent.fn_getProp(this.fv_labelCode, "ismandatory");
        	if (sNotNull == "Y") this.edt_value.set_cssclass("essential");

        
        	var sNotNull = this.parent.fn_getProp(this.fv_labelCode, "ismandatory");
        	if (sNotNull == "Y") this.cal_value.set_cssclass("essential");

         	var sDBColNm = this.parent.fn_getProp(this.fv_key, "dbfieldname");
         	var sDefValRow = this.parent.ds_searchVal.findRowExpr("key == '" + sDBColNm + "'");
        	var sDefVal = this.parent.ds_searchVal.getColumn(sDefValRow, "value");

        	if (this.gfn_isNull(sDefVal)) {
        		this.parent.fn_setDefValue(this.edt_value, "");
        	} else {
        		this.parent.fn_setDefValue(this.edt_value, sDefVal);
        	}
        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        /***********************************************************************************
        * User Function
        ***********************************************************************************/

        this.edt_value_onkeyup = function(obj,e)
        {
        	if(e.keycode == "13"){
        		var lastIndex = obj.getLength();
        		obj.setSelect(0,lastIndex);
        		//this.parent.btn_searchPop_onclick();
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.edt_value.addEventHandler("onkeyup", this.edt_value_onkeyup, this);

        };

        this.loadIncludeScript("CommSearchCondEdit.xfdl", true);

       
    };
}
)();
