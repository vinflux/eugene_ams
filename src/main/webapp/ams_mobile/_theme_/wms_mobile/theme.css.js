﻿//CSS=theme.css
    
(function() {
  return function() {
    var obj;   
    
    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("MainFrame", "background", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#maxbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#normalbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#closebutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("VScrollBarControl>#decbutton", "background", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "background", obj, ["normal"]);
    this._addCss("Button.btn_WF_Month", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_WF_top", "background", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_login_pwSearch", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Calendar>#calendaredit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#dropbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "background", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "background", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar.cal_WF_month", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar.cal_WF_month>#calendaredit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("CheckBox", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo>#comboedit", "background", obj, ["normal"]);
    this._addCss("Combo>#dropbutton", "background", obj, ["normal", "focused", "mouseover", "pushed"]);
    this._addCss("Combo.com_login_lange>#comboedit", "background", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#dropbutton", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "background", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#dropbutton", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Div", "background", obj, ["normal"]);
    this._addCss("Edit.edt_WF_code", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Grid>#controlcheckbox", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "background", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "background", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "background", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("PopupDiv", "background", obj, ["normal"]);
    this._addCss("Radio", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Spin>#spinedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin>#spinupbutton", "background", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "background", obj, ["normal"]);
    this._addCss("Static", "background", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "background", obj, ["normal", "disabled", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Tab>#spindownbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);
    this._addCss("Tab>#extrabutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("MainFrame", "border", obj, ["normal"]);
    this._addCss("ChildFrame", "border", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#maxbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#normalbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#closebutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("VScrollBarControl", "border", obj, ["disabled"]);
    this._addCss("VScrollBarControl>#decbutton", "border", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl>#decbutton", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "border", obj, ["normal"]);
    this._addCss("*>#resizebutton", "border", obj, ["normal"]);
    this._addCss("Button", "border", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_Top_center", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_centerS", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_left", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_leftS", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_right", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_rightS", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_menu", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_search", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_back", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_home", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LF_close", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_LF_save", "border", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_Search", "border", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_more", "border", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_tab", "border", obj, ["normal", "mouseover", "selected", "pushed", "disabled"]);
    this._addCss("Buttonbtn_WF_tab", "border", obj, ["focused"]);
    this._addCss("Button.btn_WF_next", "border", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_Month", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_WF_MonthS", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_WF_top", "border", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_pop_close", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_pop_confirm", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_login", "border", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Button.btn_login_pwSearch", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_CM_notice", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_CM_as", "border", obj, ["normal"]);
    this._addCss("Button.btn_CM_data", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_CM_news", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Rm_tabNext", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Rm_tabPrev", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTab", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTabS", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignlist", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignlistS", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_alignHorz", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignHorzS", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_alignVert", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignVertS", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_cartAll", "border", obj, ["normal", "mouseover", "disable", "selected", "pushed"]);
    this._addCss("Button.btn_RM_cartMove", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_general", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_detail", "border", obj, ["normal"]);
    this._addCss("Button.btn_RM_favorite", "border", obj, ["normal"]);
    this._addCss("Button.btn_RM_cart", "border", obj, ["normal"]);
    this._addCss("Button.btn_cal_monthNext", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_cal_monthPrev", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.setup_attr_on", "border", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Calendar>#calendaredit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#dropbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar.cal_WF_month", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar.cal_WF_month>#calendaredit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("CheckBox", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo>#comboedit", "border", obj, ["normal"]);
    this._addCss("Combo>#dropbutton", "border", obj, ["normal", "focused", "mouseover", "pushed"]);
    this._addCss("Combo.com_login_lange", "border", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "border", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#dropbutton", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo.com_login_lange>#combolist", "border", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "border", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#dropbutton", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Div", "border", obj, ["normal"]);
    this._addCss("Div.setup_attr_on", "border", obj, ["normal"]);
    this._addCss("Div.setup_attr_off", "border", obj, ["normal"]);
    this._addCss("Edit.edt_login_id", "border", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "border", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_WF_code", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Form", "border", obj, ["normal"]);
    this._addCss("Grid>#head", "border", obj, ["normal"]);
    this._addCss("Grid>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#summ", "border", obj, ["normal"]);
    this._addCss("Grid>#summary", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "border", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlexpand", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid.grd_LF_menu>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar>#trackbar", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid.grd_WF_menu", "border", obj, ["normal"]);
    this._addCss("Grid.grd_WF_menu>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_LM_menulev2", "border", obj, ["normal"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_align", "border", obj, ["normal"]);
    this._addCss("Grid.grd_RM_align>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "border", obj, ["normal", "mouseover"]);
    this._addCss("PopupDiv", "border", obj, ["normal"]);
    this._addCss("ProgressBar", "border", obj, ["normal", "disabled"]);
    this._addCss("Radio", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Spin>#spinedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Static", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail_duty", "border", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "border", obj, ["normal"]);
    this._addCss("Static.sta_RM_roundtxt", "border", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "border", obj, ["normal", "disabled", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Tab>#spindownbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);
    this._addCss("Tab>#extrabutton", "border", obj, ["normal"]);
    this._addCss("Tab.tab_LF_user", "border", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Tab.tab_RM", "border", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("MainFrame", "bordertype", obj, ["normal"]);
    this._addCss("ChildFrame", "bordertype", obj, ["normal"]);
    this._addCss("ChildFrame>TitlebarForm", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_Top_center", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_centerS", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_left", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_leftS", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_right", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_rightS", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_menu", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_search", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_back", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_home", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LF_close", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_WF_MonthS", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_CM_notice", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_CM_as", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_CM_data", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_CM_news", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu1", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTab", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTabS", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignlist", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignlistS", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_alignHorz", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignHorzS", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_alignVert", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_alignVertS", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_cartAll", "bordertype", obj, ["normal", "mouseover", "disable", "selected", "pushed"]);
    this._addCss("Button.btn_RM_cartMove", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_detail", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_RM_favorite", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_RM_cart", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_cal_monthNext", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_cal_monthPrev", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Calendar.cal_WF_month", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "bordertype", obj, ["normal"]);
    this._addCss("Edit.edt_RM_amount", "bordertype", obj, ["normal"]);
    this._addCss("Form", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlexpand", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar>#trackbar", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Static.sta_WF_label", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_s", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_label2", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("100");
    this._addCss("MainFrame", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_value("28");
    this._addCss("MainFrame", "titlebarheight", obj, ["normal"]);

    obj = new nexacro.Style_value("0");
    this._addCss("MainFrame", "statusbarheight", obj, ["normal"]);
    this._addCss("ChildFrame", "statusbarheight", obj, ["normal"]);

    obj = new nexacro.Style_value("28");
    this._addCss("MainFrame", "menubarheight", obj, ["normal"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("MainFrame", "color", obj, ["normal"]);
    this._addCss("Static.sta_pop_title", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 10 Dotum");
    this._addCss("MainFrame", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("ChildFrame", "background", obj, ["normal"]);
    this._addCss("VScrollBarControl", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_WF_cal", "background", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_pop", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Button.btn_Rm_tabS", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.setup_attr_on", "background", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Calendar", "background", obj, ["normal"]);
    this._addCss("Calendar.cal_WF_month", "background", obj, ["readonly"]);
    this._addCss("Combo", "background", obj, ["normal"]);
    this._addCss("Combo>#combolist", "background", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount", "background", obj, ["normal"]);
    this._addCss("Div.div_WF_SearchBg", "background", obj, ["normal"]);
    this._addCss("Div.div_RM_list", "background", obj, ["normal"]);
    this._addCss("Div.div_RM_align", "background", obj, ["normal"]);
    this._addCss("Edit", "background", obj, ["normal", "pushed", "focused"]);
    this._addCss("Edit.edt_RM_amount", "background", obj, ["normal"]);
    this._addCss("Form", "background", obj, ["normal"]);
    this._addCss("Grid", "background", obj, ["normal"]);
    this._addCss("Grid>#controledit", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "background", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Grid>#controlcombo", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "background", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu", "background", obj, ["normal"]);
    this._addCss("Grid.grd_LM_menulev2", "background", obj, ["normal"]);
    this._addCss("Grid.grd_RM_align", "background", obj, ["normal"]);
    this._addCss("Grid.grd_RM_align>#body", "background", obj, ["normal", "mouseover"]);
    this._addCss(".grd_RM_amount", "background", obj, ["normal"]);
    this._addCss(".grd_RM_edt", "background", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order", "background", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order2", "background", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order>#body", "background", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "background", obj, ["normal", "mouseover"]);
    this._addCss("GroupBox", "background", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("ImageViewer", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("ListBox", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("MaskEdit", "background", obj, ["normal", "pushed", "focused"]);
    this._addCss("PopupMenu", "background", obj, ["normal"]);
    this._addCss("Spin", "background", obj, ["normal"]);
    this._addCss("Static.sta_pop_bg", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_amount", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail_duty", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailBox", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_label2", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "background", obj, ["normal"]);
    this._addCss("Static.sta_RM_title", "background", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "background", obj, ["normal"]);
    this._addCss("Tab", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("TextArea", "background", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_value("90");
    this._addCss("ChildFrame", "titlebarheight", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("ChildFrame", "color", obj, ["normal"]);
    this._addCss("Button.btn_WF_Search", "color", obj, ["normal", "selected"]);
    this._addCss("PopupDiv", "color", obj, ["normal"]);
    this._addCss("Static.sta_RM_time", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 24 Dotum");
    this._addCss("ChildFrame", "font", obj, ["normal"]);
    this._addCss("Button.btn_pop_confirm", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_pop_cancel", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_LF_info", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_listname", "font", obj, ["normal"]);
    this._addCss("Tab.tab_RM", "font", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_background("#666666ff","","","0","0","0","0","true");
    this._addCss("ChildFrame>TitlebarForm", "background", obj, ["normal"]);
    this._addCss("TitleBarControl", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#777777ff","","0","none","","","0","none","","","0","none","","");
    this._addCss("ChildFrame>TitlebarForm", "border", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 26 Dotum");
    this._addCss("ChildFrame>TitlebarForm", "font", obj, ["normal"]);
    this._addCss("Button.btn_WF_Search", "font", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_more", "font", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_tab", "font", obj, ["normal", "mouseover", "selected", "pushed"]);
    this._addCss("Buttonbtn_WF_tab", "font", obj, ["focused"]);
    this._addCss("Button.btn_WF_next", "font", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_top", "font", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_cal", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_pop", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss(".sta_RM_name", "font", obj, ["normal"]);
    this._addCss("Static.sta_pop_title", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_titlebg", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_title", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "font", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("ChildFrame>TitlebarForm", "color", obj, ["normal"]);
    this._addCss("TitleBarControl", "color", obj, ["normal", "deactivate", "disabled"]);
    this._addCss("Button", "color", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_save", "color", obj, ["pushed"]);
    this._addCss("Button.btn_WF_more", "color", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_tab", "color", obj, ["normal", "mouseover", "selected", "pushed"]);
    this._addCss("Buttonbtn_WF_tab", "color", obj, ["focused"]);
    this._addCss("Button.btn_WF_next", "color", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_Month", "color", obj, ["focused", "selected"]);
    this._addCss("Button.btn_WF_MonthS", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_pop_confirm", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_login", "color", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Button.btn_login_pwSearch", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_naviTab", "color", obj, ["pushed"]);
    this._addCss("Button.btn_RM_naviTabS", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_cartAll", "color", obj, ["normal", "mouseover", "disable", "selected", "pushed"]);
    this._addCss("Button.btn_RM_cartMove", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_general", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "color", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "color", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar.cal_WF_month", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Combo>#combolist", "color", obj, ["selected"]);
    this._addCss("Combo.com_login_lange", "color", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "color", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "color", obj, ["normal", "selected"]);
    this._addCss("Edit.edt_login_id", "color", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "color", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "color", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "color", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("ListBox", "color", obj, ["selected"]);
    this._addCss("PopupMenu", "color", obj, ["mouseover", "focused", "selected"]);
    this._addCss("ProgressBar", "color", obj, ["normal", "disabled"]);
    this._addCss("Static.sta_WF_search", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_s", "color", obj, ["normal"]);
    this._addCss("Static.sta_Top_title", "color", obj, ["normal"]);
    this._addCss("Static.sta_RM_roundtxt", "color", obj, ["normal"]);
    this._addCss("Tab", "color", obj, ["pushed", "selected"]);
    this._addCss("Tab.tab_LF_user", "color", obj, ["pushed", "selected"]);

    obj = new nexacro.Style_align("left middle");
    this._addCss("ChildFrame>TitlebarForm", "align", obj, ["normal"]);
    this._addCss("Button.btn_LM_menu1", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar>#calendaredit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("CheckBox", "align", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo", "align", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "align", obj, ["normal"]);
    this._addCss("Combo>#combolist", "align", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "align", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "align", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "align", obj, ["normal"]);
    this._addCss("Edit", "align", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid>#controledit", "align", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcheckbox", "align", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "align", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#combolist", "align", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss(".sta_RM_name", "align", obj, ["normal"]);
    this._addCss("ListBox", "align", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Radio", "align", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Static", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailBox", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_search", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_s", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_label2", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "align", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 37");
    this._addCss("ChildFrame>TitlebarForm", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("#adadadff","","","0","0","0","0","true");
    this._addCss("TitleBarControl", "background", obj, ["deactivate"]);

    obj = new nexacro.Style_background("#eeeeeeff","","","0","0","0","0","true");
    this._addCss("TitleBarControl", "background", obj, ["disabled"]);
    this._addCss("Button", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_Search", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_more", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_tab", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_next", "background", obj, ["disabled"]);
    this._addCss("Button.btn_pop_confirm", "background", obj, ["disable"]);
    this._addCss("Button.btn_pop_cancel", "background", obj, ["disable"]);
    this._addCss("Button.btn_RM_cartAll", "background", obj, ["disable"]);
    this._addCss("Button.btn_RM_general", "background", obj, ["disable"]);
    this._addCss("Grid>#controlbutton", "background", obj, ["disabled"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("TitleBarControl>#minbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#maxbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#normalbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("TitleBarControl>#closebutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("VScrollBarControl>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("*>#resizebutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#dropbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("CheckBox", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo>#dropbutton", "bordertype", obj, ["normal", "focused", "mouseover", "pushed"]);
    this._addCss("Combo>#combolist", "bordertype", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#dropbutton", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo.cmb_RM_amount>#dropbutton", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Div", "bordertype", obj, ["normal"]);
    this._addCss("Grid", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#head", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#summ", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#summary", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid>#controlcombo>#combolist", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#resizebutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_LF_menu>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_align", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_RM_align>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order2", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("GroupBox", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("ImageViewer", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("PopupDiv", "bordertype", obj, ["normal"]);
    this._addCss("PopupMenu", "bordertype", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Static", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_login_bg", "bordertype", obj, ["normal"]);
    this._addCss("Tab", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Tab>#spinupbutton", "bordertype", obj, ["normal", "disabled", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Tab>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);
    this._addCss("Tab>#extrabutton", "bordertype", obj, ["normal"]);
    this._addCss("Tab.tab_RM", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_Titlebar_Min.png')");
    this._addCss("TitleBarControl>#minbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("50");
    this._addCss("TitleBarControl>#minbutton", "opacity", obj, ["disabled"]);
    this._addCss("TitleBarControl>#maxbutton", "opacity", obj, ["disabled"]);
    this._addCss("TitleBarControl>#normalbutton", "opacity", obj, ["disabled"]);
    this._addCss("TitleBarControl>#closebutton", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_LF_logout", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_WF_tab", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_cal", "opacity", obj, ["pushed"]);
    this._addCss("Button", "opacity", obj, ["disabled"]);
    this._addCss("Div", "opacity", obj, ["disabled"]);
    this._addCss("PopupDiv", "opacity", obj, ["disabled"]);
    this._addCss("Tab>#spinupbutton", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_Titlebar_Max.png')");
    this._addCss("TitleBarControl>#maxbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_Titlebar_Nor.png')");
    this._addCss("TitleBarControl>#normalbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_Titlebar_Close.png')");
    this._addCss("TitleBarControl>#closebutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("12");
    this._addCss("VScrollBarControl", "barminsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "barminsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "barminsize", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("VScrollBarControl", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "border", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("0");
    this._addCss("VScrollBarControl", "decbtnsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "decbtnsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "decbtnsize", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("0");
    this._addCss("VScrollBarControl", "incbtnsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "incbtnsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "incbtnsize", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("VScrollBarControl", "padding", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "padding", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button", "padding", obj, ["normal", "mouseover"]);
    this._addCss("Button.setup_attr_on", "padding", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Calendar", "padding", obj, ["normal"]);
    this._addCss("Calendar>#dropbutton", "padding", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar.cal_WF_month>#calendaredit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlbutton", "padding", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "padding", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Grid.grd_RM_align", "padding", obj, ["normal"]);
    this._addCss(".sta_RM_image", "padding", obj, ["normal"]);
    this._addCss(".sta_RM_name", "padding", obj, ["normal"]);
    this._addCss(".sta_RM_unit", "padding", obj, ["normal"]);
    this._addCss(".sta_RM_price", "padding", obj, ["normal"]);
    this._addCss(".sta_RM_priceL", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_edt", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_minus", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_plus", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_detail", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_favorite", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_cart", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_expand", "padding", obj, ["normal"]);
    this._addCss(".grd_RM_collapse", "padding", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order", "padding", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order2", "padding", obj, ["normal"]);
    this._addCss("Static.sta_RM_roundtxt", "padding", obj, ["normal"]);
    this._addCss("Tab", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Tab.tab_RM", "padding", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_value("10");
    this._addCss("VScrollBarControl", "scrollbarsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("HScrollBarControl", "scrollbarsize", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "scrollbarsize", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#fbfbfbff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl", "background", obj, ["disabled"]);
    this._addCss("HScrollBarControl", "background", obj, ["disabled"]);
    this._addCss("Combo>#combolist", "background", obj, ["disabled"]);
    this._addCss("Grid>#controledit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlmaskedit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controltextarea", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#combolist", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar", "background", obj, ["disabled"]);
    this._addCss("ListBox", "background", obj, ["disabled"]);

    obj = new nexacro.Style_value("");
    this._addCss("VScrollBarControl>#decbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("VScrollBarControl>#incbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("HScrollBarControl>#decbutton", "image", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("HScrollBarControl>#incbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Spin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "image", obj, ["normal", "disabled", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Tab>#spindownbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_value("50%");
    this._addCss("VScrollBarControl>#decbutton", "opacity", obj, ["disabled"]);
    this._addCss("VScrollBarControl>#incbutton", "opacity", obj, ["disabled"]);
    this._addCss("HScrollBarControl>#decbutton", "opacity", obj, ["disabled"]);
    this._addCss("HScrollBarControl>#incbutton", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_Top_center", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_centerS", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_left", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_leftS", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_right", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_rightS", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_menu", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_search", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_back", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Top_home", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LF_close", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LF_save", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_WF_top", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_cal", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_pop", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_LM_menu1", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu2", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu3", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu4", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu5", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu6", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu7", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_Rm_tab", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_RM_naviTab", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_RM_naviTabS", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_RM_alignlist", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_RM_alignHorz", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_RM_alignVert", "opacity", obj, ["disable"]);
    this._addCss("Button.btn_cal_monthNext", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_cal_monthPrev", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#dropbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "opacity", obj, ["disabled"]);
    this._addCss("CheckBox", "opacity", obj, ["disabled"]);
    this._addCss("CheckBox.check_login", "opacity", obj, ["disable"]);
    this._addCss("Combo>#dropbutton", "opacity", obj, ["disabled"]);
    this._addCss("Edit.edt_WF_code", "opacity", obj, ["disable"]);
    this._addCss("Grid>#controlcheckbox", "opacity", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "opacity", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "opacity", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "opacity", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "opacity", obj, ["disabled"]);
    this._addCss("Radio", "opacity", obj, ["disabled"]);
    this._addCss("Spin>#spinupbutton", "opacity", obj, ["disabled"]);
    this._addCss("Spin>#spindownbutton", "opacity", obj, ["disabled"]);
    this._addCss("Tab>#spindownbutton", "opacity", obj, ["disabled"]);
    this._addCss("Tab.tab_LF_user", "opacity", obj, ["disabled"]);
    this._addCss("Tab.tab_RM", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_background("#6c7f9aff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#trackbar", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#ffffffff","");
    this._addCss("VScrollBarControl>#trackbar", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "border", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);

    obj = new nexacro.Style_bordertype("round","5","5","true","true","true","true");
    this._addCss("VScrollBarControl>#trackbar", "bordertype", obj, ["normal", "disabled"]);
    this._addCss("HScrollBarControl>#trackbar", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_background("#cacacaff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#trackbar", "background", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#fbfbfbff","");
    this._addCss("VScrollBarControl>#trackbar", "border", obj, ["disabled"]);
    this._addCss("HScrollBarControl>#trackbar", "border", obj, ["disabled"]);

    obj = new nexacro.Style_background("#c1c1c1ff","","","0","0","0","0","true");
    this._addCss("HScrollBarControl>#trackbar", "background", obj, ["disabled"]);

    obj = new nexacro.Style_background("#dcdcdcff","","","0","0","0","0","true");
    this._addCss("*>#resizebutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Button", "align", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_Search", "align", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_more", "align", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_tab", "align", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_next", "align", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_top", "align", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_cal", "align", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_pop", "align", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Button.btn_login_pwSearch", "align", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.setup_attr_on", "align", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar.cal_WF_month", "align", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar.cal_WF_month>#calendaredit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Edit.edt_RM_amount", "align", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Static.sta_WF_amount", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_total", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "align", obj, ["normal"]);
    this._addCss("Static.sta_RM_roundtxt", "align", obj, ["normal"]);

    obj = new nexacro.Style_background("#2e333fff","","","0","0","0","0","true");
    this._addCss("Button", "background", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("hand");
    this._addCss("Button", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_save", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_Search", "cursor", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_more", "cursor", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_tab", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_next", "cursor", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_top", "cursor", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_cal", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_pop", "cursor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar", "cursor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlbutton", "cursor", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "cursor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Tab.tab_LF_user", "cursor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_font("antialias bold 22 Dotum");
    this._addCss("Button", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_save", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss(".sta_RM_price", "font", obj, ["normal"]);
    this._addCss(".sta_RM_priceL", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_amount", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_searchP", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_total", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_price", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_priceL", "font", obj, ["normal"]);
    this._addCss("Tab.tab_LF_user", "font", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);

    obj = new nexacro.Style_background("#000000ff","","","0","0","0","0","true");
    this._addCss("Button", "background", obj, ["pushed"]);
    this._addCss("Button.btn_RM_cartAll", "background", obj, ["selected", "pushed"]);
    this._addCss("Static.sta_LF_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#777777");
    this._addCss("Button", "color", obj, ["disabled"]);
    this._addCss("Button.btn_WF_more", "color", obj, ["disabled"]);
    this._addCss("Button.btn_WF_tab", "color", obj, ["disabled"]);
    this._addCss("Button.btn_WF_next", "color", obj, ["disabled"]);
    this._addCss("Button.btn_RM_cartAll", "color", obj, ["disable"]);
    this._addCss("Button.btn_RM_general", "color", obj, ["disable"]);
    this._addCss("Calendar", "color", obj, ["disabled"]);
    this._addCss("Calendar>#calendaredit", "color", obj, ["disabled"]);
    this._addCss("Combo", "color", obj, ["disabled"]);
    this._addCss("Combo>#comboedit", "color", obj, ["disabled"]);
    this._addCss("Combo>#combolist", "color", obj, ["disabled"]);
    this._addCss("Edit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controledit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlmaskedit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controltextarea", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlbutton", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#comboedit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#combolist", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "color", obj, ["disabled"]);
    this._addCss("Spin", "color", obj, ["disabled"]);
    this._addCss("Spin>#spinedit", "color", obj, ["disabled"]);
    this._addCss("Static", "color", obj, ["disabled"]);
    this._addCss("TextArea", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_centerN.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_center", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_centerS", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_centerS.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_centerS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_leftN.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_left", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_leftS", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_leftS.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_leftS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_rightN.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_right", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Top_rightS", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_rightS.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_rightS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_menu.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_menu", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_search.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_search", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_back.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_back", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_Top_home.png","","0","0","50","50","true");
    this._addCss("Button.btn_Top_home", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#080d16ff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_logout", "background", obj, ["pushed"]);

    obj = new nexacro.Style_color("#bab7b1");
    this._addCss("Button.btn_LF_logout", "color", obj, ["pushed", "disable"]);

    obj = new nexacro.Style_font("antialias bold 14 Dotum");
    this._addCss("Button.btn_LF_logout", "font", obj, ["pushed", "disable"]);

    obj = new nexacro.Style_background("#28314fff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_logout", "background", obj, ["disable"]);

    obj = new nexacro.Style_background("#262a35ff","theme://images/btn_LF_closeN.png","","0","0","50","50","true");
    this._addCss("Button.btn_LF_close", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#476cb5ff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_save", "background", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_login", "background", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_bordertype("round","4","4","true","true","true","true");
    this._addCss("Button.btn_LF_save", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_login", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Calendar>#calendaredit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Combo", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controledit", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "bordertype", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcombo", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("ListBox", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("MaskEdit", "bordertype", obj, ["normal", "pushed", "focused"]);
    this._addCss("Spin", "bordertype", obj, ["normal"]);
    this._addCss("TextArea", "bordertype", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_color("#f0f1f2");
    this._addCss("Button.btn_LF_save", "color", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#2f549dff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_save", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#fa9284ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_Search", "background", obj, ["normal", "selected", "mouseover", "focused"]);

    obj = new nexacro.Style_bordertype("round","3","3","true","true","true","true");
    this._addCss("Button.btn_WF_Search", "bordertype", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_more", "bordertype", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_tab", "bordertype", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_next", "bordertype", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_Month", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);
    this._addCss("Button.btn_WF_top", "bordertype", obj, ["normal", "selected"]);
    this._addCss("Grid>#controlbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_search.png')");
    this._addCss("Button.btn_WF_Search", "image", obj, ["normal", "selected", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Button.btn_WF_Search", "imagealign", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_more", "imagealign", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_next", "imagealign", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_top", "imagealign", obj, ["normal", "selected"]);
    this._addCss("Button.btn_WF_cal", "imagealign", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_pop", "imagealign", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Combo>#dropbutton", "imagealign", obj, ["normal", "focused", "mouseover", "pushed"]);
    this._addCss("Combo.com_login_lange>#dropbutton", "imagealign", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo.cmb_RM_amount>#dropbutton", "imagealign", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Spin>#spinupbutton", "imagealign", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "imagealign", obj, ["normal"]);

    obj = new nexacro.Style_padding("1 0 -1 0");
    this._addCss("Button.btn_WF_Search", "padding", obj, ["pushed"]);
    this._addCss("Button.btn_WF_more", "padding", obj, ["pushed"]);
    this._addCss("Button.btn_WF_next", "padding", obj, ["pushed"]);
    this._addCss("Button.btn_WF_top", "padding", obj, ["pushed"]);

    obj = new nexacro.Style_color("#909090");
    this._addCss("Button.btn_WF_Search", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#66707eff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_more", "background", obj, ["normal", "selected", "mouseover", "focused"]);
    this._addCss("Button.btn_WF_next", "background", obj, ["normal", "selected", "mouseover", "focused"]);
    this._addCss("Button.btn_pop_confirm", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_more.png')");
    this._addCss("Button.btn_WF_more", "image", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#4a525dff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_more", "background", obj, ["pushed"]);
    this._addCss("Button.btn_WF_next", "background", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_moreD.png')");
    this._addCss("Button.btn_WF_more", "image", obj, ["disabled"]);

    obj = new nexacro.Style_background("#535f79ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_tab", "background", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#4365a9ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_tab", "background", obj, ["selected", "pushed"]);
    this._addCss("Buttonbtn_WF_tab", "background", obj, ["focused"]);
    this._addCss("Tabpage.tabp_LF_user", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_next.png')");
    this._addCss("Button.btn_WF_next", "image", obj, ["normal", "selected", "mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_nextD.png')");
    this._addCss("Button.btn_WF_next", "image", obj, ["disabled"]);

    obj = new nexacro.Style_background("#f1f1f1ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_Month", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#fa7869ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_Month", "background", obj, ["focused", "selected"]);
    this._addCss("Button.btn_WF_MonthS", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disable"]);

    obj = new nexacro.Style_color("#888888");
    this._addCss("Button.btn_WF_Month", "color", obj, ["disable"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_top.png')");
    this._addCss("Button.btn_WF_top", "image", obj, ["normal", "selected", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_topP.png')");
    this._addCss("Button.btn_WF_top", "image", obj, ["pushed"]);

    obj = new nexacro.Style_border("1","solid","#cdd2d7ff","");
    this._addCss("Button.btn_WF_cal", "border", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Button.btn_WF_pop", "border", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);
    this._addCss("Calendar", "border", obj, ["normal"]);
    this._addCss("Edit", "border", obj, ["normal", "pushed", "focused"]);
    this._addCss("MaskEdit", "border", obj, ["normal", "pushed", "focused"]);
    this._addCss("TextArea", "border", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("Button.btn_WF_cal", "bordertype", obj, ["normal", "mouseover", "pushed", "selected"]);
    this._addCss("Button.btn_WF_pop", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);
    this._addCss("Calendar", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.cal_WF_month>#calendaredit", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Combo.com_login_lange", "bordertype", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "bordertype", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "bordertype", obj, ["normal"]);
    this._addCss("Edit", "bordertype", obj, ["normal", "pushed", "focused"]);
    this._addCss("Edit.edt_login_id", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_cal_dropN.png')");
    this._addCss("Button.btn_WF_cal", "image", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);
    this._addCss("Calendar>#dropbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_background("#e4e7ebff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_cal", "background", obj, ["pushed", "selected"]);
    this._addCss("Button.btn_WF_pop", "background", obj, ["selected", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_WF_popN.png')");
    this._addCss("Button.btn_WF_pop", "image", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_pop_close.png","","0","0","50","50","true");
    this._addCss("Button.btn_pop_close", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#d7d7d7ff","","","0","0","0","0","true");
    this._addCss("Button.btn_pop_confirm", "background", obj, ["pushed"]);
    this._addCss("Button.btn_pop_cancel", "background", obj, ["pushed"]);
    this._addCss("Grid>#controlbutton", "background", obj, ["pushed"]);

    obj = new nexacro.Style_font("antialias bold 28 arial");
    this._addCss("Button.btn_login", "font", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_value("30%");
    this._addCss("Button.btn_login", "opacity", obj, ["disable"]);
    this._addCss("Edit.edt_login_id", "opacity", obj, ["disable"]);
    this._addCss("Edit.edt_login_pw", "opacity", obj, ["disable"]);

    obj = new nexacro.Style_font("underline antialias bold 20 arial");
    this._addCss("Button.btn_login_pwSearch", "font", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/img_main_bg1.png","","0","0","50","50","true");
    this._addCss("Button.btn_CM_notice", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_CM_main_bg1", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_bg2.png","","0","0","50","50","true");
    this._addCss("Button.btn_CM_as", "background", obj, ["normal"]);
    this._addCss("Button.btn_CM_notice", "background", obj, ["mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_CM_main_bg2", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_bg3.png","","0","0","50","50","true");
    this._addCss("Button.btn_CM_data", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_CM_main_bg3", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_bg4.png","","0","0","50","50","true");
    this._addCss("Button.btn_CM_news", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_CM_main_bg4", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f1f9fdff","theme://images/img_LM_D1arrow.png","","0","0","100","50","true");
    this._addCss("Button.btn_LM_menu1", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_logistics.png')");
    this._addCss("Button.btn_LM_menu1", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "image", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("Button.btn_LM_menu1", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "imagepadding", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_align("left middle");
    this._addCss("Button.btn_LM_menu1", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "imagealign", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_padding("0 0 0 50");
    this._addCss("Button.btn_LM_menu1", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_border("1","solid","#828584ff","","0","none","","","1","solid","#828584ff","","0","none","","");
    this._addCss("Button.btn_LM_menu1", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "border", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "border", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_font("antialias bold 28 Dotum");
    this._addCss("Button.btn_LM_menu1", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu2", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu3", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu5", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu7", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTab", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_naviTabS", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_RM_cartAll", "font", obj, ["normal", "mouseover", "disable", "selected", "pushed"]);
    this._addCss("Button.btn_RM_cartMove", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_general", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_Top_title", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_t1", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts1", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_t2", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts2", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_t3", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts3", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_t4", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts4", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","theme://images/img_LM_D1arrow.png","","0","0","100","50","true");
    this._addCss("Button.btn_LM_menu2", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu4", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_LM_menu6", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_taste.png')");
    this._addCss("Button.btn_LM_menu2", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_kitchen.png')");
    this._addCss("Button.btn_LM_menu3", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_service.png')");
    this._addCss("Button.btn_LM_menu4", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_safety.png')");
    this._addCss("Button.btn_LM_menu5", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_business.png')");
    this._addCss("Button.btn_LM_menu6", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/img_LM_information.png')");
    this._addCss("Button.btn_LM_menu7", "image", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#e2e2e2ff","theme://images/img_LM_D1arrow.png","","0","0","100","50","true");
    this._addCss("Button.btn_LM_menu1", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu2", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu3", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu4", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu5", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu6", "background", obj, ["pushed"]);
    this._addCss("Button.btn_LM_menu7", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#ecececff","theme://images/img_LM_D1arrow.png","","0","0","100","50","true");
    this._addCss("Button.btn_LM_menu1", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu2", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu3", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu4", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu5", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu6", "background", obj, ["disable"]);
    this._addCss("Button.btn_LM_menu7", "background", obj, ["disable"]);

    obj = new nexacro.Style_background("#f5f7faff","","","0","0","0","0","true");
    this._addCss("Button.btn_Rm_tab", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Div.div_RM_tab", "background", obj, ["normal"]);
    this._addCss("Static.sta_RM_tab", "background", obj, ["normal"]);
    this._addCss("Static.sta_RM_tabbg", "background", obj, ["normal"]);
    this._addCss("Tab.tab_RM", "background", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_border("1","solid","#bfbfbfff","");
    this._addCss("Button.btn_Rm_tab", "border", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("Button.btn_Rm_tab", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Rm_tabS", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_Rm_tabNext", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Button.btn_Rm_tabPrev", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_color("#666666");
    this._addCss("Button.btn_Rm_tab", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Tab", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Tab.tab_RM", "color", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_border("5","solid","#fa9284ff","","1","solid","#bfbfbfff","","1","solid","#ffffffff","","1","solid","#bfbfbfff","");
    this._addCss("Button.btn_Rm_tabS", "border", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#f5f7faff","theme://images/btn_Rm_tabNext_N.png","","0","0","50","50","true");
    this._addCss("Button.btn_Rm_tabNext", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#f5f7faff","theme://images/btn_Rm_tabNext_D.png","","0","0","50","50","true");
    this._addCss("Button.btn_Rm_tabNext", "background", obj, ["disable"]);

    obj = new nexacro.Style_background("#f5f7faff","theme://images/btn_Rm_tabPrev_N.png","","0","0","50","50","true");
    this._addCss("Button.btn_Rm_tabPrev", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#f5f7faff","theme://images/btn_Rm_tabPrev_D.png","","0","0","50","50","true");
    this._addCss("Button.btn_Rm_tabPrev", "background", obj, ["disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_naviTab.png","stretch","40","40","0","0","true");
    this._addCss("Button.btn_RM_naviTab", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Button.btn_RM_naviTab", "color", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Calendar", "color", obj, ["normal"]);
    this._addCss("Calendar>#calendaredit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("CheckBox", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo", "color", obj, ["normal"]);
    this._addCss("Combo>#combolist", "color", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount", "color", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "color", obj, ["normal"]);
    this._addCss("Div", "color", obj, ["normal"]);
    this._addCss("Edit", "color", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid", "color", obj, ["normal"]);
    this._addCss("Grid>#controledit", "color", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "color", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "color", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlbutton", "color", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Grid>#controlcheckbox", "color", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo", "color", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "color", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "color", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid.grd_RM_align", "color", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order", "color", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order2", "color", obj, ["normal"]);
    this._addCss("ImageViewer", "color", obj, ["normal", "mouseover", "focused"]);
    this._addCss("ListBox", "color", obj, ["normal", "mouseover", "focused"]);
    this._addCss("MaskEdit", "color", obj, ["normal", "pushed", "focused"]);
    this._addCss("PopupMenu", "color", obj, ["normal"]);
    this._addCss("Radio", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Spin", "color", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "color", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Static", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_amount", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_label2", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "color", obj, ["normal"]);
    this._addCss("Static.sta_RM_txt", "color", obj, ["normal"]);
    this._addCss("Tab.tab_RM", "color", obj, ["selected"]);
    this._addCss("TextArea", "color", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_naviTabS.png","stretch","40","40","0","0","true");
    this._addCss("Button.btn_RM_naviTab", "background", obj, ["pushed"]);
    this._addCss("Button.btn_RM_naviTabS", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignList_N.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignlist", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignList_S.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignlistS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignHorz_N.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignHorz", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignHorz_S.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignHorzS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignVert_N.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignVert", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_alignVert_S.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_alignVertS", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#ffffffff","theme://images/btn_RM_minus.png","","0","0","50","50","true");
    this._addCss("Button.btn_RM_minus", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#777777ff","");
    this._addCss("Button.btn_RM_minus", "border", obj, ["normal"]);
    this._addCss("Button.btn_RM_plus", "border", obj, ["normal"]);
    this._addCss("Combo", "border", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount", "border", obj, ["normal"]);
    this._addCss("Grid>#controledit", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "border", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcombo", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "border", obj, ["normal", "mouseover"]);
    this._addCss("ListBox", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","30","30","true","false","true","false");
    this._addCss("Button.btn_RM_minus", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","theme://images/btn_RM_plus.png","","0","0","50","50","true");
    this._addCss("Button.btn_RM_plus", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","30","30","false","true","false","true");
    this._addCss("Button.btn_RM_plus", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("#171717ff","","","0","0","0","0","true");
    this._addCss("Button.btn_RM_cartAll", "background", obj, ["normal", "mouseover", "disable"]);

    obj = new nexacro.Style_background("#b5bdc8ff","","","0","0","0","0","true");
    this._addCss("Button.btn_RM_cartMove", "background", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_RM_cartAll", "background", obj, ["disable"]);
    this._addCss("Button.btn_RM_general", "background", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Static.sta_RM_roundtxt", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","6","6","true","true","true","true");
    this._addCss("Button.btn_RM_general", "bordertype", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_detailN.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_detail", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_detailP.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_detailP", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_favoriteN.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_favorite", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_favoriteP.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_favoriteP", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_cart.png","","0","0","0","0","true");
    this._addCss("Button.btn_RM_cart", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_cal_nextN.png","","0","0","50","50","true");
    this._addCss("Button.btn_cal_monthNext", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_cal_prevN.png","","0","0","50","50","true");
    this._addCss("Button.btn_cal_monthPrev", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("60");
    this._addCss("Calendar", "buttonsize", obj, ["normal"]);
    this._addCss("Combo.com_login_lange", "buttonsize", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount", "buttonsize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "buttonsize", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar", "daybackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daybackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar", "dayborder", obj, ["normal", "pushed", "selected"]);
    this._addCss("Grid>#controlcalendar", "dayborder", obj, ["normal", "mouseover", "focused", "selected"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar", "daybordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daybordertype", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Calendar", "daycolor", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "daycolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar", "daycolor", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("antialias 22 Dotum");
    this._addCss("Calendar", "dayfont", obj, ["normal"]);

    obj = new nexacro.Style_value("84 84");
    this._addCss("Calendar", "daysize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daysize", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias 22 Dotum");
    this._addCss("Calendar", "font", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("CheckBox", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo", "font", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "font", obj, ["normal"]);
    this._addCss("Combo>#combolist", "font", obj, ["normal", "selected"]);
    this._addCss("Combo.cmb_RM_amount", "font", obj, ["normal"]);
    this._addCss("Edit", "font", obj, ["normal", "pushed", "focused"]);
    this._addCss("Edit.edt_WF_code", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Edit.edt_RM_amount", "font", obj, ["normal"]);
    this._addCss(".sta_RM_unit", "font", obj, ["normal"]);
    this._addCss("ListBox", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("MaskEdit", "font", obj, ["normal", "pushed", "focused"]);
    this._addCss("Radio", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Spin", "font", obj, ["normal"]);
    this._addCss("Static", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail_duty", "font", obj, ["normal"]);
    this._addCss("Tab", "font", obj, ["normal", "mouseover", "focused", "disabled", "pushed", "selected"]);
    this._addCss("TextArea", "font", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/img_cal_monthbg.png","","0","0","0","0","true");
    this._addCss("Calendar", "popupbackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar", "popupborder", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupborder", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar", "popupbordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupbordertype", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("589 697");
    this._addCss("Calendar", "popupsize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupsize", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("#777777");
    this._addCss("Calendar", "selectbackground", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "selectbackground", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "selectbackground", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#controledit", "selectbackground", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "selectbackground", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "selectbackground", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcombo>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "selectbackground", obj, ["normal", "mouseover"]);
    this._addCss("Spin>#spinedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Calendar", "selectcolor", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "selectcolor", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "selectcolor", obj, ["normal"]);
    this._addCss("Combo.cmb_RM_amount>#comboedit", "selectcolor", obj, ["normal"]);
    this._addCss("Edit", "selectcolor", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid>#controledit", "selectcolor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "selectcolor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "selectcolor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcombo>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "selectcolor", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu>#body", "selectcolor", obj, ["normal", "mouseover"]);
    this._addCss("MaskEdit", "selectcolor", obj, ["normal", "pushed", "focused"]);
    this._addCss("Spin>#spinedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("TextArea", "selectcolor", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/img_cal_dayS.png","","0","0","0","0","true");
    this._addCss("Calendar", "daybackground", obj, ["pushed", "selected"]);
    this._addCss("Grid>#controlcalendar", "daybackground", obj, ["focused", "selected"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Calendar", "daycolor", obj, ["pushed", "selected"]);
    this._addCss("Grid>#controlcalendar", "daycolor", obj, ["focused", "selected"]);

    obj = new nexacro.Style_background("#f0f0f0ff","","","0","0","0","0","true");
    this._addCss("Calendar", "background", obj, ["disabled"]);
    this._addCss("Combo", "background", obj, ["disabled"]);
    this._addCss("Combo>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Edit", "background", obj, ["disabled"]);
    this._addCss("MaskEdit", "background", obj, ["disabled"]);
    this._addCss("ProgressBar", "background", obj, ["disabled"]);
    this._addCss("Static.sta_WF_titlebg", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "background", obj, ["normal"]);
    this._addCss("TextArea", "background", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#c9c9c9ff","");
    this._addCss("Calendar", "border", obj, ["disabled", "readonly"]);
    this._addCss("Combo", "border", obj, ["disabled", "readonly"]);
    this._addCss("Combo>#combolist", "border", obj, ["disabled"]);
    this._addCss("Edit", "border", obj, ["disabled", "readonly"]);
    this._addCss("Grid>#controledit", "border", obj, ["disabled"]);
    this._addCss("Grid>#controlmaskedit", "border", obj, ["disabled"]);
    this._addCss("Grid>#controltextarea", "border", obj, ["disabled"]);
    this._addCss("Grid>#controlbutton", "border", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo", "border", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#combolist", "border", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar", "border", obj, ["disabled"]);
    this._addCss("ImageViewer", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("ListBox", "border", obj, ["disabled"]);
    this._addCss("MaskEdit", "border", obj, ["disabled", "readonly"]);
    this._addCss("Spin", "border", obj, ["disabled", "readonly"]);
    this._addCss("TextArea", "border", obj, ["disabled", "readonly"]);

    obj = new nexacro.Style_background("#f8f8f8ff","","","0","0","0","0","true");
    this._addCss("Calendar", "background", obj, ["readonly"]);
    this._addCss("Combo", "background", obj, ["readonly"]);
    this._addCss("Edit", "background", obj, ["readonly"]);
    this._addCss("MaskEdit", "background", obj, ["readonly"]);
    this._addCss("Spin", "background", obj, ["readonly"]);
    this._addCss("TextArea", "background", obj, ["readonly"]);

    obj = new nexacro.Style_font("bold antialias 15 Dotum");
    this._addCss("Calendar>#calendaredit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_padding("0 0 0 5");
    this._addCss("Calendar>#calendaredit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar.essential", "padding", obj, ["normal", "mouseover"]);
    this._addCss("Combo.essential", "padding", obj, ["normal", "mouseover"]);
    this._addCss("Static.sta_WF_detail", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_detailP", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_detail_duty", "padding", obj, ["normal"]);

    obj = new nexacro.Style_margin("2 0 -2 0");
    this._addCss("Calendar>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_margin("-2 0 2 0");
    this._addCss("Calendar>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "bodybackground", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodybackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "bodyborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodyborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "bodybordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodybordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("yyyy.MM");
    this._addCss("Calendar>#popupcalendar", "headerformat", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerformat", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("99");
    this._addCss("Calendar>#popupcalendar", "headerheight", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerheight", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Calendar>#popupcalendar", "headercolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headercolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "headerbackground", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerbackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "headerborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "headerbordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerbordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("antialias bold 34 Dotum");
    this._addCss("Calendar>#popupcalendar", "headerfont", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerfont", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_padding("100 0 0 0");
    this._addCss("Calendar>#popupcalendar", "ncpadding", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "ncpadding", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybackground", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "saturdayborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdayborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("");
    this._addCss("Calendar>#popupcalendar", "saturdayfont", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdayfont", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#3b4873");
    this._addCss("Calendar>#popupcalendar", "saturdaycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "sundaybackground", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "sundayborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundayborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "sundaybordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#ca3e26");
    this._addCss("Calendar>#popupcalendar", "sundaycolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("");
    this._addCss("Calendar>#popupcalendar", "sundayfont", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundayfont", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Calendar>#popupcalendar", "todaycolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/img_cal_dayT.png","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "todaybackground", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaybackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "todayborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todayborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "todaybordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaybordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("antialias bold 24 Dotum");
    this._addCss("Calendar>#popupcalendar", "todayfont", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todayfont", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#d4d4d4");
    this._addCss("Calendar>#popupcalendar", "trailingdaycolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdaycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("true");
    this._addCss("Calendar>#popupcalendar", "usetrailingday", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "usetrailingday", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("일 월 화 수 목 금 토");
    this._addCss("Calendar>#popupcalendar", "weekformat", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekformat", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Calendar>#popupcalendar", "weekcolor", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekcolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#fbfbfbff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "weekbackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("antialias 24 Dotum");
    this._addCss("Calendar>#popupcalendar", "weekfont", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekfont", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_margin("0 0 0 0");
    this._addCss("Calendar>#popupcalendar", "margin", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "margin", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "margin", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Spin>#spinupbutton", "margin", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_cal_prevN.png')");
    this._addCss("Calendar>#popupcalendar>#prevbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_cal_nextN.png')");
    this._addCss("Calendar>#popupcalendar>#nextbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("0");
    this._addCss("Calendar>#popupcalendar>#yearspin", "buttonsize", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "buttonsize", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "buttonsize", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "buttonsize", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar.cal_WF_month", "buttonsize", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "buttonsize", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "buttonsize", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "buttonsize", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "buttonsize", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Spin", "buttonsize", obj, ["normal", "readonly"]);

    obj = new nexacro.Style_font("antialias 28 Dotum");
    this._addCss("Calendar>#popupcalendar>#yearspin", "font", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "font", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinedit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinedit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin", "font", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin", "font", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#yearspin>#spinedit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#monthspin>#spinedit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_background("#ffffffff","theme://images/img_required.png","","0","0","0","0","true");
    this._addCss("Calendar.essential", "background", obj, ["normal", "mouseover"]);
    this._addCss("Combo.essential", "background", obj, ["normal", "mouseover"]);
    this._addCss("Edit.essential", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("MaskEdit.essential", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin.essential", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("TextArea.essential", "background", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_border("1","solid","#fa7869ff","");
    this._addCss("Calendar.essential", "border", obj, ["normal", "mouseover"]);
    this._addCss("Combo.essential", "border", obj, ["normal", "mouseover"]);
    this._addCss("Edit.essential", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin.essential", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("TextArea.essential", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_font("antialias bold 34 Dotum");
    this._addCss("Calendar.cal_WF_month", "font", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_padding("0 -1 0 0");
    this._addCss("Calendar.cal_WF_month", "padding", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_border("1","solid","#c3c3c3ff","");
    this._addCss("Calendar.cal_WF_month", "border", obj, ["readonly"]);

    obj = new nexacro.Style_color("#ffffffrntfontttt: antialias bold 34 Dotum");
    this._addCss("Calendar.cal_WF_month>#calendaredit", "color", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_value("left middle");
    this._addCss("CheckBox", "buttonalign", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Radio", "buttonalign", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/img_check_bgN.png","","0","0","0","0","true");
    this._addCss("CheckBox", "buttonbackground", obj, ["normal", "mouseover", "pushed", "readonly"]);
    this._addCss("Grid>#controlcheckbox", "buttonbackground", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("0 none");
    this._addCss("CheckBox", "buttonborder", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Tab", "buttonborder", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_check.png')");
    this._addCss("CheckBox", "buttonimage", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcheckbox", "buttonimage", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("46");
    this._addCss("CheckBox", "buttonsize", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("CheckBox.check_login", "buttonsize", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Radio", "buttonsize", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_padding("0 0 0 20");
    this._addCss("CheckBox", "textpadding", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcheckbox", "textpadding", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Radio", "textpadding", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/img_checkD.png')");
    this._addCss("CheckBox", "buttonimage", obj, ["disabled"]);
    this._addCss("Grid>#controlcheckbox", "buttonimage", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Check.png')");
    this._addCss("CheckBox", "buttonimage", obj, ["readonly"]);

    obj = new nexacro.Style_background("#666a6eff","","","0","0","0","0","true");
    this._addCss("CheckBox.check_login", "buttonbackground", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("0 none");
    this._addCss("CheckBox.check_login", "buttonborder", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Grid>#controlcheckbox", "buttonborder", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Tab.tab_RM", "buttonborder", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("CheckBox.check_login", "buttonbordertype", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_color("#62656d");
    this._addCss("CheckBox.check_login", "color", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_font("antialias 20 Dotum");
    this._addCss("CheckBox.check_login", "font", obj, ["normal", "mouseover", "pushed", "disable"]);
    this._addCss("Grid", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "font", obj, ["normal"]);
    this._addCss("Static.sta_RM_unit", "font", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_login_check.png')");
    this._addCss("CheckBox.check_login", "buttonimage", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_padding("0 0 0 15");
    this._addCss("CheckBox.check_login", "textpadding", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_value("52");
    this._addCss("Combo", "buttonsize", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Combo", "itembackground", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itembackground", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itembackground", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itembackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ebebebff","","0","none","","");
    this._addCss("Combo", "itemborder", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("ListBox", "itemborder", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Combo", "itembordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itembordertype", obj, ["normal"]);
    this._addCss("PopupMenu", "itembordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Combo", "itemcolor", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itemcolor", obj, ["normal"]);

    obj = new nexacro.Style_value("65");
    this._addCss("Combo", "itemheight", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itemheight", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "itemheight", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itemheight", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemheight", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_padding("0 10 0 10");
    this._addCss("Combo", "itempadding", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itempadding", obj, ["normal"]);
    this._addCss("ListBox", "itempadding", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_padding("0 0 0 10");
    this._addCss("Combo", "padding", obj, ["normal"]);
    this._addCss("Edit.edt_WF_code", "padding", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Grid>#controlcombo", "padding", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","4","4rntcolorttt:","true","true","true","true");
    this._addCss("Combo>#comboedit", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "bordertype", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_padding("1 3 0 3");
    this._addCss("Combo>#comboedit", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "padding", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/cmb_WF_dropN.png')");
    this._addCss("Combo>#dropbutton", "image", obj, ["normal", "focused", "mouseover", "pushed"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "image", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_border("1","solid","#b2b2b2ff","");
    this._addCss("Combo>#combolist", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#fff1e9ff","","","0","0","0","0","true");
    this._addCss("Combo>#combolist", "itembackground", obj, ["mouseover", "pushed"]);
    this._addCss("Combo.com_login_lange>#combolist", "itembackground", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#fb9387ff","","","0","0","0","0","true");
    this._addCss("Combo>#combolist", "itembackground", obj, ["selected"]);
    this._addCss("Combo.com_login_lange>#combolist", "itembackground", obj, ["selected"]);

    obj = new nexacro.Style_background("#41454aff","","","0","0","0","0","true");
    this._addCss("Combo.com_login_lange", "background", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "background", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 25");
    this._addCss("Combo.com_login_lange", "padding", obj, ["normal"]);
    this._addCss("Static.sta_pop_title", "padding", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 22 arial");
    this._addCss("Combo.com_login_lange", "font", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#comboedit", "font", obj, ["normal"]);
    this._addCss("Combo.com_login_lange>#combolist", "font", obj, ["selected"]);
    this._addCss("Edit.edt_login_id", "font", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "font", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_value("80%");
    this._addCss("Combo.com_login_lange", "opacity", obj, ["normal"]);
    this._addCss("Edit.edt_login_id", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_value("URL('theme://images/cmb_RM_drop.png')");
    this._addCss("Combo.com_login_lange>#dropbutton", "image", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Combo.cmb_RM_amount>#dropbutton", "image", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_font("antialias 22 arial");
    this._addCss("Combo.com_login_lange>#combolist", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#41454aff","","","0","0","0","0","true");
    this._addCss("Combo.com_login_lange>#combolist", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","30","30","true","true","true","true");
    this._addCss("Combo.cmb_RM_amount", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_amount", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_padding("1 0 0 25");
    this._addCss("Combo.cmb_RM_amount", "padding", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("Combo.cmb_RM_amount>#comboedit", "font", obj, ["normal"]);
    this._addCss("Div", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "font", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "font", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("GroupBox", "font", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("1 0 0 10");
    this._addCss("Combo.cmb_RM_amount>#comboedit", "padding", obj, ["normal"]);

    obj = new nexacro.Style_border("3","solid","#272c33ff","","0","none","","","1","solid","#aaaaaaff","","0","none","","");
    this._addCss("Div.div_WF_SearchBg", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#9a9a9aff","","0","none","","","1","solid","#9a9a9aff","","0","none","","");
    this._addCss("Div.div_RM_tab", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#ff491fff","","","0","0","0","0","true");
    this._addCss("Div.setup_attr_on", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","24","24","true","true","true","true");
    this._addCss("Div.setup_attr_on", "bordertype", obj, ["normal"]);
    this._addCss("Div.setup_attr_off", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("#575d69ff","","","0","0","0","0","true");
    this._addCss("Div.setup_attr_off", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#999999");
    this._addCss("Edit", "displaynulltextcolor", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid>#controledit", "displaynulltextcolor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "displaynulltextcolor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("TextArea", "displaynulltextcolor", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_padding("0 10 0 10");
    this._addCss("Edit", "padding", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid>#controledit", "padding", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "padding", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("MaskEdit", "padding", obj, ["normal", "pushed", "focused"]);

    obj = new nexacro.Style_value("#777777");
    this._addCss("Edit", "selectbackground", obj, ["normal", "pushed", "focused"]);
    this._addCss("MaskEdit", "selectbackground", obj, ["normal", "pushed", "focused"]);
    this._addCss("TextArea", "selectbackground", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_color("#777777");
    this._addCss("Edit", "caretcolor", obj, ["normal", "pushed", "focused"]);
    this._addCss("Grid>#controledit", "caretcolor", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_background("#e5e5e5ff","","","0","0","0","0","true");
    this._addCss("Edit", "background", obj, ["pushed"]);
    this._addCss("Grid>#controledit", "background", obj, ["pushed"]);
    this._addCss("Grid>#controlmaskedit", "background", obj, ["pushed"]);
    this._addCss("Spin", "background", obj, ["pushed"]);
    this._addCss("TextArea", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#41454aff","theme://images/img_login_id.png","","0","0","0","50","true");
    this._addCss("Edit.edt_login_id", "background", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_padding("0 0 0 85");
    this._addCss("Edit.edt_login_id", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);
    this._addCss("Edit.edt_login_pw", "padding", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_background("#41454aff","theme://images/img_login_pw.png","","0","0","0","50","true");
    this._addCss("Edit.edt_login_pw", "background", obj, ["normal", "mouseover", "pushed", "focused", "disable"]);

    obj = new nexacro.Style_color("#3852a9ff");
    this._addCss("Edit.edt_WF_code", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_border("1","solid","#777777ff","");
    this._addCss("Edit.edt_RM_amount", "border", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("Edit.essential", "padding", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Spin.essential", "padding", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#eaeaf2ff","","","0","0","0","0","true");
    this._addCss("Form.frm_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#cdd2d7ff","");
    this._addCss("Grid", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_treeclose.png')");
    this._addCss("Grid", "treeclosebuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_treeitem.png')");
    this._addCss("Grid", "treeitemimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_treeopen.png')");
    this._addCss("Grid", "treeopenbuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_treeopen.png')");
    this._addCss("Grid", "treecollapseimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_treeclose.png')");
    this._addCss("Grid", "treeexpandimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#575c67ff","","","0","0","0","0","true");
    this._addCss("Grid>#head", "cellbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#575c67ff","","","0","0","0","0","true");
    this._addCss("Grid>#head", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Grid>#head", "cellalign", obj, ["normal"]);
    this._addCss("Grid>#body", "cellalign", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Grid>#head", "cellcolor", obj, ["normal"]);
    this._addCss("Grid.grd_LF_menu>#body", "cellcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Grid>#head", "cellcolor2", obj, ["normal"]);
    this._addCss("Grid.grd_LF_menu>#body", "cellcolor2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias bold 19 Dotum");
    this._addCss("Grid>#head", "cellfont", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","1","solid","#cdd2d7ff","","1","solid","#aaaaaaff","","0","none","","");
    this._addCss("Grid>#head", "cellline", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("Grid>#body", "cellcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("Grid>#body", "cellcolor2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias 19 Dotum");
    this._addCss("Grid>#body", "cellfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","","1","solid","#cdd2d7ff","","1","solid","#cdd2d7ff","","0","none","","");
    this._addCss("Grid>#body", "cellline", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#summ", "cellline", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellline", obj, ["normal"]);

    obj = new nexacro.Style_padding("5 5 5 5");
    this._addCss("Grid>#body", "cellpadding", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#summ", "cellpadding", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellpadding", obj, ["normal"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Grid>#body", "selectcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias 19 Dotum");
    this._addCss("Grid>#body", "selectfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("3","solid","crimson","","1","solid","#cdd2d7ff","");
    this._addCss("Grid>#body", "selectline", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#deecfcff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground", obj, ["pushed"]);

    obj = new nexacro.Style_background("#deecfcff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground2", obj, ["pushed"]);

    obj = new nexacro.Style_color("#777777");
    this._addCss("Grid>#body", "cellcolor", obj, ["disabled"]);

    obj = new nexacro.Style_color("#777777");
    this._addCss("Grid>#body", "cellcolor2", obj, ["disabled"]);

    obj = new nexacro.Style_background("#dae0e7ff","","","0","0","0","0","true");
    this._addCss("Grid>#summ", "cellbackground", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#dae0e7ff","","","0","0","0","0","true");
    this._addCss("Grid>#summ", "cellbackground2", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Grid>#summ", "cellcolor", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellcolor", obj, ["normal"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "cellcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Grid>#summ", "cellcolor2", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellcolor2", obj, ["normal"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "cellcolor2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias bold 19 Dotum");
    this._addCss("Grid>#summ", "selectfont", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectfont", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 19 Dotum");
    this._addCss("Grid>#controledit", "font", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlmaskedit", "font", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controltextarea", "font", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcheckbox", "font", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcombo", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#combolist", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "font", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_align("right middle");
    this._addCss("Grid>#controlmaskedit", "align", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlexpand", "align", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss(".sta_RM_price", "align", obj, ["normal"]);
    this._addCss("MaskEdit", "align", obj, ["normal", "pushed", "focused"]);
    this._addCss("Spin", "align", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "align", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Static.sta_CM_main_t2", "align", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts2", "align", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_t4", "align", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts4", "align", obj, ["normal"]);

    obj = new nexacro.Style_align("left top");
    this._addCss("Grid>#controltextarea", "align", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("TextArea", "align", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_value("10");
    this._addCss("Grid>#controltextarea", "linespace", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("TextArea", "linespace", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_padding("10 10 10 10");
    this._addCss("Grid>#controltextarea", "padding", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("TextArea", "padding", obj, ["normal", "focused", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#fc796bff","");
    this._addCss("Grid>#controltextarea", "border", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#8a8a8aff","");
    this._addCss("Grid>#controlbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_font("antialias bold 19 Dotum");
    this._addCss("Grid>#controlbutton", "font", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("left middle");
    this._addCss("Grid>#controlcheckbox", "buttonalign", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("42");
    this._addCss("Grid>#controlcheckbox", "buttonsize", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Grid>#controlcombo", "itemborder", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemborder", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_font("");
    this._addCss("Grid>#controlcombo", "itemfont", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 19 Dotum");
    this._addCss("Grid>#controlcalendar", "dayfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("1 4 0 4");
    this._addCss("Grid>#controlcalendar>#calendaredit", "padding", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_padding("1 0 0 0");
    this._addCss("Grid>#controlcalendar>#dropbutton", "padding", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_color("#117dbb");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaycolor", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekbackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("","theme://images/btn_grd_expand.png","","0","0","50","50","true");
    this._addCss("Grid>#controlexpand", "background", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_line("0","none","","");
    this._addCss("Grid>#controlexpand", "focusborder", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("100%");
    this._addCss("Grid>#controlexpand", "opacity", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("0 1");
    this._addCss("Grid>#controlexpand", "pusheddrawoffset", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_background("#f7f7f7ff","","","0","0","0","0","true");
    this._addCss("Grid>#resizebutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c9c9c9ff","","0","none","","","0","none","","","1","solid","#c9c9c9ff","");
    this._addCss("Grid>#resizebutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#1c212aff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","","","0","none","","");
    this._addCss("Grid.grd_LF_menu", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LF_treecollapse.png')");
    this._addCss("Grid.grd_LF_menu", "treeclosebuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LF_treeitem.png')");
    this._addCss("Grid.grd_LF_menu", "treeitemimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LF_treeexpand.png')");
    this._addCss("Grid.grd_LF_menu", "treeopenbuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LF_treecollapse.png')");
    this._addCss("Grid.grd_LF_menu", "treecollapseimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LF_treeexpand.png')");
    this._addCss("Grid.grd_LF_menu", "treeexpandimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#1c212aff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LF_menu>#body", "cellbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#1c212aff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LF_menu>#body", "cellbackground2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_align("left middle");
    this._addCss("Grid.grd_LF_menu>#body", "cellalign", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu>#body", "cellalign", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "cellalign", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_align>#body", "cellalign", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "cellalign", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "cellalign", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias 24 Dotum");
    this._addCss("Grid.grd_LF_menu>#body", "cellfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu>#body", "cellfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("1","solid","#3e424aff","");
    this._addCss("Grid.grd_LF_menu>#body", "cellline", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("0 0 0 30");
    this._addCss("Grid.grd_LF_menu>#body", "cellpadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("#080d16");
    this._addCss("Grid.grd_LF_menu>#body", "selectbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#ff451f");
    this._addCss("Grid.grd_LF_menu>#body", "selectcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias bold 24 Dotum");
    this._addCss("Grid.grd_LF_menu>#body", "selectfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_WF_menu>#body", "selectfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("1","solid","#3e424aff","");
    this._addCss("Grid.grd_LF_menu>#body", "selectline", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#303131ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LF_menu>#vscrollbar", "background", obj, ["normal", "mouseover", "pushed", "disable"]);

    obj = new nexacro.Style_background("#555555ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LF_menu>#vscrollbar>#trackbar", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#dde1e7ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_WF_menu>#body", "cellbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#dde1e7ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_WF_menu>#body", "cellbackground2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#1c212a");
    this._addCss("Grid.grd_WF_menu>#body", "cellcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#1c212a");
    this._addCss("Grid.grd_WF_menu>#body", "cellcolor2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#b7bbc2ff","","0","none","","");
    this._addCss("Grid.grd_WF_menu>#body", "line", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("0 0 0 80");
    this._addCss("Grid.grd_WF_menu>#body", "cellpadding", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_LM_menulev2>#body", "cellpadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("#ff451f");
    this._addCss("Grid.grd_WF_menu>#body", "selectbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LM_treecollapse.png')");
    this._addCss("Grid.grd_LM_menulev2", "treeclosebuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LM_treeitem.png')");
    this._addCss("Grid.grd_LM_menulev2", "treeitemimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LM_treeexpand.png')");
    this._addCss("Grid.grd_LM_menulev2", "treeopenbuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LM_treecollapse.png')");
    this._addCss("Grid.grd_LM_menulev2", "treecollapseimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_LM_treeexpand.png')");
    this._addCss("Grid.grd_LM_menulev2", "treeexpandimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#ebebebff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LM_menulev2>#body", "cellbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("#ebebebff","","","0","0","0","0","true");
    this._addCss("Grid.grd_LM_menulev2>#body", "cellbackground2", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("antialias bold 26 Dotum");
    this._addCss("Grid.grd_LM_menulev2>#body", "cellfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#b5b5b5ff","","0","none","","");
    this._addCss("Grid.grd_LM_menulev2>#body", "line", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("#d6d6d6");
    this._addCss("Grid.grd_LM_menulev2>#body", "selectbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("Grid.grd_LM_menulev2>#body", "selectcolor", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_align>#body", "selectcolor", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "selectcolor", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "selectcolor", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("");
    this._addCss("Grid.grd_LM_menulev2>#body", "selectfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_align>#body", "selectfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "selectfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "selectfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#b5b5b5ff","","0","none","","");
    this._addCss("Grid.grd_LM_menulev2>#body", "selectline", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Grid.grd_RM_align>#body", "line", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "line", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "line", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("Grid.grd_RM_align>#body", "cellpadding", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "cellpadding", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "cellpadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("#ffffff");
    this._addCss("Grid.grd_RM_align>#body", "selectbackground", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "selectbackground", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "selectbackground", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Grid.grd_RM_align>#body", "selectline", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order>#body", "selectline", obj, ["normal", "mouseover"]);
    this._addCss("Grid.grd_RM_order2>#body", "selectline", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("","theme://images/sta_RM_image.png","stretch","5","5","0","0","true");
    this._addCss(".sta_RM_image", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/sta_RM_image.png","stretch","5","5","0","0","true");
    this._addCss(".sta_RM_image", "background2", obj, ["normal"]);

    obj = new nexacro.Style_value("transparent URL('theme://images/sta_RM_image.png') stretch 5,5");
    this._addCss(".sta_RM_image", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 26 Dotum");
    this._addCss(".sta_RM_name", "selectfont", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 22 Dotum");
    this._addCss(".sta_RM_unit", "selectfont", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 22 Dotum");
    this._addCss(".sta_RM_price", "selectfont", obj, ["normal"]);
    this._addCss(".sta_RM_priceL", "selectfont", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss(".grd_RM_amount", "controlbordertype", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss(".grd_RM_amount", "controlalign", obj, ["normal"]);
    this._addCss(".grd_RM_edt", "controlalign", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 -1 0 -1");
    this._addCss(".grd_RM_amount", "padding", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","4","4","true","true","true","true");
    this._addCss(".grd_RM_edt", "controlbordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_minus.png","","0","0","0","50","true");
    this._addCss(".grd_RM_minus", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss(".grd_RM_minus", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_plus", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_detail", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_favorite", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_cart", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_expand", "controlborder", obj, ["normal"]);
    this._addCss(".grd_RM_collapse", "controlborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss(".grd_RM_minus", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_plus", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_detail", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_favorite", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_cart", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_expand", "controlbordertype", obj, ["normal"]);
    this._addCss(".grd_RM_collapse", "controlbordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_plus.png","","0","0","0","50","true");
    this._addCss(".grd_RM_plus", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_detailN.png","","0","0","100","50","true");
    this._addCss(".grd_RM_detail", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_favoriteN.png","","0","0","0","50","true");
    this._addCss(".grd_RM_favorite", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/btn_RM_cart.png","","0","0","50","50","true");
    this._addCss(".grd_RM_cart", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#b6bec9ff","theme://images/btn_RM_expand.png","","0","0","50","50","true");
    this._addCss(".grd_RM_expand", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#b6bec9ff","theme://images/btn_RM_collapse.png","","0","0","50","50","true");
    this._addCss(".grd_RM_collapse", "controlbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","2","solid","#c7c7c7ff","","0","none","","");
    this._addCss("Grid.grd_RM_order", "border", obj, ["normal"]);
    this._addCss("Grid.grd_RM_order2", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","dotted","#bbbbbbff","","0","none","","");
    this._addCss("Grid.grd_RM_order2", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#f0f0f0ff","");
    this._addCss("GroupBox", "border", obj, ["normal", "mouseover", "disabled"]);

    obj = new nexacro.Style_color("#555555");
    this._addCss("GroupBox", "color", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_padding("1 1");
    this._addCss("GroupBox", "titlepadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#999999");
    this._addCss("GroupBox", "color", obj, ["disabled"]);
    this._addCss("Tab", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("ListBox", "itembackground", obj, ["normal", "mouseover", "focused"]);
    this._addCss("PopupMenu", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_value("50");
    this._addCss("ListBox", "itemheight", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#8799c2ff","","","0","0","0","0","true");
    this._addCss("ListBox", "itembackground", obj, ["selected"]);

    obj = new nexacro.Style_color("#a1a1a1");
    this._addCss("ListBox", "color", obj, ["disabled"]);
    this._addCss("MaskEdit", "color", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#faa993ff","");
    this._addCss("MaskEdit.essential", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_font("9 Gulim");
    this._addCss("PopupDiv", "font", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#3598dbff","");
    this._addCss("PopupMenu", "border", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 9 Dotum");
    this._addCss("PopupMenu", "font", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ebebebff","","0","none","","");
    this._addCss("PopupMenu", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_value("24");
    this._addCss("PopupMenu", "itemheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 10");
    this._addCss("PopupMenu", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#1e90ffff","","","0","0","0","0","true");
    this._addCss("PopupMenu", "itembackground", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#aeaeae");
    this._addCss("PopupMenu", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#e6e6e6ff","","","0","0","0","0","true");
    this._addCss("ProgressBar", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#fc7e6d");
    this._addCss("ProgressBar", "barcolor", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("ProgressBar", "bargradation", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("ProgressBar", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("forward");
    this._addCss("ProgressBar", "direction", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Progressbar.png')");
    this._addCss("ProgressBar", "startimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Progressbar.png')");
    this._addCss("ProgressBar", "endimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Progressbar.png')");
    this._addCss("ProgressBar", "progressimage", obj, ["normal"]);

    obj = new nexacro.Style_value("true");
    this._addCss("ProgressBar", "smooth", obj, ["normal"]);

    obj = new nexacro.Style_color("#cccccc");
    this._addCss("ProgressBar", "barcolor", obj, ["disabled"]);

    obj = new nexacro.Style_background("","theme://images/img_radio_bgN.png","","0","0","0","0","true");
    this._addCss("Radio", "buttonbackground", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("0 none");
    this._addCss("Radio", "buttonborder", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Radio", "buttonbordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/img_radio.png')");
    this._addCss("Radio", "buttonimage", obj, ["normal", "mouseover", "pushed", "readonly"]);

    obj = new nexacro.Style_value("URL('theme://images/img_radioD.png')");
    this._addCss("Radio", "buttonimage", obj, ["disabled"]);

    obj = new nexacro.Style_background("","theme://images/img_radio_bgN.png","","0","0","0","0","true");
    this._addCss("Radio", "buttonbackground", obj, ["readonly"]);

    obj = new nexacro.Style_value("right");
    this._addCss("Spin", "buttonalign", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 10 0 0");
    this._addCss("Spin", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("#e0e0e0ff","","","0","0","0","0","true");
    this._addCss("Spin", "background", obj, ["disabled"]);

    obj = new nexacro.Style_font("paddingttt: 0 5 0 5");
    this._addCss("Spin>#spinedit", "font", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_margin("-1 0 1 0");
    this._addCss("Spin>#spindownbutton", "margin", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","15","15","true","true","true","true");
    this._addCss("Static.sta_pop_bg", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","50","true");
    this._addCss("Static.sta_pop_title", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ccccccff","","0","none","","");
    this._addCss("Static.sta_pop_title", "border", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 9 verdana");
    this._addCss("Static.sta_GA_title", "font", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "font", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#b1b1b1ff","");
    this._addCss("Static.sta_WF_amount", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#bfbfbfff","","0","none","","");
    this._addCss("Static.sta_WF_titlebg", "border", obj, ["normal"]);
    this._addCss("Static.sta_RM_title", "border", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "border", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 22");
    this._addCss("Static.sta_WF_titlebg", "padding", obj, ["normal"]);
    this._addCss("Static.sta_RM_title", "padding", obj, ["normal"]);
    this._addCss("Static.sta_RM_title2", "padding", obj, ["normal"]);

    obj = new nexacro.Style_color("#ff5a00ff");
    this._addCss("Static.sta_WF_detail_duty", "color", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#b1b1b1ff","","0","none","","","1","solid","#b1b1b1ff","","0","none","","");
    this._addCss("Static.sta_WF_detailBox", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#272c33ff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_detailTline", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","theme://images/img_WFDA_required.png","","0","0","0","50","true");
    this._addCss("Static.sta_WF_detailP", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_WF_required.png","","0","0","0","50","true");
    this._addCss("Static.sta_WF_searchP", "background", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 17");
    this._addCss("Static.sta_WF_searchP", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("#575c67ff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_label", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_s", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 18 Dotum");
    this._addCss("Static.sta_WF_label_s", "font", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 14 Dotum");
    this._addCss("Static.sta_WF_label2", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#edededff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_head", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_bar", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Static.sta_WF_head", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "border", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 20 Dotum");
    this._addCss("Static.sta_WF_head", "font", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 40");
    this._addCss("Static.sta_WF_head", "padding", obj, ["normal"]);
    this._addCss("Static.sta_LF_info", "padding", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 15 Dotum");
    this._addCss("Static.sta_WF_head2", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "font", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 20");
    this._addCss("Static.sta_WF_head2", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_head3", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("@gradation","","","0","0","0","0","true");
    this._addCss("Static.sta_LF_info", "background", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #d6be74 0,100 #c6ae64");
    this._addCss("Static.sta_LF_info", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_color("#5e4b12");
    this._addCss("Static.sta_LF_info", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("60%");
    this._addCss("Static.sta_LF_bg", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_intro_bg.png","","0","0","0","0","true");
    this._addCss("Static.sta_intro_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_intro_logo.png","","0","0","0","0","true");
    this._addCss("Static.sta_intro_logo", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_intro_txt.png","","0","0","0","0","true");
    this._addCss("Static.sta_intro_txt", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_login_back.png","stretch","0","0","0","0","true");
    this._addCss("Static.sta_login_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Static.sta_login_bg", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#5488ac");
    this._addCss("Static.sta_CM_main_t1", "color", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts1", "color", obj, ["normal"]);

    obj = new nexacro.Style_color("#7a7f5a");
    this._addCss("Static.sta_CM_main_t2", "color", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts2", "color", obj, ["normal"]);

    obj = new nexacro.Style_color("#a97f5b");
    this._addCss("Static.sta_CM_main_t3", "color", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts3", "color", obj, ["normal"]);

    obj = new nexacro.Style_color("#6a7a8d");
    this._addCss("Static.sta_CM_main_t4", "color", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts4", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 18 Dotum");
    this._addCss("Static.sta_CM_main_ts1", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts2", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts3", "font", obj, ["normal"]);
    this._addCss("Static.sta_CM_main_ts4", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_notice.png","","0","0","50","50","true");
    this._addCss("Static.sta_CM_main_img1", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_as.png","","0","0","50","50","true");
    this._addCss("Static.sta_CM_main_img2", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_data.png","","0","0","50","50","true");
    this._addCss("Static.sta_CM_main_img3", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_main_news.png","","0","0","50","50","true");
    this._addCss("Static.sta_CM_main_img4", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#989c9e");
    this._addCss("Static.sta_LM_txt", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 18 arial");
    this._addCss("Static.sta_LM_txt", "font", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","22","22","true","true","true","true");
    this._addCss("Static.sta_RM_roundtxt", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 21 Dotum");
    this._addCss("Static.sta_RM_roundtxt", "font", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 21 Dotum");
    this._addCss("Static.sta_RM_txt", "font", obj, ["normal"]);

    obj = new nexacro.Style_value("right middle");
    this._addCss("Static.sta_RM_price", "controlalignalign", obj, ["normal"]);

    obj = new nexacro.Style_align("left middle");
    this._addCss("Static.sta_RM_priceL", "controlalign", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#bfbfbfff","","0","none","","","1","solid","#bfbfbfff","","0","none","","");
    this._addCss("Static.sta_RM_tab", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_calmonth_bg.png","","0","0","50","50","true");
    this._addCss("Static.sta_WF_monthbg", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 34 arial");
    this._addCss("Static.sta_RM_time", "font", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#d1d1d1ff","","0","none","","","1","solid","#d1d1d1ff","","0","none","","");
    this._addCss("Static.sta_WF_bar", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("3","solid","#272c33ff","","0","none","","","0","none","","","0","none","","");
    this._addCss("Tab", "border", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_background("","theme://images/tab_WF_N.png","stretch","5","5","0","0","true");
    this._addCss("Tab", "buttonbackground", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Tab", "buttonbordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Tab.tab_RM", "buttonbordertype", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_padding("7 40 5 40");
    this._addCss("Tab", "buttonpadding", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_value("false");
    this._addCss("Tab", "showextrabutton", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Tab.tab_RM", "showextrabutton", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_background("","theme://images/tab_WF_S.png","stretch","5","5","0","0","true");
    this._addCss("Tab", "buttonbackground", obj, ["pushed", "selected"]);

    obj = new nexacro.Style_background("#262a35ff","","","0","0","0","0","true");
    this._addCss("Tab.tab_LF_user", "background", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#262a35ff","","","0","0","0","0","true");
    this._addCss("Tab.tab_LF_user", "buttonbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("0 none,1 solid #464a53,0 none,0 none");
    this._addCss("Tab.tab_LF_user", "buttonborder", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_padding("30 40 30 40");
    this._addCss("Tab.tab_LF_user", "buttonpadding", obj, ["normal", "mouseover", "focused", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_color("#aaabae");
    this._addCss("Tab.tab_LF_user", "color", obj, ["normal", "mouseover", "focused", "disabled"]);

    obj = new nexacro.Style_background("#4365a9ff","","","0","0","0","0","true");
    this._addCss("Tab.tab_LF_user", "buttonbackground", obj, ["pushed", "selected"]);

    obj = new nexacro.Style_background("","theme://images/tab_RM_N.png","stretch","5","5","0","0","true");
    this._addCss("Tab.tab_RM", "buttonbackground", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_padding("25 35 23 35");
    this._addCss("Tab.tab_RM", "buttonpadding", obj, ["normal", "mouseover", "pushed", "selected", "disabled"]);

    obj = new nexacro.Style_background("","theme://images/tab_RM_S.png","stretch","5","5","0","0","true");
    this._addCss("Tab.tab_RM", "buttonbackground", obj, ["selected"]);

    obj = new nexacro.Style_accessibility("","true","all","","","");
    this._addCss("Tabpage", "accessibility", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Tabpage", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Tabpage", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Tabpage", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Tabpage", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_padding("");
    this._addCss("Tabpage", "padding", obj, ["normal"]);

    obj = new nexacro.Style_align("");
    this._addCss("Tabpage", "stepalign", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Tabpage", "stepshowtype", obj, ["normal"]);

    obj = new nexacro.Style_padding("5 5 5 5");
    this._addCss("TextArea.essential", "padding", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#dae0e7ff","theme://images/img_required.png","","0","0","0","0","true");
    this._addCss(".headEssential", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#edf4faff","","","0","0","0","0","true");
    this._addCss(".bodyEssential", "background", obj, ["normal"]);

    obj = null;
    
//[add theme images]
  };
})();
