﻿//XJS=CommTran1.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************
        * Name       : commTran.xjs
        * Title      : nexacro 공통 Transaction 관련 함수 모음
        * @desc      :
        * 작성자    : 유희남
        * 작성일    : 2013-04-29
        * 변경사항  :
        ********************************************************************************/

        /**
         * Dataset의 값을 갱신하기위한 서비스를 호출하고, transaction이 완료되면 CallBack Function을 수행
         * @param    :   1. sSvcId       : Transaction을 구분하기 위한 ID 값입니다.
         * @param    :   2. sSvcUrl      : Transaction을 요청할 주소 값입니다.
         * @param    :   3. sInData      : 입력값으로 보낼 Dataset 의 ID, a=b의 형태로 실제이름과 입력이름을 매칭.
         * @param    :   4. sOutData     : 처리결과를 받을 Dataset 의 ID, a=b의 형태로 실제이름과 입력이름을 매칭.
         * @param    :   5. sArg         : 입력값으로 보낼 arguments, strFromDate="20120607"
         * @param    :   6. sCallBackFnc : transaction의 결과를 돌려줄 Function의 이름.
         * @param    :   7. bSync        : 비동기 여부를 지정.    -> Default : true(ASync) 
         * @param    :   8. bWait        : WaitCursor true/false
         * @return   :     
         */
        this.gfn_transaction = function (sSvcId,sSvcUrl,sInData,sOutData,sArg,sCallBackFnc,bSync,bWait)
        {
        	// 타 시스템에서 로그 아웃 된 경우
        	if(system.navigatorname == "nexacro"){
        		if(application.getPrivateProfile("LOGIN_FLAG") != "Y" && application.gv_userAuth == ""){
        			this.gfn_setMenu(application.gv_activeApp);
        			application.gv_TopFrame.form.gv_logoutFlag = true;
        			application.gv_TopFrame.form.fn_setGrid();
        			application.gv_LoginFrame.form.sta_back.set_visible(true);
        			application.gv_LoginFrame.form.div_init.set_visible(true);
        			application.gfn_setFrame("L");
        			return;
        		}
        	}else{
        		if(window.localStorage.getItem("LOGIN_FLAG") != "Y" && application.gv_userAuth == ""){
        			this.gfn_setMenu(application.gv_activeApp);
        			application.gv_TopFrame.form.gv_logoutFlag = true;
        			application.gv_TopFrame.form.fn_setGrid();
        			application.gv_LoginFrame.form.sta_back.set_visible(true);
        			application.gv_LoginFrame.form.div_init.set_visible(true);
        			application.gfn_setFrame("L");
        			return;
        		}
        	}
        	
        	if(application.getPrivateProfile("LOGIN_FLAG") == "Y" && application.gv_userAuth == ""){
        		if(this.gfn_sessionCheck()){
        			return;
        		}
        	}
        	
        	application.gv_userAuth = "";
        	
            var sServiceUrl = "svc::" + sSvcUrl;
            var sArgument = "";
            
            if (this.gfn_isNotNull(sArg)) {
                sArgument = sArg;
            }
            
            if(this.gfn_isNull(sCallBackFnc)) sCallBackFnc = "";
            if(this.gfn_isNull(bSync)) bSync = true;
            if(this.gfn_isNull(bWait)) bWait = true;
            if(this.gfn_isNotNull(sInData)) sInData = " " + sInData;
            
            // Async
            if ((bSync != true) && (bSync != false)) bSync = true;

            // Service ID  And sCallBackFnc Merge
            var sMergeSvcID = sSvcId + "|" + sCallBackFnc;
            
            // WaitCursor 
            if (bWait) {
                this.setWaitCursor(true, true);
            } else {
                this.setWaitCursor(false, true);
            }
            
            var userInfo = "";
            if(application.gds_userInfo.getRowCount() > 0){
        		userInfo = "IN_USER_INFO=gds_userInfo ";
            }
            var sDataset = "IN_PARAM=gds_param IN_COMMON=gds_common "+userInfo+"IN_SEARCHLIST=ds_searchList IN_MAP=gds_map";
            
            
            this.transaction( sMergeSvcID
                             ,sServiceUrl
                             ,sDataset + sInData
                             ,sOutData
                             ,sArgument
                             ,"gfn_tranCallback"   // strCallbackFunc
                             ,bSync                // bAsync
                             ,2                    // nDataType : SSV 타입
                             ,false);              // bCompress
        }

        /**
         * Transaction Callback
         * @param     :   sSvcId      : 서비스아이디
         * @param     :   nErrorCode  : 에러코드
         * @param     :   strErrorMsg : 에러메세지
         */
        this.gfn_tranCallback = function (sSvcId,nErrorCode,sErrorMsg)
        {
            var aArrSvcID     = sSvcId.split("|");
            var sServiceID    = aArrSvcID[0];
            var sCallbackFunc = aArrSvcID[1];
            var sErrMsg = "";
            
            // WaitCursor
            this.setWaitCursor(false, true);
        	
        	this.gfn_setParam("sortValue", "");
        	this.gfn_setParam("colName", "");
            
            // Server Error 
        //    trace("nErrorCode::"+nErrorCode);
            if (nErrorCode != 0) {
        		var msgKey = "MSG_"+sErrorMsg;
        		sErrMsg = application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", msgKey), "displaymessage");
        		if(sErrMsg === undefined){
        			sErrMsg = sErrorMsg;
        		}
                var sErrMessage = " ErrCode : " + nErrorCode + String.fromCharCode(13, 10) + "sErrorMsg : " +sErrMsg ; 
                trace(sErrMessage);
            }
            
            if (this.gfn_isNull(sCallbackFunc)) return;
            
            eval("this." + sCallbackFunc + "('" + sServiceID + "', nErrorCode, sErrMsg);");  
        }

        // client 세션 체크
        this.gfn_sessionCheck = function()
        {
        	var gv_sTime = 0;
        	var gv_ssesionTime = 0;
        	
        	if(system.navigatorname == "nexacro"){
        		gv_ssesionTime = application.getPrivateProfile("sessionTime");
        		gv_sTime = application.getPrivateProfile("sessionGap");
        	}else{
        		gv_ssesionTime = window.localStorage.getItem("sessionTime");
        		gv_sTime = window.localStorage.getItem("sessionGap");
        	}
        	
        	if(this.gfn_isNull(gv_sTime)) gv_sTime = 30;
        	if(this.gfn_isNull(gv_ssesionTime)) gv_ssesionTime = 0;
        	var timeGap = this.gfn_getCheckTime(gv_ssesionTime);
        	
        	// 세션이 종료된 경우 transaction 발생 기준
        	if(gv_ssesionTime != 0 && timeGap > gv_sTime){
        		this.gfn_alert("MSG_SCREEN_INIT", "", function(msg, flag){
        			this.gfn_popupClose();
        		});
        		
        		return true;
        	}else{
        		if(system.navigatorname == "nexacro"){
        			application.setPrivateProfile("sessionTime", this.gfn_setCheckTime());
        		}else{
        			window.localStorage.setItem("sessionTime", this.gfn_setCheckTime());
        		}
        		
        		return false;
        	}
        }

        this.gfn_popupClose = function(){
        	this.close();
        	var popFrames = application.popupframes;
        	for(var i = 0 ; i < popFrames.length ; i++){
        		if(popFrames[i].form != this.form) popFrames[i].form.close();
        	}
        	
        	this.gfn_setMenu(application.gv_activeApp);
        	application.gv_TopFrame.form.gv_logoutFlag = true;
        	application.gv_TopFrame.form.fn_setGrid();
        	application.gv_LoginFrame.form.sta_back.set_visible(true);
        	application.gv_LoginFrame.form.div_init.set_visible(true);
        	application.gfn_setFrame("L");
        }
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
