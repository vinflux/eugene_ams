﻿//XJS=CommTran.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************
        * Name       : commTran.xjs
        * Title      : nexacro 공통 Transaction 관련 함수 모음
        * @desc      :
        * 작성자    : 유희남
        * 작성일    : 2013-04-29
        * 변경사항  :
        ********************************************************************************/

        /**
         * Dataset의 값을 갱신하기위한 서비스를 호출하고, transaction이 완료되면 CallBack Function을 수행
         * @param    :   1. sSvcId       : Transaction을 구분하기 위한 ID 값입니다.
         * @param    :   2. sSvcUrl      : Transaction을 요청할 주소 값입니다.
         * @param    :   3. sInData      : 입력값으로 보낼 Dataset 의 ID, a=b의 형태로 실제이름과 입력이름을 매칭.
         * @param    :   4. sOutData     : 처리결과를 받을 Dataset 의 ID, a=b의 형태로 실제이름과 입력이름을 매칭.
         * @param    :   5. sArg         : 입력값으로 보낼 arguments, strFromDate="20120607"
         * @param    :   6. sCallBackFnc : transaction의 결과를 돌려줄 Function의 이름.
         * @param    :   7. bSync        : 비동기 여부를 지정.    -> Default : true(ASync) 
         * @param    :   8. bWait        : WaitCursor true/false
         * @return   :     
         */
        this.gfn_transaction = function (sSvcId,sSvcUrl,sInData,sOutData,sArg,sCallBackFnc,bSync,bWait)
        {
        // 	if(system.navigatorname == "nexacro"){
        // 		if(application.getPrivateProfile("LOGIN_FLAG") != "Y" && application.gv_userAuth == ""){
        // 			this.gfn_setMenu(application.gv_activeApp);
        // 			application.gv_TopFrame.form.fn_setGrid();
        // 			application.gv_LoginFrame.form.div_login.set_visible(true);
        // 			application.gfn_setFrame("L");
        // 			return;
        // 		}
        // 	}else{
        // 		if(window.localStorage.getItem("LOGIN_FLAG") != "Y" && application.gv_userAuth == ""){
        // 			this.gfn_setMenu(application.gv_activeApp);
        // 			application.gv_TopFrame.form.fn_setGrid();
        // 			application.gv_LoginFrame.form.div_login.set_visible(true);
        // 			application.gfn_setFrame("L");
        // 			return;
        // 		}
        // 	}
        	
        	application.gv_userAuth = "";
        	
            var sServiceUrl = "svc::" + sSvcUrl;
            var sArgument = "";
            
            if (this.gfn_isNotNull(sArg)) {
                sArgument = sArg;
            }
            
            if(this.gfn_isNull(sCallBackFnc)) sCallBackFnc = "";
            if(this.gfn_isNull(bSync)) bSync = true;
            if(this.gfn_isNull(bWait)) bWait = true;
            if(this.gfn_isNotNull(sInData)) sInData = " " + sInData;
            
            // Async
            if ((bSync != true) && (bSync != false)) bSync = true;

            // Service ID  And sCallBackFnc Merge
            var sMergeSvcID = sSvcId + "|" + sCallBackFnc;
            
            // WaitCursor 
            if (bWait) {
                this.setWaitCursor(true, true);
            } else {
                this.setWaitCursor(false, true);
            }
            
            var userInfo = "";
            if(application.gds_userInfo.getRowCount() > 0){
        		userInfo = "IN_USER_INFO=gds_userInfo ";
            }

            var sDataset = "IN_PARAM=gds_param IN_COMMON=gds_common "+userInfo+"IN_SEARCHLIST=ds_searchList IN_MAP=gds_map";

        	var objForm = this.fv_objForm;

        	if (!this.gfn_isNull(objForm)) {
        		if (!this.gfn_isNull(sOutData)) {
        			var objFunction = objForm._gfn_setLangInfo;
        			if (!this.gfn_isNull(objFunction)) objFunction.call(objForm, objForm, sOutData);
        		}
        	}

            this.transaction( sMergeSvcID
                             ,sServiceUrl
                             ,sDataset + sInData
                             ,sOutData
                             ,sArgument
                             ,"gfn_tranCallback"   // strCallbackFunc
                             ,bSync                // bAsync
                             ,2                    // nDataType : SSV 타입
                             ,false);              // bCompress
        }

        this.gfn_addTranColumn = function (objDs,sColId)
        {
        	var objColInfo = objDs.getColumnInfo(sColId);
        	if (this.gfn_isNull(objColInfo)) objDs.addColumn(sColId, "STRING", 255);
        }

        /**
         * Transaction Callback
         * @param     :   sSvcId      : 서비스아이디
         * @param     :   nErrorCode  : 에러코드
         * @param     :   strErrorMsg : 에러메세지
         */
        this.gfn_tranCallback = function (sSvcId,nErrorCode,sErrorMsg)
        {
            var aArrSvcID     = sSvcId.split("|");
            var sServiceID    = aArrSvcID[0];
            var sCallbackFunc = aArrSvcID[1];
            
            // WaitCursor
            this.setWaitCursor(false, true);
           
            // Server Error 

            if (nErrorCode != 0) {
        		if (this.gfn_isNull(sErrorMsg)) {
        			var nFindRow = application.gds_msg.findRowExpr("mulaapmsg_hdkey == 'MSG_" + String(Math.abs(nErrorCode)) + "'");
        			if (nFindRow >= 0) {
        				var sMsg = application.gds_msg.getColumn(nFindRow, "displaymessage");
        				if (!this.gfn_isNull(sMsg)) sErrorMsg = sMsg;
        			}
        		}

                var sErrMessage = " ErrCode : " + nErrorCode + String.fromCharCode(13, 10) + "sErrorMsg : " + sErrorMsg; 
                trace(sErrMessage);

        		if (nErrorCode == -9999) {
        			sErrorMsg = this.gfn_getMsgToArray("MSG_" + sErrorMsg);
        			sErrorMsg = sErrorMsg[1];
        		}
            }

        	var objForm = this.fv_objForm;

        	if (!this.gfn_isNull(objForm)) {
        		var objFunction = objForm._gfn_getLangInfo;
        		if (!this.gfn_isNull(objFunction)) objFunction.call(objForm, objForm);

        		var objNoMsgDs = objForm.objects["ds_noDataMsg"];

        		if (!this.gfn_isNull(objNoMsgDs)) {
        			var arrNoDataMsg = this.gfn_getMsgToArray("MSG_NO_SEARCHDATA");

        			if (objNoMsgDs.getRowCount() == 0) objNoMsgDs.addRow();
        			objNoMsgDs.setColumn(0, "MSG_NO_SEARCHDATA", arrNoDataMsg[1]);
        		}
        	}

            if (this.gfn_isNull(sCallbackFunc)) return;

            eval("this." + sCallbackFunc + "('" + sServiceID + "', nErrorCode, sErrorMsg);");  
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
