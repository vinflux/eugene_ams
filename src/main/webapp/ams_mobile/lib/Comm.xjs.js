﻿//XJS=Comm.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.addIncludeScript(path, "lib::CommDate.xjs");
        this.addIncludeScript(path, "lib::CommForm.xjs");
        this.addIncludeScript(path, "lib::CommFrame.xjs");
        this.addIncludeScript(path, "lib::CommGrid.xjs");
        this.addIncludeScript(path, "lib::CommTran.xjs");
        this.addIncludeScript(path, "lib::CommUtil.xjs");
        this.addIncludeScript(path, "lib::CommFile.xjs");
        this.addIncludeScript(path, "lib::CommWmsUtil.xjs");
        this.addIncludeScript(path, "lib::Constant.xjs");
        this.registerScript(path, function(exports) {
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommDate.xjs", null, exports); }	//include "lib::CommDate.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommForm.xjs", null, exports); }	//include "lib::CommForm.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommFrame.xjs", null, exports); }	//include "lib::CommFrame.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommGrid.xjs", null, exports); }	//include "lib::CommGrid.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommTran.xjs", null, exports); }	//include "lib::CommTran.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommUtil.xjs", null, exports); }	//include "lib::CommUtil.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommFile.xjs", null, exports); }	//include "lib::CommFile.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::CommWmsUtil.xjs", null, exports); }	//include "lib::CommWmsUtil.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Constant.xjs", null, exports); }	//include "lib::Constant.xjs";

        this.fv_objForm;
        this.fv_workForm;
        this.fv_mainCF;
        this.fv_langInfo = new Object();
        this.fv_searchCompDs;
        this.fv_pageLimit = 25;
        this.fv_useSrchCondPop = true;
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
