﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("imagePicker");
                this.set_classname("imagePicker");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1024,768);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new ImagePicker("ImagePicker00", this);
            this.addChild(obj.name, obj);

            
            // UI Components Initialize
            obj = new Button("Button00", "absolute", "1.56%", "55", null, "50", "86.72%", null, this);
            obj.set_taborder("0");
            obj.set_text("Button00");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "14.06%", "48", null, "248", "47.17%", null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer00", "absolute", "55.57%", "45", null, "198", "22.75%", null, this);
            obj.set_taborder("2");
            obj.set_text("ImageViewer00");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1024, 768, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("imagePicker");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Z201604_2016040170-imagePicker.xfdl", function(exports) {

        this.Button00_onclick = function(obj,e)
        {
        	this.ImagePicker00.open("0");
        }

        this.ImagePicker00_onsuccess = function(obj,e)
        {
        	var userappUrl = system.convertRealPath("%USERAPP%");
        	var url = "file:/" + e.imageurl.replace("%USERAPP%",userappUrl);
        	this.ImageViewer00.set_image("URL('" + url + "')");
        	
        	return;
        	this.TextArea00.set_value("aaaa \n");
        	this.TextArea00.set_value(e.imageurl + "\n" + e.imagefile + "\n" + e.imagedata);
        	trace(">>>>>>>>>>>>>>> 1: " + e.imageurl);
        	trace(">>>>>>>>>>>>>>> 2: " + e.imagefile);
        	trace(">>>>>>>>>>>>>>> 3: " + e.imagedata);
        }

        this.ImagePicker00_onerror = function(obj,e)
        {
        	alert("ImagePicker00_onerror");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ImagePicker00.addEventHandler("onsuccess", this.ImagePicker00_onsuccess, this);
            this.ImagePicker00.addEventHandler("onerror", this.ImagePicker00_onerror, this);
            this.Button00.addEventHandler("onclick", this.Button00_onclick, this);

        };

        this.loadIncludeScript("Z201604_2016040170-imagePicker.xfdl", true);

       
    };
}
)();
