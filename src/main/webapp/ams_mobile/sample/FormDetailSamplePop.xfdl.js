﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("FormDetailSamplePop");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,600,450);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_detailListPop", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("stc_label", "absolute", "9", "6", "520", "36", null, null, this);
            obj.set_taborder("3");
            obj.set_text("팝업 인수전달 샘플");
            obj.set_cssclass("sta_WF_label");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_line", "absolute", "9", "48", null, "2", "9", null, this);
            obj.set_taborder("4");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "9", null, this);
            obj.set_taborder("5");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg1Pop", "absolute", "95", "59", "197", "37", null, null, this);
            obj.set_taborder("11");
            obj.style.set_font("16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg2Pop", "absolute", "95", "99", "197", "37", null, null, this);
            obj.set_taborder("12");
            obj.style.set_font("16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "298", "408", "4", "25", null, null, this);
            obj.set_taborder("13");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "0", "9", "329", null, null, this);
            obj.set_taborder("14");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "591", "0", "9", "50", null, null, this);
            obj.set_taborder("15");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "9", "0", "121", "9", null, null, this);
            obj.set_taborder("16");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "93", "56", "2", "83", null, null, this);
            obj.set_taborder("18");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "23", "96", "169", "3", null, null, this);
            obj.set_taborder("19");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "17", "50", "121", "9", null, null, this);
            obj.set_taborder("23");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "25", "136", "121", "9", null, null, this);
            obj.set_taborder("24");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "242", "396", "121", "9", null, null, this);
            obj.set_taborder("25");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "241", "441", "121", "9", null, null, this);
            obj.set_taborder("26");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "526", "0", "65", "14", null, null, this);
            obj.set_taborder("28");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "526", "34", "65", "14", null, null, this);
            obj.set_taborder("29");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "591", "96", "9", "337", null, null, this);
            obj.set_taborder("30");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg1Pop", "absolute", "9", "59", "84", "37", null, null, this);
            obj.set_taborder("31");
            obj.set_text("arg1");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg2Pop", "absolute", "9", "99", "84", "37", null, null, this);
            obj.set_taborder("32");
            obj.set_text("arg2");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_detailListPop", "absolute", "9", "145", null, "251", "9", null, this);
            obj.set_taborder("33");
            obj.set_binddataset("ds_detailListPop");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/></Columns><Rows><Row size=\"36\" band=\"head\"/><Row size=\"30\"/></Rows><Band id=\"head\"><Cell style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column0\"/><Cell col=\"1\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column1\"/><Cell col=\"2\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column2\"/></Band><Band id=\"body\"><Cell edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column0\"/><Cell col=\"1\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column1\"/><Cell col=\"2\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column2\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("btn_confirmPop", "absolute", "222", "405", "76", "36", null, null, this);
            obj.set_taborder("34");
            obj.set_text("확인");
            obj.style.set_font("bold 18 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_popupClose", "absolute", "302", "405", "76", "36", null, null, this);
            obj.set_taborder("35");
            obj.set_text("닫기");
            obj.style.set_padding("0 0 0 0");
            obj.style.set_font("bold 18 Dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 600, 450, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("FormDetailSamplePop.xfdl", "lib::Comm.xjs");
        this.registerScript("FormDetailSamplePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);

        	var objArgs = obj.arguments;

        	if (!this.gfn_isNull(objArgs)) {
        		this.edt_arg1Pop.set_value(objArgs.arg1);
        		this.edt_arg2Pop.set_value(objArgs.arg2);

        		var objDs = objArgs.dsList;

        		if (objDs instanceof Dataset) {
        			this.ds_detailListPop.clear();
        			this.ds_detailListPop.copyData(objDs);
        		}
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.btn_confirmPop_onclick = function(obj,e)
        {
        	var objRtn = new Object();
        	objRtn.arg1 = this.edt_arg1Pop.value;
        	objRtn.arg2 = this.edt_arg2Pop.value;
        	objRtn.dsList = this.ds_detailListPop;

        	this.gfn_popupClose(objRtn);
        }

        this.btn_closePop_onclick = function(obj,e)
        {
        	this.gfn_popupClose();
        }

        this.btn_closeAll_onclick = function(obj,e)
        {
        	this.gfn_popupClose();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.btn_confirmPop.addEventHandler("onclick", this.btn_confirmPop_onclick, this);
            this.btn_popupClose.addEventHandler("onclick", this.btn_closePop_onclick, this);

        };

        this.loadIncludeScript("FormDetailSamplePop.xfdl", true);

       
    };
}
)();
