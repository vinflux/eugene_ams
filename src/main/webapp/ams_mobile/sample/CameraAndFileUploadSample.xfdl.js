﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MainMenu");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,989);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Camera("cmr_tax", this);
            this.addChild(obj.name, obj);

            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", null, "51", "0", null, this);
            obj.set_taborder("0");
            obj.set_text("Camera And File Upload Sample");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "50", "9", "105", null, null, this);
            obj.set_taborder("14");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "631", "58", "9", "105", null, null, this);
            obj.set_taborder("16");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "194", "9", "105", null, null, this);
            obj.set_taborder("18");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "14", "51", "553", "9", null, null, this);
            obj.set_taborder("19");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "591", "0", "9", "50", null, null, this);
            obj.set_taborder("22");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Button("btn_camera", "absolute", "12", "59", "265", "81", null, null, this);
            obj.set_taborder("23");
            obj.set_text("Camera");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 989, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CameraAndFileUploadSample.xfdl", "lib::Comm.xjs");
        this.registerScript("CameraAndFileUploadSample.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.btn_camera_onclick = function(obj,e)
        {
        	this.cmr_tax.takePicture();
        }

        this.Camera_oncapture = function(obj,e)
        {
        	alert("success");
        	var out = e.url + "\n"+ e.imagedata;
        }

        this.Camera_onerror = function(obj,e)
        {
        	this.TextArea00.value = e.errormsg;
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.cmr_tax.addEventHandler("oncapture", this.Camera_oncapture, this);
            this.cmr_tax.addEventHandler("onerror", this.Camera_onerror, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.btn_camera.addEventHandler("onclick", this.btn_camera_onclick, this);

        };

        this.loadIncludeScript("CameraAndFileUploadSample.xfdl", true);

       
    };
}
)();
