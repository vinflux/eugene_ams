﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MainMenu");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,989);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_detailList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", null, "50", "0", null, this);
            obj.set_taborder("0");
            obj.set_text("Form Detail");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_args", "absolute", "9", "62", null, "129", "9", null, this);
            obj.set_taborder("1");
            obj.set_scrollbars("fixedvert");
            obj.set_wordwrap("char");
            obj.style.set_selectcolor("azure");
            obj.style.set_selectbackground("lightblue");
            obj.style.set_font("18 dotum");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00", "absolute", "9", "203", null, "289", "9", null, this);
            obj.set_taborder("2");
            obj.set_binddataset("ds_detailList");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/></Columns><Rows><Row size=\"36\" band=\"head\"/><Row size=\"30\"/></Rows><Band id=\"head\"><Cell style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column0\"/><Cell col=\"1\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column1\"/><Cell col=\"2\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column2\"/></Band><Band id=\"body\"><Cell style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column0\"/><Cell col=\"1\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column1\"/><Cell col=\"2\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column2\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "64", "50", "369", "12", null, null, this);
            obj.set_taborder("3");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "49", "9", "105", null, null, this);
            obj.set_taborder("4");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "631", "57", "9", "105", null, null, this);
            obj.set_taborder("5");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "72", "191", "369", "12", null, null, this);
            obj.set_taborder("6");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "591", "0", "9", "50", null, null, this);
            obj.set_taborder("7");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", null, null, "120", "50", "40", "45", this);
            obj.set_taborder("8");
            obj.set_text("Button00");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 989, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("FormDetailSample.xfdl", "lib::Comm.xjs");
        this.registerScript("FormDetailSample.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);

        	if (!this.gfn_isNull(obj.arguments)) {
        		var objArgs = obj.arguments;
        		var sArg = "arg1 = [" + objArgs.arg1 + "]\n";
        		sArg += "arg2 = [" + objArgs.arg2 + "]";
        		this.txa_args.set_value(sArg);

        		var objDs = objArgs.dsList;

        		if (objDs instanceof Dataset) {
        			this.ds_detailList.clear();
        			this.ds_detailList.copyData(objDs);
        		}
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);

        };

        this.loadIncludeScript("FormDetailSample.xfdl", true);

       
    };
}
)();
