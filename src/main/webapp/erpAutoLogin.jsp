<!-- Web Root Index.html-->
<html xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>
<head> 
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
  <link rel="shortcut icon" href="../ams/favicon_eugene.ico" type="image/x-icon">
  <!--
  <title> VINFLUX AMS</title>
  -->
  <title>PTL</title>
   <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> -->
   <script src="../ams/resources/soundmanager/soundmanager2.js"></script>
   
</head>
<%
	String UserId = request.getParameter("UserId");
	String AccessToken = request.getParameter("AccessToken");
	String MenuId = request.getParameter("MenuId");
%>

<body>
	<!-- main frame -->	
	<iframe name="iframe_main"  
		src="./ams/erpAutoLogin.jsp?UserId=<%=UserId%>&AccessToken=<%=AccessToken%>&MenuId=<%=MenuId%>" 
		vspace="0" hspace="0" border="0"
		style="position:absolute;left:0px;top:0px;width:100%;height:100%;"
		marginheight="0" marginwidth="0" scrolling="no" frameborder="0"
		width="100%"
		height="100%"
	></iframe>
	
	<!-- ubi report hidden iframe  --> 
	<iframe name="iframe_hidden"  src="" style="width:50%;height:0;visibility: hidden;"></iframe>
	<iframe name="iframe_hidden2"  src="" style="width:0;height:0;visibility: hidden;"></iframe>
	<iframe name="iframe_hidden3"  src="" style="width:0;height:0;visibility: hidden;"></iframe>
</body>
</html>
