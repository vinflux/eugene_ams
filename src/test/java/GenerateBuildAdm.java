import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import com.vinflux.framework.common.EnvProperties;

public class GenerateBuildAdm {

	public static final String MODULE_NAME = "vinfluxADM";
	public static final String SUB_MODULE_NAME = "adm";

	public static final String SCH_MODULE_NAME = "ADM";

	public static String strFileList = "";

	public static String strViewList = "";
	public static String strControllerList = "";
	public static String strStoreList = "";
	public static String strLibList = "";

	public static final String MODULE_PATH = EnvProperties.RELATIVE_PATH_PREFIX.substring(0, EnvProperties.RELATIVE_PATH_PREFIX.indexOf(MODULE_NAME)
											+ MODULE_NAME.length()) + "/src/main/webapp/" + SUB_MODULE_NAME;
	
	protected static final Log LOG = LogFactory.getLog(GenerateBuildAdm.class);
	
//	public static String RELATIVE_PATH_PREFIX = "/D:/DEV/work/Deploy/vinfluxADM/target/classes/";
//	
//	public static String MODULE_PATH = RELATIVE_PATH_PREFIX.substring(0, RELATIVE_PATH_PREFIX.indexOf(MODULE_NAME)
//			+ MODULE_NAME.length()) + "/src/main/webapp/" + SUB_MODULE_NAME;
	
	public static String strJsb3Prefix = "{"+
										    "\n\"projectName\": \"Project Name\","+
										    "\n\"licenseText\": \"Copyright(c) 2012 Company Name\","+
										    "\n\"builds\": ["+
									        "\n{"+
									            "\n\"name\": \"All Classes\","+
									            "\n\"target\": \"all-classes.js\","+
									            "\n\"options\": {"+
									                "\n\"debug\": false"+
									            "\n},"+
								            "\n\"files\": [";
	public static String strJsb3Surfix = "]"+
									        "\n},"+
									        "\n{"+
									        "\n\"name\": \"Application - Production\","+
									            "\n\"target\": \"app-all.js\","+
									            "\n\"compress\": true,"+
									            "\n\"files\": ["+
											                "\n{"+
											                    "\n\"path\": \"\","+
											                    "\n\"name\": \"all-classes.js\""+
											                "\n},"+
											                "\n{"+
											                    "\n\"path\": \"\","+
											                    "\n\"name\": \"app.js\""+
											                "\n}"+
											            "\n]"+
											        "\n}"+
											    "\n],"+
											    "\n\"resources\": []"+
											"\n}";
	
	//protected static final Log LOG = LogFactory.getLog(GenerateBuildAdm.class);
	
	public static void main(String args[]) {

		// MODULE_PATH
		try {
			(new GenerateBuildAdm()).showFileList(MODULE_PATH + "/app/");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.error( e );
		}

		// view 와 store 가 controller 보다 먼저 load 되기 하기 위함임.
		strFileList = strStoreList + strViewList + strControllerList + strLibList;

		strFileList = strFileList.substring(0, strFileList.length()-2);
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter( MODULE_PATH + "/" + SUB_MODULE_NAME + ".jsb3"));
			out.write(strJsb3Prefix);
			out.newLine();
			out.write(strFileList); 
			out.newLine();
			out.write(strJsb3Surfix);

			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOG.error( e );
		}
	}
	
	public void showFileList(String path) throws Exception {

		File dir = new File(path);
		File[] files = dir.listFiles();
		//files.
		if (files != null ) { 
			for(int i=0; i<files.length; i++) {
				File file = files[i];
				if(file.isFile()) {
					if(file.getCanonicalPath().toString().endsWith(".js")) {

						String strModulPath = file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length());

						if(strModulPath.indexOf('\\') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('\\')).equals("store")) {
							strStoreList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "." + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if( "store".equals(strModulPath) ) {
							strStoreList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if(strModulPath.indexOf('\\') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('\\')).equals("controller")) {
							strControllerList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if( "controller".equals(strModulPath)) {	
							strControllerList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if(strModulPath.indexOf('\\') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('\\')).equals("view")) {
							strViewList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if( "view".equals(strModulPath) ) {
							strViewList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if(strModulPath.indexOf('\\') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('\\')).equals("lib")) {
							strLibList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						} else if( "lib".equals(strModulPath)) {
							strLibList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						}
					}
					
				} else if(file.isDirectory()) {
					try{
						showFileList(file.getCanonicalPath().toString());
					} catch (Exception e) {
						LOG.error(e);
					}
				}
			}			
		}
	}
}