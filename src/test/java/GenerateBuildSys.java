import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

//import com.vinflux.adm.addr.web.AddrController;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import com.vinflux.framework.common.EnvProperties;

public class GenerateBuildSys {
	/** LOG */
	protected static final Log LOG = LogFactory.getLog(GenerateBuildSys.class);
	
	public static final String MODULE_NAME = "vinfluxADM";
	public static final String SUB_MODULE_NAME = "sys";

	public static final String SCH_MODULE_NAME = "ADM";

	public static String strFileList = "";

	public static String strViewList = "";
	public static String strControllerList = "";
	public static String strStoreList = "";
	public static String strLibList = "";

//	public static String MODULE_PATH = EnvProperties.RELATIVE_PATH_PREFIX.substring(0, EnvProperties.RELATIVE_PATH_PREFIX.indexOf(MODULE_NAME)
//											+ MODULE_NAME.length()) + "/src/main/webapp/" + SUB_MODULE_NAME;
	public static final String MODULE_PATH = EnvProperties.RELATIVE_PATH_PREFIX.substring(0, EnvProperties.RELATIVE_PATH_PREFIX.indexOf(MODULE_NAME)
									+ MODULE_NAME.length()) + "/src/main/webapp/" + SUB_MODULE_NAME;
	
//	public static String RELATIVE_PATH_PREFIX = "/D:/DEV/work/Deploy/vinfluxADM/target/classes/";
//	
//	public static String MODULE_PATH = RELATIVE_PATH_PREFIX.substring(0, RELATIVE_PATH_PREFIX.indexOf(MODULE_NAME)
//			+ MODULE_NAME.length()) + "/src/main/webapp/" + SUB_MODULE_NAME;
	
//	public static String MODULE_PATH = "D:/DEV/work/Deploy/vinfluxADM/src/main/webapp/sys";
	
	public static String strJsb3Prefix = "{"+
										    "\n\"projectName\": \"Project Name\","+
										    "\n\"licenseText\": \"Copyright(c) 2012 Company Name\","+
										    "\n\"builds\": ["+
									        "\n{"+
									            "\n\"name\": \"All Classes\","+
									            "\n\"target\": \"all-classes.js\","+
									            "\n\"options\": {"+
									                "\n\"debug\": false"+
									            "\n},"+
								            "\n\"files\": [";
	public static String strJsb3Surfix = "]"+
									        "\n},"+
									        "\n{"+
									        "\n\"name\": \"Application - Production\","+
									            "\n\"target\": \"app-all.js\","+
									            "\n\"compress\": true,"+
									            "\n\"files\": ["+
											                "\n{"+
											                    "\n\"path\": \"\","+
											                    "\n\"name\": \"all-classes.js\""+
											                "\n},"+
											                "\n{"+
											                    "\n\"path\": \"\","+
											                    "\n\"name\": \"app.js\""+
											                "\n}"+
											            "\n]"+
											        "\n}"+
											    "\n],"+
											    "\n\"resources\": []"+
											"\n}";
	
	public static String reservedView = 
		"{\n" +
		"\"clsName\": \"ADM.view.system.SearchExtendedPanel\",\n" +
		"\"path\": \"app/view/system/\",\n" +
		"\"name\": \"SearchExtendedPanel.js\"\n" +
		"},\n"+  //file1
		"{\n" +
		"\"clsName\": \"ADM.view.system.SearchDetailPanel\",\n" +
		"\"path\": \"app/view/system/\",\n" +
		"\"name\": \"SearchDetailPanel.js\"\n" +
		"},\n"+ //file2
		"{\n" +
		"\"clsName\": \"ADM.view.system.ContentsTabPanelContainerContainer\",\n" +
		"\"path\": \"app/view/system/\",\n" +
		"\"name\": \"ContentsTabPanelContainerContainer.js\"\n" +
		"},\n"+ //file3
		"{\n" +
		"\"clsName\": \"ADM.view.system.ContentsTabPanelContainerPanel\",\n" +
		"\"path\": \"app/view/system/\",\n" +
		"\"name\": \"ContentsTabPanelContainerPanel.js\"\n" +
		"},\n"+ //file4
		"{\n" +
		"\"clsName\": \"ADM.view.override.common.search.BetweenMonthField\",\n" +
		"\"path\": \"app/view/override/common/search/\",\n" +
		"\"name\": \"BetweenMonthField.js\"\n" +
		"},\n"+ //file5
		"{\n" +
		"\"clsName\": \"ADM.view.common.search.BetweenMonthField\",\n" +
		"\"path\": \"app/view/common/search/\",\n" +
		"\"name\": \"BetweenMonthField.js\"\n" +
		"},\n"; //file6

	//protected static final Log LOG = LogFactory.getLog(GenerateBuildSys.class);
	
	public static void main(String args[]) {

		// MODULE_PATH
		try {
			(new GenerateBuildSys()).showFileList(MODULE_PATH + "/app/");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.error(e);
		}

		// view 와 store 가 controller 보다 먼저 load 되기 하기 위함임.
		strFileList = strStoreList + reservedView + strViewList + strControllerList + strLibList;

		strFileList = strFileList.substring(0, strFileList.length()-2);

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter( MODULE_PATH + "/" + SUB_MODULE_NAME + ".jsb3"));
			out.write(strJsb3Prefix);
			out.newLine();
			out.write(strFileList); 
			out.newLine();
			out.write(strJsb3Surfix);

			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOG.error( e );
		}
	}
	
	public void showFileList(String path) throws Exception {

		File dir = new File(path);
		File[] files = dir.listFiles();
		if (files != null) { 
			for(int i=0; i<files.length; i++) {
				File file = files[i];
				String fileName = file.getName();
				
				//미리 예약된 화면은 스킵
				if ("SearchExtendedPanel.js".equals(fileName) || 
						"SearchDetailPanel.js".equals(fileName) || 
						"ContentsTabPanelContainerContainer.js".equals(fileName) || 
						"ContentsTabPanelContainerPanel.js".equals(fileName) || 
						"BetweenMonthField.js".equals(fileName)) {
					continue;
				}
				
				if(file.isFile()) {
					if(file.getCanonicalPath().toString().endsWith(".js")) {

						String strModulPath = file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length());

//						if(strModulPath.indexOf("\\") > 0 && strModulPath.subSequence(0, strModulPath.indexOf("\\")).equals("store")) {
						if(strModulPath.indexOf('/') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('/')).equals("store")) {
							strStoreList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "." + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.equals("store")) {
						} else if("store".equals(strModulPath)) {
							strStoreList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.indexOf("\\") > 0 && strModulPath.subSequence(0, strModulPath.indexOf("\\")).equals("controller")) {		
						} else if(strModulPath.indexOf('/') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('/')).equals("controller")) {
							strControllerList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.equals("controller")) {
						} else if("controller".equals(strModulPath)) {		
							strControllerList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.indexOf("\\") > 0 && strModulPath.subSequence(0, strModulPath.indexOf("\\")).equals("view")) {
						} else if(strModulPath.indexOf('/') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('/')).equals("view")) {
							strViewList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.equals("view")) {
						} else if("view".equals(strModulPath)) {
							strViewList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.indexOf("\\") > 0 && strModulPath.subSequence(0, strModulPath.indexOf("\\")).equals("lib")) {
						} else if(strModulPath.indexOf('/') > 0 && strModulPath.subSequence(0, strModulPath.indexOf('/')).equals("lib")) {
							strLibList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
//						} else if(strModulPath.equals("lib")) {
						} else if("lib".equals(strModulPath)) {
							strLibList += "{" + 
									"\n\"clsName\": \"" + SCH_MODULE_NAME + "." 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", ".") + "."  + file.getName().replace(".js", "") + "\"," +
									"\n\"path\": \"" + "app" + "/" 
											+ file.getParent().toString().substring((MODULE_PATH + "/app").length(), file.getParent().toString().length()).replace("\\", "/") +"/\","
									+ "\n\"name\": \"" + file.getName() + "\""
									+ "\n},\n";
						}
					}
					
				} else if(file.isDirectory()) {
					try{
						showFileList(file.getCanonicalPath().toString());
					} catch (Exception e) {
						LOG.error(" Error. ", e);
					}
				}
			}			
		}
	}
}