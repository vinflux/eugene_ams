package com.vinflux.framework.util;

import org.junit.Test;

import com.vinflux.framework.context.AbstractApplicationContextTest;

public class CipherUtilTestCase  extends AbstractApplicationContextTest {

	
	@Test
	public void test() throws Exception {

		String pw = "1";		
		String encryptPw = CipherUtil.encryptPassword(pw);
		logger.debug("@@@pw=>"+ encryptPw);	
		logger.debug("@@@SIZE=>"+ encryptPw.length());
		
		pw = "qwer!121234";
		encryptPw = CipherUtil.encryptPassword(pw);
		logger.debug("@@@pw=>"+ encryptPw);	
		logger.debug("@@@SIZE=>"+ encryptPw.length());
	}

}
