package com.vinflux.framework.idgen;

import javax.annotation.Resource;


import org.junit.Test;

import com.vinflux.framework.context.AbstractApplicationContextTest;

public class IdGenTestCase extends AbstractApplicationContextTest {

	@Resource(name="usccHdKeyIdGenService")
	private IdGenService usccHdKeyIdGenService;
	
//	@Resource(name="usccDtKeyIdGenService")
//	private IdGenService usccDtKeyIdGenService;
	
	@Test
	public void test() throws Exception {
				
		String usccHdKey = usccHdKeyIdGenService.getNextStringId();	
		logger.debug("### usccHdKey1=>"+ usccHdKey);
		usccHdKey = usccHdKeyIdGenService.getNextStringId();	
		logger.debug("### usccHdKey2=>"+ usccHdKey);
		//String usccDtKey = (String)usccDtKeyIdGenService.getNextStringId("SC00000340");
		
		//logger.debug("### usccDtKey=>"+ usccDtKey);
	}

}
