package com.vinflux.framework.context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.vinflux.framework.session.SessionManager;
import com.vinflux.framework.session.SessionVO;
 
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		 "classpath:applicationContext.xml"
            })
public class AbstractApplicationContextTest {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
    @Autowired ApplicationContext ctx;
    
	@Before
	public void setUp() throws Exception {
	    // do you when's on attrs
	    MockHttpServletRequest request = new MockHttpServletRequest();
	    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	    
		SessionVO sessionVO = new SessionVO();
		sessionVO.setUrKey("JUNIT");
		sessionVO.setUrName("TEST CASE SYSTEM");
		sessionVO.setUrGrKey("ADMIN");
		sessionVO.setLaKey("");	//client 에서 넘오온 데이터로 다국어 설정.
		SessionManager.setUserInfo(request, sessionVO, "Y");
	}
}