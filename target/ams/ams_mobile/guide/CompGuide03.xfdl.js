﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("ListBox, Combo, Menu");
                this._setFormPosition(0,0,994,540);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">Static</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">Edit</Col></Row><Row><Col id=\"code\">3</Col><Col id=\"data\">MaskEdit</Col></Row><Row><Col id=\"code\">4</Col><Col id=\"data\">TextArea</Col></Row><Row><Col id=\"code\">5</Col><Col id=\"data\">Button</Col></Row><Row><Col id=\"code\">6</Col><Col id=\"data\">Spin</Col></Row><Row><Col id=\"code\">7</Col><Col id=\"data\">CheckBox</Col></Row><Row><Col id=\"code\">8</Col><Col id=\"data\">Radio</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_Menu", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"caption\" type=\"STRING\" size=\"256\"/><Column id=\"level\" type=\"STRING\" size=\"256\"/><Column id=\"enabled\" type=\"STRING\" size=\"256\"/><Column id=\"check\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"id\">1</Col><Col id=\"caption\">Support</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">2</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">3</Col><Col id=\"caption\">Tip</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">4</Col><Col id=\"caption\">FAQ</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">5</Col><Col id=\"caption\">Showcase</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">6</Col><Col id=\"caption\">Design</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">7</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">8</Col><Col id=\"caption\">X-UP</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">9</Col><Col id=\"caption\">X-POP</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">10</Col><Col id=\"caption\">Download</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">11</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">12</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">13</Col><Col id=\"caption\">Setup</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">14</Col><Col id=\"caption\">Manual</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">15</Col><Col id=\"caption\">Runtime Admin</Col><Col id=\"level\">3</Col></Row><Row><Col id=\"id\">16</Col><Col id=\"caption\">Server</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">17</Col><Col id=\"caption\">Education</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">18</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col><Col id=\"enabled\">0</Col></Row><Row><Col id=\"id\">19</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">20</Col><Col id=\"caption\">X-UP</Col><Col id=\"level\">1</Col><Col id=\"check\">1</Col></Row><Row><Col id=\"caption\">X-POP</Col><Col id=\"id\">21</Col><Col id=\"level\">1</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("Dataset00", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new ListBox("ListBox01", "absolute", "426", "61", "200", "196", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("28");
            obj.set_innerdataset("@ds_list");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_enable("false");

            obj = new Combo("Combo00", "absolute", "140", "362", "230", "50", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("33");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_innerdataset("@ds_list");
            obj.set_index("-1");

            obj = new Combo("Combo01", "absolute", "426", "362", "230", "50", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("34");
            obj.set_innerdataset("ds_list");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_enable("false");

            obj = new ListBox("ListBox00", "absolute", "135", "61", "200", "198", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("40");
            obj.set_multiselect("true");
            obj.set_innerdataset("@ds_list");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_index("-1");

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("41");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "59", "124", "185", null, null, this);
            obj.set_taborder("42");
            obj.set_text("ListBox");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("43");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "297", "945", "1", null, null, this);
            obj.set_taborder("44");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "135", "31", "100", "25", null, null, this);
            obj.set_taborder("45");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "426", "31", "100", "25", null, null, this);
            obj.set_taborder("46");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "313", "124", "110", null, null, this);
            obj.set_taborder("47");
            obj.set_text("Combo");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "140", "329", "100", "25", null, null, this);
            obj.set_taborder("48");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "426", "329", "100", "25", null, null, this);
            obj.set_taborder("49");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo02", "absolute", "714", "362", "230", "50", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("50");
            obj.set_innerdataset("ds_list");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_readonly("true");

            obj = new Static("Static03", "absolute", "714", "329", "100", "25", null, null, this);
            obj.set_taborder("51");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 994, 540, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("ListBox, Combo, Menu");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("CompGuide03.xfdl", true);

       
    };
}
)();
