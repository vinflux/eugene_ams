﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("Static, Edit, MaskEdit, TextArea");
                this._setFormPosition(0,0,1269,605);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static04", "absolute", "135", "35", "100", "25", null, null, this);
            obj.set_taborder("8");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("32");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "84", null, null, this);
            obj.set_taborder("33");
            obj.set_text("Static");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "112", "1005", "1", null, null, this);
            obj.set_taborder("34");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "135", "61", "260", "35", null, null, this);
            obj.set_taborder("35");
            obj.set_text("넥사크로플랫폼");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "135", "122", "260", "25", null, null, this);
            obj.set_taborder("36");
            obj.set_text("Normal & Pushed");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "0", "119", "124", "104", null, null, this);
            obj.set_taborder("39");
            obj.set_text("Edit");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "225", "1005", "1", null, null, this);
            obj.set_taborder("40");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "135", "149", "260", "50", null, null, this);
            obj.set_taborder("42");
            obj.set_value("넥사크로플랫폼");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "135", "232", "260", "25", null, null, this);
            obj.set_taborder("46");
            obj.set_text("Normal & Pushed");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "338", "765", "1", null, null, this);
            obj.set_taborder("48");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "0", "235", "124", "104", null, null, this);
            obj.set_taborder("53");
            obj.set_text("MaskEdit");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit00", "absolute", "135", "261", "260", "50", null, null, this);
            obj.set_taborder("54");
            obj.set_value("123456789");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "0", "338", "1005", "1", null, null, this);
            obj.set_taborder("57");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "135", "344", "260", "45", null, null, this);
            obj.set_taborder("58");
            obj.set_text("Normal & Pushed");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "0", "605", "765", "1", null, null, this);
            obj.set_taborder("60");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "0", "338", "124", "188", null, null, this);
            obj.set_taborder("62");
            obj.set_text("TextArea");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "135", "386", "260", "176", null, null, this);
            obj.set_taborder("63");
            obj.set_value("넥사크로플랫폼\r\n가나다라마바사\r\n디자인\r\n");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "449", "61", "260", "35", null, null, this);
            obj.set_taborder("66");
            obj.set_text("넥사크로플랫폼");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "449", "35", "100", "25", null, null, this);
            obj.set_taborder("67");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "449", "149", "260", "50", null, null, this);
            obj.set_taborder("68");
            obj.set_value("넥사크로플랫폼");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "449", "122", "140", "25", null, null, this);
            obj.set_taborder("69");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "449", "232", "140", "25", null, null, this);
            obj.set_taborder("70");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit01", "absolute", "449", "258", "260", "50", null, null, this);
            obj.set_taborder("71");
            obj.set_value("123456789");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "449", "344", "150", "45", null, null, this);
            obj.set_taborder("72");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea01", "absolute", "449", "386", "260", "176", null, null, this);
            obj.set_taborder("73");
            obj.set_value("넥사크로플랫폼\r\n가나다라마바사\r\n디자인\r\n");
            obj.set_wordwrap("char");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "-92", "90", "140", "25", null, null, this);
            obj.set_taborder("74");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "737", "149", "260", "50", null, null, this);
            obj.set_taborder("75");
            obj.set_value("넥사크로플랫폼");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "-166", "209", "140", "25", null, null, this);
            obj.set_taborder("76");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("MaskEdit02", "absolute", "737", "258", "260", "50", null, null, this);
            obj.set_taborder("77");
            obj.set_value("123456789");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "737", "344", "150", "45", null, null, this);
            obj.set_taborder("78");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea02", "absolute", "737", "386", "260", "176", null, null, this);
            obj.set_taborder("79");
            obj.set_value("넥사크로플랫폼\r\n가나다라마바사\r\n디자인\r\n");
            obj.set_wordwrap("char");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "737", "232", "150", "25", null, null, this);
            obj.set_taborder("80");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "737", "122", "150", "25", null, null, this);
            obj.set_taborder("81");
            obj.set_text("Read Only");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1269, 605, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Static, Edit, MaskEdit, TextArea");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("CompGuide01.xfdl", true);

       
    };
}
)();
