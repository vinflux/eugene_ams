﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("ImageViewer, GroupBox");
                this._setFormPosition(0,0,765,540);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">Static</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">Edit</Col></Row><Row><Col id=\"code\">3</Col><Col id=\"data\">MaskEdit</Col></Row><Row><Col id=\"code\">4</Col><Col id=\"data\">TextArea</Col></Row><Row><Col id=\"code\">5</Col><Col id=\"data\">Button</Col></Row><Row><Col id=\"code\">6</Col><Col id=\"data\">Spin</Col></Row><Row><Col id=\"code\">7</Col><Col id=\"data\">CheckBox</Col></Row><Row><Col id=\"code\">8</Col><Col id=\"data\">Radio</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_Menu", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"caption\" type=\"STRING\" size=\"256\"/><Column id=\"level\" type=\"STRING\" size=\"256\"/><Column id=\"enabled\" type=\"STRING\" size=\"256\"/><Column id=\"check\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"id\">1</Col><Col id=\"caption\">Support</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">2</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">3</Col><Col id=\"caption\">Tip</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">4</Col><Col id=\"caption\">FAQ</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">5</Col><Col id=\"caption\">Showcase</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">6</Col><Col id=\"caption\">Design</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">7</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">8</Col><Col id=\"caption\">X-UP</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">9</Col><Col id=\"caption\">X-POP</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">10</Col><Col id=\"caption\">Download</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">11</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">12</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">13</Col><Col id=\"caption\">Setup</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">14</Col><Col id=\"caption\">Manual</Col><Col id=\"level\">2</Col></Row><Row><Col id=\"id\">15</Col><Col id=\"caption\">Runtime Admin</Col><Col id=\"level\">3</Col></Row><Row><Col id=\"id\">16</Col><Col id=\"caption\">Server</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">17</Col><Col id=\"caption\">Education</Col><Col id=\"level\">0</Col></Row><Row><Col id=\"id\">18</Col><Col id=\"caption\">MiPlatform</Col><Col id=\"level\">1</Col><Col id=\"enabled\">0</Col></Row><Row><Col id=\"id\">19</Col><Col id=\"caption\">XPLATFORM</Col><Col id=\"level\">1</Col></Row><Row><Col id=\"id\">20</Col><Col id=\"caption\">X-UP</Col><Col id=\"level\">1</Col><Col id=\"check\">1</Col></Row><Row><Col id=\"caption\">X-POP</Col><Col id=\"id\">21</Col><Col id=\"level\">1</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new ImageViewer("ImageViewer00", "absolute", "135", "93", "196", "89", null, null, this);
            obj.set_taborder("43");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new GroupBox("GroupBox00", "absolute", "135", "252", "174", "121", null, null, this);
            obj.set_text("GroupBox00");
            obj.set_taborder("48");
            this.addChild(obj.name, obj);

            obj = new GroupBox("GroupBox01", "absolute", "396", "252", "174", "121", null, null, this);
            obj.set_text("GroupBox00");
            obj.set_taborder("49");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("51");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("52");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "185", null, null, this);
            obj.set_taborder("53");
            obj.set_text("ImageViewer");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "135", "35", "100", "25", null, null, this);
            obj.set_taborder("54");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "396", "48", "303", "25", null, null, this);
            obj.set_taborder("55");
            obj.set_text("stretch : fixaspectratio");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "213", "621", "1", null, null, this);
            obj.set_taborder("56");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer01", "absolute", "396", "93", "196", "89", null, null, this);
            obj.set_taborder("57");
            obj.set_enable("true");
            obj.set_stretch("fixaspectratio");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "213", "124", "185", null, null, this);
            obj.set_taborder("58");
            obj.set_text("GroupBox");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "135", "219", "100", "25", null, null, this);
            obj.set_taborder("59");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "0", "397", "627", "1", null, null, this);
            obj.set_taborder("60");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "396", "222", "100", "25", null, null, this);
            obj.set_taborder("61");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 765, 540, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("ImageViewer, GroupBox");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Static03.addEventHandler("onclick", this.Static03_onclick, this);

        };

        this.loadIncludeScript("CompGuide04.xfdl", true);

       
    };
}
)();
