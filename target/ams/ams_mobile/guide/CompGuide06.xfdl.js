﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide05");
                this.set_classname("CompGuide01");
                this.set_titletext("Tab#1");
                this._setFormPosition(0,0,765,670);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Tab("Tab00", "absolute", "135", "70", "619", "120", null, null, this);
            obj.set_taborder("34");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            obj.set_tabjustify("false");
            obj.set_tabposition("top");
            obj.style.setStyleValue("color", "selected", "#ffffffff");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.Tab00);
            obj.set_text(" tabpage1");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.Tab00);
            obj.set_text(" tabpage2 ");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage3", this.Tab00);
            obj.set_text("tabpage3");
            this.Tab00.addChild(obj.name, obj);
            obj = new Tabpage("tabpage4", this.Tab00);
            obj.set_text("tabpage4");
            this.Tab00.addChild(obj.name, obj);

            obj = new Tab("Tab01", "absolute", "135", "359", "440", "303", null, null, this);
            obj.set_taborder("35");
            obj.set_tabindex("0");
            obj.set_multiline("true");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.Tab01);
            obj.set_text("tabpage1");
            this.Tab01.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.Tab01);
            obj.set_text("tabpage2");
            this.Tab01.addChild(obj.name, obj);
            obj = new Tabpage("tabpage3", this.Tab01);
            obj.set_text("tabpage3");
            this.Tab01.addChild(obj.name, obj);
            obj = new Tabpage("tabpage4", this.Tab01);
            obj.set_text("tabpage4");
            this.Tab01.addChild(obj.name, obj);
            obj = new Tabpage("tabpage5", this.Tab01);
            obj.set_text("tabpage5");
            this.Tab01.addChild(obj.name, obj);
            obj = new Tabpage("tabpage6", this.Tab01);
            obj.set_text("tabpage6");
            this.Tab01.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "135", "294", "246", "25", null, null, this);
            obj.set_taborder("36");
            obj.set_text("multiline = \"true \"");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Tab("Tab02", "absolute", "135", "220", "599", "115", null, null, this);
            obj.set_taborder("37");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tabpage1", this.Tab02);
            obj.set_text(" tabpage1 ");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage2", this.Tab02);
            obj.set_text(" tabpage2 ");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage3", this.Tab02);
            obj.set_text("tabpage3");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage4", this.Tab02);
            obj.set_text("tabpage4");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage5", this.Tab02);
            obj.set_text("tabpage5");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage6", this.Tab02);
            obj.set_text("tabpage6");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage7", this.Tab02);
            obj.set_text("tabpage7");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage8", this.Tab02);
            obj.set_text("tabpage8");
            this.Tab02.addChild(obj.name, obj);
            obj = new Tabpage("tabpage9", this.Tab02);
            obj.set_text("tabpage9");
            this.Tab02.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "135", "165", "381", "25", null, null, this);
            obj.set_taborder("38");
            obj.set_text("Tab>#spinupbutton, Tab>#spindownbutton");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("41");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("42");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "352", "39", "100", "25", null, null, this);
            obj.set_taborder("43");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "477", null, null, this);
            obj.set_taborder("44");
            obj.set_text("Tab#1");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "137", "39", "100", "25", null, null, this);
            obj.set_taborder("45");
            obj.set_text("Select");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_font("9 verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 765, 670, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Tab#1");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("CompGuide06.xfdl", function(exports) {

        this.btntest_onclick = function(obj,e){
        	this.Pbr00.set_pos(0);

        	this.setTimer(0, 50);		
        }

        this.CompGuide05_ontimer = function(obj,e)
        {
        	if(e.timerid == 0)
        	{
        		if(this.Pbr00.pos == 100)
        		{
        			this.killTimer(0);		
        		}
        		this.Pbr00.getNumSetter("pos").postInc();
        			
        	}	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("ontimer", this.CompGuide05_ontimer, this);
            this.Static06.addEventHandler("onclick", this.Static06_onclick, this);

        };

        this.loadIncludeScript("CompGuide06.xfdl", true);

       
    };
}
)();
