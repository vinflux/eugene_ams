﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UserGroup");
                this.set_classname("style01");
                this.set_titletext("사용자그룹");
                this._setFormPosition(0,0,1230,627);
            }
            this.getSetter("menuId").set("");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_userGroup", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"1\"/><Column id=\"urgrkey\" type=\"string\" size=\"32\"/><Column id=\"updatedate\" type=\"string\" size=\"32\"/><Column id=\"inserturkey\" type=\"string\" size=\"32\"/><Column id=\"insertdate\" type=\"string\" size=\"32\"/><Column id=\"updateurkey\" type=\"string\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"bigdecimal\" size=\"8\"/><Column id=\"urgrname\" type=\"string\" size=\"32\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_adminDetailCode", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"200\"/><Column id=\"adcd_dtkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtvalue\" type=\"STRING\" size=\"256\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"DATE\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtorder\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_dtname\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"adcd_hdkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">urgrkey</Col><Col id=\"FILED_NAME1\">사용자 그룹 코드</Col><Col id=\"FILED_NAME2\">사용자 그룹 코드</Col><Col id=\"FILED_NAME3\">사용자 그룹 코드</Col></Row><Row><Col id=\"DATA_FILED\">urgrname</Col><Col id=\"FILED_NAME1\">사용자 그룹 명</Col><Col id=\"FILED_NAME2\">사용자 그룹 명</Col><Col id=\"FILED_NAME3\">사용자 그룹 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ubiReport", this);
            obj._setContents("<ColumnInfo><Column id=\"udsName\" type=\"STRING\" size=\"256\"/><Column id=\"abc\" type=\"STRING\" size=\"256\"/><Column id=\"taxyn\" type=\"STRING\" size=\"256\"/><Column id=\"icname\" type=\"STRING\" size=\"256\"/><Column id=\"ordtype\" type=\"STRING\" size=\"256\"/><Column id=\"to_arkey\" type=\"STRING\" size=\"256\"/><Column id=\"returnor\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"ordstopdate\" type=\"STRING\" size=\"256\"/><Column id=\"BRANDCODE\" type=\"STRING\" size=\"256\"/><Column id=\"height\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"bottleprice\" type=\"STRING\" size=\"256\"/><Column id=\"maxoverqty\" type=\"STRING\" size=\"256\"/><Column id=\"shelflifeindicator_desc\" type=\"STRING\" size=\"256\"/><Column id=\"icutkey\" type=\"STRING\" size=\"256\"/><Column id=\"boxprice\" type=\"STRING\" size=\"256\"/><Column id=\"ordtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"shelflifecodetype\" type=\"STRING\" size=\"256\"/><Column id=\"width\" type=\"STRING\" size=\"256\"/><Column id=\"ordttype\" type=\"STRING\" size=\"256\"/><Column id=\"icgrname\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/><Column id=\"ackey\" type=\"STRING\" size=\"256\"/><Column id=\"shelfday\" type=\"STRING\" size=\"256\"/><Column id=\"icgrtype\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"acname\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"lo_non_stock_type_desc\" type=\"STRING\" size=\"256\"/><Column id=\"safe_invqty\" type=\"STRING\" size=\"256\"/><Column id=\"DISPSHELFKEY\" type=\"STRING\" size=\"256\"/><Column id=\"po_leadtime\" type=\"STRING\" size=\"256\"/><Column id=\"ictype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"lo_non_stock_type\" type=\"STRING\" size=\"256\"/><Column id=\"spec\" type=\"STRING\" size=\"256\"/><Column id=\"weight\" type=\"STRING\" size=\"256\"/><Column id=\"ctg_level_nm3\" type=\"STRING\" size=\"256\"/><Column id=\"voltype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ickey\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"to_lckey\" type=\"STRING\" size=\"256\"/><Column id=\"abc_desc\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"256\"/><Column id=\"shelflifeindicator\" type=\"STRING\" size=\"256\"/><Column id=\"udf2\" type=\"STRING\" size=\"256\"/><Column id=\"udf1\" type=\"STRING\" size=\"256\"/><Column id=\"urkey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"udf5\" type=\"STRING\" size=\"256\"/><Column id=\"uom\" type=\"STRING\" size=\"256\"/><Column id=\"returnor_desc\" type=\"STRING\" size=\"256\"/><Column id=\"udf4\" type=\"STRING\" size=\"256\"/><Column id=\"ictype\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"256\"/><Column id=\"shelflifecodetype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"serialinputtype\" type=\"STRING\" size=\"256\"/><Column id=\"ctg_level_nm2\" type=\"STRING\" size=\"256\"/><Column id=\"ctg_level_nm1\" type=\"STRING\" size=\"256\"/><Column id=\"uom_desc\" type=\"STRING\" size=\"256\"/><Column id=\"serialinputtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"saleprice\" type=\"STRING\" size=\"256\"/><Column id=\"shelflife\" type=\"STRING\" size=\"256\"/><Column id=\"costprice\" type=\"STRING\" size=\"256\"/><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"pick_lckey\" type=\"STRING\" size=\"256\"/><Column id=\"taxyn_desc\" type=\"STRING\" size=\"256\"/><Column id=\"voltype\" type=\"STRING\" size=\"256\"/><Column id=\"foodyn\" type=\"STRING\" size=\"256\"/><Column id=\"releasedays\" type=\"STRING\" size=\"256\"/><Column id=\"foodyn_desc\" type=\"STRING\" size=\"256\"/><Column id=\"volume\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"seq\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"udsName\">DS</Col><Col id=\"abc\"/><Col id=\"taxyn\">Y</Col><Col id=\"icname\">롯데)자일리톨용기껌52g</Col><Col id=\"ordtype\">0</Col><Col id=\"to_arkey\"/><Col id=\"returnor\">2</Col><Col id=\"owkey\">HANSOL</Col><Col id=\"ordstopdate\"/><Col id=\"BRANDCODE\"/><Col id=\"height\">65</Col><Col id=\"closingdate\">20391231235959</Col><Col id=\"bottleprice\">0</Col><Col id=\"maxoverqty\"/><Col id=\"shelflifeindicator_desc\">네</Col><Col id=\"icutkey\">8801062214778</Col><Col id=\"boxprice\">0</Col><Col id=\"ordtype_desc\">정상</Col><Col id=\"shelflifecodetype\">E</Col><Col id=\"width\">50</Col><Col id=\"ordttype\">0</Col><Col id=\"icgrname\">공산품</Col><Col id=\"loggrpcd\">1</Col><Col id=\"ackey\">0301130</Col><Col id=\"shelfday\"/><Col id=\"icgrtype\">CATEGORY</Col><Col id=\"insertdate\">20141124153136</Col><Col id=\"acname\">롯데제과</Col><Col id=\"PAGING_NUM\">1</Col><Col id=\"lo_non_stock_type_desc\">재고</Col><Col id=\"safe_invqty\">0</Col><Col id=\"DISPSHELFKEY\">05</Col><Col id=\"po_leadtime\"/><Col id=\"ictype_desc\">이벤트상품</Col><Col id=\"lo_non_stock_type\">0</Col><Col id=\"spec\">52</Col><Col id=\"weight\">70</Col><Col id=\"ctg_level_nm3\"/><Col id=\"voltype_desc\">미지정</Col><Col id=\"ickey\">8801062214778</Col><Col id=\"loggrpcd_desc\">상온</Col><Col id=\"to_lckey\">6711028</Col><Col id=\"abc_desc\"/><Col id=\"icgrkey\">01</Col><Col id=\"shelflifeindicator\">Y</Col><Col id=\"udf2\"/><Col id=\"udf1\"/><Col id=\"urkey_desc\"/><Col id=\"udf5\"/><Col id=\"uom\">EA</Col><Col id=\"returnor_desc\">한도내</Col><Col id=\"udf4\"/><Col id=\"ictype\">2</Col><Col id=\"inserturkey\">EAI</Col><Col id=\"udf3\"/><Col id=\"shelflifecodetype_desc\">유효 일자(유통일)</Col><Col id=\"serialinputtype\"/><Col id=\"ctg_level_nm2\"/><Col id=\"ctg_level_nm1\"/><Col id=\"uom_desc\">낱개</Col><Col id=\"serialinputtype_desc\"/><Col id=\"saleprice\">0</Col><Col id=\"shelflife\">274</Col><Col id=\"costprice\">0</Col><Col id=\"urkey\"/><Col id=\"pick_lckey\"/><Col id=\"taxyn_desc\">네</Col><Col id=\"voltype\">9</Col><Col id=\"foodyn\"/><Col id=\"releasedays\">243</Col><Col id=\"foodyn_desc\"/><Col id=\"volume\">133250</Col><Col id=\"updateurkey\"/><Col id=\"updatedate\">20160119141652</Col><Col id=\"ctkey\">801</Col><Col id=\"id\">GridModel_grid-1306219776-1</Col><Col id=\"seq\">0</Col></Row><Row><Col id=\"udsName\">DS</Col><Col id=\"abc\"/><Col id=\"taxyn\">Y</Col><Col id=\"icname\">PB)졸음뚝용기껌</Col><Col id=\"ordtype\">0</Col><Col id=\"to_arkey\"/><Col id=\"returnor\">2</Col><Col id=\"owkey\">HANSOL</Col><Col id=\"ordstopdate\"/><Col id=\"BRANDCODE\"/><Col id=\"height\">50</Col><Col id=\"closingdate\">20391231235959</Col><Col id=\"bottleprice\">0</Col><Col id=\"maxoverqty\"/><Col id=\"shelflifeindicator_desc\">네</Col><Col id=\"icutkey\">8802534901448</Col><Col id=\"boxprice\">0</Col><Col id=\"ordtype_desc\">정상</Col><Col id=\"shelflifecodetype\">E</Col><Col id=\"icgrname\">공산품</Col><Col id=\"width\">50</Col><Col id=\"ordttype\">0</Col><Col id=\"loggrpcd\">1</Col><Col id=\"ackey\">0322480</Col><Col id=\"shelfday\"/><Col id=\"icgrtype\">CATEGORY</Col><Col id=\"insertdate\">20141124153136</Col><Col id=\"acname\">대영식품공업PB</Col><Col id=\"PAGING_NUM\">2</Col><Col id=\"lo_non_stock_type_desc\">재고</Col><Col id=\"safe_invqty\">0</Col><Col id=\"DISPSHELFKEY\">05</Col><Col id=\"po_leadtime\"/><Col id=\"ictype_desc\">이벤트상품</Col><Col id=\"lo_non_stock_type\">0</Col><Col id=\"spec\">108g</Col><Col id=\"weight\">150</Col><Col id=\"ctg_level_nm3\"/><Col id=\"voltype_desc\">미지정</Col><Col id=\"ickey\">8802534901448</Col><Col id=\"loggrpcd_desc\">상온</Col><Col id=\"to_lckey\">6211014</Col><Col id=\"abc_desc\"/><Col id=\"icgrkey\">01</Col><Col id=\"shelflifeindicator\">Y</Col><Col id=\"udf2\"/><Col id=\"udf1\"/><Col id=\"urkey_desc\"/><Col id=\"udf5\"/><Col id=\"uom\">EA</Col><Col id=\"returnor_desc\">한도내</Col><Col id=\"udf4\"/><Col id=\"ictype\">2</Col><Col id=\"inserturkey\">EAI</Col><Col id=\"shelflifecodetype_desc\">유효 일자(유통일)</Col><Col id=\"udf3\"/><Col id=\"serialinputtype\"/><Col id=\"ctg_level_nm2\"/><Col id=\"ctg_level_nm1\"/><Col id=\"uom_desc\">낱개</Col><Col id=\"serialinputtype_desc\"/><Col id=\"saleprice\">0</Col><Col id=\"shelflife\">240</Col><Col id=\"costprice\">0</Col><Col id=\"urkey\"/><Col id=\"pick_lckey\"/><Col id=\"taxyn_desc\">네</Col><Col id=\"voltype\">9</Col><Col id=\"foodyn\"/><Col id=\"releasedays\">180</Col><Col id=\"foodyn_desc\"/><Col id=\"volume\">212500</Col><Col id=\"updateurkey\"/><Col id=\"updatedate\">20160119141652</Col><Col id=\"ctkey\">801</Col><Col id=\"id\">GridModel_grid-1306219776-2</Col><Col id=\"seq\">1</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "148", null, this.div_splitTop);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_excelAll", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_userGroup", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_userGroup");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("none");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj.getSetter("userdata").set("0");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/><Column size=\"40\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\"/><Cell col=\"3\"/><Cell col=\"4\"/><Cell col=\"5\"/><Cell col=\"6\"/><Cell col=\"7\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding:0 2 0 2;\" text=\"bind:PAGING_NUM\"/><Cell col=\"2\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:urgrkey\"/><Cell col=\"3\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:urgrname\"/><Cell col=\"4\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"6\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"7\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_work", "absolute", "578", "104", null, "257", "40", null, this.div_splitTop);
            obj.set_taborder("7");
            obj.set_text("Div00");
            obj.style.set_border("1 solid #b1b1b1ff");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_cond_01", "absolute", "10", "10", "200", "30", null, null, this.div_splitTop.div_work);
            obj.set_taborder("6");
            obj.set_text("div_cond_01");
            obj.style.set_background("blue");
            obj.style.set_color("white");
            this.div_splitTop.div_work.addChild(obj.name, obj);
            obj = new Div("div_cond_02", "absolute", "10", "50", "200", "30", null, null, this.div_splitTop.div_work);
            obj.set_taborder("7");
            obj.set_text("div_cond_02");
            obj.style.set_background("blue");
            obj.style.set_color("white");
            this.div_splitTop.div_work.addChild(obj.name, obj);
            obj = new Div("div_cond_04", "absolute", "10", "130", "200", "30", null, null, this.div_splitTop.div_work);
            obj.set_taborder("8");
            obj.set_text("div_cond_04");
            obj.style.set_background("blue");
            obj.style.set_color("white");
            this.div_splitTop.div_work.addChild(obj.name, obj);
            obj = new Div("div_cond_03", "absolute", "10", "90", "200", "30", null, null, this.div_splitTop.div_work);
            obj.set_taborder("9");
            obj.set_text("div_cond_03");
            obj.style.set_background("blue");
            obj.style.set_color("white");
            this.div_splitTop.div_work.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new WebBrowser("web_ubiReport", "absolute", "1245", "75", "435", "271", null, null, this);
            obj.set_taborder("50");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 257, this.div_splitTop.div_work,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("7");
            		p.set_text("Div00");
            		p.style.set_border("1 solid #b1b1b1ff");

            	}
            );
            this.div_splitTop.div_work.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자그룹");
            		p.getSetter("menuId").set("");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("UserGroup.xfdl", "lib::Comm.xjs");
        this.registerScript("UserGroup.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AdminCode.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 관리 기준 코드 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        //this.gv_split = "";

        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_initForm(obj);
        	
        	var gridList = [this.div_splitTop.grd_userGroup];
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var gridMenuSet = ["1|0|0|0|0|0|1|1", "0|0|0|0|0|0|1|1"];
        	var divPaging = [this.div_splitTop.div_Paging];
        	var searchFunc = [this.gv_main];
        	
        	this.gfn_gridInit(gridList, gridMenuSet, divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_userGroup.set_keystring(""); // 필터 초기화
        	this.ds_userGroup.clearData();
        	
        	this.gfn_setCommon("BEANID", "userGroupController");
        	this.gfn_setCommon("METHODNM", "selectUserGroupInfoList");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_userGroup=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        
        this.fn_delete = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "deleteAdminCode");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_AdminCode=ds_adminCode:U";
            var sOutData = "";
            var sParam   = "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.pageSet(this.div_splitTop.div_Paging, this.ds_param, true);
        		
        	}else if(sSvcId == "delete"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "deleteDetail"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_SearchDetail();
        	}else if(sSvcId == "exceldown"){
        		this.gfn_excelDownload(this.ds_excelFile);
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /* div_splitTop_grd_adminCode_oncelldblclick 실행 */
        this.div_splitTop_grd_adminCode_oncelldblclick = function(obj,e)
        {
        	this.fn_SearchDetail();
        }

        /* div_splitTop_grd_adminCode_onrbuttonup 실행 */
        this.div_splitTop_grd_adminCode_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "0|0|0|0|0|0|1|1";
        	this.gfn_openGridMenu(obj, e, enable_list);
        }

        /* div_splitBottom_grd_adminDetailCode_onrbuttonup 실행 */
        this.div_splitBottom_grd_adminDetailCode_onrbuttonup = function(obj,e)
        {
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	var enable_list = "0|0|0|0|0|0|1|1";
        	this.gfn_openGridMenu(obj, e, enable_list);
        }

        /* div_splitTop_grd_adminCode_onkeydown 실행 */
        this.div_splitTop_grd_adminCode_onkeydown = function(obj,e)
        {
        	this.gfn_GridCopy(obj, e, this.ds_adminCode, obj.currentcell);
        }

        
        /* div_splitTop_grd_adminCode_onheadclick 실행 */
        this.div_splitTop_grd_adminCode_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_adminCode.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitBottom_grd_adminDetailCode_onheadclick 실행 */
        this.div_splitBottom_grd_adminDetailCode_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.div_splitTop.grd_adminCode, "AdminCode");
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_adminCode.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("UserGroupPop", "master::UserGroupPop.xfdl", oArg, 580, 187, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	var oArg = "";
        	var nRow = this.ds_adminCode.findRow("CHK", "1");
        	var nRowCnt = this.ds_adminCode.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else if(nRowCnt > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        	}else{
        		oArg = { argFlag:"U"
        				,menuId:this.parent.gv_menuId
        				,adcd_hdkey:this.ds_adminCode.getColumn(nRow, "adcd_hdkey")
        				,adcd_hdname:this.ds_adminCode.getColumn(nRow, "adcd_hdname")
        			   };
        		this.gfn_popup("AdminCodePop", "master::AdminCodePop.xfdl", oArg, 580, 187, "");
        	}
        }

        /* div_splitTop_grd_adminCode_oncellclick 실행 */
        this.div_splitTop_grd_adminCode_oncellclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var chkValue = this.ds_adminCode.getColumn(e.row, "CHK");
        	
        	if(colName != "CHK"){
        		if(chkValue == "1") this.ds_adminCode.setColumn(e.row, "CHK", "");
        		else this.ds_adminCode.setColumn(e.row, "CHK", "1");
        	}
        }

        
        this.div_splitTop_btn_excelAll_onclick = function(obj,e)
        {
        	//var ubiUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/wms/ubireport/work/itemcodelabel.jrf";
        	var ubiUrl = application.services["svc"].url+"wms/reportController/ubiReport.do";
        	var param = "udsName=DS&reportParam=%7B%22PARAM%22%3A%7B%22pagingLimit%22%3A50%2C%22currentPage%22%3A%221%22%2C%22ctkey%22%3A%22801%22%2C%22jrfName%22%3A%22itemcodelabel.jrf%22%2C%22prType%22%3Anull%2C%22eqtype%22%3Anull%2C%22printType%22%3A%22preview%22%2C%22isMultiReport%22%3A%22false%22%2C%22multiCount%22%3A%220%22%2C%22printLogYN%22%3A%22N%22%7D%2C%22MAP%22%3A%7B%7D%2C%22LIST%22%3A%7B%22DATA_LIST%22%3A%5B%7B%22abc%22%3A%22%22%2C%22taxyn%22%3A%22Y%22%2C%22icname%22%3A%22%5Cub86f%5Cub370%29%5Cuc790%5Cuc77c%5Cub9ac%5Cud1a8%5Cuc6a9%5Cuae30%5Cuaecc52g%22%2C%22ordtype%22%3A%220%22%2C%22to_arkey%22%3A%22%22%2C%22returnor%22%3A%222%22%2C%22owkey%22%3A%22HANSOL%22%2C%22ordstopdate%22%3A%22%22%2C%22BRANDCODE%22%3A%22%22%2C%22height%22%3A%2265%22%2C%22closingdate%22%3A%2220391231235959%22%2C%22bottleprice%22%3A%220%22%2C%22maxoverqty%22%3A%22%22%2C%22shelflifeindicator_desc%22%3A%22%5Cub124%22%2C%22icutkey%22%3A%228801062214778%22%2C%22boxprice%22%3A%220%22%2C%22ordtype_desc%22%3A%22%5Cuc815%5Cuc0c1%22%2C%22shelflifecodetype%22%3A%22E%22%2C%22width%22%3A%2250%22%2C%22ordttype%22%3A%220%22%2C%22icgrname%22%3A%22%5Cuacf5%5Cuc0b0%5Cud488%22%2C%22loggrpcd%22%3A%221%22%2C%22ackey%22%3A%220301130%22%2C%22shelfday%22%3A%22%22%2C%22icgrtype%22%3A%22CATEGORY%22%2C%22insertdate%22%3A%2220141124153136%22%2C%22acname%22%3A%22%5Cub86f%5Cub370%5Cuc81c%5Cuacfc%22%2C%22PAGING_NUM%22%3A%221%22%2C%22lo_non_stock_type_desc%22%3A%22%5Cuc7ac%5Cuace0%22%2C%22safe_invqty%22%3A%220%22%2C%22DISPSHELFKEY%22%3A%2205%22%2C%22po_leadtime%22%3A%22%22%2C%22ictype_desc%22%3A%22%5Cuc774%5Cubca4%5Cud2b8%5Cuc0c1%5Cud488%22%2C%22lo_non_stock_type%22%3A%220%22%2C%22spec%22%3A%2252%22%2C%22weight%22%3A%2270%22%2C%22ctg_level_nm3%22%3A%22%22%2C%22voltype_desc%22%3A%22%5Cubbf8%5Cuc9c0%5Cuc815%22%2C%22ickey%22%3A%228801062214778%22%2C%22loggrpcd_desc%22%3A%22%5Cuc0c1%5Cuc628%22%2C%22to_lckey%22%3A%226711028%22%2C%22abc_desc%22%3A%22%22%2C%22icgrkey%22%3A%2201%22%2C%22shelflifeindicator%22%3A%22Y%22%2C%22udf2%22%3A%22%22%2C%22udf1%22%3A%22%22%2C%22urkey_desc%22%3A%22%22%2C%22udf5%22%3A%22%22%2C%22uom%22%3A%22EA%22%2C%22returnor_desc%22%3A%22%5Cud55c%5Cub3c4%5Cub0b4%22%2C%22udf4%22%3A%22%22%2C%22ictype%22%3A%222%22%2C%22inserturkey%22%3A%22EAI%22%2C%22udf3%22%3A%22%22%2C%22shelflifecodetype_desc%22%3A%22%5Cuc720%5Cud6a8+%5Cuc77c%5Cuc790%28%5Cuc720%5Cud1b5%5Cuc77c%29%22%2C%22length%22%3A%2241%22%2C%22serialinputtype%22%3A%22%22%2C%22ctg_level_nm2%22%3A%22%22%2C%22ctg_level_nm1%22%3A%22%22%2C%22uom_desc%22%3A%22%5Cub0b1%5Cuac1c%22%2C%22serialinputtype_desc%22%3A%22%22%2C%22saleprice%22%3A%220%22%2C%22shelflife%22%3A%22274%22%2C%22costprice%22%3A%220%22%2C%22urkey%22%3A%22%22%2C%22pick_lckey%22%3A%22%22%2C%22taxyn_desc%22%3A%22%5Cub124%22%2C%22voltype%22%3A%229%22%2C%22foodyn%22%3A%22%22%2C%22releasedays%22%3A%22243%22%2C%22foodyn_desc%22%3A%22%22%2C%22volume%22%3A%22133250%22%2C%22updateurkey%22%3A%22%22%2C%22updatedate%22%3A%2220160119141652%22%2C%22ctkey%22%3A%22801%22%2C%22id%22%3A%22GridModel_grid-1306219776-1%22%2C%22seq%22%3A%220%22%7D%2C%7B%22abc%22%3A%22%22%2C%22taxyn%22%3A%22Y%22%2C%22icname%22%3A%22PB%29%5Cuc878%5Cuc74c%5Cub69d%5Cuc6a9%5Cuae30%5Cuaecc%22%2C%22ordtype%22%3A%220%22%2C%22to_arkey%22%3A%22%22%2C%22returnor%22%3A%222%22%2C%22owkey%22%3A%22HANSOL%22%2C%22ordstopdate%22%3A%2220120702090000%22%2C%22BRANDCODE%22%3A%22%22%2C%22height%22%3A%2250%22%2C%22closingdate%22%3A%2220391231235959%22%2C%22bottleprice%22%3A%220%22%2C%22maxoverqty%22%3A%22%22%2C%22shelflifeindicator_desc%22%3A%22%5Cub124%22%2C%22icutkey%22%3A%228802534901448%22%2C%22boxprice%22%3A%220%22%2C%22ordtype_desc%22%3A%22%5Cuc815%5Cuc0c1%22%2C%22shelflifecodetype%22%3A%22E%22%2C%22width%22%3A%2250%22%2C%22ordttype%22%3A%220%22%2C%22icgrname%22%3A%22%5Cuacf5%5Cuc0b0%5Cud488%22%2C%22loggrpcd%22%3A%221%22%2C%22ackey%22%3A%220322480%22%2C%22shelfday%22%3A%22%22%2C%22icgrtype%22%3A%22CATEGORY%22%2C%22insertdate%22%3A%2220141124153136%22%2C%22acname%22%3A%22%5Cub300%5Cuc601%5Cuc2dd%5Cud488%5Cuacf5%5Cuc5c5PB%22%2C%22PAGING_NUM%22%3A%222%22%2C%22lo_non_stock_type_desc%22%3A%22%5Cuc7ac%5Cuace0%22%2C%22safe_invqty%22%3A%220%22%2C%22DISPSHELFKEY%22%3A%2205%22%2C%22po_leadtime%22%3A%22%22%2C%22ictype_desc%22%3A%22%5Cuc774%5Cubca4%5Cud2b8%5Cuc0c1%5Cud488%22%2C%22lo_non_stock_type%22%3A%220%22%2C%22spec%22%3A%22108g%22%2C%22weight%22%3A%22150%22%2C%22ctg_level_nm3%22%3A%22%22%2C%22voltype_desc%22%3A%22%5Cubbf8%5Cuc9c0%5Cuc815%22%2C%22ickey%22%3A%228802534901448%22%2C%22loggrpcd_desc%22%3A%22%5Cuc0c1%5Cuc628%22%2C%22to_lckey%22%3A%226211014%22%2C%22abc_desc%22%3A%22%22%2C%22icgrkey%22%3A%2201%22%2C%22shelflifeindicator%22%3A%22Y%22%2C%22udf2%22%3A%22%22%2C%22udf1%22%3A%22%22%2C%22urkey_desc%22%3A%22%22%2C%22udf5%22%3A%22%22%2C%22uom%22%3A%22EA%22%2C%22returnor_desc%22%3A%22%5Cud55c%5Cub3c4%5Cub0b4%22%2C%22udf4%22%3A%22%22%2C%22ictype%22%3A%222%22%2C%22inserturkey%22%3A%22EAI%22%2C%22udf3%22%3A%22%22%2C%22shelflifecodetype_desc%22%3A%22%5Cuc720%5Cud6a8+%5Cuc77c%5Cuc790%28%5Cuc720%5Cud1b5%5Cuc77c%29%22%2C%22length%22%3A%2285%22%2C%22serialinputtype%22%3A%22%22%2C%22ctg_level_nm2%22%3A%22%22%2C%22ctg_level_nm1%22%3A%22%22%2C%22uom_desc%22%3A%22%5Cub0b1%5Cuac1c%22%2C%22serialinputtype_desc%22%3A%22%22%2C%22saleprice%22%3A%220%22%2C%22shelflife%22%3A%22240%22%2C%22costprice%22%3A%220%22%2C%22urkey%22%3A%22%22%2C%22pick_lckey%22%3A%22%22%2C%22taxyn_desc%22%3A%22%5Cub124%22%2C%22voltype%22%3A%229%22%2C%22foodyn%22%3A%22%22%2C%22releasedays%22%3A%22180%22%2C%22foodyn_desc%22%3A%22%22%2C%22volume%22%3A%22212500%22%2C%22updateurkey%22%3A%22%22%2C%22updatedate%22%3A%2220160119141652%22%2C%22ctkey%22%3A%22801%22%2C%22id%22%3A%22GridModel_grid-1306219776-2%22%2C%22seq%22%3A%221%22%7D%5D%7D%2C%22COMMON%22%3A%7B%22DEVICE%22%3A%22PC%22%2C%22EQTYPE%22%3A%2210%22%2C%22TIMEZONE%22%3A%229%22%2C%22uskey%22%3A%22US00000510%22%2C%22USER_INFO%22%3A%7B%22urKey%22%3A%22eungheon.kim%22%2C%22rstclgst%22%3A%22N%22%2C%22loginStatus%22%3A%22SUCCESS%22%2C%22owkeym%22%3A%22HANSOL%22%2C%22ctKey_desc%22%3A%22%5Cud55c%5Cuc194+%5Cuc13c%5Cud130%22%2C%22icgrkey%22%3A%22%22%2C%22dst%22%3A%22N%22%2C%22rstcdiv%22%3A%22N%22%2C%22urGrKey%22%3A%22SYSADM%22%2C%22ctKey%22%3A%22801%22%2C%22SESSION_USERINFO%22%3A%22SESSION_USERINFO%22%2C%22rep_ackey%22%3A%220000000%22%2C%22ipAddress%22%3A%22%22%2C%22expireDateStatus%22%3A%22VALID%22%2C%22usKey%22%3A%22%22%2C%22divcd%22%3A%22%22%2C%22urName%22%3A%22%5Cuae40%5Cuc751%5Cud5cc%22%2C%22usrtyp%22%3A%22%22%2C%22urliloh_key%22%3A%221600009731%22%2C%22loggrpcd%22%3A%22%22%2C%22duplLog%22%3A%22N%22%2C%22utcHour%22%3A%229%22%2C%22laKey%22%3A%22KOR%22%2C%22utcOffset%22%3A%22%2B0900%22%2C%22utcMinute%22%3A%220%22%7D%2C%22ACTIVE_APP%22%3A%22WMS%22%2C%22OWKEY_AUTH%22%3A%22BGF%2CHANSOL%2CWINFOOD%2C%5Cuc9c0%5Cuc624%5Cuc601%2C11ST%22%2C%22CTKEY_AUTH%22%3A%22801%2C802%2C901%2C902%2C903%2C904%2C905%2C906%2C907%2C908%2C909%2C912%2C913%2C916%2C917%2C918%2C920%2C921%2C923%2C924%2C925%2C926%2C927%2C928%2C929%2C930%2C998%2C999%22%7D%2C%22SEARCHLIST%22%3A%5B%5D%7D";
        	var fullUrl = ubiUrl + "?"+param;
        	trace("fullUrl : "+fullUrl);
        	this.web_ubiReport.set_url(fullUrl);
        	this.web_ubiReport.updateWindow;
        }

        this.excel_back = function(){
        	this.gfn_setParam("EXCEL_FORMAT", "EXCEL_FORMAT");
        	this.gfn_setParam("EXCEL_FILENAME", "userGroup");
        	
        	this.gfn_setCommon("FILE_BEANID", "userGroupService");
        	this.gfn_setCommon("FILE_METHODNM", "selectExcelDownUserGroup");
        	
            var sSvcId   = "exceldown";
            var sSvcUrl  = application.gv_ams + application.gv_fUrl;
            var sInData  = "IN_EXCEL_FORMAT=ds_excelSetting";
            var sOutData = "ds_excelFile=OUT_FILE_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.btn_excelAll.addEventHandler("onclick", this.div_splitTop_btn_excelAll_onclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("onheadclick", this.div_splitTop_grd_adminCode_onheadclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("oncelldblclick", this.div_splitTop_grd_adminCode_oncelldblclick, this);
            this.div_splitTop.grd_userGroup.addEventHandler("oncellclick", this.div_splitTop_grd_adminCode_oncellclick, this);
            this.div_splitTop.div_work.addEventHandler("onsize", this.div_splitTop_div_work_onsize, this);

        };

        this.loadIncludeScript("UserGroup.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
