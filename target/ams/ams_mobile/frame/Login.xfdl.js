﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("frame_login");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,1097);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_lang", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_version", this);
            obj._setContents("<ColumnInfo><Column id=\"filepath\" type=\"STRING\" size=\"256\"/><Column id=\"filenm\" type=\"STRING\" size=\"256\"/><Column id=\"appversion\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row/></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("sta_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_login_bg");
            obj.style.set_background("URL('theme://images/img_login_back_lotte.png') stretch");
            this.addChild(obj.name, obj);

            obj = new Div("div_login", "absolute", "76", "328", "488", "536", null, null, this);
            obj.set_taborder("1");
            obj.style.set_background("#ffffff80");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_bordertype("round 10 10");
            obj.style.set_accessibility("disable all '' '' ''");
            obj.set_scrollbars("none");
            obj.set_visible("false");
            this.addChild(obj.name, obj);
            obj = new Edit("edt_id", "absolute", "33", "114", "420", "67", null, null, this.div_login);
            obj.set_taborder("7");
            obj.set_displaynulltext("ID");
            obj.set_cssclass("edt_login_id");
            this.div_login.addChild(obj.name, obj);
            obj = new Edit("edt_pass", "absolute", "33", "191", "420", "67", null, null, this.div_login);
            obj.set_taborder("8");
            obj.set_password("true");
            obj.set_displaynulltext("PASSWORD");
            obj.set_cssclass("edt_login_pw");
            this.div_login.addChild(obj.name, obj);
            obj = new Button("btn_login", "absolute", "33", "372", "420", "91", null, null, this.div_login);
            obj.set_taborder("9");
            obj.set_text("LOGIN");
            obj.set_cssclass("btn_login");
            obj.style.set_background("#ec1c2aff");
            this.div_login.addChild(obj.name, obj);
            obj = new CheckBox("chk_idSave", "absolute", "37", "274", "283", "46", null, null, this.div_login);
            obj.set_taborder("10");
            obj.set_text("아이디 저장");
            obj.set_cssclass("check_login");
            this.div_login.addChild(obj.name, obj);
            obj = new Combo("cbo_lang", "absolute", "33", "37", "420", "67", null, null, this.div_login);
            this.div_login.addChild(obj.name, obj);
            var cbo_lang_innerdataset = new Dataset("cbo_lang_innerdataset", this.div_login.cbo_lang);
            cbo_lang_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">KOR</Col><Col id=\"datacolumn\">KOREA</Col></Row><Row><Col id=\"codecolumn\">ENG</Col><Col id=\"datacolumn\">ENGNGLISH</Col></Row></Rows>");
            obj.set_innerdataset(cbo_lang_innerdataset);
            obj.set_taborder("11");
            obj.set_value("KOR");
            obj.set_text("KOREA");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_cssclass("com_login_lange");
            obj.style.set_font("30 dotum");
            obj.set_index("0");

            obj = new Static("Static02", "absolute", "0", null, null, "20", "0", "46", this);
            obj.set_taborder("2");
            obj.set_text("ⓒ VINFLUX company: Corp All Rights Reserved. (ver 2.0)");
            obj.style.set_color("#ffffff80");
            obj.style.set_align("center middle");
            obj.style.set_font("11 Dotum");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer01", "absolute", "0", "52", null, "130", "0", null, this);
            obj.set_taborder("3");
            obj.set_image("URL('img::EUGENEimg.png')");
            obj.style.set_background("transparent");
            obj.style.set_border("0 none #808080ff");
            obj.set_stretch("fixaspectratio");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new WebBrowser("wb_webContext", "absolute", "653", "10", "105", "26", null, null, this);
            obj.set_taborder("4");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "653", "48", "120", "50", null, null, this);
            obj.set_taborder("5");
            obj.set_text("Button00");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("stc_appversion", "absolute", "20", null, null, "20", "20", "6", this);
            obj.set_taborder("6");
            obj.set_text("App version : ");
            obj.style.set_color("#ffffff80");
            obj.style.set_align("right middle");
            obj.style.set_font("11 Dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 488, 536, this.div_login,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.style.set_background("#ffffff80");
            		p.style.set_border("0 none #808080ff");
            		p.style.set_bordertype("round 10 10");
            		p.style.set_accessibility("disable all '' '' ''");
            		p.set_scrollbars("none");
            		p.set_visible("false");

            	}
            );
            this.div_login.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 640, 1097, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Login.xfdl", "lib::Comm.xjs");
        this.registerScript("Login.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Login.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 로그인 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.Login_onload = function(obj,e)
        {
        	if (application.afn_getOsType("Windwos") != "Windwos") {
        		this.addChild("extComPlugin", new nexacro.ExtComPlugin());
        		this.extComPlugin.addEventHandler("on_callback", this.on_extcomplugin_on_callback, this);
        	}

        	this.fn_getDBVersion();
        }

        this.on_extcomplugin_on_callback = function (obj,e)
        {
        	if (e.svcid == "getAppVersion") {
        		var sDownUrl = this.ds_version.getColumn(0, "filepath");
        		var sNewAppVer = this.ds_version.getColumn(0, "appversion");
        		var sOldAppVer = e.returnvalue.appVersion;

        		sDownUrl = encodeURI(sDownUrl);

        		if (this.gfn_isNull(sNewAppVer)) sNewAppVer = "null";
        		if (this.gfn_isNull(sOldAppVer)) sOldAppVer = "null";

        		if (sOldAppVer != "null") application.gv_appVersion = sOldAppVer;

        		this.stc_appversion.set_text("App version : " + sOldAppVer);

        		if (sNewAppVer > sOldAppVer) {
        			var sMessage = "최신버전으로 업데이트 후 사용가능합니다.\n현재 : " + sOldAppVer + " → 최신 : " + sNewAppVer; // 다국어 처리필요
        			alert(sMessage);

        			var sUrl = application.services["svc"].url + "tms/jsp/tms_mobile/openUpdatePage.jsp?downurl=";
        			this.wb_webContext.set_url(sUrl);

        		} else {
        			var param = {};
        			this.extComPlugin.callMethod("getDeviceId", param);
        		}

        		return;
        	}

        	if (e.svcid == "getDeviceId") {
        		if (e.reason == 0) {
        			if (this.gfn_isNotNull(e.returnvalue)) application.gv_pushId = e.returnvalue.deviceId;
        		}
        	}

        	if (this.gfn_isNull(application.gv_pushId)) application.gv_pushId = "null";

        	application.setPrivateProfile("LOGIN_FLAG", "N");

            if(system.navigatorname == "nexacro"){
        		if(application.getPrivateProfile("LOGIN_FLAG") == "Y"){
        			this.div_login.set_visible(false);
        			
        			if(application.getPrivateProfile("SAVE_YN") == "Y"){
        				this.div_login.chk_idSave.set_value(true);
        				this.div_login.edt_id.set_value(application.getPrivateProfile("SAVE_ID"));
        				if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(application.getPrivateProfile("SAVE_PW"));
        			}
        			
        			this.gfn_setParam("appkey", application.gv_system);
        			this.gfn_setParam("urKey" , application.getPrivateProfile("urKey"));
        			this.gfn_setParam("laKey" , application.getPrivateProfile("laKey"));
        			this.gfn_setParam("ctkey" , application.getPrivateProfile("ctkey"));
        			
        			this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        			this.gfn_setCommon("LAKEY"     , application.getPrivateProfile("laKey"));
        			
        			if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        			else this.gfn_setCommon("APKEY", application.gv_system);
        			
        			application.gds_userInfo.addRow();
        			this.gfn_setUserInfo("urKey"           , application.getPrivateProfile("urKey"));
        			this.gfn_setUserInfo("laKey"           , application.getPrivateProfile("laKey"));
        			this.gfn_setUserInfo("SESSION_USERINFO", application.getPrivateProfile("SESSION_USERINFO"));
        			this.gfn_setUserInfo("ctKey"           , application.getPrivateProfile("ctkey"));
        			this.gfn_setUserInfo("urGrKey"         , application.getPrivateProfile("urGrKey"));
        			this.gfn_setUserInfo("ctKey_desc"      , application.getPrivateProfile("ctKey_desc"));
        			this.gfn_setUserInfo("urName"          , decodeURI(application.getPrivateProfile("urName")));
        			this.gfn_setUserInfo("utcMinute"       , application.getPrivateProfile("utcMinute"));
        			
        			if(this.gfn_isNull(application.getPrivateProfile("lastpwyn"))) this.gfn_setUserInfo("lastpwyn", "");
        			else this.gfn_setUserInfo("lastpwyn", application.getPrivateProfile("lastpwyn"));
        			
        			this.gfn_setUserInfo("utcOffset"       , application.getPrivateProfile("utcOffset"));
        			this.gfn_setUserInfo("utcHour"         , application.getPrivateProfile("utcHour"));
        			this.gfn_setUserInfo("loggrpcd"        , application.getPrivateProfile("loggrpcd"));
        			this.gfn_setUserInfo("urCryptoName"    , application.getPrivateProfile("urCryptoName"));
        			this.gfn_setUserInfo("owkeym"          , application.getPrivateProfile("owkeym"));
        			
        			this.fn_selectCommInfo();
        		}else{
        			this.div_login.set_visible(true);
        			
        			var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_login.getOffsetWidth()/2);
        			var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_login.getOffsetHeight()/2);
        			
        			this.div_login.move(nLeft, nTop);
        			
        			if(application.getPrivateProfile("SAVE_YN") == "Y"){
        				this.div_login.chk_idSave.set_value(true);
        				this.div_login.edt_id.set_value(application.getPrivateProfile("SAVE_ID"));
        				if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(application.getPrivateProfile("SAVE_PW"));

        				this.div_login.edt_pass.setFocus();
        			}
        		}
            }else{
        		if(window.localStorage.getItem("LOGIN_FLAG") == "Y"){
        			this.div_login.set_visible(false);
        			
        			if(window.localStorage.getItem("SAVE_YN") == "Y"){
        				this.div_login.chk_idSave.set_value(true);
        				this.div_login.edt_id.set_value(window.localStorage.getItem("SAVE_ID"));
        				if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(window.localStorage.getItem("SAVE_PW"));
        			}
        			
        			this.gfn_setParam("appkey", application.gv_system);
        			this.gfn_setParam("urKey" , window.localStorage.getItem("urKey"));
        			this.gfn_setParam("laKey" , window.localStorage.getItem("laKey"));
        			this.gfn_setParam("ctkey" , window.localStorage.getItem("ctkey"));
        			
        			this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        			this.gfn_setCommon("LAKEY"     , window.localStorage.getItem("laKey"));
        			
        			if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        			else this.gfn_setCommon("APKEY", application.gv_system);
        			
        			application.gds_userInfo.addRow();
        			this.gfn_setUserInfo("urKey"           , window.localStorage.getItem("urKey"));
        			this.gfn_setUserInfo("laKey"           , window.localStorage.getItem("laKey"));
        			this.gfn_setUserInfo("SESSION_USERINFO", window.localStorage.getItem("SESSION_USERINFO"));
        			this.gfn_setUserInfo("ctKey"           , window.localStorage.getItem("ctkey"));
        			this.gfn_setUserInfo("urGrKey"         , window.localStorage.getItem("urGrKey"));
        			this.gfn_setUserInfo("ctKey_desc"      , window.localStorage.getItem("ctKey_desc"));
        			this.gfn_setUserInfo("urName"          , decodeURI(window.localStorage.getItem("urName")));
        			this.gfn_setUserInfo("utcMinute"       , window.localStorage.getItem("utcMinute"));
        			
        			if(this.gfn_isNull(window.localStorage.getItem("lastpwyn"))) this.gfn_setUserInfo("lastpwyn", "");
        			else this.gfn_setUserInfo("lastpwyn", window.localStorage.getItem("lastpwyn"));
        			
        			this.gfn_setUserInfo("utcOffset"       , window.localStorage.getItem("utcOffset"));
        			this.gfn_setUserInfo("utcHour"         , window.localStorage.getItem("utcHour"));
        			this.gfn_setUserInfo("loggrpcd"        , window.localStorage.getItem("loggrpcd"));
        			this.gfn_setUserInfo("urCryptoName"    , window.localStorage.getItem("urCryptoName"));
        			this.gfn_setUserInfo("owkeym"          , window.localStorage.getItem("owkeym"));
        			
        			this.fn_selectCommInfo();
        		}else{
        			this.div_login.set_visible(true);
        			
        			var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_login.getOffsetWidth()/2);
        			var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_login.getOffsetHeight()/2);
        			
        			this.div_login.move(nLeft, nTop);
        			
        			if(window.localStorage.getItem("SAVE_YN") == "Y"){
        				this.div_login.chk_idSave.set_value(true);
        				this.div_login.edt_id.set_value(window.localStorage.getItem("SAVE_ID"));
        				if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(window.localStorage.getItem("SAVE_PW"));
        				this.div_login.edt_pass.setFocus();
        			}
        		}
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_getDBVersion = function ()
        {
        	this.gfn_setParam("eqtype",  (application.gv_eqType == "iOS")?"40":"30");
        	this.gfn_setParam("appkey",  application.gv_activeApp);

        	this.gfn_setCommon("APKEY", application.gv_activeApp);
        	this.gfn_setCommon("ACTIVE_APP", application.gv_system);
        	this.gfn_setCommon("EQTYPE", (application.gv_eqType == "iOS")?"40":"30");

            var sSvcId   = "selectMobAppDownSetup";
            var sSvcUrl  = "";
            var sInData  = "";
            var sOutData = "ds_version=OUT_DATA_MAP";
            var sParam   = "";

        	if (application.gv_activeApp == "WMS") {
        		this.gfn_setCommon("BEANID"  , "appDownSetupController");
        		this.gfn_setCommon("METHODNM", "selectMobAppDownSetup");
        		sSvcUrl  = application.gv_wms + application.gv_sUrl;
        	} else {
        		this.gfn_setCommon("BEANID"  , "mobileController");
        		this.gfn_setCommon("METHODNM", "selectMobAppDownSetup");
        		sSvcUrl  = application.gv_tms + application.gv_sUrl;
        	}

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_login = function()
        {
        	this.gfn_setParam("eqtype",  application.gv_eqType);
        	this.gfn_setCommon("EQTYPE", application.gv_eqType);
        	this.gfn_setParam("pushid", application.gv_pushId);
        	this.gfn_setCommon("pushid", application.gv_pushId);

        	application.gv_userAuth = "Login";
        	this.gfn_setCommon("BEANID"  , "loginController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectUserInfo";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_userInfo=OUT_USER_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectCommInfo = function()
        {
        	this.gfn_setParam("pushid", application.gv_pushId);
        	this.gfn_setCommon("pushid", application.gv_pushId);

        	application.gv_userAuth = "Login";
            this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectModuleConfig");
        	
            var sSvcId   = "selectCommInfo";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_menu=OUT_MENU_LIST ds_lang=OUT_appMessageTxt gds_msg=OUT_appMessageMsg gds_systemComfig=OUT_SYSTEM_CONFIG gds_admSystemConfig=OUT_ADM_SYSTEM_CONFIG gds_srchCondition=OUT_SRCH_CONDITION gds_srchCondition2=OUT_SRCH_CONDITION2 gds_commCode=OUT_CODE_LIST gds_userGrid=OUT_USER_GRID gds_userMenu=OUT_USER_MENU gds_eqList=OUT_EQ_LIST gds_rtnMenuObjectRole=OUT_rtnMenuObjectRole gds_rtnAppRole=OUT_rtnAppRole gds_rtnOwnerRole=OUT_rtnOwnerRole gds_rtnWHRole=OUT_rtnWHRole";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectSessionInfo = function()
        {
        	this.gfn_setParam("pushid", application.gv_pushId);
        	this.gfn_setCommon("pushid", application.gv_pushId);

        	application.gv_userAuth = "Login";

        	this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "sessionController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectSessionInfo";
            var sSvcUrl  = application.gv_activeApp.toLowerCase() + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_common=OUT_COMMON gds_userInfo=OUT_USER_INFO";
            var sParam   = "";

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
        		if(sSvcId == "selectUserInfo"){
        			this.alert(this.MESSAGE);
        		}/*else{
        			this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		}*/
        //		this.alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		
        		if (sSvcId == "selectMobAppDownSetup") {
        			var sAppVersion = "";

        			if (application.afn_getOsType("Windwos") == "Windwos") {
        				sAppVersion = this.ds_version.getColumn(0, "appversion");
        				this.stc_appversion.set_text("App version : " + sAppVersion);
        				application.gv_appVersion = sAppVersion;

        				var objElement = new Object;
        				this.on_extcomplugin_on_callback(this, objElement);
        			}

        		} else if (sSvcId == "selectCommInfo") {
        			application.gds_menu.filter("melvl < 0");
        			for (var i = (application.gds_menu.getRowCount() - 1); i >= 0; i--) {
        				application.gds_menu.deleteRow(i);
        			}

        			application.gds_menu.filter("");

        			this.div_login.set_visible(true);
        			
        			var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_login.getOffsetWidth()/2);
        			var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_login.getOffsetHeight()/2);
        			
        			this.div_login.move(nLeft, nTop);
        			
        			if(system.navigatorname == "nexacro"){
        				if(application.getPrivateProfile("SAVE_YN") == "Y"){
        					this.div_login.chk_idSave.set_value(true);
        					this.div_login.edt_id.set_value(application.getPrivateProfile("SAVE_ID"));
        					if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(application.getPrivateProfile("SAVE_PW"));
        					this.div_login.edt_pass.setFocus();
        				}
        			}else{
        				if(window.localStorage.getItem("SAVE_YN") == "Y"){
        					this.div_login.chk_idSave.set_value(true);
        					this.div_login.edt_id.set_value(window.localStorage.getItem("SAVE_ID"));
        					if (application.gv_activeApp == "TMS") this.div_login.edt_pass.set_value(window.localStorage.getItem("SAVE_PW"));
        					this.div_login.edt_pass.setFocus();
        				}
        			}
        		}
        		
                return;
            }

        
        	if (sSvcId == "selectMobAppDownSetup") {
        		var sAppVersion = "";

        		if (application.afn_getOsType("Windwos") == "Windwos") {
        			sAppVersion = this.ds_version.getColumn(0, "appversion");
        			this.stc_appversion.set_text("App version : " + sAppVersion);
        			application.gv_appVersion = sAppVersion;

        			var objElement = new Object;
        			this.on_extcomplugin_on_callback(this, objElement);
        		} else {
        			var param = {};
        			this.extComPlugin.callMethod("getAppVersion", param);
        		}

            } else if(sSvcId == "selectUserInfo"){ //process 조회
                if(application.gds_userInfo.rowcount > 0){
        			application.gv_loginStatus = this.gfn_getUserInfo("loginStatus");
        			
        			if(system.navigatorname == "nexacro"){
        				if(this.div_login.chk_idSave.value == true){
        					application.setPrivateProfile("SAVE_YN", "Y");
        					application.setPrivateProfile("SAVE_ID", this.div_login.edt_id.value);
        					if (application.gv_activeApp == "TMS") application.setPrivateProfile("SAVE_PW", this.div_login.edt_pass.value);
        				}else{
        					application.setPrivateProfile("SAVE_YN", "N");
        					application.setPrivateProfile("SAVE_ID", "");
        					if (application.gv_activeApp == "TMS") application.setPrivateProfile("SAVE_PW", "");
        				}
        				
        				application.setPrivateProfile("appkey"          , application.gv_system);
        				application.setPrivateProfile("urKey"           , this.gfn_getUserInfo("urKey"));
        				application.setPrivateProfile("laKey"           , this.gfn_getUserInfo("laKey"));
        				application.setPrivateProfile("ctkey"           , this.gfn_getUserInfo("ctKey"));
        				application.setPrivateProfile("ACTIVE_APP"      , application.gv_activeApp);
        				application.setPrivateProfile("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        				application.setPrivateProfile("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        				application.setPrivateProfile("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        				application.setPrivateProfile("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        				application.setPrivateProfile("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        				application.setPrivateProfile("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        				application.setPrivateProfile("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        				application.setPrivateProfile("utcHour"         , this.gfn_getUserInfo("utcHour"));
        				application.setPrivateProfile("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        				application.setPrivateProfile("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        				application.setPrivateProfile("owkeym"          , this.gfn_getUserInfo("owkeym"));
        				application.setPrivateProfile("LOGIN_FLAG"      , "Y");
        			}else{
        				if(this.div_login.chk_idSave.value == true){
        					window.localStorage.setItem("SAVE_YN", "Y");
        					window.localStorage.setItem("SAVE_ID", this.div_login.edt_id.value);
        					if (application.gv_activeApp == "TMS") window.localStorage.setItem("SAVE_PW", this.div_login.edt_pass.value);
        				}else{
        					window.localStorage.setItem("SAVE_YN", "N");
        					window.localStorage.setItem("SAVE_ID", "");
        					if (application.gv_activeApp == "TMS") window.localStorage.setItem("SAVE_PW", "");
        				}
        				
        				window.localStorage.setItem("appkey"          , application.gv_system);
        				window.localStorage.setItem("urKey"           , this.gfn_getUserInfo("urKey"));
        				window.localStorage.setItem("laKey"           , this.gfn_getUserInfo("laKey"));
        				window.localStorage.setItem("ctkey"           , this.gfn_getUserInfo("ctKey"));
        				window.localStorage.setItem("ACTIVE_APP"      , application.gv_activeApp);
        				window.localStorage.setItem("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        				window.localStorage.setItem("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        				window.localStorage.setItem("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        				window.localStorage.setItem("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        				window.localStorage.setItem("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        				window.localStorage.setItem("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        				window.localStorage.setItem("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        				window.localStorage.setItem("utcHour"         , this.gfn_getUserInfo("utcHour"));
        				window.localStorage.setItem("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        				window.localStorage.setItem("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        				window.localStorage.setItem("owkeym"          , this.gfn_getUserInfo("owkeym"));
        				window.localStorage.setItem("LOGIN_FLAG", "Y");
        			}

                    this.fn_selectCommInfo();
                }
            } else if(sSvcId == "selectCommInfo"){

        		application.gds_menu.filter("melvl < 0");
        		for (var i = (application.gds_menu.getRowCount() - 1); i >= 0; i--) {
        			application.gds_menu.deleteRow(i);
        		}

        		application.gds_menu.filter("");

                this.div_login.edt_pass.set_value("");
                application.gv_CF_HIDDEN.form.ds_lang.clear();
                application.gv_CF_HIDDEN.form.ds_lang.copyData(this.ds_lang);
                application.gds_lang.clearData();
                
                for(var i = 0 ; i < this.ds_lang.rowcount ; i++){
        			var sColNmOrg = this.ds_lang.getColumn(i, "mulaapmsg_hdkey");
        			var sColNmNew = String(sColNmOrg).replace(/ /gi, "_");
        			//if (i > 20) trace("sColNmOrg : " + sColNmOrg + ", sColNmNew : " + sColNmNew);
        			application.gds_lang.addColumn(sColNmOrg);
        			
        			if(i == 0){
        				application.gds_lang.addRow();
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}else{
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}
                }
        		
        		for(var i = 0 ; i < application.gds_gridMenu.rowcount ; i++){
        			if(this.gfn_isNotNull(application.gds_gridMenu.getColumn(i, "multiLang"))) application.gds_gridMenu.setColumn(i, "menuNm", application.gds_lang.getColumn(0, application.gds_gridMenu.getColumn(i, "multiLang")));
        		}
        		
        		if(application.gv_activeApp == "AMS"){
        			application.gv_userId = this.gfn_getUserInfo("urKey");
        			application.gv_userNm = this.gfn_getUserInfo("urName");
        			
        			this.fn_windowResize();
        		}else{
        			this.fn_selectSessionInfo();
                }
            }else if(sSvcId == "selectSessionInfo"){
        		application.gv_userId = this.gfn_getUserInfo("urKey");
        		application.gv_userNm = this.gfn_getUserInfo("urName");
        		
        		if(system.navigatorname == "nexacro"){
        			application.setPrivateProfile("appkey"          , application.gv_system);
        			application.setPrivateProfile("urKey"           , this.gfn_getUserInfo("urKey"));
        			application.setPrivateProfile("laKey"           , this.gfn_getUserInfo("laKey"));
        			application.setPrivateProfile("ctkey"           , this.gfn_getUserInfo("ctKey"));
        			application.setPrivateProfile("ACTIVE_APP"      , application.gv_activeApp);
        			application.setPrivateProfile("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        			application.setPrivateProfile("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        			application.setPrivateProfile("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        			application.setPrivateProfile("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        			application.setPrivateProfile("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        			application.setPrivateProfile("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        			application.setPrivateProfile("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        			application.setPrivateProfile("utcHour"         , this.gfn_getUserInfo("utcHour"));
        			application.setPrivateProfile("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        			application.setPrivateProfile("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        			application.setPrivateProfile("owkeym"          , this.gfn_getUserInfo("owkeym"));
        		}else{
        			window.localStorage.setItem("appkey"          , application.gv_system);
        			window.localStorage.setItem("urKey"           , this.gfn_getUserInfo("urKey"));
        			window.localStorage.setItem("laKey"           , this.gfn_getUserInfo("laKey"));
        			window.localStorage.setItem("ctkey"           , this.gfn_getUserInfo("ctKey"));
        			window.localStorage.setItem("ACTIVE_APP"      , application.gv_activeApp);
        			window.localStorage.setItem("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        			window.localStorage.setItem("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        			window.localStorage.setItem("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        			window.localStorage.setItem("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        			window.localStorage.setItem("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        			window.localStorage.setItem("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        			window.localStorage.setItem("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        			window.localStorage.setItem("utcHour"         , this.gfn_getUserInfo("utcHour"));
        			window.localStorage.setItem("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        			window.localStorage.setItem("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        			window.localStorage.setItem("owkeym"          , this.gfn_getUserInfo("owkeym"));
        		}

        		var objParam = new Object();
        		objParam.uskey = "MAIN000000";
        		objParam.nxpath = "";
        		objParam.mename = "";

        		this.gfn_goPage(objParam);
            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* 로그인 Button 실행 */
        this.div_login_btn_login_onclick = function(obj,e)
        {
            if(this.gfn_isNull(this.div_login.edt_id.value)){
        		this.alert("ID 를 입력하세요.");
        		this.div_login.edt_id.setFocus();
            }else if(this.gfn_isNull(this.div_login.edt_pass.value)){
        		this.alert("PASSWORD 를 입력하세요.");
        		this.div_login.edt_pass.setFocus();
            }else{
        		this.gfn_setParam("appkey", application.gv_system);
        		this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        		
        		if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        		else this.gfn_setCommon("APKEY", application.gv_system);
        		
        		this.gfn_setParam("urKey", this.div_login.edt_id.value);
        		this.gfn_setParam("urPw" , this.div_login.edt_pass.value);
        		this.gfn_setParam("laKey", this.div_login.cbo_lang.value);
        		this.gfn_setCommon("LAKEY", this.div_login.cbo_lang.value);

        		// Param
        		this.gfn_addTranColumn(application.gds_param, "apkey");
        		application.gds_param.setColumn(0, "apkey", String(application.gv_activeApp).toLowerCase());

        		application.gds_param.setColumn(0, "eqtype", application.gv_eqType);

        		this.gfn_addTranColumn(application.gds_param, "owkey");
        		application.gds_param.setColumn(0, "owkey", application.gds_userInfo.getColumn(0, "owkeym"));

        		this.gfn_addTranColumn(application.gds_param, "pushid");
        		application.gds_param.setColumn(0, "pushid", application.gv_pushId);

        		this.gfn_addTranColumn(application.gds_param, "pagingLimit");
        		application.gds_param.setColumn(0, "pagingLimit", "");

        
        		this.gfn_addTranColumn(application.gds_param, "ENABLEMAXBETWEENDAYS_MOBILE");
        		application.gds_param.setColumn(0, "ENABLEMAXBETWEENDAYS_MOBILE", "365");

        		// Common
        		this.gfn_addTranColumn(application.gds_common, "URKEY");
        		application.gds_common.setColumn(0, "URKEY", application.gds_userInfo.getColumn(0, "urKey"));

        		this.gfn_addTranColumn(application.gds_common, "ctkey");
        		application.gds_common.setColumn(0, "ctkey", application.gds_userInfo.getColumn(0, "ctKey"));

        		application.gds_common.setColumn(0, "EQTYPE", application.gv_eqType);

        		this.gfn_addTranColumn(application.gds_common, "pushid");
        		application.gds_common.setColumn(0, "pushid", application.gv_pushId);

        		this.gfn_addTranColumn(application.gds_common, "uskey");
        		application.gds_common.setColumn(0, "uskey", application.gds_userInfo.getColumn(0, "urKey"));

        		this.fn_login();
            }
        }

        /* div_login_edt_pass_onkeydown 실행 */
        this.div_login_edt_pass_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.div_login.btn_login.click();
        }

        this.frame_login_onsize = function(obj,e)
        {
        	var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_login.getOffsetWidth()/2);
        	var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_login.getOffsetHeight()/2);
        	
        	this.div_login.move(nLeft, nTop);
        }

        this.frame_login_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        this.frame_login_ondevicebuttonup = function(obj,e)
        {
        	// Mobile Button -> e.button (0: HOME, 1 : MENU, 2: CANCEL)
        	if (e.button == 2) application.exit();
        }

        this.wb_webContext_onloadcompleted = function(obj,e)
        {
        	application.exit();
        }

        this.Button00_onclick = function(obj,e)
        {
        	this.fn_getDBVersion();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.Login_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.addEventHandler("onkeydown", this.frame_login_onkeydown, this);
            this.addEventHandler("ondevicebuttonup", this.frame_login_ondevicebuttonup, this);
            this.div_login.edt_id.addEventHandler("onkeyup", this.div_login_edt_id_onkeyup, this);
            this.div_login.edt_pass.addEventHandler("onkeydown", this.div_login_edt_pass_onkeydown, this);
            this.div_login.btn_login.addEventHandler("onclick", this.div_login_btn_login_onclick, this);
            this.div_login.chk_idSave.addEventHandler("onclick", this.div_login_chk_idSave_onclick, this);
            this.ImageViewer01.addEventHandler("onclick", this.ImageViewer01_onclick, this);
            this.wb_webContext.addEventHandler("onloadcompleted", this.wb_webContext_onloadcompleted, this);
            this.Button00.addEventHandler("onclick", this.Button00_onclick, this);

        };

        this.loadIncludeScript("Login.xfdl", true);

       
    };
}
)();
