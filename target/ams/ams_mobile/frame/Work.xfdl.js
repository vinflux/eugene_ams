﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("frame_work");
                this.set_classname("frame_work");
                this._setFormPosition(0,0,640,1097);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_lang", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_tmsLoad", this);
            obj._setContents("<ColumnInfo><Column id=\"ldlegid\" type=\"STRING\" size=\"256\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/><Column id=\"drvno\" type=\"STRING\" size=\"256\"/><Column id=\"routno\" type=\"STRING\" size=\"256\"/><Column id=\"equipid\" type=\"STRING\" size=\"256\"/><Column id=\"interval\" type=\"STRING\" size=\"256\"/><Column id=\"truck_location_yn\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("stc_bg", "absolute", "0", "0", null, "100", "0", null, this);
            obj.set_taborder("0");
            obj.style.set_background("#2e333fff");
            this.addChild(obj.name, obj);

            obj = new Static("stc_RVLine", "absolute", null, "0", "1", "99", "93", null, this);
            obj.set_taborder("1");
            obj.style.set_background("#ffffff30");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "93", "0", "1", "99", null, null, this);
            obj.set_taborder("2");
            obj.style.set_background("#ffffff30");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "-5", "99", null, "1", "0", null, this);
            obj.set_taborder("3");
            obj.style.set_background("#ffffff40");
            this.addChild(obj.name, obj);

            obj = new Div("div_work", "absolute", "0", "100", null, null, "0", "8", this);
            obj.set_taborder("4");
            this.addChild(obj.name, obj);

            obj = new Static("stc_title", "absolute", "93", "0", null, "100", "93", null, this);
            obj.set_taborder("5");
            obj.set_text("EUGENES Mobile");
            obj.set_cssclass("sta_Top_title");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_condition", "absolute", null, "0", "92", "100", "0", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_Top_search");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_menu", "absolute", "0", "0", "92", "100", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_Top_menu");
            this.addChild(obj.name, obj);

            obj = new Button("btn_prevScreen", "absolute", "94", "0", "62", "100", null, null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_Top_back");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_dataView", "absolute", "85", "125", "167", "44", null, null, this);
            obj.set_taborder("9");
            obj.set_text("DataView");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_lang", "absolute", "256", "125", "155", "44", null, null, this);
            obj.set_taborder("10");
            obj.set_text("Language");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new PopupDiv("pdiv_langView", "absolute", "1996", "108", "640", "989", null, null, this);
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);

            obj = new Div("div_leftMenu", "absolute", "280", "1128", "640", "989", null, null, this);
            obj.set_taborder("11");
            obj.set_text("Div00");
            obj.style.set_background("white");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Div("div_tools", "absolute", "928", "1128", "640", "989", null, null, this);
            obj.set_taborder("12");
            obj.set_text("Div00");
            obj.style.set_background("white");
            obj.set_visible("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 1097, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_work");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Work.xfdl", "lib::Comm.xjs");
        this.registerScript("Work.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/
         
        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        
        // 알리미 실행관련 타이머 설정
        /*****************************************/
        /* 주의 - this.fv_interval 값은 최소한 10000 이상이어야 합니다. 시간을 줄이면 익스플로러가 오류가 발생할 수 있습니다. */
        /*****************************************/
        this.fv_interval 			= 1000 * 120; // 알림기능 타이머, 타이머 실행 주기 - 1/1000 초(1000 = 1초)

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.frame_work_onload = function(obj,e)
        {
        	this.btn_dataView.move(10, 0);
        	this.btn_lang.move(180, 0);

        	var objFrame = obj.getOwnerFrame();

        	if (this.gfn_isNull(objFrame.arguments)) {
        		trace("arguments error...");
        		return;
        	}

        	var objArgs = objFrame.arguments;

        	if (this.gfn_isNull(objArgs.nxpath)) {
        		trace("nxpath(" + objArgs.nxpath + ") is required...");
        		return;
        	}

        	if (!this.gfn_isNull(objArgs.nxpath) && objArgs.nxpath != "main::MainMenu.xfdl") {
        		this.btn_prevScreen.set_visible(true);
        		this.stc_title.set_left(156);
        		this.stc_title.style.set_align("left middle");
        	} else {
        		this.btn_prevScreen.set_visible(false);
        		this.stc_title.set_left(104);
        	}

        	var sMename = objArgs.mename;
        	if (!this.gfn_isNull(sMename)) this.stc_title.set_text(sMename);
        	this.stc_title.style.set_align("center middle");

        	if (application.gv_activeApp == "TMS") {
        		this.fn_setCondVisiboe(false);
        	} else if (objArgs.nxpath == "main::MainMenu.xfdl") {
        		this.fn_setCondVisiboe(false);
        	}

        	var objFunction = this.div_work.gfn_popupSearchCond;
        	var objCallbackFunction = this.div_work.fn_popupAfter;

        	this.div_work.set_url(objArgs.nxpath);

        	if (this.gfn_isNull(this.div_leftMenu.url)) {
        		this.div_leftMenu.set_url("main::LeftMenu.xfdl");
        	}

        // 	if (this.gfn_isNull(this.pdiv_menu.url)) {
        // 		this.pdiv_menu.set_url("main::LeftMenu.xfdl");
        // 	}

        	if (application.gv_activeApp == "TMS") 
        	{
        		
        		//로그인시 현재 대기중인 배차번호, 출발지의 위경도 구한다.
        		//현재 로그인한 아이디로 기사번호를 찾고 기사번호로 현재 배차확정상태의 load를 찾는다.
        		this.gfn_setCommon("BEANID",   "mobileController");
        		this.gfn_setCommon("METHODNM", "getLatLon");
        		
        		var sSvcId   = "getLatLon";
        		var sSvcUrl  = application.gv_tms + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "ds_tmsLoad=OUT_rtnList";
        		var sParam   = "urKey="+application.gds_userInfo.getColumn(0, "urKey");
        	 
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_setCondVisiboe = function (bFlag)
        {
        	var nRight = 0;

        	if (bFlag) nRight = 93;

        	this.stc_RVLine.set_visible(bFlag);
        	this.btn_condition.set_visible(bFlag);
        //	this.stc_title.setOffsetRight(nRight);
        }

        
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "selectUserInfo") {

            }
            
            if (sSvcId == "getLatLon") {
        		if(this.ds_tmsLoad.rowcount > 0)
        		{
        			//구한값을 전역변수로 가지고 있음
        			application.gds_tmsLoad.clearData();
         			application.gds_tmsLoad.insertRow(0);
        			application.gds_tmsLoad.copyRow(0, this.ds_tmsLoad, 0);
        			application.gds_tmsLoad.setColumn(0, "auto_load_start", 'Y');
        			this.fv_interval = nexacro.toNumber(this.ds_tmsLoad.getColumn(0, "interval"));
        		}
        		//타이머 시작
        		//1분 이내면 타이머 작동 안함
        		if (this.fv_interval < 60000 )
        		{
        			return;
        		}else
        		{
        			this.setTimer(9, this.fv_interval );
        		}
            }
            if (sSvcId == "autoLoadStart") {
        		//자동출차되면 timer 초기화
        		application.gds_tmsLoad.setColumn(0, "auto_load_start", 'N');
            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {

        }

        this.fn_addDs = function (sDsId,objDs)
        {
        	if (!this.gfn_isNull(this.objects[sDsId])) this.removeChild(sDsId);
        	var objRtn = new Dataset();
        	objRtn.name = sDsId;
        	this.addChild(sDsId, objRtn);
        	objRtn.copyData(objDs);
        	return objRtn;
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        this.btn_menu_onclick = function(obj,e)
        {
        	var nWidth = this.getOffsetWidth();
        	var nHeight = this.getOffsetHeight();
        	this.div_leftMenu.move(0, 0, nWidth, nHeight);
        	var objFunction = this.div_work.fn_setWebControlVisible;
        	if (!this.gfn_isNull(objFunction)) objFunction.call(this.div_work, false);
        	this.div_leftMenu.set_visible(true);

        	//this.pdiv_menu.trackPopup(0, 0, nWidth, nHeight);
        }

        
        this.fn_closePopupMenu = function ()
        {
        	if (this.div_leftMenu.visible) {
        		var objFunction = this.div_work.fn_setWebControlVisible;
        		if (!this.gfn_isNull(objFunction)) objFunction.call(this.div_work, true);
        		this.div_leftMenu.set_visible(false);
        	}
        	//if (this.pdiv_menu.isPopup()) this.pdiv_menu.closePopup();
        }

        this.btn_prevScreen_onclick = function(obj,e)
        {
        	if (application.gds_openMenu.getRowCount() <= 1) {
        		application.gds_openMenu.deleteRow(0);

        		var objParam = new Object();
        		objParam.uskey = "MAIN000000";
        		objParam.nxpath = "";
        		objParam.mename = "";

        		this.gfn_goPage(objParam);

        		return;
        	}

        	var sWinId = application.gv_CF_MAIN.arguments.WINID;
        	var sWinSeq = application.gv_CF_MAIN.arguments.winseq;
        	var sFindExpr = "WINID == '" + sWinId + "' && winseq == " + sWinSeq;
        	var nCurRow = application.gds_openMenu.findRowExpr(sFindExpr);

        	if (!this.gfn_isNull(application.gv_CF_HIDDEN)) {
        		if (!this.gfn_isNull(application.gv_CF_HIDDEN.form)) {
        			application.gv_CF_HIDDEN.form.fn_clearArgs(sWinId + "_" + sWinSeq);
        		}
        	}

        	var nRtn = application.gds_openMenu.deleteRow(nCurRow);

        	nCurRow--;

        	var sUrlOpenYn = application.gds_openMenu.getColumn(nCurRow, "urlopen");

        	var sUsKey = application.gds_openMenu.getColumn(nCurRow, "uskey");
        	var sMeName = application.gds_openMenu.getColumn(nCurRow, "mename");
        	var sNxPath = application.gds_openMenu.getColumn(nCurRow, "nxpath");

        	if (sUrlOpenYn == "Y") {
        		sUsKey = "";
        		if (nCurRow >= 0) sMeName = application.gds_openMenu.getColumn(nCurRow, "mename");
        		if (nCurRow >= 0) sNxPath = application.gds_openMenu.getColumn(nCurRow, "nxpath");
        		if (this.gfn_isNull(sNxPath)) return;
        	} else {
        		if (nCurRow >= 0) sUsKey = application.gds_openMenu.getColumn(nCurRow, "uskey");
        		sMeName = "";
        		sNxPath = "";
        	}

        	sWinId = application.gds_openMenu.getColumn(nCurRow, "WINID");
        	sWinSeq = application.gds_openMenu.getColumn(nCurRow, "winseq");

        	var objCF = this.getOwnerFrame();

        	objCF.arguments.WINID = sWinId;
        	objCF.arguments.winseq = sWinSeq;
        	objCF.arguments.uskey = application.gds_openMenu.getColumn(nCurRow, "uskey");

        	if (nCurRow >= 0) application.gds_openMenu.deleteRow(nCurRow);
         //trace("nCurRow 2 : " + nCurRow + ", nRtn : " + nRtn);
        // trace("sWinId : " + sWinId + ", sUsKey : " + sUsKey + ", sWinSeq : " + sWinSeq + ", sNxPath : " + sNxPath + ", sMeName : " + sMeName);

        	application.gds_param.setColumn(0, "value", "");

        	var objParam = new Object();
        	objParam.WINID = sWinId;
        	objParam.winseq = sWinSeq;
        	objParam.uskey = sUsKey;
        	objParam.nxpath = sNxPath;
        	objParam.mename = sMeName;

        	this.gfn_goPage(objParam, true);
        }

        this.btn_condition_onclick = function(obj,e)
        {
        	if (this.div_work.url == "main::MainMenu.xfdl") return;

        	var objFunction = this.div_work.gfn_popupSearchCond;
        	var objCallbackFunction = this.div_work.fn_popupAfter;

        	if (!this.gfn_isNull(objFunction) && !this.gfn_isNull(objCallbackFunction)) {
        		var sPopupId = "MobInbOrderTaskPop";
        		objFunction.call(this.div_work, sPopupId, this.div_work.ds_compList, this.div_work.srchPopAtLstSel);
        	}
        }

        this.btn_dataView_onclick = function(obj,e)
        {
        	if (e.shiftKey) {
        		this.div_tools.set_url("comm::ViewDataset.xfdl");

        		var nWidth = this.getOffsetWidth();
        		var nStcHeight = this.stc_bg.getOffsetHeight();
        		var nHeight = this.getOffsetHeight() - nStcHeight;

        		this.div_tools.move(0, nStcHeight, nWidth, nHeight);
        		this.div_tools.set_visible(true);

        		if (!this.gfn_isNull(this.div_tools.fn_createFormList)) this.div_tools.fn_createFormList();
        	}
        }

        this.frame_work_onkeydown = function(obj,e)
        {
        	if (e.shiftKey) {
        		this.btn_dataView.set_visible(true);
        		this.btn_lang.set_visible(true);
        	}
        	
        	//form 포커스 get
        	var comp = this.getFocus();
        	var compId = comp.id;
        	
        	var childlist = this.div_work.components;
        	//포커스 위치가 edt가 아니고 div에 edt_searchText가 있는경우 edt_searchText로 포커스 이동
        	if (this.div_work.url != "main::MainMenu.xfdl"){
        		if(compId.substr(0,3) != "edt"){
        				if(childlist["edt_searchText"] != "undefined"){
        					this.div_work.edt_searchText.setFocus();
        				}
        		}
        	}
        }

        this.frame_work_onkeyup = function(obj,e)
        {
        	this.btn_dataView.set_visible(false);
        	this.btn_lang.set_visible(false);
        }

        this.btn_lang_onclick = function(obj,e)
        {
        	if (e.shiftKey) {
        		this.div_tools.set_url("comm::ViewLanguage.xfdl");

        		var nWidth = this.getOffsetWidth();
        		var nStcHeight = this.stc_bg.getOffsetHeight();
        		var nHeight = this.getOffsetHeight() - nStcHeight;

        		this.div_tools.move(0, nStcHeight, nWidth, nHeight);

        		this.div_tools.set_visible(true);
        		if (!this.gfn_isNull(this.div_tools.fn_createFormList)) this.div_tools.fn_createFormList();
        	}
        }

        this.frame_work_ondevicebuttonup = function(obj,e)
        {
        	if (e.button == 2 && this.btn_prevScreen.visible) {
        		if (this.div_leftMenu.visible) {
        			var objFunction = this.div_work.fn_setWebControlVisible;
        			if (!this.gfn_isNull(objFunction)) objFunction.call(this.div_work, true);
        			this.div_leftMenu.set_visible(false);
        		} else {
        			this.btn_prevScreen.click();
        		}
        	} else if (e.button == 2) {
        		var arrText = this.gfn_getMsgToArray("MSG_ALERT_LOGOUT");

        		if (this.div_leftMenu.visible) {
        			var objFunction = this.div_work.fn_setWebControlVisible;
        			if (!this.gfn_isNull(objFunction)) objFunction.call(this.div_work, true);
        			this.div_leftMenu.set_visible(false);
        		} else if (application.confirm(arrText[1])) {
        			application.exit();
        		}
        	}
        }

        this.frame_work_ontimer = function(obj,e)
        {
        	if (application.gv_activeApp == "TMS") 
        	{
        		//출차가 안된 경우에 위경도 가져와서 첫번째 센터(출발지)랑 비교
        		if(application.gds_tmsLoad.getColumn(0, "ldlegid") > '0' || application.gds_tmsLoad.getColumn(0, "truck_location_yn") == 'Y')
        		{
        			//윈도우 버전은 gps 컨트롤 안됨
        			if (application.afn_getOsType("Windwos") == "Android") {
        				if(!this.extComPlugin){
        					this.addChild("extComPlugin", new nexacro.ExtComPlugin());
        				}
        				
        				this.extComPlugin.addEventHandler("on_callback", this.on_extcomplugin_on_callback, this);
        				var keyList = ["SESSION_USERINFO", "type"];
        				var param = {
        					SESSION_USERINFO:application.gds_userInfo.getColumn(0, "SESSION_USERINFO")
        					,type:"gps"
        				};
        				this.extComPlugin.callMethod("getLocation",param);
        			}
        			else {
        				return;
        			}
        		}
        	}
        }

        
        this.on_extcomplugin_on_callback = function (obj,e)
        {
        	
        	var lat;
        	var lon;
        	var st_lat = nexacro.toNumber(application.gds_tmsLoad.getColumn(0, "lat"));
        	var st_lon = nexacro.toNumber(application.gds_tmsLoad.getColumn(0, "lon"));
        	var dist = 0;
        	
        	if(e.returnvalue.Status == "FAIL" ){
        		return;
        	}else
        	{
        	//상태가 성공이 아니면 return
        	//Status == 'FAIL' 이면 리턴
        		
        		lat = nexacro.toNumber(nexacro.round(e.returnvalue.Latitude, 4));
        		lon = nexacro.toNumber(nexacro.round(e.returnvalue.Longitude, 4));
        		
        		//출발지의 위경도와 비교해서 500M 이상이면 출차처리한다.
        		//직선거리구하기 (지구 둘레길이 약 40030km)
        		dist = Math.sqrt((lat-st_lat)*(lat-st_lat) + (lon-st_lon)*(lon-st_lon))*40030*1000/360; 
        	
        		if(dist > 500)
        		{
        			//출차 실행
        			if(application.gds_tmsLoad.getColumn(0, "ldlegid") > '0')
        			{
        				if(application.gds_tmsLoad.getColumn(0, "auto_load_start") == 'Y')
        				{
        					this.gfn_setCommon("BEANID",   "tntLdController");
        					this.gfn_setCommon("METHODNM", "autoLoadStart");
        					
        					var sSvcId   = "autoLoadStart";
        					var sSvcUrl  = application.gv_tms + application.gv_sUrl;
        					var sInData  = "";
        					var sOutData = "";
        					var sParam   = "ldlegid="+application.gds_tmsLoad.getColumn(0, "ldlegid");
        				 
        					this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        				}
        			}
        			//현재위치를 저장한다.
        			this.fn_set_lat_lon(lat, lon);
        		}else if (dist > 50)
        		{
        			//현재위치를 저장한다.
        			//센터 50미터 이상이면 현재 위치를 저장시켜준다.
        			this.fn_set_lat_lon(lat, lon);
        		}
            }
        }

        this.fn_set_lat_lon = function(aLat,aLon)
        {
        	this.ds_tmsLoad.clearData();
        	var iRow = this.ds_tmsLoad.insertRow(0);
        	this.ds_tmsLoad.setColumn(iRow, "ldlegid", application.gds_tmsLoad.getColumn(0, "ldlegid"));
        	this.ds_tmsLoad.setColumn(iRow, "drvno", application.gds_tmsLoad.getColumn(0, "drvno"));
        	this.ds_tmsLoad.setColumn(iRow, "routno", application.gds_tmsLoad.getColumn(0, "routno"));
        	this.ds_tmsLoad.setColumn(iRow, "equipid", application.gds_tmsLoad.getColumn(0, "equipid"));
        	this.ds_tmsLoad.setColumn(iRow, "lat", aLat);
        	this.ds_tmsLoad.setColumn(iRow, "lon", aLon);
        	
        	this.gfn_setCommon("BEANID",   "mobileController");
        	this.gfn_setCommon("METHODNM", "saveLatLon");
        	var sSvcId   = "saveLatLon";
        	var sSvcUrl  = application.gv_tms + application.gv_sUrl;
        	var sInData  = "IN_saveLatLon=ds_tmsLoad";
        	var sOutData = "";
        	var sParam   = "";
         
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.frame_work_onload, this);
            this.addEventHandler("onkeydown", this.frame_work_onkeydown, this);
            this.addEventHandler("onkeyup", this.frame_work_onkeyup, this);
            this.addEventHandler("ondevicebuttonup", this.frame_work_ondevicebuttonup, this);
            this.addEventHandler("ontimer", this.frame_work_ontimer, this);
            this.btn_condition.addEventHandler("onclick", this.btn_condition_onclick, this);
            this.btn_menu.addEventHandler("onclick", this.btn_menu_onclick, this);
            this.btn_prevScreen.addEventHandler("onclick", this.btn_prevScreen_onclick, this);
            this.btn_dataView.addEventHandler("onclick", this.btn_dataView_onclick, this);
            this.btn_lang.addEventHandler("onclick", this.btn_lang_onclick, this);

        };

        this.loadIncludeScript("Work.xfdl", true);

       
    };
}
)();
