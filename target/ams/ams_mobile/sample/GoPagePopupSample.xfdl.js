﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MainMenu");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,989);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/><Column id=\"Column1\" type=\"STRING\" size=\"256\"/><Column id=\"Column2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"Column0\">1</Col><Col id=\"Column1\">A</Col><Col id=\"Column2\">가</Col></Row><Row><Col id=\"Column0\">2</Col><Col id=\"Column1\">B</Col><Col id=\"Column2\">나</Col></Row><Row><Col id=\"Column0\">3</Col><Col id=\"Column1\">C</Col><Col id=\"Column2\">다</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_listRtn", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", null, "51", "0", null, this);
            obj.set_taborder("0");
            obj.set_text("Form Master");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 25 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_goForm", "absolute", "9", "230", "180", "53", null, null, this);
            obj.set_taborder("1");
            obj.set_text("Go Detail Form");
            obj.style.set_font("bold 18 Dotum");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00", "absolute", "300", "60", null, "161", "9", null, this);
            obj.set_taborder("2");
            obj.set_binddataset("ds_list");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/></Columns><Rows><Row size=\"36\" band=\"head\"/><Row size=\"30\"/></Rows><Band id=\"head\"><Cell style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column0\"/><Cell col=\"1\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column1\"/><Cell col=\"2\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column2\"/></Band><Band id=\"body\"><Cell edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column0\"/><Cell col=\"1\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column1\"/><Cell col=\"2\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column2\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("btn_openPopup", "absolute", "193", "230", "172", "53", null, null, this);
            obj.set_taborder("3");
            obj.set_text("Open Popup");
            obj.style.set_font("bold 18 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg2", "absolute", "9", "100", "84", "37", null, null, this);
            obj.set_taborder("6");
            obj.set_text("arg2");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg1", "absolute", "9", "60", "84", "37", null, null, this);
            obj.set_taborder("7");
            obj.set_text("arg1");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg1", "absolute", "95", "60", "197", "37", null, null, this);
            obj.set_taborder("8");
            obj.style.set_font("16 Dotum");
            obj.set_value("From Master");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg2", "absolute", "95", "100", "197", "37", null, null, this);
            obj.set_taborder("9");
            obj.style.set_font("16 Dotum");
            obj.set_value("100");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "23", "97", "169", "3", null, null, this);
            obj.set_taborder("12");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "93", "57", "2", "83", null, null, this);
            obj.set_taborder("13");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "50", "9", "105", null, null, this);
            obj.set_taborder("14");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "631", "58", "9", "105", null, null, this);
            obj.set_taborder("16");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "189", "246", "4", "25", null, null, this);
            obj.set_taborder("17");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "194", "9", "105", null, null, this);
            obj.set_taborder("18");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "14", "51", "553", "9", null, null, this);
            obj.set_taborder("19");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "292", "66", "9", "57", null, null, this);
            obj.set_taborder("20");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "221", "553", "9", null, null, this);
            obj.set_taborder("21");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "591", "0", "9", "50", null, null, this);
            obj.set_taborder("22");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Radio("rdo_modeless", "absolute", "369", "230", "262", "53", null, null, this);
            this.addChild(obj.name, obj);
            var rdo_modeless_innerdataset = new Dataset("rdo_modeless_innerdataset", this.rdo_modeless);
            rdo_modeless_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">N</Col><Col id=\"datacolumn\">Modal</Col></Row><Row><Col id=\"codecolumn\">Y</Col><Col id=\"datacolumn\">Modeless</Col></Row></Rows>");
            obj.set_innerdataset(rdo_modeless_innerdataset);
            obj.set_taborder("23");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.style.set_buttonsize("20");
            obj.style.set_font("bold 18 Dotum");
            obj.set_direction("vertical");
            obj.set_value("N");
            obj.set_index("0");

            obj = new Static("Static11", "absolute", "365", "246", "4", "25", null, null, this);
            obj.set_taborder("24");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "300", "332", null, "161", "9", null, this);
            obj.set_taborder("25");
            obj.set_binddataset("ds_listRtn");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/></Columns><Rows><Row size=\"36\" band=\"head\"/><Row size=\"30\"/></Rows><Band id=\"head\"><Cell style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column0\"/><Cell col=\"1\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column1\"/><Cell col=\"2\" style=\"font:bold 15 Dotum;selectfont:bold 15 Dotum;\" text=\"Column2\"/></Band><Band id=\"body\"><Cell edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column0\"/><Cell col=\"1\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column1\"/><Cell col=\"2\" edittype=\"normal\" style=\"font:13 Dotum;selectfont:13 Dotum;\" text=\"bind:Column2\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg00", "absolute", "9", "332", "84", "37", null, null, this);
            obj.set_taborder("26");
            obj.set_text("arg1");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("stc_arg03", "absolute", "9", "372", "84", "37", null, null, this);
            obj.set_taborder("27");
            obj.set_text("arg2");
            obj.set_cssclass("sta_GA_label");
            obj.style.set_font("bold 16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg1Rtn", "absolute", "95", "332", "197", "37", null, null, this);
            obj.set_taborder("28");
            obj.style.set_font("16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_arg2Rtn", "absolute", "95", "372", "197", "37", null, null, this);
            obj.set_taborder("29");
            obj.style.set_font("16 Dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "6", "300", "285", "29", null, null, this);
            obj.set_taborder("30");
            obj.set_text("Return Value");
            obj.style.set_font("bold 18 Dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 989, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("GoPagePopupSample.xfdl", "lib::Comm.xjs");
        this.registerScript("GoPagePopupSample.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.btn_goForm_onclick = function(obj,e)
        {
        	this.edt_arg1Rtn.set_value("");
        	this.edt_arg2Rtn.set_value("");
        	this.ds_listRtn.clearData();

        	var objParam = new Object();
        	objParam.nxpath = "sample::FormDetailSample.xfdl";
        	objParam.mename = "Detail Form";
        	objParam.args = new Object();
        	objParam.args.dsList = this.ds_list;
        	objParam.args.arg1 = this.edt_arg1.value;
        	objParam.args.arg2 = this.edt_arg2.value;

        	this.gfn_goPage(objParam);
        }

        this.btn_openPopup_onclick = function(obj,e)
        {
        	this.edt_arg1Rtn.set_value("");
        	this.edt_arg2Rtn.set_value("");
        	this.ds_listRtn.clearData();

        	var sPopupId = "DetailFormSamplePop";
        	var sUrl = "sample::FormDetailSamplePop.xfdl";
        	var oArg = new Object();
        	var nWidth = 600;
        	var nHeight = 450;
        	var sPopupCallback = "";
        	var bModeless = false;

        	if (this.rdo_modeless.value == "Y") bModeless = true;

        	oArg.dsList = this.ds_list;
        	oArg.arg1 = this.edt_arg1.value;
        	oArg.arg2 = this.edt_arg2.value;
        	
        	this.gfn_popup(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless);
        }

        this.fn_popupAfter = function (sPopupId,objRtn)
        {
        	if (sPopupId == "DetailFormSamplePop") {
        		this.ds_listRtn.clear();

        		if (!this.gfn_isNull(objRtn)) {
        			this.edt_arg1Rtn.set_value(objRtn.arg1);
        			this.edt_arg2Rtn.set_value(objRtn.arg2);
        			this.ds_listRtn.copyData(objRtn.dsList);
        		}
        	}
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.btn_goForm.addEventHandler("onclick", this.btn_goForm_onclick, this);
            this.btn_openPopup.addEventHandler("onclick", this.btn_openPopup_onclick, this);

        };

        this.loadIncludeScript("GoPagePopupSample.xfdl", true);

       
    };
}
)();
