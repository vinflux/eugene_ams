﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Camera");
                this.set_classname("Camera");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1024,768);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Camera("Camera00", this);
            obj.set_gettype("imagedata");
            this.addChild(obj.name, obj);

            
            // UI Components Initialize
            obj = new Button("Button00", "absolute", "3.13%", "32", null, "50", "85.16%", null, this);
            obj.set_taborder("0");
            obj.set_text("Camera");
            this.addChild(obj.name, obj);

            obj = new TextArea("TextArea00", "absolute", "3.13%", "132", null, "260", "60.16%", null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1024, 768, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Camera");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Z201604_2016040170-Camera.xfdl", function(exports) {

        this.Button00_onclick = function(obj,e)
        {
        	this.Camera00.takePicture();
        }

        this.Camera00_oncapture = function(obj,e)
        {
        	alert("success");
        	var out = e.url + "\n"+ e.imagedata;
        	this.TextArea00.set_value(out);	
        }

        this.Camera00_onerror = function(obj,e)
        {
        	this.TextArea00.value = e.errormsg;
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Camera00.addEventHandler("oncapture", this.Camera00_oncapture, this);
            this.Camera00.addEventHandler("onerror", this.Camera00_onerror, this);
            this.Button00.addEventHandler("onclick", this.Button00_onclick, this);

        };

        this.loadIncludeScript("Z201604_2016040170-Camera.xfdl", true);

       
    };
}
)();
