﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("test");
                this.set_classname("test");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,625,233);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("Dataset00", this);
            obj._setContents("<ColumnInfo><Column id=\"Column0\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("Dataset01", this);
            obj._setContents("<ColumnInfo><Column id=\"MSG_NO_SEARCHDATA\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"MSG_NO_SEARCHDATA\">검색된 데이터가 존재하지 않습니다.</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "60", "44", "419", "131", null, null, this);
            obj.set_taborder("0");
            obj.set_binddataset("Dataset00");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"406\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell/></Band><Band id=\"body\"><Cell text=\"bind:Column0\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "519", "58", "85", "32", null, null, this);
            obj.set_taborder("1");
            obj.set_text("Button00");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 625, 233, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("test");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","Grid00","nodatatext","ds_noDataMsg","MSG_NO_SEARCHDATA");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("test.xfdl", "lib::Comm.xjs");
        this.registerScript("test.xfdl", function(exports) {
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        this.test_onload = function(obj,e)
        {
        	var objNoMsgDs = obj.objects["ds_noDataMsg"];

        	if (this.gfn_isNull(objNoMsgDs)) {
        		objNoMsgDs = new Dataset;
        		objNoMsgDs.name = "ds_noDataMsg";
        		obj.addChild("ds_noDataMsg" , objNoMsgDs);
        	}

        	objNoMsgDs.clearData();
        	objNoMsgDs.addColumn("MSG_NO_SEARCHDATA", "STRING", 255);
        	objNoMsgDs.addRow();
        	objNoMsgDs.setColumn(0, "MSG_NO_SEARCHDATA", "검색된 데이터가 존재하지 않습니다.");
        	trace(objNoMsgDs.saveXML());
        }

        this.Button00_onclick = function(obj,e)
        {
        	var aMsg = this.gfn_getMsgToArray(sMsgCd);
        	aMsg[1]
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.test_onload, this);
            this.Button00.addEventHandler("onclick", this.Button00_onclick, this);

        };

        this.loadIncludeScript("test.xfdl", true);

       
    };
}
)();
