﻿//XJS=CommWmsUtil.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {

        /******************************************************************************            
        * Name         : commWmsUtil.xjs                                                                                                          
        * Title        : nexacro 공통 Transaction 관련 함수 모음                                                                             
        * @desc     : XP Engine을 이용한 Transaction 처리                                                                                                                      
        * 작성자    :                                                                                                                                 
        * 작성일    :                                                                                                           
        * 변경사항    :                                                                                                        
        ********************************************************************************/ 

        /********************************************************************************
         * 시스템 변수 Return
         * 
         * @param String Key
         * @return Boolean
         ********************************************************************************/
        this.gfn_getSystemConfigData = function(key)
        {
        	return application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", key), "value1");
        }

        /********************************************************************************
         * 다국어 Language Return
         * 
         * @param String Key
         * @return Boolean
         ********************************************************************************/
        this.gfn_getTxtLang = function(key)
        {
        	return application.gds_lang.getColumn(0, key);
        }

        /********************************************************************************
         * Checked Dataset Return
         *
         * @param String Key
         * @return Boolean
         ********************************************************************************/
        this.gfn_getCheckedDataSet = function(ds_param)
        {
        	var rtn_ds = new Dataset();
        	
        	ds_param.filter("CHK == '1'");
        	
        	rtn_ds.copyData(ds_param, true);

        	ds_param.filter("");

        // 	ds_param.set_updatecontrol(false);
        // 	for(var i=0; i<ds_param.rowcount; i++) {
        // 		if(ds_param.getColumn(i, "CHK") == '1')
        // 			ds_param.setRowType(i, Dataset.ROWTYPE_UPDATE);
        // 		else
        // 			ds_param.setRowType(i, Dataset.ROWTYPE_NORMAL);
        // 	}
        		
        	return rtn_ds;
        }

        /********************************************************************************
         * show grid column
         *
         * @param Grid, Column name
         * @return Boolean
         ********************************************************************************/
        this.gfn_show_grid_col = function(grd_param,col_nm)
        {
        	var idx = grd_param.getBindCellIndex("body", col_nm);
        	return grd_param.setRealColSize(idx, grd_param.getFormatColSize(idx), false);	
        }

        /********************************************************************************
         * hide grid column
         *
         * @param Grid, Column name
         * @return Boolean
         ********************************************************************************/
        this.gfn_hide_grid_col = function(grd_param,col_nm)
        {
        	var idx = grd_param.getBindCellIndex("body", col_nm);
        	return grd_param.setRealColSize(idx, 0, false);			
        }

        /********************************************************************************
         * initiate grid columns
         *
         * 1. align right number type.
         * 
         * @param Grid, Column name
         * @return Boolean
         ********************************************************************************/
        this.gfn_initiate_grid_type_1 = function(grd_param)
        {
        	var strPrpt;
        	var idx = grd_param.getCellCount("body");
        		
        	for(var i=0; i<idx; i++) {
        		strPrpt = grd_param.getCellProperty("body", i, "displaytype");
        		
        		// 컬럼의 속성(displaytype)이 숫자형일 경우, 오른쪽 정렬.
        		if(strPrpt == "number")
        			grd_param.setCellProperty("body", i, "align", "right");
        	}
        }

        /********************************************************************************
         * initiate grid columns
         *
         * 1. align right number type.
         * 
         * @param Grid, Column name
         * @return Boolean
         ********************************************************************************/
        this.gfn_getComboData = function(searchid ,ObjType ,viewObj ,srvID ,value)
        {
        	
        }

        
        /**
        * 체크된 Data의 빈값 확인
        * @param  {object} obj:Grid - Grid Object
        * @param  {object} oData - Dataset Object
        * @param  {array}  ds_col - dstaSet 컬럼 명
        * @return (string) 컴럼명 입력하세요.
        */
        this.gfn_isChkDataNullCheck = function(obj,oData,ds_col)
        {
            if(this.gfn_isNull(oData)) return false;
            if(this.gfn_isNull(ds_col)) return false;
            
            var bDs_col = ds_col.split("|");
            var sHaedNm = "";
            var nullCnt = 0;
            var colIndex = 0;
            
            for(var i = 0 ; i < oData.rowcount ; i++){
            
        		if(oData.getColumn(i, "CHK") != '1') continue;
        		
        		for(var j = 0 ; j < bDs_col.length ; j++){
        			if(this.gfn_isNull(oData.getColumn(i, bDs_col[j]))){
        				var nRow = i;
        				colIndex = obj.getBindCellIndex("body", bDs_col[j]);
        				sHaedNm = oData.getConstColumn(nexacro.replaceAll(obj.getCellProperty("Head", colIndex, "text"), "bind:", ""));
        				
        				nullCnt++;
        				
        				this.gfn_alert((nRow+1) + " row " + sHaedNm + " 을(를) 입력하세요.", "", function(msg, flag){
        					oData.set_rowposition(nRow);
        					obj.setCellPos(colIndex);
        					obj.setFocus();
        				});
        				
        				break;
        			}
        		}
        		
        		if(nullCnt > 0) break;
            }
            
            if(nullCnt > 0) return false;
            else return true;
        }

        /********************************************************************************
         * 가용수량 계산(그리드)
         *
         * @author 김응헌
         * @since 2017-04-04
         * @version 1.0
         *
         * @param {object} obj:Grid - Grid Object
         * @param {number} curRow - Dataset Index
         * @param {string} ttqtyName - ttqty 총수량
         * @param {string} alqtyName - alqty 가용수량
         * @param {string} piqtyName - piqty 가용수량
         * @param {string} paqtyName - paqty 가용수량
         *
         * @return number
         *
         ********************************************************************************/
        this.gfn_grdCalcAvQty = function(obj,curRow,ttqtyName,alqtyName,piqtyName,paqtyName)
        {
        	if(this.gfn_isNull(obj) || this.gfn_isNull(curRow)){
        		return -1;
        	}
        	
        	if(this.gfn_isNull(ttqtyName)){
        		ttqtyName = "ttqty";
        	}

        	if(this.gfn_isNull(alqtyName)){
        		alqtyName = "alqty";
        	}

        	if(this.gfn_isNull(piqtyName)){
        		piqtyName = "piqty";
        	}

        	if(this.gfn_isNull(paqtyName)){
        		paqtyName = "paqty";
        	}

        	var dsSource = this.lookup(obj.binddataset);
        	var ttqty  = this.gfn_isNullZero(dsSource.getColumn(curRow, ttqtyName));
        	var alqty  = this.gfn_isNullZero(dsSource.getColumn(curRow, alqtyName));
        	var piqty  = this.gfn_isNullZero(dsSource.getColumn(curRow, piqtyName));		
        	var paqty  = this.gfn_isNullZero(dsSource.getColumn(curRow, paqtyName));

        	return (ttqty - alqty - piqty - paqty);
        }

        /********************************************************************************
         * 가용수량 계산(데이터셋)
         *
         * @author 김응헌
         * @since 2017-04-04
         * @version 1.0
         *
         * @param {object} dsObj:Dataset - Dataset Object
         * @param {number} curRow - Dataset Index
         * @param {string} ttqtyName - ttqty 총수량
         * @param {string} alqtyName - alqty 가용수량
         * @param {string} piqtyName - piqty 가용수량
         * @param {string} paqtyName - paqty 가용수량
         *
         * @return number
         *
         ********************************************************************************/
        this.gfn_dsCalcAvQty = function(dsObj,curRow,ttqtyName,alqtyName,piqtyName,paqtyName)
        {
        	if(this.gfn_isNull(dsObj) || this.gfn_isNull(curRow)){
        		return -1;
        	}
        	
        	if(this.gfn_isNull(ttqtyName)){
        		ttqtyName = "ttqty";
        	}

        	if(this.gfn_isNull(alqtyName)){
        		alqtyName = "alqty";
        	}

        	if(this.gfn_isNull(piqtyName)){
        		piqtyName = "piqty";
        	}

        	if(this.gfn_isNull(paqtyName)){
        		paqtyName = "paqty";
        	}
        	
        	var ttqty  = this.gfn_isNullZero(dsObj.getColumn(curRow, ttqtyName));
        	var alqty  = this.gfn_isNullZero(dsObj.getColumn(curRow, alqtyName));
        	var piqty  = this.gfn_isNullZero(dsObj.getColumn(curRow, piqtyName));		
        	var paqty  = this.gfn_isNullZero(dsObj.getColumn(curRow, paqtyName));

        	return (ttqty - alqty - piqty - paqty);
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
