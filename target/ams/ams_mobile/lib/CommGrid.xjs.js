﻿//XJS=CommGrid.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /* 
         ===============================================================================
         ==  Grid관련 공통함수들은 여기에 작성한다.
         ===============================================================================
         ● gfn_GridSort      : 그리드의 Sort를 처리한다.
         ● gfn_clearSortMark : Seleted Column을 제외한 Sort Mark 제거
         ● gfn_DsCheckValid  : DataSet 내의 데이터 무결성을 검사하는 함수
         ● gfn_ItemCheck     : DataSet 내의 데이터 무결성을 검사하는 함수
         */

        this.fv_constSortFlag = false;

        this.ct_separator = " ";

        this.fv_arrClipboard;

        this.gv_filterDs;
        this.gv_filterCol = "";
        this.gv_gridHeadText;
        this.gv_grid;
        this.gv_col;
        this.gv_row;
        this.gv_gridMenu;

        /**
        * Grid Head중 check box가 있을 경우, check box 클릭 이벤트 발생시 전체 row에 대한 check/uncheck 설정 함수
        * @param  {object} obj - 체크를 처리할 Grid Object
        * @param  {object} e = GridClickEventInfo
        * @return N/A
        */
        this.gfn_setGridCheckAll = function (obj,e)
        {
            if (obj.readonly == true) {
                return;
            }

            var sVal;
            var sChkCol;
            var oData;

            oData = obj.getBindDataset();
            sChkCol = this.gfn_nvl(obj.getCellProperty("body", e.col, "text"), "");
            sChkCol = sChkCol.split("bind:").join("");
            
            if(this.gfn_isNull(sChkCol)) {
                return;
            }
            
            var sType = obj.getCellProperty("head", e.cell, "displaytype");
            if (sType != "checkbox") {
                return;
            }

            // Head셋팅
            sVal = this.gfn_nvl(obj.getCellProperty("head", e.cell, "text"), "0");
            
            if (sVal == "0") {
                obj.setCellProperty("head", e.cell, "text", "1");
                sVal = "1";
            } else {
                obj.setCellProperty("head", e.cell, "text", "0");
                sVal = "0";
            }

            // Body셋팅
            oData.set_enableevent(false);
            for (var i = 0; i < oData.rowcount; i++) {
                oData.setColumn(i, sChkCol, sVal);
            }
            oData.set_enableevent(true);
        }

        /**
        * Grid를 Excel로 Export하는 함수
        * @param  {object} 단일Export -  Grid Object  , 다수Export - Array Objec[Grid Object,Grid Object]
        * @param  {string} sFileName - Export될 파일명
        * @return N/A
        */

        this.fv_exportObject;            
        this.fv_exportFileName;

        this.gfn_exportExcel2 = function(objForm,obj,sFileName,strSheet)
        {    
            this.ffobj = objForm;
            this.fv_exportObject = obj;            
            this.fv_exportFileName = sFileName;

            application.set_usewaitcursor(true,true);

            this.ct_sheet = this.gfn_isNull(strSheet) ? "Sheet1" :strSheet;

            this.gfn_exportExcelProcess();
        }

        /**
        * _exportExcelProcess
        * @return N/A
        */
        this.gfn_exportExcelProcess = function()
        {
            var obj = this.fv_exportObject;
            var sFileName = this.fv_exportFileName;
            var oGrid;
            var sSheetName;
            
            var sType = obj.toString().toUpperCase();
            
            if(!this.gfn_isNull(sFileName)){
                sFileName = sFileName + "_" + this.gfn_today();
            }else{
                sFileName = this.gfn_today();
            }

            var exportObj = new ExcelExportObject();

            var sSvcUrl = application.services["svc"].url + "ams/XExportImport";
            this.ffobj.setWaitCursor(true,true);
         
            exportObj.addEventHandler("onsuccess", this._exportExcelEnd, this);
            exportObj.addEventHandler("onerror", this._exportExcelEnd, this);

            exportObj.set_exporttype(nexacro.ExportTypes.EXCEL2007);
            exportObj.set_exporturl(sSvcUrl);
            exportObj.set_exportfilename(sFileName);

            if(sType == "[OBJECT GRID]"){
                oGrid = obj;
                sSheetName = "sheet1";
                exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  sSheetName + "!A1","allband","allrecord","suppress","allstyle","background","font", "both","cellline");
            }else{
                for(var i=0; i<obj.length; i++){
                    sSheetName = this.ct_sheet+(i+1);
                    oGrid = obj[i];
                    exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  sSheetName + "!A1","allband","allrecord","suppress","allstyle","background","font", "both","cellline");
                }
            }

            exportObj.exportData();
        }

        this.gfn_exportExcel = function(objForm,obj,sFileName,bOneSheet)
        {
            this.ffobj = objForm;
            this.fv_exportObject = obj;
            this.fv_exportFileName = sFileName;
            this.fv_bOneSheet = this.gfn_isNull(sFileName) ? "Sheet1" : sFileName;
            
            if(obj.toString().toUpperCase() == "[OBJECT GRID]"){
                this.fv_exportGridSelectType = this.fv_exportObject.selecttype;
                
                if(this.fv_exportGridSelectType == "area"){
                    this.fv_exportObject.set_selecttype("row");
                }
            }else{
                this.fv_exportGridSelectType = [];
                
                for(var i = 0; i < obj.length; i++){
                    this.fv_exportGridSelectType.push(obj[i].selecttype);
                    
                    if(obj[i].selecttype == "area"){
                        obj[i].set_selecttype("row");
                    }
                }
            }
            
            this._gfn_exportExcelProcess();
        }

        this._gfn_exportExcelProcess = function()
        {
            var obj = this.fv_exportObject;
            
            var sFileName = this.fv_exportFileName;
            var dToday = new Date();
            var oGrid;
            var sSheetName;
            var strType = obj.toString().toUpperCase();
            
            if(!this.gfn_isNull(sFileName)){
                sFileName = sFileName + "_" + this.gfn_today();
            }else{
                sFileName = this.gfn_today();
            }
            
            this.exportObj = new ExcelExportObject();
            var sSvcUrl = application.services["svc"].url+"ams/XExportImport";
            
            this.exportObj.addEventHandler("onerror", this._gfn_exportExcel_onerror, this);
            this.exportObj.addEventHandler("onsuccess", this._gfn_exportExcel_onsuccess, this);
            this.exportObj.addEventHandler("onprogress", this._gfn_exportExcel_onprogress, this);
            
            this.exportObj.set_exporttype(nexacro.ExportTypes.EXCEL2007);
            this.exportObj.set_exportuitype("exportprogress");
            this.exportObj.set_exporteventtype("itemrecord");
            this.exportObj.set_exportmessageprocess("%d[%d/%d]");
            
            this.exportObj.set_exporturl(sSvcUrl);
            this.exportObj.set_exportfilename(sFileName);

            if (strType == "[OBJECT GRID]"){
                oGrid = obj;
                sSheetName = this.ct_sheet+"1";
                this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A1","allband","allrecord","nosuppress","allstyle","background","font", "both","cellline");
            } else {
                if(this.fv_bOneSheet)
                {
                    var nSheetRow = 1;
                    for (var i=0; i<obj.length; i++){
                        
                        sSheetName = this.ct_sheet+"1";
                        oGrid = obj[i];                
                        this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A"+nSheetRow,"allband","allrecord","nosuppress","allstyle","background","font", "both","cellline");
                        
                        var nHeadRowCnt = 0;
                        for(var j = 0; j < oGrid.getFormatRowCount(); j++)
                        {
                            if(oGrid.getFormatRowProperty(j,"band") == "head")
                            {
                                nHeadRowCnt++;
                            }
                        }
            
                        nSheetRow += parseInt(oGrid.getBindDataset().rowcount) +  nHeadRowCnt + 2;                
                    }
                }
                else
                {
                    for (var i=0; i<obj.length; i++){
                        sSheetName = this.ct_sheet+(i+1);
                        oGrid = obj[i];
                        this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A1","allband","allrecord","nosuppress","allstyle","background","font", "both","cellline");
                    }
                }
            }
            
            var result = this.exportObj.exportData();
        }

        
        /**
        * ExceExport 성공시 callback
        * @return N/A
        */
        this._exportExcelEnd = function(obj,e)
        {
            this.ffobj.setWaitCursor(false,true);
        }

        /**
        * Excel파일 import하는 함수
        * @param  {object} oData
        * @return boolean
        */
        this.gfn_importExcel = function(objForm,dsName,sheetName)
        {
            var importObj = new nexacro.ExcelImportObject("importExcel",objForm);
            var sSvcUrl = application.services["svc"].url+"ams/XExportImport";
            
            this.fv_bOneSheet = this.gfn_isNull(sheetName) ? "Sheet1" : sheetName;
            trace("this.fv_bOneSheet : " + this.fv_bOneSheet);
            
            importObj.set_importtype(nexacro.ImportTypes.EXCEL2007);
            importObj.addEventHandler("onsuccess", this._gfn_importExcel_onsuccess, objForm);
            importObj.addEventHandler("onerror", this._gfn_importExcel_onerror, objForm);
            
            importObj.set_importurl(sSvcUrl);
            importObj.importData("", this.fv_bOneSheet+"!A1:", dsName);
        }

        this._gfn_importExcel_onsuccess = function(obj,e)
        {
            trace(obj.name + ":" + e.eventid);
        }

        this._gfn_importExcel_onerror = function(obj,e)
        {
            trace(obj.name + ":" + e.eventid);
            trace("\ne.fromobject: " + e.fromobject);
            trace("\ne.fromreferenceobject: " + e.fromreferenceobject);
            trace("\ne.errorcode: " +  e.errorcode);
            trace("\ne.errormsg: " + e.errormsg);
        }

        /**
        * Dataset 수정여부 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @return boolean
        */
        this.gfn_isUpdate = function(oData)
        {
            if(this.gfn_isNull(oData)) { //인자 오류
                return false;
            }
            
            if(oData.getDeletedRowCount() > 0) { // 삭제 체크
                return true;
            }
            
        //    this.grd_user.set_enableredraw(false);
            if (oData.findRowExpr("(this.getRowType(rowidx)==Dataset.ROWTYPE_UPDATE)||(this.getRowType(rowidx)==Dataset.ROWTYPE_INSERT)") >= 0) {
                return true;
            }
            
            return false;
        }

        /**
        * dataSet의 Row가 변경 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @param  {integer} nRow - 수정여부를 체크할 Dataset rowposition
        * @return boolean
        */
        this.gfn_isUpdatedRow = function (oData,nRow)
        {
            if (oData.updatecontrol == true) {
                if (oData.getRowType(nRow) == 2 || oData.getRowType(nRow) == 4) {
                    return true;
                }
                
                return false;
            } else {
                for (var i = 0; i<oData.getColCount(); i++) {
                    if (this.gfn_isUpdateColumn(oData, nRow, i) == true) {
                        return true;
                    }
                }
            }
            
            return false;
        }

        /**
        * dataSet의 Row 에서 해당 칼럼이 변경 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @param  {integer} nRow - 수정여부를 체크할 Dataset rowposition
        * @param  {integer} nColIdx - 수정여부를 체크할 Dataset Column Index
        * @return boolean
        */
        this.gfn_isUpdateColumn = function (oData,nRow,nColIdx)
        {
            if (oData.getRowType(nRow) == 2) {
                if (this.gfn_isNull(oData.getColumn(nRow, nColIdx))) {
                    return false;
                }
            } else {
                if (oData.getColumn(nRow, nColIdx) == oData.getOrgColumn(nRow, nColIdx)) {
                    return false;
                }
            }
            
            return true;
        }

        /**
        * Data의 빈값 확인
        * @param  {object} obj:Grid - Grid Object
        * @param  {object} oData - Dataset Object
        * @param  {array}  ds_col - dstaSet 컬럼 명
        * @param  {array}  grid_col - grid 컬럼 index
        * @return (string) 컴럼명 입력하세요.
        */
        this.gfn_isDataNullCheck = function(obj,oData,ds_col,grid_col)
        {
            if(this.gfn_isNull(oData)) return false;
            if(this.gfn_isNull(ds_col)) return false;
            if(this.gfn_isNull(grid_col)) return false;
            
            var bDs_col = ds_col.split("|");
            var bGrid_col = grid_col.split("|");
            var bRtn = false;
            var sHaedNm = "";
            var nullCnt = 0;
            
            for(var i=0 ; i < oData.getRowCount() ; i++) {
                for(var k=0 ; k < bDs_col.length ; k++) {
                    if(this.gfn_isNull(oData.getColumn(i, bDs_col[k]))) {
                        sHaedNm = obj.getCellProperty("Head", bGrid_col[k], "text").replace("\n", " ");
                        
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.edt_value.value)){
        					nullCnt++;
        					
        					this.gfn_alert((i+1) + " row " + sHaedNm + " 을(를) 입력하세요.", "", function(msg, flag){
        						oData.set_rowposition(i);
        						obj.setCellPos(bGrid_col[k]);
        						obj.setFocus();
        						return true;
        					});
        				}
                    }
                    
                    if(nullCnt > 0) break;
                }
        		
        		if(nullCnt > 0) break;
            }
            
            return bRtn;
        }

        /**
        * dataSet의 Row 값들을 array로 반환
        * @param  {object} oData - Dataset Object
        * @param  {integer} nRow - Dataset rowposition
        * @return Array
        */
        this.gfn_getRowDataToArray = function (oData,nRow)
        {
            if(this.gfn_isNull(oData)) return;
            if(this.gfn_isNull(nRow)) return;
            var aRtn = new Array();
            var nIdx = 0;
            
            for(var i = 0; i<oData.getColCount(); i++) {
                aRtn[nIdx] = oData.getColumn(nRow, i);
                nIdx++;
            }
            return aRtn;
        }

        /**
        * dataSet의 Row 값들을 String으로 반환
        * @param  {object} oData - Dataset Object
        * @param  {integer} nRow - Dataset rowposition
        * @param  {string} sGubn - Dataset rowposition
        * @return string
        */
        this.gfn_getRowDataToString = function (oData,nRow,sGubn)
        {
            if(this.gfn_isNull(oData)) return;
            if(this.gfn_isNull(nRow)) return;
            if(this.gfn_isNull(sGubn)) sGubn = "|";
            
            var sRtn = "";
            
            for(var i = 0; i<oData.getColCount(); i++) {
                sRtn += oData.getColumn(nRow, i) + sGubn;
            }
            
            sRtn = sRtn.substr(0, sRtn.length-sGubn.length);
            
            return sRtn;
        }

        //======================================= Grid Scroll Over 관련 공통함수 시작 =======================================
        this.fv_currentPage = 1;
        this.fv_pagingTran = false;

        // 그리드 초기화
        // 1. 그리드 Head Text 처리
        // 2. Scroll Over 발생 시 다음 데이터 조회 이벤트 설정
        // 3. Arguments
        //    objForm : Form Object
        //    objGrd : Grid Object
        //    Scroll Paging : "Y"(Default)/"N"
        this.gfn_initGrd = function (objForm,objGrd,sScrollPaging)
        {
        	objGrd.form = objForm;

        	var nGrdCellCnt = objGrd.getCellCount("head");
        	var objBindDs = objGrd.getBindDataset();

        	for (var i = 0; i < nGrdCellCnt; i++) {
        		var sColNm = objGrd.getCellProperty("head", i, "text");
        		var objColInfo = objBindDs.getColumnInfo(sColNm);
        		var sVarVal = this.gfn_getLang(sColNm);

        		if (this.gfn_isNull(objColInfo)) {
        			objBindDs.addConstColumn(sColNm, sVarVal);
        		} else {
        			objBindDs.setConstColumn(sColNm, sVarVal);
        		}

        		objGrd.setCellProperty("head", i, "text", "bind:" + sColNm);
        	}

        	var sBindNm = "bind_" + objGrd.name;
        	var objBind = objForm.binds["bind_" + objGrd.name];

        	if (this.gfn_isNull(objBind)) {
        		objBind = new BindItem(sBindNm, objGrd.name, "nodatatext", "ds_noDataMsg", "MSG_NO_SEARCHDATA");
        		this.addChild(sBindNm, objBind); 
        		objBind.bind();
        	}

        //<BindItem id="item0" compid="Grid00" propid="nodatatext" datasetid="ds_noDataMsg" columnid="MSG_NO_SEARCHDATA"/>

        // 	for (var i = 0; i < objForm.length; i++) {
        // 		if (objForm[i] instanceof Dataset) {
        // 			var nConstColCnt = objForm[i].getConstCount();
        // 
        // 			if (nConstColCnt >= 0) {
        // 				for (var j = 0 ; j < nConstColCnt; j++) {
        // 					var sKey = objForm[i].getConstColID(j);
        // 					var sValue = this.gfn_getLang(sKey);
        // 					objForm[i].setConstColumn(sKey, sValue);
        // 				}
        // 			}
        // 		}
        // 	}

        	if (this.gfn_isNull(sScrollPaging)) sScrollPaging = "Y";

        	if (sScrollPaging == "Y") {
        		objGrd.addEventHandler("onvscroll", this.gfn_listGrd_onvscroll, objForm);
        	}
        }

        // Grid Head Text를 처리 하고, objTgtDs에 objSrcDs의 내용을 추가
        this.gfn_setDsData = function (objTgtDs,objSrcDs)
        {
        	var nConstColCnt = objTgtDs.getConstCount();
        	var nSrcConstColCnt = objSrcDs.getConstCount();
        	var nSrcRowCnt = objSrcDs.getRowCount();
        	var nTgtRowCnt = objTgtDs.getRowCount();

        	if (nConstColCnt >= 0 && nSrcConstColCnt == 0) {
        		for (var j = 0 ; j < nConstColCnt; j++) {
        			var sKey = objTgtDs.getConstColID(j);
        			var sVal = objTgtDs.getConstColumn(sKey);
        			objSrcDs.addConstColumn(sKey, sVal);
        		}
        	}

        	if (!this.fv_pagingTran) {
        		if (nSrcRowCnt == 0) {
        			objTgtDs.clearData();
        		} else {
        			objTgtDs.copyData(objSrcDs);
        		}

        	} else {
        		if (nSrcRowCnt > 0) {
        			objTgtDs.appendData(objSrcDs);
        		} else {
        			objTgtDs.clearData();
        		}
        	}
        	objSrcDs.clearData();

        	this.fv_pagingTran = false;
        }

        this.gv_count = 0;
        this.gv_totPage = 0;

        this.gfn_setPage = function(objParamDs){
        	this.gv_count = Number(objParamDs.getColumn(0, "COUNT"));	
        	var pagingLimit = Number(objParamDs.getColumn(0, "pagingLimit"));
        	this.fv_currentPage = Number(objParamDs.getColumn(0, "currentPage"));
        	
        	if(this.gv_count%pagingLimit == 0) this.gv_totPage = this.gv_count/pagingLimit;
        	else this.gv_totPage = parseInt(this.gv_count/pagingLimit) + 1;
        }

        
        // 그리드 Scroll Over 발생 시 다음 내용 조회 처리
        this.gfn_listGrd_onvscroll = function(obj,e)
        {	
        	var objDs = obj.getBindDataset();
        	if (this.gfn_isNull(obj.vsclbarpos) || obj.vsclbarpos < e.pos){
        		obj.vsclbarpos = e.pos;
        	} else if (obj.vsclbarpos >= e.pos){
        		obj.vsclbarpos = 0;
        	}

        	if (obj.vsclbarpos == obj.vscrollbar.max) {
        		if(this.fv_currentPage <= this.gv_totPage ){
        		//if (this.fv_pagingTran) {
        			if (objDs.getRowCount() == (this.fv_currentPage * application.gds_param.getColumn(0, "pagingLimit"))) {
        				this.fv_currentPage++;
        				obj.form.fv_currentPage = this.fv_currentPage;
        				
        				this.fv_pagingTran = true;
        				if (!this.gfn_isNull(obj.form.fn_search)) obj.form.fn_search();
        			}
        		}
        	}
        }
        //======================================= Grid Scroll Over 관련 공통함수 종료 =======================================

        //======================================= Grid Row MultiSelect 관련 공통함수 시작 =======================================
        this.gfn_setGrdSelectStatus = function (objGrd,eRow)
        {
        	objGrd.set_enableredraw(false);

        	var objDs = objGrd.getBindDataset();
        	var bSelect = true;

        	if (this.gfn_isNull(objGrd.selectedRows)) objGrd.selectedRows = "";

        	var sSelectedRow = objGrd.selectedRows;

        	if (String(sSelectedRow).indexOf("[" + eRow + "]") >= 0) bSelect = false;

        	if (bSelect) {
        		sSelectedRow += "[" + eRow + "]";
        	} else {
        		sSelectedRow = String(sSelectedRow).replace("[" + eRow + "]", "");
        	}

        	objGrd.selectedRows = sSelectedRow;

        	for (var i = 0; i < objDs.getRowCount(); i++) {
        		objGrd.selectRow(i, false);
        	}

        	var arrRows = String(objGrd.selectedRows).split("][");
        	var nSelRow = -1;
         	for (var i = 0; i < arrRows.length; i++) {
        //		trace("arrRows[" + i + "] : " + String(arrRows[i]).replace("[", "").replace("]", ""));
        		nSelRow = Number(String(arrRows[i]).replace("[", "").replace("]", ""));
         		objGrd.selectRow(nSelRow, true);
         	}

        	objGrd.set_enableredraw(true);

        	if (this.gfn_isNull(objGrd.selectedRows)) nSelRow = -1;
        	if (nSelRow < 0) objDs.set_rowposition(nSelRow);

        	return nSelRow;
        }

        this.gfn_setSelectAllStatus = function(objBtn,objGrd)
        {
        	var objDs = objGrd.getBindDataset();
        	var bSelect = true;
        	if (objBtn.selectAll == "N") bSelect = false;

        	var nRow = -1;
        	var sSelectRows = "";

        	for (var i = 0; i < objDs.getRowCount(); i++) {
        		objGrd.selectRow(i, bSelect);
        		if (bSelect) {
        			nRow = i;
        			sSelectRows += "[" + i + "]";
        		}
        	}

        	objGrd.selectedRows = sSelectRows;

        	objDs.set_rowposition(nRow);

        	return nRow;
        }

        //======================================= Grid Row MultiSelect 관련 공통함수 종료 =======================================
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
