﻿//XJS=CommFile.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /**
         * @class 파일초기설정
         * @param oForm : form object
         * @param oCallBack : callback 함수 object
         * @param bBinary : BLOB여부(true:blob, false:ftp)
         * @param bMultiSelect : 다중파일 업로드여부(true:multi, false:single)
         * @return None
         */ 
        //this.gfn_initFile = function(oForm, oCallBack, bBinary, bMultiSelect)
        this.gfn_initFile = function(oForm,oCallBack,bMultiSelect)
        {
            //FileUpload컴포넌트 생성    
            var sFileUpload = "_tempFileUpload";
            var oFileUpload = Eco.XComp.query(this, "typeOf == 'FileUpload' && prop[name] == '"+sFileUpload+"'")[0];
            
            if(Eco.isEmpty(oFileUpload)){
                oFileUpload = new FileUpload(sFileUpload);
                this.addChild(sFileUpload, oFileUpload);
                /* 2016.05.27 콜백함수 각 화면에서 처리하도록 수정, 단 콜백함수 이름은 통일할 것 */
                oFileUpload.addEventHandler("onitemchanged", this._gfn_fileUploadOnItemChanged, this);
                oFileUpload.addEventHandler("onsuccess", oForm.fncFileUploadOnSuccess, this);
                oFileUpload.addEventHandler("onerror", oForm.fncFileUploadOnError, this);
                oFileUpload.show();
                
                oFileUpload.deleteItem(0); /* FileUpload 객체 생성 시에 ITEM 생성 건 제거 -> ADD 시에 Append 하므로*/
            }
                
            //FileDownload컴포넌트 생성    
            var sFileDownload = "_tempFileDownload";
            var oFileDownload = Eco.XComp.query(this, "typeOf == 'FileDownload' && prop[name] == '"+sFileDownload+"'")[0];
            
            if(Eco.isEmpty(oFileDownload)){
                oFileDownload = new FileDownload(sFileDownload);
                this.addChild(sFileDownload, oFileDownload);
                oFileDownload.addEventHandler("onerror", this._gfn_fileDownloadOnError, this);
                oFileDownload.addEventHandler("onsuccess", this._gfn_fileDownloadOnSuccess, this);
                oFileDownload.show();
            }
                    
            //데이타셋 생성
            var sFileDataset = "dsFileList";
            var oFileDataset = Eco.XComp.query(this, "typeOf == 'Dataset' && prop[name] == '"+sFileDataset+"'")[0];

            if(Eco.isEmpty(oFileDataset)){
                oFileDataset = new Dataset(sFileDataset);
                this.addChild(sFileDataset, oFileDataset);
                
                oFileDataset.addColumn("FILE_ID", "string");
                oFileDataset.addColumn("FILE_NAME", "string");
            }    
            
            //ftp or binary 설정
            //if(Eco.isEmpty(bBinary)) bBinary = false;
                    
            //single or multi select 설정
            if(Eco.isEmpty(bMultiSelect)) bMultiSelect = false;    
            if(bMultiSelect) oFileUpload.set_multiselect(true);
            else             oFileUpload.set_multiselect(false);

            //upload path설정    
            var sUploadPath, sUrl;
            var oFormDiv = null;
            
            try{
                oFormDiv = oForm.getOwnerFrame().form.div_work;
                sUrl = oFormDiv.url;
            }catch(e){
                oFormDiv = oForm.getOwnerFrame();
                sUrl = oFormDiv.formurl;
            }
            
            sUrl = sUrl.replace("::","\\");
            sUploadPath = sUrl.substr(0,sUrl.lastIndexOf(".xfdl"));
                
            //파라미터 저장
            this._FORM_FILE_OBJ = null;
            this._FORM_FILE_OBJ = {};
            this._FORM_FILE_OBJ.form = oForm;
            this._FORM_FILE_OBJ.callback = oCallBack;
            //this._FORM_FILE_OBJ.binary = bBinary;
            this._FORM_FILE_OBJ.multi = bMultiSelect;
            this._FORM_FILE_OBJ.uploadpath = sUploadPath;
            this._FORM_FILE_OBJ.fileupload = oFileUpload;
            this._FORM_FILE_OBJ.filedownload = oFileDownload;
            this._FORM_FILE_OBJ.filedataset = oFileDataset;    
        }

        /**
         * @class 파일조회
         * @param sFileId : file 그룹ID
         * @param sFileSeq : file 그룹ID내 파일seq값
         * @return None
         */ 
        this.gfn_searchFile = function(sFileId,sFileSeq)
        {
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            
            if(Eco.isEmpty(sFileId)){
                oFileDataset.clearData();
                return false;
            }
            
            sFileId = Eco.iif(Eco.isEmpty(sFileId), "", sFileId);
            sFileSeq = Eco.iif(Eco.isEmpty(sFileSeq), "", sFileSeq);
            trace("sFileId = " + sFileId);
            trace("sFileSeq = " + sFileSeq);
            this.fsp_clear();
            this.fsp_addSearch("comm/cs:cs_multifile_ftp_s01");
            this.fsp_callSvc(
                  "DkuAction"
                , "gfn_searchFile"            //svcId
                , "ds_in=ds_cond"    //inDs
                , oFileDataset.name+"=ds_out"    //outDS
                , "FILE_ID=" + sFileId+" SEQ="+sFileSeq
                , "_gfn_callbackSearchFile"        //callBackFnc
            );
        }

        this._gfn_callbackSearchFile = function(strSvcID,nErrorCode,strErrorMag)
        {
            if(nErrorCode < 0){
                this.gfn_alert("파일조회중 오류가 발생하였습니다.\n관리자에게 문의하세요.");
                this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "search_file", false);
                return;
            }
            
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            var sRtn = oFileDataset.saveXML();
            this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "search_file", sRtn);
        }

        /**
         * @class 파일추가
         * @return None
         */ 
        this.gfn_addFile = function()
        {        
            //한건인 경우 삭제 후 추가하도록 유도
            // multi 인 경우 file dialog 에서 다건 선택 가능하므로 false임, 하지만 여러 건 등록 가능함 20160613, 
            /*if(!this._FORM_FILE_OBJ.multi)
            {
                if(this._FORM_FILE_OBJ.filedataset.rowcount == 1)
                {
                    this.gfn_alert("파일이 추가할 수 없습니다.\n(단, 변경할 경우 삭제 후 다시 파일을 추가해주세요.)");
                    this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "add_file", false);
                    return false;
                }
            }*/    
            
            //파일추가
            var oFileUpload = this._FORM_FILE_OBJ.fileupload;

            oFileUpload.appendItem(); 
            
            var nFileCnt = oFileUpload.getItemCount()-1;

            oFileUpload._items[nFileCnt].fileitembutton.click();
        }

        this._gfn_fileUploadOnItemChanged = function(obj,e)
        {
            var sFullPath = obj.value;

            if(sFullPath == null || sFullPath == "" ) {
                obj.deleteItem(obj.index);
                return;
            }
            
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            
            var sFileName;
            var sDirExpt;
            
            if(sFullPath.indexOf("\:\\") < 0){   /* IE인 경우에는 바로 File Name 이므로*/
                var nRow = oFileDataset.addRow();        
                //있으면 입력, 없으면 저장시 신규발급
                var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                if(!Eco.isEmpty(sFileId)){
                    oFileDataset.setColumn(nRow, "FILE_ID", sFileId);
                }
                
                oFileDataset.setColumn(nRow, "FILE_NAME", sFullPath);    
            }else{
                var sFileSplitFlag = sFullPath.substr(0, sFullPath.indexOf("\:\\")+2);
                var sFileArray = sFullPath.split(sFileSplitFlag);

                sFileArray = sFileArray.splice(1, sFileArray.length);

                for(var i = 0; i < sFileArray.length ; i++){
                    if(i < sFileArray.length-1){
                        sFileArray[i] = sFileArray[i].substr(0, sFileArray[i].length-1);
                    }
                    
                    sDirExpt = sFileArray[i].lastIndexOf("\\")+1;
                    sFileName = sFileArray[i].substr(sDirExpt);
                    
                    var nRow = oFileDataset.addRow();        
                    //있으면 입력, 없으면 저장시 신규발급
                    var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                    
                    if(!Eco.isEmpty(sFileId)){
                        oFileDataset.setColumn(nRow, "FILE_ID", sFileId);
                    }
                    
                    oFileDataset.setColumn(nRow, "FILE_NAME", sFileName);    
                }
            }
            
            oFileDataset.setColumn(nRow, "ITEM_INDEX", obj.index);    /* 추가된 파일 삭제를 위한 index 저장*/
            
            //데이타셋 리턴
            var sRtn = oFileDataset.saveXML();

            this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "add_file", sRtn);
        }

        /**
         * @class 파일업로드
         * @return None
         */ 
        this.gfn_uploadFile = function()
        {    
            var sFileId = this._FORM_FILE_OBJ.filedataset.getColumn(0, "FILE_ID");
            if(Eco.isEmpty(sFileId)) sFileId = "";
            
            //FTP 업로드
            strUrl = application.services["svcurl"].url + "invoice/upload.do?GROUP_CODE=1&INVOICE_SEQ=1&SHIPMENT_ID=1&CORP_TYPE=1&TRANS_CORP_CD=1&DC_CD=1&INVOICE_ID=1&STATUS=1&USER_ID=1";
            this._FORM_FILE_OBJ.fileupload.upload(strUrl);
        }

        this._gfn_fileUploadOnSuccess = function(obj,e)
        {
            alert("success");

            if( e.datasets == null || e.errorcode < 0 ){
                this.gfn_alert("파일업로드 오류가 발생하였습니다.\n관리자에게 문의하세요.");
                this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "upload_file", false);
                return;
            }else{
                //fileupload item 초기화        
                var nLoopCnt = obj.getItemCount();
                for(var i = nLoopCnt-1; i >= 0; i--){
                    obj.deleteItem(i);
                }
                
                var oFileDataset = this._FORM_FILE_OBJ.filedataset;
                oFileDataset.copyData(e.datasets[0]);
                
                var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                
                if(!Eco.isEmpty(sFileId)){            
                    var sRtn = oFileDataset.saveXML();
                    this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "upload_file", sRtn);
                }else{
                    this.gfn_alert("파일ID를 생성하지 못했습니다.\n관리자에게 문의하세요.");
                    this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "upload_file", false);
                }
            }
        }

        this._gfn_fileUploadOnError = function(obj,e)
        {
            alert("error");
        }

        /**
         * @class 파일삭제
         * @param dsObj : 데이타셋 object
         * @return None
         */ 
        this.gfn_deleteFile = function(dsObj)
        {    
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            //단건인경우
            if(Eco.isEmpty(dsObj)){
                var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                
                if(!Eco.isEmpty(sFileId)){
                    oFileDataset.setColumn(0, "CHK", '1');
                }
            }else{    //다건인경우
                for(var i = 0; i < dsObj.rowcount; i++){
                    if(dsObj.getColumn(i, "CHK") == '1'){
                        oFileDataset.setColumn(i, "CHK", '1');
                    }else{
                        oFileDataset.setColumn(i, "CHK", '0');
                    }
                }
            }
            
            var serviceURL = "svc::DeleteServlet?PATH="+this._FORM_FILE_OBJ.uploadpath;
            this.transaction("delete", serviceURL, "ds_in="+oFileDataset.name+":u", oFileDataset.name+"=ds_out", "", "_gfn_callbackDeleteFile"); 
        }

        this._gfn_callbackDeleteFile = function(strSvcID,nErrorCode,strErrorMag)
        {
            if(nErrorCode < 0)
            {
                this.gfn_alert("파일삭제중 오류가 발생하였습니다.\n관리자에게 문의하세요.");
                this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "delete_file", false);
                return;
            }
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            var sRtn = oFileDataset.saveXML();
            this._FORM_FILE_OBJ.callback.call(this._FORM_FILE_OBJ.form, "delete_file", sRtn);
        }

        /**
         * @class 파일다운로드
         * @param dsObj : 데이타셋 object
         * @return None
         */ 
        this.gfn_downloadFile = function(dsObj)
        {
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            //단건인경우
            if(Eco.isEmpty(dsObj))
            {
                var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                if(!Eco.isEmpty(sFileId))
                {
                    oFileDataset.setColumn(0, "CHK", '1');
                }
            }
            //다건인경우
            else
            {
                var nChkCnt = dsObj.getCaseCount("CHK=='1'");
                if(nChkCnt < 1){
                    this.gfn_alert("파일을 선택하세요");
                    return;
                }
                for(var i = 0; i < dsObj.rowcount; i++)
                {
                    if(dsObj.getColumn(i, "CHK") == '1')
                    {
                        oFileDataset.setColumn(i, "CHK", '1');
                    }
                    else
                    {
                        oFileDataset.setColumn(i, "CHK", '0');
                    }
                }
            }
            
            var sFileId;
            var sFileSeq;
            var sFileName;
            var oFileDownload = this._FORM_FILE_OBJ.filedownload;
            
            for(var i = 0; i<oFileDataset.getRowCount(); i++)
            {
                if(oFileDataset.getColumn(i,"CHK")==1)
                {
                    sFileName = oFileDataset.getColumn(i, "FILE_NAME");
                    sFileId = oFileDataset.getColumn(i, "FILE_ID");
                    sFileSeq = oFileDataset.getColumn(i, "SEQ");

                    oFileDownload.set_downloadfilename(sFileName);
                    if(!this._FORM_FILE_OBJ.binary)
                    {
                        oFileDownload.set_downloadurl("svc::DownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq);
                    }
                    else
                    {
                        oFileDownload.set_downloadurl("svc::BinaryDownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq);
                    }
                    try{
                        var bSucc = oFileDownload.download();
                    } catch (e){
                        trace(e);
                        this.gfn_alert("파일다운로드에 실패하였습니다.");
                    }
                }
            }    
        }

        /**
         * @class 단건파일다운로드
         * @param sFileId : 파일ID
         * @param sFileSeq : 파일SEQ(기본값 1)
         * @param sFileName : 파일명
         * @return None
         */ 
        this.gfn_downloadFileSingle = function(sFileId,sFileSeq,sFileName)
        {
            if(Eco.isEmpty(sFileId)) return false;
            if(Eco.isEmpty(sFileSeq)) sFileSeq = '1';
            if(Eco.isEmpty(sFileName)) sFileName = Eco.date.getMaskFormatString(new Date(), "yyyyMMddmmss");

            var oFileDownload = this._FORM_FILE_OBJ.filedownload;
            
            oFileDownload.set_downloadfilename(sFileName);
            
            if(!this._FORM_FILE_OBJ.binary)
            {
                oFileDownload.set_downloadurl("svc::DownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq);
            }
            else
            {
                oFileDownload.set_downloadurl("svc::BinaryDownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq);
            }
            try{
                var bSucc = oFileDownload.download();
            } catch (e){
                trace(e);
                this.gfn_alert("파일다운로드에 실패하였습니다.");
            }
        }

        this._gfn_fileDownloadOnError = function(obj,e)
        {
            trace("_gfn_fileDownloadOnError");
        }

        this._gfn_fileDownloadOnSuccess = function(obj,e)
        {
            trace("_gfn_fileDownloadOnSuccess");
        }

        
        /**
         * @class 파일실행
         * @param dsObj : 데이타셋 object
         * @return None
         */ 
        this.gfn_executeFile = function(dsObj)
        {
            var oFileDataset = this._FORM_FILE_OBJ.filedataset;
            //단건인경우
            if(Eco.isEmpty(dsObj))
            {
                var sFileId = oFileDataset.getColumn(0, "FILE_ID");
                if(!Eco.isEmpty(sFileId))
                {
                    oFileDataset.setColumn(0, "CHK", '1');
                }
            }
            //다건인경우
            else
            {
                var nChkCnt = dsObj.getCaseCount("CHK=='1'");
                if(nChkCnt < 1){
                    this.gfn_alert("파일을 선택하세요");
                    return;
                }
                for(var i = 0; i < dsObj.rowcount; i++)
                {
                    if(dsObj.getColumn(i, "CHK") == '1')
                    {
                        oFileDataset.setColumn(i, "CHK", '1');
                    }
                    else
                    {
                        oFileDataset.setColumn(i, "CHK", '0');
                    }
                }
            }
            
            var sFileId;
            var sFileSeq;    
            for(var i = 0; i<oFileDataset.getRowCount(); i++)
            {
                if(oFileDataset.getColumn(i,"CHK")==1)
                {
                    sFileId = oFileDataset.getColumn(i, "FILE_ID");
                    sFileSeq = oFileDataset.getColumn(i, "SEQ");            
                    
                    try{
                        if(!this._FORM_FILE_OBJ.binary){
                            system.execBrowser(encodeURI(application.services["svc"].url + "DownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq));
                        }else{
                            system.execBrowser(encodeURI(application.services["svc"].url + "BinaryDownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq));
                        }
                    } catch (e){
                        trace(e);
                        this.gfn_alert("파일다운로드에 실패하였습니다.");
                    }            
                }
            }    
        }

        
        /**
         * @class 단건파일실행
         * @param sFileId : 파일ID
         * @param sFileSeq : 파일SEQ(기본값 1)
         * @return None
         */ 
        this.gfn_executeFileSingle = function(sFileId,sFileSeq)
        {
            if(Eco.isEmpty(sFileId)) return false;
            if(Eco.isEmpty(sFileSeq)) sFileSeq = '1';    
            
            try {
                if(!this._FORM_FILE_OBJ.binary){
                    system.execBrowser(encodeURI(application.services["svc"].url + "DownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq));
                }else{
                    system.execBrowser(encodeURI(application.services["svc"].url + "BinaryDownloadServlet?PATH="+this._FORM_FILE_OBJ.uploadpath+"&FILE_ID="+sFileId+"&SEQ="+sFileSeq));
                }
            } catch (e){
                trace(e);
                this.gfn_alert("파일다운로드에 실패하였습니다.");
            }
        }

        /**
         * @class dataset과 매핑된 item_index 가 없는 건 upload item에서 제거 처리
                  IE 에서 파일 다이얼로그에서 [취소] 선택 시 ITEM 만 추가되고 파일은 없으므로 껍데기만 있게됨
         * @param sFileId : 파일ID
         * @param sFileSeq : 파일SEQ(기본값 1)
         * @return None
         */ 
        this.gfn_delectEmptyUploadItem = function(){

            for(var jnx = this._FORM_FILE_OBJ.fileupload.getItemCount()-1 ; jnx >= 0  ;jnx--){
                if(this._FORM_FILE_OBJ.filedataset.findRow("ITEM_INDEX",jnx) < 0){
                    this._FORM_FILE_OBJ.fileupload.deleteItem(jnx);
                }
            }
        }

        //라니안
        this.gfn_excelDownload = function(dataSetObject){
        		application.set_filesecurelevel("all");
        		var fileFullName = dataSetObject.getColumn(0, "fileFullName");
        		var fullsUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/download/"+fileFullName;
        		this.parent.fdw_excelFile.set_downloadfilename(fileFullName);
        		var bSucc = this.parent.fdw_excelFile.download(fullsUrl);
        }

        
        this.gfn_ubiReport = function(){
        	//리소스 URL
        	this.ubi_report.resource = "http://192.168.0.2:8080/wms/ubireport/ajax/js4";
        	//리포트 통신담당할 URL
        	this.ubi_report.gatewayurl = "http://192.168.0.2:8080/wms/UbiGateway";
        	this.ubi_report.resid = "UBIAJAX";
        	this.ubi_report.dataurl = "";
        	this.ubi_report.fileurl = "/wms/ubireport/work/";
        	this.ubi_report.jrffile = "itemcodelabel.jrf";
        	
        	this.ubi_report.setDataset("SQL", this.ds_ubiReport);
        	this.ubi_report.retrieve(this, "retrieveEnd");
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
