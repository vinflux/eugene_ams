﻿//XJS=Constant.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name         : Constant.xjs                                                                                                          
        * Title        : 상수 선언 부                                                                            
        * @desc        :                                                                                                                       
        * 작성자      : 김현철
        * 작성일      : 2018-11-28                                                                                                                           
        * 변경사항    : 기존에 없었으나 login 오류시 message 처리를 위해 ams > lib > Constant 를 ams_mobile > lib 로 copy/paste 하고 다른 상수 모두 제거함
        ********************************************************************************/ 
        /*********************************************************
        	Common 
        **********************************************************/
        this.MESSAGE = "";	
        this.MESSAGE_CODE = "";

        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
