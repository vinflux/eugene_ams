﻿//XJS=CommForm.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************
        * Name       : CmmnForm.xjs
        * @desc      : FORM 관련  및 시스템 공통 함추
        * 작성자    : heobg
        * 작성일    : 2016-05-01
        * 변경사항  :
        ********************************************************************************/ 

        /**
         * 공통 popup
         * @param    : sPopupId - 팝업 ID
         * @param    : sUrl - 팝업 Url
         * @param    : oArg - 팝업 parameter
         * @param    : nWidth - Popup 가로 사이즈
         * @param    : nHeight - Popup 세로 사이즈
         * @param    : sPopupCallback - Modal 일경우 callback 함수
         * @param    : bModeless - Modaless 여부 (Default : false)
         * @return    : N/A  
         */

        this.gfn_popup = function(sPopupId,sUrl,oArg,nWidth,nHeight,sPopupCallback,bModeless)
        {
            var nLeft = -1;
            var nTop = -1;
            var bShowTitle = false;
            var bShowStatus = false;
            var bLayered = true;
            var nOpacity = 100;
            var bAutoSize = false;
            var bResizable = false;
            var bRound = false;
            
            if(this.gfn_isNull(bModeless)) bModeless = false;
            if(this.gfn_isNull(sPopupCallback)) sPopupCallback = "fn_popupAfter";
            var oPopup = application.popupframes[sPopupId];

            if(oPopup != null) {
                trace(sPopupId + " === Popup ID 중복 ");
                oPopup.setFocus();
                return;
            }
            
            var sTitleText = "";
            var sOpenalign = "";
            
            if (nLeft == -1 && nTop == -1) {
                sOpenalign = "center middle";
                nLeft = (application.mainframe.width / 2) - Math.round(nWidth / 2);
                nTop  = (application.mainframe.height / 2) - Math.round(nHeight / 2);
            }
            
            var objParentFrame = this.getOwnerFrame();
            
            if(bModeless == true) {
                var sOpenStyle= "showtitlebar=false showstatusbar=false";

        		if (this.gfn_isNull(oArg)) oArg = new Object();
        		if (this.gfn_isNull(oArg.arguments)) oArg.arguments = new Object();
        		oArg.arguments["ispopup"] = true;
        		oArg.arguments["popupid"] = sPopupId;
        		oArg.arguments["popupCallback"] = sPopupCallback;
        		oArg.arguments["isModeless"] = true;
        		if (!this.gfn_isNull(oArg)) oArg.arguments["args"] = oArg;

                application.open(sPopupId, sUrl, objParentFrame, oArg, sOpenStyle, nLeft, nTop, nWidth, nHeight, this);
            } else {
                newChild = new nexacro.ChildFrame;
                newChild.init(sPopupId,"absolute", nLeft, nTop, nWidth, nHeight, null, null, sUrl);
        //        newChild.style.set_overlaycolor("#ffffff65");
                newChild.set_dragmovetype("all");
                newChild.set_showtitlebar(bShowTitle);
                newChild.set_autosize(bAutoSize);
                newChild.set_resizable(bResizable);
                newChild.set_titletext(sTitleText);
                newChild.set_showstatusbar(bShowStatus);
                newChild.set_openalign(sOpenalign);

        		newChild.arguments = new Object();
        		newChild.arguments["popupid"] = sPopupId;
        		newChild.arguments["ispopup"] = true;
        		if (!this.gfn_isNull(oArg)) newChild.arguments["args"] = oArg;
        		newChild.arguments["popupCallback"] = sPopupCallback;
        		newChild.arguments["isModeless"] = false;

                newChild.showModal(objParentFrame, oArg, this, "_gfn_popupAfter", true);
            }
        }

        /**
         * 화면에서 debug 창 호출
         * @return    : N/A  
         */
        this.gfn_showDegug = function()
        {
            this.gfn_popup("popup_debug", "comm::CommDebugPop.xfdl", null, 800, 600, "");
        }

        /**
         * 사용자 정보 찾기
         * @param    : sUsrInfoId - 사용자정보 컬럼 아이디
         * @return    : String
         */
        this.gfn_getUserInfo = function(sUsrInfoId)
        {
            if(this.gfn_isNull(sUsrInfoId)) return "";
            
            var sRtnVal = "";
            var oCol = application.gds_userInfo.getColumnInfo(sUsrInfoId);
            if(oCol != null) {
                sRtnVal = application.gds_userInfo.getColumn(0, sUsrInfoId);
            }
            
            return sRtnVal;
        }

        /**
         * 사용자 정보 찾기
         * @param    : sUsrAuthId - 사용자정보 컬럼 아이디
         * @return    : String
         */
        this.gfn_getUserAuth = function(sUsrAuthId)
        {
            if(this.gfn_isNull(sUsrAuthId)) return "";
            
            var sRtnVal = "";
            var oCol = application.gds_userAuth.getColumnInfo(sUsrAuthId);
            if(oCol != null) {
                sRtnVal = application.gds_userAuth.getColumn(0, sUsrAuthId);
            }
            
            return sRtnVal;
        }

        /**
         * Message 찾기(값만 반환)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : String
         */
        this.gfn_getMsgVal = function(sMsgCd)
        {
            var nRow = application.gds_msg.findRow("MSG_CD", sMsgCd);
            if(nRow >= 0) {
                return this.gfn_isNullEmpty(application.gds_msg.getColumn(nRow, "MSG_TXT"));
            } else {
                return "";
            }
        }

        /**
         * Message 찾기(Array 반환)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : Array
         */
        this.gfn_getMsgToArray = function(sMsgCd)
        {
            var nRow = application.gds_msg.findRow("mulaapmsg_hdkey", sMsgCd);
            if(nRow >= 0) {
                return this.gfn_getRowDataToArray(application.gds_msg, nRow);
            } else {
                return "";
            }
        }

        /* 메시지 변수 치환 함수*/
        this.gfn_getTransMsg = function(sMsg,sTrans)
        {
            sMsg = this.gfn_replaceAll(sMsg, "\\n", String.fromCharCode(10) + String.fromCharCode(13));
            var mulLang = sMsg.split("}");
            
            for(var i = 0 ; i < mulLang.length ; i++){
        		sMsg = this.gfn_replaceAll(sMsg, "#{" + mulLang[i].substr(mulLang[i].indexOf("#{")+2) + "}", application.gds_lang.getColumn(0, mulLang[i].substr(mulLang[i].indexOf("#{")+2)));
            }
            
            if(this.gfn_isNull(sTrans)) {
                return sMsg;
            }
            
            var aTrans = sTrans.split("|");
            for(var i=0; i<aTrans.length; i++) {
                sMsg = this.gfn_replaceAll(sMsg, "{" + i + "}", aTrans[i]);
            }
            
            return sMsg;
        }

        /**
         * 공통 Alert 메시지 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @return    : N/A
         */
        this.gfn_alert = function(sMsgCd,sTrans,sCallback)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans};
            this.gfn_popup("popup_" + sMsgCd, "comm::Alert.xfdl", oArg, 380, 208, sCallback);
        }

        /**
         * 공통 Error Alert 메시지 팝업 호출 ( 코드로 정의되지 못하는 DB 오류 표시할 메시지 예외처리)
         * @param    : sMsgCd - MSG_CD 값
         * @return    : N/A
         */
        this.gfn_errAlert = function(sMsgCd,sTrans,sCallback)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans};
            this.gfn_popup("popup_" + sMsgCd, "comm::Error.xfdl", oArg, 380, 194, sCallback);
        }

        /**
         * 공통 Confirm 메시지 팝업 호출
         * @param    : sMsgCd - MSG_CD 값
         * @param    : sCallback - 공통 메시지 창 호출후 결과를 받을 함수명 (Default : fn_confirmAfter)
         * @return    : N/A
         */
        this.gfn_confirm = function(sMsgCd,sTrans,sCallback)
        {
            var oArg = {argMsgCd:sMsgCd, argTrans:sTrans};
            this.gfn_popup("popup_" + sMsgCd, "comm::Confirm.xfdl", oArg, 380, 208, sCallback);
        }

        // 스플릿 시작

        this.gv_split = ""; // 스플릿 변수
        this.splitBtn; // 스플릿 사용 변수

        /**
         * @class gfn_split
         * @param form - form
                  splitButton - 스플릿 버튼 배열
         * @return None
         */
        this.gfn_split = function(form,splitButton)
        {
        	form.addEventHandler("ondrag"    , this._gfn_ondrag    , this);
        	form.addEventHandler("ondragmove", this._gfn_ondragmove, this);
        	form.addEventHandler("ondrop"    , this._gfn_ondrop    , this);
        	
        	this.splitBtn = splitButton;
        }

        /* _gfn_ondrag 실행 */
        this._gfn_ondrag = function(obj,e)
        {
        	var splitBtnObj;
        	var splitVariable;
        	
        	for(var i = 0 ; i < this.splitBtn.length ; i++){
        		splitVariable = this.splitBtn[i].split("|");
        		splitBtnObj = eval(splitVariable[3]);
        		
        		if(e.fromreferenceobject == splitBtnObj){
        			this.gv_split = "in_" + splitBtnObj.name;
        			return true;
        		}
        	}
        }

        /* _gfn_ondragmove 실행 */
        this._gfn_ondragmove = function(obj,e)
        {
        	var splitBtnObj;
        	var splitVariable;
        	var sSplit = "";
        	var divObj = "";
        	
        	for(var i = 0 ; i < this.splitBtn.length ; i++){
        		splitVariable = this.splitBtn[i].split("|");
        		splitBtnObj = eval(splitVariable[3]);
        		sSplit = "in_" + splitBtnObj.name;
        		
        		if(this.gv_split == sSplit && splitVariable[2] == "H"){
        			this.gfn_splitControlH(eval(splitVariable[0]), eval(splitVariable[1]), splitBtnObj, e);
        			return;
        		}else if(this.gv_split == sSplit && splitVariable[2] == "W"){
        			this.gfn_splitControlW(eval(splitVariable[0]), eval(splitVariable[1]), splitBtnObj, e);
        			return;
        		}else if(this.gv_split == ""){
        			return;
        		}
        	}
        }

        /* _gfn_ondrop 실행 */
        this._gfn_ondrop = function(obj,e)
        {
        	this.gv_split = "";
        	this.style.set_cursor("");
        }

        /**
         * @class split 세로
         * @param div01
                  div02
                  btn_split - button
                  e - 이벤트
         * @return None
         */  
        this.gfn_splitControlH = function(div01,div02,btn_split,e)
        {
        	var searchMargin = 0;
        	var splitSize = 8;
        	var workSpace = 10;
        	
        	if(this.parent.gv_searchValue == "L") searchMargin = 10;
        	
        	var objHeight = this.getOffsetHeight();
        	var iTop = objHeight - (nexacro.toNumber(div01.top) + 95 + searchMargin);
        	var iBottom = nexacro.toNumber(div02.bottom) + 80;
        	
        	this.style.set_cursor("n-resize");
        	
        	if((objHeight - e.clientY) < iBottom){
        		btn_split.set_bottom(iBottom);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace);
        	}else if((objHeight - e.clientY) > iTop){
        		btn_split.set_bottom(iTop);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace);
         	}else{
        		btn_split.set_bottom(objHeight - e.clientY);
        		div01.set_bottom(btn_split.bottom + splitSize);
        		div02.set_height(btn_split.bottom - workSpace);
        	}
        }

        /**
         * @class split 가로
         * @param div01
                  div02
                  btn_split - button
                  e - 이벤트
         * @return None
         */  
        this.gfn_splitControlW = function(div01,div02,btn_split,e)
        {
        	var searchMargin = 0;
        	var iRight = nexacro.toNumber(div02.right);
        	var objWidth = (this.getOffsetWidth() - iRight);
        	var iLeft = nexacro.toNumber(div01.left);
        	
        	this.style.set_cursor("e-resize");
        	
        	if(e.clientX < iLeft){
        		btn_split.set_left(iLeft);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + searchMargin);
        	}else if(e.clientX > objWidth){
        		btn_split.set_left(objWidth);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + searchMargin);
        	}else{
        		btn_split.set_left(e.clientX);
        		div01.set_width(btn_split.left - div01.left);
        		div02.set_left(btn_split.left + searchMargin);
        	}
        }

        // 스플릿 끝

        /**
         * @class 테마 설정
         * @param strTema - 테마 flag
         * @return None
         */  
        this.gfn_tema = function(strTema)
        {
        	var sTema = "";
        	
            if(this.gfn_isNull(strTema)) sTema = "";
            else sTema = strTema;
        	
        	application.gv_temaAms = sTema;
        	
        	// 수정해야 할 부분
        	application.gv_TopFrame.form.sta_back.set_cssclass("sta_"+sTema+"TF_back");
        	application.gv_TopFrame.form.sta_moduleName.set_cssclass("sta_"+sTema+"TF_module");
        	application.gv_TopFrame.form.btn_menu_all.set_cssclass("btn_"+sTema+"TF_Tmenu2");
        	application.gv_TopFrame.form.btn_menu_my.set_cssclass("btn_"+sTema+"TF_Tfavorite");
        	application.gv_LeftFrame.form.sta_back.set_cssclass("sta_"+sTema+"LF_back");
        }

        this.gfn_ExButtonPop = function(obj,datasetObj)
        {
        	obj.parent.popDiv_ExButton.grd_ExButton.setBindDataset(datasetObj);
        	
        	if(!obj.parent.popDiv_ExButton.isPopup()){

        		var v_nX = system.clientToScreenX(obj.div_splitTop.btn_excel, 0) - obj.parent.popDiv_ExButton.width;
        		var v_nY = system.clientToScreenY(obj.div_splitTop.btn_excel, parseInt(obj.div_splitTop.btn_excel.height)) - system.clientToScreenY(application.mainframe, 0) ;

        		obj.parent.popDiv_ExButton.set_left(v_nX);
        		obj.parent.popDiv_ExButton.set_top(v_nY);
        		obj.parent.popDiv_ExButton.set_visible(true);
        		obj.parent.popDiv_ExButton.trackPopup(v_nX, v_nY);	
        		//trace("v_nX : "+v_nX+", v_nY : "+v_nY);
        	}
        	
        	var gridItemHeight = 24;
        	var gridGap = 5;
        	if(datasetObj.getRowCount() > 0){
        		obj.parent.popDiv_ExButton.set_height(datasetObj.getRowCount()*gridItemHeight+gridGap);
        		//for(var i=0 ; i < datasetObj.getRowCount() ; i++){
        		//obj.parent.popDiv_ExButton.grd_ExButton.addEventHandler("oncellclick", this.exButton_oncellclick, this);	
        		//}
        	}
        }

        // this.exButton_oncellclick = function(obj, datasetObj){
        // 	var index = obj.parent.popDiv_ExButton.grd_ExButton.getSelectedRows(); 
        // 	trace(index);
        // 	var callbackFun = datasetObj.getColumn(index, "callback");
        // 	eval("obj."+callbackFun);
        // }

        
        /**
         * 화면이동 공통 함수
         * @param    : objParam.uskey - 화면 ID
         * @param    : objParam.nxpath - 화면 Url
         * @param    : objParam.mename - 화면 명
         * @param    : objParam.args - Arguments
         * @return    : N/A
         */

        this.gfn_goPage = function(objParam,bBack)
        {
        	var bUrl = false;
        	var bClickBackBtn = false;
        	var objCFMain = application.gv_CF_MAIN;
        	var objFormMain = objCFMain.form;

        	if (this.gfn_isNull(objCFMain)) {
        		trace("No Main Frame...");
        		return;
        	}

        	if (this.gfn_isNull(objFormMain)) {
        		trace("No Main Form...");
        		return;
        	}

        	if (!this.gfn_isNull(objParam.nxpath)) bUrl = true;

        	if (this.gfn_isNull(objParam.uskey) && this.gfn_isNull(objParam.nxpath)) {
        		trace("No uskey...");
        		return;
        	}

        	if (!this.gfn_isNull(bBack)) bClickBackBtn = bBack;

        	var nFindRow = application.gds_menu.findRowExpr("uskey == '" + objParam.uskey + "'");
        	var sNxPath = application.gds_menu.getColumn(nFindRow, "nxpath");
        	var nWinSeq = 0;

        	if (objParam.uskey == "MAIN000000") sNxPath = "main::MainMenu.xfdl";

        	if (this.gfn_isNull(objParam.uskey) && !this.gfn_isNull(objParam.nxpath) && !this.gfn_isNull(this.fv_mainCF)) objParam.uskey = this.fv_mainCF.arguments.uskey;

         	var nMaxWinSeq = application.gds_openMenu.getCaseMax("WINID == 'WIN_" + objParam.uskey + "'", "winseq");

        	if (objParam.uskey == "MAIN000000") nMaxWinSeq = undefined;

        	if (nMaxWinSeq == 0) {//trace("nMaxWinSeq : zero");
        		var nFindWinIdRow = application.gds_openMenu.findRowExpr("WINID == 'WIN_" + objParam.uskey + "'");

        		if (bUrl) {
        			var nRowCnt = application.gds_openMenu.getRowCount() - 1;

        			for (var i = nRowCnt; i > nFindWinIdRow; i--) {
        				if (i == 0) break;
        				application.gds_openMenu.deleteRow(i);
        			}

        			nWinSeq = 1;
        		} else {
        			application.gds_openMenu.deleteRow(nFindWinIdRow);
        			nWinSeq = 0;
        		}

        	} else if (this.gfn_isNull(nMaxWinSeq)) {//trace("nMaxWinSeq : null");
        		nWinSeq = 0;
        	} else if (nMaxWinSeq > 0) {//trace("nMaxWinSeq (other value) : " + nMaxWinSeq);
        		if (bUrl) {
        			var sFindStr = "uskey == '" + objParam.uskey + "' && nxpath == '" + objParam.nxpath + "'";
        			var nFindDupWinRow = application.gds_openMenu.findRowExpr(sFindStr);

        			if (nFindDupWinRow >= 0) {
        				nWinSeq = Number(application.gds_openMenu.getColumn(nFindDupWinRow, "winseq"));
        				application.gds_openMenu.deleteRow(nFindDupWinRow);
        			} else {
        				nWinSeq = nMaxWinSeq + 1;
        			}
        		} else {
        			var nFindWinIdRow = application.gds_openMenu.findRowExpr("WINID == 'WIN_" + objParam.uskey + "'");
        			var nRowCnt = application.gds_openMenu.getRowCount() - 1;

        			for (var i = nRowCnt; i >= nFindWinIdRow; i--) {
        				if (i == 0) break;
        				application.gds_openMenu.deleteRow(i);
        			}

        			nWinSeq = 0;
        		}
        	}

        	objCFMain.arguments = new Object();

        	var sApKey = application.gds_menu.getColumn(nFindRow, "apkey");
        	var sIsleaf = application.gds_menu.getColumn(nFindRow, "isleaf");
        	var sUsname = application.gds_menu.getColumn(nFindRow, "usname");
        	var sMename = application.gds_menu.getColumn(nFindRow, "mename");
        	var sMelvl = application.gds_menu.getColumn(nFindRow, "melvl");
        	var sUppermekey = application.gds_menu.getColumn(nFindRow, "uppermekey");
        	var sMekey = application.gds_menu.getColumn(nFindRow, "mekey");

        	if (bUrl) {
        		sMename = objParam.mename;
        		sNxPath = objParam.nxpath;
        	}

        	if (this.gfn_isNull(sNxPath) && nFindRow >= 0) {
        		var sUspath = application.gds_menu.getColumn(nFindRow, "uspath");

        		if (!this.gfn_isNull(sUspath)) {
        			sNxPath = String(sUspath).replace(".", "/") + ".xfdl";
        		} else {
        			trace("nxpath(" + sNxPath + ") is null for uskey[" + objParam.uskey + "]");
        			return;
        		}
        	}

        	//mename_eng

        	sNxPath = String(sNxPath).replace("/", "::");

        	if (objParam.uskey == "MAIN000000") {
        		sNxPath = "main::MainMenu.xfdl";
        		sIsleaf = "N";
        		sMename = "EUGENE Mobile";//모바일 상단 타이틀
        		sMelvl = "0";

        		for (var i = 0; i < application.gds_openMenu.getRowCount(); i++) {
        			var sClearWinKey = application.gds_openMenu.getColumn(i, "WINID");
        			if (!this.gfn_isNull(sClearWinKey)) this._gfn_clearArgs(sClearWinKey);
        		}
        	}

        	if (bBack) {
        		objCFMain.arguments["WINID"] = objParam.WINID;
        		nWinSeq = objParam.winseq;
        	}

        	objCFMain.arguments["uskey"] = objParam.uskey;
        	objCFMain.arguments["nxpath"] = sNxPath;
        	objCFMain.arguments["apkey"] = sApKey;
        	objCFMain.arguments["isleaf"] = sIsleaf;
        	objCFMain.arguments["usname"] = sUsname;
        	objCFMain.arguments["mename"] = sMename;
        	objCFMain.arguments["melvl"] = sMelvl;
        	objCFMain.arguments["uppermekey"] = sUppermekey;
        	objCFMain.arguments["winseq"] = nWinSeq;
        	objCFMain.arguments["bBack"] = bBack;
        	objCFMain.arguments["ispopup"] = false;

        	var sWINID = this.fn_setOpenWinData(nFindRow, objCFMain.arguments, bUrl);
        	objCFMain.arguments["WINID"] = sWINID;

        	if (!this.gfn_isNull(objParam.args)) this._gfn_setArg(objParam.args);

        	objFormMain.go("frame::Work.xfdl");
        }

        this.fn_setOpenWinData = function (nFindRow,objArgs,bUrl)
        {
        	var sUrlYn = "N";
        	var sUskey       = objArgs.uskey;
        	var sWINID       = "";
        	var sEqtype      = application.gds_menu.getColumn(nFindRow, "eqtype");
        	var sIsleaf      = application.gds_menu.getColumn(nFindRow, "isleaf");
        	var sIsseparator = application.gds_menu.getColumn(nFindRow, "isseparator");
        	var sMekey       = application.gds_menu.getColumn(nFindRow, "mekey");
        	var sMename      = objArgs.mename;
        	var sUsname      = application.gds_menu.getColumn(nFindRow, "usname");
        	var sMeorder     = application.gds_menu.getColumn(nFindRow, "meorder");
        	var sUppermekey  = application.gds_menu.getColumn(nFindRow, "uppermekey");
        	var sUspath      = application.gds_menu.getColumn(nFindRow, "uspath");
        	var sApkey       = application.gds_menu.getColumn(nFindRow, "apkey");
        	var sMelvl       = application.gds_menu.getColumn(nFindRow, "melvl");
        	var sNxpath      = objArgs.nxpath;
        	var nWinSeq      = objArgs.winseq;

        	if (sNxpath == "main::MainMenu.xfdl") {
        		sEqtype      = "30";
        		sIsleaf      = "N";
        		sIsseparator = "N";
        		sMelvl       = "0";
        	}

        	var nAddedRow = application.gds_openMenu.addRow();

        	if (objArgs.bBack) {
        		sWINID = objArgs.WINID;
        	} else {
        		sWINID = "WIN_" + sUskey;
        	}

        	if (bUrl) sUrlYn = "Y";

        	application.gds_openMenu.setColumn(nAddedRow, "WINID", sWINID);
        	application.gds_openMenu.setColumn(nAddedRow, "winseq", nWinSeq);
        	application.gds_openMenu.setColumn(nAddedRow, "eqtype", sEqtype);
        	application.gds_openMenu.setColumn(nAddedRow, "isleaf", sIsleaf);
        	application.gds_openMenu.setColumn(nAddedRow, "isseparator", sIsseparator);
        	application.gds_openMenu.setColumn(nAddedRow, "mekey", sMekey);
        	application.gds_openMenu.setColumn(nAddedRow, "mename", sMename);
        	application.gds_openMenu.setColumn(nAddedRow, "usname", sUsname);
        	application.gds_openMenu.setColumn(nAddedRow, "meorder", sMeorder);
        	application.gds_openMenu.setColumn(nAddedRow, "uppermekey", sUppermekey);
        	application.gds_openMenu.setColumn(nAddedRow, "uskey", sUskey);
        	application.gds_openMenu.setColumn(nAddedRow, "uspath", sUspath);
        	application.gds_openMenu.setColumn(nAddedRow, "apkey", sApkey);
        	application.gds_openMenu.setColumn(nAddedRow, "melvl", sMelvl);
        	application.gds_openMenu.setColumn(nAddedRow, "nxpath", sNxpath);
        	application.gds_openMenu.setColumn(nAddedRow, "urlopen", sUrlYn);

        	return sWINID;
        }

        this.gfn_init = function (obj)
        {
        	if (application.gv_system == "TMS") this.fv_useSrchCondPop = false;
        	this.fv_objForm = obj;
        	this.fv_mainCF = obj.getOwnerFrame();

        	var bAtLeastSelOne = false;

        	if (!this.gfn_isNull(obj.srchPopAtLstSel)) bAtLeastSelOne = (obj.srchPopAtLstSel == "Y")?true:false;

        	var objArguments = this.fv_mainCF.arguments;

        	if (!this.gfn_isNull(objArguments)) {
        		if (objArguments.ispopup) {
        			obj.arguments = objArguments.args;
        		} else {
        			this.fv_workForm = this.fv_mainCF.form;
        			var objArgs = this._gfn_getArgs(obj);
        			obj.arguments = objArgs;
        		}
        	}

        // 	var objFormObjects = obj.objects;
        // 
        // 	for (var i = 0; i < objFormObjects.length; i++) {
        // 		if (objFormObjects[i] instanceof Dataset) {
        // 			var nConstColCnt = objFormObjects[i].getConstCount();
        // 
        // 			if (nConstColCnt >= 0) {
        // 				for (var j = 0 ; j < nConstColCnt; j++) {
        // 					var sKey = objFormObjects[i].getConstColID(j);
        // 					var sValue = this.gfn_getLang(sKey);
        // 					objFormObjects[i].setConstColumn(sKey, sValue);
        // 				}
        // 			}
        // 		}
        // 	}

        	var objSearchEdit = obj.edt_searchText;

        	if (!this.gfn_isNull(objSearchEdit) && String(objSearchEdit.displaynulltext).length > 0) {
        		var arrNullText = String(objSearchEdit.displaynulltext).split("+");
        		var sDisplayNullText = "";
        		for (var i = 0; i < arrNullText.length; i++) {
        			var sValue = this.gfn_getLang(arrNullText[i]);
        			if (i == 0) {
        				sDisplayNullText += sValue;
        			} else {
        				sDisplayNullText += " + " + sValue;
        			}
        		}

        		objSearchEdit.set_displaynulltext(sDisplayNullText);
        	}

        	this.gfn_addTranColumn(application.gds_param, "pagingLimit");
        	application.gds_param.setColumn(0, "pagingLimit", this.fv_pageLimit);

        	this.gfn_addTranColumn(application.gds_param, "owkey");
        	application.gds_param.setColumn(0, "owkey", application.gds_userInfo.getColumn(0, "owkeym"));

        // 	if (!this.gfn_isNull(this.fv_mainCF)) {
        // 		if (!this.gfn_isNull(objArguments)) {
        // 			var sWinId = this.fv_mainCF.arguments.WINID + "_" + this.fv_mainCF.arguments.winseq;
        // 			this._gfn_clearArgs(sWinId);
        // 		}
        // 	}

        	//if (!this.gfn_isNull(obj.ds_compList)) obj.ds_compList.clear();
        	if (!objArguments.ispopup) {
        		var objSearchPopFn = obj.fn_popupAfter;

        		if (!this.gfn_isNull(objSearchPopFn)) {
        			var sPopupId = "MobInbOrderTaskPop";

        			if (!this.gfn_isNull(objArguments.uskey) && objArguments.winseq == 0) {
        				application.gds_srchCondition.filter("uskey == '" + objArguments.uskey + "'");

        				if (!this.gfn_isNull(obj.ds_compList) && application.gds_srchCondition.getRowCount() > 0) {
        					if (!this.gfn_isNull(obj.ds_compList)) {
        						obj.ds_compList.clear();
        						obj.ds_compList.copyData(application.gds_srchCondition, true);
        					}
        				}
        			}

        			this.fv_workForm.fn_setCondVisiboe(false);

        			if (this.gfn_isNull(objArguments.bBack)) {
        				if (this.fv_useSrchCondPop) {
        					if (!this.gfn_isNull(obj.ds_compList)) {
        						if (application.gv_activeApp != "TMS") this.fv_workForm.fn_setCondVisiboe(true);
        						obj.gfn_popupSearchCond.call(obj, sPopupId, obj.ds_compList, bAtLeastSelOne);
        					}
        				} else {
        					objSearchPopFn.call(obj, "onload");
        				}

        			} else {
        				if (objArguments.bBack) {
        					if (this.fv_useSrchCondPop) {
        						var objFormHidden = application.gv_CF_HIDDEN.form;
        						var sWinId = objArguments.WINID + "_" + objArguments.winseq + "_MobInbOrderTaskPop";
        						var objRtns = objFormHidden.fn_getArgs(sWinId);

        						if (!this.gfn_isNull(objRtns)) {
        							if (objRtns.ispopup == true && objRtns.popupid == "MobInbOrderTaskPop") {
        								if (application.gv_activeApp != "TMS") this.fv_workForm.fn_setCondVisiboe(true);
        								objSearchPopFn.call(obj, objRtns.popupid, objRtns.rtnVal);
        							}
        						}
        					} else {
        						objSearchPopFn.call(obj, "onload");
        					}
        				} else {
        					objSearchPopFn.call(obj, "onload");
        				}
        			}
        		}
        	}

        	var objNoMsgDs = obj.objects["ds_noDataMsg"];

        	if (this.gfn_isNull(objNoMsgDs)) {
        		objNoMsgDs = new Dataset;
        		objNoMsgDs.name = "ds_noDataMsg";
        		obj.addChild("ds_noDataMsg" , objNoMsgDs);
        	}

        	objNoMsgDs.clear();
        	objNoMsgDs.addColumn("MSG_NO_SEARCHDATA", "STRING", 255);
        /*
        		var arrMsg = this.gfn_getMsgToArray("MSG_SRCH_VALICHK_MANDATORY"); // 필수 검색조건의 입력이 누락 되었습니다.
        		arrMsg[1] = "필수 입력 데이터가 없습니다.";
        //MSG_10001 "필수 입력 데이터가 없습니다."
        		alert(arrMsg[1]);*/
        }

        this.gfn_setParamValue = function (sColNm,sValue)
        {
        	this.gfn_addTranColumn(application.gds_param, sColNm);
        	application.gds_param.setColumn(0, sColNm, sValue);
        }

        this._gfn_setLangInfo = function (objForm,sDsList)
        {
        	objForm.fv_langInfo = new Object();
        	var arrDsStr = String(sDsList).split(" ");
        	objForm.fv_langInfo["dsList"] = sDsList;

        	for (var i = 0; i < arrDsStr.length; i++) {
        		var sDsName = String(arrDsStr[i]).split("=")[0];
        		var objDs = objForm.objects[sDsName];

        		if (this.gfn_isNull(objDs)) continue;

        		if (objDs.getConstCount() > 0) objForm.fv_langInfo[sDsName] = new Object();

        		for (var j = 0; j < objDs.getConstCount(); j++) {
        			var sColNm = objDs.getConstColID(j);
        			var sColVal = objDs.getConstColumn(sColNm);

        			if (!this.gfn_isNull(sColVal)) {
        				objForm.fv_langInfo[sDsName][sColNm] = sColVal;
        			}
        		}
        	}
        }

        this._gfn_getLangInfo = function (objForm)
        {
        	var sDsList = objForm.fv_langInfo["dsList"];
        	var arrDsStr = String(sDsList).split(" ");

        	for (var i = 0; i < arrDsStr.length; i++) {
        		var sDsName = String(arrDsStr[i]).split("=")[0];
        		if (!this.gfn_isNull(sDsName)) {
        			var objDs = objForm.objects[sDsName];
        			var objColList = objForm.fv_langInfo[sDsName];

        			if (!this.gfn_isNull(objDs) && !this.gfn_isNull(objColList)) {
        				for (var sKey in objColList) {
        					var sValue = this.gfn_getLang(sKey);
        					var objColInfo = objDs.getColumnInfo(sKey);
        					if (this.gfn_isNull(objColInfo)) {
        						objDs.addConstColumn(sKey, sValue);
        					} else {
        						objDs.setConstColumn(sKey, sValue);
        					}
        				}
        			}
        		}
        	}
        }

        this.gfn_popupSearchCond = function (sPopupId,dsCompList,bAtLeastSelOne)
        {
        	if (this.gfn_isNull(sPopupId)) return;
        	if (this.gfn_isNull(dsCompList)) return;

        	var nGap = 7;
        	var nDefHeight = 199;
        	var nDefCompHeight = 50;
        	var sUrl = "cond::DynamicCondPop.xfdl";

        	var oArg = new Object();
        	var nWidth = 550;
        	var nHeight = nDefHeight + dsCompList.getRowCount() * nDefCompHeight + (dsCompList.getRowCount() * nGap);
        	var sPopupCallback = "";
        	var bModeless = false;

        	oArg.dsCompList = dsCompList;

        	if (!this.gfn_isNull(bAtLeastSelOne)) oArg.bAtLeastSelOne = bAtLeastSelOne;
        	if (!this.gfn_isNull(this.fv_objForm.ds_searchVal)) {
        		if (this.fv_objForm.ds_searchVal.getRowCount() >= 0) oArg.dsSearchVal = this.fv_objForm.ds_searchVal;
        	}

        	this.gfn_popup(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless);
        }

        //============================ Argument 처리 함수 시작 =================================
        this._gfn_setArg = function (objArgs)
        {
        	var sType = this.gfn_getObjectType(objArgs);
        	var sComptype = this.gfn_getCompType(objArgs);

        // trace("typeof(objArgs) : " + typeof(objArgs));
        // trace("======================= objArgs instanceof Object : " + (objArgs instanceof Object));
        // trace("======================= objArgs instanceof Dataset : " + (objArgs instanceof Dataset));
        // trace("======================= objArgs instanceof Array : " + (objArgs instanceof Array));
        // trace("======================= objArgs : " + objArgs);
        // trace("sType : " + sType);

        	var bPopup = false;

        	if (!this.gfn_isNull(objArgs.popupid)) bPopup = true;

        	var objForm = application.gv_CF_HIDDEN.form;
        	var objMainArgs = application.gv_CF_MAIN.arguments;

         	var objRtn = new Object();
        	var sWinId = objMainArgs.WINID + "_" + objMainArgs.winseq;
        	var objTarget = objArgs;

        	if (bPopup) {
        		sWinId += "_" + objArgs.popupid;
        		objTarget = objArgs.rtnVal;
        		sType = this.gfn_getObjectType(objTarget);
        		sComptype = this.gfn_getCompType(objTarget);
        	}

        	if (sType == "string") {
        		objRtn = objTarget;
        	} else if (sType == "Dataset") {
        		var sDsName = objTarget.id + "_" + sWinId;
        		objRtn = objForm.fn_addDs(sDsName, objTarget);
        	} else {
        		if (sType == "Object") {
        			if (this.gfn_isNull(sComptype)) {
        				for (keys in objTarget) {
        					if (this.gfn_getObjectType(objTarget[keys]) == "string") {
        						objRtn[keys] = objTarget[keys];
        					} else if (this.gfn_getObjectType(objTarget[keys]) == "Dataset") {
        						objRtn[keys] = objForm.fn_addDs(sWinId, objTarget[keys]);
        					} else if (this.gfn_getObjectType(objTarget[keys]) == "Array") {
        						objRtn[keys] = objTarget[keys];
        					} else {
        						objRtn[keys] = objTarget[keys];
        					}
        				}
        			} else {
        				objRtn = objTarget;
        			}
        		} else if (sType == "Array") {
        			objRtn = new Array();

        			for (var i = 0; i < objTarget.length; i++) {
        				objRtn[objRtn.length] = objTarget[i];
        			}
        		}
        	}

        
        	if (bPopup) {
        		var objRtnTmp = new Object();
        		if (!this.gfn_isNull(objRtn)) {
        			objRtnTmp.rtnVal = objRtn;
        			objRtnTmp.ispopup = objArgs.ispopup;
        			objRtnTmp.popupid = objArgs.popupid;
        			objRtnTmp.popupCallback = objArgs.popupCallback;
        			objRtnTmp.isModeless = objArgs.isModeless;

        			if (this.gfn_isNull(objArgs.rtnVal)) objRtnTmp.rtnVal = undefined;

        			objRtn = new Object();
        			objRtn = objRtnTmp;
        		}
        	}

        	objForm.fn_setArgs(sWinId, objRtn);

        	return sWinId;
        }

        this._gfn_clearArgs = function (sWinId)
        {
        	var objForm = application.gv_CF_HIDDEN.form;
        	objForm.fn_clearArgs(sWinId);
        }

        this._gfn_getArgs = function (objForm)
        {
        	var objFormHidden = application.gv_CF_HIDDEN.form;
        	var objFormMain = application.gv_CF_MAIN.form;
        	var objCF;
        	var objRtns = new Object();
        	var sWinId;
        	var bIsPopup = false;

        	if (objForm.ispopup) {
        		sWinId = objForm.winid;
        		bIsPopup = true;
        	}

        	if (objForm instanceof Form) {
        		objCF = objForm.fv_mainCF;
        		if (this.gfn_isNull(objCF.arguments)) return "";
        		sWinId = objCF.arguments.WINID + "_" + objCF.arguments.winseq;
        	}
        //trace("sWinId : " + sWinId);
        	objRtns = objFormHidden.fn_getArgs(sWinId);

        	var sObjType = this.gfn_getObjectType(objRtns);

        	if (sObjType == "Dataset") {
        		objRtns = objFormMain.fn_addDs(objRtns.id, objRtns);

        	} else if (sObjType == "string") {
        		var objRtn = new Object();
        		var arrArgs = String(objRtns).split("&");
        		for (var i = 0; i < arrArgs.length; i++) {
        			var arrArg = String(arrArgs[i]).split("=");
        			objRtn[arrArg[0]] = arrArg[1];
        		}

        		objRtns = objRtn;
        	} else if (sObjType == "Array") {
        	} else if (this.gfn_isNull(this.gfn_getCompType(objRtns))) {
        		for (keys in objRtns) {
        			var objRtn = objRtns[keys];
        			var sCompType = this.gfn_getCompType(objRtn);
        			var sObjectType = this.gfn_getObjectType(objRtn);

        			if (this.gfn_isNull(sCompType)) {
        				if (sObjectType == "Dataset") {
        					objRtn = objFormMain.fn_addDs(objRtn.id, objRtn);

        				} else if (sObjectType == "string") {
        					var arrArgs = String(objRtn).split("&");

        					if (String(objRtn).indexOf("&") >= 0) {
        						objRtn = new Object();
        						for (var i = 0; i < arrArgs.length; i++) {
        							var arrArg = String(arrArgs[i]).split("=");
        							objRtn[arrArg[0]] = arrArg[1];
        						}
        					}

        					objRtns[keys] = objRtn;
        				} else {
        					objRtns[keys] = objRtn;
        				}
        			}
        		}
        	}

        	if (bIsPopup && objRtns.popupid != "MobInbOrderTaskPop") this._gfn_clearArgs(sWinId);

        	return objRtns;
        }

        this.gfn_getObjectType = function (obj)
        {
        	var sType = "";

        	if (obj instanceof Array) {
        		sType = "Array";
        	} else if (obj instanceof Dataset) {
        		sType = "Dataset";
        	} else if (obj instanceof String) {
        		sType = "string";
        	} else if (obj instanceof Object) {
        		sType = "Object";
        	} else {
        		sType = typeof(obj);
        	}

        	return sType;
        }

        this.gfn_getCompType = function (obj)
        {
        	var sCompType = "";

        	if (obj instanceof Div         ) sCompType = "Div";
        	if (obj instanceof Button      ) sCompType = "Button";
        	if (obj instanceof PopupDiv    ) sCompType = "PopupDiv";
        	if (obj instanceof Combo       ) sCompType = "Combo";
        	if (obj instanceof CheckBox    ) sCompType = "CheckBox";
        	if (obj instanceof ListBox     ) sCompType = "ListBox";
        	if (obj instanceof Edit        ) sCompType = "Edit";
        	if (obj instanceof MaskEdit    ) sCompType = "MaskEdit";
        	if (obj instanceof TextArea    ) sCompType = "TextArea";
        	if (obj instanceof Menu        ) sCompType = "Menu";
        	if (obj instanceof Tab         ) sCompType = "Tab";
        	if (obj instanceof ImageViewer ) sCompType = "ImageViewer";
        	if (obj instanceof Radio       ) sCompType = "Radio";
        	if (obj instanceof Calendar    ) sCompType = "Calendar";
        	if (obj instanceof Static      ) sCompType = "Static";
        	if (obj instanceof Grid        ) sCompType = "Grid";
        	if (obj instanceof Spin        ) sCompType = "Spin";
        	if (obj instanceof PopupMenu   ) sCompType = "PopupMenu";
        	if (obj instanceof GroupBox    ) sCompType = "GroupBox";
        	if (obj instanceof ProgressBar ) sCompType = "ProgressBar";
        	if (obj instanceof Plugin      ) sCompType = "Plugin";
        	if (obj instanceof FileUpload  ) sCompType = "FileUpload";
        	if (obj instanceof FileDownload) sCompType = "FileDownload";
        	if (obj instanceof WebBrowser  ) sCompType = "WebBrowser";
        	if (obj instanceof FlashPlayer ) sCompType = "FlashPlayer";
        // 	if (obj instanceof ExportObject) sCompType = "ExportObject";
        // 	if (obj instanceof ImportObject) sCompType = "ImportObject";

        	return sCompType;
        }

        /**
        * 공통 팝업 callback
        * @param {string} popupid
        * @param {array} argument
        * @return 
        * @example
        * @memberOf comLib
        */
        this._gfn_popupAfter = function(sPopupId,objRtn)
        {
        	var bModeless = false;

        	if (!this.gfn_isNull(objRtn.isModeless)) if (objRtn.isModeless == true) bModeless = true;

        	var objArguments = objRtn;
        	var oFrame = this.getOwnerFrame();
        	var oForm = oFrame.form.div_work;

        	if (!bModeless || this.gfn_isNull(bModeless)) {
        		var objRtnArg = new Object();
        		objRtnArg["ispopup"] = true;
        		objRtnArg["winid"] = objRtn;
        		objRtnArg["isModeless"] = false;
        		objArguments = this._gfn_getArgs(objRtnArg);
        	}

        	var bIsPopup = objArguments.ispopup;
        	var sPopupId = objArguments.popupid;
        	var sPopupCallback = objArguments.popupCallback;
        	var bIsModeless = objArguments.isModeless;

        	if (this.gfn_isNull(oForm)) oForm = this;
        //	if (this.gfn_isNull(oForm)) oForm = oFrame.form;

         	var objFunction = oForm[sPopupCallback];
        	var rtnValues = objArguments.rtnVal;

        	if (!this.gfn_isNull(objFunction)) {
        		oForm.arguments = oFrame.arguments;
        		objFunction.call(oForm, sPopupId, objArguments.rtnVal);
        	}

        	if (bModeless) {
        		var objCF = application.popupframes[sPopupId];
        		if (!this.gfn_isNull(objCF)) {
        			if (!this.gfn_isNull(objCF.form)) objCF.form.close();
        		}
        	}
        }

        /**
        * 팝업창 창닫기 이벤트
        * @param {string}  창닫기시 보내질 아규먼트
        * @example N/A
        * @memberOf comLib
        */  
        this.gfn_popupClose = function(val)
        {
        	var objArguments = this.getOwnerFrame().arguments;
        	var bIsPopup = objArguments.ispopup;
        	var sPopupId = objArguments.popupid;
        	var sPopupCallback = objArguments.popupCallback;
        	var bIsModeless = objArguments.isModeless;

        	var objRtn = new Object();
        	objRtn["ispopup"] = bIsPopup;
        	objRtn["popupid"] = sPopupId;
        	objRtn["popupCallback"] = sPopupCallback;
        	objRtn["isModeless"] = bIsModeless;
        	objRtn["rtnVal"] = val;

        	var sWinId = this._gfn_setArg(objRtn);

        	if (bIsModeless) {
        		this.opener._gfn_popupAfter(sPopupId, objRtn);
        	} else {
        		this.close(sWinId);
        	}
        }

        /**
        * Modeless 팝업 닫기 함수
        * @param sPopupId{string} Modeless Popup Id
        * @example N/A
        * @memberOf comLib
        */  
        this.gfn_closeModelsessPopup = function (sPopupId)
        {
        	var objCurFrame = application.popupframes[sPopupId];
        	var objCurForm;

        	if (!comUtil.isUndefined(objCurFrame)) objCurForm = objCurFrame.form;

        	if (!comUtil.isUndefined(objCurForm)) {
        		application.gv_curFormId  = comForm.ff_preCurFormId;
        		//var sRtn = objCurForm.opener._setReturn(val);
        		objCurForm.opener.gfn_popupAfter(sPopupId, null);
        		objCurForm.close();
        	} else {
        		trace("Error on modeless popup " + sPopupId);
        	}
        }

        //============================ Argument 처리 함수 종료 =================================

        
        //============================ Validation 처리 함수 종료 ===========================
        this.gfn_chkCompNull = function (objForm,oComp)
        {
        	var bRtn = false;
        	var arrComps = this.gfn_getCompsByTaborder(objForm, oComp);

        	var nCompIdx;

        	for (nCompIdx = 0; nCompIdx < arrComps.length; nCompIdx++) {
        //		trace("arrComps[" + nCompIdx + "].name : " + arrComps[nCompIdx].name + ", cssclass : " + arrComps[nCompIdx].cssclass);

        		if (arrComps[nCompIdx] instanceof Static) continue;
        		if ((arrComps[nCompIdx] instanceof Div) && arrComps[nCompIdx].cssclass != "essential") continue;
        		if (arrComps[nCompIdx] instanceof Tab) continue;
        		if (arrComps[nCompIdx] instanceof Tabpage) continue;
        //		if (arrComps[nCompIdx].readonly) continue;
        		if (!arrComps[nCompIdx].visible) continue;
        		if (!arrComps[nCompIdx].enable) continue;
        		if (arrComps[nCompIdx].cssclass != "essential") continue;

        // 		if (arrComps[nCompIdx] instanceof Div) {
        // 			if (arrComps[nCompIdx].url == "comm::cmmMonthCal.xfdl") {
        // 				arrComps[nCompIdx] = arrComps[nCompIdx].cal_month;
        // 			} else {
        // 				continue;
        // 			}
        // 		}

        //		trace("arrComps[" + nCompIdx + "].name : " + arrComps[nCompIdx].name + ", Sort : " + arrComps[nCompIdx]._sort);

        		if (this.gfn_isNull(arrComps[nCompIdx].value)) {
        			bRtn = true;
        			break;
        		}
        	}

        	if (bRtn) {
        		var arrMsg = this.gfn_getMsgToArray("MSG_SRCH_VALICHK_MANDATORY"); // 필수 검색조건의 입력이 누락 되었습니다.
        		arrMsg[1] = "필수 입력 데이터가 없습니다.";
        //MSG_10001 "필수 입력 데이터가 없습니다."
        		alert(arrMsg[1]);
        		arrComps[nCompIdx].setFocus();
        	}

        	return bRtn;
        }

        this.gfn_getCompsByTaborder = function (objForm,oComposite)
        {
            var aryUnsorted = [];
        	this.gfn_getComps (objForm, oComposite,
        			function(objForm, comp, nLvl, owner) {
        			comp._sort;
        			owner._sort;
        			//if (comp.taborder == undefined) return;
        			if (nLvl == 1) {
        				owner._sort = 1;
        			}

        			if (comp instanceof Tabpage) {
        				if (objForm.gfn_isNull(comp.taborder)) comp.taborder = objForm.gfn_getTabIdx(comp.parent, comp.name);
        			}

        			comp._sort = owner._sort + "." + String(comp.taborder).padLeft(4, "0");
        //			trace(comp + ", " + comp.name + ", " + nLvl + ", " + owner + ", owner.sort : " + owner._sort + ", comp.sort : " + comp._sort);
        			aryUnsorted.push(comp);
        		}
        	);

            this.gfn_sortArray(aryUnsorted,
        		function(a, b) {
        			return a._sort > b._sort;
        		}
            );

            return aryUnsorted;
        }

        this.gfn_getComps = function (objForm,oOwner,fncCallback,nLvl)
        {
        	if(this.gfn_isNull(nLvl)) nLvl = 1;

        	for(var i=0,size=oOwner.components.length; i<size; i++){
        		var oComp = oOwner.components[i];

        		fncCallback(objForm, oComp, nLvl, oOwner);

        		if(oComp instanceof Div){
        			if (oComp._sort == undefined) oComp._sort = oOwner._sort;
        			this.gfn_getComps(objForm, oComp, fncCallback, nLvl+1);
        		} else if(oComp instanceof Tab){
        			if (oComp._sort == undefined) oComp._sort = oOwner._sort;
        			this.gfn_getComps(objForm, oComp, fncCallback, nLvl+1);
        		} else if(oComp instanceof Tabpage) {
        			if (oComp._sort == undefined) oComp._sort = oOwner._sort;
                   this.gfn_getComps(objForm, oComp, fncCallback, nLvl+1);
        		}
        	}
        }

        /**
         * a에 해당하는 component들을 fncCompare 순으로 Sort 한다.
         *
         * @param  a : Component Array
         * @param  fncCompare : 정렬기준
         * @return object array
         * @see
         */
        this.gfn_sortArray = function (a,fncCompare)
        {
            for (var i = 1; i < a.length; i++) {
                var j = i;
                var tmp = a[i];

                while ((j > 0) && fncCompare(a[j - 1], tmp)) {
                    a[j] = a[j - 1];
                    j--;
                }

                a[j] = tmp;
            }
        }

        
        /**
         * objParam.objDs에서 objParam.colNm 컬럼들 중 null 인 컬럼이 있을경우
         * 체크 결과 (objRtn.result), null인 컬럼명(objRtn.colnm), null인 row(objRtn.row)를 object 형태로 리턴한다.
         * (단, objParam.exprStr 이 있을경우 해당 Expr을 적용한다.)
         * 
         * @param  objParam.objDs (Object Dataset) : 체크대상 Dataset(필수)
         * @param  objParam.colNm (String) : 체크대상 컬럼명 (필수, 여러개일 경우 "Col1,Col2,Col3"의 형태로 기술)
         * @param  objParam.exprStr (String) : 별도의 조건을 만족해야 할 경우 해당 Expr을 세팅
         * @return object
         * @return objRtn.result (boolean) : true/false
         *         objRtn.colnm (String) : 컬럼명
         *         objRtn.row (number) : row
         * @see :
         *        var objParam = new Object();
         *        objParam.objDs = this.Dataset00;
         *        objParam.colNm = "COL01,COL02";
         *        objParam.exprStr = "CHK == '1'";
         *
         *        var objRtn = this.gfn_chkDSNull(objParam);
         */
        this.gfn_chkDSNull = function (objParam)
        {
        	var sExpr = "";
        	var bApplyExpr = false;

        	if (this.gfn_isNull(objParam)) return false;
        	if (this.gfn_isNull(objParam.colNm)) return false;
        	if (!(objParam.objDs instanceof Dataset)) return false;
        	if (!this.gfn_isNull(objParam.exprStr)) {
        		bApplyExpr = true;
        		sExpr = objParam.exprStr;
        	}

        	var objRtn = new Object();
        	var objDs = objParam.objDs;
            var arrColNm = String(objParam.colNm).split(",");

        	var bChkCol = true;

        	for (var i = 0; i < arrColNm.length; i++) {
        		var objColInfo = objDs.getColumnInfo(arrColNm[i]);
        		if (this.gfn_isNull(objColInfo)) bChkCol = false;
        	}

        	if (!bChkCol) return false;

        	objRtn.result = false;
        	objRtn.colnm = "";
        	objRtn.row = -1;

        	var bUpdate = false;

        	for (var nCol = 0; nCol < arrColNm.length; nCol++) {
        		for (var nRow = 0; nRow < objDs.getRowCount(); nRow++) {
        			if (objDs.getRowType(nRow) != Dataset.ROWTYPE_INSERT && objDs.getRowType(nRow) != Dataset.ROWTYPE_UPDATE) continue;

        			if (bApplyExpr) {
        				var nFindRow = objDs.findRowExpr(sExpr, i, i);
        				if (nFindRow < 0) continue;
        			}

        			var sColVal = objDs.getColumn(nRow, arrColNm[nCol]);
        //trace("nRow : " + nRow + ", arrColNm[" + nCol + "] : " + arrColNm[nCol] + ", sColVal : " + sColVal + ", isNull : " + this.gfn_isNull(sColVal));
        			if (this.gfn_isNull(sColVal)) {
        				objRtn.result = true;
        				objRtn.colnm = arrColNm[nCol];
        				objRtn.row = nRow;
        				break;
        			}
        		}
        		if (objRtn.result) break;
        //		trace("nRow : " + nRow);
        	}

        	return objRtn;
        }
        //============================ Validation 처리 함수 종료 ===========================
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
