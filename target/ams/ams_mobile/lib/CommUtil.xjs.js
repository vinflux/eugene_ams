﻿//XJS=CommUtil.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name         : commUtil.xjs                                                                                                          
        * Title        : nexacro 공통 Transaction 관련 함수 모음                                                                             
        * @desc     : XP Engine을 이용한 Transaction 처리                                                                                                                      
        * 작성자    : 유희남                                                                                                                                
        * 작성일    : 2013-04-29                                                                                                                           
        * 변경사항    :                                                                                                        
        ********************************************************************************/ 

        this.fv_sGb;
        this.fv_dataset;
        this.fv_code;
        this.fv_sId;
        this.fv_cboNm;
        this.fv_cboV;
        this.fv_frameGb = "";
        this.fv_codeCnt = 0;

        /********************************************************************************
         * Null값 여부(undefined 포함, 0은 null 아님)
         *
         * @param String 검증할 값    
         * @return Boolean
         ********************************************************************************/
        this.gfn_isNull = function(value)
        {
        	if (value == null) return true;
        	if (value == undefined) return true;
        	if (("x" + value == "xNaN") && (value == undefined)) return true;
        	if (String(value).length == 0) return true;

        	return false;
        }

        this.gfn_trimString = function(sValue)
        {
        	if(this.gfn_isNull(sValue)){
        		return "";
        	}
        	
            var retVal = new String(sValue);
            
            retVal = retVal.replace('"','');
            retVal = retVal.replace("'",'');
            retVal = retVal.replace("\\","");
            retVal = nexacro.trim(retVal);
            return retVal;
        }
        /********************************************************************************
         * Null값 아닌지 여부
         *
         * @param String 검증할 값
         * @return Boolean
         ********************************************************************************/
        this.gfn_isNotNull = function (value)
        {
        	return !this.gfn_isNull(value);
        }

        /********************************************************************************
         * 검증할 값이 Null인지 여부에 따라 지정된 값을 반환한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 검증할 값
         * @param String Null일 경우의 값
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_nvl = function (value,nullValue)
        {
            if (this.gfn_isNull(value)) {
                return nullValue;
            } else {
                return value;
            }
        }

        /********************************************************************************
         * 검증할 값이 Null일 경우에 ""(Empty-공백)을 반환한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 검증할 값
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_isNullEmpty = function (value)
        {
            if (this.gfn_isNull(value)) {
                return "";
            } else {
                return value;
            }
        }

        /********************************************************************************
         * 이메일의 정합성 체크
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 이메일주소
         *
         * @return Boolean
         *
         ********************************************************************************/
        this.gfn_isEmail = function (value)
        {
            var expression = new RegExp(/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/);

            if (this.gfn_isNotNull(value)) {
                return expression.test(value);
            } else {
                return false;
            }
        }

        /********************************************************************************
         * 도메인의 정합성 체크
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 도메인
         *
         * @return Boolean
         *
         ********************************************************************************/
        this.gfn_isDomain = function (value)
        {
            var expression = new RegExp(/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/);

            if (this.gfn_isNotNull(value)) {
                return expression.test(value);
            } else {
                return false;
            }
        }

        /********************************************************************************
         * 시간 정합성 체크
         *
         * @author 민경선
         * @since 2013-01-07
         * @version 1.0
         *
         * @param String 시분(1212)
         *
         * @return Boolean
         *
         *
         ********************************************************************************/
        this.gfn_isTime = function (value)
        {
            var expression;
            var returnValue = false;

            value = value.split(":").join("");

            if (nexacro.trim(value).toString().length != 4) {
                return false;
            }

            expression = new RegExp(/^([01][0-9]|2[0-3]?)[0-5][0-9]$/);

            if (this.gfn_isNotNull(value)) {
                returnValue = expression.test(value);
            } else {
                returnValue = false;
            }

            return returnValue;
        }

        /********************************************************************************
         * 날짜형식 정합성 체크
         *
         * @author 민경선
         * @since 2013-01-07
         * @version 1.0
         *
         * @param String 날짜
         *
         * @return Boolean
         *
         ********************************************************************************/
        this.gfn_isDate = function (value)
        {
            if (this.gfn_isNull(value)) {
                return false;
            }

            value = value.split("-").join("");

            if (value.length != 8) {
                return false;
            }

            var year = nexacro.toNumber(value.toString().substr(0, 4));
            var month = nexacro.toNumber(value.toString().substr(4, 2));
            var date = nexacro.toNumber(value.toString().substr(6, 2));

            var dayExpression = new RegExp(/^(19|20)[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/);
            var noExpression = new RegExp(/^[0-9]{8}$/);

            if (!dayExpression.test(value)) {
                return false;
            }

            if (!noExpression.test(value)) {
                return false;
            }

            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
                if (date > 31 || date < 1) {
                    return false;
                }
            } else if (month == 2) {
                if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
                    if (date > 29 || date < 1) {
                        return false;
                    }
                } else {
                    if (date > 28 || date < 1) {
                        return false;
                    }
                }
            } else if (month == 4 || month == 6 || month == 9 || month == 11) {
                if (date > 30 || date < 1) {
                    return false;
                }
            }

            return true;
        }

        /********************************************************************************
         * Object가 함수인지 확인한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 함수 명
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_isFunction = function (functionName)
        {
            //functionName은 메서드의 이름
            var objName = functionName;
            
            //this.이 포함되지 않은 경우
            if (functionName.indexOf("this.") < 0) {
                objName = "this." + functionName;
            }
            
            if (typeof ( eval ( objName ) ) == "function") {
                return true;
            } else {
                return false;
            }
        }

        /********************************************************************************
         * replace에서 정규식 없이도 replace되도록 하는 기능.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 대상 값
         * @param String 찾아 없앨 문자열
         * @param String 변경할 문자열
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_replaceAll = function (value,originalText,replaceText)
        {
            value = new String(value);
            var returnText = value;

            if (this.gfn_isNotNull(value)) {
                returnText = value.split(originalText).join(replaceText);
            }
            return returnText;
        }

        /********************************************************************************
         * 지정된 캐릭터셋 별로 byte length를 리턴한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 대상 값
         * @param String 구분 값 EUC-KR (한글을 2byte로 인식), UTF-8 (한글을 3byte로 인식)
         *
         * @return Number
         *
         ********************************************************************************/
        this.gfn_getBytes = function (value,gubun)
        {
            var inValue = value.toString();
            var length = 0;
            var step = 1;

            if (gubun == "UTF-8") {
                step = 3;
            } else if (gubun == "EUC-KR") {
                step = 2;
            }

            for (var index = 0; index < inValue.length; index++) {
                if (inValue.charCodeAt(index) > 127) {
                    length += step;
                } else {
                    length += 1;
                }
            }
            return length;
        }

        /********************************************************************************
         * 날짜를 String 문자열로 변환
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 문자 열
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_dateToString = function (value)
        {
            var objArray = new Array();

            objArray.push((new Date(value)).getFullYear());
            objArray.push(((new Date(value)).getMonth() + 1).toString().padLeft(2, "0"));
            objArray.push(((new Date(value)).getDate()).toString().padLeft(2, "0"));

            return objArray.join("-");
        }

        /********************************************************************************
         * 문자열을 날짜로 변환
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 문자 열
         *
         * @return Date
         *
         ********************************************************************************/
        this.gfn_stringToDate = function (value)
        {
        	if (this.gfn_isNull(value)) return value;

        	value = String(value).trim();

            if (value.length != 6 && value.length != 8) {
                this.gfn_alert("stringToDate는 6자(YYMMDD) 또는 8자(YYYYMMDD) 형식으로 값을 넣어주어야 합니다.");
            }

            if (value.length == 6) {
                var nowYear = (new Date()).getFullYear();
                var newYear = parseInt(value.substr(0, 2)) + 2000;

                if (nowYear < newYear) {
                    newYear -= 100;
                }

                value = newYear.toString() + value.substr(2);
            }

            return new Date(parseInt(value.substr(0, 4)), parseInt(value.substr(4, 2)) - 1, parseInt(value.substr(6, 2)));
        }

        /********************************************************************************
         * 자리 수 마다 ','를 찍어준다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 숫자
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_formatComma = function (value)
        {
            var startLength = 0;
            var endLength = 0;
            var returnValue = "";

            if (value.charAt(0) == "+" || value.charAt(0) == "-") {
                returnValue += value.charAt(0);
                startLength = 1;
            }

            endLength = value.indexOf(".");

            var mainDigit = value.substr(startLength, endLength - startLength);

            for (var index = 0; index < mainDigit.length; index++) {
                if (index != 0 && (mainDigit.length - index) % 3 == 0) {
                    returnValue += ",";
                }
                returnValue += mainDigit.charAt(index);
            }
            returnValue += value.substr(endLength);

            return returnValue;
        }

        /********************************************************************************
         * 숫자에 찍힌 ','를 제거한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 콤마가 포함된 숫자
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_replaceComma = function (value)
        {
            return value.split(",").join("");
        }

        /********************************************************************************
         * 문자 포맷 형식 변환
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 문자열
         * @param String 문자열 포맷(#:문자, 포맷스트링("-", ",", ".") 등
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_formatMask = function (value,mask)
        {
            var returnValue = "";
            var sUnit;

            if (this.gfn_isNull(value)) {
                return this.gfn_isNullEmpty(value);
            }

            for (var index = 0; index < mask.length; index++) {
                var str = mask.substr(index, 1);

                if (str == "#") {
                    returnValue += value.substr(0, 1);
                    value = value.substr(1);
                } else {
                    returnValue += str;
                }
            }

            return returnValue;
        }

        /********************************************************************************
         * 오늘 날짜를 가져오는 함수(클라이언트 PC기준)
         * @return String
         ********************************************************************************/
        this.gfn_today = function()
        {
            return this.gfn_getClientDate("YYYYMMDD");
        }

        /********************************************************************************
         * 오늘 날짜시간를 가져오는 함수(클라이언트 PC기준)
         * @return String
         ********************************************************************************/
        this.gfn_toDateTime = function()
        {
            return this.gfn_getClientDate("YYYYMMDDHH24MISS");
        }

        /********************************************************************************
         * 오늘 날짜를 가져오는 함수(클라이언트 PC기준)
         *
         * @author 조효성
         * @since 2012-11-12
         * @version 1.0
         *
         * @param String 리턴할 값을 지정(YYYY, MM, DD, HH, HH24, MI, SS, MIL)
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_getClientDate = function (value)
        {
            var date = new Date();

            if (value.indexOf("YYYY") >= 0) 
            {
                value = value.replace(/YYYY/g, date.getFullYear());
            }

            if (value.indexOf("MM") >= 0) 
            {
                value = value.replace(/MM/g, (date.getMonth() + 1).toString().padLeft(2, "0"));
            }

            if (value.indexOf("DD") >= 0) 
            {
                value = value.replace(/DD/g, date.getDate().toString().padLeft(2, "0"));
            }

            if (value.indexOf("HH24") >= 0) 
            {
                value = value.replace(/HH24/g, date.getHours().toString().padLeft(2, "0"));
            }

            if (value.indexOf("HH") >= 0) 
            {
                value = value.replace(/HH/g, (date.getHours() % 12).toString().padLeft(2, "0"));
            }

            if (value.indexOf("MI") >= 0) 
            {
                value = value.replace(/MI/g, date.getMinutes().toString().padLeft(2, "0"));
            }

            if (value.indexOf("SS") >= 0) 
            {
                value = value.replace(/SS/g, date.getSeconds().toString().padLeft(2, "0"));
            }

            if (value.indexOf("MIL") >= 0) 
            {
                value = value.replace(/MIL/g, date.getMilliseconds().toString().padLeft(3, "0"));
            }

            if (value.indexOf("TIME") >= 0) 
            {
                value = value.replace(/TIME/g, date.getTime().toString().padLeft(3, "0"));
            }

            return value;
        }

        
        /**********************************************************************************
         * 함수명      : gfn_AddDate
         * 설명        : 입력된 날자에 OffSet 으로 지정된 만큼의 일을 더한다.
         *               Date Type을 String으로 변환
         * argument    : date ('yyyyMMdd' 형태로 표현된 날자)
         *               nOffSet (날짜로부터 증가 감소값. 지정하지 않으면 Default Value = 1 로 적용됩니다)
         * return Type : String
         * return 내용 : Date에 nOffset이 더해진 결과를 'yyyyMMdd'로 표현된 날자.
         **********************************************************************************/
        this.gfn_addDate = function(sDate,nOffSet)
        {
            sDate = sDate.substr(0, 8);
            var oDate = this.gfn_stringToDate(sDate);
            
            var oAddDate = oDate.addDate(nOffSet);
            oDate = new Date(oAddDate);
            return this.gfn_dateFormatString(oDate, "%Y%m%d");
        }

        /**********************************************************************************
         * 함수명      : gfn_AddMonth
         * 설명        : 입력된 날자에 OffSet 으로 지정된 만큼의 달을 더한다.
         *               Date Type을 String으로 변환
         * argument    : date ('yyyyMMdd' 형태로 표현된 날자)
         *               nOffSet (날짜로부터 증가 감소값. 지정하지 않으면 Default Value = 1 로 적용됩니다)
         * return Type : String
         * return 내용 : Date에 nOffset이 더해진 결과를 'yyyyMMdd'로 표현된 날자.
         **********************************************************************************/
        this.gfn_addMonth = function(sDate,nOffSet)
        {
            var oDate = this.gfn_stringToDate(sDate);

            var oAddMonth = oDate.addMonth(nOffSet);
            oDate = new Date(oAddMonth);
            return this.gfn_dateFormatString(oDate, "%Y%m%d");
        }

        /**
         * @class 문자열이 숫자형식에 맞으면 숫자값을 반환
         * @param sNum 체크할 문자열
         * @return int
         */  
        this.gfn_getNum = function(sNum)
        {
            if (nexacro.isNumeric(sNum)){
                return parseInt( sNum );
            } else {
                0;
            }
        }

        /*
         * Function Name : gfn_isNum2(소수점 까지 체크)
         * Description   : Check whether the correct numeric format strings
         * Parameter     : Input String to check the status for number
         * Return        : If the correct number format return true, If not in correct number format return false
         * Example       : fn_checkNumber("-1234.56");
         */
        this.gfn_isNum2 = function(sNum)
        {
            var sChar;
            var nCnt = 0;
            var bRtn;
            if(typeof(sNum) != "stirng"){
                sNum = new String(sNum);
            }

            for (var i = 0; i < sNum.length; i++) 
            {
                sChar = sNum.charAt(i);

                if (i == 0 && (sChar == "+" || sChar == "-" )) 
                {
                    bRtn = true;
                } 
                else if (sChar >= "0" && sChar <= "9") 
                {
                    bRtn = true;
                } 
                else if (sChar == ".") 
                {
                    nCnt++;
                    if (nCnt > 1)    
                    {
                        bRtn = false;
                        break;
                    }
                } 
                else 
                {
                    bRtn = false;
                    break;
                }
            }
            
            return bRtn;
        }

        /**
         * @class 스트링의 자릿수를 Byte 단위로 환산하여 알려준다 영문, 숫자는 1Byte이고 한글은 3Byte이다.(자/모 중에 하나만 있는 글자도 3Byte이다.)
         * @param sValue(스트링)
         * @return 문자열의 byte 길이
         */  
        this.gfn_getByteLength = function(sValue) 
        {
            var byteLength = 0;
            if (this.gfn_isNull(sValue)){
                return 0;
            }
            var c;
            for (var i=0; i<sValue.length; i++){
                c = escape(sValue.charAt(i));
                if (c.length == 1){ // when English then 1byte
                    byteLength++;
                } else if (c.indexOf("%u") != -1){ // when Korean then 3byte
                    byteLength += 2;                 // utf-8 : 3
                } else if (c.indexOf("%") != -1){     // else 3byte
                    byteLength += c.length/3;
                }
            }
            return byteLength;
        }

        /**
         * @class 스트링자릿수를 체크하여 제한한다.
         * @param val(스트링)
         * @param nLimit - 제한글자수
         * @return 문자열의 byte 길이
         */  
        this.gfn_isMaxLengh = function(val,nLimit)
        {
            var nLength = this.gfn_getByteLength(val);
            var bRtn = (nLength > nLimit) ? false:true;

            return bRtn;
        }

        /**
         * @class 문자열의 오른쪽부분을 지정한 길이만큼 Return 한다.
         * @param val(오른부분을 얻어올 원본 문자열)
         * @param nSize - 얻어올 크기. [Default Value = 0]
         * @return String 오른쪽 부분이 얻어진 문자열.
         */  
        this.gfn_right = function (val,nSize)
        {
            var nStart = this.gfn_toString(val).length;
            var nEnd = Number(nStart) - Number(nSize);
            var rtnVal = val.substring(nStart, nEnd);
            
            return rtnVal;
        }

        /**
         * @class 문자열의 왼쪽부분을 지정한 길이만큼 Return 한다.
         * @param val(왼쪽부분을 얻어올 원본 문자열)
         * @param nSize - 얻어올 크기. [Default Value = 0]
         * @return String 왼쪽 부분이 얻어진 문자열.
         */  
        this.gfn_left = function (val,nSize)
        {
            return this.gfn_toString(val).substr(0, nSize);
        }

        /**
         * @class 입력값을 String으로 변경한다.
         * @param Val
         * @return String
         */  
        this.gfn_toString = function (val)
        {
            if (this.gfn_isNull(val)){
                return new String();
            }
            return new String(val);
        }

        // FormatString
        this.gfn_dateFormatString = function (oDate,sFormat)
        {
            if (this.gfn_isNull(sFormat)) {
                return "";
            }
            var fY = String(oDate.getFullYear());
            var fY2 = fY.substr(fY.length-2, 2);

            sFormat = sFormat.toString();
            sFormat = sFormat.split("%Y").join(String(oDate.getFullYear()));
            sFormat = sFormat.split("%y").join(fY2);
            sFormat = sFormat.split("%m").join(String(oDate.getMonth() + 1).padLeft(2, "0"));
            sFormat = sFormat.split("%d").join(String(oDate.getDate()).padLeft(2, "0"));
            sFormat = sFormat.split("%H").join(String(oDate.getHours()).padLeft(2, "0"));
            sFormat = sFormat.split("%M").join(String(oDate.getMinutes()).padLeft(2, "0"));
            sFormat = sFormat.split("%S").join(String(oDate.getSeconds()).padLeft(2, "0"));

            return sFormat;
        }

        //데이타셋에 컬럼 추가
        this.gfn_addColumn = function(objDs,sColumnNm,sColumnType)
        {
            if(this.gfn_isNull(objDs)) trace("gfn_addColumn dataset object error");
            if(this.gfn_isNull(sColumnNm)) trace("gfn_addColumn sColumnNm error");
            if(this.gfn_isNull(sColumnType)) sColumnType = "string";
            
            if(this.gfn_isNull(objDs.getColumnInfo(sColumnNm)))
            {
                objDs.addColumn(sColumnNm, sColumnType);
                objDs.applyChange();
            }
            
            return true;
        }

        /********************************************************************************
         * 배열에 indexOf 가 지원되지 않아 이에 동일한 기능에 대해서 구현
         *
         * @author 조효성
         * @since 2012-11-24
         * @version 1.0
         *
         * @param Array
         * @param String
         *
         * @return int
         *
         ********************************************************************************/
        this.gfn_arrayIndexOf = function (inArray,findString)
        {
            var result = -1;
            for (var index = 0; index < inArray.length; index++) 
            {
                if (inArray[index] == findString) 
                {
                    result = index;
                    break;
                }
            }

            return result;
        }

        this.gfn_lookup = function (p,name)
        {
            var o;
            
            while (p)
            {        
                o = p.components;
                if ( o && o[name] ) return o[name];
                
                o = p.objects;
                if ( o && o[name] ) return o[name];
                
                p = p.parent;
            }
            
            return null;
        }

        /**
        * 공통 코드 조회 서버
        * @param  sCode  : Code
                  sId    : searchId
                  ds_ret : dataset
                  sGb    : 선택, ALL
                  cboNm  : 콤보 명
                  cboV   : 콤보 값
        * @return
        */
        this.gfn_getCommCodeTran = function(sCode,searchId,ds_ret,sGb,cboNm,cboV)
        {
            this.fv_code    = sCode.split("|");
            this.fv_sId     = searchId.split("|");
            this.fv_dataset = ds_ret.split("|");
            this.fv_sGb     = sGb.split("|");
            this.fv_cboNm   = cboNm.split("|");
            this.fv_cboV    = cboV.split("|");
            this.fv_codeCnt = 0;
            
            this.gfn_codeSearchTran();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.gfn_codeSearchTran = function()
        {
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectCommonCode");
            
            var sSvcId   = "codeSearch";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = this.fv_dataset[this.fv_codeCnt]+"=OUT_COMMON_CODE_LIST";
            var sParam   = "adcd_hdkey="+this.fv_code[this.fv_codeCnt]
                         +" searchid="+this.fv_sId[this.fv_codeCnt]
                         +" type=combo"
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "gfn_codeCallBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.gfn_codeCallBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
            var objDs = this[this.fv_dataset[this.fv_codeCnt]];
            var nRow = 0;
            
            if(!this.gfn_isNull(this.fv_sGb[this.fv_codeCnt])){
                nRow = objDs.insertRow(0);
                objDs.setColumn(nRow, "code", "");
                objDs.setColumn(nRow, "name", this.fv_sGb[this.fv_codeCnt]);
            }
            
            if(!this.gfn_isNull(this.fv_cboNm[this.fv_codeCnt])){
                var objCbo = eval("this." + this.fv_cboNm[this.fv_codeCnt]);
                
                if(!this.gfn_isNull(this.fv_cboV[this.fv_codeCnt])){
                    objCbo.set_value(this.fv_cboV[this.fv_codeCnt]);
                }else{
                    objCbo.set_index(0);
                }
            }
            
            this.fv_codeCnt++;
            
            if(this.fv_code.length == this.fv_codeCnt) {
                this.fv_codeCnt = 0;
                return;
            }
            
            this.gfn_codeSearchTran();
        }

        /**
        * 공통 코드 조회
        * @param  sCode  : Code
                  ds_ret : dataset
                  sGb    : 선택, ALL
                  cboNm  : 콤보 명
                  cboV   : 콤보 값
        * @return
        */
        this.gfn_getCommCode = function(sCode,ds_ret,sGb,cboNm,cboV)
        {
            this.fv_code    = sCode.split("|");
            this.fv_dataset = ds_ret.split("|");
            this.fv_sGb     = sGb.split("|");
            this.fv_cboNm   = cboNm.split("|");
            this.fv_cboV    = cboV.split("|");
            this.fv_codeCnt = 0;
            
        	var objDs;
        	var objCbo;
        	var nRow = 0;
        	
        	for(var i = 0 ; i < this.fv_code.length ; i++){
        		application.gds_commCode.filter("hdkey=='"+ this.fv_code[i] +"'");
        		objDs = this[this.fv_dataset[i]];
        		
        		for(var j = 0 ; j < application.gds_commCode.rowcount ; j++){
        			objDs.addRow();
        			objDs.setColumn(j, "code", application.gds_commCode.getColumn(j, "field1"));
        			objDs.setColumn(j, "name", application.gds_commCode.getColumn(j, "field2"));
        		}
        		
        		if(!this.gfn_isNull(this.fv_sGb[i])){
        			nRow = objDs.insertRow(0);
        			objDs.setColumn(nRow, "code", "");
        			objDs.setColumn(nRow, "name", this.fv_sGb[i]);
        		}
        		
        		if(!this.gfn_isNull(this.fv_cboNm[i])){
        			objCbo = eval("this." + this.fv_cboNm[i]);
        			
        			if(!this.gfn_isNull(this.fv_cboV[i])){
        				objCbo.set_value(this.fv_cboV[i]);
        			}else{
        				objCbo.set_index(0);
        			}
        		}
        	}
        	
        	application.gds_commCode.filter("");
        }

        this.gfn_setCommon = function(sKey,sValue){
        	this.gfn_setData("COMMON", sKey, sValue);
        }

        this.gfn_setParam = function(sKey,sValue){
        	this.gfn_setData("PARAM", sKey, sValue);
        }

        this.gfn_setMap = function(sKey,sValue){
        	this.gfn_setData("MAP", sKey, sValue);
        }

        this.gfn_setUserInfo = function(sKey,sValue){
        	this.gfn_setData("USER_INFO", sKey, sValue);
        }

        this.gfn_setLang = function(sKey,sValue){
        	this.gfn_setData("LANG", sKey, sValue);
        }

        this.gfn_setData = function(sCategory,sKey,sValue)
        {
        	if(sCategory == null || sKey == null){
        		return;
        	}
        	
        	switch (sCategory.toUpperCase()) {
        		case "PARAM":
        			if(!application.gds_param.setColumn(0, sKey, sValue)){
        				application.gds_param.addColumn(sKey, "string");
        				if(application.gds_param.getRowCount() == 0){
        					application.gds_param.addRow();
        				}
        				application.gds_param.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "COMMON":
        			if(!application.gds_common.setColumn(0, sKey, sValue)){
        				application.gds_common.addColumn(sKey, "string");
        				if(application.gds_common.getRowCount() == 0){
        					application.gds_common.addRow();
        				}
        				application.gds_common.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "USERINFO":
        		case "USER_INFO":
        			if(!application.gds_userInfo.setColumn(0, sKey, sValue)){
        				application.gds_userInfo.addColumn(sKey, "string");
        				if(application.gds_userInfo.getRowCount() == 0){
        					application.gds_userInfo.addRow();
        				}
        				application.gds_userInfo.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "MAP":
        			if(!application.gds_map.setColumn(0, sKey, sValue)){
        				application.gds_map.addColumn(sKey, "string");
        				if(application.gds_map.getRowCount() == 0){
        					application.gds_map.addRow();
        				}
        				application.gds_map.setColumn(0, sKey, sValue);
        			}
        			break;
        		case "LANG":
        			if(!application.gds_lang.setColumn(0, sKey, sValue)){
        				application.gds_lang.addColumn(sKey, "string");
        				if(application.gds_lang.getRowCount() == 0){
        					application.gds_lang.addRow();
        				}				
        				application.gds_lang.setColumn(0, sKey, sValue);
        			}
        			break;			
        	}
        }

        this.gfn_getCommon = function(sKey){
        	return this.gfn_getData("COMMON", sKey);
        }

        this.gfn_getParam = function(sKey){
        	return this.gfn_getData("PARAM", sKey);
        }

        this.gfn_getMap = function(sKey){
        	return this.gfn_getData("MAP", sKey);
        }

        this.gfn_getUserInfo = function(sKey){
        	return this.gfn_getData("USER_INFO", sKey);
        }

        this.gfn_getLang = function(sKey){
        	return this.gfn_getData("LANG", sKey);
        }

        this.gfn_getData = function(sCategory,sKey){
        	var returnValue = "";
        	if(sCategory == null || sKey == null){
        		return returnValue;
        	}
        	
        	switch (sCategory.toUpperCase()) {
        		case "PARAM":
        			returnValue = application.gds_param.getColumn(0, sKey);
        			break;
        			
        		case "COMMON":
        			returnValue = application.gds_common.getColumn(0, sKey);
        			break;
        			
        		case "USERINFO":
        		case "USER_INFO":
        			returnValue = application.gds_userInfo.getColumn(0, sKey);
        			break;
        			
        		case "MAP":
        			returnValue = application.gds_map.getColumn(0, sKey);
        			break;
        			
        		case "LANG":
        			returnValue = application.gds_lang.getColumn(0, sKey);
        			break;	
        	}
        	return returnValue;
        }

        this.gfn_getActiveApp = function(){
        	var moduleNmae = "";
        	switch(application.gv_activeApp.toUpperCase()){
        		case "AMS":
        		case "ADM":
        		case "ADMIN":
        			moduleNmae = application.gv_ams;
        			break;
        		case "WM":
        		case "WMS":
        			moduleNmae = application.gv_wms;
        			break;
        		case "TM":
        		case "TMS":
        			moduleNmae = application.gv_tms;
        			break;
        	}
        	return moduleNmae;
        }

        this.startDate = null;
        this.endDate = null;
        this.lastTime = null;

        this.gfn_setCheckTime = function(msg){
        	this.startDate = new Date();
        	trace(msg+"startTime");
        }

        this.gfn_getCheckTime = function(msg)
        {
        	this.endDate = new Date();
        	var endTime = this.endDate.getTime();
        	var diff = endTime- this.startDate.getTime();
        	this.lastTime = Math.floor(diff);

        	trace(msg+" total Time : "+this.lastTime+" Millisecond");
        }

        
        //======================================= Switch Button 관련 공통함수 시작 =======================================
        this.gfn_initSetupComp = function (objForm,objDiv,sType,defValue)
        {
        	if (sType == "SWITCH") {
        		if (this.gfn_isNull(defValue)) defValue = true;
        		objDiv.btn_switch.value = defValue;
        		objDiv.owner = objForm;
        		objDiv.btn_switch.ownerForm = objForm;
        		objDiv.btn_switch.setEventHandler("onclick", objForm.gfn_btn_switch_onclick, objForm);
        		objDiv.setStatus = function (bState) {
        			var objBtn = this.btn_switch; // this는 objDiv임.
        			objBtn.value = !bState;
        			this.owner.gfn_btn_switch(this);
        		};

        		objDiv.setStatus(defValue);
        	}
        }

        this.gfn_getLocalInfo = function (sPropId)
        {
        	var sRtnVal = "";

        	if (system.navigatorname == "nexacro") {
        		sRtnVal = application.getPrivateProfile(sPropId);
        	} else {
        		sRtnVal = window.localStorage.getItem(sPropId);
        	}

        	return sRtnVal;
        }

        this.gfn_setLocalInfo = function (sPropId,sValue)
        {
        	var sRtnVal = "";

        	if (system.navigatorname == "nexacro") {
        		sRtnVal = application.setPrivateProfile(sPropId, sValue);
        	} else {
        		sRtnVal = window.localStorage.setItem(sPropId, sValue);
        	}

        	return sRtnVal;
        }

        this.gfn_btn_switch = function(obj)
        {
        	var objBtn = obj.btn_switch;
        	var sCssClassNm = "setup_attr_on";
        	var nXPos = Number(objBtn.onXPos);

        	if (objBtn.value) {
        		sCssClassNm = "setup_attr_off";
        		nXPos = Number(objBtn.offXPos);
        	}

        	obj.set_cssclass(sCssClassNm);
        	objBtn.setOffsetLeft(nXPos);

        	objBtn.value = !objBtn.value;

        	this.gfn_setLocalInfo(obj.key, objBtn.value);

        	var objExtComPlugin = obj.parent.extComPlugin;

        	if (!this.gfn_isNull(objExtComPlugin)) {
        		var param = {};

        		if (obj.key == "HavepushService") {
        			if (objBtn.value) {
        				objExtComPlugin.callMethod("enablePushService", param);
        			} else {
        				objExtComPlugin.callMethod("disablePushService", param);
        			}
        		} else if (obj.key == "HavemsgSoundService") {
        			if (objBtn.value) {
        				param = {"key":"str_pushSoundSetUp", "value":"TRUE"};
        			} else {
        				param = {"key":"str_pushSoundSetUp", "value":"FALSE"};
        			}

        			objExtComPlugin.callMethod("setProperty", param);
        		}
        	}

        	return objBtn.value;
        }

        this.gfn_btn_switch_onclick = function(obj,e)
        {
        	obj.parent.onclick.fireEvent(obj.parent, e);
        }
        //======================================= Switch Button 관련 공통함수 종료 =======================================
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
