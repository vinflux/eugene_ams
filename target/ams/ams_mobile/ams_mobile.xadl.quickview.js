﻿(function()
{
    return function()
    {
        // Theme, Component URI Setting
        this._theme_uri = "./_theme_/";
        this._globalvar_uri = "globalvars.xml";
        this.loadTypedefition = function()
        {
            // this._addService(prefixid, type, url, cachelevel, codepage, language, version, communication);
            this._addService("default_typedef.xml", "svc", "JSP", "http://localhost:8080", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "frame", "form", "./frame/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "img", "file", "./img/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "css", "file", "./css/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "lib", "js", "./lib/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "obj_xcommon", "form", "./obj_xcommon/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "guide", "form", "./guide/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "template", "form", "./template/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "sample", "form", "./sample/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "comm", "form", "./comm/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "cond", "form", "./cond/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "master", "form", "./master/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "wms", "form", "http://localhost:8080/wms/wms/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "oms", "form", "http://localhost:8080/oms/oms/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "tms", "form", "http://localhost:8080/tms/tms/", "session", null, "", "0", "0");
            this._addService("default_typedef.xml", "base", "form", "./base/", "session", null, "", "0", "0");

            this._component_uri = (this._arg_compurl ? this._arg_compurl : "./nexacro14lib/component/");
            // load components
            var registerclass = [
            		{"id":"Div", "classname":"nexacro.Div", "type":"JavaScript"},
            		{"id":"Button", "classname":"nexacro.Button", "type":"JavaScript"},
            		{"id":"PopupDiv", "classname":"nexacro.PopupDiv", "type":"JavaScript"},
            		{"id":"Combo", "classname":"nexacro.Combo", "type":"JavaScript"},
            		{"id":"CheckBox", "classname":"nexacro.CheckBox", "type":"JavaScript"},
            		{"id":"ListBox", "classname":"nexacro.ListBox", "type":"JavaScript"},
            		{"id":"Edit", "classname":"nexacro.Edit", "type":"JavaScript"},
            		{"id":"MaskEdit", "classname":"nexacro.MaskEdit", "type":"JavaScript"},
            		{"id":"TextArea", "classname":"nexacro.TextArea", "type":"JavaScript"},
            		{"id":"Menu", "classname":"nexacro.Menu", "type":"JavaScript"},
            		{"id":"Tab", "classname":"nexacro.Tab", "type":"JavaScript"},
            		{"id":"ImageViewer", "classname":"nexacro.ImageViewer", "type":"JavaScript"},
            		{"id":"Radio", "classname":"nexacro.Radio", "type":"JavaScript"},
            		{"id":"Calendar", "classname":"nexacro.Calendar", "type":"JavaScript"},
            		{"id":"Static", "classname":"nexacro.Static", "type":"JavaScript"},
            		{"id":"Grid", "classname":"nexacro.Grid", "type":"JavaScript"},
            		{"id":"Spin", "classname":"nexacro.Spin", "type":"JavaScript"},
            		{"id":"PopupMenu", "classname":"nexacro.PopupMenu", "type":"JavaScript"},
            		{"id":"GroupBox", "classname":"nexacro.GroupBox", "type":"JavaScript"},
            		{"id":"ProgressBar", "classname":"nexacro.ProgressBar", "type":"JavaScript"},
            		{"id":"Plugin", "classname":"nexacro.Plugin", "type":"JavaScript"},
            		{"id":"Dataset", "classname":"nexacro.NormalDataset", "type":"JavaScript"},
            		{"id":"FileUpload", "classname":"nexacro.FileUpload", "type":"JavaScript"},
            		{"id":"FileDownload", "classname":"nexacro.FileDownload", "type":"JavaScript"},
            		{"id":"WebBrowser", "classname":"nexacro.WebBrowser", "type":"JavaScript"},
            		{"id":"UbiReport", "classname":"nexacro.UbiViewer", "type":"JavaScript"},
            		{"id":"Camera", "classname":"nexacro.Camera", "type":"JavaScript"},
            		{"id":"Geolocation", "classname":"nexacro.Geolocation", "type":"JavaScript"}
            ];
            this._addClasses(registerclass);
        };
        
        this.on_loadGlobalVariables = function()
        {
            // global variable
            this._addVariable("gv_userId", null, false);
            this._addVariable("gv_userNm", null, false);
            this._addVariable("gv_userAuth", null, false);
            this._addVariable("gv_sServer", "L", false);
            this._addVariable("gv_language", null, false);
            this._addVariable("gv_url", null, false);
            this._addVariable("gv_chartUrl", null, false);
            this._addVariable("gv_loginStatus", null, false);
            this._addVariable("gv_system", null, false);
            this._addVariable("gv_activeApp", null, false);
            this._addVariable("gv_showHideYn", "Y", false);
            this._addVariable("gv_sUrl", "/nexacro/handlerAdapter.nx", false);
            this._addVariable("gv_ams", "ams", false);
            this._addVariable("gv_wms", "wms", false);
            this._addVariable("gv_oms", "oms", false);
            this._addVariable("gv_tms", "tms", false);
            this._addVariable("gv_vms", "vms", false);
            this._addVariable("gv_userUrl", null, false);
            this._addVariable("gv_fUrl", "/nexacro/fileHandlerAdapter.nx", false);
            this._addVariable("gv_temaAms", null, false);
            this._addVariable("gv_temaWms", null, false);
            this._addVariable("gv_temaTms", null, false);
            this._addVariable("gv_temaOms", null, false);
            this._addVariable("gv_temaVms", null, false);

            // global image

            // global dataset
            var obj = null;
            obj = new Dataset("gds_menu", this);
            obj._setContents("<ColumnInfo><Column id=\"eqtype\" type=\"string\" size=\"256\"/><Column id=\"isleaf\" type=\"string\" size=\"256\"/><Column id=\"isseparator\" type=\"string\" size=\"256\"/><Column id=\"mekey\" type=\"string\" size=\"256\"/><Column id=\"mename\" type=\"string\" size=\"256\"/><Column id=\"usname\" type=\"string\" size=\"256\"/><Column id=\"meorder\" type=\"string\" size=\"256\"/><Column id=\"uppermekey\" type=\"string\" size=\"256\"/><Column id=\"uskey\" type=\"string\" size=\"256\"/><Column id=\"uspath\" type=\"string\" size=\"256\"/><Column id=\"apkey\" type=\"string\" size=\"256\"/><Column id=\"melvl\" type=\"string\" size=\"256\"/><Column id=\"nxpath\" type=\"STRING\" size=\"256\"/></ColumnInfo><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000040</Col><Col id=\"MENAME\">기본 마스터 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">20</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"USNAME\">어플리케이션</Col><Col id=\"MEORDER\">30</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00000050</Col><Col id=\"MENAME\">어플리케이션</Col><Col id=\"USKEY\">US00000400</Col><Col id=\"USPATH\">ADMIN_US00000400_masterApplication</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000070</Col><Col id=\"MENAME\">메시지</Col><Col id=\"USNAME\">메시지</Col><Col id=\"MEORDER\">50</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000040</Col><Col id=\"USPATH\">ADMIN_US00000040_masterMessage</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000080</Col><Col id=\"MENAME\">매입거래처</Col><Col id=\"USNAME\">매입거래처</Col><Col id=\"MEORDER\">60</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000320</Col><Col id=\"USPATH\">ADMIN_US00000320_account</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103230</Col><Col id=\"MENAME\">거래처별센터</Col><Col id=\"USNAME\">거래처별센터</Col><Col id=\"MEORDER\">61</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003660</Col><Col id=\"USPATH\">ADMIN_US00003660_Acxct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MENAME\">화주</Col><Col id=\"USNAME\">화주</Col><Col id=\"MEORDER\">70</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00000090</Col><Col id=\"USKEY\">US00000280</Col><Col id=\"USPATH\">ADMIN_US00000280_owner</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000100</Col><Col id=\"MENAME\">센터</Col><Col id=\"USNAME\">센터</Col><Col id=\"MEORDER\">80</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000030</Col><Col id=\"USPATH\">ADMIN_US00000030_masterCenter</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103130</Col><Col id=\"MENAME\">매출거래처</Col><Col id=\"USNAME\">매출거래처</Col><Col id=\"MEORDER\">81</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003590</Col><Col id=\"USPATH\">ADMIN_SC00003890_STORE</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MENAME\">센터별매출처</Col><Col id=\"USNAME\">센터별매출처</Col><Col id=\"MEORDER\">82</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00103240</Col><Col id=\"USKEY\">US00003670</Col><Col id=\"USPATH\">ADMIN_US00003670_Storexct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000110</Col><Col id=\"MENAME\">장비</Col><Col id=\"USNAME\">장비</Col><Col id=\"MEORDER\">90</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000770</Col><Col id=\"USPATH\">ADMIN_US00000770_equipment</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00109380</Col><Col id=\"MENAME\">알림설정</Col><Col id=\"USNAME\">알림설정</Col><Col id=\"MEORDER\">100</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00008920</Col><Col id=\"USPATH\">ADMIN_US00008920_masterAlertSetting</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000420</Col><Col id=\"MENAME\">상품 단위</Col><Col id=\"USNAME\">상품 단위</Col><Col id=\"MEORDER\">100</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000270</Col><Col id=\"USPATH\">ADMIN_US00000270_itemUnit</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">2</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103280</Col><Col id=\"MENAME\">운송사</Col><Col id=\"USNAME\">운송사</Col><Col id=\"MEORDER\">107</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003690</Col><Col id=\"USPATH\">ADMIN_US00003690_Carrier</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\"/><Col id=\"MEKEY\">ME00103290</Col><Col id=\"MENAME\">차량</Col><Col id=\"USNAME\">차량</Col><Col id=\"MEORDER\">108</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003730</Col><Col id=\"USPATH\">ADMIN_US00003730_Equip</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\"/><Col id=\"MEKEY\">ME00103300</Col><Col id=\"MENAME\">기사정보</Col><Col id=\"USNAME\">기사정보</Col><Col id=\"MEORDER\">109</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003740</Col><Col id=\"USPATH\">ADMIN_US00003740_Driver</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\"/><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USNAME\">PUSH발송</Col><Col id=\"MEORDER\">110</Col><Col id=\"MEKEY\">ME00109471</Col><Col id=\"MENAME\">PUSH발송</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00008961</Col><Col id=\"USPATH\">ADMIN_US00008961_PUSHSEND</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000410</Col><Col id=\"MENAME\">상품 그룹</Col><Col id=\"USNAME\">상품 그룹</Col><Col id=\"MEORDER\">110</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000100</Col><Col id=\"USPATH\">ADMIN_US00000170_itemCodeGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">2</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000200</Col><Col id=\"MENAME\">관리자 마스터</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">110</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"ISLEAF\">N</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000205</Col><Col id=\"MENAME\">관리 기준 코드</Col><Col id=\"USNAME\">관리 기준 코드</Col><Col id=\"MEORDER\">115</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000080</Col><Col id=\"USPATH\">ADMIN_US00000080_masterAdminCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000430</Col><Col id=\"MENAME\">상품</Col><Col id=\"USNAME\">상품</Col><Col id=\"MEORDER\">120</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000330</Col><Col id=\"USPATH\">ADMIN_US00000330_itemCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000210</Col><Col id=\"MENAME\">사용자 그룹</Col><Col id=\"USNAME\">사용자 그룹</Col><Col id=\"MEORDER\">120</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000420</Col><Col id=\"USPATH\">ADMIN_US00000420_userUserGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000220</Col><Col id=\"MENAME\">사용자</Col><Col id=\"USNAME\">사용자</Col><Col id=\"MEORDER\">130</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"USKEY\">US00000460</Col><Col id=\"USPATH\">ADMIN_US00000460_userUSER</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000440</Col><Col id=\"MENAME\">상품 대체 코드</Col><Col id=\"USNAME\">상품 대체 코드</Col><Col id=\"MEORDER\">130</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000560</Col><Col id=\"USPATH\">ADMIN_US00000560_itemSubstitution</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00109851</Col><Col id=\"MENAME\">상품그룹인터페이스</Col><Col id=\"USNAME\">상품그룹인터페이스</Col><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"USKEY\">US00009331</Col><Col id=\"USPATH\">ADMIN_US00009331_ItemGroupInterface</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000400</Col><Col id=\"MENAME\">상품 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103190</Col><Col id=\"MENAME\">상품별센터</Col><Col id=\"USNAME\">상품별센터</Col><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003640</Col><Col id=\"USPATH\">ADMIN_US00003640_Icxct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00104240</Col><Col id=\"MENAME\">센터별거래처별상품</Col><Col id=\"USNAME\">센터별거래처별상품</Col><Col id=\"MEORDER\">150</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00004640</Col><Col id=\"USPATH\">ADMIN_US00004640_Icxctxac</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000600</Col><Col id=\"MENAME\">메뉴 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">210</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000610</Col><Col id=\"MENAME\">사용자 화면 검색 조건</Col><Col id=\"USNAME\">사용자 화면 검색 조건</Col><Col id=\"MEORDER\">220</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000050</Col><Col id=\"USPATH\">ADMIN_US00000050_searchCondition</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103260</Col><Col id=\"MENAME\">임가공 상품</Col><Col id=\"USNAME\">임가공 상품</Col><Col id=\"MEORDER\">230</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003700</Col><Col id=\"USPATH\">ADMIN_US00003700_SetItemCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000620</Col><Col id=\"MENAME\">사용자 화면</Col><Col id=\"USNAME\">사용자 화면</Col><Col id=\"MEORDER\">230</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000060</Col><Col id=\"USPATH\">ADMIN_US00000060_userScreen</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000630</Col><Col id=\"MENAME\">메뉴</Col><Col id=\"USNAME\">메뉴</Col><Col id=\"MEORDER\">240</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000070</Col><Col id=\"USPATH\">ADMIN_US00000070_menu</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000800</Col><Col id=\"MENAME\">권한 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">250</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000810</Col><Col id=\"MENAME\">ROLE 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">260</Col><Col id=\"UPPERMEKEY\">ME00000800</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000445</Col><Col id=\"MENAME\">상품 단위 업로드</Col><Col id=\"USNAME\">상품 단위 업로드</Col><Col id=\"MEORDER\">270</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00001850</Col><Col id=\"USPATH\">ADMIN_US00001850_ItemCodeUnitUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000820</Col><Col id=\"MENAME\">ROLE별 어플리케이션</Col><Col id=\"USNAME\">ROLE별 어플리케이션</Col><Col id=\"MEORDER\">270</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000090</Col><Col id=\"USPATH\">ADMIN_US00000090_applicationByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00101390</Col><Col id=\"MENAME\">상품 그룹 업로드</Col><Col id=\"USNAME\">상품 그룹 업로드</Col><Col id=\"MEORDER\">275</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00002660</Col><Col id=\"USPATH\">ADMIN_US00002660_ItemCodeGroupUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000830</Col><Col id=\"MENAME\">ROLE별 화주</Col><Col id=\"USNAME\">ROLE별 화주</Col><Col id=\"MEORDER\">280</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000290</Col><Col id=\"USPATH\">ADMIN_US00000290_ownerByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000450</Col><Col id=\"MENAME\">상품 업로드</Col><Col id=\"USNAME\">상품 업로드</Col><Col id=\"MEORDER\">280</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000550</Col><Col id=\"USPATH\">ADMIN_US00000550_itemCodeUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000460</Col><Col id=\"MENAME\">상품 대체 코드 업로드</Col><Col id=\"USNAME\">상품 대체 코드 업로드</Col><Col id=\"MEORDER\">290</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000620</Col><Col id=\"USPATH\">ADMIN_US00000620_substitutionUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000840</Col><Col id=\"MENAME\">ROLE별 센터</Col><Col id=\"USNAME\">ROLE별 센터</Col><Col id=\"MEORDER\">290</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000300</Col><Col id=\"USPATH\">ADMIN_US00000300_centerByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000850</Col><Col id=\"MENAME\">ROLE별 메뉴</Col><Col id=\"USNAME\">ROLE별 메뉴</Col><Col id=\"MEORDER\">300</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000310</Col><Col id=\"USPATH\">ADMIN_US00000310_menuByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000860</Col><Col id=\"MENAME\">ROLE별 사용자 화면 오브젝트</Col><Col id=\"USNAME\">ROLE별 사용자 화면 오브젝트</Col><Col id=\"MEORDER\">310</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000340</Col><Col id=\"USPATH\">ADMIN_US00000340_usobByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103140</Col><Col id=\"MENAME\">용기</Col><Col id=\"USNAME\">용기</Col><Col id=\"MEORDER\">310</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003600</Col><Col id=\"USPATH\">ADMIN_US00003600_Cntr</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000870</Col><Col id=\"MENAME\">사용자 ROLE 권한 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">320</Col><Col id=\"UPPERMEKEY\">ME00000800</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00108190</Col><Col id=\"MENAME\">체화예상재고</Col><Col id=\"USNAME\">체화예상재고</Col><Col id=\"MEORDER\">320</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00007930</Col><Col id=\"USPATH\">ADM_US00007930_Accexday</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000880</Col><Col id=\"MENAME\">사용자 그룹별 ROLE</Col><Col id=\"USNAME\">사용자 그룹별 ROLE</Col><Col id=\"MEORDER\">330</Col><Col id=\"UPPERMEKEY\">ME00000870</Col><Col id=\"USKEY\">US00000010</Col><Col id=\"USPATH\">ADMIN_US00000010_roleByUserGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000890</Col><Col id=\"MENAME\">사용자별 ROLE</Col><Col id=\"USNAME\">사용자별 ROLE</Col><Col id=\"MEORDER\">340</Col><Col id=\"UPPERMEKEY\">ME00000870</Col><Col id=\"USKEY\">US00000020</Col><Col id=\"USPATH\">ADMIN_US00000010_roleByUser</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000900</Col><Col id=\"MENAME\">시스템 환경 설정</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">350</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000910</Col><Col id=\"MENAME\">관리자 환경 설정</Col><Col id=\"USNAME\">관리자 환경 설정</Col><Col id=\"MEORDER\">360</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000900</Col><Col id=\"USPATH\">ADMIN_US00000900_adminConfigSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000920</Col><Col id=\"MENAME\">사용자 이력 조회</Col><Col id=\"USNAME\">사용자 이력 조회</Col><Col id=\"MEORDER\">370</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000930</Col><Col id=\"USPATH\">ADMIN_US00000930_userHistory</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000930</Col><Col id=\"MENAME\">시스템 환경 설정</Col><Col id=\"USNAME\">시스템 환경 설정</Col><Col id=\"MEORDER\">380</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000960</Col><Col id=\"USPATH\">ADMIN_US00000960_systemConfigSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000940</Col><Col id=\"MENAME\">나의 메뉴 설정</Col><Col id=\"USNAME\">나의 메뉴 설정</Col><Col id=\"MEORDER\">390</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">ADM_US00001940_MyMenuSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME99999999</Col><Col id=\"MENAME\">SMAPLE</Col><Col id=\"USNAME\">SMAPLE</Col><Col id=\"MEORDER\">400</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">sample::sampleUser.xfdl</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME99999998</Col><Col id=\"MENAME\">TEST</Col><Col id=\"USNAME\">TEST</Col><Col id=\"MEORDER\">410</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">wms::XXAMC10005.xfdl</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_openMenu", this);
            obj._setContents("<ColumnInfo><Column id=\"WINID\" type=\"STRING\" size=\"256\"/><Column id=\"winseq\" type=\"INT\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"isleaf\" type=\"STRING\" size=\"256\"/><Column id=\"isseparator\" type=\"STRING\" size=\"256\"/><Column id=\"mekey\" type=\"STRING\" size=\"256\"/><Column id=\"mename\" type=\"STRING\" size=\"256\"/><Column id=\"usname\" type=\"STRING\" size=\"256\"/><Column id=\"meorder\" type=\"STRING\" size=\"256\"/><Column id=\"uppermekey\" type=\"STRING\" size=\"256\"/><Column id=\"uskey\" type=\"STRING\" size=\"256\"/><Column id=\"uspath\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"melvl\" type=\"STRING\" size=\"256\"/><Column id=\"nxpath\" type=\"STRING\" size=\"256\"/><Column id=\"urlopen\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_msg", this);
            obj._setContents("<ColumnInfo><Column id=\"mulaapmsg_hdkey\" type=\"string\" size=\"256\"/><Column id=\"displaymessage\" type=\"string\" size=\"2000\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userInfo", this);
            obj._setContents("<ColumnInfo><Column id=\"divcd\" type=\"STRING\" size=\"256\"/><Column id=\"SESSION_USERINFO\" type=\"STRING\" size=\"256\"/><Column id=\"dst\" type=\"STRING\" size=\"256\"/><Column id=\"duplLog\" type=\"STRING\" size=\"256\"/><Column id=\"ctKey\" type=\"STRING\" size=\"256\"/><Column id=\"usrtyp\" type=\"STRING\" size=\"256\"/><Column id=\"rstclgst\" type=\"STRING\" size=\"256\"/><Column id=\"usKey\" type=\"STRING\" size=\"256\"/><Column id=\"urGrKey\" type=\"STRING\" size=\"256\"/><Column id=\"ctKey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"urName\" type=\"STRING\" size=\"256\"/><Column id=\"utcMinute\" type=\"STRING\" size=\"256\"/><Column id=\"lastpwyn\" type=\"STRING\" size=\"256\"/><Column id=\"utcOffset\" type=\"STRING\" size=\"256\"/><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"utcHour\" type=\"STRING\" size=\"256\"/><Column id=\"loginStatus\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/><Column id=\"laKey\" type=\"STRING\" size=\"256\"/><Column id=\"urCryptoName\" type=\"STRING\" size=\"256\"/><Column id=\"owkeym\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userAuth", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_gridMenu", this);
            obj._setContents("<ColumnInfo><Column id=\"menuLvl\" type=\"STRING\" size=\"256\"/><Column id=\"menuId\" type=\"STRING\" size=\"256\"/><Column id=\"menuNm\" type=\"STRING\" size=\"256\"/><Column id=\"bUse\" type=\"STRING\" size=\"256\"/><Column id=\"bEnbleColumn\" type=\"STRING\" size=\"256\"/><Column id=\"hotkeyColumn\" type=\"STRING\" size=\"256\"/><Column id=\"iconColumn\" type=\"STRING\" size=\"256\"/><Column id=\"multiLang\" type=\"STRING\" size=\"256\"/><Column id=\"option\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">100</Col><Col id=\"menuNm\">오름차순 정렬</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuNm\">내림차순 정렬</Col><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">200</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">900</Col><Col id=\"menuNm\">Copy</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">910</Col><Col id=\"menuNm\">Paste</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">300</Col><Col id=\"menuNm\">필터</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">400</Col><Col id=\"menuNm\">필터제거</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">500</Col><Col id=\"menuNm\">Lock</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">600</Col><Col id=\"menuNm\">UnLock</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">700</Col><Col id=\"menuNm\">컬럼 목록</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">800</Col><Col id=\"menuNm\">초기화</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\">INITIALIZATION</Col><Col id=\"option\"/></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_commCode", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_lang", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_param", this);
            obj._setContents("<ColumnInfo><Column id=\"pagingLimit\" type=\"STRING\" size=\"256\"/><Column id=\"currentPage\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"urPw\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"appkey\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"COUNT\" type=\"STRING\" size=\"256\"/><Column id=\"colName\" type=\"STRING\" size=\"256\"/><Column id=\"sortValue\" type=\"STRING\" size=\"256\"/><Column id=\"pushid\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"pagingLimit\">100</Col><Col id=\"currentPage\">1</Col><Col id=\"ctkey\">1001</Col><Col id=\"urKey\"/><Col id=\"urPw\"/><Col id=\"lakey\">KOR</Col><Col id=\"appkey\"/><Col id=\"eqtype\">40</Col><Col id=\"sortValue\"/><Col id=\"colName\"/><Col id=\"COUNT\"/><Col id=\"pushid\">null</Col></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_common", this);
            obj._setContents("<ColumnInfo><Column id=\"DEVICE\" type=\"STRING\" size=\"256\"/><Column id=\"EQTYPE\" type=\"STRING\" size=\"256\"/><Column id=\"TIMEZONE\" type=\"STRING\" size=\"256\"/><Column id=\"uskey\" type=\"STRING\" size=\"256\"/><Column id=\"ACTIVE_APP\" type=\"STRING\" size=\"256\"/><Column id=\"OWKEY_AUTH\" type=\"STRING\" size=\"256\"/><Column id=\"CTKEY_AUTH\" type=\"STRING\" size=\"256\"/><Column id=\"BEANID\" type=\"STRING\" size=\"256\"/><Column id=\"METHODNM\" type=\"STRING\" size=\"256\"/><Column id=\"LAKEY\" type=\"STRING\" size=\"256\"/><Column id=\"APKEY\" type=\"STRING\" size=\"256\"/><Column id=\"TRANSACTION_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"pushid\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DEVICE\">MOBILE</Col><Col id=\"EQTYPE\">40</Col><Col id=\"TIMEZONE\">9</Col><Col id=\"uskey\"/><Col id=\"ACTIVE_APP\">AMS</Col><Col id=\"OWKEY_AUTH\"/><Col id=\"CTKEY_AUTH\"/><Col id=\"BEANID\">loginController</Col><Col id=\"METHODNM\">actionLogin</Col><Col id=\"LAKEY\">KOR</Col><Col id=\"APKEY\">ADM</Col><Col id=\"TRANSACTION_TYPE\">null</Col></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_outCommon", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_systemComfig", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_admSystemConfig", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_srchCondition", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_srchCondition2", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userGrid", this);
            obj.set_useclientlayout("false");
            obj._setContents("<ColumnInfo><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"usKey\" type=\"STRING\" size=\"256\"/><Column id=\"usgridId\" type=\"STRING\" size=\"256\"/><Column id=\"urdfusInfo\" type=\"STRING\" size=\"256\"/><Column id=\"urdfusInfoInit\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userMenu", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_eqList", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnMenuObjectRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnAppRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnOwnerRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnWHRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_map", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;


            

        };
        
        // property, event, createMainFrame
        this.on_initApplication = function()
        {
            // properties
            this.set_id("ams_mobile");
            this.set_version("");
            this.set_tracemode("none");
            this.set_themeid("wms_mobile.xtheme");
            this.set_httpretry("0");
            this.set_filesecurelevel("0");
            this.set_networksecurelevel("0");

            if (this._is_attach_childframe)
            	return;

            // frame
            var mainframe = this.createMainFrame("mainframe", "absolute", "0", "0", "640", "1097", null, null, this);
            mainframe.set_resizable("true");
            mainframe.set_showtitlebar("true");
            mainframe.set_showstatusbar("false");
            mainframe.style.set_menubarheight("0");
            mainframe.style.set_background("#ffffffff");
            mainframe.style.set_border("0 solid #3d3d3dff");
            mainframe.style.set_color("#ffffffff");
            mainframe.style.set_font("9 Dotum");
            mainframe.style.set_titlebarheight("26");
            mainframe.style.set_statusbarheight("0");
            mainframe.set_showtitleicon("false");
            mainframe.on_createBodyFrame = this.mainframe_createBodyFrame;

            // tray
            var tray = null;

        };
        

        
        this.mainframe_createBodyFrame = function()
        {
            var obj = new ChildFrame("childframe", "absolute", null, null, null, null, null, null, "", this);
            this.addChild(obj.name, obj);
            this.frame = obj;
            obj.set_formurl(application._quickview_formurl);
            obj.set_showtitlebar("false");
            obj.set_showstatusbar("false");
            obj = null;
        };
        
        this.on_initEvent = function()
        {

        };
        
        // screeninfo
        this.loadScreenInfo = function()
        {
            var screeninfo = [
            		{"name":"PHONE_P", "zoommin":"100%", "type":"phone", "screenwidth":"640", "autozoom":"true", "zoommax":"100%", "formlayoutwidth":"0", "formlayoutheight":"0"},
            		{"name":"TABLET", "zoommin":"100%", "type":"tablet", "screenwidth":"0", "autozoom":"false", "zoommax":"100%", "formlayoutwidth":"0", "formlayoutheight":"0"}];
            this._loadScreenInfo(screeninfo);

        }
        
        // script Compiler
        this.registerScript("ams_mobile.xadl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ams_mobile.xadl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.07.06
        * DESCRIPTION : Application 최초 로딩시 실행
        ------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        this.gv_CF_MAIN;
        this.gv_CF_HIDDEN;
        // this.gv_serverIp = "localhost"; // Local, jyko.pe.kr
        // this.gv_serverPort = "8080"; // Local
        this.gv_serverIp = "localhost";// 개발
        this.gv_serverPort = "8080"; // 개발
        this.gv_osType = "Auto"; //Windwos/Android/iOS/Auto
        this.gv_eqType = "30";
        this.gv_pushId = "";
        this.gv_loggrpcd = "";
        this.gv_appVersion = "";

        /**
        * 프레임정보 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onload = function(obj,e)
        {
         	this.gv_CF_HIDDEN = application.mainframe.VF_MAIN.CF_HIDDEN;
            this.gv_CF_MAIN = application.mainframe.VF_MAIN.CF_MAIN;

        }

        /**
        * URL 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onloadingglobalvariables = function(obj,e)
        {
        	application.gv_system = "AMS";
        	application.gv_activeApp = "AMS";

        	if (this.gv_osType == "Auto") this.gv_osType = this.afn_getOsType("Windwos");
        	if (this.gv_osType != "Windwos") application.mainframe.set_showtitlebar(false);

        	var objServices = application.services;

        	for (var i = 0; i < objServices.length; i++) {
        		var sUrl = objServices[i].url;
        		var sPrefixId = objServices[i].prefixid;
        		var sType = objServices[i].type;

        		if (sType == "JSP") continue;
        		if (String(sUrl).indexOf("http:") >= 0) continue;
        		if (sPrefixId == "wms" || sPrefixId == "oms" || sPrefixId == "tms") continue;
        		if (sPrefixId == "main" || sPrefixId == "inbound" || sPrefixId == "putaway" || sPrefixId == "inventory" || sPrefixId == "outbound") continue;

        		if (this.gv_osType == "Windwos") application.services[sPrefixId].url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/ams/ams_mobile/" + sPrefixId + "/";
        	}

        	application.gv_url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/ams/ams/";
        	application.services["svc"].url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/";
        	application.services["wms"].url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/wms/wms/";
        	application.services["oms"].url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/oms/oms/";
        	application.services["tms"].url = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/tms/tms/";
        	application.gv_chartUrl = "http://" + this.gv_serverIp + ":" + this.gv_serverPort + "/ams/chart/";

        	application.gv_sServer = "L";
        }

        /**
        * FRAME 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.gfn_setFrame = function(sMode)
        {

        }

        /**
        * NULL Check
        * @return
        * @example
        * @memberOf public
        */
        this.agfn_isNull = function(value)
        {
        	if (sValue == null) {
        		return true;
        	}

        	if (sValue == undefined) {
        		return true;
        	}

        	if (("x" + sValue == "xNaN") && (sValue == undefined))  {
        		return true;
        	}

        	if (sValue.length == 0)  {
        		return true;
        	}

        	return false;
        }

        this.afn_getOsType = function (sDefault)
        {
        	var sRtn = "";
        	var system_type = nexacro.SystemType.toLowerCase();
        	switch (system_type) {
        		case "win32":
        		case "win64":
        		case "winphone":
        			sRtn = "Windwos";
        			break;
        		case "ipad":
        		case "iphone":
        			sRtn = "iOS";
        			break;
        		case "android":
        			sRtn = "Android";
        			break;
        		case "mac":
        		case "linux":
        		default:
        			sRtn = undefined;
        			if (!this.afn_isNull(sDefault)) sRtn = sDefault;
        			break;
        	}

        	return sRtn;
        }

        /**
        * BeforeExit 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onbeforeexit = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(!this.agfn_isNull(application.gv_loginStatus)){
        //			return "로그아웃 하시겠습니까?";
        		}
        	}
        }

        /**
        * Exit 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onexit = function(obj,e)
        {
        	if(obj == e.fromobject){
        	}
        }

        /**
        * Close 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.MainFrame_onclose = function(obj,e)
        {
        	if(obj == e.fromobject){
        	}
        }

        /**
        * beforeclose 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.MainFrame_onbeforeclose = function(obj,e)
        {
        	if(obj == e.fromobject){
        		var sMenu = "";
        		var dsTab;

        		if(obj == e.fromobject){
        			for(var i = 0 ; i < application.gds_openMenu.rowcount ; i++){
        				if(i == 0) sMenu += application.gds_openMenu.getColumn(i, "mekey");
        				else sMenu += "|" + application.gds_openMenu.getColumn(i, "mekey");
        			}

        			if(system.navigatorname == "nexacro") {
        				application.setPrivateProfile("openMenu"+application.gv_activeApp, sMenu);
        				application.setPrivateProfile("LOGIN_FLAG", "N");
        			} else {
        				window.localStorage.setItem("openMenu"+application.gv_activeApp, sMenu);
        				window.localStorage.setItem("LOGIN_FLAG", "N");
        			}
        		}
        	}
        }

        // // push 동작 시 호출되는 함수
        // this.gfn_pushReceivePopup = function (sArgs)
        // {
        // 	var arrMsg = String(sArgs).split(",");
        // 	var sMsgId = String(arrMsg[0]).trim();
        // 	var sMsgContent = String(sArgs).replace(arrMsg + ",", "");
        // 
        // 	application.alert("sMsgId : " + sMsgId + "\nsMsgContent : " + sMsgContent);
        // }
        // 
        // this.MainFrame_onsize = function(obj:MainFrame, e:nexacro.SizeEventInfo)
        // {
        // 
        // }

        // push 동작 시 호출되는 함수
        this.fun_SetDeviceID = function(arg)
        {
        	this.alert(arg);
        }
        });


        this.loadTypedefition();
        this.loadScreenInfo();
        this.loadTheme("wms_mobile.xtheme");


    };
}
)();
