﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MainMenu");
                this.set_classname("frame_login");
                this._setFormPosition(0,0,640,989);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_langResult", this);
            obj._setContents("<ColumnInfo><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"32\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"32\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_compXml", this);
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"HEAD_TAG\" type=\"STRING\" size=\"256\"/><Column id=\"XML_SCRIPT\" type=\"STRING\" size=\"256\"/><Column id=\"SRC_TEXT\" type=\"STRING\" size=\"256\"/><Column id=\"TGT_TEXT\" type=\"STRING\" size=\"256\"/><Column id=\"COMP_ID\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_textList", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "9", "51", null, "337", "9", null, this);
            obj.set_taborder("0");
            obj.set_binddataset("ds_langResult");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"280\"/><Column size=\"280\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"mulaapmsg_hdkey\"/><Cell col=\"1\" text=\"displaymessage\"/></Band><Band id=\"body\"><Cell text=\"bind:mulaapmsg_hdkey\"/><Cell col=\"1\" text=\"bind:displaymessage\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_textKor", "absolute", "288", "7", null, "36", "141", null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchTransWord", "absolute", null, "7", "73", "36", "62", null, this);
            obj.set_taborder("2");
            obj.set_text("Search");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "18", "391", "300", "36", null, null, this);
            obj.set_taborder("3");
            obj.style.set_selectcolor("lavender");
            obj.style.set_selectbackground("cornflowerblue");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "326", "391", "300", "36", null, null, this);
            obj.set_taborder("4");
            obj.style.set_selectcolor("lavender");
            obj.style.set_selectbackground("cornflowerblue");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_type", "absolute", "142", "7", "140", "36", null, null, this);
            this.addChild(obj.name, obj);
            var cbo_type_innerdataset = new Dataset("cbo_type_innerdataset", this.cbo_type);
            cbo_type_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">displaymessage</Col><Col id=\"datacolumn\">메시지</Col></Row><Row><Col id=\"codecolumn\">mulaapmsg_hdkey</Col><Col id=\"datacolumn\">메시지 코드</Col></Row></Rows>");
            obj.set_innerdataset(cbo_type_innerdataset);
            obj.set_taborder("5");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_value("displaymessage");
            obj.set_text("메시지");
            obj.set_index("0");

            obj = new Combo("cbo_msg", "absolute", "10", "7", "126", "36", null, null, this);
            this.addChild(obj.name, obj);
            var cbo_msg_innerdataset = new Dataset("cbo_msg_innerdataset", this.cbo_msg);
            cbo_msg_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">gds_lang</Col><Col id=\"datacolumn\">gds_lang</Col></Row><Row><Col id=\"codecolumn\">gds_msg</Col><Col id=\"datacolumn\">gds_msg</Col></Row></Rows>");
            obj.set_innerdataset(cbo_msg_innerdataset);
            obj.set_taborder("6");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_value("gds_lang");
            obj.set_text("gds_lang");
            obj.set_index("0");

            obj = new Button("btn_loadClipBoard", "absolute", "18", "432", "73", "34", null, null, this);
            obj.set_taborder("8");
            obj.set_text("Load\r\nClip Board");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01", "absolute", "18", "470", null, "180", "14", null, this);
            obj.set_taborder("9");
            obj.set_binddataset("ds_compXml");
            obj.set_cellsizingtype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"100\"/><Column size=\"270\"/><Column size=\"110\"/><Column size=\"110\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"HEAD_TAG\"/><Cell col=\"1\" text=\"XML_SCRIPT\"/><Cell col=\"2\" text=\"SRC_TEXT\"/><Cell col=\"3\" text=\"TGT_TEXT\"/></Band><Band id=\"body\"><Cell style=\"align:left;\" text=\"bind:HEAD_TAG\"/><Cell col=\"1\" style=\"align:left;\" text=\"bind:XML_SCRIPT\"/><Cell col=\"2\" edittype=\"normal\" style=\"align:left;\" text=\"bind:SRC_TEXT\"/><Cell col=\"3\" edittype=\"normal\" style=\"align:left;\" text=\"bind:TGT_TEXT\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("btn_setBindScript", "absolute", "97", "432", "73", "34", null, null, this);
            obj.set_taborder("10");
            obj.set_text("Set Bind\r\nScript");
            this.addChild(obj.name, obj);

            obj = new Button("btn_createResultXml", "absolute", "176", "432", "129", "34", null, null, this);
            obj.set_taborder("11");
            obj.set_text("Create Result Xml");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_resultXml", "absolute", "18", "658", null, null, "14", "7", this);
            obj.set_taborder("12");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, "7", "50", "36", "9", null, this);
            obj.set_taborder("13");
            obj.set_text("Close");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 640, 989, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","Edit00","value","ds_langResult","mulaapmsg_hdkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","Edit01","value","ds_langResult","displaymessage");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ViewLanguage.xfdl", "lib::Comm.xjs");
        this.registerScript("ViewLanguage.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Work.xfdl
        * PROGRAMMER  : jyko
        * DATE        : 2016.12.30
        * DESCRIPTION : Work 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if (nErrCd != 0) {
                return;
            }

            if (sSvcId == "") {

            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.btn_searchTransWord_onclick = function(obj,e)
        {
        	var objDs = application.gv_CF_HIDDEN.form.ds_lang;
        	var sValue = this.edt_textKor.value;

        	if (this.cbo_msg.value == "gds_msg") {
        		objDs = application.gds_msg;
        	}

        	if (!this.gfn_isNull(sValue)) {
        		objDs.filter("String(" + this.cbo_type.value + ").indexOf('" + sValue + "') >= 0");

        		this.ds_langResult.clearData();

        		if (objDs.getRowCount() >= 0) this.ds_langResult.copyData(objDs, true);
        	}
        }

        this.edt_textKor_onkeydown = function(obj,e)
        {
        	if (e.keycode == 13) this.btn_searchTransWord.click();
        }

        this.btn_loadClipBoard_onclick = function(obj,e)
        {
        	this.txa_resultXml.set_value("");
        	var sText = String(system.getClipboard("CF_TEXT")).trim();
        	var arrXml = String(sText).split("\n");

        	var bNexaComp = false;
        	if (arrXml[0] == '<?xml version="1.0" encoding="utf-8"?>' && arrXml[1] == '<UxFormClipBoard version="1.5">') bNexaComp = true;

        	if (bNexaComp) {
        		this.ds_compXml.clearData();

        		if (arrXml.length > 0) application.gv_CF_HIDDEN.form.ds_lang.filter("");

        		for (var i = 0; i < arrXml.length; i++) {
        			var sHeadTag = String(arrXml[i]).trim().split(" ")[0];
        			var sSrcText = "";
        			var sTgtText = "";
        			var sCompId = "";

        			if (sHeadTag == "<Static" || sHeadTag == "<Button" ) {
        				sSrcText = String(arrXml[i]).substr(String(arrXml[i]).indexOf("text=") + 5);
        				sSrcText = nexacro.stripQuote(String(sSrcText).substr(1, String(sSrcText).indexOf("\" ") - 1));
        			}

        			if (!this.gfn_isNull(sSrcText)) {
        				var nFindRow = application.gv_CF_HIDDEN.form.ds_lang.findRowExpr("displaymessage == '" + sSrcText + "'");
        				if (nFindRow >= 0) sTgtText = application.gv_CF_HIDDEN.form.ds_lang.getColumn(nFindRow, "mulaapmsg_hdkey");
        			}

        			if (String(arrXml[i]).indexOf("id=") >= 0) {
        				sCompId = String(arrXml[i]).substr(String(arrXml[i]).indexOf("id=") + 4);//, String(arrXml[i]).indexOf("\" ", String(arrXml[i]).indexOf("id=") + 4));
        				sCompId = String(sCompId).substr(0, String(sCompId).indexOf("\" "));
        			}

        			var nRow = this.ds_compXml.addRow();
        			this.ds_compXml.setColumn(nRow, "CHK", "0");
        			this.ds_compXml.setColumn(nRow, "HEAD_TAG", sHeadTag);
        			this.ds_compXml.setColumn(nRow, "XML_SCRIPT", arrXml[i]);
        			this.ds_compXml.setColumn(nRow, "SRC_TEXT", sSrcText);
        			this.ds_compXml.setColumn(nRow, "TGT_TEXT", sTgtText);
        			this.ds_compXml.setColumn(nRow, "COMP_ID", sCompId);
        		}
        	}
        }

        this.btn_setBindScript_onclick = function(obj,e)
        {
        	var nRowCnt = this.ds_compXml.getRowCount();

        	for (var i = (nRowCnt - 1); i >= 0; i--) {
        		var sHeadTag = this.ds_compXml.getColumn(i, "HEAD_TAG");
        		var sSrcText = this.ds_compXml.getColumn(i, "SRC_TEXT");
        		var sTgtText = this.ds_compXml.getColumn(i, "TGT_TEXT");
        		var sCompId = this.ds_compXml.getColumn(i, "COMP_ID");

        		if (this.gfn_isNull(sTgtText)) continue;

        		var nTargetRow = i + 1;

        		var sXmlScript = this.ds_compXml.getColumn(nTargetRow, "XML_SCRIPT");

        		if (String(sXmlScript).indexOf("BindItem") >= 0) {
        			var nStartIdx = String(sXmlScript).indexOf("columnid=");
        			var sStrColumnIdTag = String(sXmlScript).substr(nStartIdx).replace("/>", "");

        			sXmlScript = String(sXmlScript).replace(sStrColumnIdTag, "columnid=\"" + sTgtText + "\"");
        			this.ds_compXml.setColumn(nTargetRow, "HEAD_TAG", "<BindItem");
        			this.ds_compXml.setColumn(nTargetRow, "XML_SCRIPT", sXmlScript);

        		} else {
        			sXmlScript = '    <BindItem id="item' + String(i).padLeft(3, "0") + '" compid="' + sCompId + '" propid="text" datasetid="gds_lang" columnid="' + sTgtText + '"/>';
        			this.ds_compXml.insertRow(nTargetRow);
        			this.ds_compXml.setColumn(nTargetRow, "HEAD_TAG", "<BindItem");
        			this.ds_compXml.setColumn(nTargetRow, "XML_SCRIPT", sXmlScript);
        		}
        	}
        }

        this.btn_createResultXml_onclick = function(obj,e)
        {
        	var sResultXml = "";

        	for (var i = 0; i < this.ds_compXml.getRowCount(); i++) {
        		sResultXml += this.ds_compXml.getColumn(i, "XML_SCRIPT") + "\n";
        	}

        	this.txa_resultXml.set_value(sResultXml);
        }

        this.btn_close_onclick = function(obj,e)
        {
        	this.parent.div_tools.set_visible(false);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.edt_textKor.addEventHandler("onkeydown", this.edt_textKor_onkeydown, this);
            this.btn_searchTransWord.addEventHandler("onclick", this.btn_searchTransWord_onclick, this);
            this.btn_loadClipBoard.addEventHandler("onclick", this.btn_loadClipBoard_onclick, this);
            this.btn_setBindScript.addEventHandler("onclick", this.btn_setBindScript_onclick, this);
            this.btn_createResultXml.addEventHandler("onclick", this.btn_createResultXml_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("ViewLanguage.xfdl", true);

       
    };
}
)();
