﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("TemaPop");
                this.set_classname("style01");
                this.set_titletext("테마 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,250,325);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Radio("rdo_tema", "absolute", "18", "29", "252", "220", null, null, this);
            this.addChild(obj.name, obj);
            var rdo_tema_innerdataset = new Dataset("rdo_tema_innerdataset", this.rdo_tema);
            rdo_tema_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">1</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">bl_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">3</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">4</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">5</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">6</Col><Col id=\"datacolumn\"/></Row></Rows>");
            obj.set_innerdataset(rdo_tema_innerdataset);
            obj.set_taborder("124");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_rowcount("0");
            obj.set_columncount("2");
            obj.set_direction("horizontal");
            obj.set_index("-1");

            obj = new Static("label00", "absolute", "15", "14", "140", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("스킨 변경 설정");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "40", null, "1", "0", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "9", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "100", null, "52", "28", null, "17", this);
            obj.set_taborder("123");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "37", "101", "58", "15", null, null, this);
            obj.set_taborder("125");
            obj.set_text("다크그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_dark", "absolute", "37", "57", "58", "39", null, null, this);
            obj.set_taborder("126");
            obj.set_image("URL('img::skin_darkgray.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_blue", "absolute", "163", "57", "58", "39", null, null, this);
            obj.set_taborder("128");
            obj.set_image("URL('img::skin_blue.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "163", "101", "58", "15", null, null, this);
            obj.set_taborder("129");
            obj.set_text("블루");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "37", "174", "58", "15", null, null, this);
            obj.set_taborder("131");
            obj.set_text("레드");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_red", "absolute", "37", "130", "58", "39", null, null, this);
            obj.set_taborder("132");
            obj.set_image("URL('img::skin_red.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_green", "absolute", "163", "130", "58", "39", null, null, this);
            obj.set_taborder("134");
            obj.set_image("URL('img::skin_green.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "163", "174", "58", "15", null, null, this);
            obj.set_taborder("135");
            obj.set_text("그린");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "37", "247", "58", "15", null, null, this);
            obj.set_taborder("137");
            obj.set_text("그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_grey", "absolute", "37", "203", "58", "39", null, null, this);
            obj.set_taborder("138");
            obj.set_image("URL('img::skin_gray.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_ddd", "absolute", "163", "203", "58", "39", null, null, this);
            obj.set_taborder("140");
            obj.set_image("URL('img::skin_gray.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "163", "247", "58", "15", null, null, this);
            obj.set_taborder("141");
            obj.set_text("블루");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 250, 325, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("테마 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("TemaPop.xfdl", "lib::Comm.xjs");
        this.registerScript("TemaPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : TemaPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Tema 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            if(application.gv_activeApp == "AMS"){
        		if(this.gfn_isNull(application.gv_temaAms)) this.rdo_tema.set_value(1);
        		else this.rdo_tema.set_value(application.gv_temaAms);
            }else if(application.gv_activeApp == "WMS"){
        		if(this.gfn_isNull(application.gv_temaWms)) this.rdo_tema.set_value(1);
        		else this.rdo_tema.set_value(application.gv_temaWms);
            }else if(application.gv_activeApp == "TMS"){
        		if(this.gfn_isNull(application.gv_temaTms)) this.rdo_tema.set_value(1);
        		else this.rdo_tema.set_value(application.gv_temaTms);
            }else if(application.gv_activeApp == "OMS"){
        		if(this.gfn_isNull(application.gv_temaOms)) this.rdo_tema.set_value(1);
        		else this.rdo_tema.set_value(application.gv_temaOms);
            }
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* cancel Button 실행 */
        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        /* rdo_tema_onitemchanged 실행 */
        this.rdo_tema_onitemchanged = function(obj,e)
        {
        	if(this.rdo_tema.value == 1) this.opener.gv_tema = "";
        	else this.opener.gv_tema = this.rdo_tema.value;
        	
        	this.opener.fn_setTema();
        }

        /* img_dark_onclick 실행 */
        this.img_onclick = function(obj,e)
        {
        	if(obj.name == "img_dark") this.rdo_tema.set_value("1");
        	else if(obj.name == "img_blue") this.rdo_tema.set_value("bl_");
        	else if(obj.name == "img_red") this.rdo_tema.set_value("3");
        	else if(obj.name == "img_green") this.rdo_tema.set_value("4");
        	else if(obj.name == "img_grey") this.rdo_tema.set_value("5");
        	else if(obj.name == "img_ddd") this.rdo_tema.set_value("6");
        	
        	if(this.rdo_tema.value == 1) this.opener.gv_tema = "";
        	else this.opener.gv_tema = this.rdo_tema.value;
        	
        	this.opener.fn_setTema();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.rdo_tema.addEventHandler("onitemchanged", this.rdo_tema_onitemchanged, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_cancel_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.img_dark.addEventHandler("onclick", this.img_onclick, this);
            this.img_blue.addEventHandler("onclick", this.img_onclick, this);
            this.img_red.addEventHandler("onclick", this.img_onclick, this);
            this.img_green.addEventHandler("onclick", this.img_onclick, this);
            this.img_grey.addEventHandler("onclick", this.img_onclick, this);
            this.img_ddd.addEventHandler("onclick", this.img_onclick, this);

        };

        this.loadIncludeScript("TemaPop.xfdl", true);

       
    };
}
)();
