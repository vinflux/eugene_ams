﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CommCodePop");
                this.set_classname("DynamicCondPop");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,550,620);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_list", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_rtn", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("grd_list", "absolute", "10", "90", null, null, "10", "10", this);
            obj.set_taborder("1");
            obj.set_binddataset("ds_list");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"530\"/></Columns><Rows><Row size=\"90\"/></Rows><Band id=\"body\"><Cell style=\"align: ;\" text=\"bind:Column1\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("stc_bg", "absolute", "0", "0", null, "80", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("Search");
            obj.set_cssclass("sta_pop_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closePopup", "absolute", null, "0", "70", "80", "0", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_pop_close");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "0", "200", "10", "136", null, null, this);
            obj.set_taborder("7");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "8", "208", "10", "136", null, null, this);
            obj.set_taborder("8");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "158", "80", "142", "10", null, null, this);
            obj.set_taborder("9");
            obj.set_visible("false");
            obj.style.set_background("#ff000033");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 550, 620, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("DynamicCondPop");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CommComboPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CommComboPop.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/

        /***********************************************************************
         * Form Function
         ************************************************************************/
        // Form Load 시 공통 기능 처리
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);

        	this.ds_list.clearData();
        	if (!this.gfn_isNull(this.arguments.comboDs.keystring)) this.ds_list.keystring = this.arguments.comboDs.keystring;

        	this.ds_list.copyData(this.arguments.comboDs);

        	var objFunction = this.opener.fn_setWebControlVisible;
        	if (!this.gfn_isNull(objFunction)) objFunction.call(this.opener, false);

        	this.grd_list.setCellProperty("body", 0, "text", "bind:" + this.arguments.dataCol);
        }

        /***********************************************************************************
        * Transaction Function
        ***********************************************************************************/

        /***********************************************************************************
        * CallBack Event (strSvcId - Sevice ID, nErrorCode - ErrorCode, strErrorMsg - Error Message)
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        this.btn_closePop_onclick = function(obj,e)
        {
        	var objFunction = this.opener.fn_setWebControlVisible;
        	if (!this.gfn_isNull(objFunction)) objFunction.call(this.opener, true);

        	this.gfn_popupClose();
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        this.grd_list_oncellclick = function(obj,e)
        {
        	if (e.row < 0 || e.cell < 0) return;

        	var objFunction = this.opener.fn_setWebControlVisible;
        	if (!this.gfn_isNull(objFunction)) objFunction.call(this.opener, true);

        	var sRtnVal = String(this.ds_list.getColumn(e.row, this.arguments.codeCol));

        	this.gfn_popupClose(sRtnVal);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.grd_list.addEventHandler("oncellclick", this.grd_list_oncellclick, this);
            this.btn_closePopup.addEventHandler("onclick", this.btn_closePop_onclick, this);

        };

        this.loadIncludeScript("CommComboPop.xfdl", true);

       
    };
}
)();
