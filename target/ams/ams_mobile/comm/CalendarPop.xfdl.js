﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CalendarPopup");
                this.set_classname("CompGuide01");
                this._setFormPosition(0,0,589,699);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Calendar("cal_dateSel", "absolute", "0", "0", "589", "699", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("41");
            obj.set_type("monthonly");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 589, 699, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CalendarPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CalendarPop.xfdl", function(exports) {
        /***********************************************************************
         * 01. 업무구분 : Hidden Frame
         * 02. 메뉴명   : frm_Hidden.xfdl
         * 03. 메뉴설명 :
         * 04. 작성일   :
         * 05. 작성자   :
         * 06. 수정이력 :
         ***********************************************************************
         *     수정일     작성자   내용
         ***********************************************************************
         *
         ***********************************************************************
         */

        /***********************************************************************
         * Script Include
         ************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************
         * Form 변수 선언부
         ************************************************************************/

        /***********************************************************************
         * Form Function
         ************************************************************************/
        this.form_onload = function(obj,e)
        {
        	this.gfn_init(obj);
        	var sSelDate = this.arguments.selDate;

        	if (!this.gfn_isNull(sSelDate)) this.cal_dateSel.set_value(sSelDate);
        }

        
        this.cal_dateSel_ondayclick = function(obj,e)
        {
        	this.gfn_popupClose(e.date);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.cal_dateSel.addEventHandler("ondayclick", this.cal_dateSel_ondayclick, this);

        };

        this.loadIncludeScript("CalendarPop.xfdl", true);

       
    };
}
)();
