﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("FilterPop");
                this.set_classname("Guide11");
                this.set_titletext("데이타 필터");
                this._setFormPosition(0,0,300,500);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_filter", this);
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"FILTER\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_close", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "22", "14", "200", "20", null, null, this);
            obj.set_taborder("1");
            obj.set_text("User Filter");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ok", "absolute", null, null, "52", "28", "156", "23", this);
            obj.set_taborder("3");
            obj.set_text("Ok");
            obj.set_cssclass("btn_p");
            obj.set_tooltiptext("Save");
            this.addChild(obj.name, obj);

            obj = new Button("btn_cancel", "absolute", null, null, "60", "28", "91", "23", this);
            obj.set_taborder("11");
            obj.set_cssclass("btn_p");
            obj.set_tooltiptext("Cancle");
            obj.set_text("Cancel");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_filter", "absolute", "22", "52", null, null, "22", "66", this);
            obj.set_taborder("52");
            obj.set_binddataset("ds_filter");
            obj.set_autoenter("none");
            obj.set_selecttype("row");
            obj.set_autofittype("col");
            obj.set_nodatatext("no data found");
            obj.set_treeusecheckbox("false");
            obj.set_treeinitstatus("collapse,null");
            obj.style.set_font("9 arial");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\"/><Column size=\"225\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"23\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\" style=\"align:center middle;\"/><Cell col=\"1\" style=\"align:center;\" text=\"Filter Nm\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"align:center;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:FILTER\" editdisplay=\"edit\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("53");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 300, 500, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Guide11");
            		p.set_titletext("데이타 필터");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("FilterPop.xfdl", "lib::Comm.xjs");
        this.registerScript("FilterPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : FilterPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Filter 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_filterDsP;
        var gv_filterColP = "";;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_filterDsP = this.gfn_isNullEmpty(this.parent.argFilterDs);
            gv_filterColP = this.gfn_isNullEmpty(this.parent.argColNm);
            
            this.fn_filter();
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* 데이타셋 초기화
        * @param
        * @return
        */
        this.fn_filter = function()
        {
            var nRow = 0;
            
            for(var i = 0 ; i < this.gv_filterDsP.rowcount ; i++){
                nRow = this.ds_filter.getCaseCount(gv_filterColP + "=='" + this.gv_filterDsP.getColumn(i, gv_filterColP) + "'");
                
                if(nRow == 0){
                    this.ds_filter.addRow();
                    this.ds_filter.setColumn(this.ds_filter.rowposition, "FILTER", this.gv_filterDsP.getColumn(i, gv_filterColP));
                }
            }
            
            this.ds_filter.set_keystring("S:+FILTER");
            this.ds_filter.set_rowposition(0);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_ok_onclick 실행 */
        this.btn_ok_onclick = function(obj,e)
        {
            var nRow = this.ds_filter.findRow("CHK", "1");
            var sFilter = "";
            
            if(nRow != -1){
                for(var i = 0 ; i < this.ds_filter.rowcount ; i++){
                    if(this.ds_filter.getColumn(i, "CHK")){
                        if(sFilter == "") sFilter = gv_filterColP + "=='" + this.ds_filter.getColumn(i, "FILTER") + "'";
                        else sFilter += " || " + gv_filterColP + "=='" + this.ds_filter.getColumn(i, "FILTER") + "'";
                    }
                }
                
                this.gv_filterDsP.filter(sFilter);
            }
            
            this.close();
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        /* btn_cancle_onclick 실행 */
        this.btn_cancle_onclick = function(obj,e)
        {
            this.close();
        }

        /* grd_filter_oncellclick 실행 */
        this.grd_filter_oncellclick = function(obj,e)
        {
        	var colId = "";
        	
        	if(e.col == 1){
        		if(this.ds_filter.getColumn(e.row, "CHK") == 0 || this.gfn_isNull(this.ds_filter.getColumn(e.row, "CHK"))) this.ds_filter.setColumn(e.row, "CHK", 1);
        		else this.ds_filter.setColumn(e.row, "CHK", 0);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_ok.addEventHandler("onclick", this.btn_ok_onclick, this);
            this.btn_cancel.addEventHandler("onclick", this.btn_cancle_onclick, this);
            this.grd_filter.addEventHandler("oncellclick", this.grd_filter_oncellclick, this);

        };

        this.loadIncludeScript("FilterPop.xfdl", true);

       
    };
}
)();
