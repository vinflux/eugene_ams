<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.net.*" %>
<%@ page import="java.util.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.lang.reflect.*" %>

<%
	String udsName = request.getParameter("udsName");
	String reportParam = request.getParameter("reportParam");
	String url = request.getRequestURL().toString();
%>
<html>
<head>
</head>
<body>
	<jsp:forward page="/nexacro/ubiHandlerAdapter.nx">
		<jsp:param name="url" value="<%=url %>" />
		<jsp:param name="udsName" value="<%=udsName %>" />
		<jsp:param name="reportParam" value="<%=reportParam %>" />
	</jsp:forward>	
</body>
</html>