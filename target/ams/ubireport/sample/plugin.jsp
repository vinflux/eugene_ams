<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf8"%>
<%

%>    
<html>
<head>
<title>UbiReport Sample Code</title>
<script type="text/javascript" src="/adm/ubireport/plugin/ubiplugin.js"></script>
<script language='javascript'>
<!--

	var host = self.location.host;				// ip:port
	var app = "adm";							// WebApplication URI.
	var url = "http://" + host + "/" + app;		// WebApplication URL.
	var jrf_dir = url + "/ubireport/work/";	// jrf Directory.
	var jrf = "ubi_sample.jrf";					// 리포트파일명.
	var ds  = "jdbc/tutorial";					// data 연결부(현재 사용 하지 않음).

	var w_gap = 12;	// 가로 크기 조정.
	var h_gap = 12;	// 세로 크기 조정.

	function getArg() {// 아규먼트 값 설정. 자동 호출됨.
	
		var args = 'arg1#arg1_value1#arg2#arg2_value#...#argn#argn_value#';
		return args;
	}

	function Ubi_Init(){ // 사용할 이벤트 등록

		//UBI_AddEvent(document.getElementById('UbiViewer'), 'OnKeyDown',function(iKey,iShift,iCtrl,iAlt){Alert("OnKeyDown 이벤트가 발생하였습니다. 키값:"+iKey+" SHIFT:"+iShift+" CTRL:"+iCtrl+" ALT:"+iAlt)});
		//UBI_AddEvent(document.getElementById('UbiViewer'), 'OnPageMove', function(page){Alert("OnPageMove 이벤트가 발생하였습니다. 페이지이동:"+page);});
		UBI_AddEvent(document.getElementById('UbiViewer'), 'PrintEnd', function(){Alert("PrintEnd 이벤트가 발생하였습니다.");});
	}

	function Ubi_Resize() {// 브라우저 리사이즈 시 오브젝트 크기 조정.

		var w = ((self.innerWidth || (document.documentElement && document.documentElement.clientWidth) || document.body.clientWidth)) - w_gap;
		var h = ((self.innerHeight || (document.documentElement && document.documentElement.clientHeight) || document.body.clientHeight)) - h_gap;
		document.getElementById("UbiViewer").width = w + 'px';
		document.getElementById("UbiViewer").height = h + 'px';
	}

//-->
</script>
</head>
<body style='margin:0px' onload='Ubi_Init()' onresize='Ubi_Resize()'>	
<script>
<!--

	if( Ubi_PluginCheck() ) {

		var w = ((self.innerWidth || (document.documentElement && document.documentElement.clientWidth) || document.body.clientWidth)) - w_gap;
		var h = ((self.innerHeight || (document.documentElement && document.documentElement.clientHeight) || document.body.clientHeight)) - h_gap;

		//UbiViewer Object
		document.write("<object id='UbiViewer' type='" + Ubi_PluginType() + "' width='" + w + "px' height='" + h + "px'>");
		document.write("	<param name='ocxtype'				value='UNICODE'>");					// OCX 유형
		document.write("	<param name='fileURL'				value='" + url + "/ubireport/'>");	// 리포트에서 사용되는 이미지 또는 공통 아이템 정보를 가져오기위한 정보.
		document.write("	<param name='servletRootURL'		value='" + url + "'>");				// 서블릿 UDS 사용 시 필요함.
		//document.write("	<param name='servletURL1'			value='" + url + "/UbiForm'>");		// Form 서블릿 URL.
		document.write("	<param name='servletURL1'			value=''>");		// Form 서블릿 URL.
		document.write("	<param name='servletURL2'			value='" + url + "/UbiData'>");		// Data 서블릿 URL.
		document.write("	<param name='jrfFileDir'			value='" + jrf_dir + "'>");			// 리포트 파일 위치, 서버절대경로인 경우 위에서 servletURL1 정보 필요함. URL경로인 경우 servletURL1의 value를 공백으로 해야함.
		document.write("	<param name='jrfFileName'			value='" + jrf + "'>");				// 리포트 파일명.
		document.write("	<param name='dataSource'			value='" + ds + "'>");				// WAS에 설정된 DataSource 명.
		document.write("	<param name='execType'				value='TYPE4'>");					// 실행형태, TYPE2 : 팝업, TYPE4 : 브라우저 임베디드.  
		document.write("	<param name='margin'				value='true'>");					// 여백 마크 보임 여부 속성.
		document.write("	<param name='scale'					value='100'>");						// 최초 실행 시 배율.
		document.write("	<param name='progress'				value='true'>");					// 프로그래스바 보임 여부 속성.
		document.write("	<param name='toolbar'				value='true'>");					// 툴바 보임 여부 속성.
		document.write("	<param name='fontRevision'			value='true'>");					// 폰트보정. 변경 불가.
		document.write("	<param name='printMarginRevision'	value='true'>");					// 출력보정. 변경 불가.
		document.write("	<param name='isDeflater'			value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='isUnicode'				value='false'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='isBase64'				value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='utfData'				value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("</object>");
	}
//-->
</script>
</body>
</html>