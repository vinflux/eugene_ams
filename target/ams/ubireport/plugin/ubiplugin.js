//====================================================================================================
// 플러그인 설치 정보를 기입합니다. 
// (모든 변수와 함수는 JavaScript 응용 프로그램과의 충돌을 막기 위하여 Ubi_ 로 시작하였습니다.)
//====================================================================================================
var Ubi_Host = self.location.host;						// ip:port
var Ubi_App = "ams";									// WebApplication 명.
var Ubi_BaseUrl = "http://" + Ubi_Host + "/" + Ubi_App;	// WebApplication URL.
var Ubi_InstallSetupPath = Ubi_BaseUrl + "/ubireport/UbiPluginSetup.exe";	// 서버에 업로드된 플러그인 URL 절대경로 (또는 플러그인 다운로드가 포함되어 있는 웹페이지)
var Ubi_UpgradeSetupPath = Ubi_BaseUrl + "/ubireport/UbiPluginSetup.exe";	// 서버에 업로드된 플러그인 URL 절대경로 (또는 플러그인 다운로드가 포함되어 있는 웹페이지)
var Ubi_InstallConfirm = "UbiReport 전용 플러그인의 설치가 필요합니다. 지금 설치하시겠습니까?";		// 설치확인 메세지
var Ubi_UpgradeConfirm = "UbiReport 플러그인의 업데이트가 필요합니다.\n\n설치후에는 반드시 브라우저를 재시작해야 합니다. 지금 설치하시겠습니까?";	// 설치확인 메세지
// 다운로드 안내메세지가 "" 로 지정되면 안내말이 표시되지 않고 바로 다운로드 진행됩니다.
var Ubi_InstallMessage = "잠시후 다운로드가 완료되면 설치 안내문에 따라 설치하여 주십시오.";
var Ubi_UpgradeMessage = "잠시후 다운로드가 완료되면 설치 안내문에 따라 설치하여 주십시오.\n\n설치 후에는 반드시 브라우저를 재시작해야 합니다.";
var Ubi_PluginVersion ="2.502.1411.2501";//"2.502.1409.1603";
//====================================================================================================
// [사용자 편의 함수] 플러그인 개체 생성 함수 - 사용 환경에 맞게 변경하셔도 됩니다. (아래 플러그인 모듈 프로그램은 제거하시면 안됩니다.)
//====================================================================================================
//
// CreatePluginObject({id:'UbiViewer'
//	, width:'80%', height:'80%'
//  , isdrm:'false'
//	, jrffiledir:'/user/web/bcqre/ubireport/work/'
//	, jrffilename:'ubi_sample.jrf'
//	, datasource:'DBID#dataSource'
//	, fileurl:'http://bcqre.ubireport.com/ubireport/'
//	, servletrooturl:'http://bcqre.ubireport.com/'
//	, servleturl1:'http://bcqre.ubireport.com/UbiForm'
//	, servleturl2:'http://bcqre.ubireport.com/UbiData'
//	, exectype:'TYPE4'
//	, margin:'true'
//	, scale:'100'
//	, progress:'true'
//	, toolbar:'true'
//	, fontRevision:'true'
//	, isdeflater:'true'
//	, isunicode:'false'
//	, isbase64:'false'
//	, utfdata:'false'
//	, ubiserverurl:'http://bcqre.ubireport.com/UbiServer'
//	, docname:'default'
//	, pagenames:'default$'
//	, cdposition:'20,267'
//	, BarcodePosition:'51,267'
// });
//
//====================================================================================================

// 플러그인을 표시하기 위한 사용자 전용 함수입니다.(불필요시 삭제 가능)
function CreatePluginObject(config)
{
		var objid = 'UbiViewer';	
		var objwidth= '100%';	
		var objheight = '100%';	

		if(config['id']){objid = config['id'];}
		if(config['width']){objid = config['width'];}
		if(config['height']){objid = config['height'];}
		
		document.write("<object id='"+objid+"' type='"+Ubi_PluginType()+"' width='"+objwidth+"' height='"+objheight+"'>");

		if(config['Isdrm']){
			document.write("<param name='IsDrm' value='"+config['Isdrm']+"'>");
		}
		if(config['jrffiledir']){
			document.write("<param name='jrfFileDir' value='"+config['jrffiledir']+"'>");
		}
		if(config['jrffilename']){
			document.write("<param name='jrfFileName' value='"+config['jrffilename']+"'>");
		}
		if(config['datasource']){
			document.write("<param name='dataSource' value='"+config['datasource']+"'>");
		}

		if(config['fileurl']){
			document.write("<param name='fileURL' value='"+config['fileurl']+"'>");
		}
		if(config['servletrooturl']){
			document.write("<param name='servletRootURL' value='"+config['servletrooturl']+"'>");
		}
		if(config['servleturl1']){
			document.write("<param name='servletURL1' value='"+config['servleturl1']+"'>");
		}
		if(config['servleturl2']){
			document.write("<param name='servletURL2' value='"+config['servleturl2']+"'>");
		}
		if(config['ocxtype']){
			document.write("<param name='ocxtype' value='"+config['ocxtype']+"'>");
		}
		if(config['exectype']){
			document.write("<param name='execType' value='"+config['exectype']+"'>");
		}
		if(config['margin']){
			document.write("<param name='margin' value='"+config['margin']+"'>");
		}
		if(config['scale']){
			document.write("<param name='scale' value='"+config['scale']+"'>");
		}
		if(config['progress']){
			document.write("<param name='progress' value='"+config['progress']+"'>");
		}
		if(config['toolbar']){
			document.write("<param name='toolbar' value='"+config['toolbar']+"'>");
		}
		if(config['fontrevision']){
			document.write("<param name='fontRevision' value='"+config['fontrevision']+"'>");
		}
		if(config['isdeflater']){
			document.write("<param name='isDeflater' value='"+config['isdeflater']+"'>");
		}
		if(config['isunicode']){
			document.write("<param name='isUnicode' value='"+config['isunicode']+"'>");
		}
		if(config['isbase64']){
			document.write("<param name='isBase64' value='"+config['isbase64']+"'>");
		}
		if(config['utfdata']){
			document.write("<param name='utfData' value='"+config['utfdata']+"'>");
		}
		if(config['ubiserverurl']){
			document.write("<param name='ubiServerURL' value='"+config['ubiserverurl']+"'>");
		}
		if(config['docname']){
			document.write("<param name='DocName' value='"+config['docname']+"'>");
		}
		if(config['pagenames']){
			document.write("<param name='PageNames' value='"+config['pagenames']+"'>");
		}
		if(config['cdposition']){
			document.write("<param name='CDPosition' value='"+config['cdposition']+"'>");
		}
		if(config['barcodeposition']){
			document.write("<param name='BarcodePosition' value='"+config['barcodeposition']+"'>");
		}
		document.write("</object>");
}

//====================================================================================================
// 플러그인 모듈 프로그램 - 아래를 수정하지 마십시오.
//====================================================================================================
var Ubi_currentInstalledVersion = "";
function Ubi_PluginCheck()
{
	if(Ubi_InstallCheck(Ubi_PluginType(), "UbiDecision.UbiPlugin.1"))
	{
		if(Ubi_UpgradeCheck())
		{
			return true;
		}
		else
		{
			if(confirm(Ubi_UpgradeConfirm+"\n\n설치된 버전 : "+Ubi_currentInstalledVersion+"\n\n업그레이드 버전 :"+Ubi_PluginVersion))
			{
				if(Ubi_UpgradeMessage!='')
				{
					alert(Ubi_UpgradeMessage); // 설치전이라 Alert사용금지
				}
				window.open(Ubi_UpgradeSetupPath,'_self'); // 설치 프로그램 유도
			}
			return true; //플러그인 업그레이드 필요성에 따라 표시안함
		}
	}
	else
	{
		if(confirm(Ubi_InstallConfirm))
		{
			if(Ubi_InstallMessage!='')
			{
				alert(Ubi_InstallMessage); // 설치전이라 Alert사용금지
			}
				window.open(Ubi_InstallSetupPath,'_self'); // 설치 프로그램 유도
		}
		return false; //플러그인 처음설치 필요성에 따라 표시안함
	}
}
function Ubi_InstallCheck(mimetype, progid)
{
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';
	var isChrome = !!window.chrome && !isOpera;
	var isIE = /*@cc_on!@*/false;
	isIE = isIE || navigator.userAgent.indexOf('Trident') > 0;
	
	if(isIE){
		try{
			var plugin = new ActiveXObject(progid);
			Ubi_currentInstalledVersion = plugin.GetVersion();
			return true;
		}catch (e){
			return false;
		}
	}else{
		var plugins = navigator.plugins;
		if(plugins == null || plugins.length == 0) return false;
		for(var i=0;i<plugins.length;i++)
		{
			if (typeof(plugins[i][0]) == "undefined") continue;
			var type = plugins[i][0].type;
			
			if(type == mimetype){
				Ubi_currentInstalledVersion = plugins[i].version;
				if(Ubi_currentInstalledVersion==undefined) // Chrome/Safari/Opera
				{
					var obj = document.createElement("div");
					var objtag = "<object id='UbiViewerPluginTest' type='"+Ubi_PluginType()+"' width='1' height='1'></object>";
					obj.innerHTML = objtag;
					document.documentElement.appendChild(obj);
					Ubi_currentInstalledVersion = document.getElementById('UbiViewerPluginTest').GetVersion();
					document.documentElement.removeChild(obj);
				}
				return true;
			}
		}
		return false;
	}
}

function Ubi_UpgradeCheck()
{
	if(Ubi_PluginVersion == Ubi_currentInstalledVersion) return true;
	var ver1 = Ubi_currentInstalledVersion.split(".");
	var ver2 = Ubi_PluginVersion.split(".");

	if(parseInt(ver1[0])>parseInt(ver2[0])) {return true;}
	else if(parseInt(ver1[0])<parseInt(ver2[0])) {return false;}
	else if(parseInt(ver1[1])>parseInt(ver2[1])) {return true;}
	else if(parseInt(ver1[1])<parseInt(ver2[1])) {return false;}
	else if(parseInt(ver1[2])>parseInt(ver2[2])) {return true;}
	else if(parseInt(ver1[2])<parseInt(ver2[2])) {return false;}
	else if(parseInt(ver1[3])>parseInt(ver2[3])) {return true;}
	return false;
}

function Ubi_AddEvent(obj, name, func){if(obj){if(obj.attachEvent){obj.attachEvent("on"+name,func);}else{obj.addEventListener(name,func,false);}}}
function Alert(msg, title){
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	var isFirefox = typeof InstallTrigger !== 'undefined';
	
	if(!isSafari && !isFirefox){alert(msg);return;}
	if(!title){
		title='';
	}
	var obj = document.createElement("div");
	var objtag = "<object id='UbiViewerPluginTest' type='"+Ubi_PluginType()+"' width='1' height='1' style='display:hidden'></object>";
	obj.innerHTML = objtag;
	document.documentElement.appendChild(obj);
	Ubi_currentInstalledVersion = document.getElementById('UbiViewerPluginTest').Alert(msg,title);
	document.documentElement.removeChild(obj);
}

function Ubi_PluginType()
{
	return "application/x-ubiplugin";
}
