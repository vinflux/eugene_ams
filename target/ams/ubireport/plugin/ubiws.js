//----------------------------------------------------------------------------------------------------
// UbiViewerWS 정보 
// ubiWS_port, ubiWS_protocol 수정 불가
//----------------------------------------------------------------------------------------------------
var ubiWS_Name = "UbiViewerWS";
var ubiWS_Version = "2, 502, 1508, 3101";
var ubiWS_port = "6886";
var ubiWS_protocol = "ubi-protocol";

//var installpageURL = "./install_ubiws.html";
var Ubi_App = "ams";
var Ubi_Host = self.location.host;	// ip:port
var Ubi_BaseUrl = "http://" + Ubi_Host + "/" + Ubi_App; // WebApplication URL.
var installpageURL = Ubi_BaseUrl + "/ubireport/plugin/install_ubiws.html";	// 디자인 화면

var Ubi_isIE = false;		
var Ubi_isIE10 = false;		//IE Version 10 미만
var Ubi_isOpera = false;
var Ubi_isFirefox = false;
var Ubi_isSafari = false;
var Ubi_isChrome = false;

var Ubi_isIE = /*@cc_on!@*/false || !!document.documentMode;
var Ubi_isIE10 = document.all && !window.atob;
var Ubi_isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var Ubi_isFirefox = typeof InstallTrigger !== 'undefined';
var Ubi_isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
var Ubi_isChrome = !!window.chrome && !Ubi_isOpera;

var match = navigator.userAgent.match(/(CrOS\ \w+|Windows\ NT|Mac\ OS\ X|Linux)\ ([\d\._]+)?/);
var os = (match || [])[1] || "Unknown";
var osVersion = (match || [])[2] || "Unknown";

var _isSupportWS = true; // 해당 브라우저가 Websocket(이하 WS)를 지원하는 지 여부
var _rs_ = String.fromCharCode(30);
var _cs_ = String.fromCharCode(31);
var _eof_ = String.fromCharCode(29);

if (!window.WebSocket)
{
	_isSupportWS = false;
};

function checkSupportWS() {
	if (!window.WebSocket)
		return false;
	
	return true;
};

var UbiViewer = function(websocket) {
	var ws = websocket;
	
	// property
	this.fileurl = "";
	this.servleturl1 = "",
	this.servleturl2 = "",
	this.jrffiledir = "",
	this.jrffilename = "",
	this.datasource = "",
	this.scale = "100",
	this.resource = "fixed",
	this.invisibletoolbar = "",
	this.rowdim = "",
	this.coldim = "",
	this.margin = "",
	this.progress = "",
	this.toolbar = "",
	this.isdeflater = "true",
	this.isunicode = "false",
	this.utfdata = "true",
	this.isbase64 = "true",
	this.popupmenu = "",
	this.servletrooturl = "",
	this.exportfilename = "",
	this.exportds = "",
	this.invisibleexporttypes = "",
	this.isurf = "",
	this.printcopies = "1",
	this.printleftmargin = "",
	this.printtopmargin = "",
	this.isencrypt = "",
	this.ubiserverurl = "",
	this.barcodeposition = "",
	this.cdposition = "",
	this.docname = "",
	this.pagenames = "",
	this.isdrm = "",
	this.reportid = "",
	this.printmarginrevision = "",
	this.printsetmode = "",
	this.reporttitle = "",
	this.printautofit = "",
	this.fontrevision = "true",
	this.exportsetmode = "",
	this.ismultireport = "false",
	this.multicount = "1",
	this.arg = "",
	this.reqtype = "retrieve",
	this.variable = "",
	this.resize = "",
	
	// method
	this.retrieve = function() {
		//alert('retrieve');
		
		// UbiServer와 연결되어 있는 지 체크
		//if (this.checkWebSocket() == false)
		//	return;
		
		var msg = "reqtype" + _cs_ + "retrieve" + _rs_
			+ "fileurl" + _cs_ +	this.fileurl + _rs_
			+ "servleturl1" + _cs_ +	this.servleturl1 + _rs_
			+ "servleturl2" + _cs_ +	this.servleturl2 + _rs_
			+ "jrffiledir" + _cs_ +	this.jrffiledir + _rs_
			+ "jrffilename" + _cs_ +	this.jrffilename + _rs_
			+ "datasource" + _cs_ +	this.datasource + _rs_
			+ "scale" + _cs_ +	this.scale + _rs_
			+ "resource" + _cs_ +	this.resource + _rs_
			+ "invisibletoolbar" + _cs_ +	this.invisibletoolbar + _rs_
			+ "rowdim" + _cs_ +	this.rowdim + _rs_
			+ "coldim" + _cs_ +	this.coldim + _rs_
			+ "margin" + _cs_ +	this.margin + _rs_
			+ "progress" + _cs_ +	this.progress + _rs_
			+ "toolbar" + _cs_ +	this.toolbar + _rs_
			+ "isdeflater" + _cs_ +	this.isdeflater + _rs_
			+ "isunicode" + _cs_ +	this.isunicode + _rs_
			+ "utfdata" + _cs_ +	this.utfdata + _rs_
			+ "isbase64" + _cs_ +	this.isbase64 + _rs_
			+ "popupmenu" + _cs_ +	this.popupmenu + _rs_
			+ "servletrooturl" + _cs_ +	this.servletrooturl + _rs_
			+ "exportfilename" + _cs_ +	this.exportfilename + _rs_
			+ "exportds" + _cs_ +	this.exportds + _rs_
			+ "invisibleexporttypes" + _cs_ +	this.invisibleexporttypes + _rs_
			+ "isurf" + _cs_ +	this.isurf + _rs_
			+ "printcopies" + _cs_ +	this.printcopies + _rs_
			+ "printleftmargin" + _cs_ +	this.printleftmargin + _rs_
			+ "printtopmargin" + _cs_ +	this.printtopmargin + _rs_
			+ "isencrypt" + _cs_ +	this.isencrypt + _rs_
			+ "ubiserverurl" + _cs_ +	this.ubiserverurl + _rs_
			+ "barcodeposition" + _cs_ +	this.barcodeposition + _rs_
			+ "cdposition" + _cs_ +	this.cdposition + _rs_
			+ "docname" + _cs_ +	this.docname + _rs_
			+ "pagenames" + _cs_ +	this.pagenames + _rs_
			+ "isdrm" + _cs_ +	this.isdrm + _rs_
			+ "reportid" + _cs_ +	this.reportid + _rs_
			+ "printmarginrevision" + _cs_ +	this.printmarginrevision + _rs_
			+ "printsetmode" + _cs_ +	this.printsetmode + _rs_
			+ "reporttitle" + _cs_ +	this.reporttitle + _rs_
			+ "printautofit" + _cs_ +	this.printautofit + _rs_
			+ "fontrevision" + _cs_ +	this.fontrevision + _rs_
			+ "exportsetmode" + _cs_ +	this.exportsetmode + _rs_
			+ "ismultireport" + _cs_ +	this.ismultireport + _rs_
			+ "multicount" + _cs_ +	this.multicount + _rs_
			+ "arg" + _cs_ + this.arg + _rs_;
		
		// SetVariable을 통해 설정한 속성
		if (this.variable != '') {
			msg += this.variable;
			this.variable = '';
		}
		
		if (this.resize != '') {
			//msg += this.resize;
			msg = this.resize + msg;
			this.resize = '';
		}
		
		this.ubiWS_Send(msg);
		sleep(100);
	};
	
	this.ExportFile = function(s1) {
		
		var filetype = s1;
		var msg = "reqtype" + _cs_ + ("exportfile#"+filetype) + _rs_
				+ "exportfilename" + _cs_ + this.exportfilename + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.exportset = function() {
		var msg = "reqtype" + _cs_ + "exportset" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.exportReport = function(s1, s2, s3) {
		var jrffilename = s1;
		var args = s2;
		var exporttype = s3;
		
		var msg = "reqtype" + _cs_ + "exportreport" + _rs_
				+ "jrffilename" + _cs_ + jrffilename + _rs_
				+ "args" + _cs_ + args + _rs_
				+ "exporttype" + _cs_ + exporttype + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.exportsetReport = function(s1, s2, s3) {
		var jrffilename = s1;
		var args = s2;
		var exporttype = s3;
		
		var msg = "reqtype" + _cs_ + "exportsetreport" + _rs_
				+ "jrffilename" + _cs_ + jrffilename + _rs_
				+ "args" + _cs_ + args + _rs_
				+ "exporttype" + _cs_ + exporttype + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.print = function() {
		var msg = "reqtype" + _cs_ + "print" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.printset = function() {
		var msg = "reqtype" + _cs_ + "printset" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.directPrint = function() {
		var msg = "reqtype" + _cs_ + "directprint" + _rs_;
		this.ubiWS_Send(msg);
		sleep(100);
		
		this.close();
	};
	
	this.printReport = function(s1, s2) {
		var jrffilename = s1;
		var args = s2;
		
		var msg = "reqtype" + _cs_ + "printreport" + _rs_
				+ "jrffilename" + _cs_ + jrffilename + _rs_
				+ "args" + _cs_ + args + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.printReportResizing = function(s1, s2, s3) {
		var jrffilename = s1;
		var args = s2;
		var papersize = s3;
		
		var msg = "reqtype" + _cs_ + "printreportresizing" + _rs_
				+ "jrffilename" + _cs_ + jrffilename + _rs_
				+ "args" + _cs_ + args + _rs_
				+ "papersize" + _cs_ + papersize + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.printsetReport = function(s1, s2, s3) {
		var jrffilename = s1;
		var args = s2;
		var pagenum = s3;
		
		var msg = "reqtype" + _cs_ + "printsetreport" + _rs_
				+ "jrffilename" + _cs_ + jrffilename + _rs_
				+ "args" + _cs_ + args + _rs_
				+ "pagenum" + _cs_ + pagenum + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.SetDataset = function(s1, s2) {
		var dsid = s1;
		var dsdata = s2;
	
		var msg = "reqtype" + _cs_ + "setdataset" + _rs_
				+ dsid + "#" + dsdata;
		this.ubiWS_Send(msg);
	};
	
	this.firstPage = function() {
		var msg = "reqtype" + _cs_ + "firstpage" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.previousPage = function() {
		var msg = "reqtype" + _cs_ + "previouspage" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.nextPage = function() {
		var msg = "reqtype" + _cs_ + "nextpage" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.lastPage = function() {
		var msg = "reqtype" + _cs_ + "lastpage" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.setPage = function(s1) {
		var page = s1;
		var msg = "reqtype" + _cs_ + ("setpage#" + page) + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.zoomIn = function() {
		var msg = "reqtype" + _cs_ + "zoomin" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.zoomOut = function() {
		var msg = "reqtype" + _cs_ + "zoomout" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.SetVariable = function(s1, s2) {
		this.variable += (s1 + _cs_ + s2 + _rs_);
	};
	
	this.refresh = function() {
		var msg = "reqtype" + _cs_ + "refresh" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.sendVariable = function() {
		if (this.variable == '')
			return;
		
		var msg = "reqtype" + _cs_ + "setvariable" + _rs_
					+ this.variable;
		this.ubiWS_Send(msg);
		this.variable = "";
	};
	
	// Viewer의 버전 정보를 확인한다.
	this.aboutBox = function() {
		var msg = "reqtype" + _cs_ + "aboutbox" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.checkWebSocket = function() {
		// 0 : CONNECTING
		// 1 : OPEN
		// 2 : CLOSING
		// 3 : CLOSED
		if (ws.readyState != 1) {
			alert('UbiViewerWS\uc640 \uc5f0\uacb0\ub418\uc9c0 \uc54a\uc558\uc2b5\ub2c8\ub2e4.');
			return false;
		}
		
		return true;
	};
	
	// UbiServerWS 전체에 대한 버전을 확인한다.
	this.checkVersion = function() {
		var msg = "reqtype" + _cs_ + "checkversion#" + ubiWS_Version + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.close = function() {
		var msg = "reqtype" + _cs_ + "close" + _rs_;
		this.ubiWS_Send(msg);
	};
	
	this.setResize = function(s1, s2, s3, s4) {
		var top = s1; // x
		var left = s2; // y
		var width = s3;
		var height = s4;
		
		this.resize = '';
		this.resize = "resize" + _cs_ + (left + "#" + top + "#" + width + "#" + height + "#") + _rs_;
	};
	
	this.ubiWS_Send = function(msg) {
		msg += _eof_;
		ws.send(msg);
	};
	
};

function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}


function CheckBrowser() {
	// --------------------------------------------------------------
    // WebSocket 가능 여부 체크
    // --------------------------------------------------------------	
    if (os === 'Windows NT' && get_browser() === 'Chrome' && get_browser_version() < 19) {
    	// Chrome 19 이상 지원 가능합니다.
        alert('Chrome 19 \uc774\uc0c1 \uc9c0\uc6d0 \uac00\ub2a5\ud569\ub2c8\ub2e4.');
        return false;
    }


    if (os === 'Windows NT' && get_browser() === 'Firefox' && get_browser_version() < 18) {
    	// Firefox 18 이상 지원 가능합니다.
        alert('Firefox 18 \uc774\uc0c1 \uc9c0\uc6d0 \uac00\ub2a5\ud569\ub2c8\ub2e4.');
        return false;
    }


    if (os === 'Windows NT' && get_browser() === 'Safari') {
    	// 해당 os의 Safari 미지원
        alert('\ud574\ub2f9 os\uc758 Safari \ubbf8\uc9c0\uc6d0');
        return false;
    }


    if (os === 'Windows NT' && get_browser() === 'Opera' && get_browser_version() < 15) {
    	// Opera 15 이상 지원 가능
        alert('Opera 15 \uc774\uc0c1 \uc9c0\uc6d0 \uac00\ub2a5\ud569\ub2c8\ub2e4.');
        return false;
    }
	 
	if (os === 'Mac OS X' || os === 'Linux') {
		// Mac OS 또는 Linux는 현재 지원하지 않습니다.
		alert('Mac OS \ub610\ub294 Linux\ub294 \ud604\uc7ac \uc9c0\uc6d0\ud558\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4.');
		return false;
		//alert("os :" + os + ", get_browser :" + get_browser() + ", get_browser_version :" +  get_browser_version() +", vstrDownURL :" +  vstrDownURL);        
    }
	
	return true;
}

function get_browser() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'MSIE';
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return 'Opera';
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[0];
}

function get_browser_version() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return tem[1];
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return tem[1];
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[1];
}

function InitWebSocket(callback) {

	sleep(1000);

	if (!checkSupportWS()) {
		// 현재 브라우저 버전에서는 UbiViewerWS를 사용할 수 없습니다.
		alert('\ud604\uc7ac \ube0c\ub77c\uc6b0\uc800 \ubc84\uc804\uc5d0\uc11c\ub294 UbiViewerWS\ub97c \uc0ac\uc6a9\ud560 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4.');
		return;
	}
	
	var checkTimer = setTimeout(stopCheck,5000);
	
	var ws = new WebSocket('ws://127.0.0.1:' + ubiWS_port, ubiWS_protocol);
	
	ws.onopen = function(e) {
		//alert('open : ' + ws.readyState);
		sleep(200);
		
		installflag = true;
		
		// version check
		var checkmsg = "reqtype" + _cs_ + "checkversion#" + ubiWS_Version + _rs_ + _eof_;
		ws.send(checkmsg);
	};

	ws.onmessage = function(e) {
		var message = e.data;
		//alert(message);
		var result = '';
		
		if (message.indexOf('RETRIEVEEND') != -1)
			RetrieveEnd();
		else if (message.indexOf('EXPORTEND') != -1) {
			result = message.substring(message.indexOf('#')+1, message.length);
			ExportEnd(result);
		}
		else if (message.indexOf('PRINTEND') != -1) {
			result = message.substring(message.indexOf('#')+1, message.length);
			PrintEnd(result);
		}
		else if (message.indexOf('CHECKVERSION') != -1) {
			result = message.substring(message.indexOf('#')+1, message.length);
			if (result == 'true') {
				//alert('nothing');
				// report viewing
				versionflag = true;
				if (callback) {
					callback(ws);
				}
			}	
			else {
				//alert('new install');
				// go to install page
				top.location.href = getInstallPage();
			}
		}
	};
	
	ws.onerror = function(e) {
		//alert('error');
		
	};
	
	ws.onclose = function(e) {
		//alert('close : ' + e.code);
		//alert(e.reason);
	};
}

var installflag = false;
var versionflag = false;
function stopCheck() {
	//alert('5sec');
	if(!installflag) {
		//alert('install page');
		top.location.href = getInstallPage();  
	}
}

function getInstallPage() {
    return installpageURL;
}

var macaddr = '';
function InitGetMacAddress() {
	
	if (!checkSupportWS()) {
		// 현재 브라우저 버전에서는 UbiViewerWS를 사용할 수 없습니다.
		alert('\ud604\uc7ac \ube0c\ub77c\uc6b0\uc800 \ubc84\uc804\uc5d0\uc11c\ub294 UbiViewerWS\ub97c \uc0ac\uc6a9\ud560 \uc218 \uc5c6\uc2b5\ub2c8\ub2e4.');
		return;
	}
	
	macaddr = '';
	
	var checkTimer = setTimeout(stopCheck,5000);
	
	var ws = new WebSocket('ws://127.0.0.1:' + ubiWS_port, ubiWS_protocol);
	
	ws.onopen = function(e) {
		//alert('open : ' + ws.readyState);
		sleep(200);
		
		installflag = true;
		
		// get Mac Address
		var msg = "reqtype" + _cs_ + "getmac" + _rs_ + _eof_;
		ws.send(msg);
	};
	
	ws.onmessage = function(e) {
		var message = e.data;
		//alert(message);
		
		ubi_SetMacAddress(message);
	};
	
	ws.onerror = function(e) {
	};
	
	ws.onclose = function(e) {
	};	
}

function ubi_GetMacAddress() {
	return macaddr;
}

function ubi_SetMacAddress(message) {
	
	macaddr = message;
//	alert('SetMacAddress : ' + macaddr);
}
