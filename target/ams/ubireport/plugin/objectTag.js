	if( Ubi_PluginCheck() ) {

		var w = ((self.innerWidth || (document.documentElement && document.documentElement.clientWidth) || document.body.clientWidth)) - w_gap;
		var h = ((self.innerHeight || (document.documentElement && document.documentElement.clientHeight) || document.body.clientHeight)) - h_gap;

		//UbiViewer Object
		document.write("<object id='UbiViewer' type='" + Ubi_PluginType() + "' width='" + w + "px' height='" + h + "px'>");
		document.write("	<param name='ocxtype'				value='UNICODE'>");					// OCX 유형
		document.write("	<param name='fileURL'				value='" + url + "/ubireport/'>");	// 리포트에서 사용되는 이미지 또는 공통 아이템 정보를 가져오기위한 정보.
		document.write("	<param name='servletRootURL'		value='" + url + "'>");				// 서블릿 UDS 사용 시 필요함.
		//document.write("	<param name='servletURL1'			value='" + url + "/UbiForm'>");		// Form 서블릿 URL.
		document.write("	<param name='servletURL1'			value=''>");		// Form 서블릿 URL.
		//document.write("	<param name='servletURL2'			value='" + url + "/UbiData'>");		// Data 서블릿 URL.
		document.write("	<param name='servletURL2'			value=''>");		// Data 서블릿 URL.
		document.write("	<param name='jrfFileDir'			value='" + jrf_dir + "'>");			// 리포트 파일 위치, 서버절대경로인 경우 위에서 servletURL1 정보 필요함. URL경로인 경우 servletURL1의 value를 공백으로 해야함.
		document.write("	<param name='jrfFileName'			value='" + jrf + "'>");				// 리포트 파일명.
		document.write("	<param name='dataSource'			value='" + ds + "'>");				// WAS에 설정된 DataSource 명.
		document.write("	<param name='execType'				value='" + execType +"'>");			// 실행형태, TYPE2 : 팝업, TYPE4 : 브라우저 임베디드.   
		document.write("	<param name='margin'				value='true'>");					// 여백 마크 보임 여부 속성.
		document.write("	<param name='scale'					value='100'>");						// 최초 실행 시 배율.
		document.write("	<param name='progress'				value='true'>");					// 프로그래스바 보임 여부 속성.
		document.write("	<param name='toolbar'				value='true'>");					// 툴바 보임 여부 속성.
		document.write("	<param name='fontRevision'			value='true'>");					// 폰트보정. 변경 불가.
		document.write("	<param name='printMarginRevision'	value='true'>");					// 출력보정. 변경 불가.
		document.write("	<param name='isDeflater'			value='false'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='isUnicode'				value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='isBase64'				value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("	<param name='utfData'				value='true'>");					// 서버와의 통신 옵션 (ubigateway.property의 속성과 같아야함).
		document.write("</object>");
	}