<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%pageContext.setAttribute("crlf", "\r\n"); %>
<title><c:out value='${title}'/></title>

<script type="text/javascript" src="<c:url value='/ubireport/plugin/ubiws.js' />"></script>
<script language="javascript" type="text/javascript">

<!--
	// 기본정보
	var protocol = self.location.protocol;
	var host = self.location.host;
	var url = protocol + "//" + host;
	var appUrl = '${appUrl}';
	var jrf_dir = '${jrfDir}';
	var jrf = '${jrfName}';	
	var ds  = "";
	var arg = '${strArg}';

////////////////////////////////////////////////////////////
	var viewer = null;

	// 웹소켓 초기화 및 미리보기 콜백
	InitWebSocket(ShowReport);
	
	// 미리보기 콜백 함수. 웹소켓 초기화가 정상적으로 처리되면 자동 호출 됨.
	function ShowReport(ws) {

		viewer = new UbiViewer(ws);
		viewer.servletrooturl = appUrl;
		viewer.servleturl1    = '';
		viewer.servleturl2    = appUrl + "/UbiData";
		viewer.jrffiledir     = jrf_dir;
		viewer.jrffilename    = jrf
		viewer.datasource     = ds;
		viewer.fontrevision   = "true";
		viewer.isdeflater     = "false";
		viewer.isunicode      = "true";
		viewer.utfdata        = "true";
		viewer.isbase64       = "true";
		viewer.arg            = arg;
		//viewer.ismultireport = "true";
		//viewer.multicount = "33";
		//viewer.multicount = "1";

		// 사이즈 위치 변경 시
		viewer.setResize('200', '200', '800', '700');

		viewer.retrieve();
	}

	function checkViewerVersion() {

		viewer.aboutBox();
	}

	function ExportFile_PDF() {

		viewer.exportfilename = "C:/Users/KKD/Desktop/test.pdf";
		viewer.ExportFile('pdf');
	}

	function ExportSet() {

		viewer.SetVariable('Export.Type','rtf');
		viewer.exportset();
	}


	function PrintSet() {

		viewer.printset();
	}

	function DirectPrint() {

		viewer.directPrint();
	}

	function RetrieveEnd() {

		//alert('RetriveEnd Event');
	}

	function ExportEnd(message) {

		/*var filepath = message;
		if (filepath.length > 0) {
			alert("Export success : " + filepath);
		} else {
			alert("Export fail");
		}*/
	}

	function PrintEnd(message) {
		var status = document.getElementById('UbiViewer').getPrintStatus();
		if ( status == 0 ) {
			var f = document.printFrm;	
			f.method = "post";
			f.action = "<c:out value='${appUrl}'/>/reportController/insertPrintLog.do";
			f.submit();
		}
	}

</script>
</head>
<body style='margin:0px;'>
<!--  
   <form>
   		<input onclick="ShowReport()" value="Preview" type="button"> 
		<input onclick="checkViewerVersion()" value="CheckVersion" type="button">
		<input onclick="ExportFile_PDF()" value="PDF" type="button">
		<input onclick="ExportSet()" value="ExportSet" type="button"><br>
		<input onclick="PrintSet()" value="PrintSet" type="button">
		<input onclick="DirectPrint()" value="DirectPrint" type="button"><br>
		
	</form>
	-->
<form name="printFrm" target="iframe_hidden">
<input type="hidden" name="ctkey" value="<c:out value='${ctkey}'/>">
<input type="hidden" name="wkprhstype" value="<c:out value='${wkprhstype}'/>">
<input type="hidden" name="sourcekey" value="<c:out value='${sourcekey}'/>">
<input type="hidden" name="uskey" value="<c:out value='${uskey}'/>">
</form>
<iframe name="iframe_hidden"  src="" style="width:0;height:0;visibility: hidden;"></iframe>

</body>
</html>
