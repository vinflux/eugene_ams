﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Betweenmonthfield");
                this.set_classname("style01");
                this.set_titletext("기간 년월");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,377,28);
            }
            this.style.set_background("transparent");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label", "absolute", "18", "0", "98", "28", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("sta_WF_label");
            obj.set_wordwrap("char");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_use", "absolute", "0", "3", "15", "21", null, null, this);
            obj.set_taborder("4");
            obj.set_value("true");
            obj.set_cssclass("chk_WF_Srh");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "243", "5", "10", "18", null, null, this);
            obj.set_taborder("6");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_to", "absolute", "254", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("9");
            obj.set_dateformat("yyyy-MM");
            obj.set_editformat("yyyy-MM");
            obj.style.set_cursor("hand");

            obj = new Calendar("cal_from", "absolute", "116", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("10");
            obj.set_dateformat("yyyy-MM");
            obj.set_editformat("yyyy-MM");
            obj.style.set_cursor("hand");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 377, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("기간 년월");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Betweenmonthfield.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Betweenmonthfield.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	var sToday = this.parent.parent.gfn_today();
        	this.cal_from.set_value(sToday.substr(0, 6));
        	this.cal_to.set_value(sToday.substr(0, 6));
        }

        /* cal_from_onkeydown 실행 */
        this.cal_from_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_to.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        /* cal_to_onkeydown 실행 */
        this.cal_to_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_from.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        /* cal_from_canchange 실행 */
        this.cal_from_canchange = function(obj,e)
        {
        	var calFormValue = ("" + e.postvalue).substr(0, 6);
        	var calToValue = ("" + this.cal_to.value).substr(0, 6);
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* cal_to_canchange 실행 */
        this.cal_to_canchange = function(obj,e)
        {
        	var calFormValue = ("" + this.cal_from.value).substr(0, 6);
        	var calToValue = ("" + e.postvalue).substr(0, 6);
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* label_onclick 실행 */
        this.label_onclick = function(obj,e)
        {
        	if(this.chk_use.readonly) return;
        	
        	if(this.chk_use.value) this.chk_use.set_value(false);
        	else this.chk_use.set_value(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.label.addEventHandler("onclick", this.label_onclick, this);
            this.cal_to.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_from.addEventHandler("onkeydown", this.cal_from_onkeydown, this);

        };

        this.loadIncludeScript("Betweenmonthfield.xfdl", true);

       
    };
}
)();
