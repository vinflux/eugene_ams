﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Betweeneditbox");
                this.set_classname("style01");
                this.set_titletext("입력 구간");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,377,28);
            }
            this.style.set_background("transparent");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label", "absolute", "18", "0", "98", "28", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("sta_WF_label");
            obj.set_wordwrap("char");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_use", "absolute", "0", "3", "15", "21", null, null, this);
            obj.set_taborder("4");
            obj.set_value("true");
            obj.set_cssclass("chk_WF_Srh");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "243", "5", "10", "18", null, null, this);
            obj.set_taborder("6");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_from", "absolute", "116", "3", "123", "22", null, null, this);
            obj.set_taborder("8");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_to", "absolute", "254", "3", "123", "22", null, null, this);
            obj.set_taborder("9");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 377, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("입력 구간");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Betweeneditbox.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Betweeneditbox.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        }

        /* edt_from_onkeydown 실행 */
        this.edt_from_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.parent.parent.btn_search.click();
        }

        /* edt_to_onkeydown 실행 */
        this.edt_to_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.parent.parent.btn_search.click();
        }

        /* label_onclick 실행 */
        this.label_onclick = function(obj,e)
        {
        	if(this.chk_use.readonly) return;
        	
        	if(this.chk_use.value) this.chk_use.set_value(false);
        	else this.chk_use.set_value(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.label.addEventHandler("onclick", this.label_onclick, this);
            this.edt_from.addEventHandler("onkeydown", this.edt_from_onkeydown, this);
            this.edt_to.addEventHandler("onkeydown", this.edt_to_onkeydown, this);

        };

        this.loadIncludeScript("Betweeneditbox.xfdl", true);

       
    };
}
)();
