﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Betweendatetimefield");
                this.set_classname("style01");
                this.set_titletext("기간 달력 시간");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,377,28);
            }
            this.style.set_background("transparent");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label", "absolute", "18", "0", "98", "28", null, null, this);
            obj.set_taborder("2");
            obj.set_cssclass("sta_WF_label");
            obj.set_wordwrap("char");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_use", "absolute", "0", "3", "15", "21", null, null, this);
            obj.set_taborder("3");
            obj.set_value("true");
            obj.set_cssclass("chk_WF_Srh");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_to", "absolute", "254", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_dateformat("yyyy-MM-dd HH:mm:ss");
            obj.set_value("null");
            obj.set_editformat("yyyy-MM-dd HH:mm:ss");
            obj.style.set_cursor("hand");

            obj = new Static("Static14", "absolute", "243", "5", "10", "18", null, null, this);
            obj.set_taborder("5");
            obj.set_text("-");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_from", "absolute", "116", "3", "123", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("6");
            obj.set_dateformat("yyyy-MM-dd HH:mm:ss");
            obj.set_value("null");
            obj.set_editformat("yyyy-MM-dd HH:mm:ss");
            obj.style.set_cursor("hand");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 377, 28, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("기간 달력 시간");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.registerScript("Betweendatetimefield.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Betweendatetimefield.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_searchId;
        this.calToValue;
        this.calToHH = "235959";
        this.gv_fr_date_value = 0;
        this.gv_to_date_value = 0;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	var sToday = this.parent.parent.gfn_today();
        	var fromdate = "";
        	var todate = "";
        	
        	if(this.parent.parent.gv_menuId == "ME00105400") {
        		//this.gv_to_date_value = 3;
        	}
        	
        	if(this.gv_fr_date_value != 0) fromdate = this.parent.parent.gfn_addDate(sToday, this.gv_fr_date_value);
        	else fromdate = sToday;
        	if(this.gv_to_date_value != 0) todate = this.parent.parent.gfn_addDate(sToday, this.gv_to_date_value);
        	else todate = sToday;
        	
        	this.cal_from.set_value(fromdate+"000000");
        	this.cal_to.set_value(todate+"235959");
        }

        /* cal_from_onkeydown 실행 */
        this.cal_from_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_to.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        /* cal_to_onkeydown 실행 */
        this.cal_to_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13"){
        		this.cal_from.setFocus();
        		this.parent.parent.btn_search.click();
        	}
        }

        this.cal_from_onchanged = function(obj,e)
        {
        	if(this.parent.parent.gv_menuId == "ME00105400") {
        		var calPreValue = "" + e.prevalue;
        		
        		if(calPreValue.length == 4) calPreValue = "000000";
        		else calPreValue = ("" + e.prevalue).substr(8, 6);
        		
        		var today = this.parent.parent.gfn_today();
        		var postday = ("" + e.postvalue).substr(0, 8);
        		
        		if(today > postday){
        			this.parent.parent.gfn_alert("MSG_NOT_DATE");
        			this.cal_from.set_value("" + e.prevalue);
        		}
        	}
        }

        this.cal_to_onchanged = function(obj,e)
        {
        	var calPreValue = "" + e.prevalue;
        	
        	if(calPreValue.length == 4) calPreValue = "000000";
        	else calPreValue = ("" + e.prevalue).substr(8, 6);
        	
        	if(e.postvalue != "" && (("" + e.postvalue).substr(0, 8) + this.calToHH) != this.calToValue){
        		this.fn_calToSet(("" + e.postvalue).substr(0, 8), calPreValue);
        	}
        }

        this.fn_calToSet = function(value,sHhmmss)
        {
        	if(sHhmmss == "000000") this.calToHH = "235959";
        	else this.calToHH = sHhmmss;
        	
        	this.calToValue = value + this.calToHH;
        	this.cal_to.set_value(this.calToValue);
        }

        /* cal_from_canchange 실행 */
        this.cal_from_canchange = function(obj,e)
        {
        	var calFormValue = ("" + e.postvalue).substr(0, 14);
        	var calToValue = ("" + this.cal_to.value).substr(0, 14);
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* cal_to_canchange 실행 */
        this.cal_to_canchange = function(obj,e)
        {
        	var calFormValue = ("" + this.cal_from.value).substr(0, 14);
        	var calToValue = ("" + e.postvalue).substr(0, 14);
        	
        	if(this.parent.parent.gfn_isNotNull(calFormValue) && this.parent.parent.gfn_isNotNull(calToValue)){
        		if(calFormValue > calToValue){
        			this.parent.parent.gfn_alert("MSG_CHK_DATE_VALIDATION");
        			return false;
        		}
        	}
        }

        /* label_onclick 실행 */
        this.label_onclick = function(obj,e)
        {
        	if(this.chk_use.readonly) return;
        	
        	if(this.chk_use.value) this.chk_use.set_value(false);
        	else this.chk_use.set_value(true);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.label.addEventHandler("onclick", this.label_onclick, this);
            this.cal_to.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_to.addEventHandler("onchanged", this.cal_to_onchanged, this);
            this.cal_from.addEventHandler("onkeydown", this.cal_from_onkeydown, this);
            this.cal_from.addEventHandler("onchanged", this.cal_from_onchanged, this);

        };

        this.loadIncludeScript("Betweendatetimefield.xfdl", true);

       
    };
}
)();
