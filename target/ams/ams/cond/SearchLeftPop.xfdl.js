﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SearchLeftPop");
                this.set_classname("style01");
                this.set_titletext("left 검색");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,395,614);
            }
            this.style.set_background("#f3f3f3ff");

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_srchCondition", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_srchCondition2", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("sta_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("3");
            obj.set_cssclass("sta_search_l");
            this.addChild(obj.name, obj);

            obj = new Static("sta_guide", "absolute", "13", null, null, "1", "13", "54", this);
            obj.set_taborder("191");
            obj.style.set_background("#dcdcdcff");
            this.addChild(obj.name, obj);

            obj = new Div("div_seachCond", "absolute", "0", "0", null, null, "0", "68", this);
            obj.set_taborder("192");
            obj.style.set_background("transparent");
            obj.set_scrollbars("autovert");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", null, null, "60", "28", "13", "13", this);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_search");
            obj.set_text("조회");
            this.addChild(obj.name, obj);

            obj = new Div("div_seachCond2", "absolute", "0", "0", null, null, "0", "68", this);
            obj.set_taborder("193");
            obj.style.set_background("transparent");
            obj.set_scrollbars("autovert");
            obj.set_visible("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_seachCond,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("192");
            		p.style.set_background("transparent");
            		p.set_scrollbars("autovert");

            	}
            );
            this.div_seachCond.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 395, 614, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("left 검색");
            		p.set_scrollbars("none");
            		p.style.set_background("#f3f3f3ff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","btn_search","tooltiptext","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("SearchLeftPop.xfdl", "lib::Comm.xjs");
        this.registerScript("SearchLeftPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SearchLeft.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 메뉴관리 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.div_height = 28;
        this.div_space = 6;
        this.div_space2 = 10;
        this.div_left = 12;
        this.div_left2 = 423;
        this.div_firstLeft = 389;
        this.div_top = 10;
        this.div_top2 = 10;
        this.div_bottom = 68;

        this.gv_condId = "SEARCHID";
        this.gv_nullCnt = 0;
        this.gv_search = application.gds_admSystemConfig.getColumn(application.gds_admSystemConfig.findRow("key","SEARCHCONDITION_POSITION"),"defaultvalue");
        this.gv_dayCnt = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key","ENABLEMAXBETWEENDAYS"),"value1");
        if(this.gfn_isNull(this.gv_dayCnt)) this.gv_dayCnt = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key","ENABLEMAXBETWEENDAYS"),"defaultvalue");

        this.gv_workTab = 0;

        this.gv_divId;
        this.owObjId;
        this.acObjId;
        this.ctObjId;
        this.lcObjId;
        this.owObjId2;
        this.acObjId2;
        this.ctObjId2;
        this.lcObjId2;

        /*******************************************************************************
            3. FORM FUNCTION / EVENT 영역
        ********************************************************************************/
        this.form_onload = function(obj,e)
        {
        	var searchid = "";
        	var ussc_label = "";
        	var dbfieldname = "";
        	var defaultoperator = "";
        	var singleoperatoryn = "";
        	var defaultvalue = "";
        	var ussc_dttype = "";
        	var ismandatory = "";
        	var combo_cd = "";
        	var gmtapply_chk = "";
        	var date_format = "";
        	var fr_date_value = 0;
        	var to_date_value = 0;
        //	var url_set = "svc::ams/ams/cond/";
        	var url_set = "cond::";
        	
            var sSearchId = "";
            var oSearch;
            
            var uskey = this.parent.gv_uskey;
            application.gds_srchCondition.filter("uskey == '" + uskey + "'");
            application.gds_srchCondition2.filter("uskey == '" + uskey + "'");
            
            //application.gds_srchCondition.filter("uskey == '" + application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.parent.gv_menuId), "uskey") + "'");
        	//application.gds_srchCondition2.filter("uskey == '" + application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.parent.gv_menuId), "uskey") + "'");
        // 	if(this.parent.apkey == "AMS"){
        //       application.gds_srchCondition.filter("ussc_hdkey== 'SC00001040' && uskey=='US00001850'");	
        //       application.gds_srchCondition2.filter("ussc_hdkey == 'SC00001040' && uskey=='US00001850'");
        //     }else if(this.parent.apkey == "ICOM"){
        //       application.gds_srchCondition.filter("ussc_hdkey== 'SC00001040' && uskey=='US00009421'");	
        //       application.gds_srchCondition2.filter("ussc_hdkey == 'SC00001040' && uskey=='US00009421'");			
        //     }else if(this.parent.apkey == "WM"){
        //       application.gds_srchCondition.filter("ussc_hdkey== 'SC00001040' && uskey=='US00009691'");	
        //       application.gds_srchCondition2.filter("ussc_hdkey == 'SC00001040' && uskey=='US00009691'");
        //     }else if(this.parent.apkey == "WMS"){
        //       if(this.parent.gv_menuId == "ME00002330"){  // 로케이션메뉴 화주조건 없음 
        //         application.gds_srchCondition.filter("ussc_hdkey == 'SC00021550' && uskey =='US00009721'");	
        //         application.gds_srchCondition2.filter("ussc_hdkey == 'SC00021550' && uskey =='US00009721'");
        //       }else{
        //         application.gds_srchCondition.filter("ussc_hdkey== 'SC00001040' && uskey=='US00009691'");	
        //         application.gds_srchCondition2.filter("ussc_hdkey == 'SC00001040' && uskey=='US00009691'");
        //       }
        //   	}else if(this.parent.apkey == "TMS"){
        //       if(this.parent.gv_menuId == "ME00100310"){  // 운송경로  화주조건 없음 
        //         application.gds_srchCondition.filter("ussc_hdkey== 'SC00021550' && uskey=='US00009731'");	
        //         application.gds_srchCondition2.filter("ussc_hdkey == 'SC00021550' && uskey=='US00009731'");
        // 
        //       }else{
        //         application.gds_srchCondition.filter("ussc_hdkey== 'SC00001040' && uskey=='US00009711'");	
        //         application.gds_srchCondition2.filter("ussc_hdkey == 'SC00001040' && uskey=='US00009711'");
        //       }
        //     }

            if(this.ds_srchCondition.rowcount == 0){
        		this.ds_srchCondition.copyData(application.gds_srchCondition, true);
        		
        		for(var i = 0 ; i < this.ds_srchCondition.rowcount ; i++){
        			ussc_label       = this.ds_srchCondition.getColumn(i, "ussc_label");
        			dbfieldname      = this.ds_srchCondition.getColumn(i, "dbfieldname");
        			defaultoperator  = this.ds_srchCondition.getColumn(i, "defaultoperator");
        			singleoperatoryn = this.ds_srchCondition.getColumn(i, "singleoperatoryn");
        			defaultvalue     = this.ds_srchCondition.getColumn(i, "defaultvalue");
        			ussc_dttype      = this.ds_srchCondition.getColumn(i, "ussc_dttype");
        			ismandatory      = this.ds_srchCondition.getColumn(i, "ismandatory");
        			searchid         = this.ds_srchCondition.getColumn(i, "searchid");
        			gmtapply_chk     = this.ds_srchCondition.getColumn(i, "gmtapply_chk");
        			date_format      = this.ds_srchCondition.getColumn(i, "date_format");
        			if(!this.gfn_isNull(this.ds_srchCondition.getColumn(i, "fr_date_value"))) fr_date_value = this.ds_srchCondition.getColumn(i, "fr_date_value");
        			if(!this.gfn_isNull(this.ds_srchCondition.getColumn(i, "to_date_value"))) to_date_value = this.ds_srchCondition.getColumn(i, "to_date_value");
        //			trace("ussc_dttype : " + ussc_dttype);
        //			trace("defaultvalue : " + defaultvalue);
        			
        			sSearchId = this.gv_condId + i;
        			
        			oSearch = new Div(sSearchId, "absolute", this.div_left, this.div_top + i*this.div_space + i*this.div_height, 377, this.div_height, null, null);
        			this.div_seachCond.addChild(sSearchId, oSearch);
        			
        			oSearch.set_async(false);
        			oSearch.set_scrollbars("none");
        			oSearch.show();
        			
        			if(ussc_label == "OWKEY") this.owObjId = oSearch;
        			else if(ussc_label == "APKEY") this.acObjId = oSearch;
        			else if(ussc_label == "CTKEY") this.ctObjId = oSearch;
        			else if(ussc_label == "LOGGRPCD") this.lcObjId = oSearch;
        			
        			if(this.gfn_isNotNull(sSearchId)){
        				if(ussc_dttype == "EDITBOXSEARCHPOPUP"){
        					oSearch.set_url(url_set + "Editboxsearchpopup.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					oSearch.gv_searchValue = searchid;
        					oSearch.gv_divId = sSearchId;
        					oSearch.gv_label = ussc_label;
        					
        					if(this.gfn_isNotNull(defaultvalue)){
        						if(defaultvalue == "#{OWKEY}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("owkeym").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("owkeym"));
        								if(this.gfn_getUserInfo("owkeym").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CTKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("ctKey"));
        						}else if(defaultvalue == "#{URKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        						}else if(defaultvalue == "#{LOGGRPCD}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("loggrpcd").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("loggrpcd"));
        								if(this.gfn_getUserInfo("loggrpcd").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CONTACTXAC}"){
        							if(application.gds_acxur.rowcount > 0){
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        							}
        						}else{
        							oSearch.edt_value.set_value(defaultvalue);
        						}
        					}
        				}else if(ussc_dttype == "EDITBOX"){
        					oSearch.set_url(url_set + "Editbox.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					if(this.gfn_isNotNull(defaultvalue)){
        						if(defaultvalue == "#{OWKEY}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("owkeym").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("owkeym"));
        								if(this.gfn_getUserInfo("owkeym").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CTKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("ctKey"));
        						}else if(defaultvalue == "#{URKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        						}else if(defaultvalue == "#{LOGGRPCD}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("loggrpcd").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("loggrpcd"));
        								if(this.gfn_getUserInfo("loggrpcd").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CONTACTXAC}"){
        							if(application.gds_acxur.rowcount > 0){
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        							}
        						}else{
        							oSearch.edt_value.set_value(defaultvalue);
        						}
        					}
        				}else if(ussc_dttype == "COMBOBOX"){
        					oSearch.set_url(url_set + "Combobox.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					combo_cd = searchid.split(";");
        					this.gfn_getCode(combo_cd[1], oSearch.ds_combo, "");
        					
        					if(this.gfn_isNotNull(defaultvalue)) oSearch.cbo_value.set_value(defaultvalue);
        					else oSearch.cbo_value.set_index(0);
        				}else if(ussc_dttype == "BETWEENDATEFIELD"){
        					oSearch.gv_fr_date_value = fr_date_value;
        					oSearch.gv_to_date_value = to_date_value;
        					
        					this.p_fr_date_value = fr_date_value;
        					this.p_to_date_value = to_date_value;
        					
        					oSearch.set_url(url_set + "Betweendatefield.xfdl");
        				}else if(ussc_dttype == "DATEFIELD"){
        					oSearch.gv_to_date_value = to_date_value;
        					oSearch.set_url(url_set + "Datefield.xfdl");
        				}else if(ussc_dttype == "MONTHFIELD"){
        					oSearch.set_url(url_set + "Monthfield.xfdl");
        				}else if(ussc_dttype == "BETWEENDATETIMEFIELD"){
        					oSearch.gv_fr_date_value = fr_date_value;
        					oSearch.gv_to_date_value = to_date_value;
        					oSearch.set_url(url_set + "Betweendatetimefield.xfdl");
        				}else if(ussc_dttype == "BETWEENMONTHFIELD"){
        					oSearch.set_url(url_set + "Betweenmonthfield.xfdl");
        				}else if(ussc_dttype == "BETWEENEDITBOX"){
        					oSearch.set_url(url_set + "Betweeneditbox.xfdl");
        				}
        				
        				oSearch.gv_searchId = sSearchId;
        				
        				if(this.gfn_isNotNull(application.gds_lang.getColumn(0, ussc_label)) && this.gfn_isNotNull(oSearch.label)){
        					oSearch.label.set_text(application.gds_lang.getColumn(0, ussc_label));
        				}
        				
        				if(ismandatory == "Y" && this.gfn_isNotNull(oSearch.label)){
        					oSearch.label.set_cssclass("sta_WF_label_duty");
        					oSearch.chk_use.set_readonly(true);
        				}
        			}
        		}
            }
        	
            if(this.ds_srchCondition2.rowcount == 0){
        		this.ds_srchCondition2.copyData(application.gds_srchCondition2, true);
        		
        		for(var i = 0 ; i < this.ds_srchCondition2.rowcount ; i++){
        			ussc_label       = this.ds_srchCondition2.getColumn(i, "ussc_label");
        			dbfieldname      = this.ds_srchCondition2.getColumn(i, "dbfieldname");
        			defaultoperator  = this.ds_srchCondition2.getColumn(i, "defaultoperator");
        			singleoperatoryn = this.ds_srchCondition2.getColumn(i, "singleoperatoryn");
        			defaultvalue     = this.ds_srchCondition2.getColumn(i, "defaultvalue");
        			ussc_dttype      = this.ds_srchCondition2.getColumn(i, "ussc_dttype");
        			ismandatory      = this.ds_srchCondition2.getColumn(i, "ismandatory");
        			searchid         = this.ds_srchCondition2.getColumn(i, "searchid");
        			gmtapply_chk     = this.ds_srchCondition2.getColumn(i, "gmtapply_chk");
        			date_format      = this.ds_srchCondition.getColumn(i, "date_format");
        			fr_date_value    = this.ds_srchCondition.getColumn(i, "fr_date_value");
        			to_date_value    = this.ds_srchCondition.getColumn(i, "to_date_value");
        //			trace("ussc_dttype : " + ussc_dttype);
        //			trace("defaultvalue : " + defaultvalue);
        			
        			sSearchId = this.gv_condId + i;
        			
        			oSearch = new Div(sSearchId, "absolute", this.div_left, this.div_top + i*this.div_space + i*this.div_height, 377, this.div_height, null, null);
        			this.div_seachCond2.addChild(sSearchId, oSearch);
        			
        			oSearch.set_async(false);
        			oSearch.set_scrollbars("none");
        			oSearch.show();
        			
        			if(ussc_label == "OWKEY") this.owObjId2 = oSearch;
        			else if(ussc_label == "APKEY") this.acObjId2 = oSearch;
        			else if(ussc_label == "CTKEY") this.ctObjId2 = oSearch;
        			else if(ussc_label == "LOGGRPCD") this.lcObjId2 = oSearch;
        			
        			if(this.gfn_isNotNull(sSearchId)){
        				if(ussc_dttype == "EDITBOXSEARCHPOPUP"){
        					oSearch.set_url(url_set + "Editboxsearchpopup.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					oSearch.gv_searchValue = searchid;
        					oSearch.gv_divId = sSearchId;
        					oSearch.gv_label = ussc_label;
        					
        					if(this.gfn_isNotNull(defaultvalue)){
        						if(defaultvalue == "#{OWKEY}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("owkeym").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("owkeym"));
        								if(this.gfn_getUserInfo("owkeym").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CTKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("ctKey"));
        						}else if(defaultvalue == "#{URKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        						}else if(defaultvalue == "#{LOGGRPCD}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("loggrpcd").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("loggrpcd"));
        								if(this.gfn_getUserInfo("loggrpcd").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CONTACTXAC}"){
        							if(application.gds_acxur.rowcount > 0){
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        							}
        						}else{
        							oSearch.edt_value.set_value(defaultvalue);
        						}
        					}
        				}else if(ussc_dttype == "EDITBOX"){
        					oSearch.set_url(url_set + "Editbox.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					if(this.gfn_isNotNull(defaultvalue)){
        						if(defaultvalue == "#{OWKEY}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("owkeym").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("owkeym"));
        								if(this.gfn_getUserInfo("owkeym").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CTKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("ctKey"));
        						}else if(defaultvalue == "#{URKEY}"){
        							oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        						}else if(defaultvalue == "#{LOGGRPCD}"){
        							if(singleoperatoryn == "Y" && defaultoperator != "IN"){
        								var defaultval = this.gfn_getUserInfo("loggrpcd").split(",");
        								oSearch.edt_value.set_value(defaultval[0]);
        								oSearch.cbo_combo.set_value("=");
        							}else{
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("loggrpcd"));
        								if(this.gfn_getUserInfo("loggrpcd").indexOf(",") > 0) oSearch.cbo_combo.set_value("IN");
        							}
        						}else if(defaultvalue == "#{CONTACTXAC}"){
        							if(application.gds_acxur.rowcount > 0){
        								oSearch.edt_value.set_value(this.gfn_getUserInfo("urKey"));
        							}
        						}else{
        							oSearch.edt_value.set_value(defaultvalue);
        						}
        					}
        				}else if(ussc_dttype == "COMBOBOX"){
        					oSearch.set_url(url_set + "Combobox.xfdl");
        					oSearch.cbo_combo.set_value(defaultoperator);
        					
        					combo_cd = searchid.split(";");
        					this.gfn_getCode(combo_cd[1], oSearch.ds_combo, "");
        					
        					if(this.gfn_isNotNull(defaultvalue)) oSearch.cbo_value.set_value(defaultvalue);
        					else oSearch.cbo_value.set_index(0);
        				}else if(ussc_dttype == "BETWEENDATEFIELD"){
        					oSearch.gv_fr_date_value = fr_date_value;
        					oSearch.gv_to_date_value = to_date_value;
        					oSearch.set_url(url_set + "Betweendatefield.xfdl");
        				}else if(ussc_dttype == "DATEFIELD"){
        					oSearch.gv_to_date_value = to_date_value;
        					oSearch.set_url(url_set + "Datefield.xfdl");
        				}else if(ussc_dttype == "MONTHFIELD"){
        					oSearch.set_url(url_set + "Monthfield.xfdl");
        				}else if(ussc_dttype == "BETWEENDATETIMEFIELD"){
        					oSearch.gv_fr_date_value = fr_date_value;
        					oSearch.gv_to_date_value = to_date_value;
        					oSearch.set_url(url_set + "Betweendatetimefield.xfdl");
        				}else if(ussc_dttype == "BETWEENMONTHFIELD"){
        					oSearch.set_url(url_set + "Betweenmonthfield.xfdl");
        				}else if(ussc_dttype == "BETWEENEDITBOX"){
        					oSearch.set_url(url_set + "Betweeneditbox.xfdl");
        				}
        				
        				oSearch.gv_searchId = sSearchId;
        				
        				if(this.gfn_isNotNull(application.gds_lang.getColumn(0, ussc_label)) && this.gfn_isNotNull(oSearch.label)){
        					oSearch.label.set_text(application.gds_lang.getColumn(0, ussc_label));
        				}
        				
        				if(ismandatory == "Y" && this.gfn_isNotNull(oSearch.label)){
        					oSearch.label.set_cssclass("sta_WF_label_duty");
        					oSearch.chk_use.set_readonly(true);
        				}
        			}
        		}
            }
        	
        	var sChange = "";
        	
        	if(system.navigatorname == "nexacro"){
        		sChange = application.getPrivateProfile("searchSet");
        		
        		if(this.gfn_isNull(sChange)){
        			if(this.gfn_isNull(this.gv_search) || this.gv_search == "LEFT") this.parent.gv_searchValue = "T";
        			else this.parent.gv_searchValue = "L";
        		}else{
        			if(sChange == "L") this.parent.gv_searchValue = "T";
        			else this.parent.gv_searchValue = "L";
        		}
        	}else{
        		sChange = window.localStorage.getItem("searchSet");
        		
        		if(this.gfn_isNull(sChange)){
        			if(this.gfn_isNull(this.gv_search) || this.gv_search == "LEFT") this.parent.gv_searchValue = "T";
        			else this.parent.gv_searchValue = "L";
        		}else{
        			if(sChange == "L") this.parent.gv_searchValue = "T";
        			else this.parent.gv_searchValue = "L";
        		}
        	}
        	
        	this.parent.fn_searchChange();
        	this.parent.div_cond.set_visible(true);
        	this.parent.div_work.set_visible(true);
        }

        /* btn_search_onclick 실행 */
        this.btn_search_onclick = function(obj,e)
        {
        	this.parent.sFlag = "1";
        	var nRow = application.gds_menu.findRow("mekey", this.parent.gv_menuId);
        	var uskey = application.gds_menu.getColumn(nRow, "uskey");
        	
        	//var pageLimit = this.parent.div_work.ds_param.getColumn(0, "pagingLimit");
        	var pageLimit = this.parent.ds_param.getColumn(0, "pagingLimit");
        	if(this.gfn_isNull(pageLimit)) pageLimit = "100";
        	
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", pageLimit);
        	this.gfn_setCommon("uskey", uskey);
        	this.gfn_setUserInfo("usKey", uskey);
        	
        	this.fn_dataSet();
        }

        /* pageSearch 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */
        this.fn_pageSearch = function(currentPage,pagingLimit,count)
        {
        	var nRow = application.gds_menu.findRow("mekey", this.parent.gv_menuId);
        	var uskey = application.gds_menu.getColumn(nRow, "uskey");
        	
        	this.gfn_setParam("currentPage", currentPage);
        	this.gfn_setParam("pagingLimit", pagingLimit);
        	this.gfn_setParam("COUNT", count);
        	this.gfn_setCommon("uskey", uskey);
        	this.gfn_setUserInfo("usKey", uskey);
        	
        	this.fn_dataSet();
        }

        /* fn_excelSearch
        * @return
        * @param sFunc   : 호출함수
                 objGrid : 그리드 오브젝트
                 sForm   : this
                 oValue  : 추가 인자
        */

        var gv_sFunc;
        var gv_objGrid;
        var gv_sForm;
        var gv_oValue;

        this.fn_confirm3BtnSetting = function(sFunc,objGrid,sForm,oValue,bType,paramObj)
        {
        	gv_sFunc = sFunc;
        	gv_objGrid = objGrid;
        	gv_sForm = sForm;
        	gv_oValue = oValue;
        	
        	var nRow = application.gds_menu.findRow("mekey", this.parent.gv_menuId);
        	var uskey = application.gds_menu.getColumn(nRow, "uskey");
        	var mename = application.gds_menu.getColumn(nRow, "mename");
        	var nCount = this.parent.ds_param.getColumn(0, "COUNT");
        	if(this.gfn_isNull(nCount)) nCount = "";
        	
        	this.gfn_setCommon("uskey", uskey);
        	this.gfn_setUserInfo("usKey", uskey);
        	
        	var arrBtnText = new Array("EXCELDOWN", "ALLEXCELDOWN", "CANCEL");
        	if(bType){
        		arrBtnText = new Array("REPORT", "ALLREPORT", "CANCEL");
        	}
        	
        	var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        	if(objGrid.name == "grd_header") var sMsgText = new Array("TOTAL_COUNT," + nCount);
        	else var sMsgText = new Array("");
        	
        	this.gfn_confirm3Btn (
        		arrBtnText,
        		sMsgCode,
        		sMsgText,
        		function(msg, flag) {
        			if (flag != "0") {
        				if (flag == "1") {
        					if(bType){
        						this.gfn_callReport(
        											 paramObj.jrfName
        											,paramObj.printType
        											,paramObj.udsName
        											,this.makeDatasetToJSON(paramObj.oDatasetList)		
        											,paramObj.prType
        											,paramObj.eqtype
        											,paramObj.target
        											,paramObj.printLogYN
        											,paramObj.sourcekey
        											,paramObj.wkprhstype
        											,paramObj.width
        											,paramObj.height
        											,paramObj.isMultiReport
        											,paramObj.multiCount
        						);
        					}else{
        						this.gfn_exportExcel(this, objGrid, mename + "_" + objGrid.name.substr(4));
        					}
        				}
        				if (flag == "2") {
        					if(bType){
        						if(paramObj.allSelectProcessType == "REMOVE_KEY"){
        							this.removeDatasetToJSON(paramObj.oDatasetList);
        						}else if(paramObj.allSelectProcessType == "SEARCH_LIST"){
        							if(this.fn_searchListSetting()){
        								this.ds_searchList.copyData(application.gds_searchList);
        							}
        						}
        					
        						if(this.gfn_isNotNull(oValue)){
        							this.gfn_callReportTransaction(oValue, paramObj);
        						}else{
        							this.gfn_callReport(
        												 paramObj.jrfName
        												,paramObj.printType
        												,paramObj.udsName
        												,this.makeDatasetToJSON(paramObj.oDatasetList)		
        												,paramObj.prType
        												,paramObj.eqtype
        												,paramObj.target
        												,paramObj.printLogYN
        												,paramObj.sourcekey
        												,paramObj.wkprhstype
        												,paramObj.width
        												,paramObj.height
        												,paramObj.isMultiReport
        												,paramObj.multiCount
        							);
        						}
        					}else{
        						this.fn_dataSet("Y");
        					}
        				}
        			}
        		}
        	);
        //	this.fn_dataSet("Y");
        }
         
        this.fn_confirm2BtnSetting = function(sFunc,objGrid,sForm,oValue,bType,paramObj)
        {
        	gv_sFunc = sFunc;
        	gv_objGrid = objGrid;
        	gv_sForm = sForm;
        	gv_oValue = oValue;
        	
        	var nRow = application.gds_menu.findRow("mekey", this.parent.gv_menuId);
        	var uskey = application.gds_menu.getColumn(nRow, "uskey");
        	var mename = application.gds_menu.getColumn(nRow, "mename");
        	var nCount = gv_sForm.ds_param.getColumn(0, "COUNT");
        	var nDtCount = 0;
        	var nStopCount = 0;
        	if(this.gfn_isNotNull(gv_sForm.ds_paramDetail)){
        		nDtCount = gv_sForm.ds_paramDetail.getColumn(0, "COUNT");
        		nStopCount =  gv_sForm.ds_paramDetail.getColumn(0, "STOPCOUNT");
        	}

        	if(this.gfn_isNull(nCount)) nCount = "";
        	
        	this.gfn_setCommon("uskey", uskey);
        	this.gfn_setUserInfo("usKey", uskey);
        	
        	var arrBtnText = new Array("ALLEXCELDOWN", "CANCEL");
        	if(bType){
        		arrBtnText = new Array("ALLREPORT", "CANCEL");
        	}
        	
        	//2018.09.17 - ksh : 엑셀다운로드 & 리포트 출력에 따른 다국어 분기처리 추가
        	var text = arrBtnText[0];
        	var sMsgCode = null;
        	
        	if(text == "ALLREPORT"){
        		sMsgCode = "MSG_ALERT_REPORT_PRINT";
        	}else if(text == "ALLEXCELDOWN"){
        		sMsgCode = "MSG_ALERT_EXCELLDOWN";
        	}
        	
        	//var sMsgCode = "MSG_ALERT_EXCELLDOWN";
        	//var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        	if(objGrid.name == "grd_header"){
        		var sMsgText = new Array("TOTAL_COUNT," + nCount);
        	 }else if(objGrid.name == "grd_detail"){
        		var sMsgText = new Array("TOTAL_COUNT," + nDtCount);
        	 }else if(objGrid.name == "grd_detail_stop"){
        		var sMsgText = new Array("TOTAL_COUNT," + nStopCount);
        	 }else var sMsgText = new Array("");
        	
        	this.gfn_confirm2Btn (
        		arrBtnText,
        		sMsgCode,
        		sMsgText,
        		function(msg, flag) {
        			if (flag != "0") {
        				if (flag == "2") {
        					if(bType){
        						if(paramObj.allSelectProcessType == "REMOVE_KEY"){
        							this.removeDatasetToJSON(paramObj.oDatasetList);
        						}else if(paramObj.allSelectProcessType == "SEARCH_LIST"){
        							if(this.fn_searchListSetting()){
        								this.ds_searchList.copyData(application.gds_searchList);
        							}
        						}
        						
        						if(this.gfn_isNotNull(oValue)){
        							this.gfn_callReportTransaction(oValue, paramObj);
        						}else{
        							this.gfn_callReport(
        												 paramObj.jrfName
        												,paramObj.printType
        												,paramObj.udsName
        												,this.makeDatasetToJSON(paramObj.oDatasetList)		
        												,paramObj.prType
        												,paramObj.eqtype
        												,paramObj.target
        												,paramObj.printLogYN
        												,paramObj.sourcekey
        												,paramObj.wkprhstype
        												,paramObj.width
        												,paramObj.height
        												,paramObj.isMultiReport
        												,paramObj.multiCount
        							);
        						}
        					}else{
        						this.fn_dataSet("Y");
        					}
        				}
        			}
        		}
        	);
        //	this.fn_dataSet("Y");
        }

        this.fn_excelSearch = function(sFunc,objGrid,sForm,oValue)
        {

        	this.fn_confirm2BtnSetting(sFunc, objGrid, sForm, oValue, false);
        	//this.fn_confirm3BtnSetting(sFunc, objGrid, sForm, oValue, false);
        }

        this.fn_reportSearch = function(objGrid,sForm,tranObj,paramObj)
        {
        	this.fn_confirm3BtnSetting(null, objGrid, sForm, tranObj, true, paramObj);
        }

        /* dataSet
        * @return
        * @param
        */
        this.fn_searchListSetting = function(){
        	this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gv_nullCnt = 0;
        	application.gds_searchList.clearData();
        	
        	var srchCondDiv;
        	var srchCondDs;
        	var gmtapply_chk = "";
        	var ymd_format = "";
        	var defaultoperator = "";
        	var dataFrom = "";
        	var dataTo = "";
        	var owkey = "";
        	
        	if(this.gv_workTab == 0){
        		srchCondDs = this.ds_srchCondition;
        		srchCondDiv = this.div_seachCond;
        	}else if(this.gv_workTab == 1){
        		srchCondDs = this.ds_srchCondition2;
        		srchCondDiv = this.div_seachCond2;
        	}
        	
            for(var i = 0 ; i < srchCondDs.rowcount ; i++){
        		dbfieldname     = srchCondDs.getColumn(i, "dbfieldname");
        		ussc_dttype     = srchCondDs.getColumn(i, "ussc_dttype");
        		ismandatory     = srchCondDs.getColumn(i, "ismandatory");
        		gmtapply_chk    = srchCondDs.getColumn(i, "gmtapply_chk");
        		ymd_format      = srchCondDs.getColumn(i, "ymd_format");
        		defaultoperator = srchCondDs.getColumn(i, "defaultoperator");
        		obj_div = eval("srchCondDiv." + this.gv_condId + i);
        		
        		if(dbfieldname.indexOf("OWKEY") > 0 && application.gds_rtnOwnerRole.rowcount > 0){
        			application.gds_searchList.addRow();
        			application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        			
        			if(application.gds_rtnOwnerRole.rowcount > 1){
        				application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", "IN");
        			}else{
        				application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", "=");
        			}
        			
        			for(var o = 0 ; o < application.gds_rtnOwnerRole.rowcount ; o++){
        				if(o == 0) owkey = application.gds_rtnOwnerRole.getColumn(o, "ownerKey");
        				else owkey += "," + application.gds_rtnOwnerRole.getColumn(o, "ownerKey");
        			}
        			
        			application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value", owkey);
        		}else if(dbfieldname.indexOf("LOGGRPCD") > 0 && !this.gfn_isNull(this.gfn_getUserInfo("loggrpcd"))){
        			application.gds_searchList.addRow();
        			application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        			application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , this.gfn_getUserInfo("loggrpcd"));
        			
        			if(this.gfn_getUserInfo("loggrpcd").indexOf(",") > 0){
        				application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", "IN");
        			}else{
        				application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", "=");
        			}
        		}
        		
        		if(ussc_dttype == "EDITBOXSEARCHPOPUP"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.edt_value.value)){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						obj_div.edt_value.setFocus();
        					});
        				}else{
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", obj_div.cbo_combo.value);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , obj_div.edt_value.value);
        				}
        			}
        		}else if(ussc_dttype == "EDITBOX"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.edt_value.value)){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						obj_div.edt_value.setFocus();
        					});
        				}else{
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", obj_div.cbo_combo.value);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , obj_div.edt_value.value);
        				}
        			}
        		}else if(ussc_dttype == "COMBOBOX"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.cbo_value.value)){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						obj_div.cbo_value.setFocus();
        					});
        				}else{
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", obj_div.cbo_combo.value);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , obj_div.cbo_value.value);
        				}
        			}
        		}else if(ussc_dttype == "BETWEENDATEFIELD"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && (this.gfn_isNull(obj_div.cal_from.value) || this.gfn_isNull(obj_div.cal_to.value))){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        						else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.cal_from.value;
        					dataTo = obj_div.cal_to.value;
        					
        					if((this.gfn_isNotNull(dataFrom) && this.gfn_isNull(dataTo)) || (this.gfn_isNull(dataFrom) && this.gfn_isNotNull(dataTo))){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        							else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        						});
        						
        						return;
        					}else if(dataFrom > dataTo){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							obj_div.cal_from.setFocus();
        						});
        						
        						return;
        					}else{
        						if(!this.gfn_isNull(this.gv_dayCnt)){
        							if(this.gfn_getDiffDay(dataFrom, dataTo) > this.gv_dayCnt){
        								this.gfn_alert("MSG_CHK_DATE_PERIOD", "DATE,"+this.gv_dayCnt, function(msg, flag){
        									obj_div.cal_to.setFocus();
        								});
        								
        								return;
        							}
        						}
        					}
        					
        					if(gmtapply_chk != "N"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom + "000000";
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo + "235959";
        					}
        					
        					if(ymd_format == "10"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom + "000000";
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo + "235959";
        					}else if(ymd_format == "20"){
        					}else if(ymd_format == "30"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.substr(0, 6);
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.substr(0, 6);
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", defaultoperator);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "gmtapply_chk", gmtapply_chk);
        				}
        			}
        		}else if(ussc_dttype == "DATEFIELD"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.cal_from.value)){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						obj_div.cal_from.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.cal_from.value;
        					
        					if(gmtapply_chk != "N"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom + "000000";
        					}
        					
        					if(ymd_format == "10"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom + "000000";
        					}else if(ymd_format == "20"){
        					}else if(ymd_format == "30"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.substr(0, 6);
        					}
        					
        					if(defaultoperator == "BETWEEN"){
        						if(this.gfn_isNotNull(dataFrom)){
        							dataFrom = dataFrom + "000000";
        							dataTo = dataFrom.substr(0, 8) + "235959";
        						}
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", defaultoperator);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "gmtapply_chk", gmtapply_chk);
        				}
        			}
        		}else if(ussc_dttype == "MONTHFIELD"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && this.gfn_isNull(obj_div.cal_from.value)){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						obj_div.cal_from.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.cal_from.value;
        					
        					if(gmtapply_chk != "N"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 6) + "00000000";
        					}else{
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 6);
        					}
        					
        					if(defaultoperator == "BETWEEN"){
        						if(this.gfn_isNotNull(dataFrom)){
        							dataFrom = dataFrom.toString().substr(0, 6) + "01000000";
        							dataTo = this.gfn_lastDate(dataFrom.substr(0, 8)) + "235959";
        						}
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", defaultoperator);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "value"   , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "gmtapply_chk", gmtapply_chk);
        				}
        			}
        		}else if(ussc_dttype == "BETWEENDATETIMEFIELD"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && (this.gfn_isNull(obj_div.cal_from.value) || this.gfn_isNull(obj_div.cal_to.value))){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        						else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.cal_from.value;
        					dataTo = obj_div.cal_to.value;
        					
        					if((this.gfn_isNotNull(dataFrom) && this.gfn_isNull(dataTo)) || (this.gfn_isNull(dataFrom) && this.gfn_isNotNull(dataTo))){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        							else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        						});
        						
        						return;
        					}else if(dataFrom > dataTo){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							obj_div.cal_from.setFocus();
        						});
        						
        						return;
        					}else{
        						if(!this.gfn_isNull(this.gv_dayCnt)){
        							if(this.gfn_getDiffDay(dataFrom.toString().substr(0, 8), dataTo.toString().substr(0, 8)) > this.gv_dayCnt){
        								this.gfn_alert("MSG_CHK_DATE_PERIOD", "DATE,"+this.gv_dayCnt, function(msg, flag){
        									obj_div.cal_to.setFocus();
        								});
        								
        								return;
        							}
        						}
        					}
        					
        					if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 14);
        					if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 14);
        					
        					if(ymd_format == "10"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 14);
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 14);
        					}else if(ymd_format == "20"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 8);
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 8);
        					}else if(ymd_format == "30"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 6);
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 6);
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", defaultoperator);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "gmtapply_chk", gmtapply_chk);
        				}
        			}
        		}else if(ussc_dttype == "BETWEENMONTHFIELD"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && (this.gfn_isNull(obj_div.cal_from.value) || this.gfn_isNull(obj_div.cal_to.value))){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        						else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.cal_from.value;
        					dataTo = obj_div.cal_to.value;
        					
        					if((this.gfn_isNotNull(dataFrom) && this.gfn_isNull(dataTo)) || (this.gfn_isNull(dataFrom) && this.gfn_isNotNull(dataTo))){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							if(this.gfn_isNull(obj_div.cal_from.value)) obj_div.cal_from.setFocus();
        							else if(this.gfn_isNull(obj_div.cal_to.value)) obj_div.cal_to.setFocus();
        						});
        						
        						return;
        					}else if(dataFrom > dataTo){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							obj_div.cal_from.setFocus();
        						});
        						
        						return;
        					}
        					
        					if(gmtapply_chk != "N"){
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 6) + "00000000";
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 6) + "00000000";
        					}else{
        						if(this.gfn_isNotNull(dataFrom)) dataFrom = dataFrom.toString().substr(0, 6);
        						if(this.gfn_isNotNull(dataTo)) dataTo = dataTo.toString().substr(0, 6);
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", defaultoperator);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "gmtapply_chk", gmtapply_chk);
        				}
        			}
        		}else if(ussc_dttype == "BETWEENEDITBOX"){
        			if(obj_div.chk_use.value){
        				if(ismandatory == "Y" && (this.gfn_isNull(obj_div.edt_from.value) || this.gfn_isNull(obj_div.edt_to.value))){
        					this.gv_nullCnt++;
        					this.gfn_alert("MSG_10001", "", function(msg, flag){
        						if(this.gfn_isNull(obj_div.edt_from.value)) obj_div.edt_from.setFocus();
        						else if(this.gfn_isNull(obj_div.edt_to.value)) obj_div.edt_to.setFocus();
        					});
        				}else{
        					dataFrom = obj_div.edt_from.value;
        					dataTo = obj_div.edt_to.value;
        					
        					if((this.gfn_isNotNull(dataFrom) && this.gfn_isNull(dataTo)) || (this.gfn_isNull(dataFrom) && this.gfn_isNotNull(dataTo))){
        						this.gfn_alert("MSG_CHK_DATE_VALIDATION", "", function(msg, flag){
        							if(this.gfn_isNull(obj_div.edt_from.value)) obj_div.edt_from.setFocus();
        							else if(this.gfn_isNull(obj_div.edt_to.value)) obj_div.edt_to.setFocus();
        						});
        						
        						return;
        					}
        					
        					application.gds_searchList.addRow();
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "dbColoum", dbfieldname);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "operator", "STRINGBETWEEN");
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "fromVal" , dataFrom);
        					application.gds_searchList.setColumn(application.gds_searchList.rowposition, "toVal"   , dataTo);
        				}
        			}
        		}
        		
        		if(this.gv_nullCnt > 0) break;
            }
        	
        	//this.parent.div_work.ds_searchList.copyData(application.gds_searchList);
        	this.parent.ds_searchList.copyData(application.gds_searchList);
        	
        	if(this.gv_nullCnt == 0){
        		return true;
        	}else{
        		return false;
        	}
        }

        this.fn_dataSet = function(sEx)
        {
        	if(this.fn_searchListSetting()){
        		if(sEx == "Y"){
        			this.gfn_excelDownload(gv_sFunc, gv_objGrid, gv_sForm, gv_oValue);
        		}else{
        			//this.parent.div_work.fn_search();
        			this.parent.fn_search();
        		}
        	}
        }

        this.fn_callBackSearchPop = function()
        {
        	var obj_div;
        	
        	if(this.gv_workTab == 0) obj_div = eval("this.div_seachCond." + this.gv_divId);
        	else if(this.gv_workTab == 1) obj_div = eval("this.div_seachCond2." + this.gv_divId);
        	
        	if(this.gfn_isNotNull(obj_div.edt_value.value)){
        		if(obj_div.edt_value.value.indexOf(",") > 0) obj_div.cbo_combo.set_value("IN");
        	}
        }

        this.fn_tabChange = function(tabIncex)
        {
        	this.gv_workTab = tabIncex;
        	
        	if(tabIncex == 0){
        		this.div_seachCond.set_visible(true);
        		this.div_seachCond2.set_visible(false);
        	}else if(tabIncex == 1){
        		this.div_seachCond.set_visible(false);
        		this.div_seachCond2.set_visible(true);
        	}
        	
        	if(this.parent.gv_searchValue == "T") this.parent.gv_searchValue = "L"
        	else this.parent.gv_searchValue = "T"
        	
        	this.parent.fn_searchChange();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);

        };

        this.loadIncludeScript("SearchLeftPop.xfdl", true);

       
    };
}
)();
