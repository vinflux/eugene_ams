﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SetItemCodePopDtl");
                this.set_classname("style01");
                this.set_titletext("물류 유통가공 상품 상세 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,342);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"seiclinenum\" type=\"STRING\" size=\"32\"/><Column id=\"icutkey\" type=\"STRING\" size=\"32\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"seickey\" type=\"STRING\" size=\"32\"/><Column id=\"uom\" type=\"STRING\" size=\"32\"/><Column id=\"ickey\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"icname\" type=\"STRING\" size=\"32\"/><Column id=\"qty\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"owname\" type=\"STRING\" size=\"32\"/><Column id=\"uom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"seiclinenum\"/><Col id=\"icutkey\"/><Col id=\"owkey\"/><Col id=\"inserturkey\"/><Col id=\"seickey\"/><Col id=\"uom\"/><Col id=\"ickey\"/><Col id=\"updatedate\"/><Col id=\"icname\"/><Col id=\"qty\"/><Col id=\"owname\"/><Col id=\"uom_desc\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_uom", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field1_desc\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/><Column id=\"field3\" type=\"STRING\" size=\"256\"/><Column id=\"field4\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field1_desc\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/><Column id=\"field3\" type=\"STRING\" size=\"256\"/><Column id=\"field4\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static07", "absolute", "216", "213", "342", "31", null, null, this);
            obj.set_taborder("236");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("240");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("242");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "216", "244", "342", "31", null, null, this);
            obj.set_taborder("245");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("233");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("14");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("5");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_seickey", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("6");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static02", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("229");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "222", "62", "110", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "334", "62", "24", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "360", "62", "183", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "213", "194", "31", null, null, this);
            obj.set_taborder("234");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ickey", "absolute", "222", "155", "110", "22", null, null, this);
            obj.set_taborder("0");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIckey", "absolute", "334", "155", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icname", "absolute", "360", "155", "183", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icutkey", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static10", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_uom", "absolute", "222", "248", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("ds_uom");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field1_desc");

            obj = new Static("static12", "absolute", "22", "244", "194", "31", null, null, this);
            obj.set_taborder("246");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_seiclinenum", "absolute", "222", "124", "321", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_qty", "absolute", "222", "217", "321", "22", null, null, this);
            obj.set_taborder("2");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fn_get_master_impos", "absolute", "616", "54", "183", "22", null, null, this);
            obj.set_taborder("250");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 342, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("물류 유통가공 상품 상세 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_seickey","value","ds_header","seickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","SET ITEMCODE DETAIL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","static","text","gds_lang","SEICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static1","text","gds_lang","SEICLINENUM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static02","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","QTY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_ickey","value","ds_header","ickey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","static06","text","gds_lang","ICKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static10","text","gds_lang","ICUTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","cbo_uom","value","ds_header","uom");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_icutkey","value","ds_header","icutkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static12","text","gds_lang","UOM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","edt_seiclinenum","value","ds_header","seiclinenum");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","mdt_qty","value","ds_header","qty");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("SetItemCodePopDtl.xfdl", "lib::Comm.xjs");
        this.registerScript("SetItemCodePopDtl.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SetItemCodePopDtl.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 임가공 상품 상세 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_seickey = "";
        this.gv_seiclinenum = "";
        this.gv_ickey = "";
        this.gv_icutkey = "";

        this.gv_Pvalue = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_seickey = this.gfn_isNullEmpty(this.parent.seickey);
            this.gv_seiclinenum = this.gfn_isNullEmpty(this.parent.seiclinenum);
            this.gv_ickey = this.gfn_isNullEmpty(this.parent.ickey);
            this.gv_icutkey = this.gfn_isNullEmpty(this.parent.icutkey);
            this.gv_loggrpcd = this.gfn_isNullEmpty(this.parent.udf2);
            
        	this.ds_header.setColumn(0, "owkey", this.gv_owkey);
        	this.ds_header.setColumn(0, "seickey", this.gv_seickey);
        	this.ds_header.setColumn(0, "seiclinenum", this.gv_seiclinenum);
        	this.ds_header.applyChange();
        	
        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd, "");
        	this.gfn_getCode("UOM", this.ds_uom, "");
        	
        	this.gfn_decimalPointSet("mdt_qty");
            
            if(this.gv_flag == "U"){
        		this.edt_ickey.set_enable(false);
        		this.btn_searchIckey.set_enable(false);
        		
        		// Search cond, searchId, use Dataset, combo Name, insert value, default Value, callback
        		this.gfn_getCommCodeTran( "icutkey="+this.gv_icutkey+"|"+"owkey="+this.ds_header.getColumn(0, "owkey")
        								, "270"
        								, "ds_uom"
        								, "cbo_uom"
        								, ""
        								, ""
        								, "fn_search"
        								);
        		
        //		this.fn_search();
        	}else{
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "setItemCodeController");
        	this.gfn_setCommon("METHODNM", "selectSetItemCodeDt");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   =  "seickey="+this.gv_seickey
                         + " seiclinenum="+this.gv_seiclinenum
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "setItemCodeController");
        	this.gfn_setCommon("METHODNM", "insertSetItemCodeDt");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_setItemCodeMap=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "setItemCodeController");
        	this.gfn_setCommon("METHODNM", "updateSetItemCodeDt");
        	
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_setItemCodeMap=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        		
        		key = "OWKEY_SELECTION_LIST";
        		value = this.edt_owkey.value;
        		
        		if(this.gfn_isNotNull(this.edt_ickey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_ickey.value, "110", application.gv_ams, "", "", this.edt_ickey, this.edt_icname, "", "", "", "fn_codeCallBack('ickey')");
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			if(this.gfn_isNull(this.gfn_setParam("colName"))){
        				this.gfn_setParam("sortValue", "DESC");
        				this.gfn_setParam("colName", "UPDATEDATE");
        			}
        			
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			if(this.gfn_isNull(this.gfn_setParam("colName"))){
        				this.gfn_setParam("sortValue", "DESC");
        				this.gfn_setParam("colName", "UPDATEDATE");
        			}
        			
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        this.fn_codeCallBack = function(sKey)
        {
        	if(sKey == "ickey"){
        		this.edt_icutkey.set_value(application.gds_code.getColumn(0, "field4"));
        		this.edt_icutkey.set_tooltiptext(application.gds_code.getColumn(0, "field4"));
        		
        		if(this.gfn_isNull(this.edt_icutkey.value)){
        			this.ds_uom.clearData();
        		}else{
        			// Search cond, searchId, use Dataset, combo Name, insert value, default Value, callback
        			this.gfn_getCommCodeTran( "icutkey="+this.edt_icutkey.value+"|"+"owkey="+this.ds_header.getColumn(0, "owkey")
        									, "270"
        									, "ds_uom"
        									, "cbo_uom"
        									, ""
        									, ""
        									);
        		}
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {	
        	if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        	}

        	var dsObj = this.ds_header;
        	var dsCol = "owkey|seickey|ickey|qty|uom";
        	var sComp = "edt_owkey|edt_seickey|edt_ickey|mdt_qty|cbo_uom";
        	
        	if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_icname.value) && !this.gfn_isNull(this.edt_ickey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ickey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "D") this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey" || e.columnid == "seickey"){
        		if(e.columnid == "owkey"){
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        			this.edt_fn_get_master_impos.set_value(this.gv_Pvalue[2]);
        			
        			if(this.edt_fn_get_master_impos.value=='Y'){
        				this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        				return;
        			}
        		}
        	}else if(e.columnid == "ickey"){
        		this.edt_icname.set_value(this.gv_Pvalue[1]);
        		this.edt_icname.set_tooltiptext(this.gv_Pvalue[1]);
        		this.edt_icutkey.set_value(this.gv_Pvalue[3]);
        		this.edt_icutkey.set_tooltiptext(this.gv_Pvalue[3]);
        		
        		// Search cond, searchId, use Dataset, combo Name, insert value, default Value, callback
        		this.gfn_getCommCodeTran( "icutkey="+this.gv_Pvalue[3]+"|"+"owkey="+this.ds_header.getColumn(0, "owkey")
        								, "270"
        								, "ds_uom"
        								, "cbo_uom"
        								, ""
        								, ""
        								);
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* edt_owkey_onchanged 실행 */
        this.edt_owkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        	}
        }

        /* btn_searchIckey_onclick 실행 */
        this.btn_searchIckey_onclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("화주를 선택해주세요.");
        		return;
        	}
        	
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:1680
        				,putObj:this.edt_ickey
        				,putKey:"OWKEY_SELECTION_LIST|udf2"
        				,putValue:this.edt_owkey.value+"|"+this.gv_loggrpcd
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* edt_ickey_onchanged 실행 */
        this.edt_ickey_onchanged = function(obj,e)
        {
        	if(this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("화주를 선택해주세요.");
        		return;
        	}
        	
        	var key = "OWKEY_SELECTION_LIST";
        	var value = this.edt_owkey.value;
        	
        	if(this.gfn_isNotNull(this.edt_ickey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_ickey.value, "110", application.gv_ams, "", "", this.edt_ickey, this.edt_icname, "", "", "", "fn_codeCallBack('ickey')");
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_owkey.addEventHandler("onchanged", this.edt_owkey_onchanged, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.edt_ickey.addEventHandler("onchanged", this.edt_ickey_onchanged, this);
            this.btn_searchIckey.addEventHandler("onclick", this.btn_searchIckey_onclick, this);

        };

        this.loadIncludeScript("SetItemCodePopDtl.xfdl", true);

       
    };
}
)();
