﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ItemCode");
                this.set_classname("style01");
                this.set_titletext("상품");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/><Column id=\"dckey\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"256\"/><Column id=\"ictype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"icutkey\" type=\"STRING\" size=\"32\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"32\"/><Column id=\"rfidbottle\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"icgrtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"bottleprice\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"ctgkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflife\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"spec\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"taxyn\" type=\"STRING\" size=\"32\"/><Column id=\"uom\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"rfidbox\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"boxyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelfday\" type=\"STRING\" size=\"32\"/><Column id=\"stc_apply_grade_type_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"uom_desc\" type=\"STRING\" size=\"32\"/><Column id=\"icgrtype\" type=\"STRING\" size=\"32\"/><Column id=\"returnor_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"height\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"returnor\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"accstockdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"accstocktype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"serialinputtype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifecodetype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"weight\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"btlickey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"foodyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"volume\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"operstatype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"stc_apply_grade_name\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lengthpi\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"heightpi\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"boxyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"accexday\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level1\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ictype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level3\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"stc_apply_grade_cd_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level2\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"taxyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"stc_apply_grade_type\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ordstopdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"scmcheck_desc\" type=\"STRING\" size=\"32\"/><Column id=\"minordqty\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"foodyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level_nm3\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level_nm2\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"widthpi\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"boxickey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"accstocktype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"maxordqty\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"level_nm1\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lengthbox\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"dispshelfkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck\" type=\"STRING\" size=\"32\"/><Column id=\"releasedays\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"operstatype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ordstartdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lo_non_stock_type_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf5\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"chittype\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"lo_non_stock_type\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"32\"/><Column id=\"ordqty\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"udf4\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"udf1\" type=\"STRING\" size=\"32\"/><Column id=\"udf2\" type=\"STRING\" size=\"32\"/><Column id=\"brandcode\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"btlyn_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"stc_apply_grade_cd\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"boxprice\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"ickey\" type=\"STRING\" size=\"32\"/><Column id=\"scmurkey\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"btlyn\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifecodetype\" type=\"STRING\" size=\"32\"/><Column id=\"icname\" type=\"STRING\" size=\"32\"/><Column id=\"width\" type=\"BIGDECIMAL\" size=\"8\"/><Column id=\"regdate\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"shelflifeindicator_desc\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"shelflifeindicator\" type=\"STRING\" size=\"32\"/><Column id=\"serialinputtype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"chittype_desc\" type=\"UNDEFINED\" size=\"256\"/><Column id=\"ackey\" type=\"STRING\" size=\"256\"/><Column id=\"acname\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcdDesc\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkeyDesc\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_dckey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_icgrkey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_menu", this);
            obj._setContents("<ColumnInfo><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"name\" type=\"STRING\" size=\"256\"/><Column id=\"level\" type=\"BIGDECIMAL\" size=\"16\"/></ColumnInfo><Rows><Row><Col id=\"id\">Z</Col><Col id=\"level\">0</Col><Col id=\"name\">SCM팀 확인</Col></Row><Row><Col id=\"level\">1</Col><Col id=\"name\">SCM팀 확인</Col><Col id=\"id\">Y</Col></Row><Row><Col id=\"level\">1</Col><Col id=\"name\">SCM팀 확인해제</Col><Col id=\"id\">N</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_headerExcel", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_resultData", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_validParam", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_avqtyCheck", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("14");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "148", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"180\"/><Column size=\"130\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"110\"/><Column size=\"140\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"110\"/><Column size=\"110\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"160\"/><Column size=\"160\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"132\"/><Column size=\"100\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"145\"/><Column size=\"145\"/><Column size=\"100\"/><Column size=\"145\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"OWKEY\"/><Cell col=\"3\" text=\"OWNAME\"/><Cell col=\"4\" text=\"ICKEY\"/><Cell col=\"5\" text=\"ICNAME\"/><Cell col=\"6\" text=\"SPEC\"/><Cell col=\"7\" text=\"UOM\"/><Cell col=\"8\" text=\"CHGUOM\"/><Cell col=\"9\" text=\"ICGRNAME\"/><Cell col=\"10\" text=\"ICUTKEY\"/><Cell col=\"11\" text=\"CTG_LEVEL1\"/><Cell col=\"12\" text=\"CTG_LEVEL_NM1\"/><Cell col=\"13\" text=\"CTG_LEVEL2\"/><Cell col=\"14\" text=\"CTG_LEVEL_NM2\"/><Cell col=\"15\" text=\"CTG_LEVEL3\"/><Cell col=\"16\" text=\"CTG_LEVEL_NM3\"/><Cell col=\"17\" text=\"RETURNOR\"/><Cell col=\"18\" displaytype=\"normal\" text=\"ORDSTARTDATE\"/><Cell col=\"19\" text=\"ORDSTOPDATE\"/><Cell col=\"20\" text=\"RELEASEDAYS\"/><Cell col=\"21\" text=\"LO_NON_STOCK_TYPE\"/><Cell col=\"22\" text=\"DISPSHELFKEY\"/><Cell col=\"23\" text=\"RFIDBOX\"/><Cell col=\"24\" text=\"RFIDBOTTLE\"/><Cell col=\"25\" text=\"ICTYPE\"/><Cell col=\"26\" text=\"MINORDQTY\"/><Cell col=\"27\" text=\"MAXORDQTY\"/><Cell col=\"28\" text=\"WIDTHBOX\"/><Cell col=\"29\" text=\"LENGTHBOX\"/><Cell col=\"30\" text=\"HEIGHTBOX\"/><Cell col=\"31\" text=\"WEIGHT\"/><Cell col=\"32\" text=\"WIDTHPI\"/><Cell col=\"33\" text=\"LENGTHPI\"/><Cell col=\"34\" text=\"HEIGHTPI\"/><Cell col=\"35\" text=\"BRANDCODE\"/><Cell col=\"36\" text=\"VOLUME\"/><Cell col=\"37\" text=\"BOXPRICE\"/><Cell col=\"38\" text=\"BOTTLEPRICE\"/><Cell col=\"39\" text=\"SHELFLIFEINDICATOR\"/><Cell col=\"40\" text=\"SHELFLIFE\"/><Cell col=\"41\" text=\"SHELFLIFECODETYPE\"/><Cell col=\"42\" text=\"SERIALINPUTTYPE\"/><Cell col=\"43\" text=\"TAXYN\"/><Cell col=\"44\" displaytype=\"normal\" text=\"REGDATE\"/><Cell col=\"45\" text=\"FOODYN\"/><Cell col=\"46\" text=\"CHITTYPE\"/><Cell col=\"47\" text=\"STC_APPLY_GRADE_CD\"/><Cell col=\"48\" text=\"STOCK_GRADE_TYPE\"/><Cell col=\"49\" text=\"STOCK_GRADE_NAME\"/><Cell col=\"50\" text=\"ACCEXDAY\"/><Cell col=\"51\" text=\"OPERSTATYPE\"/><Cell col=\"52\" text=\"BOXYN\"/><Cell col=\"53\" text=\"BTLYN\"/><Cell col=\"54\" text=\"BOXICKEY\"/><Cell col=\"55\" text=\"BTLICKEY\"/><Cell col=\"56\" text=\"SHELFDAY\"/><Cell col=\"57\" text=\"ORDQTY\"/><Cell col=\"58\" text=\"ITEMTYPE\"/><Cell col=\"59\" text=\"LOGGRPCD\"/><Cell col=\"60\" text=\"ADD_EXPLN\"/><Cell col=\"61\" text=\"ORDER_DUETIME\"/><Cell col=\"62\" text=\"RELEASE_DUEDATE\"/><Cell col=\"63\" text=\"DELYN\"/><Cell col=\"64\" text=\"CLOSINGDATE\"/><Cell col=\"65\" text=\"INSERTDATE\"/><Cell col=\"66\" text=\"INSERTURKEY\"/><Cell col=\"67\" text=\"UPDATEDATE\"/><Cell col=\"68\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" expr=\"currow+1\"/><Cell col=\"2\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:owkey\"/><Cell col=\"3\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:owname\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ickey\" editdisplay=\"edit\"/><Cell col=\"5\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:icname\"/><Cell col=\"6\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:spec\"/><Cell col=\"7\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:uom_desc\"/><Cell col=\"8\" displaytype=\"number\" style=\"align:right;\" text=\"bind:udf3\"/><Cell col=\"9\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:icgrkey_desc\"/><Cell col=\"10\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:icutkey\"/><Cell col=\"11\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level1\"/><Cell col=\"12\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level_nm1\" calendardisplaynulltype=\"none\"/><Cell col=\"13\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level2\"/><Cell col=\"14\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level_nm2\" calendardisplaynulltype=\"default\"/><Cell col=\"15\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level3\"/><Cell col=\"16\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:level_nm3\"/><Cell col=\"17\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:returnor_desc\"/><Cell col=\"18\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ordstartdate\" mask=\"yyyy-MM-dd\" calendardisplaynulltype=\"none\"/><Cell col=\"19\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ordstopdate\" mask=\"yyyy-MM-dd\" calendardisplaynulltype=\"none\"/><Cell col=\"20\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:releasedays\"/><Cell col=\"21\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:lo_non_stock_type_desc\"/><Cell col=\"22\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:dispshelfkey\"/><Cell col=\"23\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:rfidbox\"/><Cell col=\"24\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:rfidbottle\"/><Cell col=\"25\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ictype_desc\"/><Cell col=\"26\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:minordqty\"/><Cell col=\"27\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:maxordqty\"/><Cell col=\"28\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:width\"/><Cell col=\"29\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:lengthbox\"/><Cell col=\"30\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:height\"/><Cell col=\"31\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:weight\"/><Cell col=\"32\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:widthpi\"/><Cell col=\"33\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:lengthpi\"/><Cell col=\"34\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:heightpi\"/><Cell col=\"35\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:brandcode\"/><Cell col=\"36\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:volume\"/><Cell col=\"37\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:boxprice\"/><Cell col=\"38\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:bottleprice\"/><Cell col=\"39\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:shelflifeindicator_desc\"/><Cell col=\"40\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:shelflife\"/><Cell col=\"41\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:shelflifecodetype_desc\"/><Cell col=\"42\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:serialinputtype_desc\"/><Cell col=\"43\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:taxyn_desc\"/><Cell col=\"44\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:regdate\" mask=\"yyyy-MM-dd\" calendardisplaynulltype=\"none\"/><Cell col=\"45\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:foodyn_desc\" calendardisplaynulltype=\"none\"/><Cell col=\"46\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:chittype\" calendardisplaynulltype=\"none\"/><Cell col=\"47\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:stc_apply_grade_cd_desc\"/><Cell col=\"48\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:stc_apply_grade_type_desc\" calendardisplaynulltype=\"none\"/><Cell col=\"49\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:stc_apply_grade_name\"/><Cell col=\"50\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:accexday\"/><Cell col=\"51\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:operstatype_desc\" calendardisplaynulltype=\"none\"/><Cell col=\"52\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:boxyn_desc\" calendardisplaynulltype=\"none\"/><Cell col=\"53\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:btlyn_desc\"/><Cell col=\"54\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:boxickey\" calendardisplaynulltype=\"none\"/><Cell col=\"55\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:btlickey\"/><Cell col=\"56\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:shelfday\"/><Cell col=\"57\" displaytype=\"number\" style=\"align:right;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:ordqty\"/><Cell col=\"58\" text=\"bind:udf1_desc\"/><Cell col=\"59\" text=\"bind:udf2_desc\"/><Cell col=\"60\" text=\"bind:udf4\"/><Cell col=\"61\" text=\"bind:udf5\"/><Cell col=\"62\" displaytype=\"normal\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:udf6_desc\" calendardisplay=\"display\" calendardisplaynulltype=\"none\"/><Cell col=\"63\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:delyn_desc\"/><Cell col=\"64\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:closingdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"65\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"66\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:inserturkey\"/><Cell col=\"67\" displaytype=\"date\" style=\"color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"68\" style=\"align:left;color:EXPR(dtlcnt == '0'?'#0000ff':'none');color2:EXPR(dtlcnt == '0'?'#0000ff':'none');\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_headerExcel", "absolute", "1097", "184", null, null, "37", "54", this.div_splitTop);
            obj.set_taborder("19");
            obj.set_binddataset("ds_headerExcel");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_visible("false");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"200\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"150\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell style=\"align: ;\" text=\"NO\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"OWKEY\"/><Cell col=\"3\" text=\"ICKEY\"/><Cell col=\"4\" text=\"ICNAME\"/><Cell col=\"5\" text=\"UOM\"/><Cell col=\"6\" text=\"ICGRKEY\"/><Cell col=\"7\" text=\"ICUTKEY\"/><Cell col=\"8\" text=\"ICUTORDER\"/><Cell col=\"9\" text=\"ICUTQTY\"/><Cell col=\"10\" text=\"COSTPRICE\"/><Cell col=\"11\" text=\"SALEPRICE\"/><Cell col=\"12\" text=\"WIDTHBOX\"/><Cell col=\"13\" text=\"LENGTHBOX\"/><Cell col=\"14\" text=\"HEIGHTBOX\"/><Cell col=\"15\" text=\"WEIGHT\"/><Cell col=\"16\" text=\"VOLUME\"/><Cell col=\"17\" text=\"BRANDCODE\"/><Cell col=\"18\" text=\"ACKEY\"/><Cell col=\"19\" text=\"UDF1\"/><Cell col=\"20\" text=\"UDF2\"/><Cell col=\"21\" text=\"UDF3\"/><Cell col=\"22\" text=\"UDF4\"/><Cell col=\"23\" text=\"UDF5\"/><Cell col=\"24\" text=\"SHELFLIFEINDICATOR\"/><Cell col=\"25\" text=\"SHELFLIFE\"/><Cell col=\"26\" text=\"SHELFLIFECODETYPE\"/><Cell col=\"27\" text=\"IFSTATUS\"/><Cell col=\"28\" text=\"MESSAGE\"/><Cell col=\"29\" text=\"PO_LEADTIME\"/><Cell col=\"30\" text=\"CLOSINGDATE\"/><Cell col=\"31\" text=\"INSERTDATE\"/><Cell col=\"32\" text=\"INSERTURKEY\"/><Cell col=\"33\" text=\"ULHSKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"number\" style=\"align:right;color2: ;\" expr=\"currow+1\"/><Cell col=\"1\" style=\"align:left;color2: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" style=\"align:left;color2: ;\" text=\"bind:owkey\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;color2: ;\" text=\"bind:ickey\" editdisplay=\"edit\"/><Cell col=\"4\" style=\"align:left;color2: ;\" text=\"bind:icname\"/><Cell col=\"5\" style=\"align:left;color2: ;\" text=\"bind:uom_desc\"/><Cell col=\"6\" style=\"align:left;color2: ;\" text=\"bind:icgrkey\"/><Cell col=\"7\" displaytype=\"normal\" style=\"align:left;color2: ;\" text=\"bind:icutkey\"/><Cell col=\"8\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:icutorder\"/><Cell col=\"9\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:icutqty\"/><Cell col=\"10\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:costprice\"/><Cell col=\"11\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:saleprice\"/><Cell col=\"12\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:width\"/><Cell col=\"13\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:lengthbox\"/><Cell col=\"14\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:height\"/><Cell col=\"15\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:weight\"/><Cell col=\"16\" displaytype=\"number\" style=\"align:right;color2: ;\" text=\"bind:volume\"/><Cell col=\"17\" style=\"align:left;color2: ;\" text=\"bind:brandcode\"/><Cell col=\"18\" style=\"align:left;color2: ;\" text=\"bind:ackey\"/><Cell col=\"19\" displaytype=\"normal\" style=\"align:left;color2: ;\" text=\"bind:udf1\" calendardisplaynulltype=\"none\"/><Cell col=\"20\" displaytype=\"normal\" style=\"align:left;color2: ;\" text=\"bind:udf2\" calendardisplaynulltype=\"none\"/><Cell col=\"21\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;color2: ;\" text=\"bind:udf3\"/><Cell col=\"22\" displaytype=\"normal\" style=\"align:left;color2: ;\" text=\"bind:udf4\" calendardisplaynulltype=\"none\"/><Cell col=\"23\" displaytype=\"normal\" style=\"align:left;color2: ;\" text=\"bind:udf5\"/><Cell col=\"24\" style=\"align:left;color2: ;\" text=\"bind:shelflifeindicator_desc\"/><Cell col=\"25\" style=\"align:left;color2: ;\" text=\"bind:shelflife\"/><Cell col=\"26\" style=\"align:left;color2: ;\" text=\"bind:shelflifecodetype_desc\"/><Cell col=\"27\" style=\"align:left;color2: ;\" text=\"bind:ifstatus_desc\"/><Cell col=\"28\" style=\"align:left;color2: ;\" text=\"bind:ifmessage\"/><Cell col=\"29\" style=\"align:left;color2: ;\" text=\"bind:po_leadtime\"/><Cell col=\"30\" displaytype=\"date\" style=\"color2: ;\" text=\"bind:closingdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"31\" displaytype=\"date\" style=\"color2: ;\" text=\"bind:cinsertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"32\" style=\"align:left;color2: ;\" text=\"bind:inserturkey\"/><Cell col=\"33\" style=\"align:left;color2: ;\" text=\"bind:ulhskey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("22");
            obj.set_text("상품 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", "321", null, null, "0", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_scrollbars("autoboth");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"100\"/><Column size=\"200\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"CTNAME\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ctname\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_text("상품별 센터 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Grid("grd_detail_save", "absolute", null, "29", "820", null, "0", "0", this.div_splitBottom);
            obj.set_taborder("22");
            obj.set_binddataset("ds_detail_save");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_scrollbars("autoboth");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"100\"/><Column size=\"200\"/><Column size=\"0\"/><Column size=\"130\"/><Column size=\"0\"/><Column size=\"100\"/><Column size=\"0\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"CTKEY\"/><Cell col=\"2\" text=\"CTNAME\"/><Cell col=\"3\" text=\"LOGGRPCD_cd\"/><Cell col=\"4\" text=\"LOGGRPCD\"/><Cell col=\"5\" text=\"DCKEY_cd\"/><Cell col=\"6\" text=\"DCKEY\"/><Cell col=\"7\" text=\"ICGR_cd\"/><Cell col=\"8\" text=\"ICGR\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ctname\"/><Cell col=\"3\" style=\"align:left;padding: ;\" text=\"bind:loggrpcd\"/><Cell col=\"4\" displaytype=\"normal\" style=\"align:left;padding: ;\" text=\"bind:loggrpcdDesc\"/><Cell col=\"5\" style=\"align:left;padding: ;\" text=\"bind:dckey\"/><Cell col=\"6\" displaytype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:dckey\" combodataset=\"ds_dckey\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"7\" style=\"align:left;padding: ;\" text=\"bind:icgrkey\"/><Cell col=\"8\" displaytype=\"normal\" style=\"align:left;padding: ;\" text=\"bind:icgrkeyDesc\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_left", "absolute", "342", "110", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("23");
            obj.set_text(">");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_right", "absolute", "342", "143", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("24");
            obj.set_text("<");
            obj.set_visible("true");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("25");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Combo("cbo_loggrpcd", "absolute", null, "0", "154", "24", "-185", null, this.div_splitBottom);
            this.div_splitBottom.addChild(obj.name, obj);
            obj.set_taborder("27");
            obj.set_innerdataset("@ds_loggrpcd");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_index("-1");
            obj = new Combo("cbo_icgrkey", "absolute", null, "0", "122", "24", "-141", null, this.div_splitBottom);
            this.div_splitBottom.addChild(obj.name, obj);
            obj.set_innerdataset("@ds_icgrkey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_taborder("31");
            obj.set_displayrowcount("15");
            obj.set_enable("false");
            obj = new Button("btn_searchAckey", "absolute", null, "0", "26", "24", "-50", null, this.div_splitBottom);
            obj.set_taborder("34");
            obj.set_cssclass("btn_WF_srh_s");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_ackey", "absolute", null, "0", "54", "24", "-77", null, this.div_splitBottom);
            obj.set_taborder("35");
            obj.set_enable("false");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_acname", "absolute", null, "0", "254", "24", "-923", null, this.div_splitBottom);
            obj.set_taborder("36");
            obj.set_enable("false");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static03", "absolute", null, "0", "68", "24", "-657", null, this.div_splitBottom);
            obj.set_taborder("37");
            obj.set_text("매입처 코드");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_icgrname", "absolute", null, "0", "102", "24", "-583", null, this.div_splitBottom);
            obj.set_taborder("38");
            obj.set_enable("false");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static02", "absolute", null, "0", "52", "24", "-471", null, this.div_splitBottom);
            obj.set_taborder("39");
            obj.set_text("상품 그룹");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Combo("cbo_dckey", "absolute", null, "0", "106", "24", "-415", null, this.div_splitBottom);
            this.div_splitBottom.addChild(obj.name, obj);
            obj.set_taborder("40");
            obj.set_innerdataset("@ds_dckey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_enable("false");
            obj.set_visible("false");
            obj = new Static("Static01", "absolute", null, "0", "52", "24", "-300", null, this.div_splitBottom);
            obj.set_taborder("41");
            obj.set_text("회차구분");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_loggrpcd", "absolute", null, "0", "102", "24", "-242", null, this.div_splitBottom);
            obj.set_taborder("42");
            obj.set_enable("false");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", null, "0", "100", "24", "-136", null, this.div_splitBottom);
            obj.set_taborder("43");
            obj.set_text("물류 그룹 코드");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_excelUpload", "absolute", null, "0", "33", "24", "0", null, this);
            obj.set_taborder("6");
            obj.set_cssclass("btn_WF_excelup");
            obj.set_tooltiptext("엑셀 업로드");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item11","div_splitBottom.Static00","text","gds_lang","LOGGRPCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","div_splitBottom.edt_loggrpcd","value","ds_header","udf2_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.Static01","text","gds_lang","DCKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitBottom.Static02","text","gds_lang","ICGR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitBottom.edt_icgrname","value","ds_header","icgrkey_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitBottom.Static03","text","gds_lang","ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","div_splitBottom.edt_acname","value","ds_header","acname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","div_splitBottom.edt_ackey","value","ds_detail_save","ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","btn_excelUpload","tooltiptext","gds_lang","EXCELUPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","div_splitTop.Static00","text","gds_lang","ME00000430_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","div_splitBottom.Static06","text","gds_lang","ME00000430_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("ItemCode.xfdl", "lib::Comm.xjs");
        this.registerScript("ItemCode.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ItemCode.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_headerExcel = this.div_splitTop.grd_headerExcel;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_detail_save = this.div_splitBottom.grd_detail_save;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_Pvalue = "";
        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";
        var dtlsearchcnt = 0;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.ds_menu.setColumn(0, "name", this.gfn_isNullEmpty(this.gfn_getLang("SCMCHECK")));

        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd);
        	this.gfn_getCode("DCKEY", this.ds_dckey);
        	
        	//this.div_splitBottom.cbo_loggrpcd.set_value("1");
        	this.div_splitBottom.cbo_dckey.set_value("1");
        	
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        //	var gridMenuSet = ["grd_header^1|0|0|0|0|0|1|1", "grd_detail^0|0|0|0|0|0|1|1"];
        	this.gv_grdList = [this.gv_header, this.gv_headerExcel, this.gv_detail, this.gv_detail_save]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	this.fn_calculateDetailPosition();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "selectItemCodeInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
         
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				this.ds_detail.clearData();
        				this.ds_detail_save.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		this.ds_detail.clearData();
        		this.ds_detail_save.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchIcgr = function()
        {
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.ds_icgrkey.clearData();

        	this.gfn_setCommon("BEANID", "itemGroupController");
        	this.gfn_setCommon("METHODNM", "selectItemGroupCode");
        	
            var sSvcId   = "selectIcgr";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_icgrkey=OUT_rtnGrid";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_searchDetail = function()
        {
        	this.gfn_getScroll(this.gv_detail_save); // 스크롤 위치 저장
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.ds_detail.clearData();
        	this.ds_detail_save.clearData();

        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "selectItemCodeCenter");
            var sSvcId   = "selectDetail"+dtlsearchcnt++;
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid1 ds_detail_save=OUT_rtnGrid2";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " ickey="+this.ds_header.getColumn(this.gv_nRow, "ickey")
                         + " icutkey="+this.ds_header.getColumn(this.gv_nRow, "icutkey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_avqtyCheck = function()
        {	
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.ds_avqtyCheck.clearData();

        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "selectItemCodeAvqtyCheck");
            var sSvcId   = "avQtyCheck";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_avqtyCheck=OUT_rtnGrid";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " ickey="+this.ds_header.getColumn(this.gv_nRow, "ickey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_delete = function(sAll)
        {
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "saveItemCode");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_header:U";
            var sOutData = "ds_resultData=OUT_resultData";
            var sParam   = "workType=DELETE"
                         + " chkAll="+sAll
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_saveDetail = function()
        {
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "saveItemCodeCenter");
        	
            var sSvcId   = "saveDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_list=ds_detail_save";
            var sOutData = "";
            var sParam   =  "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " ickey="+this.ds_header.getColumn(this.gv_nRow, "ickey")
                         + " ackey="+this.div_splitBottom.edt_ackey.value
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_saveScm = function(yn)
        {
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "saveItemCode");
        	
        	this.gfn_grdCheckCopyToDataset(this.div_splitTop.grd_header, this.ds_header, this.ds_headerExcel);
            
            var sSvcId   = "saveScm";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_SCM_LIST=ds_headerExcel";
            var sOutData = "";
            var sParam   =  "workType=SCM"
                         + " scm_workType="+yn
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_validCntrCd = function()
        {
        	this.gfn_setCommon("BEANID", "itemCodeController");
        	this.gfn_setCommon("METHODNM", "validCntrCd");
            
            var sSvcId   = "validCntrCd";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_SAVE_LIST=ds_detail_save";
            var sOutData = "ds_validParam=OUT_resultData";
            var sParam   = "icutkey="+this.ds_header.getColumn(this.ds_header.rowposition, "icutkey")
        			     + " icuttype="+this.ds_header.getColumn(this.ds_header.rowposition, "uom")
        				 + " icgrkey="+this.ds_header.getColumn(this.ds_header.rowposition, "icgrkey")
        				 + " icgrtype="+this.ds_header.getColumn(this.ds_header.rowposition, "icgrtype")
        				 + " owkey="+this.ds_header.getColumn(this.ds_header.rowposition, "owkey")
        				 + " ickey="+this.ds_header.getColumn(this.ds_header.rowposition, "ickey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_constDsSet(this.gv_header);
        		
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.fn_searchDetail();
        			this.fn_searchIcgr();
        			//this.fn_avqtyCheck();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        			this.ds_header.addColumn("STATUS");
        			
        		}else{
        			this.div_splitBottom.cbo_icgrkey.set_value('');
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId.substr(0, 12) == "selectDetail"){
        		this.ds_detail.addColumn("CHK");
        		this.ds_detail_save.addColumn("CHK");
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		if(this.ds_detail_save.rowcount == 0){
        			this.gv_detail_save.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_detail);
        		this.gfn_constDsSet(this.gv_detail_save);
        		this.searchFalg = "";
        		this.fn_avqtyCheck();
        	}else if(sSvcId == "selectIcgr"){
        		if(this.ds_icgrkey.rowcount > 0){
        			this.div_splitBottom.cbo_icgrkey.set_value(this.ds_header.getColumn(0,'icgrkey'));
        			this.div_splitBottom.edt_ackey.set_value(this.ds_header.getColumn(0,'ackey'));
        			this.div_splitBottom.edt_acname.set_value(this.ds_header.getColumn(0,'acname'));
        		
        		}
        	}else if(sSvcId == "avQtyCheck"){
        		if(this.ds_avqtyCheck.rowcount > 0){
        			
        // 			this.div_splitBottom.cbo_icgrkey.set_value(this.ds_header.getColumn(0,'icgrkey'));
        // 			this.div_splitBottom.edt_ackey.set_value(this.ds_header.getColumn(0,'ackey'));
        // 			this.div_splitBottom.edt_acname.set_value(this.ds_header.getColumn(0,'acname'));
        	//수정 삭제 디테일 오른쪽으로 옮기는거 안되게 
        		
        		}	
        	}else if(sSvcId == "delete"){
        		this.searchFalg = "save";
        		//if(this.ds_resultData.getColumn(0,'flag') == 'true') {
        			this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        				this.parent.div_cond.btn_search.click();
        			});
        			
        // 		} else {
        // 			this.gfn_alert("MSG_FOREIGNKEYEXISTS", "", function(msg, flag){
        // 				this.parent.div_cond.btn_search.click();
        // 			});
        // 		}
        		
        	}else if(sSvcId == "saveScm"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "saveDetail"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	}else if(sSvcId == "validCntrCd"){
        		var resultData = this.ds_validParam.getColumn(0, "result");
        		if(resultData == "0"){
        			this.fn_validationIsTrue(false);
        		}else{
        			this.fn_validationIsTrue(true);
        		}
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        // bottom 크기 조절
        this.fn_calculateDetailPosition = function(){
        	var btn_Gap = 10;
        	var btn_HGap = 47;
        	
        	var v_nXLeft = nexacro.round(this.div_splitBottom.getOffsetWidth() / 3) - nexacro.round(this.div_splitBottom.btn_left.getOffsetWidth() / 2) - btn_Gap;
        	var v_nXRight = nexacro.round(this.div_splitBottom.getOffsetWidth() / 3 * 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetWidth() / 2) - btn_Gap;
        	var v_nY = nexacro.round(this.gv_detail.getOffsetHeight() / 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetHeight() / 2);
        	
        	this.gv_detail.set_width(v_nXLeft);
        	this.gv_detail_save.set_width(v_nXRight);
        	this.div_splitBottom.btn_left.set_left(v_nXLeft+btn_Gap);
        	this.div_splitBottom.btn_right.set_left(v_nXLeft+btn_Gap);

        	this.div_splitBottom.btn_left.set_top(v_nY+btn_Gap);
        	this.div_splitBottom.btn_right.set_top(v_nY+btn_HGap);
        }

        // 공통 검색 팝업 콜백
        this.fn_callBackSearchPop = function()
        {
        	this.div_splitBottom.edt_acname.set_value(this.gv_Pvalue[1]);
        	this.div_splitBottom.edt_acname.set_tooltiptext(this.gv_Pvalue[1]);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.rowChg = "Y";
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.grd_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_detail.rowcount > 0){
        		this.gfn_sortDataset(obj, e);
        	}else if(colName == "CHK"){
        		this.gfn_gridProc(obj, e);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("itemCodeController/excelDown.do", this.gv_header, this, oValue);
        }

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("dataset.getRowType(currow) == '2' || dataset.getRowType(currow) == '8'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			this.fn_searchDetail();
        			this.div_splitBottom.cbo_icgrkey.set_value(this.ds_header.getColumn(this.ds_header.rowposition,'icgrkey'));
        		}
        	}
        	
        	this.rowChg = "";
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK")	obj.set_updatecontrol(false);
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("dataset.getRowType(currow) == '2' || dataset.getRowType(currow) == '8'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var nChkCnt = this.ds_header.getCaseCount("STATUS == 'D'");
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(nChkCnt == this.ds_header.rowcount){
        			var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        			var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        			var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        			
        			this.gfn_confirm3Btn (
        				arrBtnText,
        				sMsgCode,
        				sMsgText,
        				function(msg, flag) {
        					if (flag != "0") {
        						if (flag == "1") this.fn_delete("");
        						if (flag == "2") this.fn_delete("ALL");
        					} else { //최소시 ds_header reset
        						this.ds_header.reset();
        					}
        				}
        			);
        		}else{
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.fn_delete("");
        				} else { //최소시 ds_header reset
        					this.ds_header.reset();
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	
        // 	if(this.ds_avqtyCheck.rowcount > 0){
        // 		this.gfn_alert("ddddd");
        // 		return;
        // 	}
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        		if(this.ds_header.getColumn(nRow, "fn_get_master_impos")=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        		if(this.ds_header.getColumn(nRow,"ttqty") > 0){
        			this.gfn_alert((nRow+1) +"줄의 "+this.ds_header.getColumn(nRow,"icname")+"은(/는) 재고가 존재하여 삭제 불가능합니다.");
        			return;
        		}
        			this.ds_header.setColumn(nRow, "STATUS", "D");
        			nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        		}
        		this.div_splitTop_btn_save_onclick();
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
          	if(this.ds_header.getColumn(this.ds_header.rowposition, "fn_get_master_impos")=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        	}
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("ItemCodePop", "item::ItemCodePop.xfdl", oArg, 959, 734, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	
        	if(this.ds_header.rowcount > 0){
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "fn_get_master_impos")=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        		var oArg = { argFlag:"U"
        					,menuId:this.parent.gv_menuId
        					,owkey:this.ds_header.getColumn(this.ds_header.rowposition, "owkey")
        					,ickey:this.ds_header.getColumn(this.ds_header.rowposition, "ickey")
        					,icutkey:this.ds_header.getColumn(this.ds_header.rowposition, "icutkey")
        					,shelflifeindicator:this.ds_header.getColumn(this.ds_header.rowposition, "shelflifeindicator")
        					,ttqty:this.ds_header.getColumn(this.ds_header.rowposition,"ttqty")
        				   };
        		this.gfn_popup("ItemCodePop", "item::ItemCodePop.xfdl", oArg, 959, 734, "");
        	}else{
        		this.gfn_alert("MSG_90001");
        	}
        }

        /* div_splitBottom_onsize 실행 */
        this.div_splitBottom_onsize = function(obj,e)
        {
        	this.fn_calculateDetailPosition();
        }

        /* div_splitBottom_btn_left_onclick 실행 */
        this.div_splitBottom_btn_left_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	var addRow = 0;
        	var loggrpcd = this.ds_header.getColumn(this.ds_header.rowposition,"udf2");
        	var d_loggrpcd = this.div_splitBottom.cbo_loggrpcd.value

        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			
        // 		if(loggrpcd != d_loggrpcd){
        // 			this.gfn_alert("이미 지정된 물류그룹코드를 변경할 수 없습니다.\n 변경을 원하시면 상품을 삭제 후 다시 등록해주세요.");
        // 			return;
        // 		}
        		
        		
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "fn_get_master_impos")=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        		if(this.ds_header.getColumn(this.ds_header.rowposition, "fn_get_master_impos")=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        			if(this.ds_detail_save.findRow("ctkey", this.ds_detail.getColumn(nRow, "ctkey")) == -1){
        			//var nRow2 = this.ds_detail_save.findRowExpr("ctkey =='" + this.ds_detail.getColumn(nRow, "ctkey") + "' && loggrpcd =='"+this.div_splitBottom.cbo_loggrpcd.value +"'");
        			//if(nRow2 == -1) {
        				addRow = this.ds_detail_save.addRow();
        				this.ds_detail_save.copyRow(addRow, this.ds_header, this.gv_nRow);
        				this.ds_detail_save.copyRow(addRow, this.ds_detail, nRow);
        				//this.ds_detail_save.setColumn(addRow, "loggrpcd", this.div_splitBottom.cbo_loggrpcd.value);
        				this.ds_detail_save.setColumn(addRow, "loggrpcd", this.ds_header.getColumn(this.ds_header.rowposition,"udf2"));
        				this.ds_detail_save.setColumn(addRow, "loggrpcdDesc", this.ds_header.getColumn(this.ds_header.rowposition,"udf2_desc"));
        				this.ds_detail_save.setColumn(addRow, "dckey", this.div_splitBottom.cbo_dckey.value);
        				//this.ds_detail_save.setColumn(addRow, "icgrkey", this.div_splitBottom.cbo_icgrkey.value);
        				this.ds_detail_save.setColumn(addRow, "icgrkey", this.ds_header.getColumn(this.ds_header.rowposition,"icgrkey"));
        				this.ds_detail_save.setColumn(addRow, "icgrkeyDesc", this.ds_header.getColumn(this.ds_header.rowposition,"icgrkey_desc"));
        				this.ds_detail_save.setColumn(addRow, "icutkey", this.ds_header.getColumn(this.ds_header.rowposition,"icutkey"));
        			}
        			
        			nRow = this.ds_detail.findRowExpr("CHK == '1'", nRow+1);
        		}
        	}
        	
        	this.ds_detail_save.set_keystring("S:+ctkey");
        }

        /* div_splitBottom_btn_right_onclick 실행 */
        this.div_splitBottom_btn_right_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			if(this.ds_header.getColumn(nRow, "fn_get_master_impos")=='Y'){
        				this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        				return;
        			}
        			if(this.ds_avqtyCheck.getColumn(nRow,"ttqty") > 0){
        				this.gfn_alert("센터코드("+this.ds_avqtyCheck.getColumn(nRow,"ctkey")+")은(/는) 재고가 존재하여 수정 불가합니다.");
        				return;
        			}
        			
        			this.ds_detail_save.deleteRow(nRow);
        			nRow = this.ds_detail_save.findRowExpr("CHK == '1'");
        		}
        	}
        }

        /* div_splitBottom_btn_save_onclick 실행 */
        this.div_splitBottom_btn_save_onclick = function(obj,e)
        {
        	//this.fn_validCntrCd();
        	if(this.gfn_isUpdate(this.ds_detail_save)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_saveDetail();
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.fn_validationIsTrue= function(outFlag)
        {
        	if(outFlag){
        // 		var flag = true;
        // 		for(var i = 0; i < this.ds_detail_save.rowcount; i++) {
        // 			if(this.ds_detail_save.getColumn(i,'icgrkey') != this.ds_header.getColumn(this.ds_header.rowposition, 'icgrkey')) {
        // 				flag = false;
        // 				break;
        // 			}
        // 		}
        // 		
        // 		if(!flag) { this.gfn_alert("상품그룹코드가 다른 데이터 입니다."); return; }
        // 		
        // 		if(this.gfn_isNull(this.div_splitBottom.edt_ackey.value)){
        // 			this.gfn_alert("MSG_10001", "", function(msg, flag){
        // 				this.div_splitBottom.btn_searchAckey.setFocus();
        // 			});
        // 			
        // 			return;
        // 		}
        		
        //		if(this.gfn_isUpdate(this.ds_detail_save)){
        //			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        //				if(flag){
        					this.fn_saveDetail();
        //				}
        //			});
        // 		}else{
        // 			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        // 		}
        	}else{
        		this.gfn_alert("상품 단위와 상품 그룹을 먼저 센터에 등록해주세요.");
        		return;
        	};
        }

        /* div_splitTop_mnu_scm_onmenuclick 실행 */
        this.div_splitTop_mnu_scm_onmenuclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.fn_saveScm(e.id);
        	}
        }

        /* div_splitBottom_btn_searchAckey_onclick 실행 */
        this.div_splitBottom_btn_searchAckey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:40
        				,putObj:this.div_splitBottom.edt_ackey
        				,putCallback:"fn_callBackSearchPop"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* oncelldblclick 실행 */
        this.oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        this.btn_excelUpload_onclick = function(obj,e)
        {
        	var oArg = {
        		menuId:"ME00000450",
        		uskey: "US00000550",
        		menuNm:"ITEMCODE EXCELUPLOAD",
        		menu: "comm::ExcelUploadPop.xfdl",
        		ultype : this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_ITEM,
        		apkey : application.gv_activeApp,
        		controllerService : "itemCodeUploadController",
        		uploadAfterSearch : "selectItemCodeTemp",
        		tab2SearchService : "selectItemCodeUploadInfo",
        		uploadService : "uploadItemCodeExcel",
        		saveService : "saveItemCode",
        		samplePath : "item",
        		sampleDownName : "Item_ICUP_v1.0.xls",
        		excelDownService : "excelDown"
        	};
        	
        	this.gfn_popup("itemCodeExcelUploadPop", "comm::ExcelUploadPop.xfdl", oArg, 1918, 948, "");		
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_headerExcel.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitBottom.addEventHandler("onsize", this.div_splitBottom_onsize, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.grd_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.oncelldblclick, this);
            this.div_splitBottom.grd_detail_save.addEventHandler("onheadclick", this.grd_onheadclick, this);
            this.div_splitBottom.grd_detail_save.addEventHandler("oncelldblclick", this.oncelldblclick, this);
            this.div_splitBottom.btn_left.addEventHandler("onclick", this.div_splitBottom_btn_left_onclick, this);
            this.div_splitBottom.btn_right.addEventHandler("onclick", this.div_splitBottom_btn_right_onclick, this);
            this.div_splitBottom.btn_save.addEventHandler("onclick", this.div_splitBottom_btn_save_onclick, this);
            this.div_splitBottom.cbo_loggrpcd.addEventHandler("onitemchanged", this.div_splitBottom_cbo_loggrpcd_onitemchanged, this);
            this.div_splitBottom.btn_searchAckey.addEventHandler("onclick", this.div_splitBottom_btn_searchAckey_onclick, this);
            this.btn_excelUpload.addEventHandler("onclick", this.btn_excelUpload_onclick, this);

        };

        this.loadIncludeScript("ItemCode.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
