﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MixShipItemImposs");
                this.set_classname("MixShipItemImposs");
                this.set_titletext("혼적불가상품");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"owname\" type=\"STRING\" size=\"256\"/><Column id=\"group_id\" type=\"STRING\" size=\"256\"/><Column id=\"group_nm\" type=\"STRING\" size=\"256\"/><Column id=\"group_flag\" type=\"STRING\" size=\"256\"/><Column id=\"remark\" type=\"STRING\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"group_id\" type=\"STRING\" size=\"256\"/><Column id=\"sub_group_id\" type=\"STRING\" size=\"256\"/><Column id=\"group_nm\" type=\"STRING\" size=\"256\"/><Column id=\"group_flag\" type=\"STRING\" size=\"256\"/><Column id=\"icgrtype\" type=\"STRING\" size=\"256\"/><Column id=\"icgrtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"256\"/><Column id=\"icgrname\" type=\"STRING\" size=\"256\"/><Column id=\"ickey\" type=\"STRING\" size=\"256\"/><Column id=\"icname\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_paramDetail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_groupflag", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("20");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("22");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("23");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("24");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"200\"/><Column size=\"150\"/><Column size=\"200\"/><Column size=\"180\"/><Column size=\"150\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"OWKEY\"/><Cell col=\"4\" text=\"OWNAME\"/><Cell col=\"5\" text=\"GROUP_ID\"/><Cell col=\"6\" text=\"GROUP_NM\"/><Cell col=\"7\" text=\"GROUP_FLAG\"/><Cell col=\"8\" edittype=\"text\" text=\"REMARK\"/><Cell col=\"9\" text=\"INSERTDATE\"/><Cell col=\"10\" text=\"INSERTURKEY\"/><Cell col=\"11\" text=\"UPDATEDATE\"/><Cell col=\"12\" text=\"UPDATEURKEY\"/><Cell col=\"13\" text=\"DELYN\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" edittype=\"expr:dataset.getRowType(currow)=='2' ? 'expand' : 'none'\" style=\"align:left;padding: ;\" text=\"bind:owkey\" expandshow=\"expr:dataset.getRowType(currow) == &quot;2&quot; ? &quot;show&quot; : &quot;hide&quot;\" expandimage=\"URL('img::ico_search.png')\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:owname\" editdisplay=\"edit\"/><Cell col=\"5\" edittype=\"none\" text=\"bind:group_id\"/><Cell col=\"6\" edittype=\"text\" text=\"bind:group_nm\"/><Cell col=\"7\" displaytype=\"combo\" edittype=\"expr:dataset.getRowType(currow)=='2' ? 'combo' : 'none'\" style=\"align:left;background:EXPR(dataset.getRowType(currow)=='2' ? '#fff8d5' : '');background2:EXPR(dataset.getRowType(currow)=='2' ? '#fff5c2' : '');selectbackground:#e1e0d9ff;\" text=\"bind:group_flag\" combodataset=\"ds_groupflag\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"8\" edittype=\"text\" text=\"bind:remark\"/><Cell col=\"9\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"11\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"12\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/><Cell col=\"13\" displaytype=\"normal\" edittype=\"none\" text=\"bind:delyn_desc\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("25");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("26");
            obj.set_text("혼적 불가 상품 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("23");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"200\"/><Column size=\"180\"/><Column size=\"150\"/><Column size=\"200\"/><Column size=\"150\"/><Column size=\"120\"/><Column size=\"80\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" text=\"NO\"/><Cell col=\"3\" text=\"GROUP_ID\"/><Cell col=\"4\" text=\"GROUP_NM\"/><Cell col=\"5\" text=\"GROUP_FLAG\"/><Cell col=\"6\" text=\"ICGRKEY\"/><Cell col=\"7\" text=\"ICGRNAME\"/><Cell col=\"8\" text=\"ICKEY\"/><Cell col=\"9\" text=\"ICNAME\"/><Cell col=\"10\" text=\"CHGUOM\"/><Cell col=\"11\" text=\"LOGGRPCD\"/><Cell col=\"12\" text=\"INSERTDATE\"/><Cell col=\"13\" text=\"INSERTURKEY\"/><Cell col=\"14\" text=\"UPDATEDATE\"/><Cell col=\"15\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" style=\"align:left;padding: ;\" text=\"bind:group_id\"/><Cell col=\"4\" style=\"align:left;padding: ;\" text=\"bind:group_nm\"/><Cell col=\"5\" displaytype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:group_flag\" combodataset=\"ds_groupflag\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"6\" style=\"align:left;background: ;\" text=\"bind:icgrkey\" expandshow=\"hide\" combodisplay=\"edit\"/><Cell col=\"7\" style=\"align:left;padding: ;\" text=\"bind:icgrname\"/><Cell col=\"8\" style=\"align:left;background: ;\" text=\"bind:ickey\"/><Cell col=\"9\" text=\"bind:icname\"/><Cell col=\"10\" displaytype=\"normal\" text=\"bind:udf3\" calendardisplaynulltype=\"default\"/><Cell col=\"11\" displaytype=\"normal\" edittype=\"none\" text=\"bind:loggrpcd_desc\"/><Cell col=\"12\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"13\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"14\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"15\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("24");
            obj.set_text("혼적 불가 상품 상세 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitBottom);
            obj.set_taborder("25");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "74", null, this.div_splitBottom);
            obj.set_taborder("27");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("28");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "37", null, this.div_splitBottom);
            obj.set_taborder("29");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("MixShipItemImposs");
            		p.set_titletext("혼적불가상품");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitBottom.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","div_splitBottom.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00110961_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","div_splitBottom.Static06","text","gds_lang","ME00110961_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("MixShipItemImposs.xfdl", "lib::Comm.xjs");
        this.registerScript("MixShipItemImposs.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : MixShipItemImposs.xfdl
        * PROGRAMMER  : yachoi
        * DATE        : 2018.06.19
        * DESCRIPTION : 혼적 상품 불가
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";
        var dtlsearchcnt = 0;
        this.gv_bgBodyColor = "#fff8d5";
        this.gv_bg1BodyColor = "#fff5c2";
        this.gv_bgSelBodyColor = "#e1e0d9";

        this.gv_Pvalue = "";
        this.gv_code = "";
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	
        	this.gfn_getCode("GROUP_FLAG", this.ds_groupflag, "");
        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd);
        	
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header, this.gv_detail]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main, this.gv_detral]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	this.ds_detail.clearData();
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "mixShipItemImpossController");
        	this.gfn_setCommon("METHODNM", "selectMixShipItemImpossHd");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	
        }

        this.fn_searchDetail = function()
        {
        	this.gfn_getScroll(this.gv_detail); // 스크롤 위치 저장
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.gv_detail.set_nodatatext("");
        	this.ds_detail.clearData();

        	this.gfn_setCommon("BEANID", "mixShipItemImpossController");
        	this.gfn_setCommon("METHODNM", "selectMixShipItemImpossDt");
        	
            var sSvcId   = "selectDetail"+dtlsearchcnt++;
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid ds_paramDetail=OUT_PARAM";
            var sParam   = "owkey="+this.ds_header.getColumn(this.gv_nRow, "owkey")
                         + " group_id="+this.ds_header.getColumn(this.gv_nRow, "group_id")
                         + " group_flag="+this.ds_header.getColumn(this.gv_nRow, "group_flag")
                         + " delyn="+this.ds_header.getColumn(this.gv_nRow, "delyn")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", true, false);
        }

        this.fn_save = function(sAll)
        {
        	this.gfn_setCommon("BEANID", "mixShipItemImpossController");
        	this.gfn_setCommon("METHODNM", "saveMixShipItemImposs");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_INPUT_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "chkAll="+sAll;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_saveDetail = function()
        {
        	this.gfn_setCommon("BEANID", "mixShipItemImpossController");
        	this.gfn_setCommon("METHODNM", "saveDetailMixShipItemImposs");
        	
            var sSvcId   = "saveDetail";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_INPUT_LIST=ds_detail:U";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.ds_header.addColumn("STATUS");
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}

        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        		
        	}else if(sSvcId.substr(0, 12) == "selectDetail"){
        		this.ds_detail.addColumn("CHK");
        		this.ds_detail.addColumn("STATUS");
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		if(this.ds_detail.rowcount > 0){
        			var group_flag = this.ds_detail.getColumn(0,"group_flag");
        			
        			if(group_flag == "1"){ 
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "expandshow","show");
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "expandshow","hide");
        			}else if(group_flag == "2"){
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "expandshow","show");
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "expandshow","hide");
        			}
        		}
        		this.gfn_constDsSet(this.gv_detail);
        		this.searchFalg = "";
        	}else if(sSvcId == "save"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "saveDetail"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	}
         }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sType = obj.getCellProperty("head", e.cell, "displaytype");
        	
        	if(sType != "checkbox" && colName != "" && colName != "CHK" && colName != "PAGING_NUM" && this.ds_header.rowcount > 0){
        		this.rowChg = "Y";
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		this.parent.div_cond.btn_search.click();
        	}else if(sType == "checkbox"){
        		this.ds_header.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_header.set_updatecontrol(true);
        	}
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && this.ds_detail.rowcount > 0){
        		this.gfn_sortDataset(obj, e);
        	}else if(colName == "CHK"){
        		this.ds_detail.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_detail.set_updatecontrol(true);
        	}
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("mixShipItemImpossController/excelDownMixShipItemImposs.do", this.gv_header, this, oValue);
        }

        /* div_splitBottom_btn_excel_onclick 실행 */
        this.div_splitBottom_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        		owkey : this.ds_header.getColumn(this.gv_nRow, "owkey")
        		,group_id : this.ds_header.getColumn(this.gv_nRow, "group_id")
        		,group_nm : this.ds_header.getColumn(this.gv_nRow, "group_nm")
        		,group_flag : this.ds_header.getColumn(this.gv_nRow, "group_flag")
                ,delyn : this.ds_header.getColumn(this.gv_nRow, "delyn")
        	}
        	
        	this.parent.div_cond.fn_excelSearch("mixShipItemImpossController/excelDownDetailMixShipItemImposs.do", this.gv_detail, this, oValue);
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	var nChkCnt = this.ds_header.getCaseCount("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");

        // 	}else{
        // 	   //2018.10.16 즉시삭제되도록 변경
        // 		if(nChkCnt == this.ds_header.rowcount){
        // 			var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        // 			var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        // 			var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        // 			
        // 			this.gfn_confirm3Btn (
        // 				arrBtnText,
        // 				sMsgCode,
        // 				sMsgText,
        // 				function(msg, flag) {
        // 					if (flag != "0") {
        // 						if (flag == "1") {
        // 							this.fn_save("");
        // 						}
        // 						if (flag == "2") {
        // 							this.fn_save("ALL");
        // 						}
        // 					}
        // 				}
        // 			);
        		}else{
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					while(nRow != -1){
        						if(this.ds_header.getRowType(nRow) == "2"){
        							this.ds_header.deleteRow(nRow);
        							nRow = this.ds_header.findRowExpr("CHK == '1'");
        						}else{
        							this.ds_header.setColumn(nRow, "STATUS", "D");
        							nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        						}
        					}
        					if(this.ds_header.findRowExpr("STATUS == 'D'") > -1)  this.fn_save("");
        				}
        			});
        		}
        		
        	//}
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "owkey|group_nm|group_flag";
        	
        	if(this.gfn_isUpdate(this.ds_header)){		
        		if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_save("");
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	this.ds_detail.clearData();
        	
        	var nRow = this.ds_header.addRow();
        	
        	this.ds_header.setColumn(nRow, "PAGING_NUM", nRow+1);
        	this.ds_header.setColumn(nRow, "STATUS", "C");
        	this.ds_header.setColumn(nRow, "CHK", "1");
        	this.ds_header.setColumn(nRow, "delyn", "N");
        	this.ds_header.set_rowposition(nRow);
        	this.gv_header.setFocus();
        	//var owkey = this.gfn_getUserInfo("owkeym").split(",");
            //this.ds_header.setColumn(nRow, "owkey", owkey[0]);
           
           
        // 	var key = "";
        // 	var value = "";
        // 		
        // 	if(this.gfn_isNotNull(owkey.value)){
        // 		this.gv_code = "Y";
        // 		this.gfn_codeSearch(key, value, owkey.value, "50", application.gv_ams, "", "", this.owkey, this.owname);
        // 	}
        	
        }

        /* div_splitBottom_btn_add_onclick 실행 */
        this.div_splitBottom_btn_add_onclick = function(obj,e)
        {
        	var group_id = this.ds_header.getColumn(this.ds_header.rowposition, "group_id");
        	var group_nm = this.ds_header.getColumn(this.ds_header.rowposition, "group_nm");
        	var group_flag = this.ds_header.getColumn(this.ds_header.rowposition, "group_flag");
        	
        	var owkey = this.ds_header.getColumn(this.ds_header.rowposition, "owkey")
        	
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "delyn") == "Y"){
        		this.gfn_alert("MSG_20098");
        		return;		
        	}		
        		
        	if(this.gfn_isNotNull(group_id)){
        		
        			var nRow = this.ds_detail.addRow();
        			this.ds_detail.setColumn(nRow, "CHK", 1);
        			this.ds_detail.setColumn(nRow, "STATUS", "C");
        			this.ds_detail.setColumn(nRow, "group_id", group_id);
        			this.ds_detail.setColumn(nRow, "group_nm", group_nm);
        			this.ds_detail.setColumn(nRow, "group_flag", group_flag);
        			this.ds_detail.setColumn(nRow, 'owkey', owkey);
        			this.gv_detail.setFocus();
        			
        			//그룹구분 : 1은 상품그룹, 2은 상품
        			//그룹구분에 따른  expandshow 여부
        			if(group_flag == "1"){
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey") , "expandshow","show");
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "expandshow","hide");
        				//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "none");
        			}else if(group_flag == "2"){
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "expandshow","hide");
        				this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "expandshow","show");
        				//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "combo");
        			}
        			
        	}else {
        		this.gfn_alert("MSG_80110");
        	}
        }

        this.div_splitBottom_grd_detail_onexpanddown = function(obj,e)
        {
        	//그룹구분이 상품그룹(1)인 경우에는 상품그룹코드 팝업을 선택이 가능하며 선택하면 상품그룹유형/상품그룹코드/상품그룹명   
        	//그룹구분이 상품(2)인 경우에는 상품코드 팝업 선택이 가능하며 선택하면 상품코드/상품명   
        	if(this.ds_header.getColumn(this.ds_header.rowposition, "group_flag") == "1"){
        		
        		if(obj.getBindCellIndex("body", "icgrkey") == e.col){
        			var oArg = { divId:"GridMulti"
        					,searchId:90
        					,putObj:this.ds_detail
        					,putCol:"icgrkey|icgrname|icgrtype"
        					,putValue : this.ds_detail.getColumn(this.ds_detail.rowposition, "owkey")
        					,putKey : "owkey"
        					,putRow:""+e.row
        				   };
        			this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        		}
        	
        	}else {
        	
        		if(obj.getBindCellIndex("body", "ickey") == e.col){
        			var oArg = { divId:"GridMulti"
        						,searchId:110
        						,putObj:this.ds_detail
        						,putCol:"ickey|icname"
        						,putValue : this.ds_detail.getColumn(this.ds_detail.rowposition, "owkey")
        						,putKey : "owkey"
        						,putRow:""+e.row
        					   };
        			this.gfn_popup("CodeSearchMultiIcPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        		}
        		
        	} // IF end
        	
        	
        }
        // this.fn_popupAfterIckey = function (sPopupId, objRtn)
        // {
        // 	if(this.gv_loggrpcd != objRtn[7]) {
        // 		this.edt_ickey.set_value('');
        // 		this.gfn_alert('MSG_CANNOT_ICKEY');
        // 		return;
        // 	}

        /* div_splitBottom_btn_delete_onclick 실행 */
        this.div_splitBottom_btn_delete_onclick = function(obj,e)
        {
        	
        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        	    //2018.10.16 즉시삭제되도록 변경
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){				
        					while(nRow != -1){
        						if(this.ds_detail.getRowType(nRow) == "2"){
        							this.ds_detail.deleteRow(nRow);
        							nRow = this.ds_detail.findRowExpr("CHK == '1'");
        						}else{
        							this.ds_detail.setColumn(nRow, "STATUS", "D");
        							nRow = this.ds_detail.findRowExpr("CHK == '1'", nRow+1);
        						}
        					}

        					if(this.ds_detail.findRowExpr("STATUS == 'D'") > -1) this.fn_saveDetail();
        				}
        			});
        	}
        }

        /* div_splitBottom_btn_save_onclick 실행 */
        this.div_splitBottom_btn_save_onclick = function(obj,e)
        {
        	//var dsCol = "ussc_order|ussc_dttype|ussc_label|dbfieldname|defaultoperator";
        	var group_flag = this.ds_header.getColumn(this.ds_header.rowposition, "group_flag");
        	var icgrkey = this.ds_detail.getColumn(this.ds_detail.rowposition, "icgrkey");
        	var ickey = this.ds_detail.getColumn(this.ds_detail.rowposition, "ickey")
        	var loggrpcd = this.ds_detail.getColumn(this.ds_detail.rowposition, "loggrpcd")
        	
        	if(this.gfn_isNotNull(icgrkey)|| this.gfn_isNotNull(ickey)){  
        		
        		if(this.gfn_isNotNull(ickey) && this.gfn_isNull(loggrpcd) ){
        			this.gfn_alert("MSG_30714");
        			return;
        		}
        	
        		if(this.gfn_isUpdate(this.ds_detail)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_saveDetail();
        				}
        			});
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        		
        	}else {
        		this.gfn_alert("상품그룹/상품 코드를 선택해주세요.");
        	}
        }

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(this.ds_header.getColumn(e.newrow, 'group_flag') == '2') {
        					this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'displaytype', 'number');
        					this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'mask', '#,##0.00');
        				} else {
        					this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'displaytype', 'normal');
        					this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'mask', '');
        				}
        			
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			if(this.ds_header.getColumn(e.newrow, 'group_flag') == '2') {
        				this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'displaytype', 'number');
        				this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'mask', '#,##0.00');
        			} else {
        				this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'displaytype', 'normal');
        				this.gv_detail.setCellProperty('Body',this.gv_detail.getBindCellIndex('body','udf3'), 'mask', '');
        			}
        			this.fn_searchDetail();
        		}
        	}
        	
        	this.rowChg = "";
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1 && obj.getRowType(e.row) != 2){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        		
        		if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        /* ds_detail_cancolumnchange 실행 */
        this.ds_detail_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        	
        	var group_flag = this.ds_header.getColumn(this.ds_header.rowposition, "group_flag");
        			//그룹구분 : 1은 상품그룹, 2은 상품
        			//그룹구분에 따른  expandshow 여부
        		if(group_flag == "1"){
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "none");
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "background", this.gv_bgBodyColor);
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "background2", this.gv_bg1BodyColor );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "selectbackground", this.gv_bgSelBodyColor);	
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background", "" );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background2", "" );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "selectbackground", "");	
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background", "" );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background2", "" );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "selectbackground", "");	
        	
        		}else if(group_flag == "2"){
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "combo");
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "background", "" );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "background2", "" );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "icgrkey"), "selectbackground", "");
        		//	this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background", this.gv_bgBodyColor);
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background2", this.gv_bg1BodyColor );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "selectbackground", this.gv_bgSelBodyColor);	
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background", this.gv_bgBodyColor);
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background2", this.gv_bg1BodyColor );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "selectbackground", this.gv_bgSelBodyColor);			
        			}
        	
        	
        }

        /* ds_detail_oncolumnchanged 실행 */
        this.ds_detail_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK") obj.set_updatecontrol(true);
        	else if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        	
        	var group_flag = this.ds_header.getColumn(this.ds_header.rowposition, "group_flag");
        			//그룹구분 : 1은 상품그룹, 2은 상품
        		if(group_flag == "1"){
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "none");
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "background", this.gv_bgBodyColor);
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "background2", this.gv_bg1BodyColor );
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "selectbackground", this.gv_bgSelBodyColor);	
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background", "" );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background2", "" );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "selectbackground", "");	
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "background", "" );
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background2", "" );
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "selectbackground", "");	
        		
        			
        		}else if(group_flag == "2"){
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "edittype", "combo");
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "background", "" );
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "background2", "" );
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "icgrkey"), "selectbackground", "");
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background", this.gv_bgBodyColor);
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "background2", this.gv_bg1BodyColor );
        			//this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "loggrpcd"), "selectbackground", this.gv_bgSelBodyColor);	
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "background", this.gv_bgBodyColor);
        			this.gv_detail.setCellProperty("body", this.gv_detail.getBindCellIndex("body", "ickey"), "background2", this.gv_bg1BodyColor );
        			this.gv_detail.setCellProperty("body",this.gv_detail.getBindCellIndex("body", "ickey"), "selectbackground", this.gv_bgSelBodyColor);			
        	
        					
        			}
        			
        		if(e.columnid == "ickey"){
        			this.ds_detail.setColumn(this.ds_detail.rowposition,"loggrpcd",this.gv_Pvalue[7]);
        			this.gv_Pvalue = "";
        		}
        	
        }

        /* div_splitBottom_grd_detail_oncellclick 실행 */
        this.div_splitBottom_grd_detail_oncellclick = function(obj,e)
        {
        	// 한 번 클릭으로 동작
        	obj.dropdownCombo();
        	obj.dropdownCalendar();
        }

        /* div_splitBottom_grd_detail_oncelldblclick 실행 */
        this.div_splitBottom_grd_detail_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        this.div_splitTop_grd_header_onexpanddown = function(obj,e)
        {
        	if(obj.getBindCellIndex("body", "owkey") == e.col){
        		var oArg = { divId:"GridMulti"
        					,searchId:50
        					,putObj:this.ds_header
        					,putCol:"owkey|owname"
        					,putValue : ""
        					,putRow:""+e.row
        				   };
        		this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        	}

        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_detail.addEventHandler("cancolumnchange", this.ds_detail_cancolumnchange, this);
            this.ds_detail.addEventHandler("oncolumnchanged", this.ds_detail_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.grd_header.addEventHandler("onexpanddown", this.div_splitTop_grd_header_onexpanddown, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.div_splitBottom_grd_detail_oncelldblclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onexpanddown", this.div_splitBottom_grd_detail_onexpanddown, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.btn_add.addEventHandler("onclick", this.div_splitBottom_btn_add_onclick, this);
            this.div_splitBottom.btn_delete.addEventHandler("onclick", this.div_splitBottom_btn_delete_onclick, this);
            this.div_splitBottom.btn_excel.addEventHandler("onclick", this.div_splitBottom_btn_excel_onclick, this);
            this.div_splitBottom.btn_save.addEventHandler("onclick", this.div_splitBottom_btn_save_onclick, this);
            this.Static05.addEventHandler("onclick", this.Static05_onclick, this);

        };

        this.loadIncludeScript("MixShipItemImposs.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
