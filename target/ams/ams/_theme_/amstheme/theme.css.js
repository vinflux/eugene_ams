﻿//CSS=theme.css
    
(function() {
  return function() {
    var obj;    
    
    obj = new nexacro.Style_accessibility("","true","all","","","");
    this._addCss("MainFrame", "accessibility", obj, ["normal"]);
    this._addCss("ChildFrame", "accessibility", obj, ["normal"]);
    this._addCss("FrameSet", "accessibility", obj, ["normal"]);
    this._addCss("HFrameSet", "accessibility", obj, ["normal"]);
    this._addCss("VFrameSet", "accessibility", obj, ["normal"]);
    this._addCss("TileFrameSet", "accessibility", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "accessibility", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "accessibility", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "accessibility", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "accessibility", obj, ["normal"]);
    this._addCss("StatusBarControl", "accessibility", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "accessibility", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "accessibility", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "accessibility", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "accessibility", obj, ["normal"]);
    this._addCss("VScrollBarControl", "accessibility", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "accessibility", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "accessibility", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "accessibility", obj, ["normal"]);
    this._addCss("HScrollBarControl", "accessibility", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "accessibility", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "accessibility", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "accessibility", obj, ["normal"]);
    this._addCss("*>#resizebutton", "accessibility", obj, ["normal"]);
    this._addCss("Form", "accessibility", obj, ["normal"]);
    this._addCss("Combo", "accessibility", obj, ["normal"]);
    this._addCss("Calendar", "accessibility", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "accessibility", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "accessibility", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "accessibility", obj, ["normal"]);
    this._addCss("CheckBox", "accessibility", obj, ["normal"]);
    this._addCss("Div", "accessibility", obj, ["normal"]);
    this._addCss("Edit", "accessibility", obj, ["normal"]);
    this._addCss("Grid", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#head", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#body", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#summ", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#summary", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controledit", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "accessibility", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "accessibility", obj, ["normal"]);
    this._addCss("GroupBox", "accessibility", obj, ["normal"]);
    this._addCss("ListBox", "accessibility", obj, ["normal"]);
    this._addCss("MaskEdit", "accessibility", obj, ["normal"]);
    this._addCss("ImageViewer", "accessibility", obj, ["normal"]);
    this._addCss("Menu", "accessibility", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "accessibility", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "accessibility", obj, ["normal"]);
    this._addCss("PopupDiv", "accessibility", obj, ["normal"]);
    this._addCss("PopupMenu", "accessibility", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "accessibility", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "accessibility", obj, ["normal"]);
    this._addCss("ProgressBar", "accessibility", obj, ["normal"]);
    this._addCss("ProgressBarControl", "accessibility", obj, ["normal"]);
    this._addCss("Radio", "accessibility", obj, ["normal"]);
    this._addCss("Static", "accessibility", obj, ["normal"]);
    this._addCss("Spin", "accessibility", obj, ["normal"]);
    this._addCss("Tab", "accessibility", obj, ["normal", "mouseover"]);
    this._addCss("Tab>#spindownbutton", "accessibility", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "accessibility", obj, ["normal"]);
    this._addCss("Tabpage", "accessibility", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "accessibility", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "accessibility", obj, ["normal"]);
    this._addCss("TextArea", "accessibility", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "accessibility", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("MainFrame", "background", obj, ["normal"]);
    this._addCss("ChildFrame", "background", obj, ["normal"]);
    this._addCss("FrameSet", "background", obj, ["normal"]);
    this._addCss("HFrameSet", "background", obj, ["normal"]);
    this._addCss("VFrameSet", "background", obj, ["normal"]);
    this._addCss("TileFrameSet", "background", obj, ["normal"]);
    this._addCss("TitleBarControl", "background", obj, ["normal"]);
    this._addCss("Form", "background", obj, ["normal"]);
    this._addCss("Button", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_TaF_tab", "background", obj, ["selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "background", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_page", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_P_close", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Combo", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Combo>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo>#dropbutton", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_WF_search>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Combo.cmb_Lgn_leg", "background", obj, ["normal", "readonly"]);
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "background", obj, ["normal", "readonly"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar", "background", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar>#calendaredit", "background", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "background", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Calendar>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Calendar.ptl_dash", "background", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "background", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "background", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.cal_dash>#spinupbutton", "background", obj, ["pushed", "selected"]);
    this._addCss("Calendar.ptl_dash>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Edit", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_pop_con", "background", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Grid", "background", obj, ["normal"]);
    this._addCss("Grid>#controledit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlmaskedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controltextarea", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "background", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "background", obj, ["mouseover"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "background", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "background", obj, ["normal"]);
    this._addCss("Grid.grd_Lgn_bbs", "background", obj, ["normal"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("ListBox", "background", obj, ["normal"]);
    this._addCss("MaskEdit", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Menu.meu_WF_btn", "background", obj, ["normal", "focused", "selected", "pushed"]);
    this._addCss("Spin", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Spin>#spinedit", "background", obj, ["normal", "focused", "mouseover"]);
    this._addCss("Spin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Spin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("SpinControl", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("SpinControl>#spinedit", "background", obj, ["normal", "focused", "mouseover", "disabled"]);
    this._addCss("Tab", "background", obj, ["normal"]);
    this._addCss("Tabpage", "background", obj, ["normal"]);
    this._addCss("TextArea", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("TextArea.txt_WF_Alert", "background", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_border("2","solid","#302c2cff","");
    this._addCss("MainFrame", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("MainFrame", "color", obj, ["normal"]);
    this._addCss("Button.btn_WF_SrhChange", "color", obj, ["mouseover", "pushed"]);
    this._addCss("Button.btn_WF_srh_s", "color", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_Lgn_stext", "color", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Calendar>#popupcalendar", "color", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "color", obj, ["normal"]);
    this._addCss("Edit.edt_pop_con", "color", obj, ["normal", "mouseover"]);
    this._addCss("Grid", "color", obj, ["normal"]);
    this._addCss("Grid>#controledit", "color", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "color", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "color", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "color", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "color", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "color", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "color", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#controlcheckbox", "color", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "color", obj, ["normal"]);
    this._addCss("ProgressBar", "color", obj, ["normal"]);
    this._addCss("ProgressBarControl", "color", obj, ["normal"]);
    this._addCss("Static", "color", obj, ["normal"]);
    this._addCss("Static.sta_stext", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_stext", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_title", "color", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "color", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinedit", "color", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("MainFrame", "font", obj, ["normal"]);
    this._addCss("Form", "font", obj, ["normal"]);
    this._addCss("Button", "font", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_text", "font", obj, ["normal", "disabled"]);
    this._addCss("Combo", "font", obj, ["normal"]);
    this._addCss("Calendar", "font", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "font", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "font", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "font", obj, ["normal"]);
    this._addCss("CheckBox", "font", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "font", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "font", obj, ["normal"]);
    this._addCss("Edit", "font", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "font", obj, ["normal"]);
    this._addCss("Edit.edt_pop_con", "font", obj, ["normal", "mouseover"]);
    this._addCss("Edit.ptl_edt_TF_search", "font", obj, ["normal"]);
    this._addCss("Grid", "font", obj, ["normal"]);
    this._addCss("Grid>#controledit", "font", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "font", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "font", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "font", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "font", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh", "font", obj, ["normal"]);
    this._addCss("Grid.grd_Lgn_bbs", "font", obj, ["normal"]);
    this._addCss("GroupBox", "font", obj, ["normal"]);
    this._addCss("ListBox", "font", obj, ["normal"]);
    this._addCss("MaskEdit", "font", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "font", obj, ["normal", "mouseover", "focused", "selected", "pushed", "disabled"]);
    this._addCss("ProgressBar", "font", obj, ["normal"]);
    this._addCss("ProgressBarControl", "font", obj, ["normal"]);
    this._addCss("Radio", "font", obj, ["normal"]);
    this._addCss("Static", "font", obj, ["normal", "disabled"]);
    this._addCss("Static.sta_WF_navi", "font", obj, ["normal"]);
    this._addCss("Static.sta_TF_welcome", "font", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bbs", "font", obj, ["normal", "disabled"]);
    this._addCss("Spin", "font", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "font", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinedit", "font", obj, ["normal", "focused", "mouseover"]);
    this._addCss("TextArea", "font", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "font", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("MainFrame", "gradation", obj, ["normal"]);
    this._addCss("ChildFrame", "gradation", obj, ["normal"]);
    this._addCss("FrameSet", "gradation", obj, ["normal"]);
    this._addCss("HFrameSet", "gradation", obj, ["normal"]);
    this._addCss("VFrameSet", "gradation", obj, ["normal"]);
    this._addCss("TileFrameSet", "gradation", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "gradation", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "gradation", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "gradation", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "gradation", obj, ["normal"]);
    this._addCss("StatusBarControl", "gradation", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "gradation", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "gradation", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "gradation", obj, ["normal"]);
    this._addCss("VScrollBarControl", "gradation", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "gradation", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "gradation", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "gradation", obj, ["normal"]);
    this._addCss("HScrollBarControl", "gradation", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "gradation", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "gradation", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "gradation", obj, ["normal"]);
    this._addCss("*>#resizebutton", "gradation", obj, ["normal"]);
    this._addCss("Form", "gradation", obj, ["normal"]);
    this._addCss("Combo", "gradation", obj, ["normal"]);
    this._addCss("Calendar", "gradation", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "gradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "gradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "gradation", obj, ["normal"]);
    this._addCss("CheckBox", "gradation", obj, ["normal"]);
    this._addCss("Edit", "gradation", obj, ["normal"]);
    this._addCss("FileUpload", "gradation", obj, ["normal"]);
    this._addCss("Grid", "gradation", obj, ["normal"]);
    this._addCss("Grid>#head", "gradation", obj, ["normal"]);
    this._addCss("Grid>#body", "gradation", obj, ["normal"]);
    this._addCss("Grid>#summ", "gradation", obj, ["normal"]);
    this._addCss("Grid>#summary", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controledit", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "gradation", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "gradation", obj, ["normal"]);
    this._addCss("GroupBox", "gradation", obj, ["normal"]);
    this._addCss("ListBox", "gradation", obj, ["normal"]);
    this._addCss("MaskEdit", "gradation", obj, ["normal"]);
    this._addCss("ImageViewer", "gradation", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "gradation", obj, ["normal"]);
    this._addCss("PopupMenu", "gradation", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "gradation", obj, ["normal"]);
    this._addCss("ProgressBar", "gradation", obj, ["normal"]);
    this._addCss("ProgressBarControl", "gradation", obj, ["normal"]);
    this._addCss("Radio", "gradation", obj, ["normal"]);
    this._addCss("Static", "gradation", obj, ["normal"]);
    this._addCss("StepControl", "gradation", obj, ["normal"]);
    this._addCss("Spin", "gradation", obj, ["normal"]);
    this._addCss("Tab", "gradation", obj, ["normal", "mouseover"]);
    this._addCss("Tab>#spindownbutton", "gradation", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "gradation", obj, ["normal"]);
    this._addCss("Tabpage", "gradation", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "gradation", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "gradation", obj, ["normal"]);
    this._addCss("TextArea", "gradation", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("MainFrame", "menubarheight", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("MainFrame", "opacity", obj, ["normal"]);
    this._addCss("ChildFrame", "opacity", obj, ["normal"]);
    this._addCss("FrameSet", "opacity", obj, ["normal"]);
    this._addCss("HFrameSet", "opacity", obj, ["normal"]);
    this._addCss("VFrameSet", "opacity", obj, ["normal"]);
    this._addCss("TileFrameSet", "opacity", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "opacity", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "opacity", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "opacity", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "opacity", obj, ["normal"]);
    this._addCss("StatusBarControl", "opacity", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "opacity", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "opacity", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "opacity", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "opacity", obj, ["normal"]);
    this._addCss("VScrollBarControl", "opacity", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "opacity", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "opacity", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "opacity", obj, ["normal"]);
    this._addCss("HScrollBarControl", "opacity", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "opacity", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "opacity", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "opacity", obj, ["normal"]);
    this._addCss("*>#resizebutton", "opacity", obj, ["normal"]);
    this._addCss("Form", "opacity", obj, ["normal"]);
    this._addCss("Combo", "opacity", obj, ["normal"]);
    this._addCss("Calendar", "opacity", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "opacity", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "opacity", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "opacity", obj, ["normal"]);
    this._addCss("CheckBox", "opacity", obj, ["normal"]);
    this._addCss("Div", "opacity", obj, ["normal"]);
    this._addCss("Edit", "opacity", obj, ["normal"]);
    this._addCss("FileDownload", "opacity", obj, ["normal"]);
    this._addCss("FileUpload", "opacity", obj, ["normal"]);
    this._addCss("Grid", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controledit", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "opacity", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "opacity", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "opacity", obj, ["normal"]);
    this._addCss("GroupBox", "opacity", obj, ["normal"]);
    this._addCss("ListBox", "opacity", obj, ["normal"]);
    this._addCss("MaskEdit", "opacity", obj, ["normal"]);
    this._addCss("ImageViewer", "opacity", obj, ["normal"]);
    this._addCss("Menu", "opacity", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "opacity", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "opacity", obj, ["normal"]);
    this._addCss("PopupDiv", "opacity", obj, ["normal"]);
    this._addCss("PopupMenu", "opacity", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "opacity", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "opacity", obj, ["normal"]);
    this._addCss("ProgressBar", "opacity", obj, ["normal"]);
    this._addCss("ProgressBarControl", "opacity", obj, ["normal"]);
    this._addCss("Radio", "opacity", obj, ["normal"]);
    this._addCss("Static", "opacity", obj, ["normal"]);
    this._addCss("StepControl", "opacity", obj, ["normal"]);
    this._addCss("Spin", "opacity", obj, ["normal"]);
    this._addCss("Tab", "opacity", obj, ["normal", "mouseover"]);
    this._addCss("Tab>#spindownbutton", "opacity", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "opacity", obj, ["normal"]);
    this._addCss("Tabpage", "opacity", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "opacity", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "opacity", obj, ["normal"]);
    this._addCss("TextArea", "opacity", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("MainFrame", "openstatuseffect", obj, ["normal"]);
    this._addCss("ChildFrame", "openstatuseffect", obj, ["normal"]);
    this._addCss("FrameSet", "openstatuseffect", obj, ["normal"]);
    this._addCss("HFrameSet", "openstatuseffect", obj, ["normal"]);
    this._addCss("VFrameSet", "openstatuseffect", obj, ["normal"]);
    this._addCss("TileFrameSet", "openstatuseffect", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("MainFrame", "statusbarheight", obj, ["normal"]);
    this._addCss("ChildFrame", "statusbarheight", obj, ["normal"]);
    this._addCss("FrameSet", "statusbarheight", obj, ["normal"]);
    this._addCss("HFrameSet", "statusbarheight", obj, ["normal"]);
    this._addCss("VFrameSet", "statusbarheight", obj, ["normal"]);
    this._addCss("TileFrameSet", "statusbarheight", obj, ["normal"]);

    obj = new nexacro.Style_value("28");
    this._addCss("MainFrame", "titlebarheight", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#235798ff","");
    this._addCss("MainFrame", "border", obj, ["deactivate"]);

    obj = new nexacro.Style_align("");
    this._addCss("ChildFrame", "align", obj, ["normal"]);
    this._addCss("FrameSet", "align", obj, ["normal"]);
    this._addCss("HFrameSet", "align", obj, ["normal"]);
    this._addCss("VFrameSet", "align", obj, ["normal"]);
    this._addCss("TileFrameSet", "align", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "align", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "align", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "align", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "align", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "align", obj, ["normal"]);
    this._addCss("VScrollBarControl", "align", obj, ["normal"]);
    this._addCss("HScrollBarControl", "align", obj, ["normal"]);
    this._addCss("*>#resizebutton", "align", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "align", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "align", obj, ["normal"]);
    this._addCss("CheckBox", "align", obj, ["normal"]);
    this._addCss("Div", "align", obj, ["normal"]);
    this._addCss("Grid", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "align", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "align", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "align", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "align", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "align", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "align", obj, ["normal"]);
    this._addCss("ImageViewer", "align", obj, ["normal"]);
    this._addCss("PopupDiv", "align", obj, ["normal"]);
    this._addCss("ProgressBar", "align", obj, ["normal"]);
    this._addCss("ProgressBarControl", "align", obj, ["normal"]);
    this._addCss("Spin", "align", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "align", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "align", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "align", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "align", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("ChildFrame", "border", obj, ["normal"]);
    this._addCss("FrameSet", "border", obj, ["normal"]);
    this._addCss("HFrameSet", "border", obj, ["normal"]);
    this._addCss("VFrameSet", "border", obj, ["normal"]);
    this._addCss("TileFrameSet", "border", obj, ["normal"]);
    this._addCss("TitleBarControl", "border", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "border", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "border", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "border", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "border", obj, ["normal"]);
    this._addCss("VScrollBarControl", "border", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "border", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "border", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "border", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "border", obj, ["normal"]);
    this._addCss("*>#resizebutton", "border", obj, ["normal"]);
    this._addCss("Form", "border", obj, ["normal"]);
    this._addCss("Button", "border", obj, ["mouseover", "pushed"]);
    this._addCss("Button.btn_TF_logout", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_set", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_infor", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_skin", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_setup", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_search", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_home", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TaF_tab_close", "border", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_LF_hide", "border", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_show", "border", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_Mshow", "border", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_up", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_down", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_SHide", "border", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_search", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_SrhShow", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_SrhChange", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_mymenu_add", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_about", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_page", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sl_open", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_sl_close", "border", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_open", "border", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_close", "border", obj, ["pushed", "normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_WF_SHide", "border", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide2", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_Lgn_ok", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_text", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_stext", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_P_close", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.ptl_btn_TF_logout", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_set", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_infor", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_Lgn_ok", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Combo.cmb_TF", "border", obj, ["normal", "mouseover", "focused", "selected", "disabled", "readonly"]);
    this._addCss("Combo.cmb_TF>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF>#dropbutton", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search", "border", obj, ["normal", "mouseover", "focused", "selected", "disabled", "readonly"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#dropbutton", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_Lgn_leg", "border", obj, ["readonly"]);
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "border", obj, ["normal", "focused"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "border", obj, ["normal", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Calendar>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "border", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("CheckBox", "border", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "border", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "border", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "border", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "border", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_pop_con", "border", obj, ["normal", "mouseover", "selected"]);
    this._addCss("FileUpload", "border", obj, ["normal"]);
    this._addCss("Grid>#summ", "border", obj, ["normal"]);
    this._addCss("Grid>#summary", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "border", obj, ["normal"]);
    this._addCss("Grid>#controlexpand", "border", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "border", obj, ["normal"]);
    this._addCss("Grid>#resizebutton", "border", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "border", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid.grd_Lgn_bbs", "border", obj, ["normal"]);
    this._addCss("ImageViewer", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Menu", "border", obj, ["normal"]);
    this._addCss("Radio", "border", obj, ["normal"]);
    this._addCss("Static", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_duty", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_user", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_name", "border", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Static.sta_TF_titleline", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_logo", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_welcome", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_line", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_mnubg", "border", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bbs", "border", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Static.sta_Lgn_date", "border", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_new", "border", obj, ["normal"]);
    this._addCss("Static.ptl_sta_WF_myinfo", "border", obj, ["normal"]);
    this._addCss("StepControl", "border", obj, ["normal"]);
    this._addCss("Spin>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Tab>#extrabutton", "border", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Tab.tab_MDI", "border", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#extrabutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("");
    this._addCss("ChildFrame", "color", obj, ["normal"]);
    this._addCss("FrameSet", "color", obj, ["normal"]);
    this._addCss("HFrameSet", "color", obj, ["normal"]);
    this._addCss("VFrameSet", "color", obj, ["normal"]);
    this._addCss("TileFrameSet", "color", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "color", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "color", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "color", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "color", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "color", obj, ["normal"]);
    this._addCss("VScrollBarControl", "color", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "color", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "color", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "color", obj, ["normal"]);
    this._addCss("HScrollBarControl", "color", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "color", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "color", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "color", obj, ["normal"]);
    this._addCss("*>#resizebutton", "color", obj, ["normal"]);
    this._addCss("Div", "color", obj, ["normal"]);
    this._addCss("FileDownload", "color", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "color", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "color", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "color", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "color", obj, ["normal"]);
    this._addCss("ImageViewer", "color", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "color", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "color", obj, ["normal"]);
    this._addCss("PopupDiv", "color", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "color", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "color", obj, ["normal"]);
    this._addCss("Tab", "color", obj, ["mouseover"]);
    this._addCss("Tab>#spindownbutton", "color", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "color", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "color", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("ChildFrame", "font", obj, ["normal"]);
    this._addCss("FrameSet", "font", obj, ["normal"]);
    this._addCss("HFrameSet", "font", obj, ["normal"]);
    this._addCss("VFrameSet", "font", obj, ["normal"]);
    this._addCss("TileFrameSet", "font", obj, ["normal"]);
    this._addCss("TitleBarControl>#minbutton", "font", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "font", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "font", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "font", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "font", obj, ["normal"]);
    this._addCss("VScrollBarControl", "font", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "font", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "font", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "font", obj, ["normal"]);
    this._addCss("HScrollBarControl", "font", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "font", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "font", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "font", obj, ["normal"]);
    this._addCss("*>#resizebutton", "font", obj, ["normal"]);
    this._addCss("Div", "font", obj, ["normal"]);
    this._addCss("FileDownload", "font", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "font", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "font", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "font", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "font", obj, ["normal"]);
    this._addCss("ImageViewer", "font", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "font", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "font", obj, ["normal"]);
    this._addCss("PopupDiv", "font", obj, ["normal"]);
    this._addCss("PopupMenu", "font", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "font", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "font", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "font", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "font", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "font", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "font", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("ChildFrame", "titlebarheight", obj, ["normal"]);
    this._addCss("FrameSet", "titlebarheight", obj, ["normal"]);
    this._addCss("HFrameSet", "titlebarheight", obj, ["normal"]);
    this._addCss("VFrameSet", "titlebarheight", obj, ["normal"]);
    this._addCss("TileFrameSet", "titlebarheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 30");
    this._addCss("TitleBarControl", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("TitleBarControl>#minbutton", "background", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "background", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "background", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "background", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "background", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "background", obj, ["normal"]);
    this._addCss("Button.btn_TF_logout", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TF_set", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TF_infor", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TF_skin", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TF_next", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TF_prev", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TF_home", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TaF_tab_close", "background", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_next", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TaF_prev", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TaF_close", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_LF_hide", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_LF_show", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_LF_up", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_down", "background", obj, ["normal"]);
    this._addCss("Button.btn_WF_SrhShow", "background", obj, ["disabled"]);
    this._addCss("Button.btn_Lgn_text", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_stext", "background", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.ptl_btn_TF_logout", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_set", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_infor", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Combo.cmb_TF>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Combo.cmb_TF_search>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "background", obj, ["normal", "focused", "disabled"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "background", obj, ["normal", "focused", "disabled"]);
    this._addCss("Calendar>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "background", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "background", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "background", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "background", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("CheckBox", "background", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "background", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "background", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "background", obj, ["normal"]);
    this._addCss("FileUpload", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "background", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "background", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "background", obj, ["normal"]);
    this._addCss("ImageViewer", "background", obj, ["normal"]);
    this._addCss("PopupMenu", "background", obj, ["normal", "mouseover"]);
    this._addCss("Radio", "background", obj, ["normal"]);
    this._addCss("Static", "background", obj, ["normal"]);
    this._addCss("Static.sta_TF_user", "background", obj, ["normal"]);
    this._addCss("Static.sta_TF_name", "background", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Static.sta_TF_welcome", "background", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bbs", "background", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Static.sta_Lgn_date", "background", obj, ["normal"]);
    this._addCss("StepControl", "background", obj, ["normal"]);
    this._addCss("SpinControl>#spinupbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("SpinControl>#spindownbutton", "background", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Tab>#extrabutton", "background", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "background", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "background", obj, ["normal"]);
    this._addCss("Tab.tab_MDI", "background", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#extrabutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("TitleBarControl>#minbutton", "bordertype", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "bordertype", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "bordertype", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "bordertype", obj, ["normal"]);
    this._addCss("VScrollBarControl", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl", "bordertype", obj, ["normal"]);
    this._addCss("*>#resizebutton", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_TF_setup", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("FileUpload", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlexpand", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#resizebutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "bordertype", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Menu>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("PopupMenu", "bordertype", obj, ["normal"]);
    this._addCss("StepControl", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("TitleBarControl>#minbutton", "cursor", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "cursor", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "cursor", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "cursor", obj, ["normal"]);
    this._addCss("StatusBarControl", "cursor", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "cursor", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "cursor", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "cursor", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "cursor", obj, ["normal"]);
    this._addCss("VScrollBarControl", "cursor", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "cursor", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "cursor", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "cursor", obj, ["normal"]);
    this._addCss("HScrollBarControl", "cursor", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "cursor", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "cursor", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "cursor", obj, ["normal"]);
    this._addCss("*>#resizebutton", "cursor", obj, ["normal"]);
    this._addCss("Form", "cursor", obj, ["normal"]);
    this._addCss("Combo", "cursor", obj, ["normal"]);
    this._addCss("Calendar", "cursor", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "cursor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "cursor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "cursor", obj, ["normal"]);
    this._addCss("CheckBox", "cursor", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "cursor", obj, ["normal"]);
    this._addCss("Div", "cursor", obj, ["normal"]);
    this._addCss("Edit", "cursor", obj, ["normal"]);
    this._addCss("FileDownload", "cursor", obj, ["normal"]);
    this._addCss("FileUpload", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controledit", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "cursor", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "cursor", obj, ["normal"]);
    this._addCss("GroupBox", "cursor", obj, ["normal"]);
    this._addCss("ListBox", "cursor", obj, ["normal"]);
    this._addCss("MaskEdit", "cursor", obj, ["normal"]);
    this._addCss("ImageViewer", "cursor", obj, ["normal"]);
    this._addCss("Menu", "cursor", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "cursor", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "cursor", obj, ["normal"]);
    this._addCss("PopupDiv", "cursor", obj, ["normal"]);
    this._addCss("PopupMenu", "cursor", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "cursor", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "cursor", obj, ["normal"]);
    this._addCss("ProgressBar", "cursor", obj, ["normal"]);
    this._addCss("ProgressBarControl", "cursor", obj, ["normal"]);
    this._addCss("Radio", "cursor", obj, ["normal"]);
    this._addCss("Static", "cursor", obj, ["normal"]);
    this._addCss("Spin", "cursor", obj, ["normal"]);
    this._addCss("Tab", "cursor", obj, ["mouseover"]);
    this._addCss("Tab>#spindownbutton", "cursor", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "cursor", obj, ["normal"]);
    this._addCss("Tabpage", "cursor", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "cursor", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "cursor", obj, ["normal"]);
    this._addCss("TextArea", "cursor", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMinN.png')");
    this._addCss("TitleBarControl>#minbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_align("");
    this._addCss("TitleBarControl>#minbutton", "imagealign", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "imagealign", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "imagealign", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "imagealign", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "imagealign", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "imagealign", obj, ["normal"]);
    this._addCss("*>#resizebutton", "imagealign", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "imagealign", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "imagealign", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "imagealign", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "imagealign", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "imagealign", obj, ["normal"]);

    obj = new nexacro.Style_padding("");
    this._addCss("TitleBarControl>#minbutton", "padding", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "padding", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "padding", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "padding", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "padding", obj, ["normal"]);
    this._addCss("VScrollBarControl", "padding", obj, ["normal"]);
    this._addCss("HScrollBarControl", "padding", obj, ["normal"]);
    this._addCss("*>#resizebutton", "padding", obj, ["normal"]);
    this._addCss("Form", "padding", obj, ["normal"]);
    this._addCss("Combo", "padding", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "padding", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "padding", obj, ["normal"]);
    this._addCss("CheckBox", "padding", obj, ["normal"]);
    this._addCss("Div", "padding", obj, ["normal"]);
    this._addCss("FileDownload", "padding", obj, ["normal"]);
    this._addCss("FileUpload", "padding", obj, ["normal"]);
    this._addCss("Grid", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "padding", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "padding", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("ListBox", "padding", obj, ["normal"]);
    this._addCss("ImageViewer", "padding", obj, ["normal"]);
    this._addCss("Menu", "padding", obj, ["normal"]);
    this._addCss("PopupDiv", "padding", obj, ["normal"]);
    this._addCss("PopupMenu", "padding", obj, ["normal"]);
    this._addCss("ProgressBar", "padding", obj, ["normal"]);
    this._addCss("ProgressBarControl", "padding", obj, ["normal"]);
    this._addCss("Radio", "padding", obj, ["normal"]);
    this._addCss("Static", "padding", obj, ["normal"]);
    this._addCss("Spin", "padding", obj, ["normal"]);
    this._addCss("Tab", "padding", obj, ["normal", "mouseover"]);
    this._addCss("Tab>#spindownbutton", "padding", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "padding", obj, ["normal"]);
    this._addCss("Tabpage", "padding", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "padding", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "padding", obj, ["normal"]);

    obj = new nexacro.Style_shadow("");
    this._addCss("TitleBarControl>#minbutton", "shadow", obj, ["normal"]);
    this._addCss("TitleBarControl>#maxbutton", "shadow", obj, ["normal"]);
    this._addCss("TitleBarControl>#normalbutton", "shadow", obj, ["normal"]);
    this._addCss("TitleBarControl>#closebutton", "shadow", obj, ["normal"]);
    this._addCss("StatusBarControl", "shadow", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "shadow", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "shadow", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "shadow", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "shadow", obj, ["normal"]);
    this._addCss("VScrollBarControl", "shadow", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "shadow", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "shadow", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "shadow", obj, ["normal"]);
    this._addCss("HScrollBarControl", "shadow", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "shadow", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "shadow", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "shadow", obj, ["normal"]);
    this._addCss("*>#resizebutton", "shadow", obj, ["normal"]);
    this._addCss("Form", "shadow", obj, ["normal"]);
    this._addCss("Combo", "shadow", obj, ["normal"]);
    this._addCss("Calendar", "shadow", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "shadow", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "shadow", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "shadow", obj, ["normal"]);
    this._addCss("CheckBox", "shadow", obj, ["normal"]);
    this._addCss("Div", "shadow", obj, ["normal"]);
    this._addCss("FileDownload", "shadow", obj, ["normal"]);
    this._addCss("FileUpload", "shadow", obj, ["normal"]);
    this._addCss("Grid", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controledit", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "shadow", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "shadow", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "shadow", obj, ["normal"]);
    this._addCss("GroupBox", "shadow", obj, ["normal"]);
    this._addCss("ListBox", "shadow", obj, ["normal"]);
    this._addCss("MaskEdit", "shadow", obj, ["normal"]);
    this._addCss("ImageViewer", "shadow", obj, ["normal"]);
    this._addCss("Menu", "shadow", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "shadow", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "shadow", obj, ["normal"]);
    this._addCss("PopupDiv", "shadow", obj, ["normal"]);
    this._addCss("PopupMenu", "shadow", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "shadow", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "shadow", obj, ["normal"]);
    this._addCss("ProgressBar", "shadow", obj, ["normal"]);
    this._addCss("ProgressBarControl", "shadow", obj, ["normal"]);
    this._addCss("Radio", "shadow", obj, ["normal"]);
    this._addCss("Static", "shadow", obj, ["normal"]);
    this._addCss("StepControl", "shadow", obj, ["normal"]);
    this._addCss("Spin", "shadow", obj, ["normal"]);
    this._addCss("Tab", "shadow", obj, ["normal", "mouseover"]);
    this._addCss("Tab>#spindownbutton", "shadow", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "shadow", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "shadow", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "shadow", obj, ["normal"]);
    this._addCss("TextArea", "shadow", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "shadow", obj, ["normal"]);

    obj = new nexacro.Style_background("#bcbcbcff","","","0","0","0","0","true");
    this._addCss("TitleBarControl>#minbutton", "background", obj, ["mouseover"]);
    this._addCss("TitleBarControl>#maxbutton", "background", obj, ["mouseover"]);
    this._addCss("TitleBarControl>#normalbutton", "background", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMinM.png')");
    this._addCss("TitleBarControl>#minbutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#bcbcbc50","","","0","0","0","0","true");
    this._addCss("TitleBarControl>#minbutton", "background", obj, ["pushed"]);
    this._addCss("TitleBarControl>#maxbutton", "background", obj, ["pushed"]);
    this._addCss("TitleBarControl>#normalbutton", "background", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMinP.png')");
    this._addCss("TitleBarControl>#minbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMinD.png')");
    this._addCss("TitleBarControl>#minbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMaxN.png')");
    this._addCss("TitleBarControl>#maxbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMaxM.png')");
    this._addCss("TitleBarControl>#maxbutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMaxP.png')");
    this._addCss("TitleBarControl>#maxbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlMaxD.png')");
    this._addCss("TitleBarControl>#maxbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlNormalN.png')");
    this._addCss("TitleBarControl>#normalbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlNormalM.png')");
    this._addCss("TitleBarControl>#normalbutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlNormalP.png')");
    this._addCss("TitleBarControl>#normalbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlNormalD.png')");
    this._addCss("TitleBarControl>#normalbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_border("0","none","#808080ff","");
    this._addCss("TitleBarControl>#closebutton", "border", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo>#dropbutton", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_WF_search>#dropbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Calendar>#calendaredit", "border", obj, ["normal"]);
    this._addCss("Calendar>#dropbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "border", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "border", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "border", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "border", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "border", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "border", obj, ["mouseover"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "border", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "border", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "border", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "border", obj, ["normal", "focused", "mouseover"]);
    this._addCss("Spin>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("SpinControl>#spinedit", "border", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinupbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("SpinControl>#spindownbutton", "border", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlXN.png')");
    this._addCss("TitleBarControl>#closebutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_background("#e81123ff","","","0","0","0","0","true");
    this._addCss("TitleBarControl>#closebutton", "background", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlXM.png')");
    this._addCss("TitleBarControl>#closebutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#e8112350","","","0","0","0","0","true");
    this._addCss("TitleBarControl>#closebutton", "background", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlXP.png')");
    this._addCss("TitleBarControl>#closebutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_TT_controlXD.png')");
    this._addCss("TitleBarControl>#closebutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_align("left middle");
    this._addCss("StatusBarControl", "align", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "align", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "align", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "align", obj, ["normal"]);
    this._addCss("Button.btn_TF_setup", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TaF_tab", "align", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "align", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_WF_SrhChange", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.ptl_btn_TF_logout", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_set", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_infor", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Combo", "align", obj, ["normal"]);
    this._addCss("Combo>#combolist", "align", obj, ["normal"]);
    this._addCss("Combo.cmb_TF", "align", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "align", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "align", obj, ["normal"]);
    this._addCss("Combo.cmb_Lgn_leg", "align", obj, ["normal"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "align", obj, ["normal"]);
    this._addCss("Calendar", "align", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "align", obj, ["normal"]);
    this._addCss("Edit", "align", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_Lgn_id", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_Lgn_pw", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_pop_con", "align", obj, ["normal", "mouseover"]);
    this._addCss("Edit.ptl_edt_TF_search", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "align", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("FileUpload", "align", obj, ["normal"]);
    this._addCss("Grid>#controledit", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "align", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "align", obj, ["normal"]);
    this._addCss("ListBox", "align", obj, ["normal"]);
    this._addCss("Radio", "align", obj, ["normal"]);
    this._addCss("Static", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_title", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_duty", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "align", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f2efff","","","0","0","0","0","true");
    this._addCss("StatusBarControl", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#a8a8a2ff","","0","none","","","0","none","","","0","none","","");
    this._addCss("StatusBarControl", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","3","3","false","false","true","true");
    this._addCss("StatusBarControl", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#46463d");
    this._addCss("StatusBarControl", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("9 Dotum");
    this._addCss("StatusBarControl", "font", obj, ["normal"]);
    this._addCss("StatusBarControl>#progressbar", "font", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "font", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "font", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "font", obj, ["normal"]);
    this._addCss("Button.btn_TaF_tab", "font", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_grip.png')");
    this._addCss("StatusBarControl", "gripimage", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 12");
    this._addCss("StatusBarControl", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("5");
    this._addCss("StatusBarControl", "progressbargap", obj, ["normal"]);

    obj = new nexacro.Style_value("21");
    this._addCss("StatusBarControl", "progressbarheight", obj, ["normal"]);

    obj = new nexacro.Style_value("260");
    this._addCss("StatusBarControl", "progressbarwidth", obj, ["normal"]);

    obj = new nexacro.Style_align("");
    this._addCss("StatusBarControl", "stepalign", obj, ["normal"]);
    this._addCss("Form", "stepalign", obj, ["normal"]);
    this._addCss("Tab", "stepalign", obj, ["normal", "mouseover"]);
    this._addCss("Tabpage", "stepalign", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("StatusBarControl", "stepshowtype", obj, ["normal"]);
    this._addCss("Form", "stepshowtype", obj, ["normal"]);
    this._addCss("Tab", "stepshowtype", obj, ["normal", "mouseover"]);
    this._addCss("Tabpage", "stepshowtype", obj, ["normal"]);

    obj = new nexacro.Style_value("10");
    this._addCss("StatusBarControl", "zoomcombogap", obj, ["normal"]);

    obj = new nexacro.Style_value("21");
    this._addCss("StatusBarControl", "zoomcomboheight", obj, ["normal"]);

    obj = new nexacro.Style_value("80");
    this._addCss("StatusBarControl", "zoomcombowidth", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("StatusBarControl>#progressbar", "align", obj, ["normal"]);
    this._addCss("VScrollBarControl>#incbutton", "align", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "align", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "align", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "align", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "align", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "align", obj, ["normal"]);
    this._addCss("Button", "align", obj, ["normal", "focused"]);
    this._addCss("Button.btn_TF_logout", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_set", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_infor", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_skin", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_home", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_WF_search", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_SrhShow", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_srh_s", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_mymenu_add", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_about", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_page", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_save", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_report", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_excel", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_excelup", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_sampledown", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_print", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_add", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_modify", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_delete", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_list", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_sampdown", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_sl_open", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_sl_close", "align", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_open", "align", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_close", "align", obj, ["pushed", "normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_Lgn_ok", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_text", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_Lgn_stext", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_P_close", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.ptl_btn_L_members", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.ptl_btn_Lgn_ok", "align", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("FileDownload", "align", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "align", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "align", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "align", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);
    this._addCss("PopupMenu>#incbutton", "align", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "align", obj, ["normal"]);
    this._addCss("Static.sta_GA_title", "align", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_stext", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_paging", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_sum", "align", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_new", "align", obj, ["normal"]);
    this._addCss("StepControl", "align", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "align", obj, ["normal"]);

    obj = new nexacro.Style_color("#cdcdc8");
    this._addCss("StatusBarControl>#progressbar", "barcolor", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("StatusBarControl>#progressbar", "bargradation", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "bargradation", obj, ["normal"]);
    this._addCss("ProgressBar", "bargradation", obj, ["normal"]);
    this._addCss("ProgressBarControl", "bargradation", obj, ["normal"]);

    obj = new nexacro.Style_value("normal");
    this._addCss("StatusBarControl>#progressbar", "bartype", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#a8a8a2ff","");
    this._addCss("StatusBarControl>#progressbar", "border", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "border", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "border", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("StatusBarControl>#progressbar", "bordertype", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "bordertype", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "bordertype", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "bordertype", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#head", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#summ", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#summary", "bordertype", obj, ["normal"]);
    this._addCss("Tab", "bordertype", obj, ["mouseover"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#444444");
    this._addCss("StatusBarControl>#progressbar", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("forward");
    this._addCss("StatusBarControl>#progressbar", "direction", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "direction", obj, ["normal"]);
    this._addCss("ProgressBar", "direction", obj, ["normal"]);
    this._addCss("ProgressBarControl", "direction", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("StatusBarControl>#progressbar", "endimage", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "endimage", obj, ["normal"]);
    this._addCss("ProgressBar", "endimage", obj, ["normal"]);
    this._addCss("ProgressBarControl", "endimage", obj, ["normal"]);

    obj = new nexacro.Style_padding("1 1 1 1");
    this._addCss("StatusBarControl>#progressbar", "padding", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("StatusBarControl>#progressbar", "progressimage", obj, ["normal"]);

    obj = new nexacro.Style_value("true");
    this._addCss("StatusBarControl>#progressbar", "smooth", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "smooth", obj, ["normal"]);
    this._addCss("ProgressBar", "smooth", obj, ["normal"]);
    this._addCss("ProgressBarControl", "smooth", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("StatusBarControl>#progressbar", "startimage", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "startimage", obj, ["normal"]);
    this._addCss("ProgressBar", "startimage", obj, ["normal"]);
    this._addCss("ProgressBarControl", "startimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f2efff","theme://images/ico_zoomcombo.png","","0","0","0","50","true");
    this._addCss("StatusBarControl>#zoomcombo", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("16");
    this._addCss("StatusBarControl>#zoomcombo", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_color("#5a280b");
    this._addCss("StatusBarControl>#zoomcombo", "color", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "color", obj, ["normal"]);

    obj = new nexacro.Style_accessibility("","true","all","","","");
    this._addCss("StatusBarControl>#zoomcombo", "itemaccessibility", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itemaccessibility", obj, ["normal"]);
    this._addCss("Combo.cmb_TF", "itemaccessibility", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itemaccessibility", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itemaccessibility", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemaccessibility", obj, ["normal"]);
    this._addCss("ListBox", "itemaccessibility", obj, ["normal"]);
    this._addCss("Menu", "itemaccessibility", obj, ["normal"]);
    this._addCss("PopupMenu", "itemaccessibility", obj, ["normal"]);
    this._addCss("Radio", "itemaccessibility", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo", "itembackground", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("StatusBarControl>#zoomcombo", "itemborder", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemborder", obj, ["normal"]);
    this._addCss("ListBox", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("StatusBarControl>#zoomcombo", "itembordertype", obj, ["normal"]);
    this._addCss("Radio", "itembordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#6e6e63");
    this._addCss("StatusBarControl>#zoomcombo", "itemcolor", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "itemcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("StatusBarControl>#zoomcombo", "itemfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("StatusBarControl>#zoomcombo", "itemgradation", obj, ["normal"]);
    this._addCss("Combo>#combolist", "itemgradation", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itemgradation", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemgradation", obj, ["normal"]);
    this._addCss("ListBox", "itemgradation", obj, ["normal"]);
    this._addCss("Menu", "itemgradation", obj, ["normal"]);
    this._addCss("PopupMenu", "itemgradation", obj, ["normal"]);
    this._addCss("Radio", "itemgradation", obj, ["normal"]);

    obj = new nexacro.Style_value("20");
    this._addCss("StatusBarControl>#zoomcombo", "itemheight", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "itemheight", obj, ["normal"]);
    this._addCss("Combo.cmb_TF", "itemheight", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itemheight", obj, ["normal"]);
    this._addCss("FileUpload", "itemheight", obj, ["normal"]);
    this._addCss("PopupMenu", "itemheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 8");
    this._addCss("StatusBarControl>#zoomcombo", "itempadding", obj, ["normal"]);
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f2efff","","","0","0","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo", "itembackground", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#5a280b");
    this._addCss("StatusBarControl>#zoomcombo", "itemcolor", obj, ["mouseover", "selected"]);

    obj = new nexacro.Style_background("#e0e0d9ff","","","0","0","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo", "itembackground", obj, ["selected"]);

    obj = new nexacro.Style_padding("0 0 0 20");
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "padding", obj, ["normal"]);
    this._addCss("Static.sta_GA_des", "padding", obj, ["normal"]);

    obj = new nexacro.Style_color("#3da2df");
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "caretcolor", obj, ["mouseover", "focused"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "compositecolor", obj, ["mouseover", "focused"]);

    obj = new nexacro.Style_value("#3da2df");
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "selectbackground", obj, ["mouseover", "focused"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("StatusBarControl>#zoomcombo>#comboedit", "selectcolor", obj, ["mouseover", "focused"]);
    this._addCss("Edit", "selectcolor", obj, ["normal"]);
    this._addCss("Edit.edt_pop_con", "selectcolor", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_background("@gradation","theme://images/img_drop_N.png","stretch","6","6","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ffffff 0,100 #e3e3dd");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "gradation", obj, ["normal", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_drop_N.png')");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "imagealign", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "imagealign", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "imagealign", obj, ["normal"]);
    this._addCss("Button.btn_TF_search", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_next", "imagealign", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TF_prev", "imagealign", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_tab_close", "imagealign", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_next", "imagealign", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_prev", "imagealign", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_close", "imagealign", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_LF_hide", "imagealign", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_show", "imagealign", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_Mshow", "imagealign", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_up", "imagealign", obj, ["normal"]);
    this._addCss("Button.btn_LF_down", "imagealign", obj, ["normal"]);
    this._addCss("Button.btn_LF_SHide", "imagealign", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide", "imagealign", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide2", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_search", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "imagealign", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "imagealign", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "imagealign", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("@gradation","theme://images/img_drop_O.png","stretch","6","6","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "background", obj, ["mouseover"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ddddd6 0,100 #acaca6");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "gradation", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_drop_O.png')");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#e1e1daff","","","0","0","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo>#dropbutton", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "background", obj, ["normal"]);
    this._addCss("Button.btn_WF_srh_s", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_save", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_report", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_excel", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_excelup", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_sampledown", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_print", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_add", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_modify", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_delete", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_list", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_sampdown", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.ptl_btn_L_members", "background", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_pw", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Grid>#body", "background", obj, ["normal"]);
    this._addCss("GroupBox", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#6e6e63");
    this._addCss("StatusBarControl>#zoomcombo>#combolist", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("10");
    this._addCss("VScrollBarControl", "barminsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "barminsize", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "barminsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "barminsize", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("VScrollBarControl", "baroutsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "baroutsize", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "baroutsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "baroutsize", obj, ["normal"]);

    obj = new nexacro.Style_value("10");
    this._addCss("VScrollBarControl", "decbtnsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "decbtnsize", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("VScrollBarControl", "imgoutsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "imgoutsize", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "imgoutsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "imgoutsize", obj, ["normal"]);

    obj = new nexacro.Style_value("10");
    this._addCss("VScrollBarControl", "incbtnsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "incbtnsize", obj, ["normal"]);

    obj = new nexacro.Style_background("#e6e4e4ff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("10");
    this._addCss("VScrollBarControl", "scrollbarsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "scrollbarsize", obj, ["normal"]);

    obj = new nexacro.Style_value("34");
    this._addCss("VScrollBarControl", "trackbarsize", obj, ["normal"]);
    this._addCss("HScrollBarControl", "trackbarsize", obj, ["normal"]);

    obj = new nexacro.Style_value("50");
    this._addCss("VScrollBarControl", "opacity", obj, ["disabled"]);
    this._addCss("HScrollBarControl", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_setup", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_SrhShow", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_SrhChange", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_srh_s", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_mymenu_add", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_about", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_page", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_save", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_report", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_excel", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_excelup", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sampledown", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_print", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_add", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_modify", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_delete", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_list", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sampdown", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sl_open", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sl_close", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_st_open", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_WF_st_close", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_Lgn_text", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_Lgn_stext", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_P_close", "opacity", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_L_members", "opacity", obj, ["disabled"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "opacity", obj, ["normal", "focused", "disabled"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "opacity", obj, ["normal", "focused", "disabled"]);
    this._addCss("FileDownload", "opacity", obj, ["disabled"]);
    this._addCss("FileUpload", "opacity", obj, ["disabled"]);
    this._addCss("Static.sta_TF_name", "opacity", obj, ["disabled"]);
    this._addCss("Static.sta_Lgn_bbs", "opacity", obj, ["disabled"]);
    this._addCss("StepControl", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#incbutton", "background", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("VScrollBarControl>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("Form", "bordertype", obj, ["normal"]);
    this._addCss("Button", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_LF_hide", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_show", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_Mshow", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_LF_up", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_down", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_SHide", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SrhChange", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Button.btn_WF_srh_s", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_save", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_report", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_excel", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_excelup", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_sampledown", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_print", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_add", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_modify", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_delete", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_list", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_sampdown", "bordertype", obj, ["normal"]);
    this._addCss("Button.btn_WF_sl_open", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_sl_close", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_open", "bordertype", obj, ["normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_WF_st_close", "bordertype", obj, ["pushed", "normal", "mouseover", "focused", "disabled"]);
    this._addCss("Button.btn_WF_SHide", "bordertype", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide2", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Combo", "bordertype", obj, ["normal"]);
    this._addCss("Combo>#comboedit", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF", "bordertype", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#comboedit", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search", "bordertype", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_WF_search>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Combo.cmb_Lgn_leg", "bordertype", obj, ["normal"]);
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "bordertype", obj, ["normal", "focused"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "bordertype", obj, ["normal"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "bordertype", obj, ["normal", "focused"]);
    this._addCss("Calendar", "bordertype", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar>#calendaredit", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar>#popupcalendar", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#nextbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#prevbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "bordertype", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("CheckBox", "bordertype", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "bordertype", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "bordertype", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "bordertype", obj, ["normal"]);
    this._addCss("Div", "bordertype", obj, ["normal"]);
    this._addCss("Div.div_Lgn_back", "bordertype", obj, ["normal"]);
    this._addCss("Edit", "bordertype", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_Lgn_id", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.edt_Lgn_pw", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Grid", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#body", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controledit", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "bordertype", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "bordertype", obj, ["mouseover"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcalendar", "bordertype", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlprogressbar", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Grid.grd_Lgn_bbs", "bordertype", obj, ["normal"]);
    this._addCss("GroupBox", "bordertype", obj, ["normal"]);
    this._addCss("ListBox", "bordertype", obj, ["normal"]);
    this._addCss("MaskEdit", "bordertype", obj, ["normal"]);
    this._addCss("ImageViewer", "bordertype", obj, ["normal"]);
    this._addCss("Menu", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "bordertype", obj, ["normal", "mouseover", "focused", "selected", "pushed", "disabled"]);
    this._addCss("PopupMenu>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("ProgressBar", "bordertype", obj, ["normal"]);
    this._addCss("ProgressBarControl", "bordertype", obj, ["normal"]);
    this._addCss("Radio", "bordertype", obj, ["normal"]);
    this._addCss("Static", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_GA_title", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_duty", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_titleline", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_logo", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_topbg", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_welcome", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_line", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_TF_mnubg", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_MF_bg", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bg", "bordertype", obj, ["normal"]);
    this._addCss("Spin", "bordertype", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "bordertype", obj, ["normal", "focused", "mouseover"]);
    this._addCss("Spin>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Spin>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("SpinControl", "bordertype", obj, ["normal", "mouseover", "focused"]);
    this._addCss("SpinControl>#spinedit", "bordertype", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinupbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("SpinControl>#spindownbutton", "bordertype", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Tab", "bordertype", obj, ["normal"]);
    this._addCss("Tab>#extrabutton", "bordertype", obj, ["normal"]);
    this._addCss("Tab>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Tab>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Tab.tab_MDI", "bordertype", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#extrabutton", "bordertype", obj, ["normal"]);
    this._addCss("TextArea", "bordertype", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_incN.png')");
    this._addCss("VScrollBarControl>#incbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_align("cetner middle");
    this._addCss("VScrollBarControl>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "imagealign", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "imagealign", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("VScrollBarControl>#incbutton", "padding", obj, ["normal"]);
    this._addCss("VScrollBarControl>#decbutton", "padding", obj, ["normal"]);
    this._addCss("VScrollBarControl>#trackbar", "padding", obj, ["normal"]);
    this._addCss("HScrollBarControl>#incbutton", "padding", obj, ["normal"]);
    this._addCss("HScrollBarControl>#decbutton", "padding", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "padding", obj, ["normal"]);
    this._addCss("Calendar", "padding", obj, ["normal"]);
    this._addCss("Calendar>#dropbutton", "padding", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar.ptl_dash", "padding", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "padding", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "padding", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "padding", obj, ["normal"]);
    this._addCss("Menu>#incbutton", "padding", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "padding", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "padding", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "padding", obj, ["normal"]);
    this._addCss("Static.sta_GA_title", "padding", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_incM.png')");
    this._addCss("VScrollBarControl>#incbutton", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_incD.png')");
    this._addCss("VScrollBarControl>#incbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_decN.png')");
    this._addCss("VScrollBarControl>#decbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_decM.png')");
    this._addCss("VScrollBarControl>#decbutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/Vscl_WF_decD.png')");
    this._addCss("VScrollBarControl>#decbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("33");
    this._addCss("VScrollBarControl>#decbutton", "opacity", obj, ["disabled"]);
    this._addCss("HScrollBarControl>#decbutton", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_background("#696565ff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#trackbar", "background", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("VScrollBarControl>#trackbar", "image", obj, ["normal"]);
    this._addCss("HScrollBarControl>#trackbar", "image", obj, ["normal"]);
    this._addCss("*>#resizebutton", "image", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "image", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "image", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Tab>#spindownbutton", "image", obj, ["pushed", "focused"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "image", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Tab>#spinupbutton", "image", obj, ["pushed", "focused"]);

    obj = new nexacro.Style_background("#3f3d3dff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#trackbar", "background", obj, ["mouseover", "pushed"]);
    this._addCss("HScrollBarControl>#trackbar", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#c9c7c7ff","","","0","0","0","0","true");
    this._addCss("VScrollBarControl>#trackbar", "background", obj, ["disabled"]);
    this._addCss("HScrollBarControl>#trackbar", "background", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_incN.png')");
    this._addCss("HScrollBarControl>#incbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_incM.png')");
    this._addCss("HScrollBarControl>#incbutton", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_incD.png')");
    this._addCss("HScrollBarControl>#incbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_decN.png')");
    this._addCss("HScrollBarControl>#decbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_decM.png')");
    this._addCss("HScrollBarControl>#decbutton", "image", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/Hscl_WF_decD.png')");
    this._addCss("HScrollBarControl>#decbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_background("#dad5d5ff","","","0","0","0","0","true");
    this._addCss("*>#resizebutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#33333333");
    this._addCss("Form", "color", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#ccccccff","");
    this._addCss("Button", "border", obj, ["normal", "focused", "disabled"]);
    this._addCss("Button.btn_WF_page", "border", obj, ["normal", "disabled"]);
    this._addCss("Menu.meu_WF_btn", "border", obj, ["normal", "focused", "selected", "pushed", "disabled"]);

    obj = new nexacro.Style_color("#555555ff");
    this._addCss("Button", "color", obj, ["normal", "focused"]);
    this._addCss("Combo", "color", obj, ["normal"]);
    this._addCss("Calendar", "color", obj, ["normal"]);
    this._addCss("CheckBox", "color", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "color", obj, ["normal"]);
    this._addCss("Edit", "color", obj, ["normal"]);
    this._addCss("GroupBox", "color", obj, ["normal"]);
    this._addCss("ListBox", "color", obj, ["normal"]);
    this._addCss("MaskEdit", "color", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "color", obj, ["normal", "focused", "selected", "pushed"]);
    this._addCss("Radio", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_label", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "color", obj, ["normal"]);
    this._addCss("Spin", "color", obj, ["normal"]);
    this._addCss("TextArea", "color", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("hand");
    this._addCss("Button", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_TF_logout", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_set", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_infor", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_skin", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_search", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_home", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TaF_tab", "cursor", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_TaF_tab_close", "cursor", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_WF_search", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_WF_SrhShow", "cursor", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_WF_SrhChange", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_WF_srh_s", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_mymenu_add", "cursor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Button.btn_WF_about", "cursor", obj, ["normal", "focused"]);
    this._addCss("Button.btn_WF_page", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_save", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_report", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excel", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excelup", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampledown", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_print", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_add", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_modify", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_delete", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_list", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampdown", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sl_open", "cursor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Button.btn_WF_sl_close", "cursor", obj, ["normal", "mouseover", "pushed", "focused"]);
    this._addCss("Button.btn_WF_st_open", "cursor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Button.btn_WF_st_close", "cursor", obj, ["pushed", "normal", "mouseover", "focused"]);
    this._addCss("Button.btn_Lgn_ok", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_Lgn_text", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_Lgn_stext", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_P_close", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.ptl_btn_TF_logout", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_set", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_infor", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_search", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_L_members", "cursor", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.ptl_btn_Lgn_ok", "cursor", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "cursor", obj, ["normal", "disabled"]);
    this._addCss("Menu.meu_WF_btn", "cursor", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);
    this._addCss("Static.sta_TF_name", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("Static.sta_Lgn_bbs", "cursor", obj, ["normal", "mouseover"]);
    this._addCss("StepControl", "cursor", obj, ["normal"]);
    this._addCss("Tab", "cursor", obj, ["normal"]);
    this._addCss("Tab.tab_MDI", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_background("#666666ff","","","0","0","0","0","true");
    this._addCss("Button", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Button", "color", obj, ["mouseover", "pushed"]);
    this._addCss("PopupMenu", "color", obj, ["normal"]);
    this._addCss("Static.sta_TF_name", "color", obj, ["normal", "mouseover", "disabled"]);
    this._addCss("Static.sta_Lgn_new", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#edededff","","","0","0","0","0","true");
    this._addCss("Button", "background", obj, ["disabled"]);
    this._addCss("Combo", "background", obj, ["disabled"]);
    this._addCss("Combo>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Combo>#dropbutton", "background", obj, ["disabled"]);
    this._addCss("Combo.cmb_TF>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Calendar>#spinupbutton", "background", obj, ["disabled"]);
    this._addCss("Calendar>#spindownbutton", "background", obj, ["disabled"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "background", obj, ["disabled"]);
    this._addCss("Calendar.ptl_dash>#spindownbutton", "background", obj, ["disabled"]);
    this._addCss("Edit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controledit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlmaskedit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controltextarea", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "background", obj, ["disabled"]);
    this._addCss("MaskEdit", "background", obj, ["disabled"]);
    this._addCss("Menu.meu_WF_btn", "background", obj, ["disabled"]);
    this._addCss("Spin", "background", obj, ["disabled"]);
    this._addCss("Spin>#spinedit", "background", obj, ["disabled"]);
    this._addCss("Spin>#spinupbutton", "background", obj, ["disabled"]);
    this._addCss("Spin>#spindownbutton", "background", obj, ["disabled"]);
    this._addCss("TextArea", "background", obj, ["disabled"]);
    this._addCss("TextArea.txt_WF_Alert", "background", obj, ["disabled"]);

    obj = new nexacro.Style_color("#909090ff");
    this._addCss("Button", "color", obj, ["disabled"]);
    this._addCss("Combo", "color", obj, ["disabled"]);
    this._addCss("CheckBox", "color", obj, ["disabled"]);
    this._addCss("CheckBox.chk_WF_Srh", "color", obj, ["disabled"]);
    this._addCss("Edit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controledit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlmaskedit", "color", obj, ["disabled"]);
    this._addCss("Grid>#controltextarea", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo", "color", obj, ["disabled"]);
    this._addCss("Grid>#controlbutton", "color", obj, ["disabled"]);
    this._addCss("ListBox", "color", obj, ["disabled"]);
    this._addCss("MaskEdit", "color", obj, ["disabled"]);
    this._addCss("Menu.meu_WF_btn", "color", obj, ["disabled"]);
    this._addCss("Spin", "color", obj, ["disabled"]);
    this._addCss("Spin>#spinedit", "color", obj, ["disabled"]);
    this._addCss("SpinControl>#spinedit", "color", obj, ["disabled"]);
    this._addCss("TextArea", "color", obj, ["disabled"]);
    this._addCss("TextArea.txt_WF_Alert", "color", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('img::T_btn_logout.png')");
    this._addCss("Button.btn_TF_logout", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("70");
    this._addCss("Button.btn_TF_logout", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_TF_set", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_TF_infor", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_TF_skin", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("#00000050","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_logout", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_set", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_infor", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_skin", "background", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("Button.btn_TF_logout", "bordertype", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_set", "bordertype", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_infor", "bordertype", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_skin", "bordertype", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Static.sta_WF_stext", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("100");
    this._addCss("Button.btn_TF_logout", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_set", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_infor", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_skin", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TaF_next", "opacity", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_TaF_prev", "opacity", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_TaF_close", "opacity", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_LF_up", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_down", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_SHide", "opacity", obj, ["mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide", "opacity", obj, ["mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.ptl_btn_TF_logout", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_set", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_infor", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_search", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "opacity", obj, ["mouseover", "pushed"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "opacity", obj, ["mouseover"]);
    this._addCss("Edit.ptl_edt_TF_search", "opacity", obj, ["normal", "mouseover", "focused", "selected"]);

    obj = new nexacro.Style_value("30");
    this._addCss("Button.btn_TF_logout", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_set", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_infor", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_skin", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_search", "opacity", obj, ["normal"]);
    this._addCss("Button.ptl_btn_TF_logout", "opacity", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_TF_set", "opacity", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_TF_infor", "opacity", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_TF_search", "opacity", obj, ["disabled"]);
    this._addCss("Combo.cmb_TF", "opacity", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "opacity", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "opacity", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_TF_search", "opacity", obj, ["disabled"]);
    this._addCss("Grid>#controlexpand", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('img::T_btn_set.png')");
    this._addCss("Button.btn_TF_set", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('img::T_btn_infor.png')");
    this._addCss("Button.btn_TF_infor", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('img::T_btn_skin.png')");
    this._addCss("Button.btn_TF_skin", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_background("","img::btn_TB_setupN.png","","0","0","50","50","true");
    this._addCss("Button.btn_TF_setup", "background", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_background("","img::btn_TB_setupO.png","","0","0","50","50","true");
    this._addCss("Button.btn_TF_setup", "background", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#000000ff","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_search", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Combo.cmb_TF", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Combo.cmb_TF_search", "background", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_TF_search", "background", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("URL('img::T_ico_search.png')");
    this._addCss("Button.btn_TF_search", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.ptl_btn_TF_search", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Combo.cmb_TF_search>#dropbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("80");
    this._addCss("Button.btn_TF_search", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Combo.cmb_TF", "opacity", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Combo.cmb_TF_search", "opacity", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Combo.cmb_Lgn_leg", "opacity", obj, ["readonly"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "opacity", obj, ["readonly"]);
    this._addCss("Edit.edt_TF_search", "opacity", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_value("10");
    this._addCss("Button.btn_TF_search", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TaF_next", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TaF_prev", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TaF_close", "opacity", obj, ["disabled"]);
    this._addCss("Combo.cmb_TF", "opacity", obj, ["disabled"]);
    this._addCss("Combo.cmb_TF_search", "opacity", obj, ["disabled"]);
    this._addCss("Edit.edt_TF_search", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Button.btn_TF_next", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TF_prev", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_next", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_prev", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Button.btn_TaF_close", "border", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Static.sta_stext", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_stext", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_title", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_navi", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::btn_next.png')");
    this._addCss("Button.btn_TF_next", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_next_on.png')");
    this._addCss("Button.btn_TF_next", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("90");
    this._addCss("Button.btn_TF_next", "opacity", obj, ["disabled"]);
    this._addCss("Button.btn_TF_prev", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_prev.png')");
    this._addCss("Button.btn_TF_prev", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_prev_on.png')");
    this._addCss("Button.btn_TF_prev", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::T_btn_home.png')");
    this._addCss("Button.btn_TF_home", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::T_btn_home_on.png')");
    this._addCss("Button.btn_TF_home", "image", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#ecedf1ff","","","0","0","0","0","true");
    this._addCss("Button.btn_TaF_tab", "background", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("1","solid","#e3e4ebff","","1","solid","#dddee4ff","","0","none","","","1","solid","#dddee4ff","");
    this._addCss("Button.btn_TaF_tab", "border", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);

    obj = new nexacro.Style_color("#bfbfbfff");
    this._addCss("Button.btn_TaF_tab", "color", obj, ["normal"]);
    this._addCss("Tab.tab_MDI", "color", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 12 0 15");
    this._addCss("Button.btn_TaF_tab", "padding", obj, ["normal", "mouseover", "selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "padding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("underline 9 dotum");
    this._addCss("Button.btn_TaF_tab", "font", obj, ["mouseover"]);
    this._addCss("Button.btn_Lgn_text", "font", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Combo>#combolist", "font", obj, ["mouseover"]);
    this._addCss("Combo.cmb_TF>#combolist", "font", obj, ["mouseover"]);
    this._addCss("Grid>#controlcombo>#combolist", "font", obj, ["mouseover"]);
    this._addCss("ListBox", "font", obj, ["mouseover"]);
    this._addCss("Static.sta_Lgn_bbs", "font", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#6d6f6eff");
    this._addCss("Button.btn_TaF_tab", "color", obj, ["mouseover"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Button.btn_TaF_tab", "font", obj, ["selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "font", obj, ["normal", "mouseover"]);
    this._addCss("Combo.cmb_Lgn_leg", "font", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "font", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_pw", "font", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Static.sta_WF_label", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_label_duty", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "font", obj, ["normal"]);
    this._addCss("StepControl", "font", obj, ["normal"]);

    obj = new nexacro.Style_color("#6e6e6eff");
    this._addCss("Button.btn_TaF_tab", "color", obj, ["selected", "pushed", "focused"]);
    this._addCss("Button.btn_TaF_tab_on", "color", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_border("1","solid","#e3e4ebff","","1","solid","#dddee4ff","","0","none","","","1","solid","#ffffffff","");
    this._addCss("Button.btn_TaF_tab_on", "border", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("URL('img::tab_extrabtn.png')");
    this._addCss("Button.btn_TaF_tab_close", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::tab_extrabtn_on.png')");
    this._addCss("Button.btn_TaF_tab_close", "image", obj, ["mouseover", "selected", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('img::tab_btn_next.png')");
    this._addCss("Button.btn_TaF_next", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("35");
    this._addCss("Button.btn_TaF_next", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_TaF_prev", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_TaF_close", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("#00000015","","","0","0","0","0","true");
    this._addCss("Button.btn_TaF_next", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_TaF_prev", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_TaF_close", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::tab_btn_prev.png')");
    this._addCss("Button.btn_TaF_prev", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_allclose.png')");
    this._addCss("Button.btn_TaF_close", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_value("URL('img::L_ico_hide.png')");
    this._addCss("Button.btn_LF_hide", "image", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);

    obj = new nexacro.Style_background("#62636bff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_hide", "background", obj, ["mouseover", "pushed", "selected"]);
    this._addCss("Button.btn_LF_show", "background", obj, ["mouseover", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_show.png')");
    this._addCss("Button.btn_LF_show", "image", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);

    obj = new nexacro.Style_background("#e7e7e8ff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_Mshow", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("URL('img::L_ico_Mshow.png')");
    this._addCss("Button.btn_LF_Mshow", "image", obj, ["normal", "focused"]);

    obj = new nexacro.Style_background("#95959bff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_Mshow", "background", obj, ["mouseover", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_Mshow_on.png')");
    this._addCss("Button.btn_LF_Mshow", "image", obj, ["mouseover", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_up.png')");
    this._addCss("Button.btn_LF_up", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("25");
    this._addCss("Button.btn_LF_up", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_down", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("#00000025","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_up", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_down", "background", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_down.png')");
    this._addCss("Button.btn_LF_down", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("","img::L_btn_bg.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_LF_SHide", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("URL('img::L_ico_SHide.png')");
    this._addCss("Button.btn_LF_SHide", "image", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);
    this._addCss("Button.btn_WF_SHide2", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("60");
    this._addCss("Button.btn_LF_SHide", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_WF_SHide", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::L_btn_bg_on.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_LF_SHide", "background", obj, ["mouseover", "pushed", "selected"]);

    obj = new nexacro.Style_background("#4a559aff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_search", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Button.btn_WF_search", "color", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_page", "color", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_Lgn_ok", "color", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_Lgn_text", "color", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Combo>#combolist", "color", obj, ["selected"]);
    this._addCss("Combo.cmb_TF", "color", obj, ["mouseover", "focused", "selected", "disabled", "readonly"]);
    this._addCss("Combo.cmb_TF>#combolist", "color", obj, ["selected"]);
    this._addCss("Combo.cmb_TF_search", "color", obj, ["mouseover", "focused", "selected", "disabled", "readonly"]);
    this._addCss("Edit.edt_TF_search", "color", obj, ["mouseover", "focused", "selected", "disabled"]);
    this._addCss("Grid>#controlcombo>#combolist", "color", obj, ["selected"]);
    this._addCss("ListBox", "color", obj, ["focused", "selected"]);

    obj = new nexacro.Style_font("bold 10 gulim");
    this._addCss("Button.btn_WF_search", "font", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);
    this._addCss("Menu", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#5360b7ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_search", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#aba9a9ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_search", "background", obj, ["disabled"]);

    obj = new nexacro.Style_color("#d8d4d4ff");
    this._addCss("Button.btn_WF_search", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#f3f3f3ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_SrhShow", "background", obj, ["normal", "mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_font("bold 8 dotum");
    this._addCss("Button.btn_WF_SrhShow", "font", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_page", "font", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_color("#4a559aff");
    this._addCss("Button.btn_WF_SrhShow", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("underline bold 8 dotum");
    this._addCss("Button.btn_WF_SrhShow", "font", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_color("#4c3becff");
    this._addCss("Button.btn_WF_SrhShow", "color", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_color("#e0e0e0ff");
    this._addCss("Button.btn_WF_SrhShow", "color", obj, ["disabled"]);
    this._addCss("Button.btn_WF_page", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#e6e6e6ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_SrhChange", "background", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ico_srhchange.png')");
    this._addCss("Button.btn_WF_SrhChange", "image", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);

    obj = new nexacro.Style_align("right middle");
    this._addCss("Button.btn_WF_SrhChange", "imagealign", obj, ["normal", "mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_font("8 dotum");
    this._addCss("Button.btn_WF_SrhChange", "font", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);
    this._addCss("Button.btn_WF_page", "font", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_Lgn_stext", "font", obj, ["normal", "disabled"]);
    this._addCss("CheckBox.chk_Lgn_is", "font", obj, ["normal"]);
    this._addCss("Static.sta_stext", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_stext", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_navi", "font", obj, ["normal"]);
    this._addCss("Static.sta_WF_paging", "font", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_date", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#d3d3d3ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_SrhChange", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#ccccccff","");
    this._addCss("Button.btn_WF_srh_s", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_save", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_report", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_excel", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_excelup", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_sampledown", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_print", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_add", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_modify", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_delete", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_list", "border", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_WF_sampdown", "border", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ico_search.png')");
    this._addCss("Button.btn_WF_srh_s", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_background("#f8f9fbff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_srh_s", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_save", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_report", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excel", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excelup", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampledown", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_print", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_add", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_modify", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_delete", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_list", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampdown", "background", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Menu.meu_WF_btn", "background", obj, ["mouseover"]);

    obj = new nexacro.Style_border("1","solid","#888888ff","");
    this._addCss("Button.btn_WF_srh_s", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_save", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_report", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excel", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_excelup", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampledown", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_print", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_add", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_modify", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_delete", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_list", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Button.btn_WF_sampdown", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Menu.meu_WF_btn", "border", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('img::ico_search_on.png')");
    this._addCss("Button.btn_WF_srh_s", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#ffbb00ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_mymenu_add", "background", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_mymenu_add.png')");
    this._addCss("Button.btn_WF_mymenu_add", "image", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);

    obj = new nexacro.Style_background("#fa7c00ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_mymenu_add", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#a1a1a1ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_about", "background", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Button.btn_WF_about", "bordertype", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("URL('img::btn_about.png')");
    this._addCss("Button.btn_WF_about", "image", obj, ["normal", "mouseover", "pushed", "focused", "disabled"]);

    obj = new nexacro.Style_background("#6b6b6bff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_about", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("Button.btn_WF_about", "bordertype", obj, ["disabled"]);

    obj = new nexacro.Style_value("-1");
    this._addCss("Button.btn_WF_about", "letterspace", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sl_open", "letterspace", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_color("#666666ff");
    this._addCss("Button.btn_WF_page", "color", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_paging", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#666666ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_page", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_save.png')");
    this._addCss("Button.btn_WF_save", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_save_on.png')");
    this._addCss("Button.btn_WF_save", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_report.png')");
    this._addCss("Button.btn_WF_report", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_report_on.png')");
    this._addCss("Button.btn_WF_report", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_excel.png')");
    this._addCss("Button.btn_WF_excel", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_excel_on.png')");
    this._addCss("Button.btn_WF_excel", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_excelup.png')");
    this._addCss("Button.btn_WF_excelup", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_excelup_on.png')");
    this._addCss("Button.btn_WF_excelup", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_sampledown.png')");
    this._addCss("Button.btn_WF_sampledown", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_sampledown_on.png')");
    this._addCss("Button.btn_WF_sampledown", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_print.png')");
    this._addCss("Button.btn_WF_print", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_print_on.png')");
    this._addCss("Button.btn_WF_print", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_add.png')");
    this._addCss("Button.btn_WF_add", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_add_on.png')");
    this._addCss("Button.btn_WF_add", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_modify.png')");
    this._addCss("Button.btn_WF_modify", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_modify_on.png')");
    this._addCss("Button.btn_WF_modify", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_delete.png')");
    this._addCss("Button.btn_WF_delete", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_delete_on.png')");
    this._addCss("Button.btn_WF_delete", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_list.png')");
    this._addCss("Button.btn_WF_list", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_list_on.png')");
    this._addCss("Button.btn_WF_list", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_sampdown.png')");
    this._addCss("Button.btn_WF_sampdown", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_sampdown_on.png')");
    this._addCss("Button.btn_WF_sampdown", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#e7e7e8ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_sl_open", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_WF_sl_close", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_WF_st_open", "background", obj, ["normal", "focused"]);
    this._addCss("Button.btn_WF_st_close", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("URL('img::btn_sl_open.png')");
    this._addCss("Button.btn_WF_sl_open", "image", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_background("#9fa0a2ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_sl_open", "background", obj, ["mouseover", "pushed"]);
    this._addCss("Button.btn_WF_sl_close", "background", obj, ["mouseover", "pushed"]);
    this._addCss("Button.btn_WF_st_open", "background", obj, ["mouseover"]);
    this._addCss("Button.btn_WF_st_close", "background", obj, ["pushed", "mouseover"]);

    obj = new nexacro.Style_value("URL('img::btn_sl_open_on.png')");
    this._addCss("Button.btn_WF_sl_open", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#e3e3e3ff","","","0","0","0","0","true");
    this._addCss("Button.btn_WF_sl_open", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_sl_close", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_st_open", "background", obj, ["disabled"]);
    this._addCss("Button.btn_WF_st_close", "background", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_sl_close.png')");
    this._addCss("Button.btn_WF_sl_close", "image", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_sl_close_on.png')");
    this._addCss("Button.btn_WF_sl_close", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_st_open.png')");
    this._addCss("Button.btn_WF_st_open", "image", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_st_open_on.png')");
    this._addCss("Button.btn_WF_st_open", "image", obj, ["mouseover"]);
    this._addCss("Button.btn_WF_st_close", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('img::btn_st_close.png')");
    this._addCss("Button.btn_WF_st_close", "image", obj, ["normal", "focused", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_st_close_on.png')");
    this._addCss("Button.btn_WF_st_close", "image", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("","img::W_btn_bg.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_WF_SHide", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("URL('img::W_ico_SHide.png')");
    this._addCss("Button.btn_WF_SHide", "image", obj, ["normal", "mouseover", "pushed", "selected", "focused"]);

    obj = new nexacro.Style_background("","img::W_btn_bg_on.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_WF_SHide", "background", obj, ["mouseover", "pushed", "selected"]);

    obj = new nexacro.Style_background("","img::W_btn_bg2.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_WF_SHide2", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::W_btn_bg2_on.png","stretch","0","0","0","0","true");
    this._addCss("Button.btn_WF_SHide2", "background", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_SHide_on.png')");
    this._addCss("Button.btn_WF_SHide2", "image", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#476cb5ff","","","0","0","0","0","true");
    this._addCss("Button.btn_Lgn_ok", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 10 dotum");
    this._addCss("Button.btn_Lgn_ok", "font", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_background("#5b87deff","","","0","0","0","0","true");
    this._addCss("Button.btn_Lgn_ok", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#696868ff","","","0","0","0","0","true");
    this._addCss("Button.btn_Lgn_ok", "background", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_Lgn_ok", "background", obj, ["disabled"]);

    obj = new nexacro.Style_color("#939393ff");
    this._addCss("Button.btn_Lgn_ok", "color", obj, ["disabled"]);
    this._addCss("Button.ptl_btn_Lgn_ok", "color", obj, ["disabled"]);
    this._addCss("Menu", "color", obj, ["disabled"]);

    obj = new nexacro.Style_color("#a3a3a3ff");
    this._addCss("Button.btn_Lgn_text", "color", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_Lgn_stext", "color", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_font("underline 8 dotum");
    this._addCss("Button.btn_Lgn_stext", "font", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('img::P_btn_close.png')");
    this._addCss("Button.btn_P_close", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::P_btn_close_on.png')");
    this._addCss("Button.btn_P_close", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_color("#0a3f8e");
    this._addCss("Button.ptl_btn_TF_logout", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_set", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.ptl_btn_TF_infor", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_logout.png')");
    this._addCss("Button.ptl_btn_TF_logout", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_logout_on.png')");
    this._addCss("Button.ptl_btn_TF_logout", "image", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_set.png')");
    this._addCss("Button.ptl_btn_TF_set", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_set_on.png')");
    this._addCss("Button.ptl_btn_TF_set", "image", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_infor.png')");
    this._addCss("Button.ptl_btn_TF_infor", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_T_btn_infor_on.png')");
    this._addCss("Button.ptl_btn_TF_infor", "image", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#f3f3f5ff","","","0","0","0","0","true");
    this._addCss("Button.ptl_btn_TF_search", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_TF_search", "background", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_border("1","solid","#ebedf0ff","","1","solid","#ebedf0ff","","1","solid","#ebedf0ff","","0","none","","");
    this._addCss("Button.ptl_btn_TF_search", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_border("1","solid","#cececeff","");
    this._addCss("Button.ptl_btn_L_members", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","1","1","true","true","true","true");
    this._addCss("Button.ptl_btn_L_members", "bordertype", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("Button.ptl_btn_L_members", "color", obj, ["normal", "disabled"]);
    this._addCss("Menu.meu_WF_btn", "color", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('img::ptl_btn_members.png')");
    this._addCss("Button.ptl_btn_L_members", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_background("#edededff","","","0","0","0","0","true");
    this._addCss("Button.ptl_btn_L_members", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#a6a6a6ff","");
    this._addCss("Button.ptl_btn_L_members", "border", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Button.ptl_btn_L_members", "color", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#combolist", "color", obj, ["selected"]);
    this._addCss("Menu", "color", obj, ["mouseover", "focused"]);
    this._addCss("Static.sta_Lgn_bbs", "color", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#d4d4d4ff","","","0","0","0","0","true");
    this._addCss("Button.ptl_btn_L_members", "background", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#cdcdcdff","");
    this._addCss("Button.ptl_btn_L_members", "border", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ffbe45ff","","","0","0","0","0","true");
    this._addCss("Button.ptl_btn_Lgn_ok", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#000000ff");
    this._addCss("Button.ptl_btn_Lgn_ok", "color", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Menu", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 11 dotum");
    this._addCss("Button.ptl_btn_Lgn_ok", "font", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_background("#ffd27fff","","","0","0","0","0","true");
    this._addCss("Button.ptl_btn_Lgn_ok", "background", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_font("underline bold 11 dotum");
    this._addCss("Button.ptl_btn_Lgn_ok", "font", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#d5d5d5ff","");
    this._addCss("Combo", "border", obj, ["normal", "disabled"]);
    this._addCss("Calendar", "border", obj, ["normal"]);
    this._addCss("Edit", "border", obj, ["normal"]);
    this._addCss("GroupBox", "border", obj, ["normal"]);
    this._addCss("ListBox", "border", obj, ["normal"]);
    this._addCss("MaskEdit", "border", obj, ["normal"]);
    this._addCss("Spin", "border", obj, ["normal"]);
    this._addCss("TextArea", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Combo", "buttonsize", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_value("22");
    this._addCss("Combo", "itemheight", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "itemheight", obj, ["normal"]);
    this._addCss("ListBox", "itemheight", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#000000ff","");
    this._addCss("Combo", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("MaskEdit", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Spin", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("TextArea", "border", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_value("#2c6282ff");
    this._addCss("Combo", "selectbackground", obj, ["focused", "selected"]);
    this._addCss("Combo>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#calendaredit", "selectbackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#controledit", "selectbackground", obj, ["focused"]);
    this._addCss("Grid>#controlmaskedit", "selectbackground", obj, ["focused"]);
    this._addCss("Grid>#controltextarea", "selectbackground", obj, ["focused"]);
    this._addCss("Grid>#controlcombo>#comboedit", "selectbackground", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "selectbackground", obj, ["mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "selectbackground", obj, ["normal"]);
    this._addCss("Spin", "selectbackground", obj, ["focused", "selected"]);
    this._addCss("Spin>#spinedit", "selectbackground", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinedit", "selectbackground", obj, ["normal", "focused", "mouseover"]);
    this._addCss("TextArea.txt_WF_Alert", "selectbackground", obj, ["focused"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Combo", "selectcolor", obj, ["focused", "selected"]);
    this._addCss("Combo>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#calendaredit", "selectcolor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "selectcolor", obj, ["normal"]);
    this._addCss("Edit", "selectcolor", obj, ["focused", "selected"]);
    this._addCss("Edit.edt_TF_search", "selectcolor", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "selectcolor", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_pw", "selectcolor", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "selectcolor", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "selectcolor", obj, ["normal"]);
    this._addCss("Grid>#controledit", "selectcolor", obj, ["focused"]);
    this._addCss("Grid>#controlmaskedit", "selectcolor", obj, ["focused"]);
    this._addCss("Grid>#controltextarea", "selectcolor", obj, ["focused"]);
    this._addCss("Grid>#controlcombo>#comboedit", "selectcolor", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "selectcolor", obj, ["mouseover"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "selectcolor", obj, ["normal"]);
    this._addCss("MaskEdit", "selectcolor", obj, ["focused", "selected"]);
    this._addCss("Spin", "selectcolor", obj, ["focused", "selected"]);
    this._addCss("Spin>#spinedit", "selectcolor", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinedit", "selectcolor", obj, ["normal", "focused", "mouseover"]);
    this._addCss("TextArea", "selectcolor", obj, ["focused", "selected"]);
    this._addCss("TextArea.txt_WF_Alert", "selectcolor", obj, ["focused"]);

    obj = new nexacro.Style_padding("0 3 0 3");
    this._addCss("Combo>#comboedit", "padding", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF>#comboedit", "padding", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "padding", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#calendaredit", "padding", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#calendaredit", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#calendaredit", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#dropbutton", "padding", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('theme://images/img_cmbdropbtn_N.png')");
    this._addCss("Combo>#dropbutton", "image", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "image", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_cmbdropbtn_P.png')");
    this._addCss("Combo>#dropbutton", "image", obj, ["pushed"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/img_cmbdropbtn_D.png')");
    this._addCss("Combo>#dropbutton", "image", obj, ["disabled"]);
    this._addCss("Grid>#controlcombo>#dropbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#6b6b6bff","");
    this._addCss("Combo>#combolist", "border", obj, ["normal"]);
    this._addCss("Grid>#controledit", "border", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "border", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcombo", "border", obj, ["normal", "disabled"]);
    this._addCss("Grid>#controlcalendar", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "border", obj, ["normal"]);
    this._addCss("SpinControl", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Combo>#combolist", "itembackground", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itembackground", obj, ["normal"]);
    this._addCss("ListBox", "itembackground", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "itembackground", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Combo>#combolist", "itembordertype", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itembordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itembordertype", obj, ["normal"]);
    this._addCss("ListBox", "itembordertype", obj, ["normal"]);
    this._addCss("Menu", "itembordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Combo>#combolist", "itemcolor", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itemcolor", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Combo>#combolist", "itemfont", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itemfont", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#combolist", "itemfont", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 4");
    this._addCss("Combo>#combolist", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#5d5858ff","","","0","0","0","0","true");
    this._addCss("Combo>#combolist", "itembackground", obj, ["selected"]);
    this._addCss("Combo.cmb_TF>#combolist", "itembackground", obj, ["selected"]);
    this._addCss("Grid>#controlcombo>#combolist", "itembackground", obj, ["selected"]);

    obj = new nexacro.Style_color("#6d6d6d");
    this._addCss("Combo.cmb_TF", "color", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "color", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("9 Gulim");
    this._addCss("Combo.cmb_TF", "font", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#000000ff","","","0","0","0","0","true");
    this._addCss("Combo.cmb_TF", "itembackground", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Combo.cmb_TF", "itemborder", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_color("#6d6d6d");
    this._addCss("Combo.cmb_TF", "itemcolor", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itemcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 Gulim");
    this._addCss("Combo.cmb_TF", "itemfont", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itemfont", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 3 0 3");
    this._addCss("Combo.cmb_TF", "itempadding", obj, ["normal"]);
    this._addCss("Combo.cmb_TF>#combolist", "itempadding", obj, ["normal"]);
    this._addCss("Combo.cmb_TF_search", "itempadding", obj, ["normal"]);
    this._addCss("ListBox", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#414244ff","","","0","0","0","0","true");
    this._addCss("Combo.cmb_TF", "background", obj, ["disabled", "readonly"]);
    this._addCss("Combo.cmb_TF_search", "background", obj, ["disabled", "readonly"]);

    obj = new nexacro.Style_value("15");
    this._addCss("Combo.cmb_TF", "opacity", obj, ["readonly"]);
    this._addCss("Combo.cmb_TF_search", "opacity", obj, ["readonly"]);

    obj = new nexacro.Style_background("#00000030","","","0","0","0","0","true");
    this._addCss("Combo.cmb_TF>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "background", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("#00000080");
    this._addCss("Combo.cmb_TF>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#ffffff80");
    this._addCss("Combo.cmb_TF>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Combo.cmb_TF_search>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('img::T_ico_select.png')");
    this._addCss("Combo.cmb_TF>#dropbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "disabled"]);

    obj = new nexacro.Style_border("1","solid","#505153ff","");
    this._addCss("Combo.cmb_TF>#combolist", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#414244ff","","","0","0","0","0","true");
    this._addCss("Combo.cmb_TF>#combolist", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/cmb_WF_dropbtn_N.png')");
    this._addCss("Combo.cmb_WF_search>#dropbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/cmb_WF_dropbtn_O.png')");
    this._addCss("Combo.cmb_WF_search>#dropbutton", "image", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_value("URL('theme://images/cmb_WF_dropbtn_D.png')");
    this._addCss("Combo.cmb_WF_search>#dropbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#bbbbbbff","");
    this._addCss("Combo.cmb_Lgn_leg", "border", obj, ["normal", "disabled"]);
    this._addCss("Edit.edt_Lgn_id", "border", obj, ["normal", "disabled"]);
    this._addCss("Edit.edt_Lgn_pw", "border", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_color("#dddddd");
    this._addCss("Combo.cmb_Lgn_leg", "color", obj, ["normal", "disabled", "readonly"]);
    this._addCss("Edit.edt_Lgn_id", "color", obj, ["normal", "disabled"]);
    this._addCss("Edit.edt_Lgn_pw", "color", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("30");
    this._addCss("Combo.cmb_Lgn_leg", "itemheight", obj, ["normal"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "itemheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 10 0 15");
    this._addCss("Combo.cmb_Lgn_leg", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 10 0 15");
    this._addCss("Combo.cmb_Lgn_leg", "padding", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "padding", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_pw", "padding", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "padding", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "padding", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#717171ff","");
    this._addCss("Combo.cmb_Lgn_leg", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_id", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_pw", "border", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#717171");
    this._addCss("Combo.cmb_Lgn_leg", "color", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_id", "color", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.edt_Lgn_pw", "color", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_background("#91949cff","","","0","0","0","0","true");
    this._addCss("Combo.cmb_Lgn_leg", "background", obj, ["disabled"]);
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg", "background", obj, ["disabled"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "background", obj, ["disabled"]);
    this._addCss("Edit.edt_Lgn_id", "background", obj, ["disabled"]);
    this._addCss("Edit.edt_Lgn_pw", "background", obj, ["disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "background", obj, ["disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "background", obj, ["disabled"]);

    obj = new nexacro.Style_value("#ffffff");
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "selectbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#dddddd");
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#a2a2a2");
    this._addCss("Combo.cmb_Lgn_leg>#comboedit", "color", obj, ["disabled"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "color", obj, ["disabled"]);
    this._addCss("Static.sta_TF_user", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::Lgn_ico_select.png')");
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "image", obj, ["normal", "focused", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#818181ff","","0","none","","");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "border", obj, ["normal", "readonly"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "border", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#46474a");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "color", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "color", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("12 NanumGothic");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "font", obj, ["normal", "mouseover", "focused", "selected"]);
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "font", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "font", obj, ["normal", "mouseover", "focused", "selected"]);

    obj = new nexacro.Style_padding("0 10 0 10");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "itempadding", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 10 0 10");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_head", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "padding", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "padding", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ffbe45ff","","0","none","","");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "border", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "border", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#666666");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "color", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "color", obj, ["mouseover", "focused", "selected"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "color", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#9498a2");
    this._addCss("Combo.ptl_cmb_Lgn_leg", "color", obj, ["disabled", "readonly"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "color", obj, ["disabled"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "color", obj, ["disabled"]);

    obj = new nexacro.Style_value("#666666");
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "selectbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#9498a2");
    this._addCss("Combo.ptl_cmb_Lgn_leg>#comboedit", "selectcolor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#e5e5e5ff","","","0","0","0","0","true");
    this._addCss("Combo.ptl_cmb_Lgn_leg>#combolist", "itembackground", obj, ["selected"]);

    obj = new nexacro.Style_value("URL('img::ptl_Lgn_ico_select.png')");
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "image", obj, ["normal", "focused", "mouseover", "disabled"]);
    this._addCss("Combo.cmb_Lgn_leg>#dropbutton", "image", obj, ["pushed"]);

    obj = new nexacro.Style_font("11 NaumGothic");
    this._addCss("Combo.ptl_cmb_Lgn_leg>#dropbutton", "font", obj, ["disabled"]);

    obj = new nexacro.Style_value("20");
    this._addCss("Calendar", "buttonsize", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "buttonsize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar", "daybackground", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "daybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "daybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "daybackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daybackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar", "dayborder", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar>#popupcalendar", "dayborder", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "dayborder", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "dayborder", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "dayborder", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "dayborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar", "daybordertype", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "daybordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "daybordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "daybordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daybordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daybordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#555555ff");
    this._addCss("Calendar", "daycolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar", "dayfont", obj, ["normal", "mouseover"]);
    this._addCss("Calendar>#popupcalendar", "dayfont", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "dayfont", obj, ["normal", "focused", "selected", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "dayfont", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "dayfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "dayfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar", "daygradation", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "daygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "daygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "daygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daygradation", obj, ["normal"]);

    obj = new nexacro.Style_value("23 23");
    this._addCss("Calendar", "daysize", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "daysize", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "daysize", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "daysize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daysize", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daysize", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar", "popupbackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "popupbackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#8d9093ff","");
    this._addCss("Calendar", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("Calendar", "popupbordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "popupbordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupbordertype", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar", "popupgradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "popupgradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "popupgradation", obj, ["normal"]);
    this._addCss("Menu", "popupgradation", obj, ["normal"]);

    obj = new nexacro.Style_value("190 186");
    this._addCss("Calendar", "popupsize", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "popupsize", obj, ["normal"]);

    obj = new nexacro.Style_color("");
    this._addCss("Calendar", "trailingdaycolor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "trailingdaycolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "trailingdaycolor", obj, ["normal"]);

    obj = new nexacro.Style_value("true");
    this._addCss("Calendar", "usetrailingday", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "usetrailingday", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "usetrailingday", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Calendar", "viewmonthspin", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "viewmonthspin", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "viewmonthspin", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "viewmonthspin", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "viewmonthspin", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "viewmonthspin", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Calendar", "viewyearspin", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar", "viewyearspin", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash", "viewyearspin", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "viewyearspin", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "viewyearspin", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "viewyearspin", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#8d9093ff","");
    this._addCss("Calendar", "border", obj, ["focused", "selected", "mouseover"]);
    this._addCss("Calendar>#popupcalendar", "border", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#yearspin", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin", "border", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#30c5d7ff","","","0","0","0","0","true");
    this._addCss("Calendar", "daybackground", obj, ["focused", "selected"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Calendar", "daycolor", obj, ["focused", "selected", "mouseover"]);
    this._addCss("Grid>#controlcalendar", "daycolor", obj, ["focused", "selected", "mouseover"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Calendar", "dayfont", obj, ["focused", "selected"]);
    this._addCss("Grid>#controlcalendar", "dayfont", obj, ["focused", "selected"]);

    obj = new nexacro.Style_background("#8d9093ff","","","0","0","0","0","true");
    this._addCss("Calendar", "daybackground", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('img::ico_calender.png')");
    this._addCss("Calendar>#dropbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('img::ico_calender_d.png')");
    this._addCss("Calendar>#dropbutton", "image", obj, ["disabled"]);
    this._addCss("Calendar.ptl_dash>#dropbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "bodybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "bodybackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "bodyborder", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "bodyborder", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodyborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "bodybordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "bodybordertype", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "bodygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "bodygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodygradation", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar>#popupcalendar", "daycolor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "daycolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "daycolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "daycolor", obj, ["normal"]);

    obj = new nexacro.Style_background("#8d9093ff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "headerbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#8d9093ff","","0","none","","");
    this._addCss("Calendar>#popupcalendar", "headerborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "headerbordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerbordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerbordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Calendar>#popupcalendar", "headercolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headercolor", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 9 gulim");
    this._addCss("Calendar>#popupcalendar", "headerfont", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerfont", obj, ["normal"]);

    obj = new nexacro.Style_value("yyyy.MM");
    this._addCss("Calendar>#popupcalendar", "headerformat", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerformat", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerformat", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "headergradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headergradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headergradation", obj, ["normal"]);

    obj = new nexacro.Style_value("23");
    this._addCss("Calendar>#popupcalendar", "headerheight", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerheight", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("23 0 0 0");
    this._addCss("Calendar>#popupcalendar", "ncpadding", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "ncpadding", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "ncpadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaybackground", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "saturdayborder", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdayborder", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdayborder", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybordertype", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaybordertype", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybordertype", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_color("#475fb9ff");
    this._addCss("Calendar>#popupcalendar", "saturdaycolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar>#popupcalendar", "saturdayfont", obj, ["normal", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdayfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdayfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "saturdaygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaygradation", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "sundaybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaybackground", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "sundayborder", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundayborder", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundayborder", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "sundaybordertype", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaybordertype", obj, ["normal", "mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybordertype", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_color("#e63f2dff");
    this._addCss("Calendar>#popupcalendar", "sundaycolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar>#popupcalendar", "sundayfont", obj, ["normal", "mouseover"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundayfont", obj, ["normal", "mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundayfont", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "sundaygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaygradation", obj, ["normal"]);

    obj = new nexacro.Style_background("#aee5f2ff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "todaybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "todayborder", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todayborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "todaybordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaybordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar>#popupcalendar", "todaycolor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todaycolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaycolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar>#popupcalendar", "todayfont", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todayfont", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todayfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "todaygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todaygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaygradation", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "trailingdaybackground", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdaybackground", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdaybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar>#popupcalendar", "trailingdayborder", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdayborder", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdayborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Calendar>#popupcalendar", "trailingdaybordertype", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdaybordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdaybordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#bfbfbfff");
    this._addCss("Calendar>#popupcalendar", "trailingdaycolor", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdaycolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdaycolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar>#popupcalendar", "trailingdayfont", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdayfont", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdayfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "trailingdaygradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "trailingdaygradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "trailingdaygradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Calendar>#popupcalendar", "usetrailingday", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "usetrailingday", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "usetrailingday", obj, ["normal"]);

    obj = new nexacro.Style_background("#8d9093ff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "weekbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Calendar>#popupcalendar", "weekcolor", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Calendar>#popupcalendar", "weekfont", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "weekfont", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekfont", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Calendar>#popupcalendar", "weekformat", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "weekformat", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Calendar>#popupcalendar", "weekgradation", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "weekgradation", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekgradation", obj, ["normal"]);

    obj = new nexacro.Style_background("#8d9093ff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybackground", obj, ["mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybackground", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Calendar>#popupcalendar", "saturdaycolor", obj, ["mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaycolor", obj, ["mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaycolor", obj, ["mouseover", "selected"]);

    obj = new nexacro.Style_background("#8d9093ff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "sundaybackground", obj, ["mouseover"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybackground", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Calendar>#popupcalendar", "sundaycolor", obj, ["mouseover", "selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaycolor", obj, ["mouseover", "selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaycolor", obj, ["mouseover", "selected"]);

    obj = new nexacro.Style_background("#d2020bff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "saturdaybackground", obj, ["selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaybackground", obj, ["selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaybackground", obj, ["selected"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Calendar>#popupcalendar", "saturdayfont", obj, ["selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdayfont", obj, ["selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdayfont", obj, ["selected"]);

    obj = new nexacro.Style_background("#d2020bff","","","0","0","0","0","true");
    this._addCss("Calendar>#popupcalendar", "sundaybackground", obj, ["selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaybackground", obj, ["selected"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Calendar>#popupcalendar", "sundayfont", obj, ["selected"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundayfont", obj, ["selected"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundayfont", obj, ["selected"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_nextbtn_N.png')");
    this._addCss("Calendar>#popupcalendar>#nextbutton", "image", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_nextbtn_O.png')");
    this._addCss("Calendar>#popupcalendar>#nextbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_nextbtn_D.png')");
    this._addCss("Calendar>#popupcalendar>#nextbutton", "image", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#nextbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_prevbtn_N.png')");
    this._addCss("Calendar>#popupcalendar>#prevbutton", "image", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_prevbtn_O.png')");
    this._addCss("Calendar>#popupcalendar>#prevbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_Popcal_prevbtn_D.png')");
    this._addCss("Calendar>#popupcalendar>#prevbutton", "image", obj, ["disabled"]);
    this._addCss("Grid>#controlcalendar>#popupcalendar>#prevbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_font("9 gulim");
    this._addCss("Calendar>#popupcalendar>#yearspin", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Grid>#controlbutton", "font", obj, ["normal", "disabled"]);
    this._addCss("SpinControl", "font", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Tab.tab_MDI", "font", obj, ["normal"]);

    obj = new nexacro.Style_value("8");
    this._addCss("Calendar>#popupcalendar>#yearspin", "buttonsize", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin", "buttonsize", obj, ["normal", "mouseover", "focused"]);
    this._addCss("SpinControl", "buttonsize", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spinupbtn_N.png')");
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Calendar>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Spin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("SpinControl>#spinupbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spinupbtn_O.png')");
    this._addCss("Calendar>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Spin>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("SpinControl>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spindownbtn_N.png')");
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Calendar>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Spin>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("SpinControl>#spindownbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spindownbtn_O.png')");
    this._addCss("Calendar>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("SpinControl>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("6");
    this._addCss("Calendar>#popupcalendar>#monthspin", "buttonsize", obj, ["normal", "mouseover", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin", "buttonsize", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spinupbtn_D.png')");
    this._addCss("Calendar>#spinupbutton", "image", obj, ["disabled"]);
    this._addCss("Spin>#spinupbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_spindownbtn_D.png')");
    this._addCss("Calendar>#spindownbutton", "image", obj, ["disabled"]);
    this._addCss("Spin>#spindownbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_color("#666666ff");
    this._addCss("Calendar.ptl_dash", "daycolor", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar.ptl_dash", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_background("#dededeff","","","0","0","0","0","true");
    this._addCss("Calendar.ptl_dash", "daybackground", obj, ["focused", "selected", "mouseover"]);

    obj = new nexacro.Style_color("#000000ff");
    this._addCss("Calendar.ptl_dash", "daycolor", obj, ["focused", "selected", "mouseover"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerborder", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headercolor", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 10 dotum");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "headerfont", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "saturdaycolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "sundaycolor", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todaybackground", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#ffa71bff","");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todayborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "todaybordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "weekbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Calendar.ptl_dash>#popupcalendar", "weekcolor", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::ptl_cal_nextbtn_N.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_cal_nextbtn_O.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("50%");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#nextbutton", "opacity", obj, ["disabled"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "opacity", obj, ["disabled"]);
    this._addCss("Edit.edt_pop_con", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_cal_prevbtn_N.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::ptl_cal_prevbtn_O.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#prevbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spinupbtn_N.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spinupbtn_O.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#spinupbutton", "image", obj, ["mouseover", "focused"]);
    this._addCss("Calendar.cal_dash>#spinupbutton", "image", obj, ["pushed", "selected"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spindownbtn_N.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Calendar.ptl_dash>#spindownbutton", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spindownbtn_O.png')");
    this._addCss("Calendar.ptl_dash>#popupcalendar>#yearspin>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);
    this._addCss("Calendar.ptl_dash>#popupcalendar>#monthspin>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spinupbtn_D.png')");
    this._addCss("Calendar.ptl_dash>#spinupbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/ptl_cal_spindownbtn_D.png')");
    this._addCss("Calendar.ptl_dash>#spindownbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("left middle");
    this._addCss("CheckBox", "buttonalign", obj, ["normal"]);
    this._addCss("Radio", "buttonalign", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("CheckBox", "buttonbackground", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonbackground", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonbackground", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "buttonbackground", obj, ["normal"]);
    this._addCss("Radio", "buttonbackground", obj, ["normal"]);
    this._addCss("Tab", "buttonbackground", obj, ["selected", "focused"]);
    this._addCss("Tab.tab_MDI", "buttonbackground", obj, ["selected", "focused"]);

    obj = new nexacro.Style_value("1 solid #d5d5d5ff");
    this._addCss("CheckBox", "buttonborder", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("CheckBox", "buttonbordertype", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonbordertype", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonbordertype", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "buttonbordertype", obj, ["normal"]);
    this._addCss("Tab", "buttonbordertype", obj, ["normal", "selected", "focused"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("CheckBox", "buttongradation", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "buttongradation", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "buttongradation", obj, ["normal"]);
    this._addCss("Radio", "buttongradation", obj, ["normal"]);
    this._addCss("Tab", "buttongradation", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/img_check.png')");
    this._addCss("CheckBox", "buttonimage", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonimage", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonimage", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "buttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("15");
    this._addCss("CheckBox", "buttonsize", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "buttonsize", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonsize", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonsize", obj, ["normal"]);
    this._addCss("StepControl", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 3 0 5");
    this._addCss("CheckBox", "textpadding", obj, ["normal"]);
    this._addCss("CheckBox.chk_WF_Srh", "textpadding", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "textpadding", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "textpadding", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #000000ff");
    this._addCss("CheckBox", "buttonborder", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#edededff","","","0","0","0","0","true");
    this._addCss("CheckBox", "buttonbackground", obj, ["disabled"]);
    this._addCss("CheckBox.chk_WF_Srh", "buttonbackground", obj, ["disabled"]);
    this._addCss("Grid>#controlcheckbox", "buttonbackground", obj, ["disabled"]);
    this._addCss("Radio", "buttonbackground", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_check_D.png')");
    this._addCss("CheckBox", "buttonimage", obj, ["disabled"]);
    this._addCss("CheckBox.chk_WF_Srh", "buttonimage", obj, ["disabled"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonimage", obj, ["disabled"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonimage", obj, ["disabled"]);
    this._addCss("Grid>#controlcheckbox", "buttonimage", obj, ["disabled"]);

    obj = new nexacro.Style_value("left middle");
    this._addCss("CheckBox.chk_WF_Srh", "buttonalign", obj, ["normal"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonalign", obj, ["normal"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonalign", obj, ["normal"]);
    this._addCss("Grid>#controlcheckbox", "buttonalign", obj, ["normal"]);

    obj = new nexacro.Style_background("#9f9f9fff","","","0","0","0","0","true");
    this._addCss("CheckBox.chk_WF_Srh", "buttonbackground", obj, ["normal"]);

    obj = new nexacro.Style_value("0 none");
    this._addCss("CheckBox.chk_WF_Srh", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("CheckBox.chk_WF_Srh", "buttonbordertype", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("URL('img::img_check2.png')");
    this._addCss("CheckBox.chk_WF_Srh", "buttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #9f9f9fff");
    this._addCss("CheckBox.chk_WF_Srh", "buttonborder", obj, ["mouseover", "focused"]);
    this._addCss("CheckBox.chk_Lgn_is", "buttonborder", obj, ["mouseover", "focused"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonborder", obj, ["mouseover", "focused"]);

    obj = new nexacro.Style_value("1 solid #d5d5d5ff");
    this._addCss("CheckBox.chk_WF_Srh", "buttonborder", obj, ["disabled"]);

    obj = new nexacro.Style_value("1 solid #696969");
    this._addCss("CheckBox.chk_Lgn_is", "buttonborder", obj, ["normal", "disabled"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonborder", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_color("#b9b9b9");
    this._addCss("CheckBox.chk_Lgn_is", "color", obj, ["normal"]);

    obj = new nexacro.Style_color("#434448");
    this._addCss("CheckBox.chk_Lgn_is", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#91949cff","","","0","0","0","0","true");
    this._addCss("CheckBox.chk_Lgn_is", "buttonbackground", obj, ["disabled"]);
    this._addCss("CheckBox.ptl_chk_Lgn_is", "buttonbackground", obj, ["disabled"]);

    obj = new nexacro.Style_color("#696969");
    this._addCss("CheckBox.ptl_chk_Lgn_is", "color", obj, ["normal", "disabled"]);
    this._addCss("Static.sta_Lgn_bbs", "color", obj, ["normal", "disabled"]);
    this._addCss("Static.sta_Lgn_date", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("@gradation","","","0","0","0","0","true");
    this._addCss("Div", "background", obj, ["normal"]);
    this._addCss("FileDownload", "background", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar", "background", obj, ["normal"]);
    this._addCss("Grid>#vscrollbar>#trackbar", "background", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Grid>#hscrollbar", "background", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "background", obj, ["normal", "mouseover", "focused", "pushed"]);
    this._addCss("Menu", "background", obj, ["normal"]);
    this._addCss("PopupDiv", "background", obj, ["normal"]);
    this._addCss("Static.sta_TF_titleline", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","solid","#b1b1b1ff","");
    this._addCss("Div", "border", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ffffffff 0,100 #fafafaff");
    this._addCss("Div", "gradation", obj, ["normal"]);
    this._addCss("PopupDiv", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::login_back.png","stretch","0","0","0","0","true");
    this._addCss("Div.div_Lgn_back", "background", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Div.div_Lgn_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_Lgn_bg", "border", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("Edit", "padding", obj, ["normal"]);
    this._addCss("Edit.edt_TF_search", "padding", obj, ["normal"]);
    this._addCss("Edit.edt_pop_con", "padding", obj, ["normal", "mouseover"]);
    this._addCss("Edit.ptl_edt_TF_search", "padding", obj, ["normal"]);
    this._addCss("Grid>#controledit", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "padding", obj, ["normal"]);
    this._addCss("Grid>#controlcombo>#comboedit", "padding", obj, ["normal", "focused"]);
    this._addCss("Grid>#contolcombo>#controlcombo", "padding", obj, ["mouseover"]);
    this._addCss("MaskEdit", "padding", obj, ["normal"]);
    this._addCss("StepControl", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("#777777");
    this._addCss("Edit", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#777777");
    this._addCss("Edit", "caretcolor", obj, ["normal"]);

    obj = new nexacro.Style_value("#2c6282ff");
    this._addCss("Edit", "selectbackground", obj, ["focused", "selected"]);
    this._addCss("MaskEdit", "selectbackground", obj, ["focused", "selected"]);
    this._addCss("TextArea", "selectbackground", obj, ["focused", "selected"]);

    obj = new nexacro.Style_value("#091e40");
    this._addCss("Edit.edt_TF_search", "selectbackground", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "selectbackground", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_pw", "selectbackground", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "selectbackground", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_pw", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#a2a2a2");
    this._addCss("Edit.edt_TF_search", "displaynulltextcolor", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_id", "displaynulltextcolor", obj, ["normal"]);
    this._addCss("Edit.edt_Lgn_pw", "displaynulltextcolor", obj, ["normal"]);
    this._addCss("Edit.ptl_edt_Lgn_id", "displaynulltextcolor", obj, ["normal"]);

    obj = new nexacro.Style_value("#777777");
    this._addCss("Edit.edt_pop_con", "selectbackground", obj, ["normal", "mouseover", "selected"]);

    obj = new nexacro.Style_border("1","solid","#ebedf0ff","");
    this._addCss("Edit.ptl_edt_TF_search", "border", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_color("#989899");
    this._addCss("Edit.ptl_edt_TF_search", "color", obj, ["normal", "mouseover", "focused", "selected", "disabled"]);

    obj = new nexacro.Style_value("#ebedf0");
    this._addCss("Edit.ptl_edt_TF_search", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#989899");
    this._addCss("Edit.ptl_edt_TF_search", "selectcolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#dcdcdd");
    this._addCss("Edit.ptl_edt_TF_search", "displaynulltextcolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#dddddd");
    this._addCss("Edit.ptl_edt_Lgn_pw", "displaynulltextcolor", obj, ["normal"]);

    obj = new nexacro.Style_blur("");
    this._addCss("FileDownload", "blur", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#999999ff","");
    this._addCss("FileDownload", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","3","3","true","true","true","true");
    this._addCss("FileDownload", "bordertype", obj, ["normal"]);
    this._addCss("Grid>#controlbutton", "bordertype", obj, ["disabled"]);
    this._addCss("PopupDiv", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #fefefe 0,100 #ecece5");
    this._addCss("FileDownload", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("FileDownload", "linespace", obj, ["normal"]);
    this._addCss("Static", "linespace", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ecece5 0,100 #fefefe");
    this._addCss("FileDownload", "gradation", obj, ["pushed"]);

    obj = new nexacro.Style_background("@gradation","","","0","0","0","0","true");
    this._addCss("FileUpload", "buttonbackground", obj, ["normal"]);
    this._addCss("StepControl", "buttonbackground", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #999999");
    this._addCss("FileUpload", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","3","3","true","true","true","true");
    this._addCss("FileUpload", "buttonbordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("FileUpload", "buttoncolor", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("FileUpload", "buttonfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #fefefe 0,100 #ecece5");
    this._addCss("FileUpload", "buttongradation", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("FileUpload", "buttonpadding", obj, ["normal"]);

    obj = new nexacro.Style_value("70");
    this._addCss("FileUpload", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_value("File add");
    this._addCss("FileUpload", "buttontext", obj, ["normal"]);

    obj = new nexacro.Style_color("#8a8c9dff");
    this._addCss("FileUpload", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("FileUpload", "editbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#a6a6a9ff","","1","solid","#d5d5d5ff","","1","solid","#d5d5d5ff","","1","solid","#a6a6a9ff","");
    this._addCss("FileUpload", "editborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("FileUpload", "editbordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333");
    this._addCss("FileUpload", "editcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("FileUpload", "editfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("FileUpload", "editgradation", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("FileUpload", "editpadding", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 9 Dotum");
    this._addCss("FileUpload", "font", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ecece5 0,100 #fefefe");
    this._addCss("FileUpload", "buttongradation", obj, ["pushed"]);

    obj = new nexacro.Style_border("1","solid","#d5d8deff","","1","solid","#d5d8deff","","1","solid","#d5d8deff","","1","solid","#d5d8deff","");
    this._addCss("Grid", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("");
    this._addCss("Grid", "line", obj, ["normal"]);

    obj = new nexacro.Style_border("");
    this._addCss("Grid", "selectline", obj, ["normal"]);
    this._addCss("Grid>#head", "selectline", obj, ["normal"]);
    this._addCss("Grid>#body", "selectline", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectline", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectline", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid", "selectpointimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_treeclose.png')");
    this._addCss("Grid", "treeclosebuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid", "treecollapseimage", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid", "treeexpandimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_treeimage.png')");
    this._addCss("Grid", "treeitemimage", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid", "treeitemmargin", obj, ["normal"]);

    obj = new nexacro.Style_line("0","","","");
    this._addCss("Grid", "treelinetype", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_treeopen.png')");
    this._addCss("Grid", "treeopenbuttonimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#888fa2ff","","","0","0","0","0","true");
    this._addCss("Grid>#head", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","1","solid","#aeaeaeff","","1","solid","#aeaeaeff","","0","none","","");
    this._addCss("Grid>#head", "border", obj, ["normal"]);

    obj = new nexacro.Style_align("");
    this._addCss("Grid>#head", "cellalign", obj, ["normal"]);
    this._addCss("Grid>#summ", "cellalign", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellalign", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Grid>#head", "cellbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Grid>#head", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Grid>#head", "cellcolor", obj, ["normal"]);

    obj = new nexacro.Style_color("");
    this._addCss("Grid>#head", "cellcolor2", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Grid>#head", "cellfont", obj, ["normal"]);
    this._addCss("Grid>#summ", "cellfont", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Grid>#head", "cellgradation", obj, ["normal"]);
    this._addCss("Grid>#body", "cellgradation", obj, ["normal"]);
    this._addCss("Grid>#summ", "cellgradation", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellgradation", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Grid>#head", "cellgradation2", obj, ["normal"]);
    this._addCss("Grid>#body", "cellgradation2", obj, ["normal"]);
    this._addCss("Grid>#summ", "cellgradation2", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellgradation2", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#d5d8deff","");
    this._addCss("Grid>#head", "cellline", obj, ["normal"]);
    this._addCss("Grid>#body", "cellline", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid>#head", "celllinespace", obj, ["normal"]);
    this._addCss("Grid>#body", "celllinespace", obj, ["normal"]);
    this._addCss("Grid>#summ", "celllinespace", obj, ["normal"]);
    this._addCss("Grid>#summary", "celllinespace", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid>#head", "celllinetype", obj, ["normal"]);
    this._addCss("Grid>#summ", "celllinetype", obj, ["normal"]);
    this._addCss("Grid>#summary", "celllinetype", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("Grid>#head", "cellpadding", obj, ["normal"]);
    this._addCss("Grid>#body", "cellpadding", obj, ["normal"]);
    this._addCss("Grid>#summ", "cellpadding", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellpadding", obj, ["normal"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellpadding", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid>#head", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("");
    this._addCss("Grid>#head", "selectborder", obj, ["normal"]);
    this._addCss("Grid>#body", "selectborder", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectborder", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectborder", obj, ["normal"]);

    obj = new nexacro.Style_color("");
    this._addCss("Grid>#head", "selectcolor", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectcolor", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("Grid>#head", "selectfont", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectfont", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectfont", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Grid>#head", "selectgradation", obj, ["normal"]);
    this._addCss("Grid>#body", "selectgradation", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectgradation", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectgradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Grid>#head", "selectlinetype", obj, ["normal"]);
    this._addCss("Grid>#body", "selectlinetype", obj, ["normal"]);
    this._addCss("Grid>#summ", "selectlinetype", obj, ["normal"]);
    this._addCss("Grid>#summary", "selectlinetype", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","solid","#e6e6e6ff","","1","solid","#e6e6e6ff","","0","none","","");
    this._addCss("Grid>#body", "border", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Grid>#body", "cellalign", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground", obj, ["normal"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellbackground", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#f9f9faff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_color("#666666ff");
    this._addCss("Grid>#body", "cellcolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#666666ff");
    this._addCss("Grid>#body", "cellcolor2", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Grid>#body", "cellfont", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#body", "cellfont", obj, ["normal", "focused"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellfont", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_value("exhor");
    this._addCss("Grid>#body", "celllinetype", obj, ["normal"]);

    obj = new nexacro.Style_value("#d5dbfbff");
    this._addCss("Grid>#body", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#000000ff");
    this._addCss("Grid>#body", "selectcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Grid>#body", "selectfont", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#body", "selectfont", obj, ["normal", "focused"]);

    obj = new nexacro.Style_background("#f1f1f7ff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#eaeaf3ff","","","0","0","0","0","true");
    this._addCss("Grid>#body", "cellbackground2", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Grid>#body", "cellcolor", obj, ["mouseover"]);
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Grid>#body", "cellcolor2", obj, ["mouseover"]);
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor2", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#f5f9fcff","","","0","0","0","0","true");
    this._addCss("Grid>#summ", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f5f9fcff","","","0","0","0","0","true");
    this._addCss("Grid>#summ", "cellbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#f5f9fcff","","","0","0","0","0","true");
    this._addCss("Grid>#summ", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Grid>#summ", "cellcolor", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellcolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Grid>#summ", "cellcolor2", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellcolor2", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c9c6c3ff","");
    this._addCss("Grid>#summ", "cellline", obj, ["normal"]);
    this._addCss("Grid>#summary", "cellline", obj, ["normal"]);

    obj = new nexacro.Style_background("#dbe8f1ff","","","0","0","0","0","true");
    this._addCss("Grid>#summary", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#dbe8f1ff","","","0","0","0","0","true");
    this._addCss("Grid>#summary", "cellbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#dbe8f1ff","","","0","0","0","0","true");
    this._addCss("Grid>#summary", "cellbackground2", obj, ["normal"]);

    obj = new nexacro.Style_value("#ffffff00");
    this._addCss("Grid>#controledit", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "selectbackground", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "selectbackground", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_color("#333333ff");
    this._addCss("Grid>#controledit", "selectcolor", obj, ["normal"]);
    this._addCss("Grid>#controlmaskedit", "selectcolor", obj, ["normal"]);
    this._addCss("Grid>#controltextarea", "selectcolor", obj, ["normal"]);
    this._addCss("MaskEdit", "selectcolor", obj, ["normal"]);
    this._addCss("TextArea", "selectcolor", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "selectcolor", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#9a080aff","");
    this._addCss("Grid>#controledit", "border", obj, ["mouseover", "focused"]);
    this._addCss("Grid>#controlmaskedit", "border", obj, ["mouseover", "focused"]);
    this._addCss("Grid>#controltextarea", "border", obj, ["mouseover", "focused"]);
    this._addCss("Grid>#controlcombo>#combolist", "border", obj, ["normal"]);
    this._addCss("Grid>#controlcalendar", "border", obj, ["focused", "selected", "mouseover"]);

    obj = new nexacro.Style_align("right middle");
    this._addCss("Grid>#controlmaskedit", "align", obj, ["normal"]);
    this._addCss("MaskEdit", "align", obj, ["normal"]);
    this._addCss("Static.sta_WF_navi", "align", obj, ["normal"]);
    this._addCss("Spin>#spinedit", "align", obj, ["normal", "focused", "mouseover"]);
    this._addCss("SpinControl>#spinedit", "align", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_align("left top");
    this._addCss("Grid>#controltextarea", "align", obj, ["normal"]);
    this._addCss("Static.sta_search_l", "align", obj, ["normal"]);
    this._addCss("Static.sta_search_t", "align", obj, ["normal"]);
    this._addCss("TextArea", "align", obj, ["normal"]);

    obj = new nexacro.Style_value("3");
    this._addCss("Grid>#controltextarea", "linespace", obj, ["normal"]);
    this._addCss("TextArea", "linespace", obj, ["normal"]);
    this._addCss("TextArea.txt_WF_Alert", "linespace", obj, ["normal"]);

    obj = new nexacro.Style_padding("3 3 3 3");
    this._addCss("Grid>#controltextarea", "padding", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#9a080aff","");
    this._addCss("Grid>#controlcombo", "border", obj, ["mouseover"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("Grid>#controlcombo>#combolist", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#6b6b6bff","");
    this._addCss("Grid>#controlcalendar", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_value("163 186");
    this._addCss("Grid>#controlcalendar", "popupsize", obj, ["normal"]);

    obj = new nexacro.Style_background("#d2020bff","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar", "daybackground", obj, ["focused", "selected"]);

    obj = new nexacro.Style_background("#7c7676ff","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar", "daybackground", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/img_caldropbtn_N.png')");
    this._addCss("Grid>#controlcalendar>#dropbutton", "image", obj, ["normal", "mouseover", "focused", "pushed", "selected"]);

    obj = new nexacro.Style_value("URL('theme://images/img_caldropbtn_D.png')");
    this._addCss("Grid>#controlcalendar>#dropbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "bodybordertype", obj, ["normal"]);

    obj = new nexacro.Style_background("#7c7676ff","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#6b6b6bff","","0","none","","");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "headerborder", obj, ["normal"]);

    obj = new nexacro.Style_color("#0041b6ff");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "saturdaycolor", obj, ["normal"]);

    obj = new nexacro.Style_color("#e74c00ff");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaycolor", obj, ["normal"]);

    obj = new nexacro.Style_background("#fcb040ff","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "todaybackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_popcalweekbg.png","repeat-x","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekbackground", obj, ["normal"]);

    obj = new nexacro.Style_value("일 월 화 수 목 금 토");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "weekformat", obj, ["normal"]);

    obj = new nexacro.Style_background("#02d24fff","","","0","0","0","0","true");
    this._addCss("Grid>#controlcalendar>#popupcalendar", "sundaybackground", obj, ["selected"]);

    obj = new nexacro.Style_background("","theme://images/img_btn_N.png","stretch","4","4","0","0","true");
    this._addCss("Grid>#controlbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_btn_O.png","stretch","4","4","0","0","true");
    this._addCss("Grid>#controlbutton", "background", obj, ["mouseover", "focused"]);

    obj = new nexacro.Style_font("underline 9 gulim");
    this._addCss("Grid>#controlbutton", "font", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Tab.tab_MDI", "font", obj, ["mouseover"]);

    obj = new nexacro.Style_background("","theme://images/img_btn_P.png","stretch","4","4","0","0","true");
    this._addCss("Grid>#controlbutton", "background", obj, ["pushed"]);

    obj = new nexacro.Style_background("#eeeeeeff","","","0","0","0","0","true");
    this._addCss("Grid>#controlbutton", "background", obj, ["disabled"]);

    obj = new nexacro.Style_border("1","solid","#777777ff","");
    this._addCss("Grid>#controlbutton", "border", obj, ["disabled"]);

    obj = new nexacro.Style_value("1 solid #6b6b6bff");
    this._addCss("Grid>#controlcheckbox", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_value("13");
    this._addCss("Grid>#controlcheckbox", "buttonsize", obj, ["normal"]);
    this._addCss("Radio", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 5");
    this._addCss("Grid>#controlcheckbox", "textpadding", obj, ["normal"]);
    this._addCss("Radio", "textpadding", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #a31d1fff");
    this._addCss("Grid>#controlcheckbox", "buttonborder", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#999999ff");
    this._addCss("Grid>#controlcheckbox", "color", obj, ["disabled"]);
    this._addCss("GroupBox", "color", obj, ["disabled"]);

    obj = new nexacro.Style_value("1 solid #6e6e6eff");
    this._addCss("Grid>#controlcheckbox", "buttonborder", obj, ["disabled"]);

    obj = new nexacro.Style_background("","img::ico_search.png","","0","0","0","50","true");
    this._addCss("Grid>#controlexpand", "background", obj, ["normal"]);

    obj = new nexacro.Style_line("0","none","","");
    this._addCss("Grid>#controlexpand", "focusborder", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::ico_search_on.png","","0","0","0","50","true");
    this._addCss("Grid>#controlexpand", "background", obj, ["mouseover", "pushed"]);

    obj = new nexacro.Style_background("#433e3eff","","","0","0","0","0","true");
    this._addCss("Grid>#controlprogressbar", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("transparent");
    this._addCss("Grid>#controlprogressbar", "barcolor", obj, ["normal"]);
    this._addCss("ProgressBar", "barcolor", obj, ["normal"]);
    this._addCss("ProgressBarControl", "barcolor", obj, ["normal"]);

    obj = new nexacro.Style_value("image");
    this._addCss("Grid>#controlprogressbar", "bartype", obj, ["normal"]);
    this._addCss("ProgressBar", "bartype", obj, ["normal"]);
    this._addCss("ProgressBarControl", "bartype", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#433e3eff","");
    this._addCss("Grid>#controlprogressbar", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_progress.png')");
    this._addCss("Grid>#controlprogressbar", "progressimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_progress_D.png')");
    this._addCss("Grid>#controlprogressbar", "progressimage", obj, ["disabled"]);

    obj = new nexacro.Style_value("0");
    this._addCss("Grid>#vscrollbar", "decbtnsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "decbtnsize", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "decbtnsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_gradation("linear 0,0 #d7d7d7 100,0 #ffffff");
    this._addCss("Grid>#vscrollbar", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("0");
    this._addCss("Grid>#vscrollbar", "incbtnsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "incbtnsize", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "incbtnsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_padding("1 0 1 0");
    this._addCss("Grid>#vscrollbar", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("11");
    this._addCss("Grid>#vscrollbar", "scrollbarsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "scrollbarsize", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "scrollbarsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("45");
    this._addCss("Grid>#vscrollbar", "trackbarsize", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar", "trackbarsize", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#929292ff","");
    this._addCss("Grid>#vscrollbar>#trackbar", "border", obj, ["normal"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "border", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #dbdbdb 100,0 #aaaaaa");
    this._addCss("Grid>#vscrollbar>#trackbar", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/grd_WF_vsclTrackbarImg.png')");
    this._addCss("Grid>#vscrollbar>#trackbar", "image", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #dbdbdb 100,0 #7d7d7d");
    this._addCss("Grid>#vscrollbar>#trackbar", "gradation", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#6a6a6aff","");
    this._addCss("Grid>#vscrollbar>#trackbar", "border", obj, ["mouseover", "focused", "pushed"]);
    this._addCss("Grid>#hscrollbar>#trackbar", "border", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_border("0","none","","","1","solid","#c9c6c3ff","");
    this._addCss("Grid>#hscrollbar", "border", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #d7d7d7 0,100 #ffffff");
    this._addCss("Grid>#hscrollbar", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 1 0 1");
    this._addCss("Grid>#hscrollbar", "padding", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #dbdbdb 0,100 #aaaaaa");
    this._addCss("Grid>#hscrollbar>#trackbar", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/grd_WF_hsclTrackbarImg.png')");
    this._addCss("Grid>#hscrollbar>#trackbar", "image", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #dbdbdb 0,100 #7d7d7d");
    this._addCss("Grid>#hscrollbar>#trackbar", "gradation", obj, ["mouseover", "focused", "pushed"]);

    obj = new nexacro.Style_background("#d9d9d9ff","","","0","0","0","0","true");
    this._addCss("Grid>#resizebutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f5faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh", "background", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#body", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_border("1","solid","#cad0d9ff","");
    this._addCss("Grid.grd_TF_seh", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#5b6473");
    this._addCss("Grid.grd_TF_seh", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f5faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground", obj, ["normal", "focused"]);

    obj = new nexacro.Style_background("#f2f5faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground2", obj, ["normal", "focused"]);

    obj = new nexacro.Style_color("#5b6473");
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor", obj, ["normal", "focused"]);

    obj = new nexacro.Style_color("#5b6473");
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor2", obj, ["normal", "focused"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#e4e9f3ff","","0","none","","");
    this._addCss("Grid.grd_TF_seh>#body", "cellline", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_padding("0 0 0 5");
    this._addCss("Grid.grd_TF_seh>#body", "cellpadding", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("#dde2eb");
    this._addCss("Grid.grd_TF_seh>#body", "selectbackground", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Grid.grd_TF_seh>#body", "selectcolor", obj, ["normal", "focused", "mouseover"]);
    this._addCss("Grid.grd_Lgn_bbs>#body", "selectcolor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("#e9ecf0ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "background", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#e9ecf0ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#e9ecf0ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground2", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#f0f0f0ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "background", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("60");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "trackbarsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_background("#cdcdcdff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "background", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_color("#999999");
    this._addCss("Grid.grd_Lgn_bbs", "color", obj, ["normal"]);
    this._addCss("PopupMenu", "color", obj, ["disabled"]);
    this._addCss("Radio", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellbackground2", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#999999");
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellcolor", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_color("#999999");
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellcolor2", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Grid.grd_Lgn_bbs>#body", "cellline", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_font("underline 9 dotum");
    this._addCss("Grid.grd_Lgn_bbs>#body", "selectfont", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("GroupBox", "titlebackground", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("GroupBox", "titlegradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("GroupBox", "titleimage", obj, ["normal"]);

    obj = new nexacro.Style_align("");
    this._addCss("GroupBox", "titleimagealign", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("GroupBox", "titlepadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#5d5858ff","","","0","0","0","0","true");
    this._addCss("ListBox", "itembackground", obj, ["focused", "selected"]);

    obj = new nexacro.Style_value("#ffffff00");
    this._addCss("MaskEdit", "selectbackground", obj, ["normal"]);
    this._addCss("TextArea", "selectbackground", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("ImageViewer", "background", obj, ["disabled"]);

    obj = new nexacro.Style_value("");
    this._addCss("Menu", "autohotkey", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/mnu_TF_checkbox.png')");
    this._addCss("Menu", "checkboximage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/mnu_TF_expand.png')");
    this._addCss("Menu", "expandimage", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #ffffffff 0,100 #f6f4f4ff");
    this._addCss("Menu", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Menu", "itemalign", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "itemalign", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/mnu_TF_bgImg.png","","0","0","100","50","true");
    this._addCss("Menu", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","#808080ff","");
    this._addCss("Menu", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 32 0 32");
    this._addCss("Menu", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#5d5858ff","","","0","0","0","0","true");
    this._addCss("Menu", "popupbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#393838ff","");
    this._addCss("Menu", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu", "popupbordertype", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "popupbordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#cfcfcfff");
    this._addCss("Menu", "popupcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Menu", "popupfont", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "popupfont", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Menu", "popupitembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#393838ff","","0","none","","");
    this._addCss("Menu", "popupitemborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu", "popupitembordertype", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "popupitembordertype", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Menu", "popupitemgradation", obj, ["normal"]);

    obj = new nexacro.Style_value("30");
    this._addCss("Menu", "popupitemheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 3 0 8");
    this._addCss("Menu", "popupitempadding", obj, ["normal"]);
    this._addCss("Menu.meu_WF_btn", "popupitempadding", obj, ["normal"]);

    obj = new nexacro.Style_padding("");
    this._addCss("Menu", "popuppadding", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Menu", "popuptype", obj, ["normal"]);
    this._addCss("PopupMenu", "popuptype", obj, ["normal"]);

    obj = new nexacro.Style_font("underline bold 10 Gulim");
    this._addCss("Menu", "font", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_background("#413c3cff","","","0","0","0","0","true");
    this._addCss("Menu", "popupitembackground", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Menu", "popupcolor", obj, ["mouseover", "focused", "selected"]);

    obj = new nexacro.Style_color("#9a080aff");
    this._addCss("Menu", "color", obj, ["selected"]);

    obj = new nexacro.Style_font("bold 9 Dotum");
    this._addCss("Menu", "popupfont", obj, ["selected"]);

    obj = new nexacro.Style_color("#747474ff");
    this._addCss("Menu", "popupcolor", obj, ["disabled"]);

    obj = new nexacro.Style_background("#e7e5e5ff","","","0","0","0","0","true");
    this._addCss("Menu>#incbutton", "background", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "background", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "background", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c0bbbbff","");
    this._addCss("Menu>#incbutton", "border", obj, ["normal"]);
    this._addCss("Menu>#decbutton", "border", obj, ["normal"]);
    this._addCss("PopupMenu>#incbutton", "border", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_hinc_N.png')");
    this._addCss("Menu>#incbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #fbfbfa 100,0 #deded7");
    this._addCss("Menu>#decbutton", "gradation", obj, ["normal"]);
    this._addCss("PopupMenu>#decbutton", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/btn_hdec_N.png')");
    this._addCss("Menu>#decbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Menu.meu_WF_btn", "popupbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#ccccccff","");
    this._addCss("Menu.meu_WF_btn", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_color("#555555ff");
    this._addCss("Menu.meu_WF_btn", "popupcolor", obj, ["normal"]);

    obj = new nexacro.Style_background("#f1f1edff","","","0","0","0","0","true");
    this._addCss("Menu.meu_WF_btn", "popupitembackground", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ccccccff","","0","none","","");
    this._addCss("Menu.meu_WF_btn", "popupitemborder", obj, ["normal"]);

    obj = new nexacro.Style_value("26");
    this._addCss("Menu.meu_WF_btn", "popupitemheight", obj, ["normal"]);

    obj = new nexacro.Style_shadow("0");
    this._addCss("Menu.meu_WF_btn", "shadow", obj, ["normal"]);

    obj = new nexacro.Style_color("#3d3d46ff");
    this._addCss("Menu.meu_WF_btn", "popupcolor", obj, ["mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_border("1","solid","#b1b1b1ff","");
    this._addCss("PopupDiv", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#d5d8deff","");
    this._addCss("PopupMenu", "border", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_menu_check.png')");
    this._addCss("PopupMenu", "checkboximage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/ico_expand_N.png')");
    this._addCss("PopupMenu", "expandimage", obj, ["normal"]);

    obj = new nexacro.Style_background("#888fa2ff","","","0","0","0","0","true");
    this._addCss("PopupMenu", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#d5d8deff","","0","none","","");
    this._addCss("PopupMenu", "itemborder", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("PopupMenu", "itembordertype", obj, ["normal"]);

    obj = new nexacro.Style_padding("1 2 1 5");
    this._addCss("PopupMenu", "itempadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#384c84");
    this._addCss("PopupMenu", "color", obj, ["mouseover"]);

    obj = new nexacro.Style_background("#edeff2ff","","","0","0","0","0","true");
    this._addCss("PopupMenu", "itembackground", obj, ["mouseover"]);

    obj = new nexacro.Style_value("URL('theme://images/icon_Pmenu_incbtn.png')");
    this._addCss("PopupMenu>#incbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_background("#f3f1f1ff","","","0","0","0","0","true");
    this._addCss("PopupMenu>#incbutton", "background", obj, ["pushed", "focused", "selected"]);
    this._addCss("PopupMenu>#decbutton", "background", obj, ["pushed", "focused", "selected"]);

    obj = new nexacro.Style_border("1","solid","#8e8b8bff","");
    this._addCss("PopupMenu>#incbutton", "border", obj, ["pushed", "focused", "selected"]);
    this._addCss("PopupMenu>#decbutton", "border", obj, ["pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('theme://images/icon_Pmenu_decbtn.png')");
    this._addCss("PopupMenu>#decbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_background("#6a6c6dff","","","0","0","0","0","true");
    this._addCss("ProgressBar", "background", obj, ["normal"]);
    this._addCss("ProgressBarControl", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#6a6c6dff","");
    this._addCss("ProgressBar", "border", obj, ["normal"]);
    this._addCss("ProgressBarControl", "border", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::img_progress.png')");
    this._addCss("ProgressBar", "progressimage", obj, ["normal"]);
    this._addCss("ProgressBarControl", "progressimage", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::img_progress_D.png')");
    this._addCss("ProgressBar", "progressimage", obj, ["disabled"]);
    this._addCss("ProgressBarControl", "progressimage", obj, ["disabled"]);

    obj = new nexacro.Style_value("1 solid #bfbfbfff");
    this._addCss("Radio", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_radio.png')");
    this._addCss("Radio", "buttonimage", obj, ["normal"]);
    this._addCss("StepControl", "buttonimage", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Radio", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("");
    this._addCss("Radio", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_padding("");
    this._addCss("Radio", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #000000ff");
    this._addCss("Radio", "buttonborder", obj, ["mouseover"]);

    obj = new nexacro.Style_value("1 solid #d5d5d5ff");
    this._addCss("Radio", "buttonborder", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_radio_D.png')");
    this._addCss("Radio", "buttonimage", obj, ["disabled"]);

    obj = new nexacro.Style_color("#777777ff");
    this._addCss("Static", "color", obj, ["disabled"]);
    this._addCss("Static.sta_WF_navi", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#f5f5f5ff","","","0","0","0","0","true");
    this._addCss("Static.sta_GA_title", "background", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c6c6c6ff","","1","solid","#c6c6c6ff","","1","solid","#c6c6c6ff","","1","solid","#c6c6c6ff","");
    this._addCss("Static.sta_GA_title", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#373a3e");
    this._addCss("Static.sta_GA_title", "color", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 10 Verdana, malgun gothic");
    this._addCss("Static.sta_GA_title", "font", obj, ["normal"]);
    this._addCss("Static.sta_GA_label", "font", obj, ["normal"]);
    this._addCss("Static.sta_GA_des", "font", obj, ["normal"]);
    this._addCss("Static.sta_GA_dessub", "font", obj, ["normal"]);
    this._addCss("Static.sta_GA_detail", "font", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c6cbceff","");
    this._addCss("Static.sta_GA_label", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#546687ff");
    this._addCss("Static.sta_GA_des", "color", obj, ["normal"]);
    this._addCss("Static.sta_GA_detail", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_sta_des.png","","0","0","0","50","true");
    this._addCss("Static.sta_GA_des", "background", obj, ["normal"]);

    obj = new nexacro.Style_color("#ea5810ff");
    this._addCss("Static.sta_GA_dessub", "color", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 14");
    this._addCss("Static.sta_GA_dessub", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_sta_dessub.png","","0","0","0","50","true");
    this._addCss("Static.sta_GA_dessub", "background", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 9");
    this._addCss("Static.sta_GA_detail", "padding", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/img_sta_detail.png","","0","0","0","50","true");
    this._addCss("Static.sta_GA_detail", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f4f4f6ff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_stext", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 10 dotum");
    this._addCss("Static.sta_WF_title", "font", obj, ["normal"]);

    obj = new nexacro.Style_color("#b3b3b3");
    this._addCss("Static.sta_WF_navi", "color", obj, ["normal"]);

    obj = new nexacro.Style_color("#ff5a00ff");
    this._addCss("Static.sta_WF_label_duty", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "color", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#f6f6f6ff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_head", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_head2", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty2", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg2", "background", obj, ["normal"]);
    this._addCss("Static.sta_WF_paging", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#e6e6e6ff","","0","none","","");
    this._addCss("Static.sta_WF_head", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_head_duty", "border", obj, ["normal"]);
    this._addCss("Static.sta_WF_bg", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#888888ff","","0","none","","","1","solid","#888888ff","","0","none","","");
    this._addCss("Static.sta_WF_bg2", "border", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#e6e6e6ff","","0","none","","","1","solid","#e6e6e6ff","","0","none","","");
    this._addCss("Static.sta_WF_paging", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#f5f9fcff","","","0","0","0","0","true");
    this._addCss("Static.sta_WF_sum", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#e6e6e6ff","","0","none","","","1","solid","","");
    this._addCss("Static.sta_WF_sum", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#f8f8f8ff","","","0","0","0","0","true");
    this._addCss("Static.sta_search_l", "background", obj, ["normal"]);
    this._addCss("Static.sta_search_t", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("4","solid","#e7e7e8ff","");
    this._addCss("Static.sta_search_l", "border", obj, ["normal"]);
    this._addCss("Static.sta_search_t", "border", obj, ["normal"]);

    obj = new nexacro.Style_font("8 Gulim");
    this._addCss("Static.sta_TF_user", "font", obj, ["normal"]);
    this._addCss("Static.sta_TF_name", "font", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_font("underline 8 Gulim");
    this._addCss("Static.sta_TF_name", "font", obj, ["mouseover"]);

    obj = new nexacro.Style_gradation("linear 0,0 #c1171dff 0,100 #b90f13ff");
    this._addCss("Static.sta_TF_titleline", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/sta_TF_logo.png","","0","0","0","0","true");
    this._addCss("Static.sta_TF_logo", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f9f9f9ff","","","0","0","0","0","true");
    this._addCss("Static.sta_TF_topbg", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#dededfff","","0","none","","");
    this._addCss("Static.sta_TF_topbg", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#444444ff");
    this._addCss("Static.sta_TF_welcome", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#c6c6c6ff","","","0","0","0","0","true");
    this._addCss("Static.sta_TF_line", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","theme://images/sta_TF_mnubg.png","repeat-x","0","0","0","0","true");
    this._addCss("Static.sta_TF_mnubg", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#ecedf1ff","","","0","0","0","0","true");
    this._addCss("Static.sta_MF_bg", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#e4e4ecff","","0","none","","","0","solid","#e31d26ff","","0","none","","");
    this._addCss("Static.sta_MF_bg", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#ed1c24ff","","","0","0","0","0","true");
    this._addCss("Static.sta_Lgn_new", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 7 dotum");
    this._addCss("Static.sta_Lgn_new", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::ptl_myinfo_back.jpg","","0","0","0","50","true");
    this._addCss("Static.ptl_sta_WF_myinfo", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #868686ff");
    this._addCss("StepControl", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("round","7","7","true","true","true","true");
    this._addCss("StepControl", "buttonbordertype", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #fdfdfdff 0,100 #f0f0f0ff");
    this._addCss("StepControl", "buttongradation", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 5 0 5");
    this._addCss("StepControl", "buttonpadding", obj, ["normal"]);

    obj = new nexacro.Style_color("#3f3f3f");
    this._addCss("StepControl", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("right");
    this._addCss("Spin", "buttonalign", obj, ["normal"]);

    obj = new nexacro.Style_value("12");
    this._addCss("Spin", "buttonsize", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#888888ff","","0","solid","","","0","solid","","","0","solid","","");
    this._addCss("Tab", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#f6f6f6ff","","","0","0","0","0","true");
    this._addCss("Tab", "buttonbackground", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #e4e4e4ff");
    this._addCss("Tab", "buttonborder", obj, ["normal"]);

    obj = new nexacro.Style_padding("5 20 5 20");
    this._addCss("Tab", "buttonpadding", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#929292ff");
    this._addCss("Tab", "color", obj, ["normal"]);

    obj = new nexacro.Style_line("0","","","");
    this._addCss("Tab", "focusborder", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_font("bold 9 gulim");
    this._addCss("Tab", "font", obj, ["normal", "selected", "focused"]);
    this._addCss("Tab.tab_MDI", "font", obj, ["selected", "focused"]);

    obj = new nexacro.Style_value("");
    this._addCss("Tab", "showextrabutton", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Tab", "background", obj, ["mouseover"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "background", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("");
    this._addCss("Tab", "border", obj, ["mouseover"]);
    this._addCss("Tab.tab_MDI>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Tab.tab_MDI>#spinupbutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_background("#efefefff","","","0","0","0","0","true");
    this._addCss("Tab", "buttonbackground", obj, ["mouseover"]);

    obj = new nexacro.Style_value("");
    this._addCss("Tab", "buttonborder", obj, ["mouseover"]);

    obj = new nexacro.Style_bordertype("","0","0","true","true","true","true");
    this._addCss("Tab", "buttonbordertype", obj, ["mouseover"]);

    obj = new nexacro.Style_font("underline bold 9 gulim");
    this._addCss("Tab", "font", obj, ["mouseover"]);

    obj = new nexacro.Style_value("1 solid #888888ff");
    this._addCss("Tab", "buttonborder", obj, ["selected", "focused"]);

    obj = new nexacro.Style_padding("5 0 5 0");
    this._addCss("Tab", "buttonpadding", obj, ["selected", "focused"]);

    obj = new nexacro.Style_color("#5b5b5bff");
    this._addCss("Tab", "color", obj, ["selected", "focused"]);

    obj = new nexacro.Style_color("#9e9d9dff");
    this._addCss("Tab", "color", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabextrabtn.png')");
    this._addCss("Tab>#extrabutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspindown_N.png')");
    this._addCss("Tab>#spindownbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspindown_O.png')");
    this._addCss("Tab>#spindownbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspindown_D.png')");
    this._addCss("Tab>#spindownbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspinup_N.png')");
    this._addCss("Tab>#spinupbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspinup_O.png')");
    this._addCss("Tab>#spinupbutton", "image", obj, ["mouseover", "pushed", "focused"]);

    obj = new nexacro.Style_value("URL('theme://images/img_tabspinup_D.png')");
    this._addCss("Tab>#spinupbutton", "image", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ecedf1ff","","","0","0","0","0","true");
    this._addCss("Tab.tab_MDI", "buttonbackground", obj, ["normal"]);

    obj = new nexacro.Style_value("1 solid #e3e4eb,1 solid #dddee4,0 solid #dddee4,0 solid #dddee4");
    this._addCss("Tab.tab_MDI", "buttonborder", obj, ["normal", "mouseover", "selected", "focused", "disabled"]);

    obj = new nexacro.Style_padding("8 20 8 20");
    this._addCss("Tab.tab_MDI", "buttonpadding", obj, ["normal"]);

    obj = new nexacro.Style_padding("8 15 8 15");
    this._addCss("Tab.tab_MDI", "buttonpadding", obj, ["mouseover", "selected", "focused", "disabled"]);

    obj = new nexacro.Style_color("#6d6d6bff");
    this._addCss("Tab.tab_MDI", "color", obj, ["mouseover"]);

    obj = new nexacro.Style_color("#6f6f6fff");
    this._addCss("Tab.tab_MDI", "color", obj, ["selected", "focused"]);

    obj = new nexacro.Style_color("#8c8b8bff");
    this._addCss("Tab.tab_MDI", "color", obj, ["disabled"]);

    obj = new nexacro.Style_value("URL('theme://images/tab_MF_extrabtn.png')");
    this._addCss("Tab.tab_MDI>#extrabutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_padding("5 6 5 6");
    this._addCss("TextArea", "padding", obj, ["normal"]);

    obj = new nexacro.Style_border("2","solid","#eaeaeaff","","0","none","","","2","solid","#eaeaeaff","","0","none","","");
    this._addCss("TextArea.txt_WF_Alert", "border", obj, ["normal", "mouseover", "focused"]);

    obj = new nexacro.Style_padding("10 10 10 10");
    this._addCss("TextArea.txt_WF_Alert", "padding", obj, ["normal"]);

    obj = null;
    
//[add theme images]
  };
})();
