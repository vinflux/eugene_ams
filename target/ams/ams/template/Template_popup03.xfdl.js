﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,250,325);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_radio", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "15", "14", "140", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("스킨 변경 설정");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "40", null, "1", "0", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "9", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "70", null, "52", "28", null, "17", this);
            obj.set_taborder("122");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "127", null, "52", "28", null, "17", this);
            obj.set_taborder("123");
            obj.set_text("취소");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio00", "absolute", "18", "55", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("124");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");
            obj.set_index("1");

            obj = new Static("Static01", "absolute", "37", "101", "58", "15", null, null, this);
            obj.set_taborder("125");
            obj.set_text("다크그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer00", "absolute", "37", "57", "58", "39", null, null, this);
            obj.set_taborder("126");
            obj.set_image("URL('img::skin_darkgray.png')");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio01", "absolute", "144", "55", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("127");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");

            obj = new ImageViewer("ImageViewer01", "absolute", "163", "57", "58", "39", null, null, this);
            obj.set_taborder("128");
            obj.set_image("URL('img::skin_blue.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "163", "101", "58", "15", null, null, this);
            obj.set_taborder("129");
            obj.set_text("블루");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio02", "absolute", "18", "128", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("130");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");

            obj = new Static("Static03", "absolute", "37", "174", "58", "15", null, null, this);
            obj.set_taborder("131");
            obj.set_text("레드");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer02", "absolute", "37", "130", "58", "39", null, null, this);
            obj.set_taborder("132");
            obj.set_image("URL('img::skin_red.png')");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio03", "absolute", "144", "128", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("133");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");

            obj = new ImageViewer("ImageViewer03", "absolute", "163", "130", "58", "39", null, null, this);
            obj.set_taborder("134");
            obj.set_image("URL('img::skin_green.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "163", "174", "58", "15", null, null, this);
            obj.set_taborder("135");
            obj.set_text("그린");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio04", "absolute", "18", "201", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("136");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");

            obj = new Static("Static05", "absolute", "37", "247", "58", "15", null, null, this);
            obj.set_taborder("137");
            obj.set_text("그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer04", "absolute", "37", "203", "58", "39", null, null, this);
            obj.set_taborder("138");
            obj.set_image("URL('img::skin_gray.png')");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio05", "absolute", "144", "201", "15", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("139");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_value("2");
            obj.set_direction("vertical");

            obj = new ImageViewer("ImageViewer05", "absolute", "163", "203", "58", "39", null, null, this);
            obj.set_taborder("140");
            obj.set_image("URL('img::skin_gray.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "163", "247", "58", "15", null, null, this);
            obj.set_taborder("141");
            obj.set_text("블루");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 250, 325, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);

        };

        this.loadIncludeScript("Template_popup03.xfdl", true);

       
    };
}
)();
