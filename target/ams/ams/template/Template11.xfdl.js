﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,930,547);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/><Column id=\"Head1\" type=\"STRING\" size=\"300\"/><Column id=\"Head2\" type=\"STRING\" size=\"300\"/><Column id=\"Head3\" type=\"STRING\" size=\"300\"/><Column id=\"Head4\" type=\"DATE\" size=\"300\"/><Column id=\"Head5\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("출로 검수");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "55", "886", "81", null, null, this);
            obj.set_taborder("117");
            obj.style.set_border("3 solid #f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", null, "498", "92", "28", "396", null, this);
            obj.set_taborder("121");
            obj.set_text("운송장 출력");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", null, "101", "67", "24", "46", null, this);
            obj.set_taborder("124");
            obj.set_text("일괄 취소");
            obj.set_tooltiptext("선택");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00", "absolute", "22", "146", null, "341", "22", null, this);
            obj.set_taborder("126");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", null, "101", "42", "24", "118", null, this);
            obj.set_taborder("131");
            obj.set_text("취소");
            obj.set_tooltiptext("선택");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "143", "66", "169", "22", null, null, this);
            obj.set_taborder("132");
            this.addChild(obj.name, obj);

            obj = new Static("label03", "absolute", "43", "68", "101", "18", null, null, this);
            obj.set_taborder("133");
            obj.set_text("합포장주문코드");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "402", "66", "104", "22", null, null, this);
            obj.set_taborder("134");
            this.addChild(obj.name, obj);

            obj = new Static("label01", "absolute", "341", "68", "56", "18", null, null, this);
            obj.set_taborder("135");
            obj.set_text("화주코드");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("label02", "absolute", "535", "68", "56", "18", null, null, this);
            obj.set_taborder("136");
            obj.set_text("거래처명");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "596", "66", "104", "22", null, null, this);
            obj.set_taborder("137");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "780", "66", "104", "22", null, null, this);
            obj.set_taborder("138");
            this.addChild(obj.name, obj);

            obj = new Static("label04", "absolute", "729", "68", "46", "18", null, null, this);
            obj.set_taborder("139");
            obj.set_text("택배사");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", null, "95", "846", "1", "42", null, this);
            obj.set_taborder("140");
            obj.style.set_background("#f3f3f3ff");
            this.addChild(obj.name, obj);

            obj = new Static("label05", "absolute", "43", "105", "101", "18", null, null, this);
            obj.set_taborder("141");
            obj.set_text("?");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit04", "absolute", "143", "103", "614", "22", null, null, this);
            obj.set_taborder("142");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 930, 547, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);

        };

        this.loadIncludeScript("Template11.xfdl", true);

       
    };
}
)();
