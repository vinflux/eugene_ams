﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"256\"/><Column id=\"Head1\" type=\"STRING\" size=\"256\"/><Column id=\"Head2\" type=\"STRING\" size=\"256\"/><Column id=\"Head3\" type=\"STRING\" size=\"256\"/><Column id=\"Head4\" type=\"DATE\" size=\"256\"/><Column id=\"Head5\" type=\"STRING\" size=\"256\"/><Column id=\"Head6\" type=\"STRING\" size=\"256\"/><Column id=\"Head7\" type=\"INT\" size=\"256\"/><Column id=\"Head8\" type=\"STRING\" size=\"256\"/><Column id=\"Head9\" type=\"INT\" size=\"256\"/><Column id=\"Head10\" type=\"INT\" size=\"256\"/><Column id=\"Head11\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0009</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0010</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0011</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0012</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0013</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0014</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0015</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0016</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0017</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0018</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_grd00", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"200\"/><Column id=\"Head1\" type=\"STRING\" size=\"256\"/><Column id=\"Head2\" type=\"STRING\" size=\"256\"/><Column id=\"Head3\" type=\"STRING\" size=\"256\"/><Column id=\"Head4\" type=\"DATE\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0009</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row><Row><Col id=\"NO\">0010</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "30", "303", null, "8", "35", null, this);
            obj.set_taborder("45");
            obj.set_cssclass("btn_WF_SHide");
            this.addChild(obj.name, obj);

            obj = new Div("div_01", "absolute", "30", "10", null, "293", "35", null, this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("Button12", "absolute", null, "0", "33", "24", "0", null, this.div_01);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_01.addChild(obj.name, obj);
            obj = new Button("Button00", "absolute", null, "0", "33", "24", "37", null, this.div_01);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_01.addChild(obj.name, obj);
            obj = new Button("Button03", "absolute", null, "0", "33", "24", "74", null, this.div_01);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_01.addChild(obj.name, obj);
            obj = new Button("Button01", "absolute", null, "0", "33", "24", "111", null, this.div_01);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_01.addChild(obj.name, obj);
            obj = new Grid("Grid00", "absolute", "0", "29", null, null, "0", "28", this.div_01);
            obj.set_taborder("4");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.div_01.addChild(obj.name, obj);
            obj = new Div("Paging", "absolute", "0", null, null, "29", "0", "0", this.div_01);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("template::Paging.xfdl");
            this.div_01.addChild(obj.name, obj);

            obj = new Div("div_02", "absolute", "30", "321", null, null, "35", "26", this);
            obj.set_taborder("47");
            obj.set_scrollbars("none");
            obj.style.set_background("transparent");
            this.addChild(obj.name, obj);
            obj = new Button("Button02", "absolute", null, "0", "33", "24", "0", null, this.div_02);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_02.addChild(obj.name, obj);
            obj = new Grid("Grid01", "absolute", "0", "29", "551", null, null, "0", this.div_02);
            obj.set_taborder("1");
            obj.set_binddataset("ds_grd00");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.div_02.addChild(obj.name, obj);
            obj = new Grid("Grid02", "absolute", "613", "29", null, null, "0", "0", this.div_02);
            obj.set_taborder("2");
            obj.set_binddataset("ds_grd00");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.div_02.addChild(obj.name, obj);
            obj = new Button("Button10", "absolute", "561", "125", "42", "28", null, null, this.div_02);
            obj.set_taborder("3");
            obj.set_text(">");
            obj.style.set_font("15 dotum");
            this.div_02.addChild(obj.name, obj);
            obj = new Button("Button11", "absolute", "561", "173", "42", "28", null, null, this.div_02);
            obj.set_taborder("4");
            obj.set_text("<");
            obj.style.set_font("15 dotum");
            this.div_02.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_02);
            obj.set_taborder("5");
            obj.set_text("상세 현황");
            obj.set_cssclass("sta_WF_title");
            this.div_02.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "27", "0", "1168", "10", null, null, this);
            obj.set_taborder("48");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "27", "34", "1168", "5", null, null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "30", "311", "1168", "10", null, null, this);
            obj.set_taborder("51");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "27", "345", "1168", "5", null, null, this);
            obj.set_taborder("52");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "581", "350", "10", "251", null, null, this);
            obj.set_taborder("55");
            obj.set_text("w10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "633", "350", "10", "251", null, null, this);
            obj.set_taborder("56");
            obj.set_text("w10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static76", "absolute", "0", "0", "30", "627", null, null, this);
            obj.set_taborder("57");
            obj.set_text("w30");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "591", "474", "42", "20", null, null, this);
            obj.set_taborder("58");
            obj.set_text("H20");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "30", "601", "1176", "26", null, null, this);
            obj.set_taborder("59");
            obj.set_text("H26");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", null, "0", "35", "627", "0", null, this);
            obj.set_taborder("60");
            obj.set_text("w35");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 293, this.div_01,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_01.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_02,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("47");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.div_02.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "template::Paging.xfdl");
        };
        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Static09.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static10.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static76.addEventHandler("onclick", this.Static76_onclick, this);
            this.Static02.addEventHandler("onclick", this.Static76_onclick, this);

        };

        this.loadIncludeScript("Template03.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
