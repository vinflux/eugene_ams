﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1164,210);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"256\"/><Column id=\"Head1\" type=\"STRING\" size=\"256\"/><Column id=\"Head2\" type=\"STRING\" size=\"256\"/><Column id=\"Head3\" type=\"STRING\" size=\"256\"/><Column id=\"Head4\" type=\"DATE\" size=\"256\"/><Column id=\"Head5\" type=\"STRING\" size=\"256\"/><Column id=\"Head6\" type=\"STRING\" size=\"256\"/><Column id=\"Head7\" type=\"INT\" size=\"256\"/><Column id=\"Head8\" type=\"STRING\" size=\"256\"/><Column id=\"Head9\" type=\"INT\" size=\"256\"/><Column id=\"Head10\" type=\"INT\" size=\"256\"/><Column id=\"Head11\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0009</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0010</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0011</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0012</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0013</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0014</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0015</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0016</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0017</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row><Row><Col id=\"NO\">0018</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/><Col id=\"Head6\"/><Col id=\"Head7\"/><Col id=\"Head8\"/><Col id=\"Head9\"/><Col id=\"Head10\"/><Col id=\"Head11\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Grid("Grid00", "absolute", "0", "5", null, null, "0", "0", this);
            obj.set_taborder("24");
            obj.set_binddataset("ds_grd");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"52\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"80\"/><Column size=\"90\"/><Column size=\"100\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell text=\"NO\"/><Cell col=\"1\" text=\"Head01\"/><Cell col=\"2\" text=\"Head02\"/><Cell col=\"3\" text=\"Head03\"/><Cell col=\"4\" text=\"Head04\"/><Cell col=\"5\" text=\"Head05\"/><Cell col=\"6\" text=\"Head06\"/><Cell col=\"7\" text=\"Head07\"/><Cell col=\"8\" text=\"Head08\"/><Cell col=\"9\" text=\"Head09\"/><Cell col=\"10\" text=\"Head10\"/><Cell col=\"11\" text=\"Head11\"/></Band><Band id=\"body\"><Cell text=\"bind:NO\"/><Cell col=\"1\" text=\"bind:출발지\"/><Cell col=\"2\" text=\"bind:도착지\"/><Cell col=\"3\" text=\"bind:방문기관\"/><Cell col=\"4\" displaytype=\"date\" edittype=\"none\" text=\"bind:일자\"/><Cell col=\"5\" text=\"bind:교통유형\"/><Cell col=\"6\" text=\"bind:교통수단\"/><Cell col=\"7\" style=\"align:right middle;\" text=\"bind:교통비\"/><Cell col=\"8\" text=\"bind:항공사\"/><Cell col=\"9\" style=\"align:right middle;\" text=\"bind:마일리지\"/><Cell col=\"10\" style=\"align:right middle;\" text=\"bind:사용마일리지\"/><Cell col=\"11\" text=\"bind:미사용사유\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1164, 210, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("Template02_tab01.xfdl", true);

       
    };
}
)();
