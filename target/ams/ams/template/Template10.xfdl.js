﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("style01");
                this.set_classname("style01");
                this.set_titletext("New Form");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,710,250);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_grd", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"NO\" type=\"STRING\" size=\"300\"/><Column id=\"Head1\" type=\"STRING\" size=\"300\"/><Column id=\"Head2\" type=\"STRING\" size=\"300\"/><Column id=\"Head3\" type=\"STRING\" size=\"300\"/><Column id=\"Head4\" type=\"DATE\" size=\"300\"/><Column id=\"Head5\" type=\"STRING\" size=\"300\"/></ColumnInfo><Rows><Row><Col id=\"NO\">0001</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0002</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0003</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0004</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0005</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0006</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0007</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row><Row><Col id=\"NO\">0008</Col><Col id=\"Head1\"/><Col id=\"Head2\"/><Col id=\"Head3\"/><Col id=\"Head4\"/><Col id=\"Head5\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("로케이션별 상품");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", null, "197", "52", "28", "344", null, this);
            obj.set_taborder("121");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_text("화주코드<fc v='red'>*</fc>");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "546", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("Button03", "absolute", "288", "62", "24", "22", null, null, this);
            obj.set_taborder("160");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit01", "absolute", "148", "62", "138", "22", null, null, this);
            obj.set_taborder("161");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "142", "120", "406", "31", null, null, this);
            obj.set_taborder("196");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit13", "absolute", "148", "155", "373", "22", null, null, this);
            obj.set_taborder("198");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "142", "151", "546", "31", null, null, this);
            obj.set_taborder("199");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "151", "120", "31", null, null, this);
            obj.set_taborder("200");
            obj.set_text("최소수량");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin00", "absolute", "148", "155", "165", "22", null, null, this);
            obj.set_taborder("220");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            obj.style.set_align("left");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_text("로케이션코드<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "453", "62", "138", "22", null, null, this);
            obj.set_taborder("227");
            this.addChild(obj.name, obj);

            obj = new Button("Button04", "absolute", "593", "62", "24", "22", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "89", "120", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_text("상/저온 구분<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "141", "89", "546", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("233");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("234");
            obj.set_text("상품코드<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "453", "93", "138", "22", null, null, this);
            obj.set_taborder("235");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "593", "93", "24", "22", null, null, this);
            obj.set_taborder("236");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("237");
            obj.set_text("단위(UOM)<fc v='red'>*</fc>");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "142", "120", "546", "31", null, null, this);
            obj.set_taborder("238");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Combo("Combo04", "absolute", "148", "124", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("208");
            obj.set_innerdataset("ds_cmb_year");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");

            obj = new Static("Static11", "absolute", "327", "151", "120", "31", null, null, this);
            obj.set_taborder("239");
            obj.set_text("최대수량");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin01", "absolute", "453", "155", "165", "22", null, null, this);
            obj.set_taborder("240");
            obj.set_value("0");
            obj.style.set_padding("0 0 0 5");
            obj.style.set_align("left");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "621", "93", "57", "22", null, null, this);
            obj.set_taborder("241");
            obj.set_text("중복체크");
            obj.set_tooltiptext("선택");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "181", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 710, 250, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("New Form");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);

        };

        this.loadIncludeScript("Template10.xfdl", true);

       
    };
}
)();
