﻿//XJS=SchedulerVertical.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        if ( !JsNamespace.exist("Eco.Scheduler.Vertical")  )
        {
        	JsNamespace.declareClass("Eco.Scheduler.Vertical",{
        		/**
        		 * HashMap 생성자(constructor)
        		 * @class HashMap
        		 * @classdesc key와 value 를 묶어서 하나의 entry로 저장한다.
        		 * hasing을 사용하기 때문에 많은 양의 데이터를 검색하는데 뛰어난 성능을 보인다.<br>
        		 * key값은 중복되지 않고 value값은 중복허용.<br>
        		 * @constructor HashMap
        		*/
        		initialize: function(pDiv,eventItem)
        		{
        			this._pDiv = pDiv;
        			this._eventItem = eventItem;
        			this._comp = Eco.XComp.Factory.getXComp(pDiv, "Static");
        			this._comp._ecoObj = this;
        		},
        		updateText: function()
        		{
        			var eventItem = this._eventItem;
        			var txt = Eco.date.getMaskFormatString(eventItem.startdate, "HH:mm") + " - " + 
        					Eco.date.getMaskFormatString(eventItem.enddate, "HH:mm") + "\r\n" + 
        					eventItem.summary,
        					comp = this._comp;
        			Eco.XComp.Factory.setProperties(comp, "text", txt);
        		},
        		draw: function(stylestr,isOnlyStyle,x,y,width,height)
        		{
        			var eventItem = this._eventItem,
        				comp = this._comp,
        				factory = Eco.XComp.Factory;

        			if ( isOnlyStyle && comp )
        			{
        				factory.setProperties(comp, "style", stylestr); 
        			}
        			else
        			{
        				var txt = Eco.date.getMaskFormatString(eventItem.startdate, "HH:mm") + " - " + 
        						Eco.date.getMaskFormatString(eventItem.enddate, "HH:mm") + "\r\n" + 
        						eventItem.summary,
        					comp = this._comp;
        				//factory.setProperties(comp, "text", txt, "style", stylestr);
        				factory.setProperties(comp, "text", txt);
        				factory.setRect(comp, x, y, width, height);
        			}
        		},
        		clear : function()
        		{
        			var comp = this._comp;
        			if ( comp )
        			{
        				comp._ecoObj = null;
        				Eco.XComp.Factory.release(comp, null, true);
        			}
        			this._pDiv = null;
        			this._eventItem = null;
        		}
        	});
        }

        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
