﻿//XJS=CommUtil.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name         : commUtil.xjs                                                                                                          
        * Title        : nexacro 공통 Transaction 관련 함수 모음                                                                             
        * @desc     : XP Engine을 이용한 Transaction 처리                                                                                                                      
        * 작성자    : 유희남                                                                                                                                
        * 작성일    : 2013-04-29                                                                                                                           
        * 변경사항    :                                                                                                        
        ******************************gfn_getCommCodeTran**************************************************/ 

        this.fv_sGb;
        this.fv_dataset;
        this.fv_code;
        this.fv_sId;
        this.fv_cboNm;
        this.fv_cboV;
        this.fv_callBack;
        this.fv_frameGb = "";
        this.fv_codeCnt = 0;
        this.fv_value;
        this.fv_codeIdCnt = 0;

        /********************************************************************************
         * Null값 여부(undefined 포함, 0은 null 아님)
         *
         * @param String 검증할 값    
         * @return Boolean
         ********************************************************************************/
        this.gfn_isNull = function(value)
        {
            if (value === null || value === undefined || value === "" || value === "undefined") {
        		return true;
            } else {
        		return false;
            }
        }

        this.gfn_trimString = function(sValue)
        {
        	if(this.gfn_isNull(sValue)){
        		return "";
        	}
        	
            var retVal = new String(sValue);
            
            retVal = retVal.replace('"','');
            retVal = retVal.replace("'",'');
            retVal = retVal.replace("\\","");
            retVal = nexacro.trim(retVal);
            return retVal;
        }
        /********************************************************************************
         * Null값 아닌지 여부
         *
         * @param String 검증할 값
         * @return Boolean
         ********************************************************************************/
        this.gfn_isNotNull = function (value)
        {
        	return !this.gfn_isNull(value);
        }

        /********************************************************************************
         * 검증할 값이 Null인지 여부에 따라 지정된 값을 반환한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 검증할 값
         * @param String Null일 경우의 값
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_nvl = function (value,nullValue)
        {
            if (this.gfn_isNull(value)) {
                return nullValue;
            } else {
                return value;
            }
        }

        /********************************************************************************
         * 검증할 값이 Null일 경우에 ""(Empty-공백)을 반환한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 검증할 값
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_isNullEmpty = function (value)
        {
            if (this.gfn_isNull(value)) {
                return "";
            } else {
                return value;
            }
        }

        this.gfn_isNullZero = function (value)
        {
            if (this.gfn_isNull(value)) {
                return 0;
            } else {
                return value;
            }
        }

        /********************************************************************************
         * 이메일의 정합성 체크
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 이메일주소
         *
         * @return Boolean
         *
         ********************************************************************************/
        this.gfn_isEmail = function (value)
        {
            var expression = new RegExp(/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/);

            if (this.gfn_isNotNull(value)) {
                return expression.test(value);
            } else {
                return false;
            }
        }

        /********************************************************************************
         * 도메인의 정합성 체크
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 도메인
         *
         * @return Boolean
         *
         ********************************************************************************/
        this.gfn_isDomain = function (value)
        {
            var expression = new RegExp(/^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/);

            if (this.gfn_isNotNull(value)) {
                return expression.test(value);
            } else {
                return false;
            }
        }

        /********************************************************************************
         * 시간 정합성 체크
         *
         * @author 민경선
         * @since 2013-01-07
         * @version 1.0
         *
         * @param String 시분(1212)
         *
         * @return Boolean
         *
         *
         ********************************************************************************/
        this.gfn_isTime = function (value)
        {
            var expression;
            var returnValue = false;

            value = value.split(":").join("");

            if (nexacro.trim(value).toString().length != 4) {
                return false;
            }

            expression = new RegExp(/^([01][0-9]|2[0-3]?)[0-5][0-9]$/);

            if (this.gfn_isNotNull(value)) {
                returnValue = expression.test(value);
            } else {
                returnValue = false;
            }

            return returnValue;
        }

        /********************************************************************************
         * Object가 함수인지 확인한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 함수 명
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_isFunction = function (functionName)
        {
            //functionName은 메서드의 이름
            var objName = functionName;
            
            //this.이 포함되지 않은 경우
            if (functionName.indexOf("this.") < 0) {
                objName = "this." + functionName;
            }
            
            if (typeof ( eval ( objName ) ) == "function") {
                return true;
            } else {
                return false;
            }
        }

        /********************************************************************************
         * replace에서 정규식 없이도 replace되도록 하는 기능.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 대상 값
         * @param String 찾아 없앨 문자열
         * @param String 변경할 문자열
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_replaceAll = function (value,originalText,replaceText)
        {
            value = new String(value);
            var returnText = value;

            if (this.gfn_isNotNull(value)) {
                returnText = value.split(originalText).join(replaceText);
            }
            return returnText;
        }

        /********************************************************************************
         * 지정된 캐릭터셋 별로 byte length를 리턴한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 대상 값
         * @param String 구분 값 EUC-KR (한글을 2byte로 인식), UTF-8 (한글을 3byte로 인식)
         *
         * @return Number
         *
         ********************************************************************************/
        this.gfn_getBytes = function (value,gubun)
        {
            var inValue = value.toString();
            var length = 0;
            var step = 1;

            if (gubun == "UTF-8") {
                step = 3;
            } else if (gubun == "EUC-KR") {
                step = 2;
            }

            for (var index = 0; index < inValue.length; index++) {
                if (inValue.charCodeAt(index) > 127) {
                    length += step;
                } else {
                    length += 1;
                }
            }
            return length;
        }

        /********************************************************************************
         * 자리 수 마다 ','를 찍어준다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 숫자
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_formatComma = function (value)
        {
            var startLength = 0;
            var endLength = 0;
            var returnValue = "";

            if (value.charAt(0) == "+" || value.charAt(0) == "-") {
                returnValue += value.charAt(0);
                startLength = 1;
            }

            endLength = value.indexOf(".");
            if(endLength == -1) endLength = value.length;

            var mainDigit = value.substr(startLength, endLength - startLength);

            for (var index = 0; index < mainDigit.length; index++) {
                if (index != 0 && (mainDigit.length - index) % 3 == 0) {
                    returnValue += ",";
                }
                returnValue += mainDigit.charAt(index);
            }
            returnValue += value.substr(endLength);

            return returnValue;
        }

        /********************************************************************************
         * 숫자에 찍힌 ','를 제거한다.
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 콤마가 포함된 숫자
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_replaceComma = function (value)
        {
            return value.split(",").join("");
        }

        /********************************************************************************
         * 문자 포맷 형식 변환
         *
         * @author 조효성
         * @since 2012-11-11
         * @version 1.0
         *
         * @param String 문자열
         * @param String 문자열 포맷(#:문자, 포맷스트링("-", ",", ".") 등
         *
         * @return String
         *
         ********************************************************************************/
        this.gfn_formatMask = function (value,mask)
        {
            var returnValue = "";
            var sUnit;

            if (this.gfn_isNull(value)) {
                return this.gfn_isNullEmpty(value);
            }

            for (var index = 0; index < mask.length; index++) {
                var str = mask.substr(index, 1);

                if (str == "#") {
                    returnValue += value.substr(0, 1);
                    value = value.substr(1);
                } else {
                    returnValue += str;
                }
            }

            return returnValue;
        }

        /**
         * @class 문자열이 숫자형식에 맞으면 숫자값을 반환
         * @param sNum 체크할 문자열
         * @return int
         */  
        this.gfn_getNum = function(sNum)
        {
            if (nexacro.isNumeric(sNum)){
                return parseInt( sNum );
            } else {
                0;
            }
        }

        /*
         * Function Name : gfn_isNum2(소수점 까지 체크)
         * Description   : Check whether the correct numeric format strings
         * Parameter     : Input String to check the status for number
         * Return        : If the correct number format return true, If not in correct number format return false
         * Example       : fn_checkNumber("-1234.56");
         */
        this.gfn_isNum2 = function(sNum)
        {
            var sChar;
            var nCnt = 0;
            var bRtn;
            if(typeof(sNum) != "stirng"){
                sNum = new String(sNum);
            }

            for (var i = 0; i < sNum.length; i++) 
            {
                sChar = sNum.charAt(i);

                if (i == 0 && (sChar == "+" || sChar == "-" )) 
                {
                    bRtn = true;
                } 
                else if (sChar >= "0" && sChar <= "9") 
                {
                    bRtn = true;
                } 
                else if (sChar == ".") 
                {
                    nCnt++;
                    if (nCnt > 1)    
                    {
                        bRtn = false;
                        break;
                    }
                } 
                else 
                {
                    bRtn = false;
                    break;
                }
            }
            
            return bRtn;
        }

        /**
         * @class 스트링의 자릿수를 Byte 단위로 환산하여 알려준다 영문, 숫자는 1Byte이고 한글은 3Byte이다.(자/모 중에 하나만 있는 글자도 3Byte이다.)
         * @param sValue(스트링)
         * @return 문자열의 byte 길이
         */  
        this.gfn_getByteLength = function(sValue) 
        {
            var byteLength = 0;
            if (this.gfn_isNull(sValue)){
                return 0;
            }
            var c;
            for (var i=0; i<sValue.length; i++){
                c = escape(sValue.charAt(i));
                if (c.length == 1){ // when English then 1byte
                    byteLength++;
                } else if (c.indexOf("%u") != -1){ // when Korean then 3byte
                    byteLength += 2;                 // utf-8 : 3
                } else if (c.indexOf("%") != -1){     // else 3byte
                    byteLength += c.length/3;
                }
            }
            return byteLength;
        }

        /**
         * @class 스트링자릿수를 체크하여 제한한다.
         * @param val(스트링)
         * @param nLimit - 제한글자수
         * @return 문자열의 byte 길이
         */  
        this.gfn_isMaxLengh = function(val,nLimit)
        {
            var nLength = this.gfn_getByteLength(val);
            var bRtn = (nLength > nLimit) ? false:true;

            return bRtn;
        }

        /**
         * @class 문자열의 오른쪽부분을 지정한 길이만큼 Return 한다.
         * @param val(오른부분을 얻어올 원본 문자열)
         * @param nSize - 얻어올 크기. [Default Value = 0]
         * @return String 오른쪽 부분이 얻어진 문자열.
         */  
        this.gfn_right = function (val,nSize)
        {
            var nStart = this.gfn_toString(val).length;
            var nEnd = Number(nStart) - Number(nSize);
            var rtnVal = val.substring(nStart, nEnd);
            
            return rtnVal;
        }

        /**
         * @class 문자열의 왼쪽부분을 지정한 길이만큼 Return 한다.
         * @param val(왼쪽부분을 얻어올 원본 문자열)
         * @param nSize - 얻어올 크기. [Default Value = 0]
         * @return String 왼쪽 부분이 얻어진 문자열.
         */  
        this.gfn_left = function (val,nSize)
        {
            return this.gfn_toString(val).substr(0, nSize);
        }

        /**
         * @class 입력값을 String으로 변경한다.
         * @param Val
         * @return String
         */  
        this.gfn_toString = function (val)
        {
            if (this.gfn_isNull(val)){
                return new String();
            }
            return new String(val);
        }

        // FormatString
        this.gfn_dateFormatString = function (oDate,sFormat)
        {
            if (this.gfn_isNull(sFormat)) {
                return "";
            }
            var fY = String(oDate.getFullYear());
            var fY2 = fY.substr(fY.length-2, 2);

            sFormat = sFormat.toString();
            sFormat = sFormat.split("%Y").join(String(oDate.getFullYear()));
            sFormat = sFormat.split("%y").join(fY2);
            sFormat = sFormat.split("%m").join(String(oDate.getMonth() + 1).padLeft(2, "0"));
            sFormat = sFormat.split("%d").join(String(oDate.getDate()).padLeft(2, "0"));
            sFormat = sFormat.split("%H").join(String(oDate.getHours()).padLeft(2, "0"));
            sFormat = sFormat.split("%M").join(String(oDate.getMinutes()).padLeft(2, "0"));
            sFormat = sFormat.split("%S").join(String(oDate.getSeconds()).padLeft(2, "0"));

            return sFormat;
        }

        //데이타셋에 컬럼 추가
        this.gfn_addColumn = function(objDs,sColumnNm,sColumnType)
        {
            if(this.gfn_isNull(objDs)) trace("gfn_addColumn dataset object error");
            if(this.gfn_isNull(sColumnNm)) trace("gfn_addColumn sColumnNm error");
            if(this.gfn_isNull(sColumnType)) sColumnType = "string";
            
            if(this.gfn_isNull(objDs.getColumnInfo(sColumnNm)))
            {
                objDs.addColumn(sColumnNm, sColumnType);
                objDs.applyChange();
            }
            
            return true;
        }

        /********************************************************************************
         * 배열에 indexOf 가 지원되지 않아 이에 동일한 기능에 대해서 구현
         *
         * @author 조효성
         * @since 2012-11-24
         * @version 1.0
         *
         * @param Array
         * @param String
         *
         * @return int
         *
         ********************************************************************************/
        this.gfn_arrayIndexOf = function (inArray,findString)
        {
            var result = -1;
            for (var index = 0; index < inArray.length; index++) 
            {
                if (inArray[index] == findString) 
                {
                    result = index;
                    break;
                }
            }

            return result;
        }

        this.gfn_lookup = function (p,name)
        {
            var o;
            
            while (p)
            {        
                o = p.components;
                if ( o && o[name] ) return o[name];
                
                o = p.objects;
                if ( o && o[name] ) return o[name];
                
                p = p.parent;
            }
            
            return null;
        }

        /**
        * 공통 코드 조회 서버
        * @param  sCode  : Code
                  sId    : searchId
                  ds_ret : dataset
                  cboNm  : 콤보 명
                  sGb    : 선택, ALL
                  cboV   : 콤보 초기 값
                  callBack : 호출 함수
        * @return
        */
        this.gfn_getCommCodeTran = function(sCode,searchId,ds_ret,cboNm,sGb,cboV,callBack)
        {
            this.fv_code     = sCode.split("|");
            this.fv_sId      = searchId.split("|");
            this.fv_dataset  = ds_ret.split("|");
            this.fv_cboNm    = cboNm.split("|");
            this.fv_sGb      = sGb.split("|");
            this.fv_cboV     = cboV.split("|");
            this.fv_callBack = callBack
            this.fv_codeCnt  = 0;
            this.fv_value    = "";
            
            for(var i = 0 ; i < this.fv_code.length ; i++){
        		this.fv_value += " " + this.fv_code[i];
            }
            
            this.gfn_codeSearchTran();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.gfn_codeSearchTran = function()
        {
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectCommonCode");
            
            var sSvcId   = "codeSearch"+this.fv_codeIdCnt++;
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = this.fv_dataset[this.fv_codeCnt]+"=OUT_COMMON_CODE_LIST";
            var sParam   = "searchid="+this.fv_sId[this.fv_codeCnt]
                         +" type=combo"
                         +this.fv_value
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "gfn_codeCallBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.gfn_codeCallBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
            var objDs = this[this.fv_dataset[this.fv_codeCnt]];
            var nRow = 0;
            
            if(!this.gfn_isNull(this.fv_sGb[this.fv_codeCnt])){
                nRow = objDs.insertRow(0);
                objDs.setColumn(nRow, "field1", "");
                objDs.setColumn(nRow, "field1_desc", this.fv_sGb[this.fv_codeCnt]);
            }
            
            if(!this.gfn_isNull(this.fv_cboNm[this.fv_codeCnt])){
                var objCbo = eval("this." + this.fv_cboNm[this.fv_codeCnt]);
                
                if(!this.gfn_isNull(this.fv_cboV[this.fv_codeCnt])){
                    objCbo.set_value(this.fv_cboV[this.fv_codeCnt]);
                }else{
                    objCbo.set_index(0);
                }
            }
            
            this.fv_codeCnt++;
            
            if(this.fv_sId.length == this.fv_codeCnt) {
                this.fv_codeCnt = 0;
                
                if(this.gfn_isNotNull(this.fv_callBack)) eval("this." + this.fv_callBack + "()");
                return;
            }
            
            this.gfn_codeSearchTran();
        }

        /**
        * 공통 코드 조회
        * @param  sCode  : Code
                  ds_ret : dataset
                  cboNm  : 콤보 명
                  cboV   : 콤보 초기 값
                  sGb    : 선택, ALL
        * @return
        */
        this.gfn_getCommCode = function(sCode,ds_ret,cboNm,cboV,sGb)
        {
            this.fv_code    = sCode.split("|");
            this.fv_dataset = ds_ret.split("|");
            this.fv_cboNm   = cboNm.split("|");
            this.fv_cboV    = cboV.split("|");
            this.fv_sGb     = sGb.split("|");
            this.fv_codeCnt = 0;

        	var objDs;
        	var objCbo;
        	var nRow = 0;
        	
        	for(var i = 0 ; i < this.fv_code.length ; i++){
        		application.gds_commCode.filter("hdkey=='"+ this.fv_code[i] +"'");
        		objDs = this[this.fv_dataset[i]];
        		
        		for(var j = 0 ; j < application.gds_commCode.rowcount ; j++){
        			objDs.addRow();
        			objDs.setColumn(j, "field1", application.gds_commCode.getColumn(j, "field1"));
        			objDs.setColumn(j, "field2", application.gds_commCode.getColumn(j, "field2"));
        		}
        		
        		if(this.fv_code[i] == "LOGGRPCD"){
        			var sLoggrpcd = "";
        			var sFilter = "";
        			
        			if(!this.gfn_isNull(this.gfn_getUserInfo("loggrpcd"))){
        				sLoggrpcd = this.gfn_getUserInfo("loggrpcd").split(",");
        				
        				for(var k = 0 ; k < sLoggrpcd.length ; k++){
        					if(k == 0) sFilter = "field1 == '"+sLoggrpcd[k]+"'";
        					else sFilter += " || field1 == '"+sLoggrpcd[k]+"'";
        				}
        				
        				objDs.filter(sFilter);
        			}
        		}
        		
        		if(!this.gfn_isNull(this.fv_sGb[i])){
        			nRow = objDs.insertRow(0);
        			objDs.setColumn(nRow, "field1", "");
        			objDs.setColumn(nRow, "field2", this.fv_sGb[i]);
        		}
        		
        		if(!this.gfn_isNull(this.fv_cboNm[i])){
        			objCbo = eval("this." + this.fv_cboNm[i]);
        			
        			if(!this.gfn_isNull(this.fv_cboV[i])){
        				objCbo.set_value(this.fv_cboV[i]);
        			}else{
        				objCbo.set_index(0);
        			}
        		}
        	}
        	
        	application.gds_commCode.filter("");
        }

        this.gfn_setCommon = function(sKey,sValue){
        	this.gfn_setData("COMMON", sKey, sValue);
        }

        this.gfn_setParam = function(sKey,sValue){
        	this.gfn_setData("PARAM", sKey, sValue);
        }

        this.gfn_setMap = function(sKey,sValue){
        	this.gfn_setData("MAP", sKey, sValue);
        }

        this.gfn_setUserInfo = function(sKey,sValue){
        	this.gfn_setData("USER_INFO", sKey, sValue);
        }

        this.gfn_setLang = function(sKey,sValue){
        	this.gfn_setData("LANG", sKey, sValue);
        }

        this.gfn_setData = function(sCategory,sKey,sValue)
        {
        	if(sCategory == null || sKey == null){
        		return;
        	}
        	
        	switch (sCategory.toUpperCase()) {
        		case "PARAM":
        			if(!application.gds_param.setColumn(0, sKey, sValue)){
        				application.gds_param.addColumn(sKey, "string");
        				if(application.gds_param.getRowCount() == 0){
        					application.gds_param.addRow();
        				}
        				application.gds_param.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "COMMON":
        			if(!application.gds_common.setColumn(0, sKey, sValue)){
        				if(application.gds_common.getRowCount() == 0){
        					application.gds_common.addRow();
        				}
        				application.gds_common.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "USERINFO":
        		case "USER_INFO":
        			if(!application.gds_userInfo.setColumn(0, sKey, sValue)){
        				if(application.gds_userInfo.getRowCount() == 0){
        					application.gds_userInfo.addRow();
        				}
        				application.gds_userInfo.setColumn(0, sKey, sValue);
        			}
        			break;
        			
        		case "MAP":
        			if(!application.gds_map.setColumn(0, sKey, sValue)){
        				application.gds_map.addColumn(sKey, "string");
        				if(application.gds_map.getRowCount() == 0){
        					application.gds_map.addRow();
        				}
        				application.gds_map.setColumn(0, sKey, sValue);
        			}
        			break;
        		case "LANG":
        			if(!application.gds_lang.setColumn(0, sKey, sValue)){
        				if(application.gds_lang.getRowCount() == 0){
        					application.gds_lang.addRow();
        				}				
        				application.gds_lang.setColumn(0, sKey, sValue);
        			}
        			break;			
        	}
        }

        this.gfn_getCommon = function(sKey){
        	return this.gfn_getData("COMMON", sKey);
        }

        this.gfn_getParam = function(sKey){
        	return this.gfn_getData("PARAM", sKey);
        }

        this.gfn_getMap = function(sKey){
        	return this.gfn_getData("MAP", sKey);
        }

        this.gfn_getUserInfo = function(sKey){
        	return this.gfn_getData("USER_INFO", sKey);
        }

        this.gfn_getLang = function(sKey){
        	if(sKey == null){
        		return "";
        	}
        	return this.gfn_getData("LANG", sKey.toUpperCase());
        }

        this.gfn_getData = function(sCategory,sKey){
        	var returnValue = "";
        	if(sCategory == null || sKey == null){
        		return returnValue;
        	}
        	
        	switch (sCategory.toUpperCase()) {
        		case "PARAM":
        			returnValue = application.gds_param.getColumn(0, sKey);
        			break;
        			
        		case "COMMON":
        			returnValue = application.gds_common.getColumn(0, sKey);
        			break;
        			
        		case "USERINFO":
        		case "USER_INFO":
        			returnValue = application.gds_userInfo.getColumn(0, sKey);
        			break;
        			
        		case "MAP":
        			returnValue = application.gds_map.getColumn(0, sKey);
        			break;
        			
        		case "LANG":
        			returnValue = application.gds_lang.getColumn(0, sKey);
        			break;	
        	}
        	return returnValue;
        }

        this.gfn_getActiveApp = function(){
        	var moduleNmae = "";
        	switch(application.gv_activeApp.toUpperCase()){
        		case "AMS":
        		case "ADM":
        		case "ADMIN":
        			moduleNmae = application.gv_ams;
        			break;
        		case "WM":
        		case "WMS":
        			moduleNmae = application.gv_wms;
        			break;
        		case "TM":
        		case "TMS":
        			moduleNmae = application.gv_tms;
        			break;
        		case "VIMS":
        		case "VMS":
        			moduleNmae = application.gv_vms;
        			break;	
        		case "ICOM":
        			moduleNmae = application.gv_oms;
        			break;
        		case "OMS":
        			moduleNmae = application.gv_oms;
        			break;	
        		case "MDM":
        			moduleNmae = application.gv_mdm;
        			break;	
        		case "FIS":
        			moduleNmae = application.gv_fis;
        			break;	
        		case "IBS":
        			moduleNmae = application.gv_ibs;
        			break;	
        		case "CMS":
        			moduleNmae = application.gv_cms;
        			break;
        		case "KPI": 
        			moduleNmae = application.gv_kpi;
        			break;
        		case "PTL": 
        			moduleNmae = application.gv_ptl;
        			break;									
        	}
        	return moduleNmae;
        }

        this.gfn_setCheckTime = function(){
        	var startDate = new Date();
        	return startDate.getTime();
        }

        this.gfn_getCheckTime = function(startDate){
        	var MinMilli = 1000 * 60;

        	var endDate = new Date();
        	var endTime = endDate.getTime();
        	var diff = endTime - startDate;
        	var lastTime = Math.round(diff / MinMilli);

        //	trace(" total Time : " + lastTime + " Min");
        	return lastTime;
        }

        this.gfn_getCode = function(msgKey,dsObject,sAll){
        	application.gds_commCode.filter("hdkey=='"+msgKey+"'");
        	
        	dsObject.clearData();
        	for(var i = 0 ; i < application.gds_commCode.rowcount ; i++){
        		dsObject.addRow();
        		dsObject.setColumn(i, "field1", application.gds_commCode.getColumn(i, "field1"));
        		dsObject.setColumn(i, "field2", application.gds_commCode.getColumn(i, "field2"));
        	}
        	
        	if(msgKey == "LOGGRPCD"){
        		var sLoggrpcd = "";
        		var sFilter = "";
        		
        		if(!this.gfn_isNull(this.gfn_getUserInfo("loggrpcd"))){
        			sLoggrpcd = this.gfn_getUserInfo("loggrpcd").split(",");
        			
        			for(var i = 0 ; i < sLoggrpcd.length ; i++){
        				if(i == 0) sFilter = "field1 == '"+sLoggrpcd[i]+"'";
        				else sFilter += " || field1 == '"+sLoggrpcd[i]+"'";
        			}
        			
        			dsObject.filter(sFilter);
        		}
        	}
        	
        	if(!(sAll === null || sAll === undefined)){
        		dsObject.insertRow(0);
        		dsObject.setColumn(0, "field1", "");
        		dsObject.setColumn(0, "field2", sAll);
        	}
        	
        	application.gds_commCode.filter("");
        }

        this.gfn_getAppRole = function(dsObject){
        	for(var i = 0 ; i < application.gds_rtnAppRole.rowcount ; i++){
        		dsObject.addRow();
        		dsObject.setColumn(i, "appKey", application.gds_rtnAppRole.getColumn(i, "appKey"));
        	}
        }

        this.gfn_booleanYNConvert = function(obj){
        	if(typeof(obj) == "string" ) {
        		var returnValue = false;
        		if(obj.toUpperCase() == "Y"){
        			returnValue = true;
        		}
        		return returnValue;
        	}else if(typeof(obj) == "boolean" ){
        		var returnValue = "N";
        		if(obj){
        			returnValue = "Y";
        		}
        		return returnValue;
        	}
        }

        this.gfn_getTitleType = function(gv_flag){
        	var titleType = application.gds_lang.getColumn(0, "TITLE_STATUS_INSERT");
        	if(gv_flag == "U"){
        		titleType = application.gds_lang.getColumn(0, "TITLE_STATUS_UPDATE");
        	}
        	if(titleType === undefined){
        		titleType = "";
        	}
        	return titleType;
        }
        //필수여부 체크 
        this.isValidCheckList = function(objList){
        	var returnValue = true;
        	if(objList != null && objList.length != undefined){
        		for(var i=0 ; i < objList.length ; i++){
        			if(this.isNotValid(objList[i])){
        				returnValue = false;
        			}
        		}
        	}
        	return returnValue;
        }

        this.isNotValid = function(obj){
        	return !this.isValid(obj);
        }

        this.isValid = function(obj){
        	var returnValue = false;
        	if(obj != null && obj !== undefined){
        		if(this.gfn_isNull(obj.value)){
        			this.validColorFalse(obj);
        			obj.setFocus();
        		}else{
        			this.validColorTrue(obj);
        			returnValue = true;
        		}
        	}else{
        		trace("requiredList 리스트를 확인하세요.");
        	}
        	return returnValue;
        }

        this.validColorFalse = function(obj){
        	obj.style.set_border("1 solid #ff9999ff");
        }

        this.validColorTrue = function(obj){
        	obj.style.set_border("1 solid #d5d5d5ff");
        }

        this.gfn_setLocale = function(obj){
        	if(this.gfn_getUserInfo("laKey") == "KOR") obj.set_locale("ko_KR");
        	else if(this.gfn_getUserInfo("laKey") == "ENG") obj.set_locale("en_US");
        }

        /**
        * 공통 코드 명 조회
        * @param  key       : 추가 조건 키
                  value     : 추가 조건 값
                  sValue    : 검색 코드 값
                  sSearchId : sSearchId
                  sAppkey   : sAppkey
                  beanId    : controller
                  methodnm  : method
                  objKey    : key object
                  objVal    : value object
                  grdObj    : grid objec
                  col       : 데이타 셋 컬럼 id
                  row       : row
        * @return
        */
        var gv_objKey;
        var gv_objVal;
        var gv_objDs;
        var gv_colId;
        var gv_row;
        var gv_callBack;

        this.gfn_codeSearch = function(key,value,sValue,sSearchId,sAppkey,beanId,methodnm,objKey,objVal,objDs,colId,row,sCallBack)
        {
        	application.gds_code.clearData();
        	gv_objKey = objKey;
        	gv_objVal = objVal;
        	gv_objDs = objDs;
        	gv_colId = colId;
        	gv_row = row;
        	gv_callBack = sCallBack;
        	
        	if(this.gfn_isNull(beanId)) beanId = "commonController";
        	if(this.gfn_isNull(methodnm)) methodnm = "selectCommonCode";
        	
        	this.gfn_setParam("currentPage", 1);
        	this.gfn_setParam("pagingLimit", 100);
        	this.gfn_setParam("ctkey" , "");
        	this.gfn_setCommon("BEANID"  , beanId);
        	this.gfn_setCommon("METHODNM", methodnm);
        	
        	var sKey = "";
        	if(this.gfn_isNotNull(key)){
        		var mKey = key.split("|");
        		var mValue = value.split("|");
        		
        		for(var i = 0 ; i < mKey.length ; i++){
        			sKey += " " + mKey[i] + "=" + mValue[i];
        		}
        	}
        	
            var sSvcId   = "codeSearch";
            var sSvcUrl  = sAppkey + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_code=OUT_COMMON_CODE_LIST";
            var sParam   =  "searchid="+sSearchId
                         +  sKey
                         + " value="+sValue
                         + " opval=eq"
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "gfn_callBackCode", false);
        }

        this.gfn_callBackCode = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "codeSearch"){
        		if(this.gfn_isNull(gv_objDs)){
        			if(application.gds_code.rowcount > 0){
        				gv_objVal.set_value(application.gds_code.getColumn(0, "field2"));
        				gv_objVal.set_tooltiptext(application.gds_code.getColumn(0, "field2"));
        			}else{
        				if(this.gv_code == ""){
        					this.gfn_alert(application.gv_nodataMsg);
        				}
        				
        				gv_objVal.set_value("");
        			}
        		}else{
        			if(application.gds_code.rowcount > 0){
        				if(gv_colId == "uppermename|uppermelang"){
        					gv_colId = gv_colId.split("|");
        					
        					for(var i = 0 ; i < gv_colId.length  ; i++){
        						if(gv_colId[i] == "uppermename") gv_objDs.setColumn(gv_row, gv_colId[i], application.gds_code.getColumn(0, "field2"));
        						else gv_objDs.setColumn(gv_row, gv_colId[i], application.gds_code.getColumn(0, "field5"));
        					}
        					
        // 					trace("uppermename : " + gv_objDs.getColumn(gv_row, "uppermename"));
        // 					trace("uppermelang : " + gv_objDs.getColumn(gv_row, "uppermelang"));
        				}else{
        					gv_objDs.setColumn(gv_row, gv_colId, application.gds_code.getColumn(0, "field2"));
        				}
        			}else{
        				if(this.gv_code == ""){
        					this.gfn_alert(application.gv_nodataMsg);
        				}
        				
        				if(gv_colId == "uppermename|uppermelang"){
        					gv_colId = gv_colId.split("|");
        					
        					for(var i = 0 ; i < gv_colId.length  ; i++){
        						gv_objDs.setColumn(gv_row, gv_colId[i], "");
        					}
        				}else{
        					gv_objDs.setColumn(gv_row, gv_colId, "");
        				}
        			}
        		}
        		
        		this.gv_code = "";
        		
        		if(!this.gfn_isNull(gv_callBack)) eval("this." + gv_callBack + ";");
        	}
        }

        this.gfn_setCheckMilliTime = function(){
        	var startDate = new Date();
        	return startDate.getTime();
        }

        this.gfn_getCheckMilliTime = function(startDate){
        	var endDate = new Date();
        	var endTime = endDate.getTime();
        	var diff = endTime - startDate;
        	var lastTime = Math.round(diff);

        	trace(" total Time : " + lastTime + " Microsecond");
        	return lastTime;
        }

        this.gfn_setUbiSQLID = function(datasetList,argSqlID,argDsObject,filterList){
        	var applyFilter = false;
        	if(this.gfn_isNotNull(filterList) && filterList.length > 0){
        		applyFilter = true;
        		argDsObject = this.distinctDataset(argDsObject, filterList);
        	}

        	var tempObject = {
        		sqlID:argSqlID,
        		dsObject:argDsObject
        	}
        	if(this.gfn_isNull(datasetList)){
        		return;
        	}
        	
        	datasetList.push(tempObject);
        	return datasetList;
        }

        this.distinctDataset = function(argDsObject,filterList){
        	var tempDataset = new Dataset();
        	for(var i = 0 ; i < filterList.length ; i++){
        		if(!tempDataset.setColumn(0, filterList[i], "")){
        			tempDataset.addColumn(filterList[i], "string");
        		}
        	}
        	var checkData = [];
        	var checkCount = 0;
        	for(var j = 0 ; j < argDsObject.getRowCount() ; j++){
        		var dupCheck = "";
        		for(var k = 0 ; k < filterList.length ; k++){
        			if(k != 0){
        				dupCheck+="_";
        			}
        			dupCheck += argDsObject.getColumn(j, filterList[k]);
        		}
        		if(j == 0){
        			checkData[checkCount++] = dupCheck;
        		}else{
        			var isDuplicate = false;
        			for(var l = 0 ; l < checkData.length ; l++){
        				if(checkData[l] == dupCheck){
        					isDuplicate = true;
        					break;
        				}
        			}
        			if(!isDuplicate){
        				checkData[checkCount++] = dupCheck;
        			}
        		}
        	}
        	var valueData = [];
        	for(var m = 0 ; m < checkData.length ; m++){
        		tempDataset.addRow();
        		valueData = checkData[m].split("_");
        		for(var n = 0 ; n < filterList.length ; n++){
        			if(valueData.length == filterList.length){
        				tempDataset.setColumn(m, filterList[n], valueData[n]);
        			}else{
        				if(n == (filterList.length-1)){
        					tempDataset.setColumn(m, filterList[n], "");
        				}else{
        					tempDataset.setColumn(m, filterList[n], valueData[n]);
        				}
        			}
        		}
        	}
        	return tempDataset;
        }

        this.gfn_makeJsonString = function(datasetObj,keyList)
        {
        	if(this.gfn_isNull(datasetObj)){
        		return;
        	} 
        	if(keyList === undefined || keyList == null){
        		keyList = [];
        		var sColId = "";
        		for(var colIndex = 0 ; colIndex < datasetObj.colcount ; colIndex++){
        			sColId = datasetObj.getColID(colIndex);
        			keyList.push(sColId);
        		}
        	}
        	var jsonString = "[";
        	
        	for(var i = 0 ; i < datasetObj.rowcount ; i++){
        		if(i != 0){
        			jsonString += ",";
        		}
        		jsonString += "{";
        			for(j = 0 ; j < keyList.length ; j++){
        				if(j != 0 ){
        					jsonString += ",";
        				}
        				jsonString += "'"+keyList[j]+"'" +":"+"'"+datasetObj.getColumn(i, keyList[j])+"'";
        			}
        		
        		
        		jsonString += "}";
        	}
        	jsonString += "]";
        	return jsonString;
        }
        /**
        * 소수점 설정
        * @param  compObj : 컴포넌트 string
        * @return
        */
        this.gfn_decimalPointSet = function(compObj)
        {
        	if(this.gfn_isNull(compObj)) return;
        	
        	var maskValue = "";
        	var maskChgValue = "";
        	var decimalPointValue = ".";
        	var decimalPoint = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "NUMBEROFDIGITBELOWDECIMALPOINT"), "value1");
        	if(this.gfn_isNull(decimalPoint)) decimalPoint = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "NUMBEROFDIGITBELOWDECIMALPOINT"), "defaultvalue");
        	if(this.gfn_isNull(decimalPoint)) decimalPoint = 0;
        	
        	if(decimalPoint > 0){
        		for(var i = 0 ; i < decimalPoint ; i++){
        			decimalPointValue += "0";
        		}
        		
        		for(var i = 1 ; i < 16 - decimalPoint ; i++){
        			if(i == 1) maskValue = "0";
        			else maskValue = "#" + maskValue;
        			
        			if(i == 15 - decimalPoint) maskChgValue = "#" + maskChgValue;
        			else{
        				if(i == 1) maskChgValue = "0";
        				else maskChgValue = "#" + maskChgValue;
        				
        				if((i % 3) == 0) maskChgValue = "," + maskChgValue;
        			}
        		}
        	}else{
        		decimalPointValue = "";
        		maskChgValue = "###,###,###,###,##0";
        	}
        //	trace("maskChgValue : " + maskChgValue);
        	
        	var sCompObj = "";
        	compObj = compObj.split("|");
        	
        	for(var i = 0 ; i < compObj.length ; i++){
        //		trace(compObj[i] + " : " + this.components[compObj[i]]);
        		if(this.gfn_isNull(this.components[compObj[i]])) continue;
        		sCompObj = this.components[compObj[i]];
        		
        		sCompObj.set_mask(maskChgValue + decimalPointValue);
        //		sCompObj.set_mask("###,###,###,###,##0" + decimalPointValue);
        		sCompObj.set_autoselect(true);
        		sCompObj.set_limitbymask("both");
        	}
        }

        this.gfn_selectAfter = function(gridObj,thisObj)
        {
        	if(this.gfn_isNull(gridObj)){
        		return;
        	}
        	var dsSource = this.lookup(gridObj.binddataset);

        	gridObj.set_enableredraw(false);
        	dsSource.set_enableevent(false);
        		
        	if(dsSource.rowcount > 0){
        		dsSource.set_rowposition(0);
        		if(this.gfn_isNotNull(thisObj.parent) && this.gfn_isNotNull(thisObj.parent.fn_searchAfter)){
        			thisObj.parent.fn_searchAfter(); // 검색조건 숨김
        		}
        		dsSource.addColumn("CHK");
        	}else{
        		gridObj.set_nodatatext(application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage"));
        		dsSource.setConstColumn("HCHK","0");
        	}

        	this.gfn_setParam("sortValue", "");
        	this.gfn_setParam("colName", "");
        	
        	this.gfn_constDsSet(gridObj);
        	
        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	if(this.gfn_isNull(checkList) || checkList.length == 0 ){
        		dsSource.setConstColumn("HCHK","0");
        	}

        	dsSource.set_enableevent(true);
        	gridObj.set_enableredraw(true);	
        }

        this.gfn_setPage = function(divObj,datasetObj)
        {
        	if(this.gfn_isNull(divObj)){
        		return;
        	}
        	if(this.gfn_isNull(datasetObj)){
        		return;
        	}
        	divObj.fn_pageSet(datasetObj.getColumn(0, "pagingLimit"), datasetObj.getColumn(0, "currentPage"), datasetObj.getColumn(0, "COUNT"));
        }

        this.gfn_canColumnChange = function(obj,e)
        {
        	if(this.gfn_isNull(obj)){
        		return;
        	}
        	if(this.gfn_isNull(e)){
        		return;
        	}
        	
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        this.gfn_onColumnChanged = function(obj,e)
        {
        	if(this.gfn_isNull(obj)){
        		return;
        	}
        	if(this.gfn_isNull(e)){
        		return;
        	}
        	
        	if(e.columnid == "CHK") obj.set_updatecontrol(true);
        	else if(e.columnid != "STATUS") this.gfn_statusChk(obj, e.row);
        }

        this.gfn_soundPlay = function (type)  { 

        	var soundManager = new parent.SoundManager();
        	
        	var soundUrl = '';
        	var baseSound = './../ams/resources/soundmanager/vinfluxsound/';
        	if (type == 'success' ) { 		soundUrl = baseSound + 'successSound.wav';
        	} else if (type == 'error' ) { 	soundUrl = baseSound + 'errorSound.wav';
        	} else if (type == 'scan' ) { 	soundUrl = baseSound + 'scanSound.wav';
        	}

        	soundManager.setup({
        		url:  './../sys/resources/soundmanager/swf/',
        		preferFlash: false, // optional: use 100% HTML5 mode where available
        		onready: function() {
        			var mySound = soundManager.createSound({
        				id: 'aSound',
        				url: soundUrl
        			});
        			mySound.play();
        		},
        		ontimeout: function() {
        			// Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
        		}
        	});		
        }

        

        
         /*====================================================================
          * 기능 :   전화번호 자릿수 체크
          * 인수 :   STRING
          * Return :  true / false
         =====================================================================*/
        this.gfn_telNoValidation = function(telNo) {

          var sTelNo = telNo.toString();
          
          if (this.gfn_isNull(sTelNo)) {
           return true;
          }
          
          
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo," ","");
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo,"-","");
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo,")","");

          
          var sTel1 = sTelNo.substr(0,2);
          var sTel2 = "";
          var nTel2Len = 0;
          
          if (sTel1 == "02") {
           sTel2 = sTelNo.substr(2);
           nTel2Len = Eco.string.getLength(sTel2);
             
           if (nTel2Len < 7 || nTel2Len > 8) {
            return false;
           }
          } else {
           sTel1 = sTelNo.substr(0,3);
           
           // 핸드폰/일반전화 구분
          if (Eco.decode(sTel1,"010",true,"011",true,"016",true,"017",true,"018",true,"019",true,false)) {
            sTel2 = sTelNo.substr(3);
            nTel2Len = Eco.string.getLength(sTel2);
            
            if (sTel1 == "010" && nTel2Len != 8) {
             return false;
            } else if (nTel2Len < 7 || nTel2Len > 8) {
             return false;
            }
           } else {
            sTel2 = sTelNo.substr(3);
            nTel2Len = Eco.string.getLength(sTel2);
            
            if (nTel2Len < 7 || nTel2Len > 8) {
             return false;
            }
           }
          }
          
          return true;
         };
         
        /*====================================================================
          * 기능 :   전화번호 포맷 지정
          * 인수 :   STRING
          * Return :  포맷형식
         =====================================================================*/

        gfn_telNoFormat = function(telNo) {

          var sTelNo = telNo.toString();
        //   if (this.gfn_isNull(sTelNo)) 
        //   {
        //    return "##########";
        //   }
          if (sTelNo === null || sTelNo === undefined || sTelNo === "" || sTelNo === "undefined") 
          {
        	return "";
          }
        	
          
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo," ","");
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo,"-","");
          sTelNo = Eco.string.replaceIgnoreCase(sTelNo,")","");
          
          var sTel1 = sTelNo.substr(0,2);
          var sTel2 = "";
          var nTel2Len = 0;
          
          if (sTel1 == "02") {
        	   sTel2 = sTelNo.substr(2);
        	   nTel2Len = Eco.string.getLength(sTel2);
        	   //서울의 경우
        		if(nTel2Len == 7)
        		{
        			return "##-###-####";
        		}else if (nTel2Len == 8)
        		{
        			return "##-####-####";
        		}else
        		{
        			return "";
        		}
        	
          } else {
        		sTel1 = sTelNo.substr(0,3);
        		sTel2 = sTelNo.substr(3);
        		nTel2Len = Eco.string.getLength(sTel2);
        		if (nTel2Len == 7)
        		{
        			 return "###-###-####";
        		}else if (nTel2Len == 8)
        		{
        			 return "###-####-####";
        		} else
        		{
        			return "";
        		}
        	}
         };
         

         /*====================================================================
          * 기능 :   삭제시 이미지 DELETE, NORMAL 값 변경경(이미지 처리)
          * 인수 :   STRING
          * Return :  
         =====================================================================*/
        this.gfn_deleteGrid = function(obj) 
        {
        	var nRow = obj.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			if(obj.getRowType(nRow) == "2"){
        				obj.deleteRow(nRow);
        				nRow = obj.findRowExpr("CHK == '1'");
        			}else{
        				if(obj.getColumn(nRow, "STATUS") == 'D')
        				{
        					obj.set_updatecontrol(false);
        					obj.setColumn(nRow, "STATUS", "N");
        					obj.setRowType(nRow, Dataset.ROWTYPE_NORMAL);  //normal 상태로 변경
        					nRow = obj.findRowExpr("CHK == '1'", nRow+1);
        					obj.set_updatecontrol(true);
        				}else
        				{
        					obj.setColumn(nRow, "STATUS", "D");
        					nRow = obj.findRowExpr("CHK == '1'", nRow+1);
        				}
        			}
        		}
        	}
          
         };
         
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
