﻿//XJS=CommExcel.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /******************************************************************************            
        * Name  		: commExcel.xjs
        * Title      	: nexacro 공통 Excel (Server Side) 관련 함수 모음
        * @desc      	: Server Output Stream 을 이용한 Excel down
        * 작성자    	: cch
        * 작성일    	:
        * 변경사항  	:
        ********************************************************************************/ 

        /*******************************************************************************
        * Server Side Excel download Logic 
        * sUrl : excel download url 
        * oGridObj : 엑셀 다운을 위한 그리드 Object 
        * oView : 엑셀 다운로드 화면 
        * oParamValue : 추가할 Parameter 
        ********************************************************************************/
        this.gfn_excelDownload =  function(sUrl,oGridObj,oView,oParamValue)
        {

        	if(system.navigatorname == "nexacro"){
        		this.gfn_alert("MSG_RUN_BROWSER");
        		return;
        	}
        	
        	var objForm = document.createElement("form"); // form 엘리멘트 생성

        	// Url Setting 
        	var sBaseUrl = application.services.get_item("svc").url;
        	var sActionUrl = sBaseUrl + this.gfn_getActiveApp().toLowerCase() + "/" + sUrl;
        	var objInput1 = document.createElement("input");
        	
        	// CommandMap Base Struncture 
        	var oValue = {
        		PARAM : oParamValue , 
        		MAP: {},
        		LIST: {
        			EXCEL_FORMAT : []
        		},
        		COMMON : { 
        			USER_INFO : {} 
        		},
        		SEARCHLIST : []
        	};

        	// file name setting 
        	var sFileName = oView.parent.gv_menuNm;
        	var sDateTime = "" + this.gfn_toDateTime();
            if(!this.gfn_isNull(sFileName)){
                sFileName = "[" + sFileName + "]_[" + sDateTime.substr(0, 8) + "]_[" + sDateTime.substr(8, 6) + "]";
            }else{
                sFileName = this.gfn_today();
            }
        	oValue.PARAM.EXCEL_FILENAME = sFileName;
        	
        	// wms default center code setting 
        	trace("application.gv_activeApp : "+application.gv_activeApp);
        	if (application.gv_activeApp == "WMS" ) { 
        		oValue.PARAM.ctkey =  this.gfn_getUserInfo("ctKey");
        	}

        	// CommandMap Setting ; 
        	var gdsCommonCount = application.gds_common.getColCount();
        	for (var i=0 ; gdsCommonCount > i ; i++) { 
        		var obj_RowValue = application.gds_common.getColumn(0,i);
        		var obj_RowInfoId = application.gds_common.getColumnInfo(i).id;
        		oValue.COMMON[obj_RowInfoId] = obj_RowValue;
        	}

        	// Common.UserInfo
        	var gdsUserInfoCount = application.gds_userInfo.getColCount();
        	for (var i=0 ; gdsUserInfoCount > i ; i++) { 
        		var obj_RowValue = application.gds_userInfo.getColumn(0,i);
        		var obj_RowInfoId = application.gds_userInfo.getColumnInfo(i).id;
        		if (obj_RowValue === undefined ) { 
        			obj_RowValue = "";
        		}
        		oValue.COMMON.USER_INFO[obj_RowInfoId] = obj_RowValue;
        	}
         
        	// Grid Format
        	var gridCellCount = oGridObj.getCellCount("head");
        	var dataColumnCnt = oGridObj.getCellCount("body");
        	
        	for (var i=1 ; gridCellCount > i ; i++) { 
        		var str_RowText = oGridObj.getCellProperty("head", i , "text");
        		var str_RowDataField = oGridObj.getCellProperty("body", i , "text");
        		var str_TxtLang = this.gfn_getLang(str_RowText.replace("bind:H","") );
        		var FILED_NAME3 = null;
        		if ( application.gv_activeApp  == "PTL"		 ) { 
        		    if( this.gfn_isNull(str_RowText) ||  str_RowText =="bind:HCHK" ) {continue;}
        			FILED_NAME3="";
        			FILED_NAME3 = oGridObj.getCellProperty("Body", i , "align")+"" ; 
        			
        			var displaytype = oGridObj.getCellProperty("Body", i , "displaytype")+"" ; 
        			var mask = oGridObj.getCellProperty("Body", i , "mask")+"" ; 
        			var pushArr= {
        				"DATA_FILED" :  (this.gfn_isNull(str_RowDataField))?"":str_RowDataField.replace("bind:",""), 
        				"FILED_NAME1" : str_TxtLang, 
        				"FILED_NAME2" : null , 
        				"FILED_NAME3" : FILED_NAME3, 
        				"FILED_NAME4" : displaytype, 
        				"FILED_NAME5" : mask, 
        			}; 
        			oValue.LIST.EXCEL_FORMAT.push(pushArr); 
        		
        		} else if(application.gv_activeApp  == "WMS" && oParamValue.flag == "mergeheader") {
        			// WMS 모듈, 헤더가 multirow인 경우
        			// oParamValue의 flag value가 mergeheader인 경우
        			var row = oGridObj.getCellProperty("Head", i, "row");
        			
        			oValue.LIST.EXCEL_FORMAT.push({
        				"DATA_FILED"  : (this.gfn_isNull(str_RowDataField))?"":str_RowDataField.replace("bind:",""), 
        				"FIELD_NAME1" : str_TxtLang,
        				"COL" : oGridObj.getCellProperty("Head", i, "col"), // colIdx
        				"ROW" : oGridObj.getCellProperty("Head", i, "row"),	// rowIdx
        				"COL_CNT"  : oGridObj.getCellProperty("Head", i, "colspan"), // colspan
        				"ROW_CNT"  : oGridObj.getCellProperty("Head", i, "rowspan")	 // rowspan
        			}); 
        		} else{
        		  if(str_RowText != "bind:HCHK"){
        			oValue.LIST.EXCEL_FORMAT.push({
        				"DATA_FILED" :  (this.gfn_isNull(str_RowDataField))?"":str_RowDataField.replace("bind:",""), 
        				"FILED_NAME1" : str_TxtLang, 
        				"FILED_NAME2" : null , 
        				"FILED_NAME3" : FILED_NAME3, 
        			});  
        		 }
        		}
        	}	 
        	
        	// Search Condition
        	var searchCellCount = application.gds_searchList.getColCount();
        	var searchRowCount = application.gds_searchList.getRowCount();
        	for (var i=0 ; searchRowCount > i ; i++) { 
        		var obj_Row = {};
        		for (var j=0 ; searchCellCount > j ; j++ ) { 
        			var obj_RowValue = application.gds_searchList.getColumn(i,j);
        			var obj_RowInfoId = application.gds_searchList.getColumnInfo(j).id;
        			obj_Row[obj_RowInfoId] = obj_RowValue;
        		}
        		oValue.SEARCHLIST.push(obj_Row); 
        	}	
        	
        	

        	var str_JsonData = JSON.stringify(oValue);
          
        	// form Setting 
        	objForm.setAttribute("method","post");
        	objForm.setAttribute("action", sActionUrl);
        	document.body.appendChild(objForm);

        	objInput1.setAttribute("type","hidden");
        	objInput1.setAttribute("name","xlsParam");
        	objInput1.setAttribute("value",str_JsonData);

        	objForm.appendChild(objInput1);
        	
        	objForm.submit();
        }

        this.gfn_Excel = function(gridObj,thisObj,urlValue,oValue){
        	// 2018.10.04 - ksh : 엑셀다운로드 시 조회 데이터 없을때 Validation Check 추가.
        	if(gridObj.id == "grd_header"){
        		if(thisObj.ds_header.rowcount == 0){
        			this.gfn_alert("MSG_90001");
        			return;
        		}
        	}else if(gridObj.id == "grd_detail"){
        		if(thisObj.ds_detail.rowcount == 0){
        			this.gfn_alert("MSG_90001");
        			return;
        		}		
        	}
        	

        	if(this.gfn_isNull(gridObj)){
        		return;
        	}

        	if(this.gfn_isNull(thisObj)){
        		return;
        	}

        	if(this.gfn_isNull(oValue)){
        		oValue = {
        				    EXCEL_FILENAME:thisObj.id
        		           ,ctkey:this.gfn_getUserInfo("ctKey")
        		          }
        	}
        	
        	if(this.gfn_isNull(urlValue)){
        		this.gfn_exportExcel(thisObj, gridObj, "");
        		return true;
        	}

        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var privateDataset = this.lookup(gridObj.binddataset);
        	var excelFilename = oValue.EXCEL_FILENAME
        	//if(checkList.length == privateDataset.rowcount){
        		//전체
        		thisObj.parent.div_cond.fn_excelSearch(urlValue, gridObj, thisObj, oValue);
        	//}else{
        		//일반
        		//this.gfn_exportExcel(thisObj, gridObj, excelFilename);
        	//}

        }

        this.gfn_PopExcel = function(gridObj,thisObj,urlValue,oValue){
        	if(this.gfn_isNull(gridObj)){
        		return;
        	}

        	if(this.gfn_isNull(thisObj)){
        		return;
        	}

        	if(this.gfn_isNull(oValue)){
        		oValue = {
        				    EXCEL_FILENAME:thisObj.id
        		           ,ctkey:this.gfn_getUserInfo("ctKey")
        		          }
        	}
        	
        	if(this.gfn_isNull(urlValue)){
        		this.gfn_exportExcel(thisObj, gridObj, "");
        		return true;
        	}

        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var privateDataset = this.lookup(gridObj.binddataset);
        	var excelFilename = oValue.EXCEL_FILENAME
        	//if(checkList.length == privateDataset.rowcount){
        		//전체
        		thisObj.parent.form.div_cond.fn_excelSearch(urlValue, gridObj, thisObj, oValue);
        	//}else{
        		//일반
        		//this.gfn_exportExcel(thisObj, gridObj, excelFilename);
        	//}

        }

        this.gfn_PopUpExcel = function(gridObj,thisObj,urlValue,oValue){
        	if(this.gfn_isNull(gridObj)){
        		return;
        	}

        	if(this.gfn_isNull(thisObj)){
        		return;
        	}

        	if(this.gfn_isNull(oValue)){
        		oValue = {
        				    EXCEL_FILENAME:thisObj.id
        		           ,ctkey:this.gfn_getUserInfo("ctKey")
        		          }
        	}
        	
        	if(this.gfn_isNull(urlValue)){
        		this.gfn_exportExcel(thisObj, gridObj, "");
        		return true;
        	}

        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var privateDataset = this.lookup(gridObj.binddataset);
        	var excelFilename = oValue.EXCEL_FILENAME
        	//if(checkList.length == privateDataset.rowcount){
        		//전체
        		thisObj.parent.parent.form.div_cond.fn_excelSearch(urlValue, gridObj, thisObj, oValue);
        	//}else{
        		//일반
        		//this.gfn_exportExcel(thisObj, gridObj, excelFilename);
        	//}

        }

        // 메뉴 엑셀샘플 다운로드
        this.gfn_excelDownload2 = function(dataSetObject,app,pathName){
        		application.set_filesecurelevel("all");
        		var fileFullName = dataSetObject.getColumn(0, "fileFullName");
        		var fullsUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/"+app+"/sample/"+pathName+"/"+fileFullName;
        		//var fullsUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/download/"+fileFullName;
        		this.parent.fdw_excelFile.set_downloadfilename(fileFullName);
        		var bSucc = this.parent.fdw_excelFile.download(fullsUrl);
        }

        //팝업 엑셀샘플 다운로드
        this.gfn_excelDownload3 = function(dataSetObject,app,pathName){
        		application.set_filesecurelevel("all");
        		var fileFullName = dataSetObject.getColumn(0, "fileFullName");
        		var fullsUrl = application.services["svc"].url+this.gfn_getActiveApp()+"/"+app+"/sample/"+pathName+"/"+fileFullName;
        		this.parent.form.fdw_excelFile.set_downloadfilename(fileFullName);
        		var bSucc = this.parent.form.fdw_excelFile.download(fullsUrl);
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
