﻿//XJS=Scheduler.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /**
         * @fileoverview
         * Scheduler.xjs at nexacro® software.<br>
         *<br>
         * Scheduler Class is defined as Eco.Scheduler which allows you to visualize and manage their scheduled "events".<br>
         *<br>
         * Copyright (c) 2015 EcoSystem of nexacro.<br>
         * Licensed Free under nexacro® software.
        */

        if ( !JsNamespace.exist("Eco.Scheduler")  )
        {
        	JsNamespace.declareClass("Eco.Scheduler",{
        		/**
        		 * Eco.Scheduler's constructor
        		 * @class Eco.Scheduler
        		 * @classdesc  display Outlook-like day and week view calendar/scheduler inside a Div Component.
        		 * @constructor Eco.Scheduler
        		*/
        		initialize: function(div)
        		{
        			if ( !Eco.isXComponent(div) || Eco.XComp.typeOf(div) != "Div" )
        			{
        				Eco.Logger.error("first argument named div is type of Div.");
        			}
        			this._invalidatedEventColumns = [];
        			this._bodyEventCompToRangeMap = {};
        			this._schedularComp = div;
        			this._data = new Eco.Scheduler.Data();
        		},
        		properties: {
        			/**
        			 * @property {object} rowHeaderBackground
        			 * rowHeaderBackground : background style value for the row header of scheduler's body section.
        			 * @memberOf Eco.Scheduler
        			*/
        			rowHeaderBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._rowHeaderFillBrush != newobj )
        					{
        						this._rowHeaderFillBrush = newobj;
        						this._changedRowHeaderBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._rowHeaderGradation != newobj )
        					{
        						this._rowHeaderGradation = newobj;
        						if ( this._rowHeaderFillBrush && this._rowHeaderFillBrush.style == "gradation" )
        						{
        							this._changedRowHeaderBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._rowHeaderFillBrush && this._rowHeaderFillBrush.style == "gradation" )
        					{
        						return this._rowHeaderGradation ? this._rowHeaderGradation.toString() : null;
        					}
        					return this._rowHeaderFillBrush ? this._rowHeaderFillBrush.color : null;
        				}
        			},
        			rowHeaderFont: {
        				memberName: "_rowHeaderFont",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) )
        					{
        						newobj = nexacro._getCachedFontObj(val);
        					}
        					else if ( val instanceof nexacro.Style_font )
        					{
        						newobj = nexacro._getCachedFontObj(val._value);
        					}

        					if ( this._rowHeaderFont != newobj )
        					{
        						this._rowHeaderFont = newobj;
        						this._changedRowHeadFont = 7;
        					}
        				},
        				"get": function()
        				{
        					if ( this._rowHeaderFont )
        					{
        						return this._rowHeaderFont.toString();
        					}
        					return null;
        				}
        			},
        			rowHeaderColor: {
        				memberName: "_rowHeaderFontFillBrush",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr = "solid " + color;
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._rowHeaderFontFillBrush != newobj )
        					{
        						this._rowHeaderFontFillBrush = newobj;
        						this._changedRowHeadColor = true;
        					}
        				},
        				"get": function()
        				{
        					if ( this._rowHeaderFontFillBrush )
        					{
        						return this._rowHeaderFontFillBrush.color;
        					}
        					return null;
        				}
        			},
        			rowHeaderPadding: {
        				memberName: "_rowHeaderPadding",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) && val.length )
        					{
        						newobj = nexacro._getCachedPaddingObj(val);
        					}
        					else if ( val instanceof nexacro.Style_padding )
        					{
        						newobj = nexacro._getCachedPaddingObj(val._value);
        					}

        					if ( this._rowHeaderPadding != newobj )
        					{
        						this._rowHeaderPadding = newobj;
        						this._changedRowHeadPadding = 3;
        					}
        				},
        				"get": function()
        				{
        					if ( this._rowHeaderPadding )
        					{
        						return this._rowHeaderPadding.toString();
        					}
        					return null;
        				}
        			},
        			colHeaderBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._colHeaderFillBrush != newobj )
        					{
        						this._colHeaderFillBrush = newobj;
        						this._changedColHeaderBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._colHeaderGradation != newobj )
        					{
        						this._colHeaderGradation = newobj;
        						if ( this._colHeaderFillBrush && this._colHeaderFillBrush.style == "gradation" )
        						{
        							this._changedColHeaderBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._colHeaderFillBrush && this._colHeaderFillBrush.style == "gradation" )
        					{
        						return this._colHeaderGradation ? this._colHeaderGradation.toString() : null;
        					}
        					return this._colHeaderFillBrush ? this._colHeaderFillBrush.color : null;
        				}
        			},
        			colHeaderFont: {
        				memberName: "_colHeaderFont",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) )
        					{
        						newobj = nexacro._getCachedFontObj(val);
        					}
        					else if ( val instanceof nexacro.Style_font )
        					{
        						newobj = nexacro._getCachedFontObj(val._value);
        					}

        					if ( this._colHeaderFont != newobj )
        					{
        						this._colHeaderFont = newobj;
        						this._changedColHeadFont = 15;
        					}
        				},
        				"get": function()
        				{
        					if ( this._colHeaderFont )
        					{
        						return this._colHeaderFont.toString();
        					}
        					return null;
        				}
        			},
        			colHeaderColor: {
        				memberName: "_colHeaderFontFillBrush",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr = "solid " + color;
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._colHeaderFontFillBrush != newobj )
        					{
        						this._colHeaderFontFillBrush = newobj;
        						this._changedColHeadColor = true;
        					}
        				},
        				"get": function()
        				{
        					if ( this._colHeaderFontFillBrush )
        					{
        						return this._colHeaderFontFillBrush.color;
        					}
        					return null;
        				}
        			},
        			colHeaderPadding: {
        				memberName: "_colHeaderPadding",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) && val.length )
        					{
        						newobj = nexacro._getCachedPaddingObj(val);
        					}
        					else if ( val instanceof nexacro.Style_padding )
        					{
        						newobj = nexacro._getCachedPaddingObj(val._value);
        					}

        					if ( this._colHeaderPadding != newobj )
        					{
        						this._colHeaderPadding = newobj;
        						this._changedColHeadPadding = 1;
        					}
        				},
        				"get": function()
        				{
        					if ( this._colHeaderPadding )
        					{
        						return this._colHeaderPadding.toString();
        					}
        					return null;
        				}
        			},
        			allDayBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._allDayFillBrush != newobj )
        					{
        						this._allDayFillBrush = newobj;
        						this._changedAllDayBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._allDayGradation != newobj )
        					{
        						this._allDayGradation = newobj;
        						if ( this._allDayFillBrush && this._allDayFillBrush.style == "gradation" )
        						{
        							this._changedAllDayBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._allDayFillBrush && this._allDayFillBrush.style == "gradation" )
        					{
        						return this._allDayGradation ? this._allDayGradation.toString() : null;
        					}
        					return this._allDayFillBrush ? this._allDayFillBrush.color : null;
        				}
        			},
        			cellBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._cellFillBrush != newobj )
        					{
        						this._cellFillBrush = newobj;
        						this._changedCellBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._cellGradation != newobj )
        					{
        						this._cellGradation = newobj;
        						if ( this._cellFillBrush && this._cellFillBrush.style == "gradation" )
        						{
        							this._changedCellBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellFillBrush && this._cellFillBrush.style == "gradation" )
        					{
        						return this._cellGradation ? this._cellGradation.toString() : null;
        					}
        					return this._cellFillBrush ? this._cellFillBrush.color : null;
        				}
        			},
        			cellHeadBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._cellHeadFillBrush != newobj )
        					{
        						this._cellHeadFillBrush = newobj;
        						this._changedCellHeadBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._cellHeadGradation != newobj )
        					{
        						this._cellHeadGradation = newobj;
        						if ( this._cellHeadFillBrush && this._cellHeadFillBrush.style == "gradation" )
        						{
        							this._changedCellHeadBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellHeadFillBrush && this._cellHeadFillBrush.style == "gradation" )
        					{
        						return this._cellHeadGradation ? this._cellHeadGradation.toString() : null;
        					}
        					return this._cellHeadFillBrush ? this._cellHeadFillBrush.color : null;
        				}
        			},
        			cellFont: {
        				memberName: "_cellFont",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) )
        					{
        						newobj = nexacro._getCachedFontObj(val);
        					}
        					else if ( val instanceof nexacro.Style_font )
        					{
        						newobj = nexacro._getCachedFontObj(val._value);
        					}

        					if ( this._cellFont != newobj )
        					{
        						this._cellFont = newobj;
        						this._changedCellFont = 3;
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellFont )
        					{
        						return this._cellFont.toString();
        					}
        					return null;
        				}
        			},
        			cellColor: {
        				memberName: "_cellFontFillBrush",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr = "solid " + color;
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._cellFontFillBrush != newobj )
        					{
        						this._cellFontFillBrush = newobj;
        						this._changedCellColor = true;
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellFontFillBrush )
        					{
        						return this._cellFontFillBrush.color;
        					}
        					return null;
        				}

        			},
        			cellHeadPadding: {
        				memberName: "_cellHeadPadding",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) && val.length )
        					{
        						newobj = nexacro._getCachedPaddingObj(val);
        					}
        					else if ( val instanceof nexacro.Style_padding )
        					{
        						newobj = nexacro._getCachedPaddingObj(val._value);
        					}

        					if ( this._cellHeadPadding != newobj )
        					{
        						this._cellHeadPadding = newobj;
        						this._changedCellHeadPadding = 1;
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellHeadPadding )
        					{
        						return this._cellHeadPadding.toString();
        					}
        					return null;
        				}
        			},
        			todayFontBold: {
        				memberName: "_todayFontBold",
        				"set": function(val)
        				{
        					if ( this._todayFontBold !== val )
        					{
        						this._todayFontBold = val;
        						this._changedTodayHeadProps = true; //month ==> bodycell 적용, headcell적용
        						this._changedTodayProps = true;
        					}
        				},
        				"get": function()
        				{
        					return this._todayFontBold;
        				}
        			},
        			todayFontColor: {
        				memberName: "_todayFontFillBrush",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr = "solid " + color;
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._todayFontFillBrush != newobj )
        					{
        						this._todayFontFillBrush = newobj;
        						this._changedTodayHeadProps = true; //month ==> bodycell 적용, headcell적용
        						this._changedTodayProps = true;
        					}
        				},
        				"get": function()
        				{
        					if ( this._todayFontFillBrush )
        					{
        						return this._todayFontFillBrush.color;
        					}
        					return null;
        				}
        			},
        			todayHeaderBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._todayHeaderFillBrush != newobj )
        					{
        						this._todayHeaderFillBrush = newobj;
        						this._changedTodayHeadProps = true; //month ==> bodycell 적용, headcell적용
        						this._changedTodayProps = true;
        					}

        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._todayHeaderGradation != newobj )
        					{
        						this._todayHeaderGradation = newobj;
        						if ( this._todayHeaderFillBrush && this._todayHeaderFillBrush.style == "gradation" )
        						{
        							this._changedTodayHeadProps = true; //month ==> bodycell 적용, headcell적용
        							this._changedTodayProps = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._todayHeaderFillBrush && this._todayHeaderFillBrush.style == "gradation" )
        					{
        						return this._todayHeaderGradation ? this._todayHeaderGradation.toString() : null;
        					}
        					return this._todayHeaderFillBrush ? this._todayHeaderFillBrush.color : null;
        				}
        			},
        			nonWorkingBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._nonWorkingFillBrush != newobj )
        					{
        						this._nonWorkingFillBrush = newobj;
        						this._changednonWorkingBk = true;
        					}
        					
        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._nonWorkingGradation != newobj )
        					{
        						this._nonWorkingGradation = newobj;
        						if ( this._nonWorkingFillBrush && this._nonWorkingFillBrush.style == "gradation" )
        						{
        							this._changednonWorkingBk = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._nonWorkingFillBrush && this._nonWorkingFillBrush.style == "gradation" )
        					{
        						return this._nonWorkingGradation ? this._nonWorkingGradation.toString() : null;
        					}
        					return this._nonWorkingFillBrush ? this._nonWorkingFillBrush.color : null;
        				}
        			},
        			timeRangeBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "gradation transparent";
        						}
        						else
        						{
        							valstr = "solid " + color;
        						}
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._timeRangeFillBrush != newobj )
        					{
        						this._timeRangeFillBrush = newobj;
        						this._changedTimeRangeSelectionBk = true; //apply top 1, apply body: 2
        					}

        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._timeRangeGradation != newobj )
        					{
        						this._timeRangeGradation = newobj;
        						if ( this._timeRangeFillBrush && this._timeRangeFillBrush.style == "gradation" )
        						{
        							this._changedTimeRangeSelectionBk = true; //month ==> bodycell 적용, headcell적용
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._timeRangeFillBrush && this._timeRangeFillBrush.style == "gradation" )
        					{
        						return this._timeRangeGradation ? this._timeRangeGradation.toString() : null;
        					}
        					return this._timeRangeFillBrush ? this._timeRangeFillBrush.color : null;
        				}
        			},
        			lineColor: {
        				memberName: "_cellLine",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) )
        					{
        						newobj = nexacro._getCachedStrokepenObj("1 solid " + color);
        					}

        					if ( this._cellLine != newobj )
        					{
        						this._cellLine = newobj;
        						if ( newobj )
        						{
        							var ret = Eco.Color.hexToHsl(color);
        							var rgb = Eco.Color.hslToRgb(ret[0], ret[1], ret[2] + 255*0.2);
        							var hexcolor = Eco.Color.rgbToHex(rgb[0], rgb[1], rgb[2]);
        							this._cellSubLine = nexacro._getCachedStrokepenObj("1 solid " + hexcolor);
        						}
        						else
        						{
        							this._cellSubLine = null;
        						}
        						this._changedCellLine = true;
        					}
        				},
        				"get": function()
        				{
        					if ( this._cellLine )
        					{
        						return this._cellLine.color;
        					}
        					return null;
        				}
        			},
        			eventItemBorderColor: {
        				memberName: "_eventItemBorder",
        				"set": function(color)
        				{
        					var newobj;
        					if ( Eco.isString(color) )
        					{
        						newobj = nexacro._getCachedStrokepenObj("1 solid " + color);
        					}

        					if ( this._eventItemBorder != newobj )
        					{
        						this._eventItemBorder = newobj;
        						this._changedStyleVertEventItem = 3;
        						this._changedStyleHorzEventItem = 7;
        					}
        				},
        				"get": function()
        				{
        					if ( this._eventItemBorder )
        					{
        						return this._eventItemBorder.color;
        					}
        					return null;
        				}
        			},
        			eventHorzBackground: {
        				"set": function(color,gradation)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr;
        						if ( color == "gradation" )
        						{
        							valstr = "@gradation transparent";
        						}
        						else
        						{
        							valstr = color;
        						}
        						newobj = nexacro._getCachedBackgroundObj(valstr);
        					}
        					if ( this._eventHorzBackGround != newobj )
        					{
        						this._eventHorzBackGround = newobj;
        						this._changedStyleHorzEventItem = 7;
        					}

        					newobj = null;
        					if ( Eco.isString(gradation) && gradation.length )
        					{
        						newobj = nexacro._getCachedGradationObj(gradation);
        					}

        					if ( this._eventHorzGradation != newobj )
        					{
        						this._eventHorzGradation = newobj;
        						if ( this._eventHorzBackGround && this._eventHorzBackGround.color == "@gradation" )
        						{
        							this._changedStyleHorzEventItem = 7;
        						}
        					}
        				},
        				"get": function()
        				{
        					if ( this._eventHorzBackGround && this._eventHorzBackGround.color == "@gradation" )
        					{
        						return this._eventHorzGradation ? this._eventHorzGradation.toString() : null;
        					}
        					return this._eventHorzBackGround ? this._eventHorzBackGround.color : null;
        				}
        			},
        			eventVertBackground: {
        				"set": function(colors,gradations)
        				{
        					if ( !Eco.isArray(colors) )
        					{
        						var tmpcolor = colors;
        						colors = [tmpcolor];
        					}

        					if ( !Eco.isArray(gradations) )
        					{
        						var tmpgradation = gradations;
        						gradations = [tmpgradation];
        					}

        					var newobj,
        						eventVertBackGrounds = this._eventVertBackGrounds,
        						eventVertGradations = this._eventVertGradations;
        					if ( !eventVertBackGrounds )
        					{
        						eventVertBackGrounds = [];
        						eventVertGradations = [];
        						this._eventVertBackGrounds = eventVertBackGrounds;
        						this._eventVertGradations = eventVertGradations;
        					}
        					for ( var i = 0, len = colors.length ; i < len ; i++ )
        					{
        						if ( Eco.isString(colors[i]) && colors[i].length )
        						{
        							var valstr;
        							if ( colors[i] == "gradation" )
        							{
        								valstr = "@gradation transparent";
        							}
        							else
        							{
        								valstr = colors[i];
        							}
        							newobj = nexacro._getCachedBackgroundObj(valstr);
        						}
        						if ( eventVertBackGrounds[i] != newobj )
        						{
        							eventVertBackGrounds[i] = newobj;
        							this._changedStyleVertEventItem = 3;
        						}

        						newobj = null;
        						if ( Eco.isString(gradations[i]) && gradations[i].length )
        						{
        							newobj = nexacro._getCachedGradationObj(gradations[i]);
        						}

        						if ( eventVertGradations[i] != newobj )
        						{
        							eventVertGradations[i] = newobj;
        							if ( eventVertBackGrounds[i] && eventVertBackGrounds[i].color == "@gradation" )
        							{
        								this._changedStyleVertEventItem = 3;
        							}
        						}
        					}
        					if ( eventVertBackGrounds.length != colors.length )
        					{
        						eventVertBackGrounds.length = colors.length;
        						eventVertGradations.length = colors.length;
        						this._changedStyleVertEventItem = 3;
        					}
        				},
        				"get": function()
        				{
        					var res = [],
        						eventVertBackGrounds = this._eventVertBackGrounds,
        						eventVertGradations = this._eventVertGradations;
        					if ( eventVertBackGrounds )
        					{
        						for ( var i = 0, len = eventVertBackGrounds.length ; i < len ; i++ )
        						{
        							if ( eventVertBackGrounds[i] && eventVertBackGrounds[i].color == "@gradation" )
        							{
        								res.push(eventVertGradations[i] ? eventVertGradations[i].toString() : null);
        							}
        							res.push(eventVertBackGrounds[i] ? eventVertBackGrounds[i].color : null);
        						}
        						return res;
        					}
        					return null;
        				}
        			},
        			eventFontColor: {
        				"set": function(color,overColor)
        				{
        					var newobj;
        					if ( Eco.isString(color) && color.length )
        					{
        						var valstr = "solid " + color;
        						newobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					var overobj;
        					if ( Eco.isString(overColor) && overColor.length )
        					{
        						var valstr = "solid " + overobj;
        						overobj = nexacro._getCachedFillbrushObj(valstr);
        					}
        					if ( this._eventFontColor != newobj )
        					{
        						this._eventFontColor = newobj;
        						this._changedStyleVertEventItem = 3;
        						this._changedStyleHorzEventItem = 7;
        						this._changedStyleLabelEventItem = 3;
        					}
        					if ( this._eventOverFontColor != overobj )
        					{
        						this._eventOverFontColor = overobj;
        						this._changedStyleVertEventItem = 3;
        						this._changedStyleHorzEventItem = 7;
        						this._changedStyleLabelEventItem = 3;
        					}
        				},
        				"get": function()
        				{
        					var ret = [];
        					if ( this._eventFontColor )
        					{
        						ret[0] = this._eventFontColor.color;
        					}
        					if ( this._eventOverFontColor )
        					{
        						ret[1] = this._eventOverFontColor.color;
        					}
        					return ret;
        				}
        			},
        			eventFont: {
        				memberName: "_eventFont",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) )
        					{
        						newobj = nexacro._getCachedFontObj(val);
        					}
        					else if ( val instanceof nexacro.Style_font )
        					{
        						newobj = nexacro._getCachedFontObj(val._value);
        					}

        					if ( this._eventFont != newobj )
        					{
        						this._eventFont = newobj;
        						this._changedEventFont = 1; //1
        						this._changedStyleVertEventItem = 3;
        						this._changedStyleHorzEventItem = 7;
        						this._changedStyleLabelEventItem = 3;
        					}
        				},
        				"get": function()
        				{
        					if ( this._eventFont )
        					{
        						return this._eventFont.toString();
        					}
        					return null;
        				}
        			},
        			eventPadding: {
        				memberName: "_eventPadding",
        				"set": function(val)
        				{
        					var newobj;
        					if ( Eco.isString(val) && val.length )
        					{
        						newobj = nexacro._getCachedPaddingObj(val);
        					}
        					else if ( val instanceof nexacro.Style_padding )
        					{
        						newobj = nexacro._getCachedPaddingObj(val._value);
        					}

        					if ( this._eventPadding != newobj )
        					{
        						this._eventPadding = newobj;
        						this._changedEventPadding = 1;
        						this._changedStyleVertEventItem = 3;
        						this._changedStyleHorzEventItem = 7;
        						this._changedStyleLabelEventItem = 3;
        					}
        				},
        				"get": function()
        				{
        					if ( this._eventPadding )
        					{
        						return this._eventPadding.toString();
        					}
        					return null;
        				}
        			},
        			eventItemBottomPadding: {
        				value: 3
        			},
        			binddataset: {
        				"set": function(val)
        				{
        					var data = this._data;
        					if ( data.getBinddataset() != val )
        					{
        						data.setBinddataset(val);
        					}
        				}
        			},
        			visiblearea: {
        				"set": function(dt,mode)
        				{
        					var data = this._data;
        					var ret = data.setVisibleArea(dt, mode);
        					if ( ret )
        					{
        						this._basedt = null;
        						this._rangedates = [dt, mode];
        					}
        					else
        					{
        						this._basedt = dt;
        						this._rangedates = null;
        					}
        					if ( data._changedMode || data._changedVisibleRange )
        					{
        						data._weekOffset = 0;
        					}
        				},
        				"get": function()
        				{
        					if ( this._basedt )
        					{
        						return this._basedt;
        					}
        					else
        					{
        						return this._rangedates;
        					}
        				}
        			},
        			timeRangeSelection: {
        				"set": function(sdt,edt)
        				{
        					var range;
        					if (sdt == null || edt == null )
        					{
        						range = null;
        					}
        					else
        					{
        						if ( sdt > edt )
        						{
        							var tmpdate = sdt;
        							sdt = edt;
        							edt = tmpdate;
        						}
        						var data = this._data;
        						range = [data._cloneDate(sdt), data._cloneDate(edt)];
        					}
        					if ( this._timeRangeSelection != range )
        					{
        						this._timeRangeSelection = range;
        						this._changedTimeRangeSelection = true;
        					}
        					else if ( this._timeRangeSelection && range )
        					{
        						if ( this._timeRangeSelection[0].getTime() != range[0].getTime() ||
        							 this._timeRangeSelection[1].getTime() != range[1].getTime() )
        						{
        							this._timeRangeSelection[0] = range[0];
        							this._timeRangeSelection[1] = range[1];
        							this._changedTimeRangeSelection = true;
        						}
        					}
        				},
        				"get": function()
        				{
        					return this._timeRangeSelection;
        				}
        			},
        			selectedEvent: {
        				"set": function(val)
        				{
        					if ( val != this._selectedEvtItem )
        					{
        						var evt = {
        							oldEventItem: this._selectedEvtItem,
        							newEventItem: val
        						};
        						this._drawSelectedEvtItem(val, this._selectedEvtItem);
        						this._selectedEvtItem = val;
        						this.fireEvent("oneventitemselectchanged", evt);
        					}
        				},
        				"get": function()
        				{
        					return this._selectedEvtItem;
        				}
        			},
        			drillButtonHeight: {
        				value: 14
        			},
        			drillButtonWidth: {
        				value: 14
        			},
        			drillButtonStyle: {
        				value: "bordertype:normal 0 0 ;background:@gradation URL('Images::minus.png') 50 50; border:1 solid #abb1b6ff ;cursor:hand;gradation:linear 50,100 #ecececff 50,0 #ffffffff [0% #ecececff][42% #f2f2f2ff][54% #e0e0e0ff];",
        				updateValue: function(value)
        				{
        					this.drillButtonStyle = value;
        					this._changedStyleDrillButton = true;
        				}
        			},
        			percentOverlap: {
        				value: 80
        			}
        		},
        		_changedStyleDrillButton: true,
        		events: {
        			onviewrangechanged: {},
        			oneventitemselectchanged: {}
        		},
        		getColHeadHeight: function()
        		{
        			var textHeight = this._colHeadTextHeight||0,
        				vertPadding = this._colHeadVertPadding||0;
        			//1 : font 변경 여부, 2: padding 변경 여부,
        			//4 : font 적용 여부, 8: color 적용 여부
        			// 무조건 center 정렬
        			if ( this._changedColHeadFont&1 ) 
        			{
        				//textHeight = Eco.XComp.PositionSize.getTextSize(this.labelCellFont, "W핳j");
        				textHeight = Eco.XComp.PositionSize.getTextSize(this._colHeaderFont, "핳");
        				textHeight = textHeight[1];
        				this._colHeadTextHeight = textHeight;
        				this._changedColHeadFont = this._changedColHeadFont & (~1);
        			}
        			if ( this._changedColHeadPadding&1 ) 
        			{
        				var padding = this._colHeaderPadding,
        					horzPadding = 0;
        				if ( padding )
        				{
        					vertPadding = padding.top + padding.bottom;
        					horzPadding = padding.left + padding.right;
        				}
        				else
        				{
        					vertPadding = 0;
        				}
        				this._colHeadVertPadding = vertPadding;
        				this._colHeadHorzPadding = horzPadding;
        				this._changedColHeadPadding = this._changedColHeadPadding & (~1);
        			}
        			return vertPadding + textHeight;
        		},
        		getAllDayHeadHeight: function(data)
        		{
        			var eventItemHeight = this.getEventItemHeight();
        			var overLapLayout = this._allDayOverLapLayout;
        			if ( data._changeEventItems & 1 )
        			{
        				data._changeEventItems = data._changeEventItems & (~1);
        				if ( !overLapLayout )
        				{
        					overLapLayout = data.createOverlapLayout(false, false);
        					this._allDayOverLapLayout = overLapLayout;
        				}
        				data.calcOverlapping(overLapLayout, data._dayEvents, data._visibleStartdate, data._visibleEnddate);
        			}
        			var cnt = 0;
        			if ( overLapLayout )
        			{
        				cnt = data.getOverlapLayoutCount(overLapLayout);
        			}
        			return (eventItemHeight + 2)*Math.max(3, cnt + 1) + this.eventItemBottomPadding;
        		},
        		getCellHeadHeight: function()
        		{
        			var textHeight = this._cellHeadTextHeight||0,
        				vertPadding = this._cellHeadVertPadding||0;
        			//1 : font 변경 여부, 2: padding 변경 여부,
        			//4 : font 적용 여부, 8: color 적용 여부
        			// 무조건 center 정렬
        			if ( this._changedCellFont&1 ) 
        			{
        				//textHeight = Eco.XComp.PositionSize.getTextSize(this._eventFont, "W핳j");
        				textHeight = Eco.XComp.PositionSize.getTextSize(this._cellFont, "핳");
        				textHeight = textHeight[1];
        				this._cellHeadTextHeight = textHeight;
        				this._changedCellFont = this._changedCellFont & (~1);
        			}
        			if ( this._changedCellHeadPadding&1 ) 
        			{
        				var padding = this._cellHeadPadding,
        					horzPadding = 0;
        				if ( padding )
        				{
        					vertPadding = padding.top + padding.bottom;
        					horzPadding = padding.left + padding.right;
        				}
        				else
        				{
        					vertPadding = 0;
        				}
        				this._cellHeadVertPadding = vertPadding;
        				this._cellHeadHorzPadding = horzPadding;
        				this._changedCellHeadPadding = this._changedCellHeadPadding & (~1);
        			}
        			return vertPadding + textHeight;
        		},
        		getEventItemHeight: function()
        		{
        			var textHeight = this._eventTextHeight||0,
        				vertPadding = this._eventVertPadding||0;
        			//1 : font 변경 여부, 2: padding 변경 여부,
        			//4 : font 적용 여부, 8: color 적용 여부
        			// 무조건 center 정렬
        			if ( this._changedEventFont&1 ) 
        			{
        				//textHeight = Eco.XComp.PositionSize.getTextSize(this._eventFont, "W핳j");
        				textHeight = Eco.XComp.PositionSize.getTextSize(this._eventFont, "핳");
        				textHeight = textHeight[1];
        				this._eventTextHeight = textHeight;
        				this._changedEventFont = this._changedEventFont & (~1);
        			}
        			if ( this._changedEventPadding&1 ) 
        			{
        				var padding = this._eventPadding,
        					horzPadding = 0;
        				if ( padding )
        				{
        					vertPadding = padding.top + padding.bottom;
        					horzPadding = padding.left + padding.right;
        				}
        				else
        				{
        					vertPadding = 0;
        				}
        				this._eventVertPadding = vertPadding;
        				this._eventHorzPadding = horzPadding;
        				this._changedEventPadding = this._changedEventPadding & (~1);
        			}
        			return vertPadding + textHeight + 2; //border length 포함.
        		},
        		getRowHeaderWidth: function(mode)
        		{
        			var textWidth,
        				horzPadding = this._rowHeadHorzPadding||0;
        			//1 : font 변경 여부, 2: padding 변경 여부,
        			//4 : font 적용 여부, 8: color 적용 여부
        			// 무조건 center 정렬
        			if ( this._changedRowHeadPadding&1 ) 
        			{
        				var padding = this._rowHeaderPadding,
        					vertPadding = 0;
        				if ( padding )
        				{
        					horzPadding = padding.left + padding.right;
        					vertPadding = padding.top + padding.bottom;
        				}
        				else
        				{
        					horzPadding = 0;
        				}
        				this._rowHeadHorzPadding = horzPadding;
        				this._rowHeadVertPadding = vertPadding;
        				this._changedRowHeadPadding = this._changedRowHeadPadding & (~1);
        			}

        			if ( mode == "month" )
        			{
        				textWidth = this._monthTextWidth;
        				if ( this._changedRowHeadFont&1 )
        				{
        					textWidth = Eco.XComp.PositionSize.getTextSize(this._rowHeaderFont, "53");//주차 Text길이가 제일 큰 값
        					textWidth = textWidth[0];
        					this._monthTextWidth = textWidth;
        					this._changedRowHeadFont = this._changedRowHeadFont & (~1);
        				}
        			}
        			else
        			{
        				textWidth = this._weekTextWidth;
        				if ( this._changedRowHeadFont&2 )
        				{
        					textWidth = Eco.XComp.PositionSize.getTextSize(this._rowHeaderFont, "12 " + Eco.date.ttName[1]);//hour am/pm Text길이가 제일 큰 값
        					textWidth = textWidth[0];
        					this._weekTextWidth = textWidth;
        					this._changedRowHeadFont = this._changedRowHeadFont & (~2);
        				}
        			}
        			return textWidth + horzPadding;
        		},
        		draw: function()
        		{
        			var data = this._data;
        			var factory = Eco.XComp.Factory,
        				pDiv = this._schedularComp,
        				headComp = this._headComp;

        			this._width = pDiv._client_width;
        			this._height = pDiv._client_height;

        			//Graphic Comp 생성
        			if ( !headComp )
        			{
        				headComp = factory.getXComp(pDiv, "Graphic", "grp_head");
        				this._headComp = headComp;
        				factory.setProperties(headComp, "style", "background: transparent; border:0 none transparent;");
        			}
        			var bodyDiv = this._bodyComp;
        			if ( !bodyDiv )
        			{
        				bodyDiv = factory.getXComp(pDiv, "Div", "div_body");
        				this._bodyComp = bodyDiv;
        				factory.setProperties(bodyDiv, "style", "background: transparent; border:0 none transparent;", "scrollbars", "autovert");
        				//get scrollbar size
        				this.vertscrollwidth = bodyDiv.vscrollbar.getOffsetWidth();
        			}

        			this._layout(data);

        			var headBandArea = this._headBandArea;
        			factory.setRect(headComp, headBandArea.x, headBandArea.y, headBandArea.width, headBandArea.height);
        			var layer = headComp.getChildByIndex(0);
        			this._headBandDraw(layer, headBandArea, data);

        			var bodyBandArea = this._bodyBandArea;
        			if ( data._mode == "month" )
        			{
        				factory.setRect(bodyDiv, bodyBandArea.x, bodyBandArea.y, bodyBandArea.width, bodyBandArea.height);
        				bodyDiv.removeEventHandler("onvscroll", this._vscrollHandler, this);
        				Eco.XComp.Event.clearDraggable(headComp);
        			}
        			else
        			{
        				var draggingFunc = {
        					'start': this._onheaddragstarthandler,
        					'dragging': this._onheaddragginghandler,
        					'end': this._onheaddragendhandler
        				};
        				Eco.XComp.Event.makeDraggable(headComp, draggingFunc, this, null, true);
        				factory.setRect(bodyDiv, bodyBandArea.x, bodyBandArea.y, bodyBandArea.width + this.vertscrollwidth, bodyBandArea.height);
        				bodyDiv.addEventHandler("onvscroll", this._vscrollHandler, this);
        				//timeRange Selection 기능
        			}
        			var bodyGrp = this._bodyGrp;
        			if ( !bodyGrp )
        			{
        				bodyGrp = factory.getXComp(bodyDiv, "Graphic", "grp_body");
        				this._bodyGrp = bodyGrp;
        				factory.setProperties(bodyGrp, "style", "background: transparent; border:0 none transparent;");
        				var draggingFunc = {
        					'start': this._onbodydragstarthandler,
        					'dragging': this._onbodydragginghandler,
        					'end': this._onbodydragendhandler
        				};
        				Eco.XComp.Event.makeDraggable(bodyGrp, draggingFunc, this, null, true);	
        			}

        			factory.setRect(bodyGrp, 0, 0, bodyBandArea.width, bodyBandArea.rowHeadArea.height);
        			layer = bodyGrp.getChildByIndex(0);
        			this._bodyBandDraw(layer, bodyBandArea, data);

        			if ( this._changedTimeRangeSelection || data._changedVisibleRange )
        			{
        				var timeRangeSelection = this._timeRangeSelection;
        				if ( timeRangeSelection )
        				{
        					this._drawTimeRangeSelection(timeRangeSelection[0], timeRangeSelection[1]);
        				}
        				else
        				{
        					this._clearTimeRangeSelection();
        				}
        				this._changedTimeRangeSelection = false;
        				this._changedTimeRangeSelectionBk = false;
        			}
        			else if ( this._changedTimeRangeSelectionBk )
        			{
        				var bodySelectionElems = this._bodySelectionElems;
        				if ( bodySelectionElems )
        				{
        					bodySelectionElem.setFillbrush(this._timeRangeFillBrush);
        					bodySelectionElem.setFillgradation(this._timeRangeGradation);
        				}
        				var headSelectionElem = this._headSelectionElem;
        				if ( headSelectionElem )
        				{
        					headSelectionElem.setFillbrush(this._timeRangeFillBrush);
        					headSelectionElem.setFillgradation(this._timeRangeGradation);
        				}
        				this._changedTimeRangeSelectionBk = false;
        			}

        			if ( data._mode != "month" && (data._changeEventItems & 1) )
        			{
        				var overLapLayout = this._allDayOverLapLayout;
        				data._changeEventItems = data._changeEventItems & (~1);
        				if ( !overLapLayout )
        				{
        					overLapLayout = data.createOverlapLayout(false, false);
        					this._allDayOverLapLayout = overLapLayout;
        				}
        				data.calcOverlapping(overLapLayout, data._dayEvents, data._visibleStartdate, data._visibleEnddate);
        			}
        			
        			//allday eventItems draw처리
        			if ( data._mode != "month" )
        			{
        				this._bodyEventItemsMonthModeDrawClear();

        				if ( data._changeEventItems & 2 )
        				{
        					this._allDayEventItemsDraw(data, false);
        					this._bodyEventItemsWeekModeDraw(data, false);
        					data._changeEventItems = data._changeEventItems & (~2);
        				}
        				else if ( (this._changedStyleHorzEventItem & 2) ||
        						  (this._changedStyleVertEventItem & 2) )
        				{
        					if ( this._changedStyleHorzEventItem & 2 )
        					{
        						this._allDayEventItemsDraw(data, true);
        					}
        					if ( this._changedStyleVertEventItem & 2 )
        					{
        						this._bodyEventItemsWeekModeDraw(data, true);
        					}
        				}
        			}
        			else
        			{
        				this._allDayEventItemsDrawClear();
        				this._bodyEventItemsWeekModeDrawClear();
        				if ( data._changeEventItems & 2 )
        				{
        					this._bodyEventItemsMonthModeDraw(data, false);
        					data._changeEventItems = data._changeEventItems & (~2);
        				}
        				else if ( (this._changedStyleHorzEventItem & 4) ||
        						  (this._changedStyleLabelEventItem & 2) ||
        						  this._changedStyleDrillButton )
        				{
        					this._bodyEventItemsMonthModeDraw(data, true);
        				}
        			}

        			//scroll 기능 처리
        			if ( data._mode == "month" )
        			{
        				var trackBtn = factory.getXCompByName(pDiv, "trackBtn");
        				if ( trackBtn )
        				{
        					Eco.XComp.Event.clearDraggable(trackBtn);
        					factory.release(trackBtn);
        				}

        				var headBandArea = this._headBandArea,
        					scrollX = headBandArea.x + headBandArea.width,
        					btnSize = this.vertscrollwidth;

        				scrollX = scrollX + Math.round((this._width - scrollX - btnSize)/2);
        				var upArrow = this._upArrow,
        					downArrow = this._downArrow,
        					fontSize = Math.round(this.vertscrollwidth/2);
        				if ( !upArrow )
        				{
        					upArrow = factory.getXComp(pDiv, "Static", "upArrow", scrollX, 5, btnSize, btnSize);
        					factory.setProperties(upArrow, "text", "▲", "style",
        						"bordertype:normal 0 0 ;padding:0 0 0 0;align:center middle;font:Consolas,-" + fontSize + ",antialias; color: black;cursor:hand; background:@gradation; border:1 solid #abb1b6ff; gradation:linear 50,100 #ecececff 50,0 #ffffffff [0% #ecececff][42% #f2f2f2ff][54% #e0e0e0ff];");
        					this._upArrow = upArrow;
        					Eco.XComp.Event.makeRepeatable(upArrow,
        						this._upRepeatHandler, this);
        				}
        				else
        				{
        					factory.setRect(upArrow, scrollX, 5, btnSize, btnSize);
        				}

        				if ( !downArrow )
        				{
        					downArrow = factory.getXComp(pDiv, "Static", "downArrow", scrollX, btnSize + 5 + 5, btnSize, btnSize);
        					factory.setProperties(downArrow, "text", "▼", "style",
        						"bordertype:normal 0 0 ;padding:0 0 0 0;align:center middle;font:Consolas,-" + fontSize + ",antialias; color: black; cursor:hand; background:@gradation; border:1 solid #abb1b6ff; gradation:linear 50,100 #ecececff 50,0 #ffffffff [0% #ecececff][42% #f2f2f2ff][54% #e0e0e0ff];");
        					this._downArrow = downArrow;
        					Eco.XComp.Event.makeRepeatable(downArrow,
        						this._downRepeatHandler, this);
        				}
        				else
        				{
        					factory.setRect(downArrow, scrollX, btnSize + 5 + 5, btnSize, btnSize);
        				}
        			}
        			else
        			{
        				var upArrow = this._upArrow,
        					downArrow = this._downArrow;

        				if ( upArrow )
        				{
        					Eco.XComp.Event.clearRepeatable(upArrow);
        					factory.release(upArrow);
        					this._upArrow = null;
        				}
        				if ( downArrow )
        				{
        					Eco.XComp.Event.clearRepeatable(downArrow);
        					factory.release(downArrow);
        					this._downArrow = null;
        				}
        				data._weekOffset = 0;
        			}

        			this._applyRender();

        			this._drawSelectedEvtItem(this._selectedEvtItem);

        			if ( data._mode != "month" && data._changedMode )
        			{
        				var yPos = data._hourHeight*9;
        				bodyDiv.vscrollbar.set_pos(yPos);
        			}

        			if ( data._changedVisibleRange || data._changedMode )
        			{
        				var evt = {
        					startdate: data._cloneDate(data._visibleStartdate),
        					enddate:  data._cloneDate(data._visibleEnddate),
        					mode: data._mode
        				};
        				this.fireEvent("onviewrangechanged", evt);
        			}
        			data._changedVisibleRange = false;
        			data._changedMode = false;
        			this._changedColumnSized = false;
        			this._changedRowSized = false;
        		},
        		_applyRender: function()
        		{
        			var headComp = this._headComp,
        				bodyGrp = this._bodyGrp,
        				curRect, rect = headComp._rect;

        			if ( !headComp._needsRender //|| 
        				 //( !(curRect = headComp._curRect) ||
        				//	rect.width != curRect.width || rect.height != curRect.height 
        				 //)
        			   )
        			{
        				headComp.invalidate();
        			}

        			rect = bodyGrp._rect;
        			if ( !bodyGrp._needsRender //|| 
        				 //( !(curRect = bodyGrp._curRect) ||
        				//	rect.width != curRect.width || rect.height != curRect.height 
        				 //)
        			   )
        			{
        				bodyGrp.invalidate();
        			}

        			var pDiv = this._schedularComp,
        				bodyDiv = this._bodyComp;

        			Eco.XComp.Factory.doLayout(pDiv, true);
        			bodyDiv.resetScroll();
        		},
        		_upRepeatHandler : function()
        		{
        			var data = this._data;
        			data._weekOffset -= 1;
        			if ( this._basedt )
        			{
        				data.setVisibleArea(this._basedt, "month");
        			}
        			else
        			{
        				data.setVisibleArea(this._rangedates[0], this._rangedates[1]);
        			}
        			this.draw();
        		},
        		_downRepeatHandler : function()
        		{
        			var data = this._data;
        			data._weekOffset += 1;
        			if ( this._basedt )
        			{
        				data.setVisibleArea(this._basedt, "month");
        			}
        			else
        			{
        				data.setVisibleArea(this._rangedates[0], this._rangedates[1]);
        			}
        			//console.log(data._visibleStartdate, data._visibleEnddate, data._mode, data._weekOffset);
        			this.draw();
        		},
        		_vscrollHandler: function(obj,e)
        		{
        			var data = this._data;
        			if ( !data ) return;
        			var pos = e.pos;
        			var hour = Math.floor(pos/data._hourHeight);
        			var bodyGrp = this._bodyGrp;
        			var ttTexts = this._noons||[];
        			var layer = bodyGrp.getChildByIndex(0);
        			if ( hour < 12 )
        			{
        				var amTextElem = ttTexts[0];
        				if ( !amTextElem )
        				{
        					amTextElem = new Eco.GraphicText();
        					layer.addChild(amTextElem);
        					amTextElem.setValign("bottom");
        					amTextElem.setHalign("left");
        					amTextElem.setText("AM");
        					ttTexts[0] = amTextElem;
        				}
        				var xPos = Math.round(this._bodyBandArea.rowHeadArea.width/2);
        				var yPos = hour*data._hourHeight + data._hourHeight - 3;
        				amTextElem.setX(xPos);
        				amTextElem.setY(yPos);
        				amTextElem.setFont(this._rowHeaderFont);
        				amTextElem.setFillbrush(this._rowHeaderFontFillBrush);
        				var bodyDiv = this._bodyComp;
        				var noonPos = data._hourHeight*12;
        				var lastpos = pos + bodyDiv.vscrollbar.view;
        				if ( noonPos < lastpos )
        				{
        					var pmTextElem = ttTexts[1];
        					yPos = 12*data._hourHeight + data._hourHeight - 3;
        					if ( !pmTextElem )
        					{
        						pmTextElem = new Eco.GraphicText();
        						layer.addChild(pmTextElem);
        						pmTextElem.setValign("bottom");
        						pmTextElem.setHalign("left");
        						pmTextElem.setText("PM");
        						ttTexts[1] = pmTextElem;
        					}
        					pmTextElem.setX(xPos);
        					pmTextElem.setY(yPos);
        					pmTextElem.setFont(this._rowHeaderFont);
        					pmTextElem.setFillbrush(this._rowHeaderFontFillBrush);
        				}
        			}
        			else
        			{
        				var pmTextElem = ttTexts[1];
        				if ( !pmTextElem )
        				{
        					pmTextElem = new Eco.GraphicText();
        					layer.addChild(pmTextElem);
        					pmTextElem.setValign("bottom");
        					pmTextElem.setHalign("left");
        					pmTextElem.setText("PM");
        					ttTexts[1] = pmTextElem;
        				}
        				var xPos = Math.round(this._bodyBandArea.rowHeadArea.width/2);
        				var yPos = hour*data._hourHeight + data._hourHeight - 3;
        				pmTextElem.setX(xPos);
        				pmTextElem.setY(yPos);
        				pmTextElem.setFont(this._rowHeaderFont);
        				pmTextElem.setFillbrush(this._rowHeaderFontFillBrush);
        			}

        			this._noons = ttTexts;
        			bodyGrp.invalidate();
        		},
        		_onheaddragstarthandler: function(x,y,shiftKey,ctrlKey)
        		{
        			this._startTimeRangeSelection(x, y, shiftKey, "head");
        			this.draw();
        		},
        		_onheaddragginghandler: function(offsetX,offsetY,shiftKey,ctrlKey)
        		{
        			this._resizeTimeRangeSelection(offsetX, offsetY, "head");
        			this.draw();
        		},
        		_onheaddragendhandler: function(x,y,shiftKey,ctrlKey)
        		{
        			this._endTimeRangeSelection();
        			this.draw();
        		},
        		_onbodydragstarthandler: function(x,y,shiftKey,ctrlKey)
        		{
        			x = x - this._bodyBandArea.rowHeadArea.width;
        			this._startTimeRangeSelection(x, y, shiftKey, "body");
        			this.draw();
        		},
        		_onbodydragginghandler: function(offsetX,offsetY,shiftKey,ctrlKey)
        		{
        			this._resizeTimeRangeSelection(offsetX, offsetY, "body");
        			this.draw();
        		},
        		_onbodydragendhandler: function(x,y,shiftKey,ctrlKey)
        		{
        			this._endTimeRangeSelection();
        			this.draw();
        		},
        		_startTimeRangeSelection: function(x,y,shiftKey,gbn)
        		{
        			var sdt, edt;
        			var selectRange = this._timeRangeSelection;
        			var selectRangeEventInfo = this._selectRangeEventInfo;
        			var data = this._data,
        				calendar = data._calendar;

        			if ( !selectRangeEventInfo )
        			{
        				selectRangeEventInfo = {};
        				this._selectRangeEventInfo = selectRangeEventInfo;
        			}
        			selectRangeEventInfo._isSelecting = true;
        			selectRangeEventInfo._startX = x;
        			selectRangeEventInfo._startY = y;
        			var tmpDt = this._getDateFromPt(gbn, x, y, true);
        			if ( data._mode == "month" )
        			{
        				if ( shiftKey && selectRange != null )
        				{
        					selectRangeEventInfo.startSelectedTime = selectRange[0];
        					selectRangeEventInfo.endSelectedTime = tmpDt;
        					if ( selectRange[0] > tmpDt )
        					{
        						sdt = tmpDt;
        						edt = calendar.addUnits(selectRange[0], Eco.TimeUnit.DAY, 1);
        					}
        					else
        					{
        						sdt = selectRange[0];
        						edt = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, 1);
        					}
        				}
        				else
        				{
        					selectRangeEventInfo.startSelectedTime = tmpDt;
        					selectRangeEventInfo.endSelectedTime = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, 1);
        					sdt = selectRangeEventInfo.startSelectedTime;
        					edt = selectRangeEventInfo.endSelectedTime;
        				}
        			}
        			else if (shiftKey && selectRange != null)
        			{
        				selectRangeEventInfo.startSelectedTime = selectRange[0];
        				selectRangeEventInfo.endSelectedTime = tmpDt;
        				if (selectRange[0] > tmpDt)
        				{
        					sdt = data._cloneDate(tmpDt);
        					edt = this._getTimeRangeSnapDate(gbn, selectRange[0]);
        				}
        				else
        				{
        					sdt = data._cloneDate(selectRange[0]);
        					edt = this._getTimeRangeSnapDate(gbn, tmpDt);
        				}
        			}
        			else
        			{
        				selectRangeEventInfo.startSelectedTime = tmpDt;
        				selectRangeEventInfo.endSelectedTime = this._getTimeRangeSnapDate(gbn, tmpDt);
        				sdt = selectRangeEventInfo.startSelectedTime;
        				edt = selectRangeEventInfo.endSelectedTime;
        			}
        			if ( this.showTimeRangeTips )
        			{
        				//this._renderData.schedulerControl._toolTipManger.createToolTip(this._renderData.schedulerControl.computeTimeRangeString(tmpStartDt,tmpEndDt)
        				//												   ,e.screenX + 20,e.screenY + 20,"left");
        			}
        			this.setTimeRangeSelection(sdt, edt);
        		},
        		_resizeTimeRangeSelection: function(offsetx,offsety,gbn)
        		{
        			var selectRangeEventInfo = this._selectRangeEventInfo;
        			if ( selectRangeEventInfo && selectRangeEventInfo._isSelecting )
        			{
        				var x = selectRangeEventInfo._startX + offsetx;
        				var y = selectRangeEventInfo._startY + offsety;
        				var data = this._data,
        					calendar = data._calendar,
        					sdt, edt;
        				if ( this.showTimeRangeTips )
        				{
        					//this._renderData.schedulerControl._toolTipManger.move(e.screenX + 20,e.screenY + 20,"left");		
        				}
        				if ( data._mode == "month" )
        				{
        					edt = this._getDateFromPt(gbn, x, y, true);
        					if ( selectRangeEventInfo.endSelectedTime.getTime() != edt.getTime() )
        					{
        						selectRangeEventInfo.endSelectedTime = edt;
        						if ( selectRangeEventInfo.startSelectedTime > selectRangeEventInfo.endSelectedTime )
        						{
        							sdt = data._cloneDate(selectRangeEventInfo.endSelectedTime);
        							edt = calendar.addUnits(selectRangeEventInfo.startSelectedTime, Eco.TimeUnit.DAY, 1);
        						}
        						else
        						{
        							sdt = data._cloneDate(selectRangeEventInfo.startSelectedTime);
        							edt = calendar.addUnits(selectRangeEventInfo.endSelectedTime, Eco.TimeUnit.DAY, 1);
        						}

        						if ( this.showTimeRangeTips )
        						{
        							//this._renderData.schedulerControl._toolTipManger.createToolTip(this._renderData.schedulerControl.computeTimeRangeString(start,end)
        							//													   ,e.screenX + 20,e.screenY + 20,"left");
        						}
        						this.setTimeRangeSelection(sdt, edt);
        					}
        				}
        				else
        				{
        					var retArr;
        					if ( gbn != "head" && this.dragScroll )
        					{
        						var bodyDiv = this._bodyComp;
        						var tmpY = y - bodyDiv.vscrollbar.pos;
        						if ( tmpY < 0 || tmpY > bodyDiv.getOffsetHeight() )
        						{
        							selectRangeEventInfo.scrollStep = ( y < 0 ? (-bodyDiv.vscrollbar.line) : bodyDiv.vscrollbar.line );
        							if ( bodyDiv.vscrollbar.pos + selectRangeEventInfo.scrollStep < 0 )
        							{
        								bodyDiv.vscrollbar.set_pos(0);
        							}
        							else if ( bodyDiv.vscrollbar.pos + selectRangeEventInfo.scrollStep > bodyDiv.vscrollbar.max )
        							{
        								bodyDiv.vscrollbar.set_pos(bodyDiv.vscrollbar.max);
        							}
        							else
        							{
        								bodyDiv.vscrollbar.set_pos(bodyDiv.vscrollbar.pos + selectRangeEventInfo.scrollStep);
        							}
        							retArr = this._calcEndTimeSelectedInDay(gbn, x, y, selectRangeEventInfo);
        						}
        						else
        						{
        							retArr = this._calcEndTimeSelectedInDay(gbn, x, y, selectRangeEventInfo);
        						}
        					}
        					else
        					{
        						retArr = this._calcEndTimeSelectedInDay(gbn, x, y, selectRangeEventInfo);
        					}
        					if ( retArr )
        					{
        						this.setTimeRangeSelection(retArr[0], retArr[1]);
        					}
        					//console.log("====>", "x", x, "y", y, retArr);
        				}
        			}
        		},
        		_calcEndTimeSelectedInDay: function(gbn,x,y,selectRangeEventInfo)
        		{
        			var data = this._data,
        				calendar = data._calendar,
        				endSelected = selectRangeEventInfo.endSelectedTime;

        			var tmpDt = this._getDateFromPt(gbn, x, y, true);
        			if ( endSelected.getTime() != tmpDt.getTime() )
        			{
        				var sdt, edt;
        				selectRangeEventInfo.endSelectedTime = tmpDt;
        				if ( selectRangeEventInfo.startSelectedTime > tmpDt )
        				{
        					sdt = data._cloneDate(tmpDt);
        					edt = this._getTimeRangeSnapDate(gbn, selectRangeEventInfo.startSelectedTime);
        				}
        				else
        				{
        					sdt = data._cloneDate(selectRangeEventInfo.startSelectedTime);
        					edt = this._getTimeRangeSnapDate(gbn, tmpDt);
        				}
        				if ( this.showTimeRangeTips )
        				{
        					//this._renderData.schedulerControl._toolTipManger.createToolTip(this._renderData.schedulerControl.computeTimeRangeString(sdate,edate)
        					//													   ,e.screenX + 20,e.screenY + 20,"left");
        				}
        				return [sdt, edt];
        			}
        		},
        		_endTimeRangeSelection: function()
        		{
        			var selectRangeEventInfo = this._selectRangeEventInfo;
        			if ( selectRangeEventInfo )
        			{
        				selectRangeEventInfo._isSelecting = false;
        				if (this.showTimeRangeTips)
        				{
        					//this._renderData.schedulerControl._toolTipManger.destoryToolTip();
        				}
        			}
        		},
        		_getDateFromPt: function(gbn,x,y,isDayUnit)
        		{
        			var data = this._data,
        				calendar = data._calendar,
        				tmpDt;
        			if ( gbn == "head" )
        			{
        				var columnWidth = data._columnWidth;
        				var pos = parseInt(x / columnWidth);
        				tmpDt = calendar.addUnits(data._visibleStartdate, Eco.TimeUnit.DAY, pos);
        				if (!isDayUnit)
        				{
        					pos = x - pos * columnWidth;
        					pos = parseInt(24 * 60 * pos / columnWidth);
        					tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.MINUTE, pos, true);
        				//	trace(pos + "///-->" + tmpDate);
        				}
        			}
        			else
        			{
        				if ( data._mode == "month" )
        				{
        					var rowHeight = data._rowHeight,
        						columnWidth = data._columnWidth,
        						tmpX, tmpY;
        					tmpY = parseInt(y / rowHeight);
        					tmpX = parseInt(x / columnWidth);
        					tmpDt = calendar.addUnits(data._visibleStartdate, Eco.TimeUnit.DAY, tmpY * data._columnCount + tmpX);
        					tmpDt = calendar.floor(tmpDt, Eco.TimeUnit.DAY, 1);
        					if (!isDayUnit)
        					{
        						tmpX = x - tmpX * columnWidth;
        						tmpX = 1440 * tmpX / columnWidth;
        						tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.MINUTE, tmpX, true);
        						//trace("execute in here!!!-->" + tmpDate);
        					}
        				}
        				else
        				{
        					var dayHeight = data._hourHeight * 24;
        					var timeslot = 30, tmpY;
        					tmpDt = data._cloneDate(data._visibleStartdate);
        					tmpDt = calendar.addUnits(tmpDt, Eco.TimeUnit.DAY, Math.floor(x / data._columnWidth), true);
        					tmpDt = calendar.floor(tmpDt, Eco.TimeUnit.DAY, 1);
        					tmpY = y * 1440 / dayHeight;
        					var hour = Math.floor(tmpY / 60);
        					var min = tmpY % 60;
        					tmpDt.setHours(hour);
        					if (isDayUnit)
        					{
        						tmpDt.setMinutes(Math.floor(min/ timeslot) * timeslot);
        					}
        					else
        					{
        						tmpDt.setMinutes(min);
        					}
        				}
        			}
        			return tmpDt;
        		},
        		_getTimeRangeSnapDate: function(gbn,dt)
        		{
        			if ( gbn == "head" )
        			{
        				return this._data._calendar.addUnits(dt, Eco.TimeUnit.DAY, 1);
        			}
        			else
        			{
        				var timeslot = 30;
        				return this._data._calendar.addUnits(dt, Eco.TimeUnit.MINUTE, timeslot);
        			}
        		},
        		_clearTimeRangeSelection: function()
        		{
        			var bodyGrp = this._bodyGrp;
        			var bodyLayer = bodyGrp.getChildByIndex(0);
        			var bodySelectionElems = this._bodySelectionElems;
        			if ( bodySelectionElems )
        			{
        				for ( var i = 0, len = bodySelectionElems.length ; i < len ; i++ )
        				{
        					bodyLayer.removeChild(bodySelectionElems[i]);
        				}
        				this._bodySelectionElems = null;
        			}
        			var headSelectionElem = this._headSelectionElem;
        			if ( headSelectionElem )
        			{
        				var headComp = this._headComp;
        				var layer = headComp.getChildByIndex(0);
        				var headband = layer.getElementById("headband");
        				headband.removeChild(headSelectionElem);
        				this._headSelectionElem = null;
        			}
        		},
        		_drillBtnClickHandler: function(obj,e)
        		{
        			var dt = obj._keydate;
        			if ( !dt )
        			{
        				var data = this._data,
        					visiblesdt = data._visibleStartdate;
        				dt = data._calendar.addUnits(visiblesdt, Eco.TimeUnit.DAY, obj._row * data._columnCount + obj._col);
        				obj._keydate = dt;
        			}
        			this.setVisiblearea(dt, "day");
        			this.draw();
        		},
        		_evtItemMouseMoveHandler: function(obj,e)
        		{
        			var evtItemEditEventInfo = this._evtItemEditEventInfo;

        			if ( evtItemEditEventInfo )
        			{
        				if ( evtItemEditEventInfo.isEditing ) return;
        			}
        			else
        			{
        				evtItemEditEventInfo = {};
        				this._evtItemEditEventInfo = evtItemEditEventInfo;
        			}

        			var jobgbn = this._getKindOfJob(this._data, obj, e.clientX, e.clientY);
        			evtItemEditEventInfo.editedEvtItem = jobgbn[3];
        			evtItemEditEventInfo.isDayUnit = this._data._mode == "month" ? (!jobgbn[2]) : false;
        			evtItemEditEventInfo.gbn = jobgbn[4];
        			if ( jobgbn[0] == "start" )
        			{
        				evtItemEditEventInfo.editMode = "start";
        				obj.style.setStyleValue( "cursor", "normal", jobgbn[1] );
        			}
        			else if ( jobgbn[0] == "end" )
        			{
        				evtItemEditEventInfo.editMode = "end";
        				obj.style.setStyleValue( "cursor", "normal", jobgbn[1] );
        			}
        			else
        			{
        				evtItemEditEventInfo.editMode = "move";
        				obj.style.setStyleValue( "cursor", "normal", jobgbn[1] );
        			}
        		},
        		_evtItemDragStartHandler: function(obj,x,y)
        		{
        			var evtItemEditEventInfo = this._evtItemEditEventInfo;
        			if ( evtItemEditEventInfo )
        			{
        				if ( evtItemEditEventInfo.gbn == "head" )
        				{
        					var headComp = this._headComp;
        					x = obj.getOffsetLeft() - headComp.getOffsetLeft() + x;
        					y = obj.getOffsetTop() - headComp.getOffsetTop() + y;
        				}
        				else
        				{
        					x = obj.getOffsetLeft() + x - this._bodyBandArea.rowHeadArea.width;
        					y = obj.getOffsetTop() + y;
        				}
        				var data = this._data,
        					evtItem, editBaseDt;

        				if ( obj._ecoObj )
        				{
        					evtItem = obj._ecoObj._eventItem;
        				}
        				else
        				{
        					evtItem = obj._eventItem;
        				}

        				if ( this._selectedEvtItem != evtItem )
        				{
        					var evt = {
        						oldEventItem: this._selectedEvtItem,
        						newEventItem: evtItem
        					};
        					this._drawSelectedEvtItem(evtItem, this._selectedEvtItem);
        					this._selectedEvtItem = evtItem;
        					this.fireEvent("oneventitemselectchanged", evt);
        				}

        				evtItemEditEventInfo.editedEvtItem = evtItem;
        				editBaseDt = evtItemEditEventInfo.editMode == "end" ? evtItem.enddate : evtItem.startdate;

        				evtItemEditEventInfo.editBaseDt = data._cloneDate(editBaseDt);
        				evtItemEditEventInfo.dragStartDt = this._getDateFromPt(evtItemEditEventInfo.gbn, x, y, evtItemEditEventInfo.isDayUnit);
        				evtItemEditEventInfo.isEditing = true;
        				evtItemEditEventInfo.startX = x;
        				evtItemEditEventInfo.startY = y;
        			}
        		},
        		_evtItemDraggingHandler: function(offsetX,offsetY,obj)
        		{
        			var evtItemEditEventInfo = this._evtItemEditEventInfo;
        			if ( evtItemEditEventInfo && evtItemEditEventInfo.isEditing )
        			{
        				var x = evtItemEditEventInfo.startX + offsetX;
        				var y = evtItemEditEventInfo.startY + offsetY;
        				var dt = this._getDateFromPt(evtItemEditEventInfo.gbn, x, y, evtItemEditEventInfo.isDayUnit);
        				var curdt = new Date(evtItemEditEventInfo.editBaseDt.getTime() + (dt.getTime() - evtItemEditEventInfo.dragStartDt.getTime()));
        				this._moveOrResizeEvtItem(evtItemEditEventInfo.editedEvtItem, evtItemEditEventInfo.editMode, curdt, obj);
        			}
        		},
        		_evtItemDragEndHandler: function(obj,x,y)
        		{
        			var evtItemEditEventInfo = this._evtItemEditEventInfo;
        			if ( evtItemEditEventInfo )
        			{
        				var evtItem = evtItemEditEventInfo.editedEvtItem;
        				if ( evtItemEditEventInfo.isEditing && evtItem )
        				{
        					this._data.applyToDS(evtItem.key);
        				}
        				evtItemEditEventInfo.isEditing = false;
        				evtItemEditEventInfo.isDayUnit = null;
        				evtItemEditEventInfo.editBaseDt = null;
        				evtItemEditEventInfo.dragStartDt = null;
        				evtItemEditEventInfo.gbn = null;
        				evtItemEditEventInfo.editMode = null;
        				evtItemEditEventInfo.startX = null;
        				evtItemEditEventInfo.startY = null;
        				evtItemEditEventInfo.dt = null;
        			}
        		},
        		_moveOrResizeEvtItem: function(evtItem,editKind,dt,comp)
        		{
        			var data = this._data,
        				orgSdt = data._cloneDate(evtItem.startdate),
        				orgEdt = data._cloneDate(evtItem.enddate),
        				sdt, edt,
        				isChanged = false;

        			if ( editKind == "move" )
        			{
        				if ( orgSdt.getTime() != dt.getTime() )
        				{
        					var gap = orgEdt.getTime() - orgSdt.getTime();
        					sdt = new Date(dt.getTime());
        					edt = new Date(dt.getTime() + gap);
        					isChanged = true;
        				}
        			}
        			else if ( editKind == "start" )
        			{
        				if ( orgSdt.getTime() != dt.getTime() )
        				{
        					sdt = new Date(dt.getTime());
        					edt = orgEdt;
        					isChanged = true;
        				}
        			}
        			else if ( editKind == "end" )
        			{
        				if ( orgEdt.getTime() != dt.getTime() )
        				{
        					sdt = orgSdt;
        					edt = new Date(dt.getTime());
        					isChanged = true;
        				}
        			}

        			if ( !isChanged ) return false;

        			if ( sdt > edt )
        			{
        				var tmpdate = sdt;
        				sdt = edt;
        				edt = tmpdate;
        			}

        			if ( orgSdt.getTime() == sdt.getTime() && orgEdt.getTime() == edt.getTime() )
        			{
        				return false;
        			}

        			data.updateEventItemTimeValue(evtItem, sdt, edt);

        			this._drawEventItem(evtItem, comp);
        			return true;
        		},
        		_getKindOfJob: function(data,comp,x,y)
        		{
        			if ( data._mode == "month" && ( (comp instanceof Static ) && comp._eventItem ) )
        			{
        				//labelcomp
        				return ["move", "move", false, comp._eventItem, "body"]; //jobmode, cursor, isDayData, isHeadorBody
        			}
        			else
        			{
        				var isVert = (comp._ecoObj instanceof Eco.Scheduler.Vertical );
        				var baseSize = isVert ?  comp.getOffsetHeight() : comp.getOffsetWidth();
        				var eventCompToRangeMap, range,
        					gbn,
        					evtItem = comp._ecoObj._eventItem;
        				if ( this._schedularComp === comp.parent )
        				{
        					eventCompToRangeMap = this._allDayEventCompToRangeMap;
        					gbn = "head";
        				}
        				else
        				{
        					eventCompToRangeMap = this._bodyEventCompToRangeMap;
        					gbn = "body";
        				}
        				var nPos =  isVert ? y : x;
        				if ( nPos <= 6 )
        				{
        					range = eventCompToRangeMap[comp._id];
        					if ( evtItem.startdate.getTime() == range[0].getTime() )
        					{
        						return ["start", (isVert ? "n-resize": "w-resize"), (isVert ? false: true), evtItem, gbn];
        					}
        					else
        					{
        						return ["move", "move", (isVert ? false: true), evtItem, gbn];
        					}
        				}
        				else if ( nPos >= (baseSize - 6) )
        				{
        					range = eventCompToRangeMap[comp._id];
        					if ( evtItem.enddate.getTime() == range[1].getTime() )
        					{
        						return ["end", (isVert ? "s-resize": "e-resize"), (isVert ? false: true), evtItem, gbn];
        					}
        					else
        					{
        						return ["move", "move", (isVert ? false: true), evtItem, gbn];
        					}
        				}
        				return ["move", "move", (isVert ? false: true), evtItem, gbn];
        			}
        		},
        		_drawEventItem: function(evtItem,comp)
        		{
        			var data = this._data;
        			if ( data._mode == "month" )
        			{
        				this._drawBodyEventItemMonthMode(evtItem, data, comp);
        			}
        			else
        			{
        				this._drawAlldayEventItem(evtItem, data, comp);
        				this._drawBodyEventItemWeekMode(evtItem, data, comp);
        			}
        			this._applyRender();
        			if ( evtItem === this._selectedEvtItem )
        			{
        				this._drawSelectedEvtItem(this._selectedEvtItem);
        			}
        		},
        		_drawTimeRangeSelection: function(sdt,edt)
        		{
        			var data = this._data;
        			var calendar = data._calendar;
        			var bodyGrp = this._bodyGrp;
        			var layer = bodyGrp.getChildByIndex(0);
        			var timeRangeFillBrush = this._timeRangeFillBrush,
        				timeRangeGradation = this._timeRangeGradation;

        			var dayUnit = Eco.TimeUnit.DAY;

        			//row별로 selection 처리
        			if ( data._mode == "month" )
        			{
        				var drawSdate, drawEdate;
        				var yBase = 0,
        					yPos, xPos, x0, x1,
        					rowHeight = data._rowHeight,
        					headHeight = this.getCellHeadHeight(),
        					heightRect = rowHeight - headHeight,
        					xBase = this._bodyBandArea.rowHeadArea.width;

        				var tmpSdt = data._cloneDate(data._visibleStartdate);
        				var tmpEdt = calendar.addUnits(tmpSdt, dayUnit, 7);
        				var overlap,
        					refElem = layer.getElementById("bodylinesgroup"),
        					bodySelectionElems = this._bodySelectionElems||[],
        					bodySelectionElem,
        					elemsSeq = 0;

        				for ( var i = 0, len = data._rowCount; i < len ; i++ )
        				{
        					overlap = data.calcDateRangeOverlap(tmpSdt, tmpEdt, sdt, edt);
        					if ( overlap )
        					{
        						yPos = yBase + headHeight;
        						drawSdate = calendar.floor(overlap[0], dayUnit, 1);
        						drawEdate = calendar.floor(overlap[1], dayUnit, 1);
        						x0 = data.calcHProjection(tmpSdt, drawSdate);
        						x1 = data.calcHProjection(tmpSdt, drawEdate);
        						bodySelectionElem = bodySelectionElems[elemsSeq];
        						if ( !bodySelectionElem )
        						{
        							bodySelectionElem = new Eco.GraphicRect();
        							layer.insertBefore(bodySelectionElem, refElem);
        							bodySelectionElems[elemsSeq] = bodySelectionElem;
        						}
        						bodySelectionElem.setFillbrush(timeRangeFillBrush);
        						bodySelectionElem.setFillgradation(timeRangeGradation);
        						bodySelectionElem.setX(x0 + xBase);
        						bodySelectionElem.setY(yPos);
        						bodySelectionElem.setWidth(x1 - x0);
        						//console.log("xBase", xBase, "x0", x0, "x1", x1);
        						bodySelectionElem.setHeight(heightRect);
        						elemsSeq++;
        					}
        					tmpSdt = data._cloneDate(tmpEdt);
        					tmpEdt = calendar.addUnits(tmpSdt, dayUnit, 7);
        					yBase += rowHeight;
        				}
        				for ( var i = elemsSeq, len = bodySelectionElems.length ; i < len ; i++ )
        				{
        					layer.removeChild(bodySelectionElems[i]);
        				}
        				bodySelectionElems.length = elemsSeq;
        				this._bodySelectionElems = bodySelectionElems;
        				var headSelectionElem = this._headSelectionElem;
        				if ( headSelectionElem )
        				{
        					var headComp = this._headComp;
        					var headLayer = headComp.getChildByIndex(0);
        					var headband = headLayer.getElementById("headband");
        					headband.removeChild(headSelectionElem);
        					this._headSelectionElem = null;
        				}
        			}
        			else
        			{
        				var tmpSdt = data._cloneDate(data._visibleStartdate);
        				var tmpEdt = calendar.addUnits(tmpSdt, dayUnit, 1);
        				var overlap,
        					bodyHeight = this._bodyBandArea.cellsArea.height,
        					columnWidth = data._columnWidth,
        					refElem = layer.getElementById("bodylinesgroup"),
        					bodySelectionElems = this._bodySelectionElems||[],
        					bodySelectionElem,
        					xBase = this._bodyBandArea.rowHeadArea.width,
        					xPos = xBase,
        					elemsSeq = 0;

        				for ( var i = 0, len = data._columnCount; i < len ; i++ )
        				{
        					overlap = data.calcDateRangeOverlap(tmpSdt, tmpEdt, sdt, edt);
        					if ( overlap )
        					{
        						y0 = data.calcVProjection(tmpSdt, overlap[0], bodyHeight);
        						y1 = data.calcVProjection(tmpSdt, overlap[1], bodyHeight);
        						bodySelectionElem = bodySelectionElems[elemsSeq];
        						if ( !bodySelectionElem )
        						{
        							bodySelectionElem = new Eco.GraphicRect();
        							layer.insertBefore(bodySelectionElem, refElem);
        							bodySelectionElems[elemsSeq] = bodySelectionElem;
        						}
        						bodySelectionElem.setFillbrush(timeRangeFillBrush);
        						bodySelectionElem.setFillgradation(timeRangeGradation);
        						bodySelectionElem.setX(xPos);
        						bodySelectionElem.setY(y0);
        						bodySelectionElem.setWidth(columnWidth);
        						bodySelectionElem.setHeight(y1 - y0);
        						elemsSeq++;
        					}
        					tmpSdt = data._cloneDate(tmpEdt);
        					tmpEdt = calendar.addUnits(tmpSdt, dayUnit, 1);
        					xPos += columnWidth;
        				}
        				for ( var i = elemsSeq, len = bodySelectionElems.length ; i < len ; i++ )
        				{
        					layer.removeChild(bodySelectionElems[i]);
        				}
        				bodySelectionElems.length = elemsSeq;
        				this._bodySelectionElems = bodySelectionElems;
        				this._headBandSelectionDraw(sdt, edt);
        			}
        		},
        		_headBandSelectionDraw: function(sdt,edt)
        		{
        			var data = this._data;
        			if ( !data ) return;
        			var calendar = data._calendar;
        			var end = calendar.addUnits(data._visibleEnddate, Eco.TimeUnit.DAY, 1),
        				visibleStartDt = data._visibleStartdate,
        				visibleEndDt = data._visibleEnddate;
        			var range = data.calcDateRangeOverlap(visibleStartDt, end, sdt, edt);
        			var headSelectionElem = this._headSelectionElem,
        				headComp = this._headComp;

        			if (range !== null)
        			{
        				if ( data.isDayEvent(null, sdt, edt) )
        				{
        					var sdate, edate;
        					if (range[0] < visibleStartDt )
        					{
        						sdate = data._cloneDate(visibleStartDt);
        					}
        					else
        					{
        						sdate = data._cloneDate(range[0]);
        					}
        					var tmpdate = calendar.addUnits(visibleEndDt, Eco.TimeUnit.DAY, 1);
        					if (range[1] > tmpdate )
        					{
        						edate = data._cloneDate(tmpdate);
        					}
        					else
        					{
        						edate = data._cloneDate(range[1]);
        					}
        					//그린다.

        					if ( !headSelectionElem )
        					{
        						var layer = headComp.getChildByIndex(0),
        							headband = layer.getElementById("headband");
        						var headVertLines = this._headVertLines;
        						var refElem = headVertLines[0];
        						headSelectionElem = new Eco.GraphicRect();
        						headband.insertBefore(headSelectionElem, refElem);
        						this._headSelectionElem = headSelectionElem;
        					}
        					var x0 = data.calcHProjection(visibleStartDt, sdate);
        					var x1 = data.calcHProjection(visibleStartDt, edate);
        					headSelectionElem.setX(x0);
        					headSelectionElem.setY(this._headBandArea.colHeadArea.height);
        					headSelectionElem.setWidth(x1 - x0);
        					headSelectionElem.setHeight(this._headBandArea.allDayArea.height - this.eventItemBottomPadding);
        					headSelectionElem.setFillbrush(this._timeRangeFillBrush);
        					headSelectionElem.setFillgradation(this._timeRangeGradation);
        					headSelectionElem.setVisible(true);
        					return;
        				}
        			}
        			//지운다.
        			if ( headSelectionElem )
        			{
        				headSelectionElem.setVisible(false);
        			}
        		},
        		_getColHeadDisplayDates: function(data)
        		{
        			var tmpdt = data._cloneDate(data._visibleStartdate);
        			var visibledates = [];
        			visibledates._todayIndex = -1;
        			var cnt = data._columnCount,
        				calendar = data._calendar,
        				dateUtil = Eco.date,
        				format = "yyyyMMdd",
        				dayUnit = Eco.TimeUnit.DAY;
        			var today = new Date(),
        				todayYY = today.getFullYear(),
        				todayMM = today.getMonth(),
        				todayDD = today.getDate();
        			for ( var i = 0; i < cnt ; i++)
        			{
        				visibledates[i] = {
        					val: tmpdt, 
        					key: dateUtil.getMaskFormatString(tmpdt, format),
        					isToday: ( todayYY == tmpdt.getFullYear() &&
        							 todayMM == tmpdt.getMonth() &&
        							 todayDD == tmpdt.getDate() )
        				};
        				if ( visibledates[i].isToday )
        				{
        					visibledates._todayIndex = i;
        				}
        				tmpdt = calendar.addUnits(tmpdt, dayUnit, 1);
        			}
        			return visibledates;
        		},
        		_getColHeadLabelFormat: function(mode,kind)
        		{
        			if (mode == "month")
        			{
        				switch (kind)
        				{
        					case 0:
        					case 1:
        						return "ddd";
        					case 2:
        						return "dddd";
        				}
        			}
        			else
        			{
        				switch (kind)
        				{
        					case 0:
        						return "ddd mm/dd";
        					case 1:
        						return "dddd,MMM dd";
        					case 2:
        						return "dddd,MMMM dd";
        				}
        			}
        		},
        		_calcColTexts: function(dates,mode,kind)
        		{
        			var format = this._getColHeadLabelFormat(mode, kind),
        				dataUtil = Eco.date,
        				szUtil = Eco.XComp.PositionSize,
        				texts = [],
        				maxWidth = 0,
        				font = this._colHeaderFont;

        			for ( var i = 0, len = dates.length ; i < len ; i++ )
        			{
        				texts[i] = dataUtil.getMaskFormatString(dates[i].val, format);
        				sz = szUtil.getTextSize(font, texts[i]);
        				maxWidth = Math.max(maxWidth, sz[0]);
        			}
        			return [maxWidth, texts];
        		},
        		getColHeadCurKind: function(data)
        		{
        			var columnWidth = data._columnWidth,
        				labelsInfo = this._colLabelsInfo,
        				paddingVal = this._colHeadHorzPadding;

        			var bigW = labelsInfo[2][0] + paddingVal,
        				middleW = labelsInfo[1][0] + paddingVal,
        				smallW = labelsInfo[0][0] + paddingVal;

        			if ( columnWidth >= middleW && columnWidth < bigW )
        			{
        				return 1;
        			}
        			else if ( columnWidth >= bigW )
        			{
        				return 2;
        			}
        			else
        			{
        				data._columnWidth = Math.max(columnWidth, smallW);
        				return 0;
        			}
        		},
        		_drawSelectedEvtItem: function(evtItem,oldEvtItem)
        		{
        			if ( oldEvtItem )
        			{
        				var comps = this._getCompsByEvtItem(oldEvtItem);
        				if ( comps )
        				{
        					var comp, realcomp,
        						HorizontalCls = Eco.Scheduler.Horizontal,
        						VerticalCls = Eco.Scheduler.Vertical;
        					for ( var i = 0, len = comps.length ; i < len ; i++ )
        					{
        						comp = comps[i];
        						if ( (comp instanceof HorizontalCls) ||
        							(comp instanceof VerticalCls) )
        						{
        							realcomp = comp._comp;
        							if ( realcomp && realcomp._orgBorderstr )
        							{
        								realcomp.style.set_border(realcomp._orgBorderstr);
        							}
        						}
        						else if ( comp && comp._orgBorderstr )
        						{
        							comp.style.set_border(comp._orgBorderstr);
        						}
        					}
        				}
        			}

        			if ( evtItem )
        			{
        				var comps = this._getCompsByEvtItem(evtItem);
        				if ( comps )
        				{
        					var comp, realcomp,
        						HorizontalCls = Eco.Scheduler.Horizontal,
        						VerticalCls = Eco.Scheduler.Vertical;
        					for ( var i = 0, len = comps.length ; i < len ; i++ )
        					{
        						comp = comps[i];
        						if ( (comp instanceof HorizontalCls) ||
        							(comp instanceof VerticalCls) )
        						{
        							realcomp = comp._comp;
        							if ( realcomp )
        							{
        								if ( !realcomp._orgBorderstr ) realcomp._orgBorderstr = ( realcomp.style.border ? realcomp.style.border.toString() : "0 none transparent" );
        								realcomp.style.set_border("1 solid red");
        							}
        						}
        						else
        						{
        							if ( !comp._orgBorderstr ) comp._orgBorderstr = ( comp.style.border ? comp.style.border.toString() : "0 none transparent" );
        							comp.style.set_border("1 solid red");
        						}
        					}
        				}
        			}
        		},
        		_drawCompsTextByEvtItem: function(evtItem)
        		{
        			var comps = this._getCompsByEvtItem(evtItem);
        			if ( comps )
        			{
        				var comp, txt, factory = Eco.XComp.Factory,
        					HorizontalCls = Eco.Scheduler.Horizontal,
        					VerticalCls = Eco.Scheduler.Vertical;
        				for ( var i = 0, len = comps.length ; i < len ; i++ )
        				{
        					comp = comps[i];
        					if ( (comp instanceof HorizontalCls) ||
        						(comp instanceof VerticalCls) )
        					{
        						comp.updateText();
        					}
        					else
        					{
        						txt = Eco.date.getMaskFormatString(evtItem.startdate, "HH:mm") + " " + evtItem.summary;
        						factory.setProperties(comp, "text", txt);
        					}
        				}
        			}
        		},
        		_getCompsByEvtItem: function(evtItem)
        		{
        			if ( evtItem )
        			{
        				var data = this._data,
        					calendar = data._calendar,
        					sdt = data._visibleStartdate,
        					edt = calendar.addUnits(data._visibleEnddate, Eco.TimeUnit.DAY, 1);

        				if ( data.isEventInRange(evtItem, sdt,edt) )
        				{
        					var ret = [], comp;
        					if ( data._mode == "month" )
        					{
        						if ( data.isDayEvent(evtItem) )
        						{
        							var dayEventComps = this._dayEventComps,
        								comps, key = evtItem.key;
        							for ( var row = 0, len = data._rowCount ; row < len ; row++ )
        							{
        								comps = dayEventComps[row];
        								if ( comps )
        								{
        									comp = comps[key];
        									if ( comp ) ret.push(comp);
        								}
        							}
        						}
        						else
        						{
        							var labelComps = this._labelComps, 
        								labelKey, key = evtItem.key,
        								rowCount = data._rowCount,
        								founded = false,
        								columnCount = data._columnCount;
        							for ( var row = 0 ; row < rowCount ; row++ )
        							{
        								for ( var col = 0 ; col < columnCount ; col++ )
        								{
        									labelKey = row + "_" + col + key;
        									comp = labelComps[labelKey];
        									if ( comp )
        									{
        										ret.push(comp);
        										founded = true;
        									}
        								}
        								if ( founded ) break;
        							}
        						}
        					}
        					else
        					{
        						if ( data.isDayEvent(evtItem) )
        						{
        							var allDayEventComps = this._allDayEventComps;
        							comp = allDayEventComps[evtItem.key];
        							if ( comp )
        							{
        								ret.push(comp);
        							}
        						}
        						else
        						{
        							var timeEventComps = this._timeEventComps,
        								comps, key = evtItem.key;
        							for ( var col = 0, len = data._columnCount ; col < len ; col++ )
        							{
        								comps = timeEventComps[col];
        								if ( comps )
        								{
        									comp = comps[key];
        									if ( comp ) ret.push(comp);
        								}
        							}
        						}
        					}
        					return ret;
        				}
        			}
        			return null;
        		},
        		_allDayEventItemsDrawClear: function()
        		{
        			var oldAllDayEventComps = this._allDayEventComps;
        			if ( oldAllDayEventComps )
        			{
        				var allDayEventCompToRangeMap = this._allDayEventCompToRangeMap,
        					pThis = this,
        					eventUtil = Eco.XComp.Event;
        				Eco.object.Each(oldAllDayEventComps, function(prop, val) {
        					var comp = val._comp;
        					if ( comp )
        					{
        						comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        						eventUtil.clearDraggable(comp);
        						delete allDayEventCompToRangeMap[comp._id];
        					}
        					val.clear();
        				});
        				this._allDayEventComps = null;
        				var allDayEventCompToRangeMap = null;
        			}
        		},
        		_drawAlldayEventItem: function(evtItem,data)
        		{
        			if ( data._mode == "month" ) return;

        			this._layout(data);

        			var factory = Eco.XComp.Factory,
        				headBandArea = this._headBandArea,
        				headComp = this._headComp;

        			factory.setRect(headComp, headBandArea.x, headBandArea.y, headBandArea.width, headBandArea.height);

        			var bodyBandArea = this._bodyBandArea,
        					bodyDiv = this._bodyComp;

        			factory.setRect(bodyDiv, bodyBandArea.x, bodyBandArea.y, bodyBandArea.width + this.vertscrollwidth, bodyBandArea.height);
        			this._allDayEventItemsDraw(data, false);
        		},
        		_allDayEventItemsDraw: function(data,isStyle)
        		{
        			var dayEvents = data._dayEvents,
        				calendar = data._calendar,
        				evtItem, overlap,
        				sdt = data._visibleStartdate,
        				edt = calendar.addUnits(data._visibleEnddate, Eco.TimeUnit.DAY, 1);

        			var edt0 = calendar.floor(edt, Eco.TimeUnit.DAY, 1);

        			var oldAllDayEventComps = this._allDayEventComps||{},
        				allDayEventCompToRangeMap = this._allDayEventCompToRangeMap||{},
        				newAllDayEventComps = {}, key,
        				pDiv = this._schedularComp;

        			var draggingFunc = {
        					'start': this._evtItemDragStartHandler,
        					'dragging': this._evtItemDraggingHandler,
        					'end': this._evtItemDragEndHandler
        				},
        				eventUtil = Eco.XComp.Event;

        			if ( dayEvents )
        			{
        				var horzStyleArr = this._getStyleHorzEventItem();
        				if ( isStyle )
        				{
        					if ( oldAllDayEventComps )
        					{
        						Eco.object.Each(oldAllDayEventComps, function(prop, val) {
        							val.draw(horzStyleArr[0], horzStyleArr[1], true);
        						});
        					}
        				}
        				else
        				{
        					var overLapLayout = this._allDayOverLapLayout, layout;
        					var eventItemHeight = this.getEventItemHeight(),
        						headBandArea = this._headBandArea, eventComp,
        						xGap = headBandArea.x,
        						yGap = headBandArea.colHeadArea.height;
        					for ( var i = 0, len = dayEvents.length ; i < len ; i++ )
        					{
        						evtItem = dayEvents[i];
        						overlap = data.calcDateRangeOverlap(sdt, edt0, evtItem.startdate, evtItem.enddate);
        						if ( overlap )
        						{
        							key = evtItem.key;
        							eventComp = oldAllDayEventComps[key];
        							if ( !eventComp )
        							{
        								eventComp = new Eco.Scheduler.Horizontal(pDiv, evtItem);
        							}
        							else
        							{
        								delete oldAllDayEventComps[key];
        							}
        							newAllDayEventComps[key] = eventComp;
        							x0 = data.calcHProjection(sdt, overlap[0]);
        							x1 = data.calcHProjection(sdt, overlap[1]);
        							layout = data.getOverlapLayoutInfo(overLapLayout, evtItem);
        							y = 2 + layout.offset * (eventItemHeight + 2);
        							eventComp.draw(horzStyleArr[0], horzStyleArr[1], false, sdt, edt, overlap, xGap + x0, yGap + y, x1 - x0, eventItemHeight);
        							eventComp._comp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        							eventUtil.makeDraggable(eventComp._comp, draggingFunc, this, [eventComp._comp], true);
        							allDayEventCompToRangeMap[eventComp._comp._id] = overlap;
        						}
        					}
        					if ( oldAllDayEventComps )
        					{
        						var pThis = this;
        						Eco.object.Each(oldAllDayEventComps, function(prop, val) {
        							var comp = val._comp;
        							if ( comp )
        							{
        								comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        								delete allDayEventCompToRangeMap[comp._id];
        								eventUtil.clearDraggable(comp);
        							}
        							val.clear();
        						});
        					}
        					this._allDayEventComps = newAllDayEventComps;
        					this._allDayEventCompToRangeMap = allDayEventCompToRangeMap;
        				}
        			}
        			else //clear
        			{
        				if ( oldAllDayEventComps )
        				{
        					var pThis = this;
        					Eco.object.Each(oldAllDayEventComps, function(prop, val) {
        						var comp = val._comp;
        						if ( comp )
        						{
        							comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        							delete allDayEventCompToRangeMap[comp._id];
        							eventUtil.clearDraggable(comp);
        						}
        						val.clear();
        					});
        				}
        			}
        			this._changedStyleHorzEventItem = this._changedStyleHorzEventItem & (~2);
        		},
        		_bodyEventItemsMonthModeDrawClear: function()
        		{
        			var dayEventComps = this._dayEventComps,
        				oldDayEventComps,
        				factory = Eco.XComp.Factory,
        				eventUtil = Eco.XComp.Event,
        				bodyEventCompToRangeMap = this._bodyEventCompToRangeMap;
        			var pThis = this;

        			if ( dayEventComps )
        			{
        				for ( var i = 0, len = dayEventComps.length ; i < len ; i++ )
        				{
        					oldDayEventComps = dayEventComps[i];
        					if ( oldDayEventComps )
        					{
        						delete oldDayEventComps["_cnt"];
        						Eco.object.Each(oldDayEventComps, function(prop, val) {
        							var comp = val._comp;
        							if ( comp )
        							{
        								comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        								eventUtil.clearDraggable(comp);
        								delete bodyEventCompToRangeMap[comp._id];
        							}
        							val.clear();
        						});
        					}
        				}
        				this._dayEventComps = null;
        			}
        			var oldLabelComps = this._labelComps;
        			if ( oldLabelComps )
        			{
        				Eco.object.Each(oldLabelComps, function(prop, val) {
        					delete bodyEventCompToRangeMap[val._id];
        					val._eventItem = null;
        					val.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        					eventUtil.clearDraggable(val);
        					factory.release(val, null, true);
        				});
        				this._labelComps = null;
        			}

        			var drillButtonCompsByRow = this._drillButtonCompsByRow;
        			if ( drillButtonCompsByRow )
        			{
        				var drillButtonComps;
        				for ( var i = 0, len = drillButtonCompsByRow.length ; i < len ; i++ )
        				{
        					drillButtonComps = drillButtonCompsByRow[i];
        					if ( drillButtonComps )
        					{
        						Eco.object.Each(drillButtonComps, function(prop, val) {
        							val._col = null;
        							val._row = null;
        							val._keydate = null;
        							val.removeEventHandler("onclick", pThis._drillBtnClickHandler, pThis);
        							factory.release(val, null, true);
        						});
        					}
        				}
        				this._drillButtonCompsByRow = null;
        			}
        			this._bodyDrawDayEvents = [];
        			this._bodyDrawTimeEventsAtMonth = [];
        		},
        		_sort_func: function(a,b)
        		{
        			if ( a.startdate.getTime() == b.startdate.getTime() ) return 0;
        			return a.startdate > b.startdate ? (1) : (-1);
        		},
        		_drawBodyEventItemMonthMode: function(evtItem,data,comp)
        		{
        			var bodyEventCompToRangeMap = this._bodyEventCompToRangeMap,
        				calendar = data._calendar,
        				sdt = data._visibleStartdate,
        				pDiv = this._bodyComp,
        				invalidatedEventRows = [];

        			var draggingFunc = {
        					'start': this._evtItemDragStartHandler,
        					'dragging': this._evtItemDraggingHandler,
        					'end': this._evtItemDragEndHandler
        				},
        				eventUtil = Eco.XComp.Event;

        			if ( data.isDayEvent(evtItem) )
        			{
        				var dayEventComps = this._dayEventComps||[],
        					bodyDrawDayEvents = this._bodyDrawDayEvents,
        					newDayEventComps, oldDayEventComps, key,
        					columnCount = data._columnCount,
        					dayUnit = Eco.TimeUnit.DAY,
        					tmpEvtItems, weekstartdt, weekenddt,
        					eventComp;

        				var availEventComps = [];

        				if ( comp )
        				{
        					availEventComps[0] = comp._ecoObj;
        				}
        				if ( !bodyDrawDayEvents )
        				{
        					bodyDrawDayEvents = [];
        					this._bodyDrawDayEvents = bodyDrawDayEvents;
        				}

        				for ( var i = 0, len = data._rowCount ; i < len ; i++ )
        				{
        					weekstartdt = calendar.addUnits(sdt, dayUnit, i*columnCount);
        					weekenddt = calendar.addUnits(weekstartdt, dayUnit, columnCount);
        					overlap = data.calcDateRangeOverlap(weekstartdt, weekenddt, evtItem.startdate, evtItem.enddate);
        					if ( overlap )
        					{
        						invalidatedEventRows.push(i);
        						oldDayEventComps = dayEventComps[i];
        						eventComp = oldDayEventComps[evtItem.key];
        						tmpEvtItems = bodyDrawDayEvents[i];
        						if ( eventComp && eventComp._comp === comp )
        						{
        							delete oldDayEventComps[evtItem.key];
        							eventComp = null;
        						}
        						if ( !eventComp && availEventComps.length )
        						{
        							eventComp = availEventComps.pop();
        							oldDayEventComps[evtItem.key] = eventComp;
        						}

        						if ( !eventComp )
        						{
        							eventComp = new Eco.Scheduler.Horizontal(pDiv, evtItem, true);
        							eventComp._comp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        							eventUtil.makeDraggable(eventComp._comp, draggingFunc, this, [eventComp._comp], true);
        							oldDayEventComps[evtItem.key] = eventComp;
        							//console.log("111111 create row", i , eventComp);
        						}

        						var idx = Eco.array.indexOf(tmpEvtItems, evtItem);
        						if ( idx < 0 )
        						{
        							tmpEvtItems.push(evtItem);
        							tmpEvtItems.sort(this._sort_func);
        						}
        						bodyEventCompToRangeMap[eventComp._comp._id] = overlap;
        					}
        					else
        					{
        						tmpEvtItems = bodyDrawDayEvents[i];
        						var idx = Eco.array.indexOf(tmpEvtItems, evtItem);
        						if ( idx > -1 )
        						{
        							tmpEvtItems.splice(idx, 1);
        							invalidatedEventRows.push(i);
        							oldDayEventComps = dayEventComps[i];
        							eventComp = oldDayEventComps[evtItem.key];
        							delete oldDayEventComps[evtItem.key];
        							if ( comp )
        							{
        								//console.log("deleted row", i , eventComp);
        								if ( eventComp && eventComp._comp === comp )
        								{
        									continue;
        								}
        							}
        							//console.log("1111 deleted row", i , eventComp);
        							if ( eventComp && eventComp._comp )
        							{
        								var comp = eventComp._comp;
        								if ( comp )
        								{
        									comp.removeEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        									eventUtil.clearDraggable(comp);
        									delete bodyEventCompToRangeMap[comp._id];
        								}
        								eventComp.clear();
        							}
        						}
        					}
        				}
        			}
        			else
        			{
        				var columnCount = data._columnCount,
        					dayUnit = Eco.TimeUnit.DAY,
        					cellStartdt, cellEnddt,
        					bodyDrawTimeEvents = this._bodyDrawTimeEventsAtMonth,
        					labelComps = this._labelComps,
        					factory = Eco.XComp.Factory,
        					overlap, eventComp;

        				if ( !bodyDrawTimeEvents )
        				{
        					bodyDrawTimeEvents = [];
        					this._bodyDrawTimeEventsAtMonth = bodyDrawTimeEvents;
        				}
        				for ( var row = 0, len = data._rowCount ; row < len ; row++ )
        				{
        					for ( var col = 0 ; col < columnCount ; col++ )
        					{
        						cellStartdt = calendar.addUnits(sdt, dayUnit, row*columnCount + col);
        						cellEnddt = calendar.addUnits(cellStartdt, dayUnit, 1);
        						overlap = data.calcDateRangeOverlap(cellStartdt, cellEnddt, evtItem.startdate, evtItem.enddate);
        						if ( !overlap )
        						{
        							bodyDrawTimeEventsByRow = bodyDrawTimeEvents[row];
        							if ( bodyDrawTimeEventsByRow )
        							{
        								bodyDrawTimeEventsByCell = bodyDrawTimeEventsByRow[col];
        								if ( bodyDrawTimeEventsByCell )
        								{
        									var idx = Eco.array.indexOf(bodyDrawTimeEventsByCell, evtItem);
        									if ( idx > -1 )
        									{
        										bodyDrawTimeEventsByCell.splice(idx, 1);
        										//console.log("delete==>", "pos", row, col, ":::", evtItem, idx, bodyDrawTimeEventsByCell);
        										invalidatedEventRows.push(row);
        										if ( comp )
        										{
        											delete labelComps[row + "_" + col + evtItem.key];
        										}
        										else
        										{
        											eventComp = labelComps[row + "_" + col + evtItem.key];
        											delete labelComps[row + "_" + col + evtItem.key];
        											if ( eventComp )
        											{
        												delete bodyEventCompToRangeMap[eventComp._id];
        												eventComp._eventItem = null;
        												eventComp.removeEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        												eventUtil.clearDraggable(eventComp);
        												factory.release(eventComp, null, true);
        											}
        											delete bodyEventCompToRangeMap[eventComp._id];
        										}
        									}
        								}
        							}
        						}
        						else
        						{
        							invalidatedEventRows.push(row);
        							bodyDrawTimeEventsByRow = bodyDrawTimeEvents[row];
        							if ( !bodyDrawTimeEventsByRow )
        							{
        								bodyDrawTimeEventsByRow = [];
        								bodyDrawTimeEvents[row] = bodyDrawTimeEventsByRow;
        							}
        							bodyDrawTimeEventsByCell = bodyDrawTimeEventsByRow[col];
        							if ( !bodyDrawTimeEventsByCell )
        							{
        								bodyDrawTimeEventsByCell = [];
        								bodyDrawTimeEventsByRow[col] = bodyDrawTimeEventsByCell;
        							}
        							var idx = Eco.array.indexOf(bodyDrawTimeEventsByCell, evtItem);
        							if ( idx < 0 )
        							{
        								bodyDrawTimeEventsByCell.push(evtItem);
        								bodyDrawTimeEventsByCell.sort(this._sort_func);
        								//console.log("insert==>", "pos", row, col, ":::", evtItem, bodyDrawTimeEventsByCell);
        							}

        							if ( comp )
        							{
        								labelComps[row + "_" + col + evtItem.key] = comp;
        								eventComp = comp;
        							}
        							else
        							{
        								eventComp = labelComps[row + "_" + col + evtItem.key];
        								if ( !eventComp )
        								{
        									//bodyDrawTimeEventsByCell.push(evtItem);
        									eventComp = factory.getXComp(pDiv, "Static");
        									eventComp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        									eventUtil.makeDraggable(eventComp, draggingFunc, this, [eventComp], true);
        									eventComp._eventItem = evtItem;
        									labelComps[row + "_" + col + evtItem.key] = eventComp;
        								}
        							}
        							bodyEventCompToRangeMap[eventComp._id] = overlap;
        						}
        					}
        				}
        			}
        			//console.log("labelComps===>",labelComps);
        			this._invalidatedEventRows = invalidatedEventRows;
        			if ( invalidatedEventRows.length ) this._bodyEventItemsMonthModeLayout(data);
        		},
        		_bodyEventItemsMonthModeDraw: function(data,isStyle)
        		{
        			var factory = Eco.XComp.Factory;
        			var draggingFunc = {
        					'start': this._evtItemDragStartHandler,
        					'dragging': this._evtItemDraggingHandler,
        					'end': this._evtItemDragEndHandler
        				},
        				eventUtil = Eco.XComp.Event;

        			if ( isStyle )
        			{
        				if ( this._changedStyleHorzEventItem & 4 )
        				{
        					var dayEventComps = this._dayEventComps,
        						oldDayEventComps;
        					if ( dayEventComps )
        					{
        						var styleArr = this._getStyleHorzEventItem();
        						for ( var i = 0, len = dayEventComps.length ; i < len ; i++ )
        						{
        							oldDayEventComps = dayEventComps[i];
        							if ( oldDayEventComps )
        							{
        								Eco.object.Each(oldDayEventComps, function(prop, val) {
        									if ( prop != "_cnt" ) val.draw(styleArr[0], styleArr[1], true);
        								});
        							}
        						}
        					}
        					this._changedStyleHorzEventItem = this._changedStyleHorzEventItem  & (~4);
        				}

        				var stylestr;
        				if ( this._changedStyleLabelEventItem & 2 )
        				{
        					var oldLabelComps = this._labelComps;
        					if ( oldLabelComps )
        					{
        						stylestr = this._getStyleLabelEventItem();
        						Eco.object.Each(oldDayEventComps, function(prop, val) {
        							factory.setProperties(val, "style", stylestr);
        						});
        					}
        					this._changedStyleLabelEventItem = this._changedStyleLabelEventItem & (~2);
        				}
        				if ( this._changedStyleDrillButton )
        				{
        					var drillButtonCompsByRow = this._drillButtonCompsByRow;
        					if ( drillButtonCompsByRow )
        					{
        						var drillButtonComps;
        						stylestr = this.drillButtonStyle;
        						for ( var i = 0, len = drillButtonCompsByRow.length ; i < len ; i++ )
        						{
        							drillButtonComps = drillButtonCompsByRow[i];
        							if ( drillButtonComps )
        							{
        								Eco.object.Each(drillButtonComps, function(prop, val) {
        									factory.setProperties(val, "style", stylestr);
        								});
        							}
        						}
        					}
        					this._changedStyleDrillButton = false;
        				}
        			}
        			else
        			{
        				var dayEvents = data._dayEvents,
        					timeEvents = data._timeEvents,
        					calendar = data._calendar,
        					evtItem, overlap,
        					bodyEventCompToRangeMap = this._bodyEventCompToRangeMap,
        					sdt = data._visibleStartdate,
        					pDiv = this._bodyComp;

        				var invalidatedEventRows = [], dupInvalidatedRows = {};
        				var pThis = this;
        				if ( dayEvents && dayEvents.length )
        				{
        					var dayEventComps = this._dayEventComps||[],
        						bodyDrawDayEvents = this._bodyDrawDayEvents,
        						newDayEventComps, oldDayEventComps, key,
        						columnCount = data._columnCount,
        						dayUnit = Eco.TimeUnit.DAY,
        						tmpEvtItems, weekstartdt, weekenddt;

        					if ( !bodyDrawDayEvents )
        					{
        						bodyDrawDayEvents = [];
        						this._bodyDrawDayEvents = bodyDrawDayEvents;
        					}

        					for ( var i = 0, len = data._rowCount ; i < len ; i++ )
        					{
        						weekstartdt = calendar.addUnits(sdt, dayUnit, i*columnCount);
        						weekenddt = calendar.addUnits(weekstartdt, dayUnit, columnCount);
        						tmpEvtItems = [];
        						oldDayEventComps = dayEventComps[i];
        						if ( !oldDayEventComps )
        						{
        							oldDayEventComps = {};
        							dayEventComps[i] = oldDayEventComps;
        						}
        						newDayEventComps = {"_cnt":0};

        						for ( var j = 0, jlen = dayEvents.length; j < jlen ; j++ )
        						{
        							evtItem = dayEvents[j];
        							overlap = data.calcDateRangeOverlap(weekstartdt, weekenddt, evtItem.startdate, evtItem.enddate);
        							if ( overlap )
        							{
        								tmpEvtItems.push(evtItem);
        								key = evtItem.key;
        								eventComp = oldDayEventComps[key];
        								if ( !eventComp )
        								{
        									eventComp = new Eco.Scheduler.Horizontal(pDiv, evtItem, true);
        									eventComp._comp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        									eventUtil.makeDraggable(eventComp._comp, draggingFunc, this, [eventComp._comp], true);
        								}
        								else
        								{
        									delete oldDayEventComps[key];
        								}
        								newDayEventComps[key] = eventComp;
        								newDayEventComps._cnt++;
        								bodyEventCompToRangeMap[eventComp._comp._id] = overlap;
        							}
        						}
        						dayEventComps[i] = newDayEventComps;
        						bodyDrawDayEvents[i] = tmpEvtItems;
        						invalidatedEventRows.push(i);
        						dupInvalidatedRows[i] = true;
        						delete oldDayEventComps["_cnt"];
        						Eco.object.Each(oldDayEventComps, function(prop, val) {
        							var comp = val._comp;
        							if ( comp )
        							{
        								comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        								eventUtil.clearDraggable(comp);
        								delete bodyEventCompToRangeMap[comp._id];
        							}
        							val.clear();
        						});
        					}
        					if ( dayEventComps.length > data._rowCount )
        					{
        						for ( var i = data._rowCount, len = dayEventComps.length ; i < len ; i++ )
        						{
        							oldDayEventComps = dayEventComps[i];
        							if ( oldDayEventComps )
        							{
        								delete oldDayEventComps["_cnt"];
        								Eco.object.Each(oldDayEventComps, function(prop, val) {
        									var comp = val._comp;
        									if ( comp )
        									{
        										comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        										eventUtil.clearDraggable(comp);
        										delete bodyEventCompToRangeMap[comp._id];
        									}
        									val.clear();
        								});
        							}
        						}
        					}
        					dayEventComps.length = data._rowCount;
        					this._dayEventComps = dayEventComps;
        				}
        				else
        				{
        					var dayEventComps = this._dayEventComps,
        						oldDayEventComps;

        					if ( dayEventComps )
        					{
        						for ( var i = 0, len = dayEventComps.length ; i < len ; i++ )
        						{
        							oldDayEventComps = dayEventComps[i];
        							if ( oldDayEventComps )
        							{
        								if ( oldDayEventComps._cnt )
        								{
        									if ( !dupInvalidatedRows[i] ) invalidatedEventRows.push(i);
        								}
        								delete oldDayEventComps["_cnt"];
        								Eco.object.Each(oldDayEventComps, function(prop, val) {
        									var comp = val._comp;
        									if ( comp )
        									{
        										comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        										eventUtil.clearDraggable(comp);
        										delete bodyEventCompToRangeMap[comp._id];
        									}
        									val.clear();
        								});
        							}
        						}
        						this._dayEventComps = null;
        					}
        					this._bodyDrawDayEvents = [];
        				}

        				if ( timeEvents && timeEvents.length )
        				{
        					var bodyDrawTimeEvents = [],
        						bodyDrawTimeEventsByRow, bodyDrawTimeEventsByCell,
        						oldLabelComps = this._labelComps||{},
        						newLabelComps = {},
        						labelKey, checkRow,
        						isMidnight, nDay, tmpDay, enddt, row, col;

        					this._bodyDrawTimeEventsAtMonth = bodyDrawTimeEvents;

        					for ( var i = 0, len = timeEvents.length ; i < len ; i++ )
        					{
        						evtItem = timeEvents[i];
        						isMidnight = false;
        						nDay = -1;
        						if ( evtItem.startdate >= sdt )
        						{
        							nDay = calendar.getDays(sdt, evtItem.startdate);
        							row = parseInt(nDay/columnCount);
        							if ( !dupInvalidatedRows[row] )
        							{
        								invalidatedEventRows.push(row);
        							}
        							col = nDay - row*columnCount;
        							bodyDrawTimeEventsByRow = bodyDrawTimeEvents[row];
        							if ( !bodyDrawTimeEventsByRow )
        							{
        								bodyDrawTimeEventsByRow = [];
        								bodyDrawTimeEvents[row] = bodyDrawTimeEventsByRow;
        							}
        							bodyDrawTimeEventsByCell = bodyDrawTimeEventsByRow[col];
        							if ( !bodyDrawTimeEventsByCell )
        							{
        								bodyDrawTimeEventsByCell = [];
        								bodyDrawTimeEventsByRow[col] = bodyDrawTimeEventsByCell;
        							}
        							bodyDrawTimeEventsByCell.push(evtItem);

        							labelKey = row + "_" + col + evtItem.key;
        							eventComp = oldLabelComps[labelKey];
        							if ( !eventComp )
        							{
        								eventComp = factory.getXComp(pDiv, "Static");
        								eventComp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        								eventUtil.makeDraggable(eventComp, draggingFunc, this, [eventComp], true);
        							}
        							else
        							{
        								delete oldLabelComps[labelKey];
        							}
        							daystartdt = calendar.addUnits(sdt, dayUnit, row*columnCount + col);
        							dayenddt = calendar.addUnits(daystartdt, dayUnit, 1);
        							eventComp._eventItem = evtItem;
        							overlap = data.calcDateRangeOverlap(daystartdt, dayenddt, evtItem.startdate, evtItem.enddate);
        							newLabelComps[labelKey] = eventComp;
        							bodyEventCompToRangeMap[eventComp._id] = overlap;
        						}
        						tmpDay = nDay;
        						enddt = evtItem.enddate;
        						nDay = calendar.getDays(sdt, enddt);
        						if ( enddt.getHours() == 0 && enddt.getMinutes() == 0 && enddt.getSeconds() == 0 )
        						{
        							isMidnight = enddt.getMilliseconds() == 0;
        						}
        						if ( nDay != tmpDay && !isMidnight )
        						{
        							row = parseInt(nDay / columnCount);
        							if ( !dupInvalidatedRows[row] )
        							{
        								invalidatedEventRows.push(row);
        							}
        							col = nDay - row * columnCount;
        							bodyDrawTimeEventsByRow = bodyDrawTimeEvents[row];
        							if ( !bodyDrawTimeEventsByRow )
        							{
        								bodyDrawTimeEventsByRow = [];
        								bodyDrawTimeEvents[row] = bodyDrawTimeEventsByRow;
        							}
        							bodyDrawTimeEventsByCell = bodyDrawTimeEventsByRow[col];
        							if ( !bodyDrawTimeEventsByCell )
        							{
        								bodyDrawTimeEventsByCell = [];
        								bodyDrawTimeEventsByRow[col] = bodyDrawTimeEventsByCell;
        							}
        							bodyDrawTimeEventsByCell.push(evtItem);

        							labelKey = row + "_" + col + evtItem.key;
        							eventComp = oldLabelComps[labelKey];
        							if ( !eventComp )
        							{
        								eventComp = factory.getXComp(pDiv, "Static");
        								eventComp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        								eventUtil.makeDraggable(eventComp, draggingFunc, this, [eventComp], true);
        							}
        							else
        							{
        								delete oldLabelComps[labelKey];
        							}
        							eventComp._eventItem = evtItem;
        							daystartdt = calendar.addUnits(sdt, dayUnit, row*columnCount + col);
        							dayenddt = calendar.addUnits(daystartdt, dayUnit, 1);
        							overlap = data.calcDateRangeOverlap(daystartdt, dayenddt, evtItem.startdate, evtItem.enddate);
        							newLabelComps[labelKey] = eventComp;
        							bodyEventCompToRangeMap[eventComp._id] = overlap;
        						}
        					}
        					Eco.object.Each(oldLabelComps, function(prop, val) {
        						if ( val )
        						{
        							delete bodyEventCompToRangeMap[val._id];
        						}
        						val._eventItem = null;
        						val.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        						eventUtil.clearDraggable(val);
        						factory.release(val, null, true);
        					});
        					this._labelComps = newLabelComps;
        				}
        				else
        				{
        					var oldLabelComps = this._labelComps;
        					if ( oldLabelComps )
        					{
        						Eco.object.Each(oldLabelComps, function(prop, val) {
        							var strrow = prop.substr(0, 1);
        							if ( !dupInvalidatedRows[strrow] )
        							{
        								invalidatedEventRows.push(parseInt(strrow));
        							}
        							delete bodyEventCompToRangeMap[val._id];
        							val._eventItem = null;
        							val.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        							eventUtil.clearDraggable(val);
        							factory.release(val, null, true);
        						});
        						this._labelComps = {};
        					}
        					/*
        					var drillButtonCompsByRow = this._drillButtonCompsByRow;
        					if ( drillButtonCompsByRow )
        					{
        						//for ( var i = 0
        						Eco.object.Each(oldDrillButtonComps, function(prop, val) {
        							val._col = null;
        							val._row = null;
        							factory.release(val, null, true);
        						});
        						this._drillButtonComps = {};
        					}
        					*/
        					this._bodyDrawTimeEventsAtMonth = [];
        				}
        				this._invalidatedEventRows = invalidatedEventRows;
        				if ( invalidatedEventRows.length ) this._bodyEventItemsMonthModeLayout(data);
        				this._changedStyleHorzEventItem = this._changedStyleHorzEventItem  & (~4);
        				this._changedStyleLabelEventItem = this._changedStyleLabelEventItem & (~2);
        				this._changedStyleDrillButton = false;
        			}
        		},
        		_bodyEventItemsMonthModeLayout: function(data)
        		{
        			var invalidatedEventRows = this._invalidatedEventRows,
        				bodyDrawDayEvents = this._bodyDrawDayEvents,
        				bodyDrawTimeEvents = this._bodyDrawTimeEventsAtMonth,
        				bodyDrawTimeEventsByRow, bodyDrawTimeEventsByCell,
        				dayEvents,
        				calendar = data._calendar,
        				visiblesdt = data._visibleStartdate,
        				columnCount = data._columnCount,
        				rowHeight = data._rowHeight,
        				dayUnit = Eco.TimeUnit.DAY,
        				col, row, timeEvents, evtItem, overlap;

        			var pDiv = this._bodyComp,
        				factory = Eco.XComp.Factory;

        			var pThis = this;

        			var overLapLayout = this._monthEventOverLapLayout,
        				tmplayoutinfo;
        			if ( !overLapLayout )
        			{
        				overLapLayout = data.createOverlapLayout(false, false);
        				this._monthEventOverLapLayout = overLapLayout;
        			}

        			var dayEventComps = this._dayEventComps,
        				labelComps = this._labelComps,
        				bodyEventCompToRangeMap = this._bodyEventCompToRangeMap,
        				eventComps, 
        				newDrillButtonComps, oldDrillButtonComps,
        				drillButtonCompsByRow = this._drillButtonCompsByRow||[],
        				cellKey,
        				labelKey,
        				cellsArea = this._bodyBandArea.cellsArea,
        				columnWidth = data._columnWidth,
        				columnCount = data._columnCount,
        				xBasePos = cellsArea.x,
        				drillButtonH = this.drillButtonHeight,
        				drillButtonW = this.drillButtonWidth,
        				headHeight = this.getCellHeadHeight(),
        				eventItemHeight = this.getEventItemHeight(),
        				dataUtil = Eco.date,
        				labelStylestr = this._getStyleLabelEventItem(),
        				horzStyleArr = this._getStyleHorzEventItem(),
        				drillBtnStylestr = this.drillButtonStyle;

        			var subRowMaxCnt = parseInt((data._rowHeight - this.getCellHeadHeight() - drillButtonH)/(eventItemHeight + 2)),
        				cellsbyRow = this._cellsByRow, subrow, cells;

        			var tmpCells = [], endcol;
        			for ( var empsubrow = 0; empsubrow < subRowMaxCnt; empsubrow++ )
        			{
        				tmpCells[empsubrow] = null;
        			}
        			//console.log("ssss===>", tmpCells, tmpCells.length);
        			if ( !cellsbyRow )
        			{
        				cellsbyRow = [];
        				this._cellsByRow = cellsbyRow;
        			}
        			for ( var i = 0, len = invalidatedEventRows.length ; i < len ; i++ )
        			{
        				row = invalidatedEventRows[i];
        				oldDrillButtonComps = drillButtonCompsByRow[row]||{};
        				newDrillButtonComps = {};
        				dayEvents = bodyDrawDayEvents[row];
        				sdt = calendar.addUnits(visiblesdt, dayUnit, row * columnCount);
        				edt = calendar.addUnits(sdt, dayUnit, columnCount);
        				data.calcOverlapping(overLapLayout, dayEvents||[], sdt, edt);
        				cells = [];
        				cellsbyRow[row] = cells;
        				if ( dayEvents && dayEvents.length )
        				{
        					eventComps = dayEventComps[row];
        					for ( var j = 0, jlen = dayEvents.length; j < jlen ; j++ )
        					{
        						evtItem = dayEvents[j];
        						eventComp = eventComps[evtItem.key];
        						tmplayoutinfo = data.getOverlapLayoutInfo(overLapLayout, evtItem);
        						ypos = headHeight + row * rowHeight + tmplayoutinfo.offset * (eventItemHeight + 2);
        						ylastpos = (row + 1) * rowHeight;
        						overlap = bodyEventCompToRangeMap[eventComp._comp._id];
        						var tmpDt = calendar.addUnits(overlap[1], Eco.TimeUnit.SECOND, -60);
        						endcol = tmpDt.getDay();
        						col = overlap[0].getDay();
        						if ( ypos + eventItemHeight + drillButtonH < ylastpos )
        						{
        							x0 = data.calcHProjection(sdt, overlap[0]);
        							x1 = data.calcHProjection(sdt, overlap[1]);
        							eventComp.draw(horzStyleArr[0], horzStyleArr[1], false, sdt, edt, overlap, xBasePos + x0, ypos, x1 - x0, eventItemHeight);
        							eventComp.setVisible(true);
        							subrow = tmplayoutinfo.offset;
        							//console.log("day start  ===>", evtItem.summary, "row", row, "col", col, "endcol", endcol, overlap);
        							while ( col <= endcol )
        							{
        								//cells[col + "_" + subrow] = true;
        								if ( !cells[col] )
        								{
        									cells[col] = tmpCells.slice(0);
        									cells[col].empcnt = subRowMaxCnt;
        								}
        								cells[col][subrow] = subrow;
        								cells[col].empcnt -= 1;
        								//console.log("day===>", "row", row, "col", col, "subrow", subrow, cells[col], cells[col].empcnt);
        								col++;
        							}
        							continue;
        						}
        						endcol = tmpDt.getDay();
        						col = overlap[0].getDay();
        						eventComp.setVisible(false);
        						tmpdt = calendar.floor(overlap[0], dayUnit, 1);
        						while ( col <= endcol-- )
        						{
        							cellKey = "_" + col;
        							drillBtnComp = oldDrillButtonComps[cellKey];
        							if ( drillBtnComp )
        							{
        								delete oldDrillButtonComps[cellKey];
        							}
        							else
        							{
        								drillBtnComp = factory.getXComp(pDiv, "Static");
        								drillBtnComp.addEventHandler("onclick", this._drillBtnClickHandler, this);
        							}
        							drillBtnComp._col = col;
        							drillBtnComp._row = row;
        							factory.setProperties(drillBtnComp,  "visible", true, "style", drillBtnStylestr);
        							drillbtnY = (row + 1)*rowHeight - drillButtonH - 2;
        							drillbtnX = xBasePos + (col + 1)*columnWidth - drillButtonW - 2;
        							factory.setRect(drillBtnComp, drillbtnX, drillbtnY, drillButtonW, drillButtonH);
        							newDrillButtonComps[cellKey] = drillBtnComp;
        							//drillBtnComp._keydate = tmpdt;
        							tmpdt = calendar.addUnits(tmpdt, dayUnit, 1);
        							col++;
        						}
        					}
        				}
        				bodyDrawTimeEventsByRow = bodyDrawTimeEvents[row];
        				labelCntByCol = [];
        				stopProcessingByCol = [];
        				if ( bodyDrawTimeEventsByRow && bodyDrawTimeEventsByRow.length )
        				{
        					for ( var j = 0 ; j < columnCount ; j++ )
        					{
        						bodyDrawTimeEventsByCell = bodyDrawTimeEventsByRow[j];
        						if ( bodyDrawTimeEventsByCell && bodyDrawTimeEventsByCell.length )
        						{
        							for ( var seq = 0, seqLen = bodyDrawTimeEventsByCell.length ; seq < seqLen ; seq++ )
        							{
        								//if ( !stopProcessingByCol[j] )
        								//{
        									evtItem = bodyDrawTimeEventsByCell[seq];
        									labelKey = row + "_" + j + evtItem.key;
        									eventComp = labelComps[labelKey];
        									overlap = bodyEventCompToRangeMap[eventComp._id];
        								var  tmpDay = overlap[0].getDay();
        								if ( (!cells[tmpDay]) || cells[tmpDay].empcnt > 0 )
        								{
        									if ( !cells[tmpDay] )
        									{
        										subrow = 0;
        										cells[tmpDay] = tmpCells.slice(0);
        										cells[tmpDay][0] = 0;
        										cells[tmpDay].empcnt = subRowMaxCnt - 1;
        									}
        									else
        									{
        										subrow = Eco.array.indexOf(cells[tmpDay], null);
        										if ( subrow > -1 )
        										{
        											cells[tmpDay][subrow] = subrow;
        											cells[tmpDay].empcnt -= 1;
        										}
        									}
        									//console.log("row", row, "col", j, tmpDay, cells[tmpDay], "subrow", subrow, eventComp);
        									//dayItemCnt = data.getOverlapLayoutCount(overLapLayout, overlap[0].getDate());
        									//if ( !labelCntByCol[tmpDay] )
        									//{
        									//	labelCntByCol[tmpDay] = 0;
        									//}
        									if ( subrow > -1 )
        									{
        										//ypos = row * rowHeight + headHeight + dayItemCnt * (eventItemHeight + 2);
        										//ypos = ypos + labelCntByCol[tmpDay] * eventItemHeight + 2;
        										ypos = row * rowHeight + headHeight + subrow * (eventItemHeight + 2);
        										//ypos = ypos + labelCntByCol[tmpDay] * eventItemHeight + 2;
        										x0 = xBasePos + columnWidth * j + 2;
        										//if ( (ypos + eventItemHeight + drillButtonH) < (row + 1) * rowHeight )
        										//{
        											factory.setRect(eventComp, x0, ypos, columnWidth - 4, eventItemHeight);
        											txt = dataUtil.getMaskFormatString(evtItem.startdate, "HH:mm") + " " + evtItem.summary;
        											factory.setProperties(eventComp, "text", txt, "style", labelStylestr, "visible", true);
        										//	labelCntByCol[tmpDay] = labelCntByCol[tmpDay] + 1;
        											continue;
        										//}
        									}
        									//stopProcessingByCol[tmpDay] = true;
        									factory.setProperties(eventComp, "visible", false);
        									cellKey = "_" + tmpDay;
        									if ( !newDrillButtonComps[cellKey] )
        									{
        										drillBtnComp = oldDrillButtonComps[cellKey];
        										if ( drillBtnComp )
        										{
        											delete oldDrillButtonComps[cellKey];
        										}
        										else
        										{
        											drillBtnComp = factory.getXComp(pDiv, "Static");
        											drillBtnComp.addEventHandler("onclick", this._drillBtnClickHandler, this);
        										}
        										drillBtnComp._col = tmpDay;
        										drillBtnComp._row = row;
        										factory.setProperties(drillBtnComp, "visible", true, "style", drillBtnStylestr);
        										drillbtnY = (row + 1)*rowHeight - drillButtonH - 2;
        										drillbtnX = xBasePos + (j + 1)*columnWidth - drillButtonW - 2;
        										factory.setRect(drillBtnComp, drillbtnX, drillbtnY, drillButtonW, drillButtonH);
        										newDrillButtonComps[cellKey] = drillBtnComp;
        									}
        								}
        								else
        								{
        									evtItem = bodyDrawTimeEventsByCell[seq];
        									labelKey = row + "_" + j + evtItem.key;
        									eventComp = labelComps[labelKey];
        									factory.setProperties(eventComp, "visible", false);
        									cellKey = "_" + tmpDay;
        									//if ( cells[tmpDay] && cells[tmpDay] <= 0 )
        									//{
        										if ( !newDrillButtonComps[cellKey] )
        										{
        											drillBtnComp = oldDrillButtonComps[cellKey];
        											if ( drillBtnComp )
        											{
        												delete oldDrillButtonComps[cellKey];
        											}
        											else
        											{
        												drillBtnComp = factory.getXComp(pDiv, "Static");
        												drillBtnComp.addEventHandler("onclick", this._drillBtnClickHandler, this);
        											}
        											drillBtnComp._col = tmpDay;
        											drillBtnComp._row = row;
        											factory.setProperties(drillBtnComp, "visible", true, "style", drillBtnStylestr);
        											drillbtnY = (row + 1)*rowHeight - drillButtonH - 2;
        											drillbtnX = xBasePos + (j + 1)*columnWidth - drillButtonW - 2;
        											factory.setRect(drillBtnComp, drillbtnX, drillbtnY, drillButtonW, drillButtonH);
        											newDrillButtonComps[cellKey] = drillBtnComp;
        										}
        									//}
        								}
        							}
        						}
        					}
        				}
        				Eco.object.Each(oldDrillButtonComps, function(prop, val) {
        					val._col = null;
        					val._row = null;
        					val._keydate = null;
        					val.removeEventHandler("onclick", pThis._drillBtnClickHandler, pThis);
        					factory.release(val, null, true);
        				});
        				drillButtonCompsByRow[row] = newDrillButtonComps;
        			}
        			this._drillButtonCompsByRow = drillButtonCompsByRow;
        		},
        		_getStyleVertEventItem: function()
        		{
        			var vertEventItemStyles = this._vertEventItemStyles;

        			if ( this._changedStyleVertEventItem & 1 )
        			{
        				if ( !vertEventItemStyles ) vertEventItemStyles = [];
        				var eventVertBackGrounds = this._eventVertBackGrounds,
        					eventVertGradations = this._eventVertGradations;
        				var stylestr = "", overStyles = "", tmpstylestr = "";
        				if ( this._eventItemBorder )
        				{
        					stylestr += "border: " + this._eventItemBorder.toString() + ";";
        				}
        				if ( this._eventFont )
        				{
        					stylestr += "font: " + this._eventFont.toString() + ";";
        				}
        				if ( this._eventPadding )
        				{
        					stylestr += "padding: " + this._eventPadding.toString() + ";";
        				}
        				if ( this._eventFontColor )
        				{
        					stylestr += "color: " + this._eventFontColor.color + ";";
        				}
        				stylestr += "bordertype:round 4 4 ;"; 
        				if ( this._eventOverFontColor && this._eventFontColor != this._eventOverFontColor )
        				{
        					overStyles = ":mouseover {color:" + this._eventOverFontColor.color + ";}";
        				}
        				for ( var i = 0, len = eventVertBackGrounds.length ; i < len ; i++ )
        				{
        					tmpstylestr = "";
        					if ( eventVertBackGrounds[i] )
        					{
        						tmpstylestr += "background: " + eventVertBackGrounds[i].toString() + ";";
        					}
        					if ( eventVertGradations[i] )
        					{
        						tmpstylestr += "gradation: " + eventVertGradations[i].toString() + ";";
        					}
        					vertEventItemStyles[i] = "align:left top;" + stylestr + tmpstylestr + overStyles;
        				}
        				vertEventItemStyles.length = eventVertBackGrounds.length;
        				this._vertEventItemStyles = vertEventItemStyles;
        				this._changedStyleVertEventItem = this._changedStyleVertEventItem & (~1);
        			}
        			return vertEventItemStyles;
        		},
        		_getStyleHorzEventItem: function()
        		{
        			var horzEventItemStyle = this._horzEventItemStyle;
        			if ( this._changedStyleHorzEventItem & 1 )
        			{
        				var stylestr = "align:left top;", overStyles = "";
        				if ( this._eventItemBorder )
        				{
        					stylestr += "border: " + this._eventItemBorder.toString() + ";";
        				}
        				if ( this._eventHorzBackGround )
        				{
        					stylestr += "background: " + this._eventHorzBackGround.toString() + ";";
        				}
        				if ( this._eventHorzGradation )
        				{
        					stylestr += "gradation: " + this._eventHorzGradation.toString() + ";";
        				}
        				if ( this._eventFont )
        				{
        					stylestr += "font: " + this._eventFont.toString() + ";";
        				}
        				if ( this._eventPadding )
        				{
        					stylestr += "padding: " + this._eventPadding.toString() + ";";
        				}
        				if ( this._eventFontColor )
        				{
        					stylestr += "color: " + this._eventFontColor.color + ";";
        				}
        				if ( this._eventOverFontColor && this._eventFontColor != this._eventOverFontColor )
        				{
        					overStyles = ":mouseover {color:" + this._eventOverFontColor.color + ";}";
        				}
        				horzEventItemStyle = [stylestr, overStyles];
        				this._horzEventItemStyle = horzEventItemStyle;
        				this._changedStyleHorzEventItem  = this._changedStyleHorzEventItem & (~1);
        			}
        			return horzEventItemStyle||["", ""];
        		},
        		_getStyleLabelEventItem: function()
        		{
        			var labelStylestr = this._labelEventItemStyle;
        			if ( this._changedStyleLabelEventItem & 1 )
        			{
        				labelStylestr = "align:left top; background: transparent; border: 0 none transparent;";

        				if ( this._eventFont )
        				{
        					labelStylestr += "font: " + this._eventFont.toString() + ";";
        				}
        				if ( this._eventPadding )
        				{
        					labelStylestr += "padding: " + this._eventPadding.toString() + ";";
        				}
        				if ( this._eventFontColor )
        				{
        					labelStylestr += "color: " + this._eventFontColor.color + ";";
        				}
        				if ( this._eventOverFontColor && this._eventFontColor != this._eventOverFontColor )
        				{
        					labelStylestr += ":mouseover {color:" + this._eventOverFontColor.color + ";}";
        				}
        				this._labelEventItemStyle = labelStylestr;
        				this._changedStyleLabelEventItem = this._changedStyleLabelEventItem & (~1);
        			}
        			return labelStylestr;
        		},
        		_drawBodyEventItemWeekMode: function(evtItem,data,comp)
        		{
        			var bodyDrawTimeEvents = this._bodyDrawTimeEvents;
        			if ( !bodyDrawTimeEvents )
        			{
        				bodyDrawTimeEvents = [];
        				this._bodyDrawTimeEvents = bodyDrawTimeEvents;
        			}

        			var invalidatedEventColumns = this._invalidatedEventColumns||[],
        				timeEventComps = this._timeEventComps||[],
        				bodyEventCompToRangeMap = this._bodyEventCompToRangeMap||{},
        				idx, tmpEvtItems,
        				oldTimeEventComps, eventComp;

        			var draggingFunc = {
        					'start': this._evtItemDragStartHandler,
        					'dragging': this._evtItemDraggingHandler,
        					'end': this._evtItemDragEndHandler
        				},
        				eventUtil = Eco.XComp.Event;

        			if ( data.isDayEvent(evtItem) )
        			{
        				for ( var i = 0, len = data._columnCount ; i < len ; i++ )
        				{
        					tmpEvtItems = bodyDrawTimeEvents[i];
        					if ( tmpEvtItems )
        					{
        						idx = Eco.array.indexOf(tmpEvtItems, evtItem);
        						if ( idx > -1 )
        						{
        							invalidatedEventColumns.push(i);
        							tmpEvtItems.splice(idx, 1);
        							oldTimeEventComps = timeEventComps[i];
        							if ( oldTimeEventComps )
        							{
        								eventComp = oldTimeEventComps[evtItem.key];
        								if ( eventComp )
        								{
        									var comp = eventComp._comp;
        									if ( comp )
        									{
        										comp.removeEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        										eventUtil.clearDraggable(comp);
        										delete bodyEventCompToRangeMap[comp._id];
        									}
        									eventComp.clear();
        								}
        								delete oldTimeEventComps[evtItem.key];
        							}
        						}
        					}
        				}
        			}
        			else
        			{
        				var calendar = data._calendar,
        					sdt = data._visibleStartdate,
        					dayUnit = Eco.TimeUnit.DAY,
        					pDiv = this._bodyComp, usedComp = false,
        					colstartdt, colenddt;
        				for ( var i = 0, len = data._columnCount ; i < len ; i++ )
        				{
        					colstartdt = calendar.addUnits(sdt, dayUnit, i);
        					colenddt = calendar.addUnits(colstartdt, dayUnit, 1);
        					overlap = data.calcDateRangeOverlap(colstartdt, colenddt, evtItem.startdate, evtItem.enddate);
        					if ( overlap )
        					{
        						invalidatedEventColumns.push(i);
        						oldTimeEventComps = timeEventComps[i];
        						if ( oldTimeEventComps )
        						{
        							eventComp = oldTimeEventComps[evtItem.key];
        						}
        						if ( comp && usedComp )
        						{
        							if ( !eventComp )
        							{
        								eventComp = new Eco.Scheduler.Vertical(pDiv, evtItem);
        								eventComp._comp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        								eventUtil.makeDraggable(eventComp._comp, draggingFunc, this, [eventComp._comp], true);
        								eventComp.draw(this._getItemStyle(), true);
        								if ( !oldTimeEventComps )
        								{
        									oldTimeEventComps = {};
        									timeEventComps[i] = oldTimeEventComps;
        								}
        								oldTimeEventComps[evtItem.key] = eventComp;
        							}
        						}
        						else if ( comp )
        						{
        							usedComp = true;
        							if ( !eventComp )
        							{
        								eventComp = comp._ecoObj;
        								if ( !oldTimeEventComps )
        								{
        									oldTimeEventComps = {};
        									timeEventComps[i] = oldTimeEventComps;
        								}
        								oldTimeEventComps[evtItem.key] = eventComp;
        							}
        						}
        						bodyEventCompToRangeMap[eventComp._comp._id] = overlap;
        						tmpEvtItems = bodyDrawTimeEvents[i];
        						if ( tmpEvtItems )
        						{
        							idx = Eco.array.indexOf(tmpEvtItems, evtItem);
        							if ( idx == -1 ) tmpEvtItems.push(evtItem);
        							tmpEvtItems.sort(this._sort_func);
        						}
        						else
        						{
        							tmpEvtItems = [];
        							bodyDrawTimeEvents[i] = tmpEvtItems;
        							tmpEvtItems.push(evtItem);
        						}
        					}
        					else
        					{
        						tmpEvtItems = bodyDrawTimeEvents[i];
        						if ( tmpEvtItems )
        						{
        							idx = Eco.array.indexOf(tmpEvtItems, evtItem);
        							if ( idx > -1 )
        							{
        								invalidatedEventColumns.push(i);
        								tmpEvtItems.splice(idx, 1);
        								oldTimeEventComps = timeEventComps[i];
        								eventComp = null;
        								if ( oldTimeEventComps )
        								{
        									eventComp = oldTimeEventComps[evtItem.key];
        									delete oldTimeEventComps[evtItem.key];
        								}
        								if ( !comp )
        								{
        									if ( eventComp )
        									{
        										var tmpcomp = eventComp._comp;
        										tmpcomp.removeEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        										eventUtil.clearDraggable(tmpcomp);
        										eventComp.clear();
        										delete bodyEventCompToRangeMap[tmpcomp._id];
        									}
        								}
        							}
        						}
        					}
        				}
        			}

        			this._invalidatedEventColumns = invalidatedEventColumns;
        			this._timeEventComps = timeEventComps;
        			this._bodyEventCompToRangeMap = bodyEventCompToRangeMap;
        			if ( invalidatedEventColumns && invalidatedEventColumns.length )
        			{
        				this._bodyEventItemsWeekModeLayout(data);
        			}
        		},
        		_bodyEventItemsWeekModeDraw: function(data,isStyle)
        		{
        			var timeEvents = data._timeEvents,
        				calendar = data._calendar,
        				evtItem, overlap,
        				sdt = data._visibleStartdate,
        				edt = calendar.addUnits(data._visibleEnddate, Eco.TimeUnit.DAY, 1);

        			var edt0 = calendar.floor(edt, Eco.TimeUnit.DAY, 1);

        			var pDiv = this._bodyComp,
        				pThis = this;
        			var draggingFunc = {
        					'start': this._evtItemDragStartHandler,
        					'dragging': this._evtItemDraggingHandler,
        					'end': this._evtItemDragEndHandler
        				},
        				eventUtil = Eco.XComp.Event;

        			if ( this._changedStyleVertEventItem & 2 )
        			{
        				this._vertEventItemStyleIndex = 0;
        				this._changedStyleVertEventItem = this._changedStyleVertEventItem & (~2);
        			}
        			if ( timeEvents )
        			{
        				if ( isStyle )
        				{
        					var timeEventComps = this._timeEventComps||[],
        						oldTimeEventComps;
        					for ( var i = 0, len = timeEventComps.length ; i < len ; i++ )
        					{
        						oldTimeEventComps = timeEventComps[i];
        						if ( oldTimeEventComps )
        						{
        							Eco.object.Each(oldTimeEventComps, function(prop, val) {
        								val.draw(this._getItemStyle(), true);
        							}, this);
        						}
        					}
        				}
        				else
        				{
        					var dayUnit = Eco.TimeUnit.DAY,
        						sdt = data._visibleStartdate;
        					var timeEventComps = this._timeEventComps||[],
        						oldTimeEventComps, eventComp, tmpArr,
        						bodyEventCompToRangeMap = this._bodyEventCompToRangeMap,
        						newTimeEventComps, key;
        					var invalidatedEventColumns = this._invalidatedEventColumns;

        					var bodyDrawTimeEvents = this._bodyDrawTimeEvents;
        					if ( !bodyDrawTimeEvents )
        					{
        						bodyDrawTimeEvents = [];
        						this._bodyDrawTimeEvents = bodyDrawTimeEvents;
        					}
        					for ( var i = 0, len = data._columnCount ; i < len ; i++ )
        					{
        						colstartdt = calendar.addUnits(sdt, dayUnit, i);
        						colenddt = calendar.addUnits(colstartdt, dayUnit, 1);
        						oldTimeEventComps = timeEventComps[i];
        						if ( !oldTimeEventComps )
        						{
        							oldTimeEventComps = {};
        						}
        						newTimeEventComps = {};
        						tmpArr = [];
        						invalidated = false;
        						for ( var j = 0, jlen = timeEvents.length ; j < jlen ; j++ )
        						{
        							evtItem = timeEvents[j];
        							overlap = data.calcDateRangeOverlap(colstartdt, colenddt, evtItem.startdate, evtItem.enddate);
        							if ( overlap )
        							{
        								key = evtItem.key;
        								eventComp = oldTimeEventComps[key];
        								if ( !eventComp )
        								{
        									eventComp = new Eco.Scheduler.Vertical(pDiv, evtItem);
        									eventComp._comp.addEventHandler("onmousemove", this._evtItemMouseMoveHandler, this);
        									eventUtil.makeDraggable(eventComp._comp, draggingFunc, this, [eventComp._comp], true);
        									eventComp.draw(this._getItemStyle(), true);
        								}
        								else
        								{
        									delete oldTimeEventComps[key];
        								}
        								newTimeEventComps[key] = eventComp;
        								bodyEventCompToRangeMap[eventComp._comp._id] = overlap;
        								tmpArr.push(evtItem);
        							}
        						}
        						timeEventComps[i] = newTimeEventComps;
        						bodyDrawTimeEvents[i] = tmpArr;
        						invalidatedEventColumns.push(i);
        						Eco.object.Each(oldTimeEventComps, function(prop, val) {
        							var comp = val._comp;
        							if ( comp )
        							{
        								comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        								eventUtil.clearDraggable(comp);
        								delete bodyEventCompToRangeMap[comp._id];
        							}
        							val.clear();
        						});
        					}
        					if ( timeEventComps.length > data._columnCount )
        					{
        						for ( var i = data._columnCount, len = timeEventComps.length ; i < len ; i++ )
        						{
        							oldTimeEventComps = timeEventComps[i];
        							if ( oldTimeEventComps )
        							{
        								Eco.object.Each(oldTimeEventComps, function(prop, val) {
        									var comp = val._comp;
        									if ( comp )
        									{
        										comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        										eventUtil.clearDraggable(comp);
        										delete bodyEventCompToRangeMap[comp._id];
        									}
        									val.clear();
        								});
        							}
        						}
        					}
        					timeEventComps.length = data._columnCount;
        					this._timeEventComps = timeEventComps;
        					this._bodyEventItemsWeekModeLayout(data);
        				}
        			}
        			else
        			{
        				var timeEventComps = this._timeEventComps||[],
        					oldTimeEventComps,
        					bodyEventCompToRangeMap = this._bodyEventCompToRangeMap;
        				for ( var i = 0, len = timeEventComps.length ; i < len ; i++ )
        				{
        					oldTimeEventComps = timeEventComps[i];
        					if ( oldTimeEventComps )
        					{
        						Eco.object.Each(oldTimeEventComps, function(prop, val) {
        							var comp = val._comp;
        							if ( comp )
        							{
        								comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        								eventUtil.clearDraggable(comp);
        								delete bodyEventCompToRangeMap[comp._id];
        							}
        							val.clear();
        						});
        					}
        				}
        				this._timeEventComps = null;
        			}
        		},
        		_bodyEventItemsWeekModeDrawClear: function()
        		{
        			var timeEventComps = this._timeEventComps||[],
        				oldTimeEventComps,
        				bodyEventCompToRangeMap = this._bodyEventCompToRangeMap;
        			for ( var i = 0, len = timeEventComps.length ; i < len ; i++ )
        			{
        				oldTimeEventComps = timeEventComps[i];
        				if ( oldTimeEventComps )
        				{
        					var pThis = this,
        						eventUtil = Eco.XComp.Event;
        					Eco.object.Each(oldTimeEventComps, function(prop, val) {
        						var comp = val._comp;
        						if ( comp )
        						{
        							comp.removeEventHandler("onmousemove", pThis._evtItemMouseMoveHandler, pThis);
        							eventUtil.clearDraggable(comp);
        							delete bodyEventCompToRangeMap[comp._id];
        						}
        						val.clear();
        					});
        				}
        			}
        			this._timeEventComps = null;
        		},
        		_getItemStyle: function()
        		{
        			var vertEventItemStyleIndex = this._vertEventItemStyleIndex,
        				vertEventItemStyles = this._getStyleVertEventItem();
        			if ( vertEventItemStyles )
        			{
        				if ( vertEventItemStyleIndex >= vertEventItemStyles.length )
        				{
        					vertEventItemStyleIndex = 0;
        				}
        				var ret = vertEventItemStyles[vertEventItemStyleIndex];
        				vertEventItemStyleIndex++;
        				this._vertEventItemStyleIndex = vertEventItemStyleIndex;
        				return ret;
        			}
        			return "";
        		},
        		_bodyEventItemsWeekModeLayout: function(data)
        		{
        			var invalidatedEventColumns = this._invalidatedEventColumns,
        				bodyDrawTimeEvents = this._bodyDrawTimeEvents,
        				calendar = data._calendar,
        				visiblesdt = data._visibleStartdate,
        				dayUnit = Eco.TimeUnit.DAY,
        				col, timeEvents, evtItem, overlap;

        			var overlapGapValue = this.percentOverlap,
        				defaultEventItemWidth = data._columnWidth - 2 * 2; // 2는 Gap
        			if ( !Eco.isNumber(overlapGapValue) || (overlapGapValue < 0) )
        			{
        				overlapGapValue = 0;
        			}
        			else if ( overlapGapValue > 100 )
        			{
        				overlapGapValue = 100;
        			}

        			overlapGapValue = overlapGapValue / 100;

        
        			var cellsArea = this._bodyBandArea.cellsArea,
        				columnWidth = data._columnWidth,
        				xBasePos = cellsArea.x,
        				baseColumnX, x0, x1, y0, y1, itemWidth;

        			var bodyEventCompToRangeMap = this._bodyEventCompToRangeMap,
        				bodyHeight = this._bodyBandArea.cellsArea.height,
        				timeEventComps = this._timeEventComps, timeEventCompsByCol, eventComp;
        			var overLapLayout = this._weekEventOverLapLayout,
        				bodyGrp = this._bodyGrp, prevComp; //이 라인 주석 해야 함
        			if ( !overLapLayout )
        			{
        				overLapLayout = data.createOverlapLayout(true, true);
        				this._weekEventOverLapLayout = overLapLayout;
        			}
        			for ( var i = 0, len = invalidatedEventColumns.length ; i < len ; i++ )
        			{
        				col = invalidatedEventColumns[i];
        				timeEvents = bodyDrawTimeEvents[col];
        				if ( timeEvents && timeEvents.length )
        				{
        					baseColumnX = xBasePos + columnWidth*col + 2;
        					sdt = calendar.addUnits(visiblesdt, dayUnit, col);
        					edt = calendar.addUnits(sdt, dayUnit, 1);
        					data.calcOverlapping(overLapLayout, timeEvents, sdt, edt);
        					timeEventCompsByCol = timeEventComps[col];
        					for ( var j = 0, jlen = timeEvents.length ; j < jlen ; j++ )
        					{
        						evtItem = timeEvents[j];
        						eventComp = timeEventCompsByCol[evtItem.key];
        						overlap = bodyEventCompToRangeMap[eventComp._comp._id];
        						y0 = data.calcVProjection(sdt, overlap[0], bodyHeight);
        						y1 = data.calcVProjection(sdt, overlap[1], bodyHeight);
        						tmplayoutinfo = data.getOverlapLayoutInfo(overLapLayout, evtItem);
        						overlapcnt = data.getOverlapLayoutCount(overLapLayout);
        						if (overlapGapValue == 0)
        						{
        							x1 = (overlapcnt == 1 ? defaultEventItemWidth : ((defaultEventItemWidth - (overlapcnt - 1) * 2) / overlapcnt));
        							x0 = tmplayoutinfo.offset * 2;
        							itemWidth = (tmplayoutinfo.nb == 1 ? x1 : defaultEventItemWidth);
        						}
        						else
        						{
        							x1 = (overlapcnt == 1 ? defaultEventItemWidth : (defaultEventItemWidth/(overlapcnt - (overlapcnt-1) * overlapGapValue)));
        							x0 = (tmplayoutinfo.offset * (x1 - overlapGapValue * x1));
        							itemWidth = (tmplayoutinfo.nb == 1 ? x1 : (x1 * (tmplayoutinfo.nb - (tmplayoutinfo.nb-1) * overlapGapValue)));
        						}
        						eventComp.draw(null, false, baseColumnX + x0, y0, itemWidth, y1 - y0);
        						eventComp._comp.bringToFront(); //moveToPrev(bodyGrp);						
        						//eventComp._comp.moveToPrev((prevComp ? prevComp : bodyGrp)); //moveToPrev(bodyGrp);
        						//prevComp = eventComp._comp;
        					}
        				}
        			}
        			this._invalidatedEventColumns = [];
        		},
        		_headBandDraw: function(layer,headBandArea,data)
        		{
        			//colheader background
        			var headband = layer.getElementById("headband");
        			if ( !headband )
        			{
        				headband = new Eco.GraphicGroup();
        				headband.setId("headband");
        				layer.addChild(headband);
        			}

        			var colheadbk = headband.getElementById("colheadbk");
        			if ( !colheadbk )
        			{
        				colheadbk = new Eco.GraphicRect();
        				colheadbk.setId("colheadbk");
        				headband.insertBefore(colheadbk, headband._firstChild);
        			}

        			if ( this._changedColHeaderBk )
        			{
        				colheadbk.setFillbrush(this._colHeaderFillBrush);
        				colheadbk.setFillgradation(this._colHeaderGradation);
        				this._changedColHeaderBk = false;
        			}

        			var colHeadArea = headBandArea.colHeadArea;
        			colheadbk.setPoint(colHeadArea.x, colHeadArea.y);

        			var allDayHeadbk = colheadbk.getElementById("alldayheadbk");
        			if ( data._mode == "month" )
        			{
        				if ( allDayHeadbk )
        				{
        					headband.removeChild(allDayHeadbk);
        					this._changedAllDayBk = true;
        				}
        			}
        			else
        			{
        				if ( !allDayHeadbk )
        				{
        					allDayHeadbk = new Eco.GraphicRect();
        					allDayHeadbk.setId("alldayheadbk");
        					headband.insertBefore(allDayHeadbk, headband.getChildByIndex(1));
        				}
        			}

        			//colheader date 값들 생성
        			if ( data._changedVisibleRange )
        			{
        				this._colHeadDates = this._getColHeadDisplayDates(data);
        			}
        			//colheader cells 생성
        			if ( data._mode == "month" )
        			{
        				if ( data._changedVisibleRange || 
        					 (this._changedColHeadFont&2) )
        				{
        					var dts = this._colHeadDates,
        						labelsInfo = [];
        					labelsInfo[0] = this._calcColTexts(dts, "month", 0);
        					labelsInfo[1] = this._calcColTexts(dts, "month", 1);
        					labelsInfo[2] = this._calcColTexts(dts, "month", 2);
        					this._colLabelsInfo = labelsInfo;
        					this._changedColHeadFont = this._changedColHeadFont & (~2);
        				}
        			}
        			else
        			{
        				if ( data._changedVisibleRange || 
        					 (this._changedColHeadFont&4) )
        				{
        					var dts = this._colHeadDates,
        						mode = data.mode,
        						labelsInfo = [];
        					labelsInfo[0] = this._calcColTexts(dts, mode, 0);
        					labelsInfo[1] = this._calcColTexts(dts, mode, 1);
        					labelsInfo[2] = this._calcColTexts(dts, mode, 2);
        					this._colLabelsInfo = labelsInfo;
        					this._changedColHeadFont = this._changedColHeadFont & (~4);
        				}
        			}

        			var kind = this.getColHeadCurKind(data);
        			if ( data._columnWidth != this._oldColumnWidth )
        			{
        				this._changedColumnSized = true;
        				this._oldColumnWidth = data._columnWidth;
        			}

        			if ( data._rowHeight != this._oldRowHeight )
        			{
        				this._changedRowSized = true;
        				this._oldRowHeight = data._rowHeight;
        			}

        			if ( data._changedVisibleRange || this._changedColumnSized )
        			{
        				colheadbk.setWidth(colHeadArea.width);
        				colheadbk.setHeight(colHeadArea.height);

        				//kind, data, group, isStyleChanged, isOnlyTodayStyleChanged
        				this.updateColHead(kind, data, headband);
        				//this._changedColumnSized  = false;
        				this._changedColHeadFont = this._changedColHeadFont & (~8);
        				this._changedColHeadColor = false;
        				this._changedTodayHeadProps = false;
        				if ( data._mode != "month" )
        				{
        					var allDayArea = headBandArea.allDayArea;
        					allDayHeadbk.setY(allDayArea.y);
        					allDayHeadbk.setWidth(allDayArea.width);
        					allDayHeadbk.setHeight(allDayArea.height);
        				}
        			}
        			else if ( this._changedColHeadColor || 
        					this._changedCellLine ||
        					(this._changedColHeadFont & 8) ) //today는 mode가 month가 아니고 today있는 경우 
        			{
        				//kind, data, group, isStyleChanged, isOnlyTodayStyleChanged
        				this.updateColHead(kind, data, headband, true);
        				this._changedColHeadFont = this._changedColHeadFont & (~8);
        				this._changedColHeadColor = false;
        				this._changedTodayHeadProps = false;
        			}
        			else if ( data._mode != "month" && (this._colHeadDates._todayIndex > -1) &&
        					this._changedTodayHeadProps )
        			{
        				//kind, data, group, isStyleChanged, isOnlyTodayStyleChanged
        				this.updateColHead(kind, data, headband, false, true);
        				this._changedTodayHeadProps = false;
        			}

        			if ( data._mode == "month" )
        			{
        				var allDayHorzLine = this._allDayHorzLine;
        				if ( allDayHorzLine )
        				{
        					headband.removeChild(allDayHorzLine);
        					this._allDayHorzLine = null;
        				}
        			}
        			else
        			{
        				if ( this._changedAllDayBk )
        				{
        					allDayHeadbk.setFillbrush(this._allDayFillBrush);
        					allDayHeadbk.setFillgradation(this._allDayGradation);
        					this._changedAllDayBk = false;
        				}
        				//horz line 생성
        				var allDayHorzLine = this._allDayHorzLine,
        					cellLineStorke = this._cellLine;
        				if ( !allDayHorzLine )
        				{
        					allDayHorzLine = new Eco.GraphicLine();
        					headband.addChild(allDayHorzLine);
        					this._allDayHorzLine = allDayHorzLine;
        				}
        				var bottomPos = headBandArea.height - this.eventItemBottomPadding - 1;
        				allDayHorzLine.setStrokepen(cellLineStorke);
        				allDayHorzLine.setX1(0);
        				allDayHorzLine.setY1(bottomPos);
        				allDayHorzLine.setX2(headBandArea.width);
        				allDayHorzLine.setY2(bottomPos);
        			}
        		},
        		updateColHead: function(kind,data,group,isStyleChanged,isOnlyTodayStyleChanged)
        		{
        			var labelsInfoForKind = this._colLabelsInfo[kind],
        				texts = labelsInfoForKind[1], txt,
        				dates = this._colHeadDates;

        			if ( data._mode != "month" && isOnlyTodayStyleChanged )
        			{
        				this._colHeadToDayElemUpdate(group, dates, texts, this._colHeadTextElems, this._colHeaderFont, data);
        			}
        			else if ( isStyleChanged )
        			{
        				var usedVertLines = this._headVertLines||[], lineElem,
        					colHeadHorzLine = this._colHeadHorzLine,
        					textElems = this._colHeadTextElems||{},
        					fontfillbrush = this._colHeaderFontFillBrush,
        					font = this._colHeaderFont,
        					cellLineStorke = this._cellLine;

        				if ( data._mode == "month" )
        				{
        					for ( var i = 0, len = usedVertLines.length ; i < len ; i++ )
        					{
        						lineElem = usedVertLines[i];
        						lineElem.setStrokepen(cellLineStorke);
        					}
        					Eco.object.Each(textElems, function(prop, val) {
        						if ( val )
        						{
        							val.setFont(font);
        							val.setFillbrush(fontfillbrush);
        						}
        					});
        					this._colHeadToDayElemClear(group);
        				}
        				else
        				{
        					for ( var i = 0, len = usedVertLines.length ; i < len ; i++ )
        					{
        						lineElem = usedVertLines[i];
        						lineElem.setStrokepen(cellLineStorke);
        					}
        					Eco.object.Each(textElems, function(prop, val) {
        						if ( val )
        						{
        							val.setFont(font);
        							val.setFillbrush(fontfillbrush);
        						}
        					});
        					this._colHeadToDayElemUpdate(group, dates, texts, textElems, font, data);
        				}
        				colHeadHorzLine.setStrokepen(cellLineStorke);
        			}
        			else
        			{
        				var colHeadArea = this._headBandArea.colHeadArea,
        					columnWidth = data._columnWidth,
        					columnHeight = colHeadArea.height,
        					vLineHeight = this._headBandArea.height - this.eventItemBottomPadding,
        					columnCount = data._columnCount,
        					yPos = Math.round(columnHeight/2), xPos = 0,
        					halfXgap = Math.round(columnWidth/2),
        					usedVertLines = this._headVertLines||[], lineElem,
        					colHeadHorzLine = this._colHeadHorzLine,
        					textElems = this._colHeadTextElems||{},
        					newTextElems = {}, txtElem,
        					fontfillbrush = this._colHeaderFontFillBrush,
        					font = this._colHeaderFont,
        					cellLineStorke = this._cellLine;
        				//line, text 생성
        				for ( var i = 0 ; i < columnCount ; i++ )
        				{
        					lineElem = usedVertLines[i];
        					if ( !lineElem )
        					{
        						lineElem = new Eco.GraphicLine();
        						group.addChild(lineElem);
        						usedVertLines[i] = lineElem;
        					}
        					lineElem.setStrokepen(cellLineStorke);
        					lineElem.setX1(xPos);
        					lineElem.setY1(0);
        					lineElem.setX2(xPos);
        					lineElem.setY2(vLineHeight);

        					txt = texts[i];
        					txtElem = textElems[txt];
        					if ( !txtElem )
        					{
        						txtElem = new Eco.GraphicText();
        						group.addChild(txtElem);
        						txtElem.setHalign("center");
        						txtElem.setValign("middle");
        						newTextElems[txt] = txtElem;
        					}
        					else
        					{
        						newTextElems[txt] = txtElem;
        						delete textElems[txt];
        					}
        					txtElem.setY(yPos);
        					txtElem.setX(xPos + halfXgap);
        					xPos += columnWidth;
        					txtElem.setFont(font);
        					txtElem.setText(txt);
        					txtElem.setFillbrush(fontfillbrush);
        				}

        				lineElem = usedVertLines[columnCount];
        				if ( !lineElem )
        				{
        					lineElem = new Eco.GraphicLine();
        					group.addChild(lineElem);
        					usedVertLines[columnCount] = lineElem;
        				}
        				lineElem.setStrokepen(cellLineStorke);
        				lineElem.setX1(xPos - 1);
        				lineElem.setY1(0);
        				lineElem.setX2(xPos - 1);
        				lineElem.setY2(vLineHeight);

        				//horz line 생성
        				if ( !colHeadHorzLine )
        				{
        					colHeadHorzLine = new Eco.GraphicLine();
        					group.addChild(colHeadHorzLine);
        					this._colHeadHorzLine = colHeadHorzLine;
        				}
        				colHeadHorzLine.setStrokepen(cellLineStorke);
        				colHeadHorzLine.setX1(0);
        				colHeadHorzLine.setY1(columnHeight);
        				colHeadHorzLine.setX2(colHeadArea.width);
        				colHeadHorzLine.setY2(columnHeight);

        
        				for ( var i = columnCount + 1, len = usedVertLines.length ; i < len ; i++ )
        				{
        					lineElem = usedVertLines[i];
        					if ( lineElem )
        					{
        						group.removeChild(lineElem);
        					}
        				}
        				Eco.object.Each(textElems, function(prop, val) {
        					if ( val )
        					{
        						group.removeChild(val);
        					}
        				});
        				usedVertLines.length = columnCount + 1;
        				this._headVertLines = usedVertLines;
        				this._colHeadTextElems = newTextElems;
        				if ( data._mode == "month" || dates._todayIndex < 0 )
        				{
        					this._colHeadToDayElemClear(group);
        				}
        				else
        				{
        					this._colHeadToDayElemUpdate(group, dates, texts, newTextElems, font, data);
        				}
        			}
        			this._kind = kind;
        		},
        		_colHeadToDayElemClear: function(group)
        		{
        			if ( this._colHeadTodayTextElem )
        			{
        				this._colHeadTodayTextElem = null;
        			}
        			if ( this._colHeadTodayRectElem )
        			{
        				group.removeChild(this._colHeadTodayRectElem);
        				this._colHeadTodayRectElem = null;
        			}
        		},
        		_colHeadToDayElemUpdate: function(group,dates,texts,textElems,font,data)
        		{
        			if ( dates._todayIndex > -1 )
        			{
        				txt = texts[dates._todayIndex];
        				txtElem = textElems[txt];
        				if ( txtElem )
        				{
        					if ( this._todayFontBold )
        					{
        						if ( font._bold )
        						{
        							txtElem.setFont(font);
        						}
        						else
        						{
        							font._bold = true;
        							txtElem.setFont(font._getValueStr());
        							font._bold = false;
        						}
        					}
        					else
        					{
        						if ( font._bold )
        						{
        							font._bold = false;
        							txtElem.setFont(font._getValueStr());
        							font._bold = true;
        						}
        						else
        						{
        							txtElem.setFont(font);
        						}
        					}
        					txtElem.setFillbrush(this._todayFontFillBrush);
        					this._colHeadTodayTextElem = txtElem;
        				}
        				var rectElem = this._colHeadTodayRectElem;
        				if ( !rectElem )
        				{
        					rectElem = new Eco.GraphicRect();
        					var b = group.getChildByIndex(1);
        					group.insertBefore(rectElem, b);
        					//group.addChild(rectElem);
        					this._colHeadTodayRectElem = rectElem;
        				}
        				rectElem.setX(data._columnWidth*dates._todayIndex);
        				rectElem.setY(0);
        				rectElem.setWidth(data._columnWidth);
        				rectElem.setHeight(this._headBandArea.colHeadArea.height);
        				rectElem.setFillbrush(this._todayHeaderFillBrush);
        				rectElem.setFillgradation(this._todayHeaderGradation);
        			}
        		},
        		_bodyBandDraw: function(layer,bodyBandArea,data)
        		{
        			//rowheader background
        			var rowheadbk = layer.getElementById("rowheadbk");
        			if ( !rowheadbk )
        			{
        				rowheadbk = new Eco.GraphicRect();
        				rowheadbk.setId("rowheadbk");
        				layer.addChild(rowheadbk);
        			}
        			var cellbk = layer.getElementById("cellbk");
        			if ( !cellbk )
        			{
        				cellbk = new Eco.GraphicRect();
        				cellbk.setId("cellbk");
        				layer.addChild(cellbk);
        			}

        			if ( this._changedRowHeaderBk )
        			{
        				rowheadbk.setFillbrush(this._rowHeaderFillBrush);
        				rowheadbk.setFillgradation(this._rowHeaderGradation);
        				this._changedRowHeaderBk = false;
        			}

        			if ( this._changedCellBk )
        			{
        				cellbk.setFillbrush(this._cellFillBrush);
        				cellbk.setFillgradation(this._cellGradation);
        				this._changedCellBk = false;
        			}

        			//update rowHeader
        			if ( data._changedVisibleRange || 
        				 this._changedColumnSized ||
        				( data._mode == "month" && this._changedRowSized )
        			   )
        			{
        				var rowHeadArea = bodyBandArea.rowHeadArea;
        				rowheadbk.setWidth(rowHeadArea.width);
        				rowheadbk.setHeight(rowHeadArea.height);

        				this._updateRowHead(data, layer, bodyBandArea);

        				var cellsArea = bodyBandArea.cellsArea;
        				cellbk.setX(cellsArea.x);
        				cellbk.setWidth(cellsArea.width);
        				cellbk.setHeight(cellsArea.height);
        				var usedVertLines = this._bodyVertLines||[], lineElem,
        					cellLineStorke = this._cellLine,
        					columnWidth = data._columnWidth,
        					columnCount = data._columnCount,
        					yEnd = cellsArea.height,
        					xPos = cellsArea.x + columnWidth;
        				for ( var i = 0 ; i < columnCount ; i++ )
        				{
        					lineElem = usedVertLines[i];
        					if ( !lineElem )
        					{
        						lineElem = new Eco.GraphicLine();
        						layer.addChild(lineElem);
        						usedVertLines[i] = lineElem;
        					}
        					lineElem.setStrokepen(cellLineStorke);
        					lineElem.setX1(xPos);
        					lineElem.setY1(0);
        					lineElem.setX2(xPos);
        					lineElem.setY2(yEnd);
        					xPos += columnWidth;
        					if ( i == columnCount - 2 )
        					{
        						xPos -= 1;
        					}
        				}
        				for ( var i = columnCount, len = usedVertLines.length ; i < len ; i++ )
        				{
        					lineElem = usedVertLines[i];
        					if ( lineElem )
        					{
        						layer.removeChild(lineElem);
        					}
        				}
        				usedVertLines.length = columnCount;
        				this._bodyVertLines = usedVertLines;

        				this._changedCellLine = false;
        			}
        			else
        			{
        				if ( data._mode == "month" )
        				{
        					if ( this._changedRowHeadColor || this._changedCellLine ||
        						 this._changedCellHeadBk || this._changedCellColor ||
        						 ( this._changedCellFont & 2 ) ||
        						 ( this._changedRowHeadFont & 4 ) ||
        						( this._changedRowHeadPadding & 2 ) 
        					) 
        					{
        						this._updateRowHead(data, layer, bodyBandArea, true);
        					}
        					else if ( this._bodyTodayRectElem && this._changedTodayProps )
        					{
        						this._bodyToDayElemUpdate(layer, this._bodyTodayTextElem, this._cellFont, data, bodyBandArea.rowHeadArea.width, this.getCellHeadHeight());
        						this._changedTodayProps = false;
        					}
        				}
        				else
        				{
        					if ( this._changedRowHeadColor || this._changedCellLine ||
        						this._changednonWorkingBk || 
        						( this._changedRowHeadFont & 4 ) ||
        						( this._changedRowHeadPadding & 2 ) 
        					)
        					{
        						this._updateRowHead(data, layer, bodyBandArea, true);
        					}
        				}
        			}
        		},
        		_updateRowHead: function(data,layer,bodyBandArea,isStyleChanged)
        		{
        			if ( isStyleChanged )
        			{
        				var usedHorzLines = this._bodyHorzLines||[], lineElem,
        					rowHeadVertLine = this._rowHeadVertLine,
        					textElems = this._rowHeadTextElems||{},
        					cellLineStorke = this._cellLine;

        				if ( this._changedCellLine )
        				{
        					for ( var i = 0, len = usedHorzLines.length ; i < len ; i++ )
        					{
        						lineElem = usedHorzLines[i];
        						lineElem.setStrokepen(cellLineStorke);
        					}
        					var usedSubHorzLines = this._bodySubHorzLines,
        						cellSubLineStorke = this._cellSubLine;
        					if ( usedSubHorzLines )
        					{
        						var cellSubLineStorke = this._cellSubLine;
        						for ( var i = 0, len = usedSubHorzLines.length ; i < len ; i++ )
        						{
        							lineElem = usedSubHorzLines[i];
        							lineElem.setStrokepen(cellSubLineStorke);
        						}
        					}
        					rowHeadVertLine.setStrokepen(cellLineStorke);
        					this._changedCellLine = false;
        				}

        				if ( this._changedRowHeadColor ||
        					 ( this._changedRowHeadFont & 4 )
        					)
        				{
        					var fontfillbrush = this._rowHeaderFontFillBrush,
        						font = this._rowHeaderFont;
        					Eco.object.Each(textElems, function(prop, val) {
        						if ( val )
        						{
        							val.setFont(font);
        							val.setFillbrush(fontfillbrush);
        						}
        					});
        					this._changedRowHeadFont = this._changedRowHeadFont & (~4);
        					this._changedRowHeadColor = false;
        				}
        				this._changedRowHeadPadding = this._changedRowHeadPadding & (~2);

        				if ( data._mode == "month" )
        				{
        					var cellHeadRects = this._cellHeadRects;
        					if ( cellHeadRects && this._changedCellHeadBk )
        					{
        						var rectElem,
        							cellHeadFillBrush = this._cellHeadFillBrush,
        							cellHeadGradation = this._cellHeadGradation;
        						for ( var i = 0, len = cellHeadRects.length ; i < len ; i++ )
        						{
        							rectElem = cellHeadRects[i];
        							rectElem.setFillbrush(cellHeadFillBrush);
        							rectElem.setFillgradation(cellHeadGradation);
        						}
        						this._changedCellHeadBk = false;
        					}

        					if ( this._changedCellColor || ( this._changedCellFont & 2 ) )
        					{
        						var cellTexts = this._cellHeadTexts,
        							fontfillbrush = this._cellFontFillBrush,
        							font = this._cellFont;
        						if ( cellTexts )
        						{
        							Eco.object.Each(dayTxtElems, function(prop, val) {
        								if ( val )
        								{
        									val.setFont(font);
        									val.setFillbrush(fontfillbrush);
        								}
        							});
        						}
        						this._changedCellColor = false;
        						this._changedCellFont = this._changedCellFont & (~2);
        					}

        					if ( this._bodyTodayTextElem && this._changedTodayProps )
        					{
        						this._bodyToDayElemUpdate(layer, this._bodyTodayTextElem, this._cellFont, data, bodyBandArea.rowHeadArea.width, this.getCellHeadHeight());
        						this._changedTodayProps = false;
        					}
        				}
        				else
        				{
        					if ( this._changednonWorkingBk )
        					{
        						var nonWorkingRects = this._nonWorkingRects, nonWorkingRect;
        						if ( nonWorkingRects )
        						{
        							nonWorkingRect = nonWorkingRects[0];
        							nonWorkingRect.setFillbrush(this._nonWorkingFillBrush);
        							nonWorkingRect.setFillgradation(this._nonWorkingGradation);
        							nonWorkingRect = nonWorkingRects[1];
        							nonWorkingRect.setFillbrush(this._nonWorkingFillBrush);
        							nonWorkingRect.setFillgradation(this._nonWorkingGradation);
        						}
        						this._changednonWorkingBk = false;
        					}
        				}
        			}
        			else
        			{
        				var backgroundGroup = layer.getElementById("bodybkgroup"),
        					linesGroup = layer.getElementById("bodylinesgroup"),
        					cellLabelsGroup = layer.getElementById("bodycellLabelsgroup");

        				if ( !backgroundGroup )
        				{
        					backgroundGroup = new Eco.GraphicGroup();
        					backgroundGroup.setId("bodybkgroup");
        					layer.addChild(backgroundGroup);
        				}
        				if ( !linesGroup )
        				{
        					linesGroup = new Eco.GraphicGroup();
        					linesGroup.setId("bodylinesgroup");
        					layer.addChild(linesGroup);
        				}
        				if ( !cellLabelsGroup )
        				{
        					cellLabelsGroup = new Eco.GraphicGroup();
        					cellLabelsGroup.setId("bodycellLabelsgroup");
        					layer.addChild(cellLabelsGroup);
        				}

        				var rowHeight, rowCount,
        					usedSubHorzLines = this._bodySubHorzLines||[],
        					cellLineStorke = this._cellLine,
        					lineElem,
        					rowHeadArea = bodyBandArea.rowHeadArea,
        					rowHeaderWidth = rowHeadArea.width,
        					yPos, xPos, lineWidth;

        				if ( data._mode == "month" )
        				{
        					var nonWorkingRects = this._nonWorkingRects;
        					if ( nonWorkingRects )
        					{
        						for ( var i = 0, len = nonWorkingRects.length ; i < len ; i++ )
        						{
        							backgroundGroup.removeChild(nonWorkingRects[i]);
        						}
        						this._nonWorkingRects = null;
        					}
        					var noons = this._noons;
        					if ( noons )
        					{
        						for ( var i = 0, len = noons.length ; i < len ; i++ )
        						{
        							layer.removeChild(noons[i]);
        						}
        						this._noons = null;
        					}
        					rowHeight = data._rowHeight;
        					rowCount = data._rowCount;
        					var headHeight = this.getCellHeadHeight();

        					lineWidth = bodyBandArea.width;
        					xPos = rowHeaderWidth;
        					yPos = 0;

        					var cellHeadRects = this._cellHeadRects||[],
        						cellHeadFillBrush = this._cellHeadFillBrush,
        						cellHeadGradation = this._cellHeadGradation,
        						rectElem, cellHeadWidth = lineWidth - rowHeaderWidth;

        					for ( var i = 0 ; i < rowCount ; i++ )
        					{
        						rectElem = cellHeadRects[i];
        						if ( !rectElem )
        						{
        							rectElem = new Eco.GraphicRect();
        							backgroundGroup.addChild(rectElem);
        							cellHeadRects[i] = rectElem;
        						}
        						rectElem.setFillbrush(cellHeadFillBrush);
        						rectElem.setFillgradation(cellHeadGradation);
        						rectElem.setX(rowHeaderWidth);
        						rectElem.setY(yPos);
        						rectElem.setWidth(cellHeadWidth);
        						rectElem.setHeight(headHeight);
        						yPos += rowHeight;
        					}
        					for ( var i = rowCount, len = cellHeadRects.length ; i < len ; i++ )
        					{
        						backgroundGroup.removeChild(cellHeadRects[i]);
        					}
        					cellHeadRects.length = rowCount;
        					this._cellHeadRects = cellHeadRects;

        					var columnCount = data._columnCount, dayTxtElem
        						columnWidth = data._columnWidth;

        					var tmpdt = data._cloneDate(data._visibleStartdate);
        						calendar = data._calendar,
        						dateUtil = Eco.date,
        						format = "yyyyMMdd",
        						dayUnit = Eco.TimeUnit.DAY;
        					var today = new Date(),
        						todayYY = today.getFullYear(),
        						todayMM = today.getMonth(),
        						todayDD = today.getDate(),
        						curMM, tmpMM, tmpDD,
        						monthShortName = dateUtil.monthShortName,
        						dayTxtElems = this._cellHeadTexts||{}, dayTxtElem,
        						newDayTxtElems = {};

        					var yHalfPos = Math.round(headHeight/2);
        					yPos = 0;
        					var x, y, dayTxt, keyDayTxt, isIncludeToday = false;
        					var fontfillbrush = this._cellFontFillBrush,
        						font = this._cellFont,
        						padding = this._cellHeadPadding,
        						rightGap;
        					if ( padding ) rightGap = padding.right;

        					for ( var i = 0 ; i < rowCount ; i++ )
        					{
        						y = yPos + yHalfPos;
        						xPos = rowHeaderWidth + columnWidth; //right align
        						for ( var j = 0 ; j < columnCount ; j++ )
        						{
        							tmpMM = tmpdt.getMonth();
        							tmpDD = tmpdt.getDate();
        							keyDayTxt = tmpMM + "/" + tmpDD;
        							if ( tmpMM != curMM )
        							{
        								dayTxt = monthShortName[tmpMM] + "/" + tmpDD;
        								curMM = tmpMM;
        							}
        							else
        							{
        								dayTxt = tmpDD;
        							}
        							dayTxtElem = dayTxtElems[keyDayTxt];
        							if ( !dayTxtElem )
        							{
        								dayTxtElem = new Eco.GraphicText();
        								cellLabelsGroup.addChild(dayTxtElem);
        								dayTxtElem.setHalign("right");
        								dayTxtElem.setValign("middle");
        								newDayTxtElems[keyDayTxt] = dayTxtElem;
        							}
        							else
        							{
        								newDayTxtElems[keyDayTxt] = dayTxtElem;
        								delete dayTxtElems[keyDayTxt];
        							}
        							if ( todayYY == tmpdt.getFullYear() &&
        								todayMM == tmpMM &&
        								todayDD == tmpDD )
        							{
        								this._cellTodayColIndex = j;
        								this._cellTodayRowIndex = i;
        								this._bodyToDayElemUpdate(layer, dayTxtElem, font, data, 
        									rowHeaderWidth, headHeight);
        								isIncludeToday = true;
        							}
        							else
        							{
        								dayTxtElem.setFont(font);
        								dayTxtElem.setFillbrush(fontfillbrush);
        							}
        							dayTxtElem.setX(xPos - rightGap);
        							dayTxtElem.setY(y);
        							dayTxtElem.setText(dayTxt);
        							xPos += columnWidth;
        							tmpdt = calendar.addUnits(tmpdt, dayUnit, 1);
        						}
        						yPos += rowHeight;
        					}
        					if ( !isIncludeToday )
        					{
        						this._bodyToDayElemClear(layer);
        					}
        					Eco.object.Each(dayTxtElems, function(prop, val) {
        						if ( val )
        						{
        							cellLabelsGroup.removeChild(val);
        						}
        					});
        					this._cellHeadTexts = newDayTxtElems;
        					xPos = rowHeaderWidth;
        					yPos = headHeight;
        					for ( var i = 0 ; i < rowCount ; i++ )
        					{
        						lineElem = usedSubHorzLines[i];
        						if ( !lineElem )
        						{
        							lineElem = new Eco.GraphicLine();
        							linesGroup.addChild(lineElem);
        							usedSubHorzLines[i] = lineElem;
        						}
        						lineElem.setStrokepen(cellLineStorke);
        						lineElem.setX1(xPos);
        						lineElem.setY1(yPos);
        						lineElem.setX2(lineWidth);
        						lineElem.setY2(yPos);
        						yPos += rowHeight;
        					}
        					for ( var i = rowCount, len = usedSubHorzLines.length ; i < len ; i++ )
        					{
        						linesGroup.removeChild(usedSubHorzLines[i]);
        					}
        					usedSubHorzLines.length = rowCount;
        					this._bodySubHorzLines = usedSubHorzLines;

        					//flag 처리
        					this._changedCellHeadBk = false;
        					this._changedCellColor = false;
        					this._changedCellFont = this._changedCellFont & (~2);
        					this._changedTodayProps = false;
        				}
        				else
        				{
        					this._bodyToDayElemClear(layer);
        					var cellHeadRects = this._cellHeadRects;
        					if ( cellHeadRects )
        					{
        						for ( var i = 0, len = cellHeadRects.length ; i < len ; i++ )
        						{
        							backgroundGroup.removeChild(cellHeadRects[i]);
        						}
        						this._cellHeadRects = null;
        					}
        					var dayTxtElems = this._cellHeadTexts;
        					if ( dayTxtElems )
        					{
        						Eco.object.Each(dayTxtElems, function(prop, val) {
        							if ( val )
        							{
        								cellLabelsGroup.removeChild(val);
        							}
        						});
        						this._cellHeadTexts = null;
        					}

        					var cellSubLineStorke = this._cellSubLine,
        						nonWorkingRects = this._nonWorkingRects||[],
        						nonWorkingRect;

        					rowHeight = data._hourHeight;
        					rowCount = 24;
        					lineWidth = bodyBandArea.width;
        					usedSubHorzLines = usedSubHorzLines||[];

        					nonWorkingRect = nonWorkingRects[0];
        					if ( !nonWorkingRect )
        					{
        						nonWorkingRect = new Eco.GraphicRect();
        						backgroundGroup.addChild(nonWorkingRect);
        						nonWorkingRects[0] = nonWorkingRect;
        					}
        					nonWorkingRect.setFillbrush(this._nonWorkingFillBrush);
        					nonWorkingRect.setFillgradation(this._nonWorkingGradation);
        					nonWorkingRect.setX(rowHeaderWidth);
        					nonWorkingRect.setY(0);
        					nonWorkingRect.setWidth(lineWidth - rowHeaderWidth);
        					nonWorkingRect.setHeight(rowHeight*9);

        					nonWorkingRect = nonWorkingRects[1];
        					if ( !nonWorkingRect )
        					{
        						nonWorkingRect = new Eco.GraphicRect();
        						backgroundGroup.addChild(nonWorkingRect);
        						nonWorkingRects[1] = nonWorkingRect;
        					}
        					nonWorkingRect.setFillbrush(this._nonWorkingFillBrush);
        					nonWorkingRect.setFillgradation(this._nonWorkingGradation);
        					nonWorkingRect.setX(rowHeaderWidth);
        					nonWorkingRect.setY(rowHeight*19);
        					nonWorkingRect.setWidth(lineWidth - rowHeaderWidth);
        					nonWorkingRect.setHeight(rowHeight*6);

        					this._nonWorkingRects = nonWorkingRects;

        					xPos = rowHeaderWidth;
        					yPos = Math.round(rowHeight/2);
        					for ( var i = 0 ; i < rowCount ; i++ )
        					{
        						lineElem = usedSubHorzLines[i];
        						if ( !lineElem )
        						{
        							lineElem = new Eco.GraphicLine();
        							linesGroup.addChild(lineElem);
        							usedSubHorzLines[i] = lineElem;
        						}
        						lineElem.setStrokepen(cellSubLineStorke);
        						lineElem.setX1(xPos);
        						lineElem.setY1(yPos);
        						lineElem.setX2(lineWidth);
        						lineElem.setY2(yPos);
        						yPos += rowHeight;
        					}
        					for ( var i = rowCount, len = usedSubHorzLines.length ; i < len ; i++ )
        					{
        						linesGroup.removeChild(usedSubHorzLines[i]);
        					}
        					usedSubHorzLines.length = rowCount;
        					this._bodySubHorzLines = usedSubHorzLines;

        					//flag 처리
        					this._changednonWorkingBk = false;
        				}
        				var usedHorzLines = this._bodyHorzLines||[],
        					rowHeadVertLine = this._rowHeadVertLine,
        					textElems = this._rowHeadTextElems||{},
        					newTextElems = {},
        					texts = this._getRowHeaderTexts(data),
        					fontfillbrush = this._rowHeaderFontFillBrush,
        					font = this._rowHeaderFont,
        					rowHeadArea = bodyBandArea.rowHeadArea,
        					rowHeaderWidth = rowHeadArea.width,
        					txt,
        					halfYGap = Math.round(rowHeight/2);

        					yPos = 0;
        					xPos = Math.round(rowHeaderWidth/2);
        					lineWidth = bodyBandArea.width;

        				var halign = ( data._mode == "month" ? "center" : "right" );
        				for ( var i = 0 ; i < rowCount ; i++ )
        				{
        					lineElem = usedHorzLines[i];
        					if ( !lineElem )
        					{
        						lineElem = new Eco.GraphicLine();
        						linesGroup.addChild(lineElem);
        						usedHorzLines[i] = lineElem;
        					}
        					lineElem.setStrokepen(cellLineStorke);
        					lineElem.setX1(0);
        					lineElem.setY1(yPos);
        					lineElem.setX2(lineWidth);
        					lineElem.setY2(yPos);

        					txt = texts[i];
        					txtElem = textElems[txt];
        					if ( !txtElem )
        					{
        						txtElem = new Eco.GraphicText();
        						layer.addChild(txtElem);
        						txtElem.setValign("middle");
        						newTextElems[txt] = txtElem;
        					}
        					else
        					{
        						newTextElems[txt] = txtElem;
        						delete textElems[txt];
        					}
        					txtElem.setHalign(halign);
        					txtElem.setX(xPos);
        					txtElem.setY(yPos + halfYGap);
        					txtElem.setFont(font);
        					if ( data._mode != "month" )
        					{
        						txt = txt.substring(0,2);
        					}
        					txtElem.setText(txt);
        					txtElem.setFillbrush(fontfillbrush);
        					yPos += rowHeight;
        				}
        				//vert line 생성
        				if ( !rowHeadVertLine )
        				{
        					rowHeadVertLine = new Eco.GraphicLine();
        					linesGroup.addChild(rowHeadVertLine);
        					this._rowHeadVertLine = rowHeadVertLine;
        				}
        				rowHeadVertLine.setStrokepen(cellLineStorke);
        				rowHeadVertLine.setX1(rowHeaderWidth);
        				rowHeadVertLine.setY1(0);
        				rowHeadVertLine.setX2(rowHeaderWidth);
        				rowHeadVertLine.setY2(rowHeadArea.height);

        				for ( var i = rowCount, len = usedHorzLines.length ; i < len ; i++ )
        				{
        					lineElem = usedHorzLines[i];
        					if ( lineElem )
        					{
        						linesGroup.removeChild(lineElem);
        					}
        				}
        				Eco.object.Each(textElems, function(prop, val) {
        					if ( val )
        					{
        						layer.removeChild(val);
        					}
        				});
        				usedHorzLines.length = rowCount;
        				this._bodyHorzLines = usedHorzLines;
        				this._rowHeadTextElems = newTextElems;				

        				this._changedRowHeadFont = this._changedRowHeadFont & (~4);
        				this._changedRowHeadColor = false;
        				this._changedRowHeadPadding = this._changedRowHeadPadding & (~2);
        			}
        		},
        		_getRowHeaderTexts: function(data)
        		{
        			var tmpdt, rowHeadTexts;

        			if ( data._mode == "month" )
        			{
        				rowHeadTexts = this._cachedWeekTexts;
        				if ( data._changedVisibleRange )
        				{
        					tmpdt = data._cloneDate(data._visibleStartdate);
        					var calendar = data._calendar,
        						weekUnit = Eco.TimeUnit.WEEK;
        					rowHeadTexts = [];
        					for ( var i = 0, len = data._rowCount; i < len ; i++ )
        					{
        						rowHeadTexts[i] = calendar.getWeek(tmpdt);
        						tmpdt = calendar.addUnits(tmpdt, weekUnit, 1);
        					}
        					this._cachedWeekTexts = rowHeadTexts;
        				}
        			}
        			else
        			{
        				rowHeadTexts = this._cachedHourTexts;
        				if ( !rowHeadTexts )
        				{
        					tmpdt = new Date();
        					tmpdt.setHours(0, 0, 0, 0);
        					var seq = 0,
        						calendar = data._calendar,
        						hourUnit = Eco.TimeUnit.HOUR,
        						dateUtil = Eco.date,
        						format = "hh tt",
        						//format = "hh",
        						len = 24;
        					rowHeadTexts = [];
        					while ( seq < len )
        					{
        						rowHeadTexts[seq] = dateUtil.getMaskFormatString(tmpdt, format);
        						tmpdt = calendar.addUnits(tmpdt, hourUnit, 1);
        						seq++;
        					}
        					this._cachedHourTexts = rowHeadTexts;
        				}
        			}
        			return rowHeadTexts;
        		},
        		_bodyToDayElemClear: function(layer)
        		{
        			this._cellTodayColIndex = null;
        			this._cellTodayRowIndex = null;
        			if ( this._bodyTodayTextElem )
        			{
        				this._bodyTodayTextElem = null;
        			}
        			if ( this._bodyTodayRectElem )
        			{
        				layer.removeChild(this._bodyTodayRectElem);
        				this._bodyTodayRectElem = null;
        			}
        		},
        		_bodyToDayElemUpdate: function(layer,dayTxtElem,font,data,rowHeaderWidth,headHeight)
        		{
        			if ( dayTxtElem )
        			{
        				if ( this._todayFontBold )
        				{
        					if ( font._bold )
        					{
        						dayTxtElem.setFont(font);
        					}
        					else
        					{
        						font._bold = true;
        						dayTxtElem.setFont(font._getValueStr());
        						font._bold = false;
        					}
        				}
        				else
        				{
        					if ( font._bold )
        					{
        						font._bold = false;
        						dayTxtElem.setFont(font._getValueStr());
        						font._bold = true; 
        					}
        					else
        					{
        						dayTxtElem.setFont(font);
        					}
        				}
        				dayTxtElem.setFillbrush(this._todayFontFillBrush);
        				this._bodyTodayTextElem = dayTxtElem;

        				var rectElem = this._bodyTodayRectElem;
        				if ( !rectElem )
        				{
        					var linesGroup = layer.getElementById("bodylinesgroup")
        					rectElem = new Eco.GraphicRect();
        					layer.insertBefore(rectElem, linesGroup);
        					this._bodyTodayRectElem = rectElem;
        				}
        				rectElem.setX(rowHeaderWidth + data._columnWidth*this._cellTodayColIndex);
        				rectElem.setY(data._rowHeight*this._cellTodayRowIndex);
        				rectElem.setWidth(data._columnWidth);
        				rectElem.setHeight(headHeight);
        				rectElem.setFillbrush(this._todayHeaderFillBrush);
        				rectElem.setFillgradation(this._todayHeaderGradation);
        			}
        		},
        		_layout: function(data)
        		{
        			var colHeadHeight = this.getColHeadHeight(),
        				allDayHeadHeight = ( data._mode == "month" ? this.eventItemBottomPadding : this.getAllDayHeadHeight(data)),
        				rowHeadWidth = this.getRowHeaderWidth(data._mode);

        			var headBandArea = this._headBandArea;
        			if ( !headBandArea )
        			{
        				headBandArea = new Eco.Rectangle();
        				this._headBandArea = headBandArea;
        				headBandArea.colHeadArea = new Eco.Rectangle();
        				headBandArea.allDayArea = new Eco.Rectangle();
        			}
        			var bodyBandArea = this._bodyBandArea;
        			if ( !bodyBandArea )
        			{
        				bodyBandArea = new Eco.Rectangle();
        				this._bodyBandArea = bodyBandArea;
        				bodyBandArea.rowHeadArea = new Eco.Rectangle();
        				bodyBandArea.cellsArea = new Eco.Rectangle();
        			}

        			var w = this._width,
        				h = this._height,
        				vertScrollWidth = this.vertscrollwidth;

        			if (data._mode == "month")
        			{
        				data._columnWidth = Math.floor((w - rowHeadWidth - vertScrollWidth) / data._columnCount);
        				data._rowHeight = Math.floor((h - (colHeadHeight + allDayHeadHeight)) / data._rowCount);
        			}
        			else
        			{
        				//data.hourHeight = 50;
        				data._columnWidth = (w - rowHeadWidth - vertScrollWidth) / data._columnCount;
        				data._rowHeight = 24 * data._hourHeight;
        			}

        			headBandArea.x = rowHeadWidth;
        			headBandArea.width = parseInt(data._columnWidth * data._columnCount);
        			headBandArea.y = 0;
        			headBandArea.height = colHeadHeight + allDayHeadHeight;

        			var subArea = headBandArea.colHeadArea;
        			subArea.x = 0;
        			subArea.width = headBandArea.width;
        			subArea.y = 0;
        			subArea.height = colHeadHeight;

        			subArea = headBandArea.allDayArea;
        			subArea.x = 0;
        			subArea.width = headBandArea.width;
        			subArea.y = colHeadHeight;
        			subArea.height = allDayHeadHeight;
        			
        			bodyBandArea.x = 0;
        			bodyBandArea.width = rowHeadWidth + headBandArea.width;
        			bodyBandArea.y = colHeadHeight + allDayHeadHeight;
        			bodyBandArea.height = h - colHeadHeight - (data._mode == "month" ? 0 : allDayHeadHeight);

        			subArea = bodyBandArea.rowHeadArea;
        			subArea.x = 0;
        			subArea.width = rowHeadWidth;
        			subArea.y = 0;
        			subArea.height = parseInt(data._rowHeight * data._rowCount);

        			subArea = bodyBandArea.cellsArea;
        			subArea.x = rowHeadWidth;
        			subArea.width = headBandArea.width;
        			subArea.y = 0;
        			subArea.height = bodyBandArea.rowHeadArea.height;
        		}
        	});
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
