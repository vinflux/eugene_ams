﻿//XJS=chartUtil.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /***********************************************************************************
        * FILE NAME   : chartUtil.xjs
        * PROGRAMMER  : eungheon.kim
        * DATE        : 2017.09.26
        * DESCRIPTION : VMS rMateChart 동적생성 
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/
        this.divName 			= "div_";
        this.divArray 			= [];
        this.staticKeyArray 	= [];
        this.gridArray 			= [];
        this.gridKeyArray 		= []
        window.chartKeyList 	= [];
        window.chartDataList 	= [];

        this.gfn_DivisionInit = function(parentDivObject )
        {
        	this.nLeft         = 0;
        	this.nTop          = 0;
        	this.positionCount = 0;
        	this.divWidth      = 500; //default
        	this.divHeight     = 400; //default
         	this.divGapWidth   = 10;
         	this.divGapHeight  = 10;

        	this.frameLeft = this.div_chart.getOffsetWidth();
        	this.frameTop  = this.div_chart.getOffsetHeight();
        	this.widthCount = parseInt( this.frameLeft / this.divWidth );
        }

        this.gfn_chartSearch = function(menuId )
        {
        	if( this.divArray[ menuId ] !== undefined ){
        		this.gfn_destroyList( this.divArray[ menuId ] );
        		this.staticKeyArray = [];
        	}
        	if( this.divArray.length > 0 ){
        		this.gfn_destroyList( this.divArray[ menuId ] );
        	}
        	if( this.gridArray.length > 0 ){
        		this.gfn_destroyList( this.gridArray );
        		this.gridKeyArray 	= [];
        	}

        	this.divArray[ menuId ] = new Array();
        	
        	this.gfn_setCommon( "BEANID", "chartController" );
        	this.gfn_setCommon( "METHODNM", "selectChart" );
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_vms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_chartHeader_LIST ds_detail=OUT_chartDetail_LIST";
            var sParam   = "uskey="+application.gds_menu.getColumn(application.gds_menu.findRow("mekey", menuId ), "uskey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.gfn_destroyList = function(arrayObject ){
        	if( this.gfn_isNotNull( arrayObject ) && arrayObject.length !== undefined ){
        		for( var i = arrayObject.length -1 ; i >= 0 ; i-- ){
        			this.gfn_destroyObject( arrayObject[i] );
        			arrayObject.splice(i, 1);
        		}
        	}
        }

        this.gfn_destroyObject = function(destroyObject ){
        	if( this.gfn_isNotNull( destroyObject ) ){
        		destroyObject.destroy();
        		destroyObject = null;
        	}
        }

        this.gfn_createDashBoard = function(menuId,parentDivObject,dsHeaderDataset,dsDetailDataset )
        {
        	var key 	   = "";
        	var layout 	   = "";
        	var title 	   = "";
        	var chart_type = "";	
        	var chart_width = 0;
        	var chart_height = 0;
        	
        	window.chartKeyList = [];
        	window.chartDataList = [];

        	parentDivObject.chartKeyList = [];
        	parentDivObject.childDivList = [];
        	
        	for( var i = 0 ; i < dsHeaderDataset.rowcount ; i++ ){
        		key 	   = dsHeaderDataset.getColumn( i, "dash_hdkey" );
        		layout 	   = dsHeaderDataset.getColumn( i, "chart_layout" );
        		title 	   = dsHeaderDataset.getColumn( i, "chart_title" );
        		subTitle   = dsHeaderDataset.getColumn( i, "chart_sub_title" );
        		chart_type = dsHeaderDataset.getColumn( i, "chart_type" );

        		chart_width = this.gfn_isNullZero( dsHeaderDataset.getColumn( i, "chart_width" ) );
        		chart_height = this.gfn_isNullZero( dsHeaderDataset.getColumn( i, "chart_height" ) );
        		
        		if( chart_width == 0 ){
        			chart_width = this.divWidth;
        		}
        		if( chart_height == 0 ){
        			chart_height = this.divHeight;
        		}		

        		var ds = new Dataset( ( "ds_" + key ) );
        		
        		ds.set_updatecontrol(false);
        		ds.assign( dsDetailDataset );
        		ds.deleteAll();
        		ds.clearData();

        		data_series_label = [];
        		
        		if( chart_type != "920" ){
        			var last_plot_series_on = "";
        			var rowIndex = 1;
        			var plotIndex = 0;
        			var currrow = -1;		
        			
        			//차트 데이터 생성
        			for(var j = 0 ; j < dsDetailDataset.rowcount ; j++){
        				if( key == dsDetailDataset.getColumn( j , "dash_hdkey") ){
        					ds.copyRow(ds.addRow(), dsDetailDataset, j);
        					if( last_plot_series_on != dsDetailDataset.getColumn(j, "plot_series_on") ){
        						last_plot_series_on = dsDetailDataset.getColumn(j, "plot_series_on");
        						data_series_label.push( dsDetailDataset.getColumn(j, "data_series_label") );
        					}
        				}
        			}
        			//ds pivot
        			for(var j = 0 ; j < ds.rowcount ; j++){
        				
        				plotIndex = parseInt ( j % parseInt( ds.getColumn(j, "plot_count") ) );
        				if( plotIndex == 0 ){
        					if(!ds.setColumn( plotIndex, ("data_value_"+rowIndex),ds.getColumn(j, "data_value"))){
        						ds.addColumn( ( "data_value_"+rowIndex++ ) , "string");
        					}			
        				}
        				
        				var dataValue = ds.getColumn(j, "data_value");		
        				ds.setColumn(plotIndex, ("data_value_"+ ( rowIndex -1 ) ), dataValue);
        			}
        			
        			//필요없는 로우 삭제 (plotIndex 별 처리)
        			for(var j = ( ds.rowcount -1 ) ; j >= 0 ; j--){
        				if( parseInt(ds.getColumn(j, "plot_series_on")) != 1 ){
        					ds.deleteRow( j );
        				}
        			}
        			ds.set_updatecontrol(true);

        			series_count = ds.getColumn( 0, "series_count" );		
        		}else{
        			var last_plot_series_on = "";
        			var rowIndex = -1;
        			var plotIndex = 0;
        			var currrow = -1;
        					
        			//그리드 데이터 생성
        			for(var j = 0 ; j < dsDetailDataset.rowcount ; j++){
        				if( key == dsDetailDataset.getColumn( j , "dash_hdkey") ){
        					ds.copyRow(ds.addRow(), dsDetailDataset, j);
        					if( last_plot_series_on != dsDetailDataset.getColumn(j, "plot_series_on") ){
        						last_plot_series_on = dsDetailDataset.getColumn(j, "plot_series_on");
        						data_series_label.push( dsDetailDataset.getColumn(j, "data_series_label") );
        					}
        				}
        			}
        			//ds pivot
        			for(var j = 0 ; j < ds.rowcount ; j++){
        				plotIndex = parseInt ( j % parseInt( ds.getColumn(j, "plot_count") ) );
        				if( plotIndex == 0 ){
        					++rowIndex;
        				} 
        				if(!ds.setColumn( rowIndex, ("data_value_"+(plotIndex+1)),ds.getColumn(j, "data_value"))){
        					ds.addColumn( ( "data_value_"+(plotIndex+1) ) , "string");
        					ds.addColumn( ( "axis_"+(plotIndex+1) ) , "string");
        				}			
        				ds.setColumn( rowIndex, ("data_value_"+(plotIndex+1)),ds.getColumn(j, "data_value"));
        				ds.setColumn( rowIndex, ("axis_"+(plotIndex+1)),ds.getColumn(j, "axis"));
        			}
        			//필요없는 로우 삭제 (plotIndex 별 처리)
        			
        			for(var j = ( ds.rowcount -1 ) ; j >= ds.getColumn(0, "axis_order_count") ; j--){
        					ds.deleteRow( j );
        			}
        			
        			ds.set_updatecontrol(true);		
        		}
        		
        		
        		var dataStructure 					= {};
        			dataStructure.id 				= ( menuId +"_"+ this.divName + key );
        			dataStructure.key 				= key;
        			dataStructure.title 			= title;
        			dataStructure.subTitle 			= subTitle;
        			dataStructure.series_count 		= series_count;
        			dataStructure.data_series_label = data_series_label;
        			dataStructure.dataSet			= ds;
        			dataStructure.chart_type		= chart_type;
        			dataStructure.chart_width		= chart_width;
        			dataStructure.chart_height		= chart_height;

        		var divObject = this.gfn_createDiv( parentDivObject, ( menuId + "_" + this.divName + key ), layout, i, this.nLeft, this.nTop, chart_width, chart_height, null, null );
        		
        		divObject.chartKey  = dataStructure;
        		divObject.chartData = this.gfn_createJsonObject( ( this.divName + key ), ds );
        		this.divArray[ menuId ].push( divObject );
        		
        		parentDivObject.childDivList.push( divObject );
        		window.chartKeyList.push( divObject.chartKey );
        		window.chartDataList.push( divObject.chartData );
        	}
        	this.gfn_calculateDivisionPosition( menuId, parentDivObject);
        }

        this.gfn_createJsonObject = function(key,datasetObject )
        {			
        	var jsonString = this.gfn_makeJsonString( datasetObject, null );
        	var jsonObject = eval("(" + jsonString + ")");
        	return jsonObject;
        }

        this.gfn_createDiv = function(targetObj,id,layout,positionIndex,nLeft,nTop,nWidth,nHeight,nRight,nBottom )
        {
        	var divObj = new Div( id, "absolute", nLeft, nTop, nWidth, nHeight, nRight, nBottom );
        	targetObj.addChild( divObj.id, divObj); 
        	return divObj;
        }

        this.gfn_calculateDivisionPosition = function(menuId,parentDivObject )
        {
        	this.gfn_DivisionInit();
        	var divType = "";
        	var divObj;
        	
        	window.chartKeyList = [];
        	window.chartDataList = [];

        	for(var i = 0 ; i < parentDivObject.childDivList.length ; i++ ){
        		divType = parentDivObject.childDivList[i].chartKey.chart_type;
        		divObj = parentDivObject.childDivList[i];

        		if( ( this.nLeft +  divObj.width + this.divGapWidth ) >= this.frameLeft ){
        			this.nLeft = 0;
        			if( i > 0 ){
        				this.nTop += divObj.height + this.divGapHeight;
        			}
        		}
        		
        		divObj.set_left( this.nLeft );
        		divObj.set_top( this.nTop );
        		divObj.show();
        		
        		window.chartKeyList.push( parentDivObject.childDivList[i].chartKey );
        		window.chartDataList.push( parentDivObject.childDivList[i].chartData );
        		
        		if( divType == "910" ){
        			this.gfn_createText( menuId, divObj );
        		}else if( divType == "920" ){
        			this.gfn_createGrid(menuId, divObj);
        		}else{
        			this.gfn_createChart( ( divObj.id ), divObj._unique_id, "settingChart", "", "" );
        		}
        		this.nLeft += ( divObj.width + this.divGapWidth ) ;
        	}
        }

        this.gfn_createText = function(menuId,parentDivObject )
        {
        	var aleradyCreate = false;
        	for( var i = 0 ; i < this.staticKeyArray.length ; i++ ){
        		if( this.staticKeyArray[i] ==  ( parentDivObject.id +"_header" ) ){
        			aleradyCreate = true;
        			break;
        		}
        	}
        	
        	if( !aleradyCreate ){
        		var staticObject = new Static( ( parentDivObject.id +"_header" ) , "absolute", 0, 0, parentDivObject.chartKey.chart_width, 100, null, null );
        		parentDivObject.addChild(( parentDivObject.id +"_header" ), staticObject); 	

        		staticObject.show();
        		staticObject.style.set_background("yellow");
        		staticObject.style.set_align("center middle");
        		staticObject.style.set_font("22 bold  arial");
        		staticObject.style.set_border("1 solid #808080ff");
        		staticObject.set_text(parentDivObject.chartKey.title);
        		staticObject.set_wordwrap("char");
        		
        		this.staticKeyArray.push( ( parentDivObject.id +"_header" ) );
        		
        		var staticObject2 = new Static( ( parentDivObject.id +"_detail" ) , "absolute", 0, 100, parentDivObject.chartKey.chart_width, ( parentDivObject.chartKey.chart_height -100), null,null );
        		parentDivObject.addChild(( parentDivObject.id +"_detail" ), staticObject2); 	
        		staticObject2.show();
        		//staticObject2.style.set_background("#0000ff");
        		staticObject2.style.set_align("center middle");
        		staticObject2.style.set_font("43 bold  arial");
        		staticObject2.style.set_border("1 solid #808080ff");
        		staticObject2.style.set_align("center middle");
        		staticObject2.set_wordwrap("char");
        		staticObject2.set_text(parentDivObject.chartKey.subTitle);
        	}
        }

        this.gfn_createGrid = function(menuId,parentDivObject )
        {
        	var aleradyCreate = false;
        	for( var i = 0 ; i < this.gridKeyArray.length ; i++ ){
        		if( this.gridKeyArray[i] ==  ("grd_"+parentDivObject.id) ){
        			aleradyCreate = true;
        			break;
        		}
        	}
        	
        	if( !aleradyCreate ){
        		var gridObj = new Grid(("grd_"+parentDivObject.id), "absolute", 0, 0, parentDivObject.chartKey.chart_width, parentDivObject.chartKey.chart_height, null,null );
        		gridObj.set_enableredraw(false);
        		//gridObj.set_formats(this.grd_sample.getCurFormatString());
        		parentDivObject.addChild(("grd_"+parentDivObject.id), gridObj); 
        		
        		//gridObj.addEventHandler("onheadclick", this.grd_ldlegid_onheadclick, this);
        		//gridObj.addEventHandler("ondrag", this.grd_ldlegid_ondrag, this);
        		//gridObj.addEventHandler("ondragmove", this.grd_ldlegid_ondragmove, this);
        		//gridObj.addEventHandler("ondrop", this.grd_ldlegid_ondrop, this);
        		console.log("parentDivObject.chartKey.dataSet.id : "+parentDivObject.chartKey.dataSet.id);

        		var sFormats  = '<Formats>    \n';
        			sFormats += '<Format id="default">    \n';
        			sFormats += '</Format>    \n';
        			sFormats += '</Formats>    \n';

        		gridObj.set_formats(sFormats);
        		gridObj.appendContentsRow("head");
        		gridObj.appendContentsRow("body");
        		gridObj.set_autofittype("col");
        		console.log("plot_count : "+parentDivObject.chartKey.dataSet.getColumn(0, "plot_count"));
        		for (var i = 0; i < parentDivObject.chartKey.dataSet.getColumn(0, "plot_count"); i++) {
        			console.log("axis : "+parentDivObject.chartKey.dataSet.getColumn(0, "axis_"+( i+1 )));
        			gridObj.appendContentsCol("body");
        			gridObj.setCellProperty("head", i, "text", parentDivObject.chartKey.dataSet.getColumn(0, "axis_"+( i+1 ) ) );
        			gridObj.setCellProperty("body", i, "text", ( "bind:data_value_"+( i+1 ) ) );
        		}

        		var datasetObj = eval( "("+"this."+("grd_ds_"+parentDivObject.id)+")" );
        		console.log("#### 1111 datasetObj : "+datasetObj);
        		if( this.gfn_isNull( datasetObj ) ){
        			console.log("#### create DB ##");
        			datasetObj =  new Dataset(("grd_"+parentDivObject.chartKey.dataSet.id));
        			this.addChild(("grd_ds_"+parentDivObject.id), datasetObj);
        		}
        		console.log("#### 2222 datasetObj : "+datasetObj);
        		
        		datasetObj.assign(parentDivObject.chartKey.dataSet);
        		
        		gridObj.set_binddataset(datasetObj);
        		gridObj.set_enableredraw(true);
        		gridObj.show();
        		
        		this.gridArray.push(gridObj);
        		this.gridKeyArray.push( ("grd_"+parentDivObject.id) );
        		console.log("grid Key : "+("grd_"+parentDivObject.id));
        	}
        }

        
        this.gfn_createChart = function(chartId,divId,functionName,layoutString,chartData )
        {
        	var chartVars = "rMateOnLoadCallFunction="+functionName;
        	rMateChartH5.create( chartId, divId, chartVars, "100%", "100%");
        }

        window.settingChart = function(id ) {	
        	var chartObj = document.getElementById( id );
        	var layoutString = null;
        	var chartData = null;
        	var optionList = [
        		{ dimensional:'3D', showDataTips:true, color:'#40b2e6', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'3D', showDataTips:true, color:'#ff0000', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'2D', showDataTips:true, color:'#40b2e6', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'3D', showDataTips:true, color:'#00ff00', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'3D', showDataTips:true, color:'#40b2e6', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'3D', showDataTips:true, color:'#ff0000', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'2D', showDataTips:true, color:'#40b2e6', showDataEffect:true, selectionMode:'multiple' },
        		{ dimensional:'3D', showDataTips:true, color:'#00ff00', showDataEffect:true, selectionMode:'multiple' }
        	];
        //	console.log("settingChart keyList : "+window.chartKeyList.length);
        	for(var i = 0 ; i < window.chartKeyList.length ; i ++ ){
        		if( window.chartKeyList[i].id == id ){
        			layoutString = rMateLayoutGenerator( id, window.chartKeyList[i], optionList[i] );
        			chartData = window.chartDataList[ i ] ;
        			break;
        		}
        	}	
        	if( layoutString != null && layoutString != "" ){
        		chartObj.setLayout( layoutString );
        		chartObj.setData( chartData );
        	}
        }

        window.rMateLayoutGenerator = function(key,dataObject,options )
        {
        	rMateSetting( dataObject.title, dataObject.subTitle, options );
        	
        	if( dataObject.series_count ===  undefined || dataObject.series_count == null || dataObject.series_count == "" ){
        		dataObject.series_count = 1;
        	}
        	
        	var chartString = "";
        	var multiSeries = "";
        	var data_series_label_index = 0 ;
        	//차트 타입별 오브젝트 생성
        	console.log("dataObject.chart_type : "+dataObject.chart_type);
        	if( dataObject.chart_type == "110" ){
        		//꺾은선(Line)
        		chartString = chartLineGenerator( dataObject );
        	}else if( dataObject.chart_type == "120" ){
        		//영역(Area)
        		chartString = chartAreaGenerator( dataObject );
        	}else if( dataObject.chart_type == "130" ){
        		//가로막대(Bar)
        		chartString = chartBarGenerator( dataObject );
        	}else if( dataObject.chart_type == "140" ){
        		//세로막대(Column)
        		chartString = chartColumnGenerator( dataObject );
        	}else if( dataObject.chart_type == "150" ){
        		//원형(Pie)
        		chartString = chartPieGenerator( dataObject );
        	}else if( dataObject.chart_type == "160" ){
        		//방사형(Rader)
        		chartString = chartRaderGenerator( dataObject );
        	}else if( dataObject.chart_type == "210" ){
        		//꺽은선형+세로 막대형(Line + Column)
        		chartString = chartCombinationGenerator( dataObject, ["Column", "Line"] );
        	}else if( dataObject.chart_type == "220" ){
        		//꺽은선형+영역형(Line + Area) 
        		chartString = chartCombinationGenerator( dataObject, ["Area", "Line"] );
        	}else if( dataObject.chart_type == "230" ){
        		//꺽은선형+영역형+세로 막대형(Line + Area + Column)
        		chartString = chartCombinationGenerator( dataObject, ["Column", "Area", "Line"] );
        	}else if( dataObject.chart_type == "240" ){
        		//거품형+꺽은선형(Buble + Line)
        		//없음
        		chartString = chartColumnGenerator( dataObject );
        	}else if( dataObject.chart_type == "910" ){
        		//텍스트(TEXT)
        		//chartString = chartColumnGenerator( dataObject );
        		chartString = textGenerator( dataObject );
        	}else if( dataObject.chart_type == "920" ){
        		//그리드(GRID)
        		chartString = chartColumnGenerator( dataObject );
        	}else{
        		chartString = chartColumnGenerator( dataObject );
        	}
        	return chartString;
        }

        window.rMateSetting = function(title,subTitle,optionObject ){
        	this.rMate 										= {};
        	this.rMate.backgroundColor 						= "#FFFFFF";
        	this.rMate.borderStyle     						= "none";
        	this.rMate.titleText							= title;
        	this.rMate.subTitleText							= subTitle;
        	this.rMate.mouseOverAction						= true;
        	this.rMate.showDataTips							= true;
        	this.rMate.categoryField						= "";
        	this.rMate.seriesList							= [];
        	this.rMate.seriesDisplayNameList				= [];
        	this.rMate.seriesBindDataList					= [];
        	
        	this.rMate.chartSet 							= {};
        	this.rMate.chartSet.startHeader 				= '<rMateChart backgroundColor="' + this.rMate.backgroundColor + '"  borderStyle="' + this.rMate.borderStyle + '">';
        	this.rMate.chartSet.startHeader 			   += '<Options><Caption text="' + this.rMate.titleText + '"/>';
        	this.rMate.chartSet.startHeader 			   += '<SubCaption text="' + this.rMate.subTitleText + '"/>';
        	this.rMate.chartSet.startHeader 			   += '<Legend defaultMouseOverAction="' + this.rMate.mouseOverAction + '"/>';
        	this.rMate.chartSet.startHeader 			   += '</Options>';
        	this.rMate.chartSet.endHeader					= '</rMateChart>';
        	this.rMate.chartSet.startSeries					= '<series>';
        	this.rMate.chartSet.endSeries					= '</series>';
        	this.rMate.chartSet.showDataEffect				= '<showDataEffect><SeriesInterpolate/></showDataEffect>';

        	this.rMate.chartSet.column						= {};
        	
        	if( optionObject === undefined || optionObject == null || optionObject == ""  ){
        		this.options = { dimensional:'2D', showDataTips:true, color:'#40b2e6', showDataEffect:true, selectionMode:'single' };
        	}else{
        		this.options = optionObject;
        	}
        }

        window.textGenerator = function(dataObject ){

        }

        window.chartLineGenerator = function(dataObject ){
        	this.options.dimensional = "2D";
        	var chart_start  = '<Line'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<horizontalAxis><CategoryAxis categoryField="axis"/></horizontalAxis>';
        		chart_start += '<verticalAxis><LinearAxis /></verticalAxis>';
        	var chart_end    = '</Line'+this.options.dimensional+'Chart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		var series_body  = '<Line'+this.options.dimensional+'Series yField="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</Line'+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartAreaGenerator = function(dataObject ){
        	this.options.dimensional = "2D";
        	var chart_start  = '<Area'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<horizontalAxis><CategoryAxis categoryField="axis"/></horizontalAxis>';
        		chart_start += '<verticalAxis><LinearAxis /></verticalAxis>';
        	var chart_end    = '</Area'+this.options.dimensional+'Chart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		var series_body  = '<Area'+this.options.dimensional+'Series yField="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</Area'+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartColumnGenerator = function(dataObject ){
        	var chart_start  = '<Column'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<horizontalAxis><CategoryAxis categoryField="axis"/></horizontalAxis>';
        		chart_start += '<verticalAxis><LinearAxis /></verticalAxis>';
        	var chart_end    = '</Column'+this.options.dimensional+'Chart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		var series_body  = '<Column'+this.options.dimensional+'Series yField="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</Column'+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartBarGenerator = function(dataObject ){
        	var chart_start  = '<Bar'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<horizontalAxis><LinearAxis /></horizontalAxis>';
        		chart_start += '<verticalAxis><CategoryAxis categoryField="axis"/></verticalAxis>';
        	var chart_end    = '</Bar'+this.options.dimensional+'Chart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		var series_body  = '<Bar'+this.options.dimensional+'Series xField="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</Bar'+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartPieGenerator = function(dataObject ){
        	var chart_start  = '<Pie'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'">';
        	var chart_end    = '</Pie'+this.options.dimensional+'Chart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		var series_body  = '<Pie'+this.options.dimensional+'Series field="data_value_'+(i+1)+'"';
        			if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] !== "undefined" && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' nameField="data_series_label"';
        			}
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</Pie'+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartRaderGenerator = function(dataObject ){
        	//type 을 디비에서 관리하도록 변경 필요
        	var raderType = "polygon";
        	var chart_start  = '<RadarChart showDataTips="'+this.options.showDataTips+'" type="'+raderType+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<radialAxis><LinearAxis id="rAxis"/></radialAxis>';
        		chart_start += '<angularAxis><CategoryAxis id="aAxis" categoryField="axis"/></angularAxis>';
        		chart_start += '<radialAxisRenderers>';
        		chart_start += '<Axis2DRenderer axis="{rAxis}" horizontal="true" visible="true" tickPlacement="outside"/>';
        		chart_start += '</radialAxisRenderers>';				
        		chart_start += '<angularAxisRenderers>';
        		chart_start += '<AngularAxisRenderer axis="{aAxis}"/>';
        		chart_start += '</angularAxisRenderers>';				
        		
        	var chart_end    = '</RadarChart>';
        	
        	var multiSeries = "";
        	for( var i = 0 ; i < dataObject.series_count ; i++){
        		console.log("### [ "+i+" ]");
        		var series_body  = '<RadarSeries radius="0" selectionRadius="0" field="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</RadarSeries>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        window.chartCombinationGenerator = function(dataObject,seriesArray ){
        	var chart_start  = '<Combination'+this.options.dimensional+'Chart showDataTips="'+this.options.showDataTips+'" selectionMode="'+this.options.selectionMode+'" columnWidthRatio="0.48">';
        		chart_start += '<horizontalAxis><CategoryAxis categoryField="axis"/></horizontalAxis>';
        		chart_start += '<verticalAxis><LinearAxis /></verticalAxis>';
        	var chart_end    = '</Combination'+this.options.dimensional+'Chart>';

        	var multiSeries = "";
        	var originalDimensional = this.options.dimensional;
        	
        	for( var i = 0 ; i < seriesArray.length ; i++){
        		if( seriesArray[i] == "Line" || seriesArray[i] == "Area" ){
        			this.options.dimensional = "2D";
        		}else{
        			this.options.dimensional = originalDimensional;
        		}
        		var series_body  = '<'+seriesArray[i]+this.options.dimensional+'Series yField="data_value_'+(i+1)+'"';
        		    if( dataObject.data_series_label[ i ] !== undefined && dataObject.data_series_label[ i ] != "" && dataObject.data_series_label[ i ] != "null"  && dataObject.data_series_label[ i ] != "NULL"){
        				series_body += ' displayName="'+dataObject.data_series_label[ i ]+'"';
        		    }
        		    series_body += '>';
        			//series_body += '<fill><SolidColor color="'+this.options.color+'"/></fill>';
        			if( this.options.showDataEffect ){
        				series_body += this.rMate.chartSet.showDataEffect;
        			}
        		series_body += '</'+seriesArray[i]+this.options.dimensional+'Series>';

        		multiSeries += series_body;
        	}
        	return ( this.rMate.chartSet.startHeader + chart_start + this.rMate.chartSet.startSeries + multiSeries + this.rMate.chartSet.endSeries + chart_end + this.rMate.chartSet.endHeader );
        }

        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
