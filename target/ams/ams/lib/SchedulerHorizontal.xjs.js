﻿//XJS=SchedulerHorizontal.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        if ( !JsNamespace.exist("Eco.Scheduler.Horizontal")  )
        {
        	JsNamespace.declareClass("Eco.Scheduler.Horizontal",{
        		/**
        		 * HashMap 생성자(constructor)
        		 * @class HashMap
        		 * @classdesc key와 value 를 묶어서 하나의 entry로 저장한다.
        		 * hasing을 사용하기 때문에 많은 양의 데이터를 검색하는데 뛰어난 성능을 보인다.<br>
        		 * key값은 중복되지 않고 value값은 중복허용.<br>
        		 * @constructor HashMap
        		*/
        		initialize: function(pDiv,eventItem,bCreate)
        		{
        			this._pDiv = pDiv;
        			this._eventItem = eventItem;
        			if ( bCreate )
        			{
        				this._comp = Eco.XComp.Factory.getXComp(this._pDiv, "Static");
        				this._comp._ecoObj = this;
        			}
        		},
        		setVisible: function(val)
        		{
        			var comp = this._comp;
        			if ( comp )
        			{
        				Eco.XComp.Factory.setProperties(comp, "visible", val); 
        			}
        		},
        		updateText: function()
        		{
        			Eco.XComp.Factory.setProperties(this._comp, "text", this._eventItem.summary);
        		},
        		draw: function(stylestr,overStyles,isOnlyStyle,sdt,edt,range,x,y,width,height)
        		{
        			var eventItem = this._eventItem,
        				comp = this._comp,
        				factory = Eco.XComp.Factory;

        			if ( isOnlyStyle && comp )
        			{
        				factory.setProperties(comp, "style", stylestr + this._borderType + overStyles); 
        			}
        			else
        			{
        				var comp = this._comp;
        				if ( !comp )
        				{
        					comp = factory.getXComp(this._pDiv, "Static");
        					this._comp = comp;
        					this._comp._ecoObj = this;
        				}
        				if ( range )
        				{
        					var borderRadius = Math.round(height/2),
        						borderType = "bordertype: round " + borderRadius + " " + borderRadius,
        						addValues = [], bothRound = 2;
        					if ( eventItem.startdate < sdt && sdt.getTime() == range[0].getTime() )
        					{
        						addValues.push("righttop");
        						addValues.push("rightbottom");
        						bothRound--;
        					}

        					if ( eventItem.enddate > edt && edt.getTime() == range[1].getTime() )
        					{
        						addValues.push("lefttop");
        						addValues.push("leftbottom");
        						bothRound--;
        					}
        					if ( bothRound == 0 )
        					{
        						//borderType = "";
        						borderType = "bordertype: normal;";
        					}
        					else
        					{
        						borderType += " " + addValues.join(" ") + ";";
        					}
        					this._borderType = borderType;
        					factory.setProperties(comp, "style", stylestr + borderType + overStyles);
        				}
        				factory.setProperties(comp, "text", eventItem.summary);
        				factory.setRect(comp, x, y, width, height);
        			}
        		},
        		clear : function()
        		{
        			var comp = this._comp;
        			if ( comp )
        			{
        				comp._ecoObj = null;
        				Eco.XComp.Factory.release(comp, null, true);
        			}
        			this._pDiv = null;
        			this._eventItem = null;
        		}
        	});
        }
        
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
