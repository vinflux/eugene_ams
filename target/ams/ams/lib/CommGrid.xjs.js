﻿//XJS=CommGrid.xjs
(function()
{
    return function(path)
    {
        var obj;
    
        // User Script
        this.registerScript(path, function(exports) {
        /* 
         ===============================================================================
         ==  Grid관련 공통함수들은 여기에 작성한다.
         ===============================================================================
         ● gfn_GridSort      : 그리드의 Sort를 처리한다.
         ● gfn_clearSortMark : Seleted Column을 제외한 Sort Mark 제거
         ● gfn_DsCheckValid  : DataSet 내의 데이터 무결성을 검사하는 함수
         ● gfn_ItemCheck     : DataSet 내의 데이터 무결성을 검사하는 함수
         */

        // 헤더 클릭시 정렬
        this.fv_constAscMark = "↑";
        this.fv_constDescMark = "↓";
        // this.fv_constAscMark = "▼";
        // this.fv_constDescMark = "▲";

        // ✔ Ÿ Ÿ ૪ ⵖ Ύ Ϋ ᛉ Ý Ŷ Ύ Ÿ Y ╦ ₸ ᛉ
        this.fv_filterMark = "ᛉ";

        this.fv_constSortFlag = false;

        this.ct_separator = " ";

        this.gv_filterDs;
        this.gv_filterCol = "";
        this.gv_gridHeadText;
        this.gv_gridHeadBindText;
        this.gv_grid;
        this.gv_e;
        this.gv_col;
        this.gv_row;
        this.gv_gridMenu;
        this.gv_bgColor = "#6d6f86";
        this.gv_bgColorD = "#888fa2";
        this.gv_bgBodyColor = "#fff8d5";
        this.gv_bg1BodyColor = "#fff5c2";
        this.gv_bgSelBodyColor = "#e1e0d9";
        this.gv_bgMtColor = "#ff0000ff";
        this.gv_isPop = "";

        /**
        * 그리드의 기능 처리
        * @param  {object} obj - 기능을 처리할 Grid Object
        * @param  {object} e - GridClickEventInfo
        * @return N/A
        */
        this.gfn_gridProc = function(obj,e)
        {
            var oData = obj.getBindDataset();
            if (oData.rowcount == 0) {
                this.gfn_clearSortMark(obj);
                return;
            }
            
            var sType = obj.getCellProperty("head", e.cell, "displaytype");
            if(sType == "checkbox") {
                this.gfn_setGridCheckAll(obj, e);
            } else {
                this.gfn_gridSort(obj, e);
            }
        }

        /**
        * 그리드의 Sort를 처리한다.
        * @param  {object} obj - Sort 를 처리할 Grid Object
        * @param  {object} e - GridClickEventInfo
        * @return N/A
        */
        this.gfn_gridSort = function(obj,e)
        {
            // 컬럼의 정렬방식을 'head'의 text에 "↑,↓"여부로 판단.
            // 이미지로 대체 가능.
            var sType = obj.getCellProperty("head", e.cell, "displaytype");
            if (sType == "checkbox") {
                return;
            }
            
            var oData = obj.getBindDataset();
            var sColId = (obj.getCellProperty("body", e.col, "text")).toString().split(":")[1];
            var sHeadText;
            
            for (var i = 0; i < obj.getCellCount("head"); i++) {
                if(this.gfn_isNull(obj.getCellText(-1, i))) {
                    continue;
                }
                
                sHeadText = obj.getCellText(-1, i);
                
                if (i == e.cell) {
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark) {
                        obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1) + this.fv_constDescMark);
                        oData.set_keystring("S:-" + sColId);
                    } else if (sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
                        obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1) + this.fv_constAscMark);
                        oData.set_keystring("S:+" + sColId);
                    } else {
                        obj.setCellProperty("head", i, "text", sHeadText + this.fv_constAscMark);
                        oData.set_keystring("S:+" + sColId);
                    }
                } else {
                    // 정렬표시 삭제
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark || sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
                        obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1));
                    }
                }
            }
        }

        /**
        * Seleted Column을 제외한 Sort Mark 제거
        * @param  {object} obj - Sort Mark 제거를 처리할 Grid Object
        * @param  {integer} nCell - 예외 cell index
        * @return N/A
        */
        this.gfn_clearSortMark = function (obj,nCell)
        {
            var nColCnt = obj.getCellCount("head");
            var sRepText;
            if(this.gfn_isNull(nCell)) nCell = -1;

            for (var ii = 0; ii < nColCnt; ii++) {
                if(nCell == ii) { // 선택한 Cell을 제외하고 처리
                    continue;
                }
                
                if(this.gfn_isNull(obj.getCellProperty("head", ii, "text"))) {
                    continue;
                }

                sRepText = obj.getCellProperty("head", ii, "text").split(this.fv_constAscMark).join("").split(this.fv_constDescMark).join("");
                obj.setCellProperty("head", ii, "text", sRepText);
            }
        }

        /**
        * Seleted Sort Mark 제거
        * @param  {object} obj - Sort Mark 제거를 처리할 Grid Object
        * @param  {integer} nCell - 예외 cell index
        * @return N/A
        */
        this.gfn_clearSortAndMark = function (obj)
        {
        	var sColNm = "";
        	var grdDs = obj.getBindDataset();
            grdDs.set_keystring("");
        	
        	obj.set_enableredraw(false);
        	for(var i = 0 ; i < obj.getCellCount("head") ; i++){
                sColNm = nexacro.replaceAll(obj.getCellProperty("Head", i, "text") , "bind:", "");
                
                if(this.gfn_isNull(sColNm) || sColNm == "NO") {
                    continue;
                }
                
                grdLang = this.gfn_getLang(sColNm.substr(1));
        		grdDs.setConstColumn(sColNm, grdLang);
            }
            obj.set_enableredraw(true);
        }

        /**
        * Grid Head중 check box가 있을 경우, check box 클릭 이벤트 발생시 전체 row에 대한 check/uncheck 설정 함수
        * @param  {object} obj - 체크를 처리할 Grid Object
        * @param  {object} e = GridClickEventInfo
        * @return N/A
        */
        this.gfn_setGridCheckAll = function (obj,e)
        {
            var sVal;
            var sChkCol;
            var oData;
            
            if (obj.readonly == true || obj.getCellProperty("head", e.cell, "displaytype") != "checkbox") {
                return;
            }

            oData = obj.getBindDataset();
            sChkCol = this.gfn_nvl(obj.getCellProperty("body", e.col, "text"), "");
            if(this.gfn_isNotNull(sChkCol)) sChkCol = sChkCol.split("bind:").join("");

            // Head셋팅
            var sText = obj.getCellProperty("head", e.cell, "text");
            if(sText.indexOf("bind") >= 0) sText = "H";
            
            if(sText == "H"){
        		sVal = oData.getConstColumn("HCHK");
        		
        		if(this.gfn_isNull(sVal) || sVal == "0"){
        			oData.setConstColumn("HCHK","1");
        			sVal = "1";
        		}else{
        			oData.setConstColumn("HCHK","0");
        			sVal = "0";
        		}
            }else{
        		sVal = this.gfn_nvl(obj.getCellProperty("head", e.cell, "text"), "0");
        		
        		if(sVal == "0"){
        			obj.setCellProperty("head", e.cell, "text", "1");
        			sVal = "1";
        		}else{
        			obj.setCellProperty("head", e.cell, "text", "0");
        			sVal = "0";
        		}
        	}

            // Body셋팅
            if(sChkCol == ""){
        		if(sVal == "1") obj.setCellProperty("body", e.cell, "text", sVal);
        		else obj.setCellProperty("body", e.cell, "text", "");
            }else{
        		for (var i = 0; i < oData.rowcount; i++) {
        			oData.setColumn(i, sChkCol, sVal);
        		}
            }
        }

        /**
         * @class 그리드의 내용을 범위로 지정하여 복사(Ctrl+C) 할 수 있음
         * @param obj:Grid (단일Export - Grid Object, 다수Export - Array Objec[Grid Object,Grid Object])
         * @param e:KeyEventInfo
         * @return None
         */  
        this.gfn_GridCopy = function (obj,e,ds_grd,cell)
        {
            // Ctrl + C
            if (e.ctrlKey && e.keycode == 67){
                this.gfn_clipboardCopy(obj, e, ds_grd, cell);
            } else if (e.ctrlKey && e.keycode == 86){    // Ctrl + V
                //this.gfn_clipboardPaste(obj);
            }
        }

        /**
         * @class Clipboard에 Copy된 내용을 그리드의 선택된 영역에 Copy 한다.
         * @param objGrid: Area Select 된 Grid
         * @return None gfn_clipboardCopy(objGrid);
         */
        this.gfn_clipboardCopy = function (obj,e,ds_grd,cell)
        {
            var orgDataset = ds_grd;
            var strColID;
            var strValue;
            var strClipboard = "";
            var nAreaStartRow;
            var nAreaEndRow;
            var nAreaStartCol;
            var nAreaEndCol;

            if(obj.selecttype == "area"){
                nAreaStartRow = obj.selectstartrow;
                nAreaEndRow = obj.selectendrow;
                nAreaStartCol = obj.selectstartcol;
                nAreaEndCol = obj.selectendcol;
        //     }else if(obj.selecttype == "row"){
        //         nAreaStartRow = obj.selectstartrow;
        //         nAreaEndRow = obj.selectendrow;
        //         nAreaStartCol = 0;
        //         nAreaEndCol = obj.getCellCount("Body");
            }else{
                nAreaStartRow = obj.selectstartrow;
                nAreaEndRow = obj.selectendrow;
                
        		if(this.gfn_isNull(e)){
        			nAreaStartRow = this.gv_row;
        			nAreaEndRow = this.gv_row;
        		}
                
                nAreaStartCol = cell;
                nAreaEndCol = cell;
            }

            var sDispType;
            
            for (var nRow = nAreaStartRow; nRow <= nAreaEndRow; nRow++){
                if(nRow != nAreaStartRow) strClipboard = strClipboard + "\n";
                
                for (var nCell = nAreaStartCol; nCell <= nAreaEndCol; nCell++){
                    sDispType = obj.getCellProperty("body", nCell, "displaytype");
        //            trace("sDispType = " + sDispType);
                    
                    if(sDispType != "combo"){
                        strColID = String(obj.getCellProperty("body", nCell, "text"));
                        strValue = this.gfn_isNullEmpty(orgDataset.getColumn(nRow, strColID.substr(5)));
                    }else{
                        strValue = obj.getCellText(nRow, nCell);
                    }
                    
                    strClipboard = strClipboard + strValue + this.ct_separator;
                }
                
                strClipboard = strClipboard.substr(0, strClipboard.length - 1);
        //        trace("gfn_clipboardCopy > strClipboard = " + strClipboard);
            }
            
            application.gv_arrClipboard = strClipboard;
            
            this.open_setClipboard(strClipboard);
            
            return;
        }

        this.open_setClipboard = function (str){
        	//Runtime,  IE8
        	if(nexacro.Browser == "Runtime" || (nexacro.Browser == "IE" && nexacro.Browser == "8")){
        		system.setClipboard("CF_TEXT", str);
        	}else{
        		if(!this.gfn_isNull(application.popupframes[this.name])){
        			if(this.gfn_isNull(this.TextArea)){
        				var objTextArea = new TextArea("TextArea", "absolute", 0, 2000, 100, 0, null, null);
        				this.addChild("TextArea", objTextArea);
        				objTextArea.show();
        				objTextArea.setFocus();
        				objTextArea.set_value(str);
        				objTextArea.setSelect(0);
        			}else{
        				this.TextArea.setFocus();
        				this.TextArea.set_value(str);
        				this.TextArea.setSelect(0);
        			}
        		}else{
        			application.gv_TopFrame.form.txa_copy.setFocus();
        			application.gv_TopFrame.form.txa_copy.set_value(str);
        			application.gv_TopFrame.form.txa_copy.setSelect(0);
        		}
        		
        		window.document.execCommand("copy");
        	}
        }

        /**
        * Grid를 Excel로 Export하는 함수
        * @param  {object} 단일Export -  Grid Object  , 다수Export - Array Objec[Grid Object,Grid Object]
        * @param  {string} sFileName - Export될 파일명
        * @return N/A
        */

        this.fv_exportObject;
        this.fv_exportFileName;

        this.gfn_exportExcel2 = function(objForm,obj,sFileName,strSheet)
        {    
            this.ffobj = objForm;
            this.fv_exportObject = obj;            
            this.fv_exportFileName = sFileName;

            application.set_usewaitcursor(true,true);

            this.ct_sheet = this.gfn_isNull(strSheet) ? "Sheet1" :strSheet;

            this.gfn_exportExcelProcess();
        }

        /**
        * _exportExcelProcess
        * @return N/A
        */
        this.gfn_exportExcelProcess = function()
        {
            var obj = this.fv_exportObject;
            var sFileName = this.fv_exportFileName;
            var oGrid;
            var sSheetName;
            
            var sType = obj.toString().toUpperCase();
            
            if(!this.gfn_isNull(sFileName)){
                sFileName = sFileName + "_" + this.gfn_today();
            }else{
                sFileName = this.gfn_today();
            }

            var exportObj = new ExcelExportObject();

            var sSvcUrl = application.services["svc"].url + "ams/XExportImport";
            this.ffobj.setWaitCursor(true,true);
         
            exportObj.addEventHandler("onsuccess", this._exportExcelEnd, this);
            exportObj.addEventHandler("onerror", this._exportExcelEnd, this);

            exportObj.set_exporttype(nexacro.ExportTypes.EXCEL2007);
            exportObj.set_exporturl(sSvcUrl);
            exportObj.set_exportfilename(sFileName);

            if(sType == "[OBJECT GRID]"){
                oGrid = obj;
                sSheetName = "sheet1";
                exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  sSheetName + "!A1","allband","allrecord","suppress","allstyle","font","both","cellline");
            }else{
                for(var i=0; i<obj.length; i++){
                    sSheetName = this.ct_sheet+(i+1);
                    oGrid = obj[i];
                    exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  sSheetName + "!A1","allband","allrecord","suppress","allstyle","font","both","cellline");
                }
            }

            exportObj.exportData();
        }

        this.gfn_exportExcel = function(objForm,obj,sFileName,bOneSheet)
        {
            this.ffobj = objForm;
            this.fv_exportObject = obj;
           
            this.fv_exportFileName = sFileName;
            this.fv_bOneSheet = this.gfn_isNull(sFileName) ? "Sheet1" : sFileName;
            
            this.fv_exportFileName = nexacro.replaceAll(this.fv_exportFileName, "/", "_");
            this.fv_bOneSheet = nexacro.replaceAll(this.fv_bOneSheet, "/", "_");
                  
            if(obj.toString().toUpperCase() == "[OBJECT GRID]"){
                this.fv_exportGridSelectType = this.fv_exportObject.selecttype;
                
                if(this.fv_exportGridSelectType == "area"){
                    this.fv_exportObject.set_selecttype("row");
                }
            }else{
                this.fv_exportGridSelectType = [];
                
                for(var i = 0; i < obj.length; i++){
                    this.fv_exportGridSelectType.push(obj[i].selecttype);
                    
                    if(obj[i].selecttype == "area"){
                        obj[i].set_selecttype("row");
                    }
                }
            }
            
            this._gfn_exportExcelProcess();
        }

        this._gfn_exportExcelProcess = function()
        {
            var obj = this.fv_exportObject;
            
            var sFileName = this.fv_exportFileName;
            var dToday = new Date();
            var oGrid;
            var sSheetName;
            var strType = obj.toString().toUpperCase();
            var sDateTime = "" + this.gfn_toDateTime();
                
            if(!this.gfn_isNull(sFileName)){
               sFileName = "[" + sFileName + "]_[" + sDateTime.substr(0, 8) + "]_[" + sDateTime.substr(8, 6) + "]";
            }else{
                sFileName = this.gfn_today();
            }
               
            this.exportObj = new ExcelExportObject();
            var sSvcUrl = application.services["svc"].url+"ams/XExportImport";
            
            this.exportObj.addEventHandler("onerror", this._gfn_exportExcel_onerror, this);
            this.exportObj.addEventHandler("onsuccess", this._gfn_exportExcel_onsuccess, this);
            this.exportObj.addEventHandler("onprogress", this._gfn_exportExcel_onprogress, this);
            
            this.exportObj.set_exporttype(nexacro.ExportTypes.EXCEL2007);
            this.exportObj.set_exportuitype("exportprogress");
            this.exportObj.set_exporteventtype("itemrecord");
            this.exportObj.set_exportmessageprocess("%d[%d/%d]");
            this.exportObj.set_exporturl(sSvcUrl);
            this.exportObj.set_exportfilename(sFileName);
                
            if (strType == "[OBJECT GRID]"){
                oGrid = obj;
                sSheetName = this.ct_sheet+"1";
                this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A1","allband", "allrecord", "merge", "allstyle", "image", "font", "both", "cellline");
            } else {
                if(this.fv_bOneSheet)
                {
                    var nSheetRow = 1;
                    for (var i=0; i<obj.length; i++){
                        
                        sSheetName = this.ct_sheet+"1";
                        oGrid = obj[i];                
                        this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A"+nSheetRow,"allband", "allrecord", "merge", "allstyle", "image", "font", "both", "cellline");
                        
                        var nHeadRowCnt = 0;
                        for(var j = 0; j < oGrid.getFormatRowCount(); j++)
                        {
                            if(oGrid.getFormatRowProperty(j,"band") == "head")
                            {
                                nHeadRowCnt++;
                            }
                        }
            
                        nSheetRow += parseInt(oGrid.getBindDataset().rowcount) +  nHeadRowCnt + 2;
                    }
                }
                else
                {
                    for (var i=0; i<obj.length; i++){
                        sSheetName = this.ct_sheet+(i+1);
                        oGrid = obj[i];
                        this.exportObj.addExportItem(nexacro.ExportItemTypes.GRID, oGrid,  this.fv_bOneSheet + "!A1","allband", "allrecord", "merge", "allstyle", "image", "font", "both", "cellline");
                    }
                }
            }
            
            var result = this.exportObj.exportData();
        }

        
        /**
        * ExceExport 성공시 callback
        * @return N/A
        */
        this._exportExcelEnd = function(obj,e)
        {
            this.ffobj.setWaitCursor(false,true);
        }

        /**
        * Excel파일 import하는 함수
        * @param  {object} oData
        * @return boolean
        */
        this.gfn_importExcel = function(objForm,dsName,sheetName)
        {
            var importObj = new nexacro.ExcelImportObject("importExcel",objForm);
            var sSvcUrl = application.services["svc"].url+"ams/XExportImport";
            
            this.fv_bOneSheet = this.gfn_isNull(sheetName) ? "Sheet1" : sheetName;
            trace("this.fv_bOneSheet : " + this.fv_bOneSheet);
            
            importObj.set_importtype(nexacro.ImportTypes.EXCEL2007);
            importObj.addEventHandler("onsuccess", this._gfn_importExcel_onsuccess, objForm);
            importObj.addEventHandler("onerror", this._gfn_importExcel_onerror, objForm);
            
            importObj.set_importurl(sSvcUrl);
            importObj.importData("", this.fv_bOneSheet+"!A1:", dsName);
        }

        this._gfn_importExcel_onsuccess = function(obj,e)
        {
            trace(obj.name + ":" + e.eventid);
        }

        this._gfn_importExcel_onerror = function(obj,e)
        {
        //     trace(obj.name + ":" + e.eventid);
        //     trace("\ne.fromobject: " + e.fromobject);
        //     trace("\ne.fromreferenceobject: " + e.fromreferenceobject);
        //     trace("\ne.errorcode: " +  e.errorcode);
        //     trace("\ne.errormsg: " + e.errormsg);
        }

        /**
        * Dataset 수정여부 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @return boolean
        */
        this.gfn_isUpdate = function(oData)
        {
            if(this.gfn_isNull(oData)) { //인자 오류
                return false;
            }
            
            if(oData.getDeletedRowCount() > 0) { // 삭제 체크
                return true;
            }
            
            if (oData.findRowExpr("(this.getRowType(rowidx)==Dataset.ROWTYPE_UPDATE)||(this.getRowType(rowidx)==Dataset.ROWTYPE_INSERT)") >= 0) {
                return true;
            }
            
            return false;
        }

        /**
        * dataSet의 Row가 변경 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @param  {integer} nRow - 수정여부를 체크할 Dataset rowposition
        * @return boolean
        */
        this.gfn_isUpdatedRow = function (oData,nRow)
        {
            if (oData.updatecontrol == true) {
                if (oData.getRowType(nRow) == 2 || oData.getRowType(nRow) == 4) {
                    return true;
                }
                
                return false;
            } else {
                for (var i = 0; i<oData.getColCount(); i++) {
                    if (this.gfn_isUpdateColumn(oData, nRow, i) == true) {
                        return true;
                    }
                }
            }
            
            return false;
        }

        /**
        * dataSet의 Row 에서 해당 칼럼이 변경 체크
        * @param  {object} oData - 수정여부를 체크할 Dataset Object
        * @param  {integer} nRow - 수정여부를 체크할 Dataset rowposition
        * @param  {integer} nColIdx - 수정여부를 체크할 Dataset Column Index
        * @return boolean
        */
        this.gfn_isUpdateColumn = function (oData,nRow,nColIdx)
        {
            if (oData.getRowType(nRow) == 2) {
                if (this.gfn_isNull(oData.getColumn(nRow, nColIdx))) {
                    return false;
                }
            } else {
                if (oData.getColumn(nRow, nColIdx) == oData.getOrgColumn(nRow, nColIdx)) {
                    return false;
                }
            }
            
            return true;
        }

        /**
        * Data의 빈값 확인
        * @param  {object} obj:Grid - Grid Object
        * @param  {object} oData - Dataset Object
        * @param  {array}  ds_col - dstaSet 컬럼 명
        * @return (string) 컴럼명 입력하세요.
        */
        this.gfn_isDataNullCheck = function(obj,oData,ds_col)
        {
            if(this.gfn_isNull(oData)) return false;
            if(this.gfn_isNull(ds_col)) return false;
            
            var bDs_col = ds_col.split("|");
            var sHaedNm = "";
            var nullCnt = 0;
            var colIndex = 0;
            var sKeyVal = "";
            var sColId = "";
            
            for(var i = 0 ; i < oData.rowcount ; i++){
        		if(oData.getRowType(i) != "2" && oData.getRowType(i) != "4" && oData.getRowType(i) != "8") continue;
        		//if(oData.getColumn(i, "STATUS") == "U") continue;
        		
        		for(var j = 0 ; j < bDs_col.length ; j++){
        			var nRow = i;
        			
        			var sIndex = bDs_col[j].indexOf(",");
        			
        			if(sIndex > -1){
        				sKeyVal = bDs_col[j].split(",");
        				sColId = sKeyVal[0];
        			}else{
        				sColId = bDs_col[j];
        			}
        			
        			colIndex = obj.getBindCellIndex("body", sColId);
        			
        			// 좌우공백제거 
        			var coldata = oData.getColumn(i, sColId);
        		    if(!this.gfn_isNull(coldata)){
        			 oData.setColumn(i, sColId,nexacro.trim(""+coldata));
        			}
        						 
        			if(this.gfn_isNull(""+oData.getColumn(i, sColId))){
        				nullCnt++;
        				sHaedNm = oData.getConstColumn(nexacro.replaceAll(obj.getCellProperty("Head", colIndex, "text"), "bind:", ""));
        				
        				this.gfn_alert((nRow+1) + " row " + sHaedNm + " 을(를) 입력하세요.", "", function(msg, flag){
        					oData.set_rowposition(nRow);
        					obj.setCellPos(colIndex);
        					obj.setFocus();
        				});
        				
        				break;
        			}else{
        				if(sIndex > -1){
        //					if(oData.getColumn(i, sKeyVal[0]) == "0") continue;
        					
        					if(this.gfn_isNull(""+oData.getColumn(i, sKeyVal[1])) && !this.gfn_isNull(""+oData.getColumn(i, sKeyVal[0]))){
        						nullCnt++;
        						
        						this.gfn_alert("MSG_80300", "", function(msg, flag){
        							oData.set_rowposition(nRow);
        							obj.setCellPos(colIndex);
        							obj.setFocus();
        						});
        						
        						break;
        					}
        				}
        			}
        		}
        		
        		if(nullCnt > 0) break;
            }
            
            if(nullCnt > 0) return false;
            else return true;
        }

        /**
        * dataSet의 Row 값들을 array로 반환
        * @param  {object} oData - Dataset Object
        * @param  {integer} nRow - Dataset rowposition
        * @return Array
        */
        this.gfn_getRowDataToArray = function (oData,nRow)
        {
            if(this.gfn_isNull(oData)) return;
            if(this.gfn_isNull(nRow)) return;
            var aRtn = new Array();
            var nIdx = 0;
            
            for(var i = 0; i<oData.getColCount(); i++) {
                aRtn[nIdx] = oData.getColumn(nRow, i);
                nIdx++;
            }
            return aRtn;
        }

        /**
        * dataSet의 Row 값들을 String으로 반환
        * @param  {object} oData - Dataset Object
        * @param  {integer} nRow - Dataset rowposition
        * @param  {string} sGubn - Dataset rowposition
        * @return string
        */
        this.gfn_getRowDataToString = function (oData,nRow,sGubn)
        {
            if(this.gfn_isNull(oData)) return;
            if(this.gfn_isNull(nRow)) return;
            if(this.gfn_isNull(sGubn)) sGubn = "|";
            
            var sRtn = "";
            
            for(var i = 0; i<oData.getColCount(); i++) {
                sRtn += oData.getColumn(nRow, i) + sGubn;
            }
            
            sRtn = sRtn.substr(0, sRtn.length-sGubn.length);
            
            return sRtn;
        }

        /**
        * Grid의 조회건수 표시
        * @param  {object} oText - Static Object
        * @param  {object} oData - Dataset Object
        * @return string
        */
        this.gfn_setRowCnt = function(oText,oData,oCnt)
        {
            if(this.gfn_isNull(oText)) return;
            if(this.gfn_isNull(oData)) return;
            
            if(oText.usedecorate == false) {
                oText.set_usedecorate(true);
            }
            
            if(oCnt!=null){
                oText.set_text("Total <fc v='#c8004b'><b v='true'>" + oCnt + "</b></fc> row(s)");
            }else{
                oText.set_text("Total <fc v='#c8004b'><b v='true'>" + oData.getRowCount() + "</b></fc> row(s)");
            }
        }

        /**
         * @class 그리드 추가메뉴 Open(컨텍스트 메뉴)
         * @param obj - Grid object
         * @param e - GridMouseEventInfo
         * @return None
         */
        this.gfn_openGridMenu = function(obj,e,enable_list,isPop)
        {
            var objDs = application.gds_gridMenu;
            this.gv_filterDs = obj.getBindDataset();
            this.gv_isPop = isPop;
            
            if((this.gv_filterDs.rowcount == 0 && e.row != -1) || e.col == -1) return;
            
            this.gv_filterCol = nexacro.replaceAll(obj.getCellProperty("body", e.col, "text"), "bind:", "");
            this.gv_gridHeadText = nexacro.replaceAll(obj.getCellText(-1, e.cell), "bind:", "");
            this.gv_gridHeadBindText = nexacro.replaceAll(obj.getCellProperty("head", e.cell, "text"), "bind:", "");
            this.gv_grid = obj;
            this.gv_e = e;
            this.gv_col = e.col;
            this.gv_row = e.row;
            
            var gv_lang = "";
            var sEnable_list = "";
            application.gds_gridMenu.filter("");
            
            if(this.gfn_isNotNull(enable_list)){
        		sEnable_list = enable_list.split("|");
        		
        		for(var i = 0 ; i < application.gds_gridMenu.rowcount ; i++){
        			gv_lang = this.gfn_getLang(application.gds_gridMenu.getColumn(i, "multiLang"));
        			if(this.gfn_isNotNull(gv_lang)) application.gds_gridMenu.setColumn(i, "menuNm", gv_lang);
        			
        			if(application.gds_gridMenu.getColumn(i, "menuId") != "900" && application.gds_gridMenu.getColumn(i, "menuId") != "910"){
        				if(e.row == -1){
        					application.gds_gridMenu.setColumn(i, "bEnbleColumn", sEnable_list[i]);
        					application.gds_gridMenu.setColumn(i, "bUse", "Y");
        				}else{
        					application.gds_gridMenu.setColumn(i, "bUse", "N");
        				}
        			}else{
        				if(e.row == -1) application.gds_gridMenu.setColumn(i, "bUse", "N");
        				else application.gds_gridMenu.setColumn(i, "bUse", "Y");
        			}
        		}
            }else{
        		for(var i = 0 ; i < application.gds_gridMenu.rowcount ; i++){
        			gv_lang = this.gfn_getLang(application.gds_gridMenu.getColumn(i, "multiLang"));
        			if(this.gfn_isNotNull(gv_lang)) application.gds_gridMenu.setColumn(i, "menuNm", gv_lang);
        			
        			if(application.gds_gridMenu.getColumn(i, "menuId") != "900" && application.gds_gridMenu.getColumn(i, "menuId") != "910"){
        				if(e.row == -1){
        					application.gds_gridMenu.setColumn(i, "bEnbleColumn", 1);
        					application.gds_gridMenu.setColumn(i, "bUse", "Y");
        				}else{
        					application.gds_gridMenu.setColumn(i, "bUse", "N");
        				}
        			}else{
        				if(e.row == -1) application.gds_gridMenu.setColumn(i, "bUse", "N");
        				else application.gds_gridMenu.setColumn(i, "bUse", "Y");
        			}
        		}
            }
            
            application.gds_gridMenu.filter("bUse == 'Y'");
            
            var sPmuGridMenu = "pmu_gridMenu_"+obj.name;
            var oPmuGridMenu = this.components[sPmuGridMenu];
            
            if (this.gfn_isNull(oPmuGridMenu)){
                oPmuGridMenu = new PopupMenu;
                oPmuGridMenu.init(sPmuGridMenu, 0,0,0,0);
                this.addChild(sPmuGridMenu, oPmuGridMenu);
                oPmuGridMenu.set_innerdataset(objDs);
                oPmuGridMenu.set_levelcolumn("menuLvl");
                oPmuGridMenu.set_idcolumn("menuId");
                oPmuGridMenu.set_captioncolumn("menuNm");
                oPmuGridMenu.set_enablecolumn("bEnbleColumn");
                oPmuGridMenu.set_iconcolumn("iconColumn");
                
                oPmuGridMenu.show();
        		
                oPmuGridMenu.addEventHandler("onmenuclick", this._gfn_grdMenu_onmenuclick);
            }
            
            var sPdvGridMenu = "pdv_gridMenu_"+obj.name;
            var oPdvGridMenu = this.components[sPdvGridMenu];    
            
            if (this.gfn_isNull(oPdvGridMenu)){
                oPdvGridMenu = new PopupDiv;
                oPdvGridMenu.init(sPdvGridMenu,0,0,0,0);
                this.addChild(sPdvGridMenu, oPdvGridMenu);
                oPdvGridMenu.addEventHandler("oncloseup", this._pdv_FilterList_oncloseup, this);
                oPdvGridMenu.show();
            } 
            
            //userproperty 설정
            oPmuGridMenu.form = this;
            oPmuGridMenu.grid = obj;
            oPmuGridMenu.cell = e.cell;
            oPmuGridMenu.col = e.col;
            oPmuGridMenu.popupdiv = oPdvGridMenu;
            oPmuGridMenu.excelgrid = obj;
            
            //PopupMenu 오픈
            var x = 0;
            var y = 0;
            
            //마우스 우측 버튼 팝업 위치
        	if (nexacro.Browser == "IE" )
        	{
        		//x = e.screenX - system.clientToScreenX(application.mainframe, 0);
        		//y = e.screenY - system.clientToScreenY(application.mainframe, 0);
        		oPmuGridMenu.trackPopupByComponent(obj, e.clientX, e.clientY, null, null, null, false);
        	}else
        	{
        		x = e.screenX - system.clientToScreenX(application.mainframe, 0);
        		y = e.screenY - system.clientToScreenY(application.mainframe, 0);
        		oPmuGridMenu.trackPopup(x, y);
        	}
        	    
            this.gv_gridMenu = oPdvGridMenu;
        }

        /**
         * @class 그리드 메뉴에서 메뉴를 눌렀을 때 발생하는 이벤트
         * @param obj - PopupMenu
         * @param e - MenuClickEventInfo
         * @return None
         */  
        this._gfn_grdMenu_onmenuclick = function(obj,e)
        {    
            switch (e.id){
                case "100":        // 오름차순
                    var grdObj = this.parent.gv_grid;
                    var nCell = this.parent.gv_col;
                    var sColId = this.parent.gv_filterCol;
                    var oData = this.parent.gv_filterDs;
                    var sColNm = "";
                    var sHeadText = "";
                    var sBindNm = "";
                    
                    if(sColId == "NO" || sColId == "CHK") return;
                    if(grdObj.getCellProperty("head", this.parent.gv_e.cell, "colspan" ) > 1 ) return;
                    
                    if(this.parent.gfn_isNull(this.parent.gv_isPop)){
        				for (var i = 0; i < grdObj.getCellCount("head"); i++) {
        					sBindNm = grdObj.getCellProperty("Head", i, "text");
        					if(this.parent.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm, "bind:", "");
        					
        					sHeadText = oData.getConstColumn(sColNm);
        					if(this.parent.gfn_isNull(sHeadText)) continue;
        					
        					if (i == this.parent.gv_e.cell) {
        						if(sHeadText.substr(sHeadText.length-1) == this.parent.fv_constAscMark || sHeadText.substr(sHeadText.length-1) == this.parent.fv_constDescMark)
        							oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length-1) + this.parent.fv_constDescMark);
        						else oData.setConstColumn(sColNm, sHeadText + this.parent.fv_constDescMark);
        					} else {
        						if(sHeadText.substr(sHeadText.length-1) == this.parent.fv_constAscMark || sHeadText.substr(sHeadText.length-1) == this.parent.fv_constDescMark)
        							oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length-1));
        					}
        					
        					if(i == nCell) {
        						oData.set_keystring("S:+" + sColId);
        					}
        				}
                    }else{
        				this.parent.gfn_gridSort(grdObj, this.parent.gv_e);
        			}
        			
        			oData.set_rowposition(0);
                    
                    break;
                case "200":        // 내림차순
                    var grdObj = this.parent.gv_grid;
                    var nCell = this.parent.gv_col;
                    var sColId = this.parent.gv_filterCol;
                    var oData = this.parent.gv_filterDs;
                    var sColNm = "";
                    var sHeadText = "";
                    var sBindNm = "";
                    
                    if(sColId == "NO" || sColId == "CHK") return;
                    if(grdObj.getCellProperty("head", this.parent.gv_e.cell, "colspan" ) > 1 ) return;
                    
                    if(this.parent.gfn_isNull(this.parent.gv_isPop)){
        				for (var i = 0; i < grdObj.getCellCount("head"); i++) {
        					sBindNm = grdObj.getCellProperty("Head", i, "text");
        					if(this.parent.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm, "bind:", "");
        					
        					sHeadText = oData.getConstColumn(sColNm);
        					if(this.parent.gfn_isNull(sHeadText)) continue;
        					
        					if (i == this.parent.gv_e.cell) {
        						if(sHeadText.substr(sHeadText.length-1) == this.parent.fv_constAscMark || sHeadText.substr(sHeadText.length-1) == this.parent.fv_constDescMark)
        							oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length-1) + this.parent.fv_constAscMark);
        						else oData.setConstColumn(sColNm, sHeadText + this.parent.fv_constAscMark);
        					} else {
        						if(sHeadText.substr(sHeadText.length-1) == this.parent.fv_constAscMark || sHeadText.substr(sHeadText.length-1) == this.parent.fv_constDescMark)
        							oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length-1));
        					}
        					
        					if(i == nCell) {
        						oData.set_keystring("S:-" + sColId);
        					}
        				}
                    }else{
        				this.parent.gfn_gridSort(grdObj, this.parent.gv_e);
        			}
        			
        			oData.set_rowposition(0);
                    
                    break;
                case "300":        // 필터
        			var grdObj = this.parent.gv_grid;
        			var sColNm = "";
        			var sBindNm = grdObj.getCellProperty("Head", this.parent.gv_e.cell, "text");
        			if(this.parent.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm, "bind:", "");
        			
        			if(this.parent.gfn_isNull(sColNm) || sColNm == "NO" || sColNm == "CHK" || this.parent.gfn_isNull(sBindNm)) return;
        			
                    var oArg = { argFilterGrd:grdObj
                               , argFilterDs:this.parent.gv_filterDs
                               , argColNm:this.parent.gv_filterCol
                               , argColHeadNm:sColNm
                               , argGrdInit:this.parent.ds_grdInit
                               };
                    this.parent.gfn_popup("FilterPop", "comm::FilterPop.xfdl", oArg, 300, 500, "");
                    
                    break;
                case "400":        // 필터제거
        			var oData = this.parent.gv_filterDs;
        			var grdObj = this.parent.gv_grid;
        			var sColNm = "";
        			var sHeadText = "";
        			var sBindNm = "";
        			
                    oData.filter("");
        			
        			for (var i = 0; i < grdObj.getCellCount("head"); i++) {
        				sBindNm = grdObj.getCellProperty("Head", i, "text");
        				if(this.parent.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm, "bind:", "");
        				
        				sHeadText = oData.getConstColumn(sColNm);
        				if(this.parent.gfn_isNull(sHeadText)) continue;
        				if(sHeadText.substr(0, 1) == this.parent.fv_filterMark) oData.setConstColumn(sColNm, sHeadText.substr(1));
        			}
        			
        			this.parent.ds_grdInit.setColumn(0, grdObj.name + "FilterId", "");
                    
                    break;
                case "500":        // 컬럼고정
        			this.parent.gv_grid.set_enableredraw(false);
        			var nCol = 0;
        			var nIndex = this.parent.ds_grdInit.getColumn(0, this.parent.gv_grid.name + "Index").split("|");
        			
        			var hBackground             = this.parent.gv_grid.getCellProperty("head", this.parent.gv_col, "background");
        			if(hBackground == "#6d6f86") return;
        			
        			var sumdisplaytype          = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "displaytype");
        			var summask                 = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "mask");
        			var sumtext                 = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "text");
        			var displaytype             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "displaytype");
        			var edittype                = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "edittype");
        			var tooltiptext             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "tooltiptext");
        			var mask                    = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "mask");
        			var calendardisplay         = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "calendardisplay");
        			var calendardisplaynulltype = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "calendardisplaynulltype");
        			var editdisplay             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editdisplay");
        			var editfilter              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editfilter");
        			var editlimit               = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editlimit");
        			var editautoselect          = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editautoselect");
        			var align                   = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "align");
        			var background              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "background");
        			var background2             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "background2");
        			var selectbackground        = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "selectbackground");
        			var combocodecol            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combocodecol");
        			var combodatacol            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combodatacol");
        			var combodataset            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combodataset");
        			var treelevel               = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "treelevel");
        			var treestartlevel          = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "treestartlevel");
        			var expandshow              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "expandshow");
        			var expandsize              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "expandsize");
        			var size                    = this.parent.gv_grid.getRealColSize(this.parent.gv_col);
        			
        			for(var i = 0 ; i < this.parent.gv_grid.getCellCount("head") ; i++){
        				if(this.parent.gv_grid.getFormatColProperty(i,"band") != "left"){
        					nCol = i;
        					break;
        				}
        			}
        			
        			this.parent.gv_grid.deleteContentsCol(this.parent.gv_col);
        			if(nCol == nIndex.length-1) nCol = this.parent.gv_grid.appendContentsCol();
        			else  this.parent.gv_grid.insertContentsCol(nCol);
        			
        			this.parent.gv_grid.setCellProperty("Summ", nCol, "displaytype"             , sumdisplaytype);
        			this.parent.gv_grid.setCellProperty("Summ", nCol, "mask"                    , summask);
        			this.parent.gv_grid.setCellProperty("Summ", nCol, "text"                    , sumtext);
        			this.parent.gv_grid.setCellProperty("head", nCol, "text"                    , "bind:" + this.parent.gv_gridHeadBindText);
        			this.parent.gv_grid.setCellProperty("body", nCol, "text"                    , "bind:" + this.parent.gv_filterCol);
        			this.parent.gv_grid.setCellProperty("body", nCol, "displaytype"             , displaytype);
        			this.parent.gv_grid.setCellProperty("body", nCol, "edittype"                , edittype);
        			this.parent.gv_grid.setCellProperty("body", nCol, "tooltiptext"             , tooltiptext);
        			this.parent.gv_grid.setCellProperty("body", nCol, "mask"                    , mask);
        			this.parent.gv_grid.setCellProperty("body", nCol, "calendardisplay"         , calendardisplay);
        			this.parent.gv_grid.setCellProperty("body", nCol, "calendardisplaynulltype" , calendardisplaynulltype);
        			this.parent.gv_grid.setCellProperty("body", nCol, "editdisplay"             , editdisplay);
        			this.parent.gv_grid.setCellProperty("body", nCol, "editfilter"              , editfilter);
        			this.parent.gv_grid.setCellProperty("body", nCol, "editlimit"               , editlimit);
        			this.parent.gv_grid.setCellProperty("body", nCol, "editautoselect"          , editautoselect);
        			this.parent.gv_grid.setCellProperty("body", nCol, "align"                   , align);
        			this.parent.gv_grid.setCellProperty("body", nCol, "background"              , ""+background);
        			this.parent.gv_grid.setCellProperty("body", nCol, "background2"             , ""+background2);
        			this.parent.gv_grid.setCellProperty("body", nCol, "selectbackground"        , ""+selectbackground);
        			this.parent.gv_grid.setCellProperty("body", nCol, "combocodecol"            , combocodecol);
        			this.parent.gv_grid.setCellProperty("body", nCol, "combodatacol"            , combodatacol);
        			this.parent.gv_grid.setCellProperty("body", nCol, "combodataset"            , combodataset);
        			this.parent.gv_grid.setCellProperty("body", nCol, "treelevel"               , treelevel);
        			this.parent.gv_grid.setCellProperty("body", nCol, "treestartlevel"          , treestartlevel);
        			this.parent.gv_grid.setCellProperty("body", nCol, "expandshow"              , expandshow);
        			this.parent.gv_grid.setCellProperty("body", nCol, "expandsize"              , expandsize);
        			this.parent.gv_grid.setFormatColProperty(nCol, "band", "left");
        			this.parent.gv_grid.setFormatColProperty(nCol, "size", size);
        			this.parent.gv_grid.setCellProperty("head", nCol, "background", "#6d6f86");
        			this.parent.gv_grid.set_enableredraw(true);
        			
                    break;
                case "600":        // 컬럼고정해제
        			if(this.parent.gv_gridHeadText != "CHK" && this.parent.gv_gridHeadText != "NO" && !this.parent.gfn_isNull(this.parent.gv_gridHeadText)){
        				this.parent.gv_grid.set_enableredraw(false);
        				var nCol = 0;
        				var nLeftCol = 0;
        				var nIndex = this.parent.ds_grdInit.getColumn(0, this.parent.gv_grid.name + "Index").split("|");
        				
        				var hBackground             = this.parent.gv_grid.getCellProperty("head", this.parent.gv_col, "background");
        				if(hBackground != "#6d6f86") return;
        				
        				var sumdisplaytype          = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "displaytype");
        				var summask                 = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "mask");
        				var sumtext                 = this.parent.gv_grid.getCellProperty("Summ", this.parent.gv_col, "text");
        				var displaytype             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "displaytype");
        				var edittype                = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "edittype");
        				var tooltiptext             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "tooltiptext");
        				var mask                    = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "mask");
        				var calendardisplay         = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "calendardisplay");
        				var calendardisplaynulltype = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "calendardisplaynulltype");
        				var editdisplay             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editdisplay");
        				var editfilter              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editfilter");
        				var editlimit               = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editlimit");
        				var editautoselect          = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "editautoselect");
        				var align                   = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "align");
        				var background              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "background");
        				var background2             = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "background2");
        				var selectbackground        = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "selectbackground");
        				var combocodecol            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combocodecol");
        				var combodatacol            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combodatacol");
        				var combodataset            = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "combodataset");
        				var treelevel               = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "treelevel");
        				var treestartlevel          = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "treestartlevel");
        				var expandshow              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "expandshow");
        				var expandsize              = this.parent.gv_grid.getCellProperty("body", this.parent.gv_col, "expandsize");
        				var size                    = this.parent.gv_grid.getRealColSize(this.parent.gv_col);
        				
        				var headRightText = "";
        				var headText = "";
        				var addCnt = 0;
        				
        				for(var i = 0 ; i < nIndex.length ; i++){
        					if(nIndex[i] == this.parent.gv_gridHeadText || nIndex[i]+this.parent.fv_constAscMark == this.parent.gv_gridHeadText || nIndex[i]+this.parent.fv_constDescMark == this.parent.gv_gridHeadText){
        						nCol = i;
        					}else{
        						if(nCol != 0 && nCol < i) headRightText += "|" + nIndex[i];
        					}
        				}
        				
        				if(headRightText != "") headRightText = headRightText.substr(1).split("|");
        				
        				for(var i = 0 ; i < this.parent.gv_grid.getCellCount("head") ; i++){
        					if(this.parent.gv_grid.getFormatColProperty(i,"band") != "left"){
        						nLeftCol = i-1;
        						break;
        					}else{
        						headText = this.parent.gv_grid.getCellText(-1, i);
        						
        						for(var j = 0 ; j < headRightText.length ; j++){
        							if(headRightText[j] == headText || headRightText[j] == headText.substr(0, headText.length-1)) addCnt++;
        						}
        					}
        				}
        				
        				if(addCnt != 0) nCol += addCnt;
        				if(nCol < nLeftCol) nCol = nLeftCol;
        				
        				this.parent.gv_grid.deleteContentsCol(this.parent.gv_col);
        				if(nCol >= nIndex.length-1) nCol = this.parent.gv_grid.appendContentsCol();
        				else  this.parent.gv_grid.insertContentsCol(nCol);
        				
        				this.parent.gv_grid.setCellProperty("Summ", nCol, "displaytype"             , sumdisplaytype);
        				this.parent.gv_grid.setCellProperty("Summ", nCol, "mask"                    , summask);
        				this.parent.gv_grid.setCellProperty("Summ", nCol, "text"                    , sumtext);
        				this.parent.gv_grid.setCellProperty("head", nCol, "text"                    , "bind:" + this.parent.gv_gridHeadBindText);
        				this.parent.gv_grid.setCellProperty("body", nCol, "text"                    , "bind:" + this.parent.gv_filterCol);
        				this.parent.gv_grid.setCellProperty("body", nCol, "displaytype"             , displaytype);
        				this.parent.gv_grid.setCellProperty("body", nCol, "edittype"                , edittype);
        				this.parent.gv_grid.setCellProperty("body", nCol, "tooltiptext"             , tooltiptext);
        				this.parent.gv_grid.setCellProperty("body", nCol, "mask"                    , mask);
        				this.parent.gv_grid.setCellProperty("body", nCol, "calendardisplay"         , calendardisplay);
        				this.parent.gv_grid.setCellProperty("body", nCol, "calendardisplaynulltype" , calendardisplaynulltype);
        				this.parent.gv_grid.setCellProperty("body", nCol, "editdisplay"             , editdisplay);
        				this.parent.gv_grid.setCellProperty("body", nCol, "editfilter"              , editfilter);
        				this.parent.gv_grid.setCellProperty("body", nCol, "editlimit"               , editlimit);
        				this.parent.gv_grid.setCellProperty("body", nCol, "editautoselect"          , editautoselect);
        				this.parent.gv_grid.setCellProperty("body", nCol, "align"                   , align);
        				this.parent.gv_grid.setCellProperty("body", nCol, "background"              , ""+background);
        				this.parent.gv_grid.setCellProperty("body", nCol, "background2"             , ""+background2);
        				this.parent.gv_grid.setCellProperty("body", nCol, "selectbackground"        , ""+selectbackground);
        				this.parent.gv_grid.setCellProperty("body", nCol, "combocodecol"            , combocodecol);
        				this.parent.gv_grid.setCellProperty("body", nCol, "combodatacol"            , combodatacol);
        				this.parent.gv_grid.setCellProperty("body", nCol, "combodataset"            , combodataset);
        				this.parent.gv_grid.setCellProperty("body", nCol, "treelevel"               , treelevel);
        				this.parent.gv_grid.setCellProperty("body", nCol, "treestartlevel"          , treestartlevel);
        				this.parent.gv_grid.setCellProperty("body", nCol, "expandshow"              , expandshow);
        				this.parent.gv_grid.setCellProperty("body", nCol, "expandsize"              , expandsize);
        				this.parent.gv_grid.setFormatColProperty(nCol, "band", "body");
        				this.parent.gv_grid.setFormatColProperty(nCol, "size", size);
        				this.parent.gv_grid.setCellProperty("head", nCol, "background", this.gv_bgColorD);
        				this.parent.gv_grid.set_enableredraw(true);
        			}
        			
                    break;
                case "700":        // 컬럼목록
        			var grdObj = this.parent.gv_grid;
        			var rowSize = 0;
        			
        			for(var i = 0 ; i < grdObj.getCellCount("head") ; i++){
        				if(this.parent.gfn_isNull(grdObj.getCellText(-1, i)) || grdObj.getCellText(-1, i) == "CHK" || grdObj.getCellText(-1, i) == "NO"){
        					continue;
        				}else{
        					rowSize++;
        				}
        			}
        			
        			var nHeight = nexacro.round(this.parent.getOffsetHeight());
        			rowSize = rowSize*23+76;
        			if(rowSize > nHeight) rowSize = nHeight;
        			
                    var oArg = {argFilterGrid:grdObj, argIndex:this.parent.ds_grdInit.getColumn(0, this.parent.gv_grid.name + "Index").split("|"), argSize:this.parent.ds_grdInit.getColumn(0, this.parent.gv_grid.name + "Size").split("|")};
                    this.parent.gfn_popup("HeadFilterPop", "comm::HeadFilterPop.xfdl", oArg, 250, rowSize, "");
                    
                    break;
                case "800":        // 초기화
                    this.parent.gfn_resetGrid(this.parent.gv_grid);
                    
                    break;
                case "900":        // Copy
                    this.parent.gfn_clipboardCopy(this.parent.gv_grid, "", this.parent.gv_filterDs, this.parent.gv_col);
                    
                    break;
                case "910":        // Paste
                    if(this.parent.gfn_isNull(system.getClipboard("CF_TEXT"))) this.parent.gv_filterDs.setColumn(this.parent.gv_row, this.parent.gv_filterCol, application.parent.gv_arrClipboard);
                    else this.parent.gv_filterDs.setColumn(this.parent.gv_row, this.parent.gv_filterCol, system.getClipboard("CF_TEXT"));
                    
                    break;
            }
        }

        this.gfn_resetGrid = function(grdObj)
        {
        	this.gfn_confirm("MSG_ALERT_INITIALIZATION", "", function(msg, flag){
        		if(flag){
        			this.gv_filterDs.filter("");
        			this.gv_filterDs.set_keystring("");
        			this.gfn_clearSortAndMark(grdObj);
        			application.gds_param.setColumn(0, "sortValue", "");
        			
        			this.ds_grdInit.setColumn(0, grdObj.name + "Col", "");
                    this.ds_grdInit.setColumn(0, grdObj.name + "SortId", "");
                    this.ds_grdInit.setColumn(0, grdObj.name + "CellIdx", "");
                    this.ds_grdInit.setColumn(0, grdObj.name + "Mark", "");
                    this.ds_grdInit.setColumn(0, grdObj.name + "FilterId", "");
        			
        			grdObj.set_formats(this.ds_grdInit.getColumn(0, grdObj.name + "Init"));
        			this.gfn_constDsSet(grdObj);
        		}
        	});
        }

        this.gfn_clearFilterMarker = function(obj)
        {
            for(var i = 0; i < obj.getCellCount("head"); i++){
                obj.setCellProperty("head", i, "expandshow", "hide");
            }
        }

        /**
         * @class 팝업Div가 닫힐 때 발생하는 이벤트
         * @param obj - PopupDiv
         * @param e - EventInfo
         * @return None
         */  
        this._pdv_FilterList_oncloseup = function(obj,e)
        {
            trace("_pdv_FilterList_oncloseup = "+oPmuGridMenu.grid.name);
            obj.fn_closeup();
        }

        /**
         * @class 그리드 헤드 셋팅
         * @param obj - Grid
         *        nCell - cell
         * @return None
         */  
        this.gfn_gridHeadSet = function(obj,nCell)
        {
            var sColNm = "";
            var sColBodyText = "bind:";
            
        	for(var i = 0 ; i < obj.getCellCount("head") ; i++){
        		if(obj.getFormatColProperty(i,"band") == "left"){
        			obj.setCellProperty("head", i, "background", this.gv_bgColor);
        			obj.setCellProperty("head", i, "background2", this.gv_bgColor);
        		}
        		
        		sColNm = obj.getCellProperty("head", i, "text");
        		if(this.gfn_isNull(sColNm) || sColNm == "CHK" || sColNm == "PAGING_NUM") continue;
        		if(this.gfn_isNotNull(application.gds_lang.getColumn(0, sColNm.toUpperCase()))) obj.setCellProperty("head", i, "text", application.gds_lang.getColumn(0, sColNm.toUpperCase()));
        		
        		if(this.gfn_isNull(obj.getCellProperty("body", i, "text"))){
        			obj.setCellProperty("body", i, "text", (sColBodyText+sColNm));	
        		}
        	}
        }

        /**
         * @class 그리드 sort 페이징 X
         * @param obj - Grid
         *        nCell - cell
         *        colName
         *        sortValue
         * @return None
         */  
        this.gfn_sortDataset = function(obj,e)
        {
        	var oData = obj.getBindDataset();
            if((oData.rowcount == 0 && e.row != -1) || e.col == -1) return;
            
        	var sColId = nexacro.replaceAll(obj.getCellProperty("body", e.col, "text"), "bind:", "");
        	var sColNm = "";
        	var sHeadText = "";
        	var sBindNm = "";
        	
        	if(this.gfn_isNull(sColId) || sColId == "NO" || sColId == "CHK") return;
        	
        	for (var i = 0; i < obj.getCellCount("head"); i++) {
        		sBindNm = obj.getCellProperty("Head", i, "text");
        		
        		if(this.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm, "bind:", "");
        		else sColNm = "";
        		
        		if(this.gfn_isNull(sColNm) || sColNm == "NO") {
        			continue;
        		}
        		
        		sHeadText = oData.getConstColumn(sColNm);
        		if(this.gfn_isNull(sHeadText)) continue;
        		
        		if (i == e.cell) {
        			if( obj.getCellProperty("head", e.cell, "colspan" ) > 1 ) return;
        			
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark) {
        				oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length - 1) + this.fv_constDescMark);
                    } else if (sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
        				oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length - 1) + this.fv_constAscMark);
                    } else {
        				oData.setConstColumn(sColNm, sHeadText + this.fv_constAscMark);
                    }
        		} else {
        			if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark || sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark)
        				oData.setConstColumn(sColNm, sHeadText.substr(0, sHeadText.length - 1));
        		}
        		
                if (i == e.col) {
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark) {
                        oData.set_keystring("S:-" + sColId);
                    } else if (sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
                        oData.set_keystring("S:+" + sColId);
                    } else {
                        oData.set_keystring("S:+" + sColId);
                    }
                }
        	}
        }

        /**
         * @class 그리드 sort search
         * @param obj - Grid
         *        nCell - cell
         *        colName
         *        sortValue
         * @return None
         */  
        this.gfn_sortSearch = function(obj,nCell,colName,sortValue)
        {
        	var sColNm = "";
        	var sHeadText = "";
        	var grdLang = "";
        	var grdDs = obj.getBindDataset();
        	grdDs.enableevent = false;
        	obj.set_enableredraw(false);
        	for(var i = 0 ; i < obj.getCellCount("head") ; i++){
        		var sBindNm = obj.getCellProperty("Head", i, "text");
        		if(this.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm , "bind:", "");
        		else sColNm = "";
                
                if(this.gfn_isNull(sColNm) || sColNm == "NO") {
                    continue;
                }
                
                grdLang = this.gfn_getLang(sColNm.substr(1));
                sHeadText = grdDs.getConstColumn(sColNm);
                
                if (i == nCell) {
        			if( obj.getCellProperty("head", nCell, "colspan" ) > 1 ) return "N";
        			
                    this.ds_grdInit.setColumn(0, obj.name + "Col", sColNm);
                    this.ds_grdInit.setColumn(0, obj.name + "SortId", colName);
                    this.ds_grdInit.setColumn(0, obj.name + "CellIdx", nCell);
        			application.gds_param.setColumn(0, "colName", colName);
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark) {
                        this.ds_grdInit.setColumn(0, obj.name + "Mark", this.fv_constDescMark);
                        grdDs.set_keystring("S:-" + colName);
        				grdDs.setConstColumn(sColNm, grdLang + this.fv_constDescMark);
                        application.gds_param.setColumn(0, "sortValue", "DESC");
                    } else if (sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
                        this.ds_grdInit.setColumn(0, obj.name + "Mark", this.fv_constAscMark);
                        grdDs.set_keystring("S:+" + colName);
        				grdDs.setConstColumn(sColNm, grdLang + this.fv_constAscMark);
                        application.gds_param.setColumn(0, "sortValue", "ASC");
                    } else {
                        this.ds_grdInit.setColumn(0, obj.name + "Mark", this.fv_constAscMark);
                        grdDs.set_keystring("S:+" + colName);
        				grdDs.setConstColumn(sColNm, grdLang + this.fv_constAscMark);
                        application.gds_param.setColumn(0, "sortValue", "ASC");
                    }
                } else {
        			grdDs.setConstColumn(sColNm, grdLang);
                }
            }
            obj.set_enableredraw(true);
            grdDs.enableevent = true;
        }

        /**
         * @class 그리드 sort search Popup
         * @param obj - Grid
         *        nCell - cell
         *        colName
         *        sortValue
         * @return None
         */  
        this.gfn_sortSearchPopup = function(obj,nCell,colName,sortValue)
        {
        	var sColNm = "";
        	var sHeadText = "";
        	var grdLang = "";
        	var grdDs = obj.getBindDataset();
        	
        	for(var i = 0 ; i < obj.getCellCount("head") ; i++){
        		var sBindNm = obj.getCellProperty("Head", i, "text");
        		if(this.gfn_isNotNull(sBindNm)) sColNm = nexacro.replaceAll(sBindNm , "bind:", "");
        		else sColNm = "";
                
                if(this.gfn_isNull(sColNm) || sColNm == "NO") {
                    continue;
                }
                
                sHeadText = obj.getCellText(-1, i);
                
                if (i == nCell) {
        			application.gds_param.setColumn(0, "colName", colName);
        			
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark) {
        				obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1) + this.fv_constDescMark);
                        grdDs.set_keystring("S:-" + colName);
                        application.gds_param.setColumn(0, "sortValue", "DESC");
                    } else if (sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
        				obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1) + this.fv_constAscMark);
                        grdDs.set_keystring("S:+" + colName);
                        application.gds_param.setColumn(0, "sortValue", "ASC");
                    } else {
        				obj.setCellProperty("head", i, "text", sHeadText + this.fv_constAscMark);
                        grdDs.set_keystring("S:+" + colName);
                        application.gds_param.setColumn(0, "sortValue", "ASC");
                    }
                } else {
                    // 정렬표시 삭제
                    if (sHeadText.substr(sHeadText.length - 1) == this.fv_constAscMark || sHeadText.substr(sHeadText.length - 1) == this.fv_constDescMark) {
                        obj.setCellProperty("head", i, "text", sHeadText.substr(0, sHeadText.length - 1));
                    }
                }
            }
        }

        /**
         * @class 그리드 sort search
         * @param obj - Grid
         *        nCell - cell
         *        colName
         *        sortValue
         * @return None
         */  
        this.gfn_grdSortSet = function(obj)
        {
        	var grdDs = obj.getBindDataset();
        	
        	if(this.ds_grdInit.getColumn(0, obj.name + "Mark") == this.fv_constAscMark){
        		this.gfn_setParam("sortValue", "ASC");
        		grdDs.set_keystring("S:+" + this.ds_grdInit.getColumn(0, obj.name + "SortId"));
        	}else if(this.ds_grdInit.getColumn(0, obj.name + "Mark") == this.fv_constDescMark){
        		this.gfn_setParam("sortValue", "DESC");
        		grdDs.set_keystring("S:-" + this.ds_grdInit.getColumn(0, obj.name + "SortId"));
        	}
        	
        	if(this.gfn_isNotNull(this.ds_grdInit.getColumn(0, obj.name + "SortId"))) this.gfn_setParam("colName", this.ds_grdInit.getColumn(0, obj.name + "SortId"));
        }

        var _pStrDataset = nexacro.Dataset.prototype;

        _pStrDataset._createSortFunc = function(){
        	var _keys = this._keycols;
        	var _keycnt = _keys.length;
        	var _locale = this._getLocale();
        	var pThis = this;
        	
        	return function(a, b){
        		for(var i = _keycnt - 1 ; i >= 0 ; i--){
        			var key = _keys[i];
        			var value1 = a[key.colidx];
        			var value2 = b[key.colidx];
        			var cmp = 0;
        			
        			if((value1 instanceof nexacro.Decimal) == false){
        				if(value1 != null){
        					if(value1 != value2){
        						if(value2 != null){
        							if((value2 instanceof nexacro.Decimal) == false){
        								var _type = pThis.colinfos[key.colidx].type.toLowerCase();
        								cmp = value1 > value2 ? 1 : -1;
        							}else{
        								cmp = ((value1.hi > value2.hi || (value1.hi >= value2.hi && value1.lo > value2.lo)) ? 1 : -1);
        							}
        						}else{
        							cmp = 1;
        						}
        					}else{
        						cmp = 0;
        					}
        				}else if(value1 != value2){
        					cmp = -1;
        				}else{
        					cmp = 0;
        				}
        			}else{
        				if(value2 != null){
        					cmp = (value1.hi == value2.hi && value1.lo == value2.lo) ? 0 : ((value1.hi > value2.hi || (value1.hi >= value2.hi && value1.lo > value2.lo)) ? 1 : -1);
        				}else{
        					cmp = 1;
        				}
        			}
        			
        			if(cmp != 0){
        				return (key.descending) ? -cmp : cmp;
        			}
        		}
        		
        		return (a._rawidx > b._rawidx) ? 1 : -1;
        	};
        };

        /**
         * @class 그리드 바인드명
         * @param obj - Grid
         *        nCol - col
         * @return None
         */  
        this.gfn_gridBindName = function(obj,nCol)
        {
        	var sBindNm = obj.getCellProperty("body", nCol, "text");
        	var colName = "";
        	
        	if(this.gfn_isNotNull(sBindNm)) var colName = nexacro.replaceAll(sBindNm, "bind:", "");
        	
        	if(colName == "NO" || this.gfn_isNull(colName)) colName = "PAGING_NUM";
            return colName;
        }

        // 그리드 초기화 시작

        this.gv_gridMenuSet; // 그리드 팝업 메뉴 셋

        /**
         * @class gfn_gridInit
         * @param gridList   - 그리드 배열
                  grdMenuSet - 그리드 메뉴 셋
                  divPaging  - 그리드 페이지 div
                  searchFunc - 그리드 페이지 검색 옵션
                  keyField   - 그리드 변경 키값
         * @return None
         */
        this.gfn_gridInit = function(gridList,grdMenuSet,divPaging,searchFunc,keyField)
        {
        	var decimalPointValue = ".";
        	var decimalPoint = application.gds_admSystemConfig.getColumn(application.gds_admSystemConfig.findRow("key", "NUMBEROFDIGITBELOWDECIMALPOINT"), "value1");
        	if(this.gfn_isNull(decimalPoint)) decimalPoint = application.gds_admSystemConfig.getColumn(application.gds_admSystemConfig.findRow("key", "NUMBEROFDIGITBELOWDECIMALPOINT"), "defaultvalue");
        	if(this.gfn_isNull(decimalPoint)) decimalPoint = 0;
        	
        	if(decimalPoint > 0){
        		for(var i = 0 ; i < decimalPoint ; i++){
        			decimalPointValue += "0";
        		}
        	}else{
        		decimalPointValue = "";
        	}
        	
        	var nRow = 0;
        	var uskey = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.parent.gv_menuId), "uskey");
        	var grdObj;
        	var grdPaging;
        	var tempSize = "";
        	var tempIndex = "";
        	var grdLang = "";
        	var grdDs;
        	var sType = "";
        	var dispType = "";
        	var editType = "";
        	var expandShow = "";
        	var sBind = "";
        	var sKeyField;
        	this.gv_gridMenuSet = grdMenuSet;
        	var arrList;
        	
        	if(this.gfn_isNull(this.ds_grdInit)){
        		this.ds_grdInit = new Dataset;
        	}
        	
        	for(var i = 0 ; i < gridList.length ; i++){
        		grdObj = gridList[i];
        		grdObj.set_enableredraw(false);
        		grdDs = grdObj.getBindDataset();

        		if(this.gfn_isNotNull(keyField)){
        			if(this.gfn_isNotNull(keyField[i])) sKeyField = keyField[i].split("|");
        			else sKeyField = "";
        		}
        		
        		grdObj.addEventHandler("onkeydown", this._gfn_onkeydown, this);
        		grdObj.addEventHandler("onkeyup", this._gfn_onkeyup, this);
        		grdObj.addEventHandler("onrbuttonup", this._gfn_onrbuttonup, this);
        		grdObj.set_selectchangetype("up");
        		
        		if(this.gfn_isNotNull(divPaging[i])){
        			grdPaging = divPaging[i];
        			grdPaging.gv_menuId = this.parent.gv_menuId; // 페이징 form 셋팅
        			grdPaging.gv_flag = searchFunc[i]; // 페이징 검색 함수 셋팅
        		}
        		
        		var temp = "";
        		
        		grdObj.style.set_padding(0, 2, 0, 2);
        		for(var j = 0 ; j < grdObj.getCellCount("head") ; j++){
        			sType = grdObj.getCellProperty("head", j, "displaytype");
        			dispType = grdObj.getCellProperty("body", j, "displaytype");
        			editType = this.gfn_isNullEmpty(grdObj.getCellProperty("body", j, "edittype"));
        			expandShow = grdObj.getCellProperty("body", j, "expandshow");
        			
        			//만약 포맷 지정되어 있으면 포맷 지정으로 간다. 20181025
        			
        			if(editType == "masknumber" || dispType == "number"){
        				grdObj.setCellProperty("body", j, "editautoselect", true);
        				if(this.gfn_isNull(grdObj.getCellProperty("body", j, "mask")))
        				{
        					if(grdObj.getCellText(-1, j) != "NO"){
        						grdObj.setCellProperty("body", j, "mask", "#,##0" + decimalPointValue);
        						grdObj.setCellProperty("summary", j, "mask", "#,##0" + decimalPointValue);
        					}else{
        						grdObj.setCellProperty("body", j, "mask", "#,##0");
        					}
        				}
        			}
        			
        			if(editType == "masknumber" || dispType == "number"){
        				grdObj.setCellProperty("body", j, "align", "right");
        			}else if(dispType == "date"){
        				grdObj.setCellProperty("body", j, "align", "center");
        			}else{//만약 포맷 지정되어 있으면 포맷 지정으로 간다. 20181025
        				if(this.gfn_isNull(grdObj.getCellProperty("body", j, "align")))
        				{
        					grdObj.setCellProperty("body", j, "align", "left middle");
        				}
        			}
        			
        			if(sType == "checkbox"){
        				grdDs.addConstColumn("HCHK", "0");
        				grdObj.setCellProperty("Head", j, "text", "bind:HCHK");
        			}
        			
        			if((editType != "none" || expandShow == "show") && editType != "checkbox" && editType != "tree" && editType.indexOf("expr") == -1){
        				grdObj.setCellProperty("body", j, "background", this.gv_bgBodyColor);
        				grdObj.setCellProperty("body", j, "background2", this.gv_bg1BodyColor);
        				grdObj.setCellProperty("body", j, "selectbackground", this.gv_bgSelBodyColor);
        			}
        			
        			if(grdObj.getFormatColProperty(j,"band") == "left"){
        				grdObj.setCellProperty("head", j, "background", this.gv_bgColor);
        			}else{
        				// 그리그 입력 필수값인경우 backgroud color setting : 2018.01.08 - ksh
        				var text = grdObj.getCellProperty("head", j, "text");
        				var bgColor = grdObj.getCellProperty("head", j, "background");
        				
        				if(bgColor != null){
        					grdObj.setCellProperty("head", j, "background", bgColor.color);
        				}else{
        					if(grdObj.mandantoryfield != null){
        						arrList = grdObj.mandantoryfield.split("|");
        						for(var k=0; k<arrList.length; k++){
        							var mandantoryField = arrList[k];
        							if(text == mandantoryField){
        								grdObj.setCellProperty("head", j, "background", this.gv_bgMtColor);
        								break;
        							}else{
        								grdObj.setCellProperty("head", j, "background", this.gv_bgColorD);
        							}
        						}							
        					}else{
        						grdObj.setCellProperty("head", j, "background", this.gv_bgColorD);
        					}				
        						
        				}
        			}
        			
        			grdLang = grdObj.getCellProperty("Head", j, "text");
        			var colTitle = "";
        			if(sType != "checkbox" && grdLang != ""){
        			    
        			    var rep_grdLang = this.gfn_getLang(grdLang);
        			    
        			    if(!this.gfn_isNull(rep_grdLang)){
        			      rep_grdLang = rep_grdLang.replace("<BR>","\n");
        			      rep_grdLang = rep_grdLang.replace("<br>","\n");
        			      colTitle = rep_grdLang;
        			    }
        			    
        				grdDs.addConstColumn("H" + grdLang, rep_grdLang);
        				grdObj.setCellProperty("Head", j, "text", "bind:H" + grdLang);
        			}
        			
        			if(colTitle.indexOf("일수")> -1 || colTitle.indexOf("순서")> -1 || colTitle.indexOf("금액")> -1) {
        				grdObj.setCellProperty("body", j, "mask", "" );
        				grdObj.setCellProperty("summary", j, "mask", "");				
        				grdObj.setCellProperty("body", j, "align", "right");		
        				grdObj.setCellProperty("summary", j, "align", "right");
        			}	
        			
        			//라니안 추가
        			temp = grdObj.getCellProperty("body", j, "text");
        			if(temp == null || temp === undefined || temp == ""){
        				grdObj.setCellProperty("body", j, "text", "bind:" + grdLang);
        			}
        			
        			var sBindNm = grdObj.getCellProperty("body", j, "text");
        			
        			if(this.gfn_isNotNull(sBindNm)) sBind = nexacro.replaceAll(sBindNm , "bind:", "");
        			else sBind = "";
        			
        			if(this.gfn_isNotNull(sKeyField)){
        				for(var z = 0 ; z < sKeyField.length ; z++){
        					if(sKeyField[z] == sBind){
        						if(dispType == "combo"){
        							grdObj.setCellProperty("body", j, "edittype", "expr:dataset.getRowType(currow) == '2' ? 'combo' : 'none'");
        						}else if(dispType == "date"){
        							grdObj.setCellProperty("body", j, "edittype", "expr:dataset.getRowType(currow) == '2' ? 'date' : 'none'");
        						}else{
        							grdObj.setCellProperty("body", j, "edittype", "expr:dataset.getRowType(currow) == '2' ? 'text' : 'none'");
        							if(grdObj.getCellProperty("body", j, "expandshow") == "show") grdObj.setCellProperty("body", j, "expandshow", "expr:dataset.getRowType(currow) == '2' ? 'show' : 'hide'");
        						}
        						
        						grdObj.setCellProperty("body", j, "background", "expr:dataset.getRowType(currow) == '2' ? '" + this.gv_bgBodyColor + "' : ''");
        						grdObj.setCellProperty("body", j, "background2", "expr:dataset.getRowType(currow) == '2' ? '" + this.gv_bg1BodyColor + "' : ''");
        						grdObj.setCellProperty("body", j, "selectbackground", "expr:dataset.getRowType(currow) == '2' ? '" + this.gv_bgSelBodyColor + "' : ''");
        					}
        				}
        			}
        			
        			if(j == 0){
        				tempSize = grdObj.getRealColSize(j);
        				tempIndex = grdObj.getCellText(-1, j);
        			}else{
        				if(sBind != "") tempSize += "|" + grdObj.getRealColSize(j);
        				tempIndex += "|" + grdObj.getCellText(-1, j);
        			}
        		}
        		
        		grdObj.set_enableredraw(true);
        		
        		// 그리드 초기값 셋팅
        		if(this.gfn_isNotNull(this.ds_grdInit)){
        			this.ds_grdInit.addColumn(grdObj.name+"Init");
        			this.ds_grdInit.addColumn(grdObj.name+"Size");
        			this.ds_grdInit.addColumn(grdObj.name+"Index");
        			this.ds_grdInit.addColumn(grdObj.name+"Col");
        			this.ds_grdInit.addColumn(grdObj.name+"Mark");
        			this.ds_grdInit.addColumn(grdObj.name+"SortId");
        			this.ds_grdInit.addColumn(grdObj.name+"CellIdx");
        			this.ds_grdInit.addColumn(grdObj.name+"FilterId");
        			this.ds_grdInit.addColumn(grdObj.name+"Sch");
        			this.ds_grdInit.addColumn(grdObj.name+"Scv");
        			if(this.ds_grdInit.rowcount == 0) this.ds_grdInit.addRow();
        			
        			this.ds_grdInit.setColumn(0, grdObj.name + "Init", grdObj.getCurFormatString(false));
        //			trace(grdObj.getCurFormatString(false));
        			
        			nRow = application.gds_userGrid.findRowExpr("urKey == '"+this.gfn_getUserInfo("urKey")+"' && usKey == '"+uskey+"' && usgridId == '"+grdObj.name+"'");			
        			if(nRow != -1){
        				if(application.gds_userGrid.getColumn(nRow, "urdfusInfoInit") == grdObj.getCurFormatString(false)){
        					grdObj.set_formats(application.gds_userGrid.getColumn(nRow, "urdfusInfo"));
        				}
        			}
        			
        			this.ds_grdInit.setColumn(0, grdObj.name + "Size", tempSize);
        			this.ds_grdInit.setColumn(0, grdObj.name + "Index", tempIndex);
        			this.ds_grdInit.setColumn(0, grdObj.name + "Sch", 0);
        			this.ds_grdInit.setColumn(0, grdObj.name + "Scv", 0);
        		}
        	}
        }

        /* _gfn_onkeydown 실행 */
        this._gfn_onkeydown = function(obj,e)
        {
        	if(e.ctrlKey && e.keycode == 67){
        		this.gfn_GridCopy(obj, e, obj.getBindDataset(), obj.currentcell);
        	}
        }

        /* _gfn_onkeyup 실행 */
        this._gfn_onkeyup = function(obj,e)
        {
        	if(e.keycode == "13"){
        		var grdDs = obj.getBindDataset();
        		var grdPos = obj.getCellPos();
        		
        		if(grdDs.rowcount > grdDs.rowposition+1) grdDs.set_rowposition(grdDs.rowposition+1);
        		else grdDs.set_rowposition(0);
        		obj.setCellPos(grdPos);
        		obj.setFocus();
        	}
        }

        /* _gfn_onrbuttonup 실행 */
        this._gfn_onrbuttonup = function(obj,e)
        {
        	var enable_list = "";
        	var objName = "";
        	
        	for(var i = 0 ; i < this.gv_gridMenuSet.length ; i++){
        		objName = this.gv_gridMenuSet[i].substr(0, this.gv_gridMenuSet[i].indexOf("^"));
        		
        		if(objName == obj.name){
        			enable_list = this.gfn_isNullEmpty(this.gv_gridMenuSet[i].substr(this.gv_gridMenuSet[i].indexOf("^")+1));
        		}
        	}
        	
        	this.gfn_openGridMenu(obj, e, enable_list);
        }

        /* gfn_constDsSet 실행 */
        this.gfn_constDsSet = function(grdObj)
        {
        	var sColNm = "";
        	var grdLang = "";
        	var sMark = "";
        	var sFilterId = "";
        	var sCol = "";
        	var sCellIdx = "";
        	var grdDs = grdObj.getBindDataset();
        	
        	grdObj.set_enableredraw(false);
        	
         	for(var j = 0 ; j < grdObj.getCellCount("head") ; j++){
        		sColNm = grdObj.getCellProperty("Head", j, "text");
        		if(this.gfn_isNull(sColNm) || sColNm == "NO") continue;
        		
        		grdLang = nexacro.replaceAll(grdObj.getCellProperty("Head", j, "text") , "bind:H", "");
        		
        		sMark = this.ds_grdInit.getColumn(0, grdObj.name + "Mark");
        		sFilterId = this.ds_grdInit.getColumn(0, grdObj.name + "FilterId");
        		sCol = this.ds_grdInit.getColumn(0, grdObj.name + "Col");
        		sCellIdx = this.ds_grdInit.getColumn(0, grdObj.name + "CellIdx");
        		
         		var rep_grdLang = this.gfn_getLang(grdLang);
        			    
        		if(!this.gfn_isNull(rep_grdLang)){
        		   rep_grdLang = rep_grdLang.replace("<BR>","\n");
        		   rep_grdLang = rep_grdLang.replace("<br>","\n");
        		}
        		
         		if(sCol == "H" + grdLang && sCellIdx == j){
        			if(grdLang == "CHK"){
        				grdDs.addConstColumn("H" + grdLang, "0");
        			}else{
        				if(sFilterId == "H" + grdLang) grdDs.addConstColumn("H" + grdLang, this.fv_filterMark + rep_grdLang + sMark);
        				else grdDs.addConstColumn("H" + grdLang, rep_grdLang + sMark);
        			}
         		}else{
        			if(grdLang == "CHK"){
        				grdDs.addConstColumn("H" + grdLang, "0");
        			}else{
        				if(sFilterId == "H" + grdLang) grdDs.addConstColumn("H" + grdLang, this.fv_filterMark + rep_grdLang);
        				else grdDs.addConstColumn("H" + grdLang, rep_grdLang);
        			}
         		}
         		
         		grdObj.setCellProperty("Head", j, "text", "bind:H" + grdLang);
         	}
        	
        	for(var i = 0 ; i < grdDs.rowcount ; i++){
        		grdDs.setColumn(i, "CHK", 0);
        	}
        	grdDs.applyChange();
        	
        	grdObj.set_enableredraw(true);
        	
        	this.gfn_setScroll(grdObj); // 스크롤 위치 이동
        }

        this.pageSet = function(obj,ds,initSortValue){
        	if(initSortValue){
        		this.gfn_setParam("sortValue", "");
        		this.gfn_setParam("colName", "");
        	}
        	obj.fn_pageSet(ds.getColumn(0, "pagingLimit"), ds.getColumn(0, "currentPage"), ds.getColumn(0, "COUNT"));
        }

        //라니안 유비리포트
        //printTypeValue : PREVIEW / DIRECT
        this.openUbiReport = function(gridObject,datasetObjectList,appModuleValue,fileUrlValue,jrfFileValue,ubiWidth,ubiHeight,printTypeValue){

        	if(this.gfn_isNull(ubiWidth)){
        		ubiWidth = 950;
        	}

        	if(this.gfn_isNull(ubiHeight)){
        		ubiHeight = 700;
        	}
        	
        	var nLeft = nexacro.toNumber(application.mainframe.width) /2 -(ubiWidth/2);
        	var nTop = 50;

        	var newChild = new nexacro.ChildFrame;
        	newChild.init(("UbiReportPop"+jrfFileValue), "absolute", nLeft, nTop, ubiWidth, ubiHeight, null, null, "comm::UbiReportPop.xfdl");
        	newChild.set_dragmovetype("all");
        	newChild.set_showtitlebar(false);
        	newChild.set_autosize(false);
        	newChild.set_resizable(false);
        	newChild.set_showstatusbar(false);

        	var ubiArgs = {
        		appModule:appModuleValue,
        		fileUrl:fileUrlValue,
        		jrfFile:jrfFileValue,
        		dsObjectList:datasetObjectList,
        		printType:printTypeValue
        	};
        	newChild.showModal(this.getOwnerFrame(), ubiArgs, this, "", true);
        }

        //라니안 체크된 그리드데이터만 데이터셋에 복사
        this.gfn_checkGridCopy = function(gridObj,datasetName){
        	if(this.gfn_isNull(datasetName)){
        		datasetName = "temp";
        	}
        	var ds = new Dataset(datasetName);

        	if(this.gfn_isNotNull(gridObj)){
        		var sourceDataset = this.lookup(gridObj.binddataset);
        		var checkList = this.gfn_getCheckedIndex(gridObj);
        		
        		var sKey;
        		var sValue;
        		var dsIndex;
        		if(checkList != null && checkList.length > 0){
        			for(var i=0 ; i< checkList.length ; i++){
        				dsIndex = ds.addRow();
        				for(var j=0 ; j<sourceDataset.getColCount() ; j++){
        					sKey = sourceDataset.getColID(j);
        					sValue = sourceDataset.getColumn(checkList[i], sKey);

        					if(!ds.setColumn(i, sKey, sValue)){
        						ds.addColumn(sKey, "string");
        						if(ds.getRowCount() == 0){
        							ds.addRow();
        						}
        						ds.setColumn(i, sKey, sValue);
        					}
        				}
        			}
        		}
        	}

        	return ds;
        }

        this.gfn_checkedNSelectGridDelete = function(gridObj)
        {
        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var dsSource = this.lookup(gridObj.binddataset);
        	if(this.gfn_isNull(checkList)|| checkList.length == 0){
        		checkList.push(dsSource.rowposition);
        	}
        	dsSource.deleteMultiRows(checkList);
        	if(dsSource.rowcount == 0 ){
        		dsSource.setConstColumn("HCHK","0");
        	}
        }

        this.gfn_checkedGridDelete = function(gridObj)
        {
        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var dsSource = this.lookup(gridObj.binddataset);
        	
        	dsSource.deleteMultiRows(checkList);
        	if(dsSource.rowcount == 0 ){
        		dsSource.setConstColumn("HCHK","0");
        	}
        }

        this.gfn_checkedNSelectGridAppend = function(gridObj,dsTarget,updateColumn,updateValue)
        {
        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var dsSource = this.lookup(gridObj.binddataset);
        // 	if(dsTarget.rowcount == 0){
        // 		dsTarget.assign(dsSource);
        // 		dsTarget.deleteAll();
        // 	}
        	if(this.gfn_isNull(checkList) || checkList.length == 0){
        		if(dsSource.getRowCount() > 0){
        			checkList.push(dsSource.rowposition);
        		}
        	}
        	var addRowIndex = -1;
        	for(var idx=0;idx<checkList.length;idx++){
        		addRowIndex = dsTarget.addRow();
        		dsTarget.copyRow(addRowIndex, dsSource, checkList[idx]);
        		if(this.gfn_isNotNull(updateColumn)){
        			dsTarget.setColumn(addRowIndex, updateColumn, updateValue);
        		}
        	}
        }

        this.gfn_checkedGridCopy = function(gridObj,dsTarget)
        {
        	var checkList = this.gfn_getCheckedIndex(gridObj);
        	var dsSource = this.lookup(gridObj.binddataset);
        	dsTarget.assign(dsSource);
        	dsTarget.deleteAll();
        	for(var idx=0;idx<checkList.length;idx++){
        		dsTarget.copyRow(dsTarget.addRow(), dsSource, checkList[idx]);
        	}
        }

        //라니안 그리드의 체크된 인덱스 리스트
        this.gfn_getCheckedIndex = function(gridObj)
        {
        	var checkList = this.getSelected(gridObj);
        	return checkList;
        }

        this.getSelected = function (privateGrid)
        {
        	var privateDataset = this.lookup(privateGrid.binddataset);

        	var selectedPosition = new Array();
        	var checked = 0;
        	if(this.gfn_isNotNull(privateDataset)) 
        	{
        		for(var index = 0; index < privateGrid.rowcount; index++) 
        		{
        			checked = privateDataset.getColumn(index, "CHK");
        			if(checked == 1 || checked == true){
        				selectedPosition[selectedPosition.length] = index;
        			}
        		}
        		return selectedPosition;
        	}
        	else 
        	{
        		return null;
        	}
        }

        this.gfn_grdDoubleClickChangeRowType = function(gridObj){
        	gridObj.set_enableredraw(false);
        	var privateDataset = this.lookup(gridObj.binddataset);

        	for(var i=0 ; i< gridObj.rowcount ; i++){
        		privateDataset.setColumn(i, "CHK", 0);
        	}
        	privateDataset.setColumn(gridObj.getSelectedRows(), "CHK", 1);
        	gridObj.set_enableredraw(true);
        }

        this.gfn_grdDoubleClick = function(gridObj,notChecked){
        	if(this.gfn_isNull(gridObj)){
        		return false;
        	}
        	gridObj.set_enableredraw(false);
        	var privateDataset = this.lookup(gridObj.binddataset);
        	privateDataset.set_updatecontrol(false);
        	for(var i=0 ; i< gridObj.rowcount ; i++){
        		privateDataset.setColumn(i, "CHK", 0);
        	}
        	if(notChecked){
        		privateDataset.setColumn(gridObj.getSelectedRows(), "CHK", 0);
        	}else{
        		privateDataset.setColumn(gridObj.getSelectedRows(), "CHK", 1);
        	}
        	privateDataset.set_updatecontrol(true);
        	gridObj.set_enableredraw(true);
        	
        	return true;
        }

        this.gfn_grdHeaderClick = function(obj,e,thisObj)
        {
         	var colName = this.gfn_gridBindName(obj, e.cell);
         	var sortValue = thisObj.ds_param.getColumn(0, "sortValue");
        	var privateDataset = this.lookup(obj.binddataset);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && privateDataset.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		thisObj.parent.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		privateDataset.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		privateDataset.set_updatecontrol(true);
        	}
        }

        this.gfn_popGrdHeaderClick = function(obj,e,thisObj)
        {
         	var colName = this.gfn_gridBindName(obj, e.cell);
         	var sortValue = thisObj.ds_param.getColumn(0, "sortValue");
        	var privateDataset = this.lookup(obj.binddataset);
        	
        	if(colName != "CHK" && colName != "PAGING_NUM" && privateDataset.rowcount > 0){
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		thisObj.parent.form.div_cond.btn_search.click();
        	}else if(colName == "CHK"){
        		privateDataset.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		privateDataset.set_updatecontrol(true);
        	}
        }

        this.gfn_grdHeaderClickNotSearch = function(obj,e,thisObj)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var privateDataset = this.lookup(obj.binddataset);
        	
        	obj.set_enableredraw(false);
        	if(colName != "CHK" && colName != "PAGING_NUM" && privateDataset.rowcount > 0){
        	
        		this.gfn_sortDataset(obj, e);

        	}else if(colName == "CHK"){
        		privateDataset.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		privateDataset.set_updatecontrol(true);
        	}
        	obj.set_enableredraw(true);
        }

        this.gfn_grdSingleRowCheck = function(obj)
        {
        	if(obj == null){
        		return false;
        	}
        	var checkList = this.gfn_getCheckedIndex(obj);
        	if(checkList.length == 0){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return false;
        	}else if(checkList.length > 1){
        		this.gfn_alert("MSG_CHKDATA_ONLY_ONE_SELECT");
        		return false;
        	}else{
        		return true;
        	}
        }

        this.gfn_grdMultiRowCheck = function(obj)
        {
        	if(obj == null){
        		return false;
        	}
        	var checkList = this.gfn_getCheckedIndex(obj);
        	if(checkList.length == 0){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return false;
        	}else{
        		return true;
        	}
        }

        this.gfn_getGridSelectedColumn = function(gridObj,key){
        	var returnValue = "";
        	var privateDataset = this.lookup(gridObj.binddataset);
        	returnValue = privateDataset.getColumn(privateDataset.rowposition, key);
        	return this.gfn_isNullEmpty(returnValue);
        }

        this.gfn_setGridSelectedColumn = function(gridObj,key,val){
        	var returnValue = "";
        	var privateDataset = this.lookup(gridObj.binddataset);
        	privateDataset.setColumn(privateDataset.rowposition, key, val);
        }

        /**
         * @class gfn_statusChk
         * @param objDs - 데이타 셋
                  nRow  - 해당 행
         * @return None
         */
        this.gfn_statusChk = function(objDs,nRow)
        {
        	if(objDs.getRowType(nRow) == "4" && objDs.getColumn(nRow, "STATUS") == "") objDs.setColumn(nRow, "STATUS", "U");
        }

        this.gfn_grdCheckCopyToDataset = function(gridObject,sourceDataset,targetDataset){
        	if(this.gfn_isNull(gridObject)){
        		return;
        	}
        	
        	if(this.gfn_isNull(sourceDataset)){
        		return;
        	}	
        	
        	if(this.gfn_isNull(targetDataset)){
        		return;
        	}
        	
        	targetDataset.set_updatecontrol(false);
        	
        	targetDataset.assign(sourceDataset);
        	targetDataset.deleteAll();
        	targetDataset.clearData();
        	
        	var checkList = this.gfn_getCheckedIndex(gridObject);
        	for(var i=0; i<checkList.length ; i++){
        		targetDataset.copyRow(targetDataset.addRow(), sourceDataset, checkList[i]);
        	}
        	targetDataset.set_updatecontrol(true);
        }

        /**
         * @class gfn_statusChk
         * @param grdObj - 그리드
         * @return None
         */
        this.gfn_getScroll = function(grdObj)
        {
        	if(grdObj.autofittype == "col") return;
        	
        	this.ds_grdInit.setColumn(0, grdObj.name+"Sch", grdObj.hscrollbar.pos);
        	this.ds_grdInit.setColumn(0, grdObj.name+"Scv", grdObj.vscrollbar.pos);
        }

        /**
         * @class gfn_statusChk
         * @param grdObj - 그리드
         * @return None
         */
        this.gfn_setScroll = function(grdObj)
        {
        	if(grdObj.autofittype == "col") return;
        	
        	grdObj.hscrollbar.set_pos(this.ds_grdInit.getColumn(0, grdObj.name+"Sch"));
        //	grdObj.vscrollbar.set_pos(this.ds_grdInit.getColumn(0, grdObj.name+"Scv"));
        }

        this.gfn_headerRowPosChangeCheckeDetail = function(headerDatasetObj,targetDatasetObj,eventObj,sCallback,thisObj)
        {
        	if(this.gfn_isChangeDataset(targetDatasetObj)){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				eval("thisObj."+sCallback+"();");
        			}else{
        				headerDatasetObj.set_rowposition(eventObj.oldrow);
        			}
        		});
        	}else {
        		eval("thisObj."+sCallback+"();");
        	}
        }

        this.gfn_isChangeDataset = function(datasetObj)
        {
        	if(this.gfn_isNull(datasetObj)){
        		return false;
        	}
        	var returnValue = false;
        	var status = "";
        	if(datasetObj.getDeletedRowCount() > 0) {
        		return true;
        	}
        	for(var i = 0 ; i < datasetObj.rowcount ; i++){
        		if(datasetObj.getRowType(i) == nexacro.NormalDataset.ROWTYPE_INSERT || datasetObj.getRowType(i) == nexacro.NormalDataset.ROWTYPE_UPDATE || datasetObj.getRowType(i) == nexacro.NormalDataset.ROWTYPE_DELETE){
        			returnValue = true;
        			break;
        		}
        		status = datasetObj.getColumn(i, "STATUS");
        		if(status == "C" || status == "U" || status == "D"){
        			returnValue = true;
        			break;
        		}
        	}
        	return returnValue;
        }

        this.gfn_saveStatusMyGridFormat = function(obj)
        {
        	var nRow = 0;
        	var uCnt = 0;
        	var grdCnt = 0;
        	var uskey = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.gv_menuId), "uskey");
        	var grdObj;
        	
        	var sOldInitString = ""; // 저장된 초기 데이타(개인화 시점 기준)
        	var sOldFormatString = ""; // 그리드 초기 데이타(소스코드 기준)
        	var sNewFormatString = ""; // 변경한 그리드 데이타
        	
        	if(this.gfn_isNull(application.gds_userGrid.getColumnInfo("delYn"))) application.gds_userGrid.addColumn("delYn");
        	if(this.gfn_isNotNull(this.div_work.gv_grdList)) grdCnt = this.div_work.gv_grdList.length;
        	
        	for(var i = 0 ; i < grdCnt ; i++){
        		grdObj = this.div_work.gv_grdList[i];
        		sOldFormatString = this.div_work.ds_grdInit.getColumn(0, grdObj.name + "Init");
        		sNewFormatString = grdObj.getCurFormatString();
        		
        		nRow = application.gds_userGrid.findRowExpr("urKey == '"+this.gfn_getUserInfo("urKey")+"' && usKey == '"+uskey+"' && usgridId == '"+grdObj.name+"'");
        		if(nRow != -1) sOldInitString = application.gds_userGrid.getColumn(nRow, "urdfusInfoInit");
        		
        		if(nRow != -1){
        			if(sOldFormatString == sOldInitString){
        				if(sNewFormatString != application.gds_userGrid.getColumn(nRow, "urdfusInfo")){
        					//그리드 구성 정보가 변경된 경우
        					application.gds_userGrid.setColumn(nRow, "urdfusInfo", sNewFormatString);
        					uCnt++;
        				} 
        			}else{ //그리드 개인화 데이터가 저장 된후 개발자가 그리드 구성을 바꾼 경우 해당 화면 개인화 데이터를 날려 버린다.
        				application.gds_userGrid.setColumn(nRow, "delYn", "Y");
        				uCnt++;
        			}
        		}else{
        			if(sOldFormatString != sNewFormatString){
        				nRow = application.gds_userGrid.addRow();
        				application.gds_userGrid.setColumn(nRow, "urKey", this.gfn_getUserInfo("urKey"));
        				application.gds_userGrid.setColumn(nRow, "usKey", uskey);
        				application.gds_userGrid.setColumn(nRow, "usgridId", grdObj.name);
        				application.gds_userGrid.setColumn(nRow, "urdfusInfo", grdObj.getCurFormatString());
        				application.gds_userGrid.setColumn(nRow, "urdfusInfoInit", this.div_work.ds_grdInit.getColumn(0, grdObj.name+"Init"));
        				
        				uCnt++;
        			}
        		}
        	}
        	
        	return uCnt;
        }
        });


    
        this.loadIncludeScript(path, true);
        
        obj = null;
    };
}
)();
