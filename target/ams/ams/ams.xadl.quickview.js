﻿(function()
{
    return function()
    {
        // Theme, Component URI Setting
        this._theme_uri = "./_theme_/";
        this._globalvar_uri = "globalvars.xml";
        this.loadTypedefition = function()
        {
            // this._addService(prefixid, type, url, cachelevel, codepage, language, version, communication);
            this._addService("default_typedef.xml", "svc", "JSP", "http://localhost:8080/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "frame", "form", "./frame/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "img", "file", "./img/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "css", "file", "./css/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "nexacro14lib", "file", "./nexacro14lib/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "lib", "js", "./lib/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "obj_xcommon", "form", "./obj_xcommon/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "guide", "form", "./guide/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "template", "form", "./template/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "sample", "form", "./sample/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "comm", "form", "./comm/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "cond", "form", "./cond/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "master", "form", "./master/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "menu", "form", "./menu/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "user", "form", "./user/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "item", "form", "./item/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "role", "form", "./role/", "none", null, "", "0", "0");
            this._addService("default_typedef.xml", "system", "form", "./system/", "none", null, "", "0", "0");

            this._component_uri = (this._arg_compurl ? this._arg_compurl : "./nexacro14lib/component/");
            // load components
            var registerclass = [
            		{"id":"Div", "classname":"nexacro.Div", "type":"JavaScript"},
            		{"id":"Button", "classname":"nexacro.Button", "type":"JavaScript"},
            		{"id":"PopupDiv", "classname":"nexacro.PopupDiv", "type":"JavaScript"},
            		{"id":"Combo", "classname":"nexacro.Combo", "type":"JavaScript"},
            		{"id":"CheckBox", "classname":"nexacro.CheckBox", "type":"JavaScript"},
            		{"id":"ListBox", "classname":"nexacro.ListBox", "type":"JavaScript"},
            		{"id":"Edit", "classname":"nexacro.Edit", "type":"JavaScript"},
            		{"id":"MaskEdit", "classname":"nexacro.MaskEdit", "type":"JavaScript"},
            		{"id":"TextArea", "classname":"nexacro.TextArea", "type":"JavaScript"},
            		{"id":"Menu", "classname":"nexacro.Menu", "type":"JavaScript"},
            		{"id":"Tab", "classname":"nexacro.Tab", "type":"JavaScript"},
            		{"id":"ImageViewer", "classname":"nexacro.ImageViewer", "type":"JavaScript"},
            		{"id":"Radio", "classname":"nexacro.Radio", "type":"JavaScript"},
            		{"id":"Calendar", "classname":"nexacro.Calendar", "type":"JavaScript"},
            		{"id":"Static", "classname":"nexacro.Static", "type":"JavaScript"},
            		{"id":"Grid", "classname":"nexacro.Grid", "type":"JavaScript"},
            		{"id":"Spin", "classname":"nexacro.Spin", "type":"JavaScript"},
            		{"id":"PopupMenu", "classname":"nexacro.PopupMenu", "type":"JavaScript"},
            		{"id":"GroupBox", "classname":"nexacro.GroupBox", "type":"JavaScript"},
            		{"id":"ProgressBar", "classname":"nexacro.ProgressBar", "type":"JavaScript"},
            		{"id":"Plugin", "classname":"nexacro.Plugin", "type":"JavaScript"},
            		{"id":"Dataset", "classname":"nexacro.NormalDataset", "type":"JavaScript"},
            		{"id":"FileUpload", "classname":"nexacro.FileUpload", "type":"JavaScript"},
            		{"id":"FileDownload", "classname":"nexacro.FileDownload", "type":"JavaScript"},
            		{"id":"WebBrowser", "classname":"nexacro.WebBrowser", "type":"JavaScript"},
            		{"id":"UbiReport", "classname":"nexacro.UbiViewer", "type":"JavaScript"},
            		{"id":"ExtMonthCalendar", "classname":"nexacro.ExtMonthCalendar", "type":"JavaScript"},
            		{"id":"Graphic", "classname":"nexacro.Graphic", "type":"JavaScript"}
            ];
            this._addClasses(registerclass);
        };
        
        this.on_loadGlobalVariables = function()
        {
            // global variable
            this._addVariable("gv_userId", null, false);
            this._addVariable("gv_userNm", null, false);
            this._addVariable("gv_userAuth", null, false);
            this._addVariable("gv_sServer", "L", false);
            this._addVariable("gv_language", null, false);
            this._addVariable("gv_url", null, false);
            this._addVariable("gv_chartUrl", null, false);
            this._addVariable("gv_loginStatus", null, false);
            this._addVariable("gv_system", null, false);
            this._addVariable("gv_activeApp", null, false);
            this._addVariable("gv_sUrl", "/nexacro/handlerAdapter.nx", false);
            this._addVariable("gv_ams", "ams", false);
            this._addVariable("gv_wms", "wms", false);
            this._addVariable("gv_oms", "oms", false);
            this._addVariable("gv_tms", "tms", false);
            this._addVariable("gv_vms", "vms", false);
            this._addVariable("gv_userUrl", null, false);
            this._addVariable("gv_fUrl", "/nexacro/fileHandlerAdapter.nx", false);
            this._addVariable("gv_showHideYn", "Y", false);
            this._addVariable("gv_nodataMsg", null, false);
            this._addVariable("gv_saveUrl", "/nexacro/handlerAdapterSave.nx", false);
            this._addVariable("gv_arrClipboard", null, false);
            this._addVariable("gv_usobAuth", null, false);
            this._addVariable("gv_mdm", "mdm", false);
            this._addVariable("gv_fis", "fis", false);
            this._addVariable("gv_ibs", "ibs", false);
            this._addVariable("gv_cms", "cms", false);
            this._addVariable("gv_kpi", "kpi", false);
            this._addVariable("gv_ptl", "ptl", false);
            this._addVariable("gv_connectUrl", null, false);

            // global image

            // global dataset
            var obj = null;
            obj = new Dataset("gds_menu", this);
            obj._setContents("<ColumnInfo><Column id=\"eqtype\" type=\"string\" size=\"256\"/><Column id=\"isleaf\" type=\"string\" size=\"256\"/><Column id=\"isseparator\" type=\"string\" size=\"256\"/><Column id=\"mekey\" type=\"string\" size=\"256\"/><Column id=\"mename\" type=\"string\" size=\"256\"/><Column id=\"usname\" type=\"string\" size=\"256\"/><Column id=\"meorder\" type=\"string\" size=\"256\"/><Column id=\"uppermekey\" type=\"string\" size=\"256\"/><Column id=\"uskey\" type=\"string\" size=\"256\"/><Column id=\"uspath\" type=\"string\" size=\"256\"/><Column id=\"apkey\" type=\"string\" size=\"256\"/><Column id=\"melvl\" type=\"string\" size=\"256\"/><Column id=\"nxpath\" type=\"STRING\" size=\"256\"/><Column id=\"remark\" type=\"STRING\" size=\"256\"/></ColumnInfo><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000040</Col><Col id=\"MENAME\">기본 마스터 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">20</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"USNAME\">어플리케이션</Col><Col id=\"MEORDER\">30</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00000050</Col><Col id=\"MENAME\">어플리케이션</Col><Col id=\"USKEY\">US00000400</Col><Col id=\"USPATH\">ADMIN_US00000400_masterApplication</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000070</Col><Col id=\"MENAME\">메시지</Col><Col id=\"USNAME\">메시지</Col><Col id=\"MEORDER\">50</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000040</Col><Col id=\"USPATH\">ADMIN_US00000040_masterMessage</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000080</Col><Col id=\"MENAME\">매입거래처</Col><Col id=\"USNAME\">매입거래처</Col><Col id=\"MEORDER\">60</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000320</Col><Col id=\"USPATH\">ADMIN_US00000320_account</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103230</Col><Col id=\"MENAME\">거래처별센터</Col><Col id=\"USNAME\">거래처별센터</Col><Col id=\"MEORDER\">61</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003660</Col><Col id=\"USPATH\">ADMIN_US00003660_Acxct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MENAME\">화주</Col><Col id=\"USNAME\">화주</Col><Col id=\"MEORDER\">70</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00000090</Col><Col id=\"USKEY\">US00000280</Col><Col id=\"USPATH\">ADMIN_US00000280_owner</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000100</Col><Col id=\"MENAME\">센터</Col><Col id=\"USNAME\">센터</Col><Col id=\"MEORDER\">80</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000030</Col><Col id=\"USPATH\">ADMIN_US00000030_masterCenter</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103130</Col><Col id=\"MENAME\">매출거래처</Col><Col id=\"USNAME\">매출거래처</Col><Col id=\"MEORDER\">81</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003590</Col><Col id=\"USPATH\">ADMIN_SC00003890_STORE</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MENAME\">센터별매출처</Col><Col id=\"USNAME\">센터별매출처</Col><Col id=\"MEORDER\">82</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"MEKEY\">ME00103240</Col><Col id=\"USKEY\">US00003670</Col><Col id=\"USPATH\">ADMIN_US00003670_Storexct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000110</Col><Col id=\"MENAME\">장비</Col><Col id=\"USNAME\">장비</Col><Col id=\"MEORDER\">90</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00000770</Col><Col id=\"USPATH\">ADMIN_US00000770_equipment</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00109380</Col><Col id=\"MENAME\">알림설정</Col><Col id=\"USNAME\">알림설정</Col><Col id=\"MEORDER\">100</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00008920</Col><Col id=\"USPATH\">ADMIN_US00008920_masterAlertSetting</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000420</Col><Col id=\"MENAME\">상품 단위</Col><Col id=\"USNAME\">상품 단위</Col><Col id=\"MEORDER\">100</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000270</Col><Col id=\"USPATH\">ADMIN_US00000270_itemUnit</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">2</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103280</Col><Col id=\"MENAME\">운송사</Col><Col id=\"USNAME\">운송사</Col><Col id=\"MEORDER\">107</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003690</Col><Col id=\"USPATH\">ADMIN_US00003690_Carrier</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\"/><Col id=\"MEKEY\">ME00103290</Col><Col id=\"MENAME\">차량</Col><Col id=\"USNAME\">차량</Col><Col id=\"MEORDER\">108</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003730</Col><Col id=\"USPATH\">ADMIN_US00003730_Equip</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\"/><Col id=\"MEKEY\">ME00103300</Col><Col id=\"MENAME\">기사정보</Col><Col id=\"USNAME\">기사정보</Col><Col id=\"MEORDER\">109</Col><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USKEY\">US00003740</Col><Col id=\"USPATH\">ADMIN_US00003740_Driver</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\"/><Col id=\"UPPERMEKEY\">ME00000040</Col><Col id=\"USNAME\">PUSH발송</Col><Col id=\"MEORDER\">110</Col><Col id=\"MEKEY\">ME00109471</Col><Col id=\"MENAME\">PUSH발송</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00008961</Col><Col id=\"USPATH\">ADMIN_US00008961_PUSHSEND</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000410</Col><Col id=\"MENAME\">상품 그룹</Col><Col id=\"USNAME\">상품 그룹</Col><Col id=\"MEORDER\">110</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000100</Col><Col id=\"USPATH\">ADMIN_US00000170_itemCodeGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">2</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000200</Col><Col id=\"MENAME\">관리자 마스터</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">110</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"ISLEAF\">N</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000205</Col><Col id=\"MENAME\">관리 기준 코드</Col><Col id=\"USNAME\">관리 기준 코드</Col><Col id=\"MEORDER\">115</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000080</Col><Col id=\"USPATH\">ADMIN_US00000080_masterAdminCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000430</Col><Col id=\"MENAME\">상품</Col><Col id=\"USNAME\">상품</Col><Col id=\"MEORDER\">120</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000330</Col><Col id=\"USPATH\">ADMIN_US00000330_itemCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000210</Col><Col id=\"MENAME\">사용자 그룹</Col><Col id=\"USNAME\">사용자 그룹</Col><Col id=\"MEORDER\">120</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"USKEY\">US00000420</Col><Col id=\"USPATH\">ADMIN_US00000420_userUserGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000220</Col><Col id=\"MENAME\">사용자</Col><Col id=\"USNAME\">사용자</Col><Col id=\"MEORDER\">130</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"USKEY\">US00000460</Col><Col id=\"USPATH\">ADMIN_US00000460_userUSER</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000440</Col><Col id=\"MENAME\">상품 대체 코드</Col><Col id=\"USNAME\">상품 대체 코드</Col><Col id=\"MEORDER\">130</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000560</Col><Col id=\"USPATH\">ADMIN_US00000560_itemSubstitution</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00109851</Col><Col id=\"MENAME\">상품그룹인터페이스</Col><Col id=\"USNAME\">상품그룹인터페이스</Col><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000200</Col><Col id=\"USKEY\">US00009331</Col><Col id=\"USPATH\">ADMIN_US00009331_ItemGroupInterface</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000400</Col><Col id=\"MENAME\">상품 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00103190</Col><Col id=\"MENAME\">상품별센터</Col><Col id=\"USNAME\">상품별센터</Col><Col id=\"MEORDER\">140</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003640</Col><Col id=\"USPATH\">ADMIN_US00003640_Icxct</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00104240</Col><Col id=\"MENAME\">센터별거래처별상품</Col><Col id=\"USNAME\">센터별거래처별상품</Col><Col id=\"MEORDER\">150</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00004640</Col><Col id=\"USPATH\">ADMIN_US00004640_Icxctxac</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000600</Col><Col id=\"MENAME\">메뉴 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">210</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000610</Col><Col id=\"MENAME\">사용자 화면 검색 조건</Col><Col id=\"USNAME\">사용자 화면 검색 조건</Col><Col id=\"MEORDER\">220</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000050</Col><Col id=\"USPATH\">ADMIN_US00000050_searchCondition</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103260</Col><Col id=\"MENAME\">임가공 상품</Col><Col id=\"USNAME\">임가공 상품</Col><Col id=\"MEORDER\">230</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003700</Col><Col id=\"USPATH\">ADMIN_US00003700_SetItemCode</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000620</Col><Col id=\"MENAME\">사용자 화면</Col><Col id=\"USNAME\">사용자 화면</Col><Col id=\"MEORDER\">230</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000060</Col><Col id=\"USPATH\">ADMIN_US00000060_userScreen</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000630</Col><Col id=\"MENAME\">메뉴</Col><Col id=\"USNAME\">메뉴</Col><Col id=\"MEORDER\">240</Col><Col id=\"UPPERMEKEY\">ME00000600</Col><Col id=\"USKEY\">US00000070</Col><Col id=\"USPATH\">ADMIN_US00000070_menu</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000800</Col><Col id=\"MENAME\">권한 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">250</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000810</Col><Col id=\"MENAME\">ROLE 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">260</Col><Col id=\"UPPERMEKEY\">ME00000800</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000445</Col><Col id=\"MENAME\">상품 단위 업로드</Col><Col id=\"USNAME\">상품 단위 업로드</Col><Col id=\"MEORDER\">270</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00001850</Col><Col id=\"USPATH\">ADMIN_US00001850_ItemCodeUnitUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000820</Col><Col id=\"MENAME\">ROLE별 어플리케이션</Col><Col id=\"USNAME\">ROLE별 어플리케이션</Col><Col id=\"MEORDER\">270</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000090</Col><Col id=\"USPATH\">ADMIN_US00000090_applicationByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00101390</Col><Col id=\"MENAME\">상품 그룹 업로드</Col><Col id=\"USNAME\">상품 그룹 업로드</Col><Col id=\"MEORDER\">275</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00002660</Col><Col id=\"USPATH\">ADMIN_US00002660_ItemCodeGroupUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000830</Col><Col id=\"MENAME\">ROLE별 화주</Col><Col id=\"USNAME\">ROLE별 화주</Col><Col id=\"MEORDER\">280</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000290</Col><Col id=\"USPATH\">ADMIN_US00000290_ownerByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000450</Col><Col id=\"MENAME\">상품 업로드</Col><Col id=\"USNAME\">상품 업로드</Col><Col id=\"MEORDER\">280</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000550</Col><Col id=\"USPATH\">ADMIN_US00000550_itemCodeUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000460</Col><Col id=\"MENAME\">상품 대체 코드 업로드</Col><Col id=\"USNAME\">상품 대체 코드 업로드</Col><Col id=\"MEORDER\">290</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00000620</Col><Col id=\"USPATH\">ADMIN_US00000620_substitutionUpload</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000840</Col><Col id=\"MENAME\">ROLE별 센터</Col><Col id=\"USNAME\">ROLE별 센터</Col><Col id=\"MEORDER\">290</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000300</Col><Col id=\"USPATH\">ADMIN_US00000300_centerByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000850</Col><Col id=\"MENAME\">ROLE별 메뉴</Col><Col id=\"USNAME\">ROLE별 메뉴</Col><Col id=\"MEORDER\">300</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000310</Col><Col id=\"USPATH\">ADMIN_US00000310_menuByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000860</Col><Col id=\"MENAME\">ROLE별 사용자 화면 오브젝트</Col><Col id=\"USNAME\">ROLE별 사용자 화면 오브젝트</Col><Col id=\"MEORDER\">310</Col><Col id=\"UPPERMEKEY\">ME00000810</Col><Col id=\"USKEY\">US00000340</Col><Col id=\"USPATH\">ADMIN_US00000340_usobByRole</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00103140</Col><Col id=\"MENAME\">용기</Col><Col id=\"USNAME\">용기</Col><Col id=\"MEORDER\">310</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00003600</Col><Col id=\"USPATH\">ADMIN_US00003600_Cntr</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000870</Col><Col id=\"MENAME\">사용자 ROLE 권한 관리</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">320</Col><Col id=\"UPPERMEKEY\">ME00000800</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00108190</Col><Col id=\"MENAME\">체화예상재고</Col><Col id=\"USNAME\">체화예상재고</Col><Col id=\"MEORDER\">320</Col><Col id=\"UPPERMEKEY\">ME00000400</Col><Col id=\"USKEY\">US00007930</Col><Col id=\"USPATH\">ADM_US00007930_Accexday</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000880</Col><Col id=\"MENAME\">사용자 그룹별 ROLE</Col><Col id=\"USNAME\">사용자 그룹별 ROLE</Col><Col id=\"MEORDER\">330</Col><Col id=\"UPPERMEKEY\">ME00000870</Col><Col id=\"USKEY\">US00000010</Col><Col id=\"USPATH\">ADMIN_US00000010_roleByUserGroup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000890</Col><Col id=\"MENAME\">사용자별 ROLE</Col><Col id=\"USNAME\">사용자별 ROLE</Col><Col id=\"MEORDER\">340</Col><Col id=\"UPPERMEKEY\">ME00000870</Col><Col id=\"USKEY\">US00000020</Col><Col id=\"USPATH\">ADMIN_US00000010_roleByUser</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">N</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000900</Col><Col id=\"MENAME\">시스템 환경 설정</Col><Col id=\"USNAME\"/><Col id=\"MEORDER\">350</Col><Col id=\"UPPERMEKEY\">ME00000010</Col><Col id=\"USKEY\">DIR</Col><Col id=\"USPATH\"/><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">0</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000910</Col><Col id=\"MENAME\">관리자 환경 설정</Col><Col id=\"USNAME\">관리자 환경 설정</Col><Col id=\"MEORDER\">360</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000900</Col><Col id=\"USPATH\">ADMIN_US00000900_adminConfigSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">N</Col><Col id=\"MEKEY\">ME00000920</Col><Col id=\"MENAME\">사용자 이력 조회</Col><Col id=\"USNAME\">사용자 이력 조회</Col><Col id=\"MEORDER\">370</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000930</Col><Col id=\"USPATH\">ADMIN_US00000930_userHistory</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000930</Col><Col id=\"MENAME\">시스템 환경 설정</Col><Col id=\"USNAME\">시스템 환경 설정</Col><Col id=\"MEORDER\">380</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00000960</Col><Col id=\"USPATH\">ADMIN_US00000960_systemConfigSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME00000940</Col><Col id=\"MENAME\">나의 메뉴 설정</Col><Col id=\"USNAME\">나의 메뉴 설정</Col><Col id=\"MEORDER\">390</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">ADM_US00001940_MyMenuSetup</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME99999999</Col><Col id=\"MENAME\">SMAPLE</Col><Col id=\"USNAME\">SMAPLE</Col><Col id=\"MEORDER\">400</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">sample::sampleUser.xfdl</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row><Row><Col id=\"EQTYPE\">10</Col><Col id=\"ISLEAF\">Y</Col><Col id=\"ISSEPARATOR\">Y</Col><Col id=\"MEKEY\">ME99999998</Col><Col id=\"MENAME\">TEST</Col><Col id=\"USNAME\">TEST</Col><Col id=\"MEORDER\">410</Col><Col id=\"UPPERMEKEY\">ME00000900</Col><Col id=\"USKEY\">US00001940</Col><Col id=\"USPATH\">wms::XXAMC10005.xfdl</Col><Col id=\"APKEY\">ADMIN</Col><Col id=\"MELVL\">1</Col></Row>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_openMenu", this);
            obj._setContents("<ColumnInfo><Column id=\"WINID\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"isleaf\" type=\"STRING\" size=\"256\"/><Column id=\"isseparator\" type=\"STRING\" size=\"256\"/><Column id=\"mekey\" type=\"STRING\" size=\"256\"/><Column id=\"mename\" type=\"STRING\" size=\"256\"/><Column id=\"usname\" type=\"STRING\" size=\"256\"/><Column id=\"meorder\" type=\"STRING\" size=\"256\"/><Column id=\"uppermekey\" type=\"STRING\" size=\"256\"/><Column id=\"uskey\" type=\"STRING\" size=\"256\"/><Column id=\"uspath\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"melvl\" type=\"STRING\" size=\"256\"/><Column id=\"nxpath\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_msg", this);
            obj._setContents("<ColumnInfo><Column id=\"mulaapmsg_hdkey\" type=\"string\" size=\"256\"/><Column id=\"displaymessage\" type=\"string\" size=\"2000\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userInfo", this);
            obj._setContents("<ColumnInfo><Column id=\"divcd\" type=\"STRING\" size=\"256\"/><Column id=\"SESSION_USERINFO\" type=\"STRING\" size=\"256\"/><Column id=\"dst\" type=\"STRING\" size=\"256\"/><Column id=\"duplLog\" type=\"STRING\" size=\"256\"/><Column id=\"ctKey\" type=\"STRING\" size=\"256\"/><Column id=\"usrtyp\" type=\"STRING\" size=\"256\"/><Column id=\"rstclgst\" type=\"STRING\" size=\"256\"/><Column id=\"usKey\" type=\"STRING\" size=\"256\"/><Column id=\"urGrKey\" type=\"STRING\" size=\"256\"/><Column id=\"ctKey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"urName\" type=\"STRING\" size=\"256\"/><Column id=\"utcMinute\" type=\"STRING\" size=\"256\"/><Column id=\"lastpwyn\" type=\"STRING\" size=\"256\"/><Column id=\"utcOffset\" type=\"STRING\" size=\"256\"/><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"utcHour\" type=\"STRING\" size=\"256\"/><Column id=\"loginStatus\" type=\"STRING\" size=\"256\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"256\"/><Column id=\"laKey\" type=\"STRING\" size=\"256\"/><Column id=\"urCryptoName\" type=\"STRING\" size=\"256\"/><Column id=\"owkeym\" type=\"STRING\" size=\"256\"/><Column id=\"gmtUseYn\" type=\"STRING\" size=\"256\"/><Column id=\"chgPw\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userAuth", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_gridMenu", this);
            obj._setContents("<ColumnInfo><Column id=\"menuLvl\" type=\"STRING\" size=\"256\"/><Column id=\"menuId\" type=\"STRING\" size=\"256\"/><Column id=\"menuNm\" type=\"STRING\" size=\"256\"/><Column id=\"bUse\" type=\"STRING\" size=\"256\"/><Column id=\"bEnbleColumn\" type=\"STRING\" size=\"256\"/><Column id=\"hotkeyColumn\" type=\"STRING\" size=\"256\"/><Column id=\"iconColumn\" type=\"STRING\" size=\"256\"/><Column id=\"multiLang\" type=\"STRING\" size=\"256\"/><Column id=\"option\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">100</Col><Col id=\"menuNm\">오름차순 정렬</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuNm\">내림차순 정렬</Col><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">200</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">300</Col><Col id=\"menuNm\">필터</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">400</Col><Col id=\"menuNm\">필터제거</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">500</Col><Col id=\"menuNm\">Lock</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">600</Col><Col id=\"menuNm\">UnLock</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">700</Col><Col id=\"menuNm\">컬럼 목록</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">800</Col><Col id=\"menuNm\">초기화</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\">INITIALIZATION</Col><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">900</Col><Col id=\"menuNm\">Copy</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row><Row><Col id=\"menuLvl\">0</Col><Col id=\"menuId\">910</Col><Col id=\"menuNm\">Paste</Col><Col id=\"bUse\">Y</Col><Col id=\"bEnbleColumn\">1</Col><Col id=\"hotkeyColumn\"/><Col id=\"iconColumn\"/><Col id=\"multiLang\"/><Col id=\"option\"/></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_commCode", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_lang", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_param", this);
            obj._setContents("<ColumnInfo><Column id=\"pagingLimit\" type=\"STRING\" size=\"256\"/><Column id=\"currentPage\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"urPw\" type=\"STRING\" size=\"256\"/><Column id=\"laKey\" type=\"STRING\" size=\"256\"/><Column id=\"appkey\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"COUNT\" type=\"STRING\" size=\"256\"/><Column id=\"colName\" type=\"STRING\" size=\"256\"/><Column id=\"sortValue\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"pagingLimit\">100</Col><Col id=\"currentPage\">1</Col><Col id=\"ctkey\">1001</Col><Col id=\"urKey\"/><Col id=\"urPw\"/><Col id=\"laKey\"/><Col id=\"appkey\"/><Col id=\"eqtype\">10</Col><Col id=\"sortValue\"/><Col id=\"colName\"/><Col id=\"COUNT\"/></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_common", this);
            obj._setContents("<ColumnInfo><Column id=\"DEVICE\" type=\"STRING\" size=\"256\"/><Column id=\"EQTYPE\" type=\"STRING\" size=\"256\"/><Column id=\"TIMEZONE\" type=\"STRING\" size=\"256\"/><Column id=\"uskey\" type=\"STRING\" size=\"256\"/><Column id=\"ACTIVE_APP\" type=\"STRING\" size=\"256\"/><Column id=\"OWKEY_AUTH\" type=\"STRING\" size=\"256\"/><Column id=\"CTKEY_AUTH\" type=\"STRING\" size=\"256\"/><Column id=\"BEANID\" type=\"STRING\" size=\"256\"/><Column id=\"METHODNM\" type=\"STRING\" size=\"256\"/><Column id=\"LAKEY\" type=\"STRING\" size=\"256\"/><Column id=\"APKEY\" type=\"STRING\" size=\"256\"/><Column id=\"TRANSACTION_TYPE\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DEVICE\">PC</Col><Col id=\"EQTYPE\">10</Col><Col id=\"TIMEZONE\">9</Col><Col id=\"uskey\"/><Col id=\"ACTIVE_APP\">AMS</Col><Col id=\"OWKEY_AUTH\"/><Col id=\"CTKEY_AUTH\"/><Col id=\"BEANID\">loginController</Col><Col id=\"METHODNM\">actionLogin</Col><Col id=\"LAKEY\">KOR</Col><Col id=\"APKEY\">ADM</Col><Col id=\"TRANSACTION_TYPE\">NEXACRO</Col></Row></Rows>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/><Column id=\"gmtapply_chk\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_outCommon", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_systemConfig", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_admSystemConfig", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_srchCondition", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_srchCondition2", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userGrid", this);
            obj.set_useclientlayout("false");
            obj._setContents("<ColumnInfo><Column id=\"urKey\" type=\"STRING\" size=\"256\"/><Column id=\"usKey\" type=\"STRING\" size=\"256\"/><Column id=\"usgridId\" type=\"STRING\" size=\"256\"/><Column id=\"urdfusInfo\" type=\"STRING\" size=\"256\"/><Column id=\"urdfusInfoInit\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_userMenu", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_eqList", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnMenuObjectRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnAppRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnOwnerRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_rtnWHRole", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_map", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_code", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;

            obj = new Dataset("gds_acxur", this);
            obj._setContents("");
            this._addDataset(obj.name, obj);
            obj = null;


            

        };
        
        // property, event, createMainFrame
        this.on_initApplication = function()
        {
            // properties
            this.set_id("ams");
            this.set_version("");
            this.set_tracemode("none");
            this.set_themeid("amstheme.xtheme");
            this.set_httpretry("0");
            this.set_httptimeout("300");
            this.set_loadingimage("img::waitimage.gif");
            this.set_usewaitcursor("0");

            if (this._is_attach_childframe)
            	return;

            // frame
            var mainframe = this.createMainFrame("mainframe", "absolute", "0", "0", "1280", "800", null, null, this);
            mainframe.set_resizable("true");
            mainframe.set_showtitlebar("true");
            mainframe.set_showstatusbar("false");
            mainframe.style.set_menubarheight("0");
            mainframe.style.set_background("#ffffffff");
            mainframe.style.set_border("1 solid #3d3d3dff");
            mainframe.style.set_color("#ffffffff");
            mainframe.style.set_font("9 Dotum");
            mainframe.style.set_titlebarheight("26");
            mainframe.style.set_statusbarheight("0");
            mainframe.on_createBodyFrame = this.mainframe_createBodyFrame;

            // tray
            var tray = null;

        };
        

        
        this.mainframe_createBodyFrame = function()
        {
            var obj = new ChildFrame("childframe", "absolute", null, null, null, null, null, null, "", this);
            this.addChild(obj.name, obj);
            this.frame = obj;
            obj.set_formurl(application._quickview_formurl);
            obj.set_showtitlebar("false");
            obj.set_showstatusbar("false");
            obj = null;
        };
        
        this.on_initEvent = function()
        {

        };
        
        // screeninfo
        this.loadScreenInfo = function()
        {

        }
        
        // script Compiler
        this.registerScript("ams.xadl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AMS.xadl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.07.06
        * DESCRIPTION : Application 최초 로딩시 실행
        ------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        this.gv_HFrameSet = "";
        this.gv_LeftFrame = "";
        this.gv_VFrameSet = "";
        this.gv_LoginFrame = "";
        this.gv_TopFrame = "";
        this.gv_SubMainFrame = "";
        this.gv_TabFrame = "";
        this.gv_WorkFrame = "";
        this.gv_BottomFrame = "";

        /**
        * 프레임정보 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onload = function(obj,e)
        {
            this.gv_HFrameSet    = application.mainframe.HFrameSet;
            this.gv_LeftFrame    = application.mainframe.HFrameSet.LeftFrame;
            this.gv_VFrameSet    = application.mainframe.HFrameSet.VFrameSet;
            this.gv_LoginFrame   = application.mainframe.HFrameSet.VFrameSet.LoginFrame;
            this.gv_TopFrame     = application.mainframe.HFrameSet.VFrameSet.TopFrame;
            this.gv_SubMainFrame = application.mainframe.HFrameSet.VFrameSet.SubMainFrame;
            this.gv_TabFrame     = application.mainframe.HFrameSet.VFrameSet.TabFrame;
            this.gv_WorkFrame    = application.mainframe.HFrameSet.VFrameSet.WorkFrame;
            this.gv_BottomFrame  = application.mainframe.HFrameSet.VFrameSet.BottonFrame;
        }

        /**
        * URL 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onloadingglobalvariables = function(obj,e)
        {
        	application.gv_system = "ADM";
        	application.gv_activeApp = "AMS";
        	
            var sUrl = application.xadl;
            
            if(sUrl.indexOf("file://") >= 0) { // 로컬 실행
        //         application.gv_url = "http://localhost:8080/ams/ams/";
        //         application.services["svc"].url = "http://localhost:8080/";
        //         application.gv_chartUrl = "http://localhost:8080/ams/chart/";
                application.gv_sServer = "L";
            } else if(sUrl.indexOf("192.168.192.213") >= 0) { // 개발서버
        //         application.gv_url = "http://localhost:8080/ams/ams/";
                 application.services["svc"].url = "http://192.168.192.213:8080/";
                 application.services["frame"].set_cachelevel("session");
                 application.services["img"].set_cachelevel("session");
                 application.services["css"].set_cachelevel("session");
                 application.services["nexacro14lib"].set_cachelevel("session");
                 application.services["lib"].set_cachelevel("session");
                 application.services["obj_xcommon"].set_cachelevel("session");
                 application.services["guide"].set_cachelevel("session");
                 application.services["template"].set_cachelevel("session");
                 application.services["comm"].set_cachelevel("session");
                 application.services["cond"].set_cachelevel("session");
                 application.services["master"].set_cachelevel("session");
                 application.services["menu"].set_cachelevel("session");
                 application.services["user"].set_cachelevel("session");
                 application.services["item"].set_cachelevel("session");
                 application.services["role"].set_cachelevel("session");
                 application.services["system"].set_cachelevel("session");
        //         application.gv_chartUrl = "http://Url/ams/chart/ams/";
                application.gv_sServer = "D";
            } else if(sUrl.indexOf("localhost") >= 0) { // 운영서버
                 application.services["svc"].url = "http://localhost:8080/";
                 application.services["frame"].set_cachelevel("session");
                 application.services["img"].set_cachelevel("session");
                 application.services["css"].set_cachelevel("session");
                 application.services["nexacro14lib"].set_cachelevel("session");
                 application.services["lib"].set_cachelevel("session");
                 application.services["obj_xcommon"].set_cachelevel("session");
                 application.services["guide"].set_cachelevel("session");
                 application.services["template"].set_cachelevel("session");
                 application.services["comm"].set_cachelevel("session");
                 application.services["cond"].set_cachelevel("session");
                 application.services["master"].set_cachelevel("session");
                 application.services["menu"].set_cachelevel("session");
                 application.services["user"].set_cachelevel("session");
                 application.services["item"].set_cachelevel("session");
                 application.services["role"].set_cachelevel("session");
                 application.services["system"].set_cachelevel("session");
                application.gv_sServer = "L";
            } else { // 미 예외 처리된 경우
        //         application.gv_url = "http://localhost:8080/ams/ams/";
        //         application.services["svc"].url = "http://localhost:8080/";
        //         application.gv_chartUrl = "http://localhost:8080/ams/chart/";
                application.gv_sServer = "L";
            }
        }

        /**
        * FRAME 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.gfn_setFrame = function(sMode)
        {
            if(sMode == "L") { // Login 모드
                application.gv_HFrameSet.set_separatesize("0,*");
                application.gv_VFrameSet.set_separatesize("*,0,0,0,0,0");
            } else if(sMode == "M") { // 메인 모드
                if(application.gv_showHideYn == "Y") application.gv_HFrameSet.set_separatesize("48,*");
                else application.gv_HFrameSet.set_separatesize("10,*");
                application.gv_VFrameSet.set_separatesize("0,70,*,0,0,23");
            } else if(sMode == "W") { // Work 모드
                if(application.gv_showHideYn == "Y") application.gv_HFrameSet.set_separatesize("48,*");
                else application.gv_HFrameSet.set_separatesize("10,*");
                application.gv_VFrameSet.set_separatesize("0,70,0,28,*,23");
            }
        }

        /**
        * NULL Check
        * @return
        * @example
        * @memberOf public
        */
        this.agfn_isNull = function(value)
        {
            if (value === null || value === undefined || value == "") {
        		return true;
            } else {
        		return false;
            }
        }

        /**
        * BeforeExit 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onbeforeexit = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(!this.agfn_isNull(application.gv_loginStatus)){
        //			return "로그아웃 하시겠습니까?";
        		}
        	}
        }

        /**
        * Exit 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.application_onexit = function(obj,e)
        {
        	if(obj == e.fromobject){
        	}
        }

        /**
        * Close 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.MainFrame_onclose = function(obj,e)
        {
        	if(obj == e.fromobject){
        	}
        }

        /**
        * beforeclose 셋팅
        * @return
        * @example
        * @memberOf public
        */
        this.MainFrame_onbeforeclose = function(obj,e)
        {
        	var sMenu = "";
        	var dsTab;
        	
        	if(obj == e.fromobject){
        		for(var i = 0 ; i < application.gds_openMenu.rowcount ; i++){
        			if(i == 0) sMenu += application.gds_openMenu.getColumn(i, "mekey");
        			else sMenu += "|" + application.gds_openMenu.getColumn(i, "mekey");
        		}
        		
        		if(system.navigatorname == "nexacro"){
        			application.setPrivateProfile("openMenu"+application.gv_activeApp, sMenu);
        			application.setPrivateProfile("logoutLnag", application.getPrivateProfile("laKey"));
        		}else{
        			window.localStorage.setItem("openMenu"+application.gv_activeApp, sMenu);
        			window.localStorage.setItem("logoutLnag", window.localStorage.getItem("laKey"));
        		}
        		
        		dsTab = this.gv_TabFrame.form;
        		
        		for(var i = 0 ; i < dsTab.ds_Tab.rowcount ; i++){
        			TabObj = dsTab.fn_findObj(dsTab.ds_Tab.getColumn(i, "TAB_ID"));
        			
        			if(TabObj.cssclass == "btn_TaF_tab_on"){
        				if(system.navigatorname == "nexacro") application.setPrivateProfile("showMenu"+application.gv_activeApp, dsTab.ds_Tab.getColumn(i, "WINID"));
        				else window.localStorage.setItem("showMenu"+application.gv_activeApp, dsTab.ds_Tab.getColumn(i, "WINID"));
        				
        				break;
        			}
        		}
        	}
        }
        
        });


        this.loadTypedefition();
        this.loadScreenInfo();
        this.loadTheme("amstheme.xtheme");

this.loadCss("css::LeftFrame.css");
this.loadCss("css::TopFrame.css");

    };
}
)();
