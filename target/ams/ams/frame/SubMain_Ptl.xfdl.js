﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("DashBoard");
                this.set_classname("DashBoard");
                this.set_scrollbars("none");
                this.set_titletext("데쉬보드");
                this._setFormPosition(0,0,1230,670);
            }
            this.style.set_background("#eeeeeeff");

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Div("div_init", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            obj.set_text("Div00");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);
            obj = new Div("Div00", "absolute", "1.22%", "15", "22.68%", null, null, "20", this.div_init);
            obj.set_taborder("0");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::myinfo.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("Div02", "absolute", "24.39%", "16", "52.76%", "67", null, null, this.div_init);
            obj.set_taborder("1");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::sumdata.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("Div03", "absolute", "77.56%", "15", "21.14%", null, null, "20", this.div_init);
            obj.set_taborder("2");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::systeminfo.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_inBoundStatus", "absolute", "24.39%", "88", "26.18%", "188", null, null, this.div_init);
            obj.set_taborder("3");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::inorder.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_outBoundStatus", "absolute", "51.06%", "88", "26.1%", "188", null, null, this.div_init);
            obj.set_taborder("4");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::outorder.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_inventoryStatus", "absolute", "24.39%", "474", "52.76%", null, null, "20", this.div_init);
            obj.set_taborder("5");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::invendata.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_inBound", "absolute", "24.39%", "281", "26.18%", "188", null, null, this.div_init);
            obj.set_taborder("6");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::indata.xfdl");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_outBound", "absolute", "51.06%", "281", "26.1%", "188", null, null, this.div_init);
            obj.set_taborder("7");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("1 solid #dadadaff");
            obj.set_scrollbars("none");
            obj.set_url("dashboard::outdata.xfdl");
            this.div_init.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_init,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_text("Div00");
            		p.style.set_align("center middle");

            	}
            );
            this.div_init.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 670, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("DashBoard");
            		p.set_scrollbars("none");
            		p.set_titletext("데쉬보드");
            		p.style.set_background("#eeeeeeff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "dashboard::myinfo.xfdl");
            this._addPreloadList("fdl", "dashboard::sumdata.xfdl");
            this._addPreloadList("fdl", "dashboard::systeminfo.xfdl");
            this._addPreloadList("fdl", "dashboard::inorder.xfdl");
            this._addPreloadList("fdl", "dashboard::outorder.xfdl");
            this._addPreloadList("fdl", "dashboard::invendata.xfdl");
            this._addPreloadList("fdl", "dashboard::indata.xfdl");
            this._addPreloadList("fdl", "dashboard::outdata.xfdl");
        };
        
        // User Script
        this.addIncludeScript("SubMain_Ptl.xfdl", "lib::Comm.xjs");
        this.addIncludeScript("SubMain_Ptl.xfdl", "lib::chartUtil.xjs");
        this.registerScript("SubMain_Ptl.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SubMain_Ptl.xfdl
        * PROGRAMMER  : 장강철
        * DATE        : 2016.09.08
        * DESCRIPTION : SubMain 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/
         
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::chartUtil.xjs", null, exports); }	//include "lib::chartUtil.xjs"; 

         
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
         window.fv_chartData = "";
        this.divChartId= [];
        this.form_onload = function(obj,e) 
        {
            if ( nexacro.Browser != "Runtime"  ) {
                //this.fn_search (); 
            }
        }
        this.fn_createDashBoard = function(chartId,divObj ,layoutStr,dsDetailDataset ) {
            //var divObj = this.div_init.Div01;
         
        	window.fv_layoutStr=  layoutStr ;
        	var ss = this.gfn_createJsonObject( "column",dsDetailDataset) + ";";
        	eval("window.fv_chartData=" + this.gfn_makeJsonString(dsDetailDataset,null) + ";");
         
            this.divChartId.push(chartId);
            this.gfn_createChart(  chartId , divObj._unique_id, "fn_chartReadyHandler", "", "" ); 
        }
         
         
        // 공통
        window.fn_chartReadyHandler = function(id) { 
        	document.getElementById(id).setLayout(window.fv_layoutStr);
        	document.getElementById(id).setData(window.fv_chartData); 
        }
        window.fn_resize = function(id) {
              document.getElementById(id).resize();
        }

        this.DashBoard_onsize = function(obj,e)
        {
         
            for(var i = 0;i < this.divChartId.length ;i++) {
                  eval( "window.fn_resize("+ this.divChartId[i] +")" ) ;   
            }

         
        }

         
         
        this.div_init_onkeydown = function(obj,e)
        {
        trace( e.keycode );
        	 if(e.ctrlKey && e.shiftKey && e.keycode == 81  ){
         
        		    this.gfn_popup("popup_debug2", "test::test2.xfdl", null, 1200, 600, "",true, true);
        		    //function(sPopupId, sUrl, oArg, nWidth, nHeight, sPopupCallback, bModeless, bResizable)
        	 }
        }

        this.div_init_onclick = function(obj,e)
        {
         
        }
         
         
        this.div_init_Button00_onclick = function(obj,e)
        {
        	this.fn_chartReload();
        }

        
        this.div_init_Button01_onclick = function(obj,e)
        {
            this.div_init.div_inventoryStatus.div_inventoryStatus.resize(400,300);	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.DashBoard_onsize, this);
            this.addEventHandler("onkeydown", this.DashBoard_onkeydown, this);
            this.div_init.addEventHandler("onkeydown", this.div_init_onkeydown, this);
            this.div_init.addEventHandler("onclick", this.div_init_onkeydown, this);

        };

        this.loadIncludeScript("SubMain_Ptl.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
