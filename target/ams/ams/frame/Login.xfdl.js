﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("frame_login");
                this.set_classname("frame_login");
                this.set_scrollbars("none");
                this.set_cssclass("sta_Lgn_bg");
                this._setFormPosition(0,0,1280,780);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_lang", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("sta_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.style.set_background("URL('img::BG.jpg') stretch");
            obj.set_cssclass("sta_Lgn_bg");
            this.addChild(obj.name, obj);

            obj = new Div("div_init", "absolute", "50", "0", "1180", null, null, "170", this);
            obj.set_taborder("0");
            obj.set_text("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<UxFormClipBoard version=\"1.5\">\r\n  <CopyComponent>\r\n    <Static id=\"Static02\" taborder=\"5\" text=\"ⓒ VINFLUX company: Corp All Rights Reserved. (ver 2.0)\" left=\"472\" bottom=\"24\" width=\"336\" height=\"13\" style=\"color:#b0b0b0ff;align:center middle;font:8 Dotum;\"/>\r\n  </CopyComponent>\r\n</UxFormClipBoard>\r\n");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Static("Static01", "absolute", "296", null, "623", "260", null, "0", this.div_init);
            obj.set_taborder("8");
            obj.style.set_background("#00000090");
            obj.style.set_bordertype("round 16 16");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("div_login", "absolute", "337", null, "550", "235", null, "10", this.div_init);
            obj.set_taborder("9");
            obj.style.set_background("transparent");
            obj.style.set_accessibility("disable all '' '' ''");
            obj.set_scrollbars("none");
            this.div_init.addChild(obj.name, obj);
            obj = new Edit("edt_id", "absolute", "34", "108", "303", "38", null, null, this.div_init.div_login);
            obj.set_taborder("22");
            obj.set_displaynulltext("ID");
            obj.set_cssclass("edt_Lgn_id");
            obj.style.set_accessibility("enable all '아이디' '' ''");
            this.div_init.div_login.addChild(obj.name, obj);
            obj = new Edit("edt_pass", "absolute", "34", "151", "303", "38", null, null, this.div_init.div_login);
            obj.set_taborder("23");
            obj.set_password("true");
            obj.set_displaynulltext("PASSWORD");
            obj.set_cssclass("edt_Lgn_pw");
            obj.style.set_accessibility("enable all '비밀번호' '' ''");
            this.div_init.div_login.addChild(obj.name, obj);
            obj = new Button("btn_login", "absolute", "355", "65", "153", "124", null, null, this.div_init.div_login);
            obj.set_taborder("24");
            obj.set_text("LOGIN");
            obj.set_cssclass("btn_Lgn_ok");
            this.div_init.div_login.addChild(obj.name, obj);
            obj = new CheckBox("chk_idSave", "absolute", "34", "201", "113", "22", null, null, this.div_init.div_login);
            obj.set_taborder("25");
            obj.set_text("ID Save check");
            obj.set_cssclass("chk_Lgn_is");
            obj.style.set_color("#b9b9b9ff");
            this.div_init.div_login.addChild(obj.name, obj);
            obj = new ImageViewer("ImageViewer00", "absolute", "34", "5", "135", "38", null, null, this.div_init.div_login);
            obj.set_taborder("27");
            obj.set_image("URL('img::login_tit.png')");
            obj.set_imagealign("left middle");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #e5e5e5ff");
            this.div_init.div_login.addChild(obj.name, obj);
            obj = new Combo("cbo_lang", "absolute", "34", "65", "303", "38", null, null, this.div_init.div_login);
            this.div_init.div_login.addChild(obj.name, obj);
            var cbo_lang_innerdataset = new Dataset("cbo_lang_innerdataset", this.div_init.div_login.cbo_lang);
            cbo_lang_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">KOR</Col><Col id=\"datacolumn\">한국어</Col></Row><Row><Col id=\"codecolumn\">ENG</Col><Col id=\"datacolumn\">English</Col></Row><Row><Col id=\"codecolumn\">ZHO</Col><Col id=\"datacolumn\">中文(简体)</Col></Row><Row><Col id=\"codecolumn\">CHI</Col><Col id=\"datacolumn\">中文(繁體)</Col></Row></Rows>");
            obj.set_innerdataset(cbo_lang_innerdataset);
            obj.set_taborder("28");
            obj.set_value("KOR");
            obj.set_text("KOR");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_cssclass("cmb_Lgn_leg");
            obj.set_index("0");

            obj = new Static("Static02", "absolute", "50", null, null, "15", "0", "30", this);
            obj.set_taborder("2");
            obj.set_text("Copyright ⓒ EUGENE SUPERFREEZE CO., LTD All rights reserved.");
            obj.style.set_color("#b0b0b0ff");
            obj.style.set_align("center middle");
            obj.style.set_font("8 Dotum");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 550, 235, this.div_init.div_login,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("9");
            		p.style.set_background("transparent");
            		p.style.set_accessibility("disable all '' '' ''");
            		p.set_scrollbars("none");

            	}
            );
            this.div_init.div_login.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1180, 0, this.div_init,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_text("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<UxFormClipBoard version=\"1.5\">\r\n  <CopyComponent>\r\n    <Static id=\"Static02\" taborder=\"5\" text=\"ⓒ VINFLUX company: Corp All Rights Reserved. (ver 2.0)\" left=\"472\" bottom=\"24\" width=\"336\" height=\"13\" style=\"color:#b0b0b0ff;align:center middle;font:8 Dotum;\"/>\r\n  </CopyComponent>\r\n</UxFormClipBoard>\r\n");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_init.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1280, 780, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");
            		p.set_scrollbars("none");
            		p.set_cssclass("sta_Lgn_bg");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Login.xfdl", "lib::Comm.xjs");
        this.registerScript("Login.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Login.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 로그인 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.fn_getLocalStorage = function(key)
        {	
        	if(system.navigatorname == "nexacro"){
        		return application.getPrivateProfile(key);
        	}else{
        		return window.localStorage.getItem(key);
        	}
        }

        this.fn_setLocalStorage = function(key,value)
        {	
        	if(system.navigatorname == "nexacro"){
        		return application.setPrivateProfile(key, value);
        	}else{
        		return window.localStorage.setItem(key, value);
        	}
        }

        this.Login_onload = function(obj,e)
        {
        	var gv_sTime       = this.gfn_isNotNull(this.fn_getLocalStorage("sessionGap"))? this.fn_getLocalStorage("sessionGap"): 30;
        	var gv_ssesionTime = this.gfn_isNotNull(this.fn_getLocalStorage("sessionTime"))? this.fn_getLocalStorage("sessionTime"): 0;
        	var timeGap        = this.gfn_getCheckTime(gv_ssesionTime);
        	
        	if(gv_ssesionTime != 0 && timeGap > gv_sTime){
        		//this.fn_setLocalStorage("LOGIN_FLAG", "");
        	}
        	
        	if(this.fn_getLocalStorage("LOGIN_FLAG") == "Y"){
        		this.sta_back.set_visible(false);
        		this.div_init.set_visible(false);
        		
        		if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        			this.div_init.div_login.chk_idSave.set_value(true);
        			this.div_init.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
        		
        		}
        		
        		application.gds_userInfo.addRow();
        		this.gfn_setUserInfo("urKey"           , this.fn_getLocalStorage("urKey"));
        		this.gfn_setUserInfo("laKey"           , this.fn_getLocalStorage("laKey"));
        		this.gfn_setUserInfo("SESSION_USERINFO", this.fn_getLocalStorage("SESSION_USERINFO"));
        		this.gfn_setUserInfo("ctKey"           , this.fn_getLocalStorage("ctkey"));
        		this.gfn_setUserInfo("urGrKey"         , this.fn_getLocalStorage("urGrKey"));
        		this.gfn_setUserInfo("ctKey_desc"      , this.fn_getLocalStorage("ctKey_desc"));
        		this.gfn_setUserInfo("urName"          , decodeURI(this.fn_getLocalStorage("urName")));
        		this.gfn_setUserInfo("utcMinute"       , this.fn_getLocalStorage("utcMinute"));
        		this.gfn_setUserInfo("lastpwyn"        , this.gfn_isNullEmpty(this.fn_getLocalStorage("lastpwyn")));
        		this.gfn_setUserInfo("utcOffset"       , this.fn_getLocalStorage("utcOffset"));
        		this.gfn_setUserInfo("utcHour"         , this.fn_getLocalStorage("utcHour"));
        		this.gfn_setUserInfo("loggrpcd"        , this.gfn_isNullEmpty(this.fn_getLocalStorage("loggrpcd")));
        		this.gfn_setUserInfo("urCryptoName"    , this.fn_getLocalStorage("urCryptoName"));
        		this.gfn_setUserInfo("owkeym"          , this.fn_getLocalStorage("owkeym"));
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.gfn_setUserInfo("gmtUseYn"        , this.fn_getLocalStorage("gmtUseYn"));
        		
        		this.gfn_setParam("appkey", application.gv_system);
        		this.gfn_setParam("urKey" , this.fn_getLocalStorage("urKey"));
        		this.gfn_setParam("laKey" , this.fn_getLocalStorage("laKey"));
        		this.gfn_setParam("ctkey" , this.fn_getLocalStorage("ctkey"));
        		
        		this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        		this.gfn_setCommon("LAKEY"     , this.fn_getLocalStorage("laKey"));
        		
        		if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        		else this.gfn_setCommon("APKEY", application.gv_system);
        		
        		this.fn_selectCommInfo();
        	}else{
        		this.sta_back.set_visible(true);
        		this.div_init.set_visible(true);
        		
        		var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        		var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        		
        		if(nLeft < 0) nLeft = 0;
        		this.div_init.set_left(nLeft);
        		
        		if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        			this.div_init.div_login.chk_idSave.set_value(true);
        			this.div_init.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
        			this.div_init.div_login.edt_pass.setFocus();
        		}	
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_login = function()
        {
        	application.gds_userInfo.clearData();
        	
        	application.gv_userAuth = "Login";
        	this.gfn_setCommon("BEANID"  , "loginController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectUserInfo";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_userInfo=OUT_USER_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectCommInfo = function()
        {
        	application.gv_userAuth = "Login";
            this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectModuleConfig");
        	
            var sSvcId   = "selectCommInfo";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_menu=OUT_MENU_LIST ds_lang=OUT_appMessageTxt gds_msg=OUT_appMessageMsg gds_systemConfig=OUT_SYSTEM_CONFIG gds_admSystemConfig=OUT_ADM_SYSTEM_CONFIG gds_srchCondition=OUT_SRCH_CONDITION gds_srchCondition2=OUT_SRCH_CONDITION2 gds_commCode=OUT_CODE_LIST gds_userGrid=OUT_USER_GRID gds_userMenu=OUT_USER_MENU gds_eqList=OUT_EQ_LIST gds_rtnMenuObjectRole=OUT_rtnMenuObjectRole gds_rtnAppRole=OUT_rtnAppRole gds_rtnOwnerRole=OUT_rtnOwnerRole gds_rtnWHRole=OUT_rtnWHRole gds_acxur=OUT_ACXUR_LIST";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectSessionInfo = function()
        {
        	application.gv_userAuth = "Login";
        	
        	this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "sessionController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectSessionInfo";
            var sSvcUrl  = this.gfn_getActiveApp().toLowerCase() + application.gv_sUrl;

            var sInData  = "";
            var sOutData = "gds_common=OUT_COMMON gds_userInfo=OUT_USER_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if(nErrCd != 0){
        		if(sSvcId == "selectUserInfo"){
        			this.gfn_alert(this.MESSAGE);
        		}/*else{
        			this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		}*/

        		if(sSvcId == "selectCommInfo" || sSvcId == "selectSessionInfo"){
        			this.sta_back.set_visible(true);
        			this.div_init.set_visible(true);
        			
        			if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        				this.div_init.div_login.chk_idSave.set_value(true);
        				this.div_init.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
        				this.div_init.div_login.edt_pass.setFocus();			
        			}
        		}
                return;
            }
            
            if(sSvcId == "selectUserInfo"){ //process 조회
                if(application.gds_userInfo.rowcount > 0){
        			application.gv_loginStatus = this.gfn_getUserInfo("loginStatus");
        			
        			if(this.div_init.div_login.chk_idSave.value == true){
        				this.fn_setLocalStorage("SAVE_YN", "Y");
        				this.fn_setLocalStorage("SAVE_ID", this.div_init.div_login.edt_id.value);
        			}else{
        				this.fn_setLocalStorage("SAVE_YN", "N");
        				this.fn_setLocalStorage("SAVE_ID", "");
        			}

        			this.gfn_setUserInfo("loggrpcd", this.gfn_isNullEmpty(this.gfn_getUserInfo("loggrpcd")));
        			
        			this.fn_setLocalStorage("appkey"          , application.gv_system);
        			this.fn_setLocalStorage("urKey"           , this.gfn_getUserInfo("urKey"));
        			this.fn_setLocalStorage("laKey"           , this.gfn_getUserInfo("laKey"));
        			this.fn_setLocalStorage("ctkey"           , this.gfn_getUserInfo("ctKey"));
        			this.fn_setLocalStorage("ACTIVE_APP"      , application.gv_activeApp);
        			this.fn_setLocalStorage("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        			this.fn_setLocalStorage("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        			this.fn_setLocalStorage("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        			this.fn_setLocalStorage("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        			this.fn_setLocalStorage("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        			this.fn_setLocalStorage("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        			this.fn_setLocalStorage("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        			this.fn_setLocalStorage("utcHour"         , this.gfn_getUserInfo("utcHour"));
        			this.fn_setLocalStorage("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        			this.fn_setLocalStorage("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        			this.fn_setLocalStorage("owkeym"          , this.gfn_getUserInfo("owkeym"));
        			this.fn_setLocalStorage("gmtUseYn"        , this.gfn_getUserInfo("gmtUseYn"));
        			this.fn_setLocalStorage("LOGIN_FLAG"      , "Y");
        			
        			this.fn_selectCommInfo();
        		}
            }else if(sSvcId == "selectCommInfo"){

                if (application.gv_connectUrl == undefined || application.gv_connectUrl == "") {
                } else {
                    if (application.gv_connectUrl == "N") {
                    
                    } else {
                        var appKey = "";
                        var arrAppKey = new Array();
        	            for (i=0; i<10; i++){
        	              	 arrAppKey.push("");
        	            }    
                    
                        for (var i = 0 ; i < application.gds_rtnAppRole.rowcount ; i++){
        		             appKey = application.gds_rtnAppRole.getColumn(i, "appKey");
        		             switch(appKey){
        			             case "WM":
        				              arrAppKey[0] = "WMS";
        			             case "TMS":
        			                  arrAppKey[1] = "TMS";
        			             case "ICOM":
        				              arrAppKey[2] = "OMS";
        			             case "IBS":
        				              arrAppKey[3] = "IBS";
        			             case "FIS":
        				              arrAppKey[4] = "FIS";
        			             case "CMS":
        				              arrAppKey[5] = "CMS";
        			             case "KPI":
        				              arrAppKey[6] = "KPI";
        			             case "VIMS":
        				              arrAppKey[7] = "VMS";
        			             case "PTL":
        				              arrAppKey[8] = "PTL";
        			             case "ADMIN":
        				              arrAppKey[9] = "AMS";
        		             }
        	            }   
        	            appKey = "";         
        	            for (i=0; i<10; i++){
        	              	 if (arrAppKey[i] != "") {
        	              	     appKey = arrAppKey[i].toLowerCase() + "/";
        	              	     break;
        	              	 }
        	            }
                        var url = application.services["svc"].url + appKey + "index.html";
                        window.top.location.href = url;
                        return;
                    }    	        
                } 
                
        		var nRow = application.gds_menu.findRowExpr("isseparator == 'Y'");
        		var insertRow = 0;
        		
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.gfn_setUserInfo("gmtUseYn", application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "GMTUSEYN"), "value1"));
        		
        		while(nRow != -1){
        			insertRow = application.gds_menu.insertRow(nRow);
        			application.gds_menu.setColumn(insertRow, "mekey", "9999999999");
        			application.gds_menu.setColumn(insertRow, "mename", "-");
        			application.gds_menu.setColumn(insertRow, "melvl", application.gds_menu.getColumn(insertRow+1, "melvl"));
        			
        			nRow = application.gds_menu.findRowExpr("isseparator == 'Y'", nRow+2);
        		}
        		
        		application.gv_nodataMsg = application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage");
                this.div_init.div_login.edt_pass.set_value("");
                application.gds_lang.clearData();
                
        		var gv_sTime = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "SESSIONTIMEOUT"), "value1");
        		if(this.gfn_isNull(gv_sTime)) gv_sTime = 30;
        		
        		this.fn_setLocalStorage("sessionTime", this.gfn_setCheckTime());
        		this.fn_setLocalStorage("sessionGap", gv_sTime);
                
                for(var i = 0 ; i < this.ds_lang.rowcount ; i++){
        			application.gds_lang.addColumn(this.ds_lang.getColumn(i, "mulaapmsg_hdkey"));
        			
        			if(i == 0){
        				application.gds_lang.addRow();
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}else{
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}
                }
        		
        		for(var i = 0 ; i < application.gds_gridMenu.rowcount ; i++){
        			if(this.gfn_isNotNull(application.gds_gridMenu.getColumn(i, "multiLang"))) application.gds_gridMenu.setColumn(i, "menuNm", application.gds_lang.getColumn(0, application.gds_gridMenu.getColumn(i, "multiLang")));
        		}
        		
        		if(application.gv_activeApp == "AMS"){
        			application.gv_userId = this.gfn_getUserInfo("urKey");
        			application.gv_userNm = this.gfn_getUserInfo("urName");
        			
        			this.fn_windowResize();
        		}else{
        			this.fn_selectSessionInfo();
                }
                
                //메뉴별 버튼 권한 데이터 생성
                var usobxmeObj = {};
        		for(var i=0; i<application.gds_rtnMenuObjectRole.rowcount; i++) {
        			var mekey = application.gds_rtnMenuObjectRole.getColumn(i, "menuKey");
        			var usobkey = application.gds_rtnMenuObjectRole.getColumn(i, "userScrnObjectKey");
        		
        			if (this.gfn_isNull(usobxmeObj[mekey])) {
        				var arr = new Array();
        				arr.push(usobkey);
        				usobxmeObj[mekey] = arr;
        			} else {
        				usobxmeObj[mekey].push(usobkey);
        			}
        		}
        		application.gv_usobAuth = usobxmeObj;
                
            }else if(sSvcId == "selectSessionInfo"){
         
        		application.gv_userId = this.gfn_getUserInfo("urKey");
        		application.gv_userNm = this.gfn_getUserInfo("urName");
        		
        		this.fn_setLocalStorage("appkey"          , application.gv_system);
        		this.fn_setLocalStorage("urKey"           , this.gfn_getUserInfo("urKey"));
        		this.fn_setLocalStorage("laKey"           , this.gfn_getUserInfo("laKey"));
        		this.fn_setLocalStorage("ctkey"           , this.gfn_getUserInfo("ctKey"));
        		this.fn_setLocalStorage("ACTIVE_APP"      , application.gv_activeApp);
        		this.fn_setLocalStorage("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        		this.fn_setLocalStorage("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        		this.fn_setLocalStorage("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        		this.fn_setLocalStorage("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        		this.fn_setLocalStorage("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        		this.fn_setLocalStorage("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        		this.fn_setLocalStorage("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        		this.fn_setLocalStorage("utcHour"         , this.gfn_getUserInfo("utcHour"));
        		this.fn_setLocalStorage("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        		this.fn_setLocalStorage("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        		this.fn_setLocalStorage("owkeym"          , this.gfn_getUserInfo("owkeym"));
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.fn_setLocalStorage("gmtUseYn"        , this.gfn_getUserInfo("gmtUseYn"));
        		
        		this.fn_windowResize();
            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	application.gv_LeftFrame.set_formurl("");
        	application.gv_SubMainFrame.set_formurl("");
        	application.gv_TopFrame.set_formurl("");
        	//alert("↑↑↑↑요기");
        	application.gv_LeftFrame.set_formurl("frame::Left.xfdl");
        	application.gv_SubMainFrame.set_formurl("frame::SubMain.xfdl");
        	application.gv_TopFrame.set_formurl("frame::Top.xfdl");
        	application.gfn_setFrame("M");

        //	this.gfn_setLocale(application); // 시스템 언어 설정
        	
        	if(system.navigatorname == "nexacro"){
        		var nIndex = system.getMonitorIndex(application.mainframe.left, application.mainframe.top);
        		var sSize = system.getScreenResolution(nIndex).split(" ");
        		
        // 		application.mainframe.move(0, 0);
        // 		application.mainframe.set_width(sSize[0]);
        // 		application.mainframe.set_height(sSize[1]);
        	}
        		
        }

        
        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* 로그인 Button 실행 */
        this.div_login_btn_login_onclick = function(obj,e)
        {
            if(this.gfn_isNull(this.div_init.div_login.edt_id.value)){
        		this.gfn_alert("ID 를 입력하세요.", "", function(msg, flag){
        			this.div_init.div_login.edt_id.setFocus();
        		});
            }else if(this.gfn_isNull(this.div_init.div_login.edt_pass.value)){
        		this.gfn_alert("PASSWORD 를 입력하세요.", "", function(msg, flag){
        			this.div_init.div_login.edt_pass.setFocus();
        		});
            }else{
        		this.gfn_setParam("appkey", application.gv_system);
        		this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        		
        		if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        		else this.gfn_setCommon("APKEY", application.gv_system);
        		
        		this.gfn_setParam("urKey", this.div_init.div_login.edt_id.value);
        		this.gfn_setParam("urPw" , this.div_init.div_login.edt_pass.value);
        		this.gfn_setParam("laKey", this.div_init.div_login.cbo_lang.value);
        		this.gfn_setCommon("LAKEY", this.div_init.div_login.cbo_lang.value);
        		
        		this.fn_login();
            }
        }

        /* div_login_edt_pass_onkeydown 실행 */
        this.div_login_edt_pass_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.div_init.div_login.btn_login.click();
        }

        /* frame_login_onkeydown 실행 */
        this.frame_login_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        /* frame_login_onsize 실행 */
        this.frame_login_onsize = function(obj,e)
        {
        	var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        	var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        	
        	if(nLeft < 0) nLeft = 0;
        	this.div_init.set_left(nLeft);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.Login_onload, this);
            this.addEventHandler("onkeydown", this.frame_login_onkeydown, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.div_init.div_login.edt_id.addEventHandler("onkeyup", this.div_login_edt_id_onkeyup, this);
            this.div_init.div_login.edt_pass.addEventHandler("onkeydown", this.div_login_edt_pass_onkeydown, this);
            this.div_init.div_login.btn_login.addEventHandler("onclick", this.div_login_btn_login_onclick, this);
            this.div_init.div_login.chk_idSave.addEventHandler("onclick", this.div_login_chk_idSave_onclick, this);

        };

        this.loadIncludeScript("Login.xfdl", true);

       
    };
}
)();
