﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("TopForm");
                this.set_classname("frame_top");
                this.set_scrollbars("none");
                this.set_dragscrolltype("both");
                this._setFormPosition(0,0,1232,70);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_center", this);
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"name\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_menu", this);
            obj._setContents("<ColumnInfo><Column id=\"mename\" type=\"STRING\" size=\"32\"/><Column id=\"meorder\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"melvl\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"usname\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"uskey\" type=\"STRING\" size=\"32\"/><Column id=\"USEYN\" type=\"STRING\" size=\"32\"/><Column id=\"mekey\" type=\"STRING\" size=\"32\"/><Column id=\"nxpath\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"uspath\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"eqtype\" type=\"STRING\" size=\"32\"/><Column id=\"isseparator\" type=\"STRING\" size=\"32\"/><Column id=\"uppermekey\" type=\"STRING\" size=\"32\"/><Column id=\"apkey\" type=\"STRING\" size=\"32\"/><Column id=\"isleaf\" type=\"STRING\" size=\"32\"/><Column id=\"mename_eng\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows/>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("sta_back", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("34");
            obj.set_cssclass("sta_TF_back");
            this.addChild(obj.name, obj);

            obj = new Button("btn_menu_my", "absolute", null, "11", "75", "28", "20", null, this);
            obj.set_taborder("31");
            obj.set_cssclass("btn_TF_Tfavorite");
            obj.style.set_color("#ffffc8ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_menu_all", "absolute", null, "11", "75", "28", "97", null, this);
            obj.set_taborder("32");
            obj.set_cssclass("btn_TF_Tmenu2");
            this.addChild(obj.name, obj);

            obj = new Static("sta_menu", "absolute", "0", "38", null, "32", "0", null, this);
            obj.set_taborder("35");
            obj.set_cssclass("sta_TF_menu");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer00", "absolute", "32", "0", "100", "38", null, null, this);
            obj.set_taborder("6");
            obj.set_image("URL('img::T_logo_eugene.png')");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #e5e5e5ff");
            this.addChild(obj.name, obj);

            obj = new Static("sta_moduleName", "absolute", "135", "11", "44", "17", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("sta_TF_module");
            this.addChild(obj.name, obj);

            obj = new Div("div_menu_my", "absolute", "81", "37", null, "33", "0", null, this);
            obj.set_taborder("27");
            obj.style.set_background("transparent");
            obj.style.set_bordertype("normal 3 3");
            obj.set_scrollbars("none");
            obj.set_visible("false");
            this.addChild(obj.name, obj);
            obj = new Menu("mnu_myMenu", "absolute", "0", "0", null, null, "0", "0", this.div_menu_my);
            this.div_menu_my.addChild(obj.name, obj);
            obj.set_taborder("0");
            obj.set_innerdataset("@gds_userMenu");
            obj.set_idcolumn("mekey");
            obj.set_captioncolumn("mename");
            obj.set_levelcolumn("melvl");
            obj.set_cssclass("meu_TF");
            obj.set_userdatacolumn("isleaf");

            obj = new Button("btn_home", "absolute", "36", "44", "20", "20", null, null, this);
            obj.set_taborder("28");
            obj.set_cssclass("btn_TF_home");
            this.addChild(obj.name, obj);

            obj = new Div("div_menu_all", "absolute", "81", "37", null, "33", "0", null, this);
            obj.set_taborder("26");
            obj.style.set_background("transparent");
            obj.style.set_bordertype("normal 3 3");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Menu("mnu_menu", "absolute", "0", "0", null, null, "0", "0", this.div_menu_all);
            this.div_menu_all.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@gds_menu");
            obj.set_idcolumn("mekey");
            obj.set_captioncolumn("mename");
            obj.set_levelcolumn("melvl");
            obj.set_cssclass("meu_TF");
            obj.set_userdatacolumn("isleaf");

            obj = new Div("div_info", "absolute", "202", "0", null, "37", "184", null, this);
            obj.set_taborder("30");
            obj.style.set_background("transparent");
            this.addChild(obj.name, obj);
            obj = new Button("btn_logout", "absolute", null, "10", "20", "20", "24", null, this.div_info);
            obj.set_taborder("0");
            obj.set_cssclass("btn_TF_logout");
            obj.set_tooltiptext("Logout");
            this.div_info.addChild(obj.name, obj);
            obj = new Button("btn_changePw", "absolute", null, "10", "20", "20", "49", null, this.div_info);
            obj.set_taborder("3");
            obj.set_cssclass("btn_TF_set");
            obj.set_tooltiptext("User Info");
            this.div_info.addChild(obj.name, obj);
            obj = new Button("Button04", "absolute", null, "10", "20", "20", "2", null, this.div_info);
            obj.set_taborder("4");
            obj.set_cssclass("btn_TF_infor");
            obj.set_visible("false");
            this.div_info.addChild(obj.name, obj);
            obj = new Button("btn_tema", "absolute", null, "10", "20", "20", "75", null, this.div_info);
            obj.set_taborder("5");
            obj.set_cssclass("btn_TF_skin");
            this.div_info.addChild(obj.name, obj);
            obj = new Static("Static01", "absolute", null, "0", "1", "37", "103", null, this.div_info);
            obj.set_taborder("6");
            obj.set_text("Static01");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("0 solid #a2a2a2ff");
            obj.style.set_opacity("15");
            this.div_info.addChild(obj.name, obj);
            obj = new Static("sta_userName", "absolute", null, "13", "163", "15", "110", null, this.div_info);
            obj.set_taborder("7");
            obj.set_text("관리자");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_TF_name");
            obj.style.set_align("left middle");
            this.div_info.addChild(obj.name, obj);
            obj = new Static("Static03", "absolute", null, "0", "1", "37", "298", null, this.div_info);
            obj.set_taborder("8");
            obj.set_text("Static01");
            obj.style.set_background("#ffffffff");
            obj.style.set_border("0 solid #a2a2a2ff");
            obj.style.set_opacity("15");
            this.div_info.addChild(obj.name, obj);
            obj = new ImageViewer("ImageViewer01", "absolute", null, "13", "15", "15", "276", null, this.div_info);
            obj.set_taborder("9");
            obj.set_image("URL('img::T_ico_user.png')");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #e5e5e5ff");
            obj.style.set_opacity("50");
            this.div_info.addChild(obj.name, obj);
            obj = new Combo("cbo_center", "absolute", null, "8", "130", "22", "458", null, this.div_info);
            this.div_info.addChild(obj.name, obj);
            obj.set_taborder("10");
            obj.set_text("창고선택");
            obj.set_cssclass("cmb_TF");
            obj.set_visible("false");
            obj.set_innerdataset("@ds_center");
            obj.set_datacolumn("name");
            obj.set_codecolumn("code");
            obj = new Edit("edt_menu", "absolute", null, "8", "118", "22", "336", null, this.div_info);
            obj.set_taborder("15");
            obj.set_cssclass("edt_TF_search");
            this.div_info.addChild(obj.name, obj);
            obj = new Button("btn_menuPop", "absolute", null, "8", "24", "22", "312", null, this.div_info);
            obj.set_taborder("16");
            obj.set_cssclass("btn_TF_search");
            this.div_info.addChild(obj.name, obj);

            obj = new PopupDiv("popDiv_menuSuggest", "absolute", "383", "57", "180", "163", null, null, this);
            obj.set_text("popDiv_menuSuggest");
            obj.style.set_background("#333333ff");
            obj.style.set_border("1 solid #b1b1b1ff");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_menu", "absolute", "0", "0", null, null, "0", "0", this.popDiv_menuSuggest);
            obj.set_taborder("0");
            obj.set_binddataset("ds_menu");
            obj.style.set_padding("0 5 0 5");
            obj.style.set_background("#000000ff");
            obj.style.set_color("#666666ff");
            obj.set_scrollbars("autovert");
            obj.set_summarytype("default");
            obj.set_cssclass("grd_TF_seh");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"180\"/><Column size=\"0\"/></Columns><Rows><Row size=\"24\"/></Rows><Band id=\"body\"><Cell style=\"align:left;\" text=\"bind:mename\"/><Cell col=\"1\" text=\"bind:mekey\"/></Band></Format></Formats>");
            this.popDiv_menuSuggest.addChild(obj.name, obj);
            obj = new Static("sta_noDataFound", "absolute", "5%", "3", null, "22", "5%", null, this.popDiv_menuSuggest);
            obj.set_taborder("1");
            obj.set_text("No Data Found");
            this.popDiv_menuSuggest.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "0", "38", null, "1", "0", null, this);
            obj.set_taborder("36");
            obj.style.set_background("#ffffff15");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("ImageViewer01", "absolute", "80", "38", "1", "32", null, null, this);
            obj.set_taborder("33");
            obj.set_text("ImageViewer01");
            obj.set_image("URL('img::T_menu_line.png')");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #d5d5d5ff");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_copy", "absolute", "95", "137", "73", "0", null, null, this);
            obj.set_taborder("37");
            obj.set_visible("true");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 33, this.div_menu_my,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("27");
            		p.style.set_background("transparent");
            		p.style.set_bordertype("normal 3 3");
            		p.set_scrollbars("none");
            		p.set_visible("false");

            	}
            );
            this.div_menu_my.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 33, this.div_menu_all,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("26");
            		p.style.set_background("transparent");
            		p.style.set_bordertype("normal 3 3");
            		p.set_scrollbars("none");

            	}
            );
            this.div_menu_all.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 37, this.div_info,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("30");
            		p.style.set_background("transparent");

            	}
            );
            this.div_info.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 180, 163, this.popDiv_menuSuggest,
            	//-- Layout function
            	function(p) {
            		p.set_text("popDiv_menuSuggest");
            		p.style.set_background("#333333ff");
            		p.style.set_border("1 solid #b1b1b1ff");

            	}
            );
            this.popDiv_menuSuggest.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1232, 70, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_top");
            		p.set_scrollbars("none");
            		p.set_dragscrolltype("both");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_info.btn_logout","tooltiptext","gds_lang","LOGOUT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_info.btn_changePw","tooltiptext","gds_lang","USER INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_info.btn_tema","tooltiptext","gds_lang","THEMA");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","btn_menu_all","text","gds_lang","MENU");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","btn_menu_my","text","gds_lang","BOOKMARK");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Top.xfdl", "lib::Comm.xjs");
        this.registerScript("Top.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Top.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 탑 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_uCnt = 0;
        this.gv_logoutFlag = false;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.topInit();
        	
        	var index = application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA");
        	this.div_info.edt_menu.set_displaynulltext(application.gds_msg.getColumn(index, "displaymessage"));
        	this.popDiv_menuSuggest.sta_noDataFound.set_text(application.gds_msg.getColumn(index, "displaymessage"));

        	if(system.navigatorname == "nexacro") this.gfn_thema(application.getPrivateProfile("SAVE_TEMA_"+application.gv_activeApp));
        	else this.gfn_thema(window.localStorage.getItem("SAVE_TEMA_"+application.gv_activeApp));
        }

        this.topInit = function(){
        	this.div_menu_all.set_visible(true);
        	this.div_menu_my.set_visible(false);
        	
        	this.div_info.sta_userName.set_text(application.gv_userNm+"("+application.gv_userId+")");

        	if(application.gv_activeApp == ""){
        		application.gv_activeApp = "AMS";
        	}
        	
        	if(application.gv_activeApp == "ICOM"){
        		this.sta_moduleName.set_text("OMS");
        	} else if(application.gv_activeApp == "VIMS"){
        		this.sta_moduleName.set_text("VMS");
        	}else{
        		this.sta_moduleName.set_text(application.gv_activeApp);
        	}

        	if( application.gv_activeApp == "WMS"){
        		
        		var userInfoCtkey = this.gfn_getUserInfo("ctKey");
        		var ctkeyIndex = 0;
        		for(var i = 0 ; i < application.gds_rtnWHRole.rowcount ; i++){
        			this.ds_center.addRow();
        			this.ds_center.setColumn(i, "code", application.gds_rtnWHRole.getColumn(i, "centerKey"));
        			this.ds_center.setColumn(i, "name", application.gds_rtnWHRole.getColumn(i, "centerName"));
        			
        			if(userInfoCtkey == application.gds_rtnWHRole.getColumn(i, "centerKey")){
        				ctkeyIndex = i;
        			}
        		}
        		
        		this.div_info.cbo_center.set_visible(true);
        		this.div_info.cbo_center.set_index(ctkeyIndex);
        	}else{
        		this.div_info.cbo_center.set_visible(false);
        	}

        	application.gds_menu.enableevent = false;
        	application.gds_menu.filter("isleaf == 'Y'");
        	this.ds_menu.copyData(application.gds_menu, true);
        	application.gds_menu.enableevent = true;
        	application.gds_menu.filter("");
        	
        	var openMenu;
        	var sLang = "";
        	var sLangOrg = "";
        	var sReload = "N";
        	
        	if(system.navigatorname == "nexacro"){
        		sLang = application.getPrivateProfile("laKey");
        		sLangOrg = application.getPrivateProfile("logoutLnag");
        		
        		if(this.gfn_isNull(application.getPrivateProfile("openMenu"+application.gv_activeApp))) openMenu = "";
        		else openMenu = application.getPrivateProfile("openMenu"+application.gv_activeApp).split("|");
        		
        		application.setPrivateProfile("openMenu"+application.gv_activeApp, "");
        	}else{
        		sLang = window.localStorage.getItem("laKey");
        		sLangOrg = window.localStorage.getItem("logoutLnag");
        		
        		if(this.gfn_isNull(window.localStorage.getItem("openMenu"+application.gv_activeApp))) openMenu = "";
        		else openMenu = window.localStorage.getItem("openMenu"+application.gv_activeApp).split("|");
        		
        		window.localStorage.setItem("openMenu"+application.gv_activeApp, "");
        	}
        	
        	if(openMenu.length > 0)	application.gfn_setFrame("W");
        	
        	for(var i = 0 ; i < openMenu.length ; i++){
        		if(application.gds_menu.findRow("mekey", openMenu[i]) != -1){
        			if(application.gds_openMenu.findRow("mekey", openMenu[i]) == -1){
        				this.gfn_openMenu(openMenu[i]);
        			}else{
        				if(this.gfn_isNotNull(application.gv_WorkFrame.frames["M_" + openMenu[i]]) && sLang != sLangOrg && this.gfn_isNotNull(sLangOrg)){
        					sReload = "Y";
        				}
        				
        				application.gv_WorkFrame.frames["M_" + openMenu[i]].form.reload();
        			}
        		}
        	}
        	
        	if(sReload == "Y") application.gv_TabFrame.form.fn_reload();
        	
        	if(system.navigatorname == "nexacro"){
        		if(!this.gfn_isNull(application.getPrivateProfile("showMenu"+application.gv_activeApp))) this.gfn_showMenu(application.getPrivateProfile("showMenu"+application.gv_activeApp));
        		application.setPrivateProfile("showMenu"+application.gv_activeApp, "");
        	}else{
        		if(!this.gfn_isNull(window.localStorage.getItem("showMenu"+application.gv_activeApp))) this.gfn_showMenu(window.localStorage.getItem("showMenu"+application.gv_activeApp));
        		window.localStorage.setItem("showMenu"+application.gv_activeApp, "");
        	}
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_logoutAms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "loginController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutAms";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutWms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutWms";
            var sSvcUrl  = application.gv_wms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutTms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutTms";
            var sSvcUrl  = application.gv_tms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutOms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutOms";
            var sSvcUrl  = application.gv_oms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutVms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutVms";
            var sSvcUrl  = application.gv_vms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutMdm = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutMdm";
            var sSvcUrl  = application.gv_mdm + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutFis = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutFis";
            var sSvcUrl  = application.gv_fis + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutIbs = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutIbs";
            var sSvcUrl  = application.gv_ibs + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutCms = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutCms";
            var sSvcUrl  = application.gv_cms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutKpi = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutKpi";
            var sSvcUrl  = application.gv_kpi + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_logoutPtl = function()
        {
        	application.gv_userAuth = "Logout";
        	application.gds_common.setColumn(0, "BEANID"  , "sessionController");
        	application.gds_common.setColumn(0, "METHODNM", "actionLogout");
        	
            var sSvcId   = "logoutPtl";
            var sSvcUrl  = application.gv_ptl + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_saveGrid = function()
        {
        	application.gv_userAuth = "Logout";
        	this.gfn_setCommon("BEANID", "myGridHeaderFomatController");
        	this.gfn_setCommon("METHODNM", "saveMyGridHeaderFormat");
        	
        	var sSvcId   = "saveMyGrid";
        	var sSvcUrl  = this.gfn_getActiveApp() + application.gv_sUrl;
        	//var sSvcUrl  = application.gv_activeApp.toLowerCase() + application.gv_sUrl;
        	var sInData  = "IN_GRID_HEADER_LIST=gds_userGrid:U";
        	var sOutData = "gds_userGrid=OUT_USER_GRID";
        	var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if(nErrCd != 0){
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
                return;
            }
            if(sSvcId == "logoutAms" || sSvcId == "logoutWms" || sSvcId == "logoutTms" || sSvcId == "logoutOms" || sSvcId == "logoutVms" 
        		|| sSvcId == "logoutIbs" || sSvcId == "logoutKpi"){
        		this.gv_logoutFlag = false;
            	if(system.navigatorname == "nexacro"){
            	
        			if(application.getPrivateProfile("SAVE_YN") != "Y") application.gv_LoginFrame.form.div_init.div_login.edt_id.set_value("");
        			application.setPrivateProfile("logoutLnag"      , application.getPrivateProfile("laKey"));
        			application.setPrivateProfile("appkey"          , "");
        			application.setPrivateProfile("urKey"           , "");
        			application.setPrivateProfile("laKey"           , "");
        			application.setPrivateProfile("ctkey"           , "");
        			application.setPrivateProfile("ACTIVE_APP"      , "");
        			application.setPrivateProfile("SESSION_USERINFO", "");
        			application.setPrivateProfile("urGrKey"         , "");
        			application.setPrivateProfile("ctKey_desc"      , "");
        			application.setPrivateProfile("urName"          , "");
        			application.setPrivateProfile("utcMinute"       , "");
        			application.setPrivateProfile("lastpwyn"        , "");
        			application.setPrivateProfile("utcOffset"       , "");
        			application.setPrivateProfile("utcHour"         , "");
        			application.setPrivateProfile("loggrpcd"        , "");
        			application.setPrivateProfile("urCryptoName"    , "");
        			application.setPrivateProfile("owkeym"          , "");
        			application.setPrivateProfile("LOGIN_FLAG"      , "");
               }else{		/* !=nexacro */
        			if(window.localStorage.getItem("SAVE_YN") != "Y") application.gv_LoginFrame.form.div_init.div_login.edt_id.set_value("");
        			window.localStorage.setItem("logoutLnag"      , window.localStorage.getItem("laKey"));
        			window.localStorage.setItem("appkey"          , "");
        			window.localStorage.setItem("urKey"           , "");
        			window.localStorage.setItem("laKey"           , "");
        			window.localStorage.setItem("ctkey"           , "");
        			window.localStorage.setItem("ACTIVE_APP"      , "");
        			window.localStorage.setItem("SESSION_USERINFO", "");
        			window.localStorage.setItem("urGrKey"         , "");
        			window.localStorage.setItem("ctKey_desc"      , "");
        			window.localStorage.setItem("urName"          , "");
        			window.localStorage.setItem("utcMinute"       , "");
        			window.localStorage.setItem("lastpwyn"        , "");
        			window.localStorage.setItem("utcOffset"       , "");
        			window.localStorage.setItem("utcHour"         , "");
        			window.localStorage.setItem("loggrpcd"        , "");
        			window.localStorage.setItem("urCryptoName"    , "");
        			window.localStorage.setItem("owkeym"          , "");
        			window.localStorage.setItem("LOGIN_FLAG"      , "");
                }
                this.gfn_setMenu(application.gv_activeApp);
        		application.gv_loginStatus = "";
        		
                application.gv_LoginFrame.form.sta_back.set_visible(true);
        		application.gv_LoginFrame.form.div_init.set_visible(true);
        		application.gfn_setFrame("L");
               }else if(sSvcId == "logoutWms"){
        		
               
        //     }else if(sSvcId == "logoutTms"){
        //     }else if(sSvcId == "logoutOms"){
        //     }else if(sSvcId == "logoutVms"){
        //     }else if(sSvcId == "logoutMdm"){
        //     }else if(sSvcId == "logoutFis"){
        //     }else if(sSvcId == "logoutIbs"){
        //     }else if(sSvcId == "logoutCms"){
        //     }else if(sSvcId == "logoutKpi"){
        //     }else if(sSvcId == "logoutPtl"){
            }else if(sSvcId == "saveMyGrid"){
        		var nRow = application.gds_userGrid.findRowExpr("delYn == 'Y'");
        		
        		while(nRow != -1){
        			application.gds_userGrid.deleteRow(nRow);
        			nRow = application.gds_userGrid.findRowExpr("delYn == 'Y'");
        		}
        		
        		var appKey = "";
        		if(this.gv_logoutFlag){
        			for(var i = 0 ; i < application.gds_rtnAppRole.rowcount ; i++){
        				
        				appKey = application.gds_rtnAppRole.getColumn(i, "appKey");
        				
        				if(appKey == "ADMIN") this.fn_logoutAms();
        				else if(appKey == "WM") this.fn_logoutWms();
        				else if(appKey == "TMS") this.fn_logoutTms();
         				else if(appKey == "ICOM") this.fn_logoutOms();
         				else if(appKey == "VIMS") this.fn_logoutVms();
         				else if(appKey == "MDM") this.fn_logoutMdm();
         				else if(appKey == "FIS") this.fn_logoutFis();
         				else if(appKey == "CMS") this.fn_logoutCms();
         				else if(appKey == "IBS") this.fn_logoutIbs();
         				else if(appKey == "KPI") this.fn_logoutKpi();
        			}
        		}
            }else if(sSvcId == "selectUserRole"){
        		if(this.div_info.cbo_center.value == null || this.div_info.cbo_center.value == ""){
        			return;
        		}
        		
        		this.gfn_setParam("ctkey", this.div_info.cbo_center.value);
        		this.gfn_setParam("lakey", this.gfn_getUserInfo("laKey"));
        		this.gfn_setParam("apkey", this.gfn_getCommon("APKEY"));
        		
        		this.gfn_setUserInfo("ctKey", this.div_info.cbo_center.value);

        		this.gfn_setCommon("BEANID", "commonController");
        		this.gfn_setCommon("METHODNM", "selectAppMessage");
        		
        		var sSvcId   = "selectAppMessage";
        		var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "";
        		var sParam   = "";
        		
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
            }else if(sSvcId == "selectAppMessage"){
        		if(this.div_info.cbo_center.value == null || this.div_info.cbo_center.value == ""){
        			return;
        		}
        		
        		this.gfn_setParam("ctkey", this.div_info.cbo_center.value);
        		this.gfn_setParam("ctkey_desc", this.div_info.cbo_center.text);

        		this.gfn_setUserInfo("ctKey", this.div_info.cbo_center.value);

        		this.gfn_setCommon("BEANID", "sessionController");
        		this.gfn_setCommon("METHODNM", "changeCenter");
        		
        		var sSvcId   = "changeCenter";
        		var sSvcUrl  = application.gv_wms + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "";
        		var sParam   = "";
        		
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
            }else if(sSvcId == "changeCenter"){
        		if(this.div_info.cbo_center.value == null || this.div_info.cbo_center.value == ""){
        			return;
        		}
        		
        		this.gfn_setParam("ctkey", this.div_info.cbo_center.value);
        		this.gfn_setParam("lakey", this.gfn_getUserInfo("laKey"));
        		this.gfn_setParam("apkey", this.gfn_getCommon("APKEY"));
        		
        		this.gfn_setUserInfo("ctKey", this.div_info.cbo_center.value);
        		
        		application.gv_LoginFrame.form.fn_selectCommInfo();
            }else if(sSvcId == "selectModuleConfig"){
        		
            }else if(sSvcId == "userChangeCenter"){
        		application.gv_LoginFrame.form.fn_selectCommInfo();
            }
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* 개인 그리드 설정
        * @param
        * @return
        */
        this.fn_setGrid = function()
        {
        	var nRow = 0;
        	this.gv_uCnt = 0;
        	var grdCnt = 0;
        	var uskey = "";
        	var grdObj;
        	var divWork;
        	var sOldInitString = ""; // 저장된 초기 데이타
        	var sOldFormatString = ""; // 그리드 초기 데이타
        	var sNewFormatString = ""; // 변경한 그리드 데이타
        	
        // 	if(this.gfn_isNull(application.gds_userGrid.getColumn("delYn"))) application.gds_userGrid.addColumn("delYn");
        // 	
        // 	for(var j = 0 ; j < application.gds_openMenu.rowcount ; j++){
        // 		divWork = application.gv_WorkFrame.frames[application.gds_openMenu.getColumn(j, "WINID")].form.div_work;
        // 		uskey = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", application.gds_openMenu.getColumn(j, "mekey")), "uskey");
        // 		
        // 		if(this.gfn_isNotNull(divWork.gv_grdList)) grdCnt = divWork.gv_grdList.length;
        // 		else grdCnt = 0;
        // 		
        // 		for(var i = 0 ; i < grdCnt ; i++){
        // 			grdObj = divWork.gv_grdList[i];
        // 			sOldFormatString = divWork.ds_grdInit.getColumn(0, grdObj.name + "Init");
        // 			sNewFormatString = grdObj.getCurFormatString();
        // 			
        // 			nRow = application.gds_userGrid.findRowExpr("urKey == '"+this.gfn_getUserInfo("urKey")+"' && usKey == '"+uskey+"' && usgridId == '"+grdObj.name+"'");
        // 			if(nRow != -1) sOldInitString = application.gds_userGrid.getColumn(nRow, "urdfusInfoInit");
        // 			
        // 			if(nRow != -1){
        // 				if(sOldFormatString == sOldInitString){
        // 					if(sNewFormatString != application.gds_userGrid.getColumn(nRow, "urdfusInfo")){
        // 						application.gds_userGrid.setColumn(nRow, "urdfusInfo", grdObj.getCurFormatString());
        // 						uCnt++;
        // 					}
        // 				}else{
        // 					application.gds_userGrid.setColumn(nRow, "delYn", "Y");
        // 					uCnt++;
        // 				}
        // 			}else{
        // 				if(sOldFormatString != sNewFormatString){
        // 					nRow = application.gds_userGrid.addRow();
        // 					application.gds_userGrid.setColumn(nRow, "urKey", this.gfn_getUserInfo("urKey"));
        // 					application.gds_userGrid.setColumn(nRow, "usKey", uskey);
        // 					application.gds_userGrid.setColumn(nRow, "usgridId", grdObj.name);
        // 					application.gds_userGrid.setColumn(nRow, "urdfusInfo", grdObj.getCurFormatString());
        // 					application.gds_userGrid.setColumn(nRow, "urdfusInfoInit", this.div_work.ds_grdInit.getColumn(0, grdObj.name+"Init"));
        // 					
        // 					uCnt++;
        // 				}
        // 			}
        // 		}
        // 	}
        	
        	var appKey = "";
        	if(this.gv_uCnt > 0) this.fn_saveGrid();
        	else if(this.gv_logoutFlag){
        		for(var i = 0 ; i < application.gds_rtnAppRole.rowcount ; i++){
        			appKey = application.gds_rtnAppRole.getColumn(i, "appKey");
        			
        			if(appKey == "ADMIN") this.fn_logoutAms();
        			else if(appKey == "WM") this.fn_logoutWms();
        			else if(appKey == "TMS") this.fn_logoutTms();
        // 			else if(appKey == "ICOM") this.fn_logoutOms();
        // 			else if(appKey == "VIMS") this.fn_logoutVms();
        		}
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_logout_onclick 실행 */
        this.btn_logout_onclick = function(obj,e)
        {
        	this.gfn_confirm("MSG_ALERT_LOGOUT", "", function(msg, flag){
        		if(flag){
        			this.gv_logoutFlag = true;
        			this.fn_saveGrid();
        //			application.exit();
        		}
        	});
        }

        /* div_menu_all_mnu_menu_onmenuclick 실행 */
        this.div_menu_all_mnu_menu_onmenuclick = function(obj,e)
        {
        	if(e.userdata == "Y"){
        		application.gfn_setFrame("W");
        		this.gfn_openMenu(e.id);
        	}
        }

        /* div_menu_my_mnu_myMenu_onmenuclick 실행 */
        this.div_menu_my_mnu_myMenu_onmenuclick = function(obj,e)
        {
        	if(e.userdata == "Y"){
        		application.gfn_setFrame("W");
        		this.gfn_openMenu(e.id);
            }
        }

        /* Menu_men_all_onmenuclick 실행 */
        this.btn_menu_all_onclick = function(obj,e)
        {
        	this.div_menu_all.set_visible(true);
        	this.div_menu_my.set_visible(false);
        	
        	this.btn_menu_all.set_cssclass("btn_TF_Tmenu2");
        	this.btn_menu_my.set_cssclass("btn_TF_Tfavorite");
        // 	this.btn_menu_all.set_cssclass("btn_"+this.gv_tema+"TF_Tmenu2");
        // 	this.btn_menu_my.set_cssclass("btn_"+this.gv_tema+"TF_Tfavorite");
        }

        /* btn_menu_my_onclick 실행 */
        this.btn_menu_my_onclick = function(obj,e)
        {
        	this.div_menu_all.set_visible(false);
        	this.div_menu_my.set_visible(true);
        	
        	this.btn_menu_all.set_cssclass("btn_TF_Tmenu");
        	this.btn_menu_my.set_cssclass("btn_TF_Tfavorite2");
        // 	this.btn_menu_all.set_cssclass("btn_"+this.gv_tema+"TF_Tmenu");
        // 	this.btn_menu_my.set_cssclass("btn_"+this.gv_tema+"TF_Tfavorite2");
        }

        /* btn_home_onclick 실행 */
        this.btn_home_onclick = function(obj,e)
        {
            application.gfn_setFrame("M");
        }

        /* div_info_btn_tema_onclick 실행 */
        this.div_info_btn_tema_onclick = function(obj,e)
        {
        	var nLeft = nexacro.toNumber(application.mainframe.width) - 275;
        	var nTop = 0;
        	
        	if(system.navigatorname == "nexacro") nTop = 58;
        	else nTop = 30;
        		
        	newChild = new nexacro.ChildFrame;
        	newChild.init("ThemaPop", "absolute", nLeft, nTop, 250, 400, null, null, "comm::ThemaPop.xfdl");
        	newChild.set_dragmovetype("all");
        	newChild.set_showtitlebar(false);
        	newChild.set_autosize(false);
        	newChild.set_resizable(false);
        	newChild.set_showstatusbar(false);
        	newChild.showModal(this.getOwnerFrame(), "", this, "", true);
        }

        this.div_info_cbo_center_onitemchanged = function(obj,e)
        {
        	//현재 선택된콤보와 같은 콤보 선택시 패스
        	this.gfn_confirm("MSG_ASK_CENTER_CHANGE", "", function(msg, flag){
        				if(flag){
        					this.changeCenter(msg, flag);
        				}else{
        					this.div_info.cbo_center.set_value(this.gfn_getUserInfo("ctKey"));
        				}
        			});
        }

        this.changeCenter = function(msgId,flag){
        	if(flag){
        		for(var i = application.gv_TabFrame.form.ds_Tab.getRowCount()-1 ; i >= 0 ; i--){
        			
        			application.gv_TabFrame.form.fn_TabOnClose(application.gv_TabFrame.form.ds_Tab.getColumn(i, "WINID"));
        		}
        		
        		application.gds_openMenu.clearData();
        		application.gfn_setFrame("M");

        		if(this.div_info.cbo_center.value == null || this.div_info.cbo_center.value == ""){
        			return;
        		}

        		this.gfn_setParam("ctkey", this.div_info.cbo_center.value);
        		this.gfn_setParam("urKey", this.gfn_getUserInfo("urKey"));
        		this.gfn_setParam("appKey", this.gfn_getCommon("APKEY"));
        		
        		this.gfn_setUserInfo("ctKey", this.div_info.cbo_center.value);
        		this.gfn_setUserInfo("ctKey_desc", this.div_info.cbo_center.text);

        		this.gfn_setCommon("BEANID", "userByRoleAdmController");
        		this.gfn_setCommon("METHODNM", "selectUserRole");
        		
        		var sSvcId   = "userChangeCenter";
        		var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        		var sInData  = "";
        		var sOutData = "gds_userInfo=OUT_USER_INFO";
        		var sParam   = "";
        		
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.div_info_edt_menu_onkeyup = function(obj,e)
        {
        	this.popDiv_menuSuggest_Open();

        	if(e.keycode == 13){
        		var mename = "";
        		var mekey = "";
        		for(var i=0 ; i < this.ds_menu.getRowCount() ; i++){
        			mename = this.ds_menu.getColumn(i, "mename");
        			if(obj.value == mename){
        				mekey = this.ds_menu.getColumn(i, "mekey");
        				break;
        			}
        		}
        		if(mekey != ""){
        			application.gfn_setFrame("W");
        			this.gfn_openMenu(mekey);
        		}

        		this.popDiv_menuSuggest.closePopup();
        		this.div_info.edt_menu.set_text(null);
        		this.div_info.edt_menu.set_value(null);
        		this.ds_menu.filter("");
        	}
        	if(e.keycode == 40){
        		this.popDiv_menuSuggest.grd_menu.setFocus();
        	}
        	// ↓ 40 ↑ 38 엔터 13
        }

        this.div_info_edt_menu_onmouseleave = function(obj,e)
        {
        	if(this.popDiv_menuSuggest.isPopup()){
        		this.popDiv_menuSuggest.closePopup();
        	}
        }

        this.popDiv_menuSuggest_grd_menu_onkeydown = function(obj,e)
        {
        	if(e.keycode == 38){
        		if(obj.getSelectedRows() == 0){
        			this.div_info.edt_menu.setFocus();
        		}
        	}else if(e.keycode == 40){
        		
        	}else if(e.keycode == 13){
        		this.openMenuAndInit(obj);	
        	}else{
        		this.div_info.edt_menu.setFocus();
        	}
        }

        this.TopForm_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        this.popDiv_menuSuggest_grd_menu_oncellclick = function(obj,e)
        {
        	this.openMenuAndInit(obj);
        }

        this.openMenuAndInit = function(gridObj){
        	var mekey = this.ds_menu.getColumn(gridObj.getSelectedRows(), "mekey");
        	this.ds_menu.filter("");
        	
        	application.gfn_setFrame("W");
        	this.gfn_openMenu(mekey);
        	
        	this.popDiv_menuSuggest.closePopup();
        	
        	this.div_info.edt_menu.set_text(null);
        	this.div_info.edt_menu.set_value(null);
        }

        this.div_info_btn_menuPop_onclick = function(obj,e)
        {
        	this.popDiv_menuSuggest_Open();
        }

        this.popDiv_menuSuggest_Open = function()
        {
        	if(!this.popDiv_menuSuggest.isPopup()){

        		var v_nX = system.clientToScreenX(this.div_info.edt_menu, 0) - system.clientToScreenX(application.mainframe, 0);
        		var v_nY = system.clientToScreenY(this.div_info.edt_menu, parseInt(this.div_info.edt_menu.height)) - system.clientToScreenY(application.mainframe, 0) ;

        		this.popDiv_menuSuggest.set_left(v_nX);
        		this.popDiv_menuSuggest.set_top(v_nY);
        		this.popDiv_menuSuggest.trackPopup(v_nX, v_nY);	
        		this.div_info.edt_menu.setFocus();
        	}
        	var gridItemHeight = 24;
        	var gridGap = 5;
        	
        	this.ds_menu.filter("mename.indexOf('"+this.div_info.edt_menu.text+"')>-1");
        	
        	if(this.ds_menu.getRowCount() > 0){
        		this.popDiv_menuSuggest.sta_noDataFound.set_visible(false);

        		var menuListHeight = this.ds_menu.getRowCount()*gridItemHeight+gridGap;
        		var mainFrameHeight = application.mainframe.getOffsetHeight();

        		if(menuListHeight > mainFrameHeight){
        			this.popDiv_menuSuggest.set_height(mainFrameHeight -80);
        		}else{
        			this.popDiv_menuSuggest.set_height(this.ds_menu.getRowCount()*gridItemHeight+gridGap);
        		}
        	}else{
        		this.popDiv_menuSuggest.sta_noDataFound.set_visible(true);
        		this.popDiv_menuSuggest.set_height(gridItemHeight+gridGap);
        	}
        }

        this.div_info_btn_changePw_onclick = function(obj,e)
        {
        	this.gfn_popup("ChangePwPop", "comm::ChangePwPop.xfdl", "", 580, 249, "");
        }

        this.div_info_Button04_onclick = function(obj,e)
        {
        	this.gfn_alert("개발 중인 기능 입니다.");
        	return;	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onkeydown", this.TopForm_onkeydown, this);
            this.btn_menu_my.addEventHandler("onclick", this.btn_menu_my_onclick, this);
            this.btn_menu_all.addEventHandler("onclick", this.btn_menu_all_onclick, this);
            this.sta_moduleName.addEventHandler("onclick", this.sta_moduleName_onclick, this);
            this.div_menu_my.mnu_myMenu.addEventHandler("onmenuclick", this.div_menu_my_mnu_myMenu_onmenuclick, this);
            this.btn_home.addEventHandler("onclick", this.btn_home_onclick, this);
            this.div_menu_all.mnu_menu.addEventHandler("onmenuclick", this.div_menu_all_mnu_menu_onmenuclick, this);
            this.div_info.btn_logout.addEventHandler("onclick", this.btn_logout_onclick, this);
            this.div_info.btn_changePw.addEventHandler("onclick", this.div_info_btn_changePw_onclick, this);
            this.div_info.Button04.addEventHandler("onclick", this.div_info_Button04_onclick, this);
            this.div_info.btn_tema.addEventHandler("onclick", this.div_info_btn_tema_onclick, this);
            this.div_info.cbo_center.addEventHandler("onitemchanged", this.div_info_cbo_center_onitemchanged, this);
            this.div_info.cbo_center.addEventHandler("onmouseenter", this.div_info_cbo_center_onmouseenter, this);
            this.div_info.edt_menu.addEventHandler("onkeyup", this.div_info_edt_menu_onkeyup, this);
            this.div_info.edt_menu.addEventHandler("onmouseleave", this.div_info_edt_menu_onmouseleave, this);
            this.div_info.btn_menuPop.addEventHandler("onclick", this.div_info_btn_menuPop_onclick, this);
            this.popDiv_menuSuggest.grd_menu.addEventHandler("onkeydown", this.popDiv_menuSuggest_grd_menu_onkeydown, this);
            this.popDiv_menuSuggest.grd_menu.addEventHandler("oncelldblclick", this.popDiv_menuSuggest_grd_menu_oncelldblclick, this);
            this.popDiv_menuSuggest.grd_menu.addEventHandler("oncellclick", this.popDiv_menuSuggest_grd_menu_oncellclick, this);

        };

        this.loadIncludeScript("Top.xfdl", true);

       
    };
}
)();
