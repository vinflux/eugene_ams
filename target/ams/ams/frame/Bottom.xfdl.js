﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("BottomForm");
                this.set_classname("TopFrame");
                this.set_dragscrolltype("both");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,1232,26);
            }
            this.style.set_color("#ffffffff");

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static00", "absolute", "0", "0", null, "26", "0", null, this);
            obj.set_taborder("3");
            obj.style.set_background("#f1f1f1ff");
            obj.style.set_border("0 solid #808080");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "0", "7", null, "13", "0", null, this);
            obj.set_taborder("2");
            obj.set_text("Copyright ⓒ EUGENE SUPERFREEZE CO., LTD All Rights reserved.");
            obj.style.set_color("#b0b0b0ff");
            obj.style.set_align("center middle");
            obj.style.set_font("8 Dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_changeSearch", "absolute", null, "0", "85", null, "20", "0", this);
            obj.set_taborder("4");
            obj.set_text("   검색영역");
            obj.set_cssclass("btn_WF_SrhChange");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1232, 26, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("TopFrame");
            		p.set_dragscrolltype("both");
            		p.set_scrollbars("none");
            		p.style.set_color("#ffffffff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Bottom.xfdl", "lib::Comm.xjs");
        this.registerScript("Bottom.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : BottomForm.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 저작권 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_menuId;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_changeSearch_onclick 실행 */
        this.btn_changeSearch_onclick = function(obj,e)
        {
        	var dsTab = application.gv_TabFrame.form;
        	
        	if(this.gfn_isNotNull(dsTab)){
        		for(var i = 0 ; i < dsTab.ds_Tab.rowcount ; i++){
        			TabObj = dsTab.fn_findObj(dsTab.ds_Tab.getColumn(i, "TAB_ID"));
        			
        			if(TabObj.cssclass == "btn_TaF_tab_on"){
        				this.gv_menuId = dsTab.ds_Tab.getColumn(i, "WINID");
        				break;
        			}
        		}
        		
        		var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        		if(this.gfn_isNotNull(oWorkFrame)) oWorkFrame.form.fn_searchChange();
        	}
        }

        this.Static01_onclick = function(obj,e)
        {
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.TabForm_onsize, this);
            this.Static01.addEventHandler("onclick", this.Static01_onclick, this);
            this.btn_changeSearch.addEventHandler("onclick", this.btn_changeSearch_onclick, this);

        };

        this.loadIncludeScript("Bottom.xfdl", true);

       
    };
}
)();
