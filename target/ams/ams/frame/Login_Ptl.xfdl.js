﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("frame_login");
                this.set_classname("frame_login");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,1280,780);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_lang", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_init", "absolute", "0", "0", "1280", null, null, "0", this);
            obj.set_taborder("0");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #b1b1b1ff");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Div("div_cont", "absolute", "0", "16.03%", "1280", "530", null, null, this.div_init);
            obj.set_taborder("0");
            obj.set_text("Div00");
            obj.style.set_align("center middle");
            this.div_init.addChild(obj.name, obj);
            obj = new Div("Div00", "absolute", "0", "50", "1280", "437", null, null, this.div_init.div_cont);
            obj.set_taborder("10");
            obj.style.set_background("URL('img::login_img_eugene.png') stretch");
            this.div_init.div_cont.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "12.97%", "490", "412", null, null, "0", this.div_init.div_cont);
            obj.set_taborder("6");
            obj.set_text("Copyright ⓒ EUGENE SUPERFREEZE CO., LTD All rights reserved.");
            this.div_init.div_cont.addChild(obj.name, obj);
            obj = new Combo("Combo00", "absolute", null, "498", "170", null, "13.98%", "0", this.div_init.div_cont);
            this.div_init.div_cont.addChild(obj.name, obj);
            var Combo00_innerdataset = new Dataset("Combo00_innerdataset", this.div_init.div_cont.Combo00);
            Combo00_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\"/><Col id=\"datacolumn\"> FAMILY SITE</Col></Row></Rows>");
            obj.set_innerdataset(Combo00_innerdataset);
            obj.set_taborder("7");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_text(" FAMILY SITE");
            obj.set_index("0");
            obj = new Div("div_login", "absolute", null, "50", "360", null, "13.98%", "43", this.div_init.div_cont);
            obj.set_taborder("9");
            obj.style.set_opacity("90");
            this.div_init.div_cont.addChild(obj.name, obj);
            obj = new Static("sta_custCenter", "absolute", "35", "380", "290", "38", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("56");
            obj.set_text("<b v='true'>고객센터  1588-0000</b> (평일 09:00-18:00)");
            obj.set_usedecorate("true");
            obj.style.set_align("center middle");
            obj.style.set_font("10 NanumGothic");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Button("btn_login", "absolute", "35", "307", "290", "53", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("6");
            obj.set_text("로그인");
            obj.set_cssclass("btn_Lgn_ok3");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new CheckBox("chk_idSave", "absolute", "37", "255", "113", "22", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("3");
            obj.set_text("ID Save");
            obj.set_cssclass("chk_Lgn_is2");
            obj.style.set_color("#b9b9b9ff");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Edit("edt_pass", "absolute", "35", "201", "290", "38", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("2");
            obj.set_password("true");
            obj.set_displaynulltext("PASSWORD");
            obj.set_cssclass("edt_Lgn_pw");
            obj.style.set_accessibility("enable all '비밀번호' '' ''");
            obj.set_autoselect("true");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Edit("edt_id", "absolute", "35", "150", "290", "38", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("1");
            obj.set_displaynulltext("ID");
            obj.set_cssclass("edt_Lgn_id");
            obj.style.set_accessibility("enable all '아이디' '' ''");
            obj.set_autoselect("true");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Combo("cbo_lang", "absolute", "35", "99", "290", "38", null, null, this.div_init.div_cont.div_login);
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            var cbo_lang_innerdataset = new Dataset("cbo_lang_innerdataset", this.div_init.div_cont.div_login.cbo_lang);
            cbo_lang_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">KOR</Col><Col id=\"datacolumn\">한국어</Col></Row><Row><Col id=\"codecolumn\">ENG</Col><Col id=\"datacolumn\">English</Col></Row><Row><Col id=\"codecolumn\">ZHO</Col><Col id=\"datacolumn\">中文(简体)</Col></Row><Row><Col id=\"codecolumn\">CHI</Col><Col id=\"datacolumn\">中文(繁體)</Col></Row></Rows>");
            obj.set_innerdataset(cbo_lang_innerdataset);
            obj.set_taborder("0");
            obj.set_value("KOR");
            obj.set_text("한국어");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_cssclass("cmb_Lgn_leg");
            obj.set_index("0");
            obj = new ImageViewer("ImageViewer00", "absolute", "35", "35", "129", "38", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("62");
            obj.set_image("URL('img::L_tit_login.png')");
            obj.set_imagealign("left middle");
            obj.style.set_background("transparent");
            obj.style.set_border("0 solid #e5e5e5ff");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Button("sta_idFind", "absolute", "174", "256", "62", "23", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("4");
            obj.set_text("아이디 찾기");
            obj.set_cssclass("btn_Lgn_stext");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Button("sta_passFind", "absolute", "248", "256", "80", "23", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("5");
            obj.set_text("비밀번호 찾기");
            obj.set_cssclass("btn_Lgn_stext");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Button("btn_memberReg", "absolute", "60.83%", "41", null, "29", "9.72%", null, this.div_init.div_cont.div_login);
            obj.set_taborder("7");
            obj.set_text(" 신규회원가입");
            obj.set_cssclass("btn_L_members");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new Static("Static02", "absolute", "243", "260", "1", "15", null, null, this.div_init.div_cont.div_login);
            obj.set_taborder("66");
            obj.set_text("Static02");
            obj.style.set_background("#a3a3a3ff");
            this.div_init.div_cont.div_login.addChild(obj.name, obj);
            obj = new ImageViewer("ImageViewer01", "absolute", "12.97%", "0", "164", "46", null, null, this.div_init.div_cont);
            obj.set_taborder("11");
            obj.set_image("URL('img::L_logo_eugene.png')");
            obj.set_imagealign("left top");
            this.div_init.div_cont.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1280, 437, this.div_init.div_cont.Div00,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("10");
            		p.style.set_background("URL('img::login_img_eugene.png') stretch");

            	}
            );
            this.div_init.div_cont.Div00.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 360, 0, this.div_init.div_cont.div_login,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("9");
            		p.style.set_opacity("90");

            	}
            );
            this.div_init.div_cont.div_login.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1280, 530, this.div_init.div_cont,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_text("Div00");
            		p.style.set_align("center middle");

            	}
            );
            this.div_init.div_cont.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1280, 780, this.div_init,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.style.set_background("transparent");
            		p.style.set_border("0 solid #b1b1b1ff");
            		p.set_scrollbars("none");

            	}
            );
            this.div_init.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1280, 780, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_login");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_init.div_cont.div_login.btn_login","text","gds_lang","LOGIN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_init.div_cont.div_login.btn_memberReg","text","gds_lang","REG_MEMBER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_init.div_cont.div_login.sta_idFind","text","gds_lang","ID_FIND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_init.div_cont.div_login.sta_passFind","text","gds_lang","PASSWORD_FIND");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Login_Ptl.xfdl", "lib::Comm.xjs");
        this.registerScript("Login_Ptl.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Login.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 로그인 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/
          
        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.fv_loginImgArray = ["login1.jpg","login2.jpg","login3.jpg"];
        this.fv_loginImgIdx   = 1;
        this.fv_chgImg = 5000;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.fn_getLocalStorage = function(key)
        {	
        	if(system.navigatorname == "nexacro"){
        		return application.getPrivateProfile(key);
        	}else{
        		return window.localStorage.getItem(key);
        	}
        }

        this.fn_setLocalStorage = function(key,value)
        {	
        	if(system.navigatorname == "nexacro"){
        		return application.setPrivateProfile(key, value);
        	}else{
        		return window.localStorage.setItem(key, value);
        	}
        }

        this.fn_setTimer = function() {
             this.fv_loginImgIdx  =  ( Math.floor ( Math.random()*10 )+1 ) % 3 ;
         
             this.fn_setLodingImg();
             this.setTimer(1, this.fv_chgImg);
        }
        this.fn_setLodingImg = function() {
             var imgUrl ="URL('img::"+this.fv_loginImgArray[this.fv_loginImgIdx]+"') center";
             
             this.div_init.sta_back.style.set_background( imgUrl );

        }
        this.Login_onload = function(obj,e)
        {
         

        
         this.frame_login_onsize();
         //this.fn_setTimer();

        
        	var gv_sTime       = this.gfn_isNotNull(this.fn_getLocalStorage("sessionGap"))? this.fn_getLocalStorage("sessionGap"): 30;
        	var gv_ssesionTime = this.gfn_isNotNull(this.fn_getLocalStorage("sessionTime"))? this.fn_getLocalStorage("sessionTime"): 0;
        	var timeGap        = this.gfn_getCheckTime(gv_ssesionTime);
        	
        	if(gv_ssesionTime != 0 && timeGap > gv_sTime){
        		this.fn_setLocalStorage("LOGIN_FLAG", "");
        	}
        	if(   application.gv_loginStatus != "Y" ){
        	      this.fn_setLocalStorage("LOGIN_FLAG", "");
        	}
        	if(this.fn_getLocalStorage("LOGIN_FLAG") == "Y"){
        		//this.div_init.sta_back.set_visible(false);
        		this.div_init.set_visible(false);
        		
        		if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        			this.div_init.div_cont.div_login.chk_idSave.set_value(true);
        			this.div_init.div_cont.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
         
        		}
        		
        		application.gds_userInfo.addRow();
        		this.gfn_setUserInfo("urKey"           , this.fn_getLocalStorage("urKey"));
        		this.gfn_setUserInfo("laKey"           , this.fn_getLocalStorage("laKey"));
        		this.gfn_setUserInfo("SESSION_USERINFO", this.fn_getLocalStorage("SESSION_USERINFO"));
        		this.gfn_setUserInfo("ctKey"           , this.fn_getLocalStorage("ctkey"));
        		this.gfn_setUserInfo("urGrKey"         , this.fn_getLocalStorage("urGrKey"));
        		this.gfn_setUserInfo("ctKey_desc"      , this.fn_getLocalStorage("ctKey_desc"));
        		this.gfn_setUserInfo("urName"          , decodeURI(this.fn_getLocalStorage("urName")));
        		this.gfn_setUserInfo("utcMinute"       , this.fn_getLocalStorage("utcMinute"));
        		this.gfn_setUserInfo("lastpwyn"        , this.gfn_isNullEmpty(this.fn_getLocalStorage("lastpwyn")));
        		this.gfn_setUserInfo("utcOffset"       , this.fn_getLocalStorage("utcOffset"));
        		this.gfn_setUserInfo("utcHour"         , this.fn_getLocalStorage("utcHour"));
        		this.gfn_setUserInfo("loggrpcd"        , this.gfn_isNullEmpty(this.fn_getLocalStorage("loggrpcd")));
        		this.gfn_setUserInfo("urCryptoName"    , this.fn_getLocalStorage("urCryptoName"));
        		this.gfn_setUserInfo("owkeym"          , this.fn_getLocalStorage("owkeym"));
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.gfn_setUserInfo("gmtUseYn"        , this.fn_getLocalStorage("gmtUseYn"));
        		

        		this.gfn_setParam("appkey", application.gv_system);
        		this.gfn_setParam("urKey" , this.fn_getLocalStorage("urKey"));
        		this.gfn_setParam("laKey" , this.fn_getLocalStorage("laKey"));
        		this.gfn_setParam("ctkey" , this.fn_getLocalStorage("ctkey"));
        		
        		this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        		this.gfn_setCommon("LAKEY"     , this.fn_getLocalStorage("laKey"));
        		
        		if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        		else this.gfn_setCommon("APKEY", application.gv_system);
        		
        		this.fn_selectCommInfo();
        		
        	}else{

        	     this.fn_selectInit();
        	     
        		if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        			this.div_init.div_cont.div_login.chk_idSave.set_value(true);
        			this.div_init.div_cont.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
         
        		}
        	     
        // 		this.sta_back.set_visible(true);
        // 		this.div_init.set_visible(true);
        // 		
        // 		var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        // 		var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        // 		
        // 		if(nLeft < 0) nLeft = 0;
        // 		this.div_init.set_left(nLeft);
        // 		
        // 		if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        // 			this.div_init.div_login.chk_idSave.set_value(true);
        // 			this.div_init.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
        // 			this.div_init.div_login.edt_pass.setFocus();
        // 		}	
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_login = function()
        {
        	application.gds_userInfo.clearData();
        	
        	application.gv_userAuth = "Login";
        	this.gfn_setCommon("BEANID"  , "loginController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectUserInfo";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_userInfo=OUT_USER_INFO";
            var sParam   = "";
        	
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectCommInfo = function()
        {
         
        	application.gv_userAuth = "Login";
            this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "commonController");
        	this.gfn_setCommon("METHODNM", "selectModuleConfig");
        	
            var sSvcId   = "selectCommInfo"; 
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "gds_menu=OUT_MENU_LIST ds_lang=OUT_appMessageTxt gds_msg=OUT_appMessageMsg gds_systemConfig=OUT_SYSTEM_CONFIG gds_admSystemConfig=OUT_ADM_SYSTEM_CONFIG gds_srchCondition=OUT_SRCH_CONDITION gds_srchCondition2=OUT_SRCH_CONDITION2 gds_commCode=OUT_CODE_LIST gds_userGrid=OUT_USER_GRID gds_userMenu=OUT_USER_MENU gds_eqList=OUT_EQ_LIST gds_rtnMenuObjectRole=OUT_rtnMenuObjectRole gds_rtnAppRole=OUT_rtnAppRole gds_rtnOwnerRole=OUT_rtnOwnerRole gds_rtnWHRole=OUT_rtnWHRole gds_acxur=OUT_ACXUR_LIST";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /********************
        *  초기 페이지로딩시 필요한것 로딩.
        *    1) 다국어
        *    2) 메세지.
        *
        *********************************/
        this.fn_selectInit=function(){
        	 this.ds_lang.clear();
        	 application.gds_lang.clear();
        trace("###############");	     
        	     
        	application.gv_userAuth = "Login"; 
        	this.gfn_setCommon("BEANID"  , "loginMainController");
        	this.gfn_setCommon("METHODNM", "selectInitInfo");
        	
            this.gfn_setParam("lakey", this.div_init.div_cont.div_login.cbo_lang.value); 
            
            var sSvcId   = "selectInitInfo";
            var sSvcUrl  = application.gv_ptl + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_lang=OUT_appMessageTxt gds_msg=OUT_appMessageMsg";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_selectSessionInfo = function()
        {
        	application.gv_userAuth = "Login";
        	
        	this.gfn_setParam("ctkey" , this.gfn_getUserInfo("ctKey"));
        	this.gfn_setCommon("BEANID"  , "sessionController");
        	this.gfn_setCommon("METHODNM", "actionLogin");
        	
            var sSvcId   = "selectSessionInfo";
            var sSvcUrl  = this.gfn_getActiveApp().toLowerCase() + application.gv_sUrl;

            if(this.gfn_getActiveApp().toLowerCase() == "vims"){ 
        		sSvcUrl  = application.gv_vms.toLowerCase() + application.gv_sUrl;
            }
            var sInData  = "";
            var sOutData = "gds_common=OUT_COMMON gds_userInfo=OUT_USER_INFO";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
            if(nErrCd != 0){
        		if(sSvcId == "selectUserInfo"){
        			this.gfn_alert(this.MESSAGE);
        			
        		}else{
        			this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		}

        		if(sSvcId == "selectCommInfo" || sSvcId == "selectSessionInfo"){
        		//	this.sta_back.set_visible(true);
        			this.div_init.set_visible(true);
        			 
        			if(this.fn_getLocalStorage("SAVE_YN") == "Y"){
        				this.div_init.div_cont.div_login.chk_idSave.set_value(true);
        				this.div_init.div_cont.div_login.edt_id.set_value(this.fn_getLocalStorage("SAVE_ID"));
        				this.div_init.div_cont.div_login.edt_pass.setFocus();			
        			}
        			 
        		}
                return;
            }
            if( sSvcId ==   "selectInitInfo"){
        	     for(var i = 0 ; i < this.ds_lang.rowcount ; i++){
        			application.gds_lang.addColumn(this.ds_lang.getColumn(i, "mulaapmsg_hdkey"));
        			
        			if(i == 0){
        				application.gds_lang.addRow();
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}else{
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}
                }
                var sCustCenterInfoTel = this.gfn_getLang( "CUST_CENTER_TEL" ) ;
                var sCustCenterInfoTime = this.gfn_getLang( "CUST_CENTER_TIME" ) ;     
                var sCustCenterInfo  = "<b v='true'>"+sCustCenterInfoTel+"</b> ("+sCustCenterInfoTime+")";         
                this.div_init.div_cont.div_login.sta_custCenter.set_text ( sCustCenterInfo  );
            }
            if(sSvcId == "selectUserInfo"){ //process 조회
                if(application.gds_userInfo.rowcount > 0){
        			this.fn_pass = function(){
        				application.gv_loginStatus = this.gfn_getUserInfo("loginStatus");
        		 
        				if(this.div_init.div_cont.div_login.chk_idSave.value == true){
        					this.fn_setLocalStorage("SAVE_YN", "Y");
        					this.fn_setLocalStorage("SAVE_ID", this.div_init.div_cont.div_login.edt_id.value);
        				}else{
        					this.fn_setLocalStorage("SAVE_YN", "N");
        					this.fn_setLocalStorage("SAVE_ID", "");
        				}

        				this.gfn_setUserInfo("loggrpcd", this.gfn_isNullEmpty(this.gfn_getUserInfo("loggrpcd")));
        				
        				this.fn_setLocalStorage("appkey"          , application.gv_system);
        				this.fn_setLocalStorage("urKey"           , this.gfn_getUserInfo("urKey"));
        				this.fn_setLocalStorage("laKey"           , this.gfn_getUserInfo("laKey"));
        				this.fn_setLocalStorage("ctkey"           , this.gfn_getUserInfo("ctKey"));
        				this.fn_setLocalStorage("ACTIVE_APP"      , application.gv_activeApp);
        				this.fn_setLocalStorage("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        				this.fn_setLocalStorage("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        				this.fn_setLocalStorage("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        				this.fn_setLocalStorage("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        				this.fn_setLocalStorage("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        				this.fn_setLocalStorage("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        				this.fn_setLocalStorage("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        				this.fn_setLocalStorage("utcHour"         , this.gfn_getUserInfo("utcHour"));
        				this.fn_setLocalStorage("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        				this.fn_setLocalStorage("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        				this.fn_setLocalStorage("owkeym"          , this.gfn_getUserInfo("owkeym"));
        				this.fn_setLocalStorage("gmtUseYn"        , this.gfn_getUserInfo("gmtUseYn"));
        				this.fn_setLocalStorage("LOGIN_FLAG"      , "Y");
        				
        				this.fn_selectCommInfo();
        			}
        			if(application.gds_userInfo.getColumn(0,"chgPw") == 'TARGET'){
        					var oArg = {urkey:this.gfn_getUserInfo("urKey")};
        					this.gfn_popup("ChangeLongPasswordPop", "member::ChangeLongPasswordPop.xfdl", oArg, 715, 400, "fn_pass");
        			}else{
        				this.fn_pass();
        			}
        		}

            }else if(sSvcId == "selectCommInfo"){
        		var nRow = application.gds_menu.findRowExpr("isseparator == 'Y'");
        		var insertRow = 0;
        	 
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.gfn_setUserInfo("gmtUseYn", application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "GMTUSEYN"), "value1"));
        		
        		while(nRow != -1){
        			insertRow = application.gds_menu.insertRow(nRow);
        			application.gds_menu.setColumn(insertRow, "mekey", "9999999999");
        			application.gds_menu.setColumn(insertRow, "mename", "-");
        			application.gds_menu.setColumn(insertRow, "melvl", application.gds_menu.getColumn(insertRow+1, "melvl"));
        			
        			nRow = application.gds_menu.findRowExpr("isseparator == 'Y'", nRow+2);
        		}
        		
        		application.gv_nodataMsg = application.gds_msg.getColumn(application.gds_msg.findRow("mulaapmsg_hdkey", "MSG_NO_SEARCHDATA"), "displaymessage");
                this.div_init.div_cont.div_login.edt_pass.set_value("");
                application.gds_lang.clearData();
                
        		var gv_sTime = application.gds_systemConfig.getColumn(application.gds_systemConfig.findRow("key", "SESSIONTIMEOUT"), "value1");
        		if(this.gfn_isNull(gv_sTime)) gv_sTime = 30;
        		
        		this.fn_setLocalStorage("sessionTime", this.gfn_setCheckTime());
        		this.fn_setLocalStorage("sessionGap", gv_sTime);
                
                for(var i = 0 ; i < this.ds_lang.rowcount ; i++){
        			application.gds_lang.addColumn(this.ds_lang.getColumn(i, "mulaapmsg_hdkey"));
        			
        			if(i == 0){
        				application.gds_lang.addRow();
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}else{
        				application.gds_lang.setColumn(0, this.ds_lang.getColumn(i, "mulaapmsg_hdkey"), this.ds_lang.getColumn(i, "displaymessage"));
        			}
                }
        		
        		for(var i = 0 ; i < application.gds_gridMenu.rowcount ; i++){
        			if(this.gfn_isNotNull(application.gds_gridMenu.getColumn(i, "multiLang"))) application.gds_gridMenu.setColumn(i, "menuNm", application.gds_lang.getColumn(0, application.gds_gridMenu.getColumn(i, "multiLang")));
        		}
        		
        		if(application.gv_activeApp == "AMS"){
        			application.gv_userId = this.gfn_getUserInfo("urKey");
        			application.gv_userNm = this.gfn_getUserInfo("urName");
        			
        			this.fn_windowResize();
        		}else{
        			this.fn_selectSessionInfo();
                }
                
                //메뉴별 버튼 권한 데이터 생성
                var usobxmeObj = {};
        		for(var i=0; i<application.gds_rtnMenuObjectRole.rowcount; i++) {
        			var mekey = application.gds_rtnMenuObjectRole.getColumn(i, "menuKey");
        			var usobkey = application.gds_rtnMenuObjectRole.getColumn(i, "userScrnObjectKey");
        		
        			if (this.gfn_isNull(usobxmeObj[mekey])) {
        				var arr = new Array();
        				arr.push(usobkey);
        				usobxmeObj[mekey] = arr;
        			} else {
        				usobxmeObj[mekey].push(usobkey);
        			}
        		}
        		application.gv_usobAuth = usobxmeObj;
                
            }else if(sSvcId == "selectSessionInfo"){
         
        		application.gv_userId = this.gfn_getUserInfo("urKey");
        		application.gv_userNm = this.gfn_getUserInfo("urName");
        		
        		this.fn_setLocalStorage("appkey"          , application.gv_system);
        		this.fn_setLocalStorage("urKey"           , this.gfn_getUserInfo("urKey"));
        		this.fn_setLocalStorage("laKey"           , this.gfn_getUserInfo("laKey"));
        		this.fn_setLocalStorage("ctkey"           , this.gfn_getUserInfo("ctKey"));
        		this.fn_setLocalStorage("ACTIVE_APP"      , application.gv_activeApp);
        		this.fn_setLocalStorage("SESSION_USERINFO", this.gfn_getUserInfo("SESSION_USERINFO"));
        		this.fn_setLocalStorage("urGrKey"         , this.gfn_getUserInfo("urGrKey"));
        		this.fn_setLocalStorage("ctKey_desc"      , this.gfn_getUserInfo("ctKey_desc"));
        		this.fn_setLocalStorage("urName"          , encodeURI(this.gfn_getUserInfo("urName")));
        		this.fn_setLocalStorage("utcMinute"       , this.gfn_getUserInfo("utcMinute"));
        		this.fn_setLocalStorage("lastpwyn"        , this.gfn_getUserInfo("lastpwyn"));
        		this.fn_setLocalStorage("utcOffset"       , this.gfn_getUserInfo("utcOffset"));
        		this.fn_setLocalStorage("utcHour"         , this.gfn_getUserInfo("utcHour"));
        		this.fn_setLocalStorage("loggrpcd"        , this.gfn_getUserInfo("loggrpcd"));
        		this.fn_setLocalStorage("urCryptoName"    , this.gfn_getUserInfo("urCryptoName"));
        		this.fn_setLocalStorage("owkeym"          , this.gfn_getUserInfo("owkeym"));
        		//2018.08.03 - ksh : gmt사용 옵션 처리를 위한 추가
        		this.fn_setLocalStorage("gmtUseYn"        , this.gfn_getUserInfo("gmtUseYn"));
        		
        		this.fn_windowResize();
            }
        }

        /***********************************************************************************
        * User Function
        ***********************************************************************************/

        
        /* 화면 리사이즈
        * @return
        * @param
        */
        this.fn_windowResize = function()
        {
        	application.gv_LeftFrame.set_formurl("");
        	application.gv_SubMainFrame.set_formurl("");
        	application.gv_TopFrame.set_formurl("");
        	//alert("↑↑↑↑요기");
        	application.gv_LeftFrame.set_formurl("frame::Left.xfdl");
        	application.gv_SubMainFrame.set_formurl("frame::SubMain_Ptl.xfdl");
        	application.gv_TopFrame.set_formurl("frame::Top_Ptl.xfdl");
        	application.gfn_setFrame("M");

        //	this.gfn_setLocale(application); // 시스템 언어 설정
        	
        	if(system.navigatorname == "nexacro"){
        		var nIndex = system.getMonitorIndex(application.mainframe.left, application.mainframe.top);
        		var sSize = system.getScreenResolution(nIndex).split(" ");
        		
        // 		application.mainframe.move(0, 0);
        // 		application.mainframe.set_width(sSize[0]);
        // 		application.mainframe.set_height(sSize[1]);
        	}
        		
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* 로그인 Button 실행 */
        this.div_login_btn_login_onclick = function(obj,e)
        {
        this.killTimer(1);

        
            if(this.gfn_isNull(this.div_init.div_cont.div_login.edt_id.value)){
        		this.gfn_alert("ID 를 입력하세요.", "", function(msg, flag){
        			this.div_init.div_cont.div_login.edt_id.setFocus();
        		});
            }else if(this.gfn_isNull(this.div_init.div_cont.div_login.edt_pass.value)){
        		this.gfn_alert("PASSWORD 를 입력하세요.", "", function(msg, flag){
        			this.div_init.div_cont.div_login.edt_pass.setFocus();
        		});
            }else{
        		this.gfn_setParam("appkey", application.gv_system);
        		this.gfn_setCommon("ACTIVE_APP", application.gv_activeApp);
        		
        		if(application.gv_activeApp == "WMS") this.gfn_setCommon("APKEY", application.gv_activeApp);
        		else this.gfn_setCommon("APKEY", application.gv_system);
        		
        		this.gfn_setParam("urKey", this.div_init.div_cont.div_login.edt_id.value);
        		this.gfn_setParam("urPw" , this.div_init.div_cont.div_login.edt_pass.value);
        		this.gfn_setParam("laKey", this.div_init.div_cont.div_login.cbo_lang.value);
        		this.gfn_setCommon("LAKEY", this.div_init.div_cont.div_login.cbo_lang.value);
        		
        		this.fn_login();
            }
        }

        /* div_login_edt_pass_onkeydown 실행 */
        this.div_login_edt_pass_onkeydown = function(obj,e)
        {
        	if(e.keycode == "13") this.div_init.div_cont.div_login.btn_login.click();
        }

        /* frame_login_onkeydown 실행 */
        this.frame_login_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        /* frame_login_onsize 실행 */
        this.frame_login_onsize= function(obj,e)
        {

        	var nLeft = nexacro.round(this.getOffsetWidth()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
        	var nTop = nexacro.round(this.getOffsetHeight()/2) - nexacro.round(this.div_init.getOffsetWidth()/2);
         
        	if(nLeft < 0) nLeft = 0;
        	this.div_init.set_left(nLeft);
        } 
        this.frame_login_ontimer = function(obj,e)
         {
            if ( e.timerid == 1 ) {
            
            
                   ++this.fv_loginImgIdx;
                   if( this.fv_loginImgIdx -1 >= this.fv_loginImgArray.length-1 ){
                        this.fv_loginImgIdx  = 0;
                   }
                   
                     
                   
                   this.fn_setLodingImg();           	
            }
        }

        
        this.div_init_div_login_btn_memberReg_onclick = function(obj,e)
        {
        	var oArg = { };
        	this.gfn_popup("memberRegAgreePopup", "member::memberRegAgreePopup.xfdl", oArg, 800, 600, "");
        }

         
         
        this.cbo_lang_onitemchanged = function(obj,e)
        {
        	this.fn_selectInit();
        }
        /* 아이디 찾기 */
        this.sta_idFind_onclick = function(obj,e)
        {
        return;
        	var oArg = { };
        	this.gfn_popup("memberRegIdFindPopup", "member::memberRegIdFindPop.xfdl", oArg, 711, 208, "");
        }
        /* 비번 찾기 */
        this.sta_passFind_onclick = function(obj,e)
        {
        return;
        	var oArg = {}; 
        	this.gfn_popup("memberRegPassFindPopup", "member::memberRegPassFindPop.xfdl", oArg, 711, 208, "");
        	
        }

        this.div_login_edt_id_onkeyup = function(obj,e)
        {
        	if ( e.keycode == 13 ) {
        	    var  objComponent = this.getNextComponent(obj);
                objComponent.setFocus();
        	}
        }

        this.div_init_div_cont_div_login_chk_idSave_onclick = function(obj,e)
        {
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.Login_onload, this);
            this.addEventHandler("onkeydown", this.frame_login_onkeydown, this);
            this.addEventHandler("onsize", this.frame_login_onsize, this);
            this.addEventHandler("ontimer", this.frame_login_ontimer, this);
            this.div_init.div_cont.div_login.btn_login.addEventHandler("onclick", this.div_login_btn_login_onclick, this);
            this.div_init.div_cont.div_login.chk_idSave.addEventHandler("onclick", this.div_init_div_cont_div_login_chk_idSave_onclick, this);
            this.div_init.div_cont.div_login.edt_pass.addEventHandler("onkeydown", this.div_login_edt_pass_onkeydown, this);
            this.div_init.div_cont.div_login.edt_id.addEventHandler("onkeyup", this.div_login_edt_id_onkeyup, this);
            this.div_init.div_cont.div_login.cbo_lang.addEventHandler("onitemchanged", this.cbo_lang_onitemchanged, this);
            this.div_init.div_cont.div_login.sta_idFind.addEventHandler("onclick", this.sta_idFind_onclick, this);
            this.div_init.div_cont.div_login.sta_passFind.addEventHandler("onclick", this.sta_passFind_onclick, this);
            this.div_init.div_cont.div_login.btn_memberReg.addEventHandler("onclick", this.div_init_div_login_btn_memberReg_onclick, this);

        };

        this.loadIncludeScript("Login_Ptl.xfdl", true);

       
    };
}
)();
