﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("WorkForm");
                this.set_classname("frame_top");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,1230,670);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_work", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("0");
            obj.set_text("Div00");
            this.addChild(obj.name, obj);
            obj = new WebBrowser("WebBrowser00", "absolute", "0", "0", null, null, "0", "0", this.div_work);
            obj.set_taborder("0");
            this.div_work.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_work,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_text("Div00");

            	}
            );
            this.div_work.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 670, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("frame_top");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Work_vms.xfdl", "lib::Comm.xjs");
        this.registerScript("Work_vms.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : XXAMC10005.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 메뉴관리 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_menuId;
        this.gv_menuNm;
        this.gv_navi;
        this.gv_menuParam;
        this.gv_split = "";
        this.gv_searchValue = "L";
        this.viewSpace = 20;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        this.form_onload = function(obj,e)
        {	
        	var sParam = obj.getOwnerFrame().arguments;
            
            if(this.gfn_isNotNull(sParam)){
                this.gv_menuId = obj.getOwnerFrame().arguments["menuId"];        
                this.gv_menuNm = application.gds_menu.getColumn(application.gds_menu.findRow("mekey", this.gv_menuId), "mename");
        //        this.fn_start();
            }	
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_start = function(){

        	this.gfn_setCommon("BEANID", "tableauCertController");
        	this.gfn_setCommon("METHODNM", "doGet");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_las + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	var url = "";
        	if(sSvcId == "select") {
        	}
        }		

        

        this.form_onactivate = function(obj,e)
        {
            this.gfn_showMenu(this.gv_menuId);
        }

        this.WorkForm_onclose = function(obj,e)
        {
        	if(obj == e.fromobject){
        		var nRow = application.gds_openMenu.findRow("WINID", "M_" + this.gv_menuId);
        		
        		if(nRow != -1) application.gds_openMenu.deleteRow(nRow);
        		application.gv_TabFrame.form.fn_delTab(this.gv_menuId);
        		
        		var sMenuId = "";
        		
        		if(application.gds_openMenu.rowcount == 0){
        			application.gfn_setFrame("M");
        		}else if(application.gds_openMenu.rowcount == 1){
        			sMenuId = application.gds_openMenu.getColumn(0,"mekey");
        			this.gfn_showMenu(sMenuId);
        		}else{
        			sMenuId = application.gds_openMenu.getColumn(application.gds_openMenu.rowposition,"mekey");
        			this.gfn_showMenu(sMenuId);
        		}
        		
        		application.gv_TabFrame.form.fn_setActive(application.gv_TabFrame.form.ds_Tab.getColumn(application.gv_TabFrame.form.ds_Tab.findRow("WINID", sMenuId),"TAB_ID"));
        	}
        }

        this.WorkForm_onbeforeclose = function(obj,e)
        {
         	var uCnt = 0;	
         	if(obj == e.fromobject){
         		uCnt = this.gfn_saveStatusMyGridFormat(obj);
         		if(uCnt > 0) application.gv_TopFrame.form.fn_saveGrid();
         	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onclose", this.WorkForm_onclose, this);
            this.addEventHandler("onbeforeclose", this.WorkForm_onbeforeclose, this);

        };

        this.loadIncludeScript("Work_vms.xfdl", true);

       
    };
}
)();
