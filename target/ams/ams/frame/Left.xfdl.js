﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("LeftFrame");
                this.set_classname("LeftFrame");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,48,800);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_Tab", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"TAB_ID\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE\" type=\"STRING\" size=\"256\"/><Column id=\"TITLE_KO\" type=\"STRING\" size=\"256\"/><Column id=\"WINID\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("sta_back", "absolute", "0", "0", "38", null, null, "0", this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_LF_back");
            this.addChild(obj.name, obj);

            obj = new Div("div_Tab", "absolute", "0", "89", "38", null, null, "125", this);
            obj.set_taborder("16");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);

            obj = new Button("btn_PreMdi", "absolute", "0", null, "38", "26", null, "95", this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_LF_up");
            this.addChild(obj.name, obj);

            obj = new Button("btn_NexMdi", "absolute", "0", null, "38", "26", null, "69", this);
            obj.set_taborder("10");
            obj.set_cssclass("btn_LF_down");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_hide", "absolute", "0", "0", "38", "89", null, null, this);
            obj.set_taborder("17");
            obj.set_cssclass("btn_LF_hide");
            this.addChild(obj.name, obj);

            obj = new Button("btn_show", "absolute", "38", "0", "10", null, null, "0", this);
            obj.set_taborder("2");
            obj.set_cssclass("btn_LF_Mshow");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("sta_gubun", "absolute", null, "0", "1", null, "0", "0", this);
            obj.set_taborder("18");
            obj.set_text("Static00");
            obj.style.set_background("#cfcfcfff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 48, 800, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("LeftFrame");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Left.xfdl", "lib::Comm.xjs");
        this.registerScript("Left.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Left.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 패키지 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.FIRST_GAP = 1;
        this.BTN_GAP = -1;
        this.TAB_WIDTH = 38;
        this.TAB_HEIGHT = 66;
        this.STATIC_HEIGHT = 1;

        this.gv_sys = [];
        this.gv_module = [];

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gv_module["ADMIN"] = "AMS";
        	this.gv_module["OMS"]  = "OMS";
        	this.gv_module["WMS"]   = "WMS";
        	this.gv_module["VMS"]  = "VMS";
        	this.gv_module["TMS"]   = "TMS";
        	this.gv_module["FIS"]   = "FIS";
        	this.gv_module["IBS"]   = "IBS";
        	this.gv_module["CMS"]   = "CMS";
        	this.gv_module["KPI"]   = "KPI";
        	//this.gv_module["PTL"]   = "PTL";
        	
        	this.gv_sys["AMS"] = "ADM";
        	this.gv_sys["WMS"] = "WM";
        	this.gv_sys["TMS"] = "TMS";
        	this.gv_sys["OMS"] = "OMS";
        	this.gv_sys["VMS"] = "VMS";
        	this.gv_sys["FIS"] = "FIS";
        	this.gv_sys["IBS"] = "IBS";
        	this.gv_sys["CMS"] = "CMS";
        	this.gv_sys["KPI"] = "KPI";
        	//this.gv_sys["PTL"] = "PTL";
        	
        	this.leftInit();
        }

        /* form 초기화 함수 */
        this.leftInit = function(){
        	var appKey = "";
        	
        //	for(var i = 0 ; i < application.gds_rtnAppRole.rowcount ; i++){
        //		appKey = application.gds_rtnAppRole.getColumn(i, "appKey");
        	
        	// 모듈 정렬 순서 정의
        	var moduleSeq = new Array("ADMIN", "ICOM", "WM", "TMS", "IBS", "KPI", "VIMS");

        	for(var i = 0 ; i < moduleSeq.length; i++){
        		for(var j = 0; j < application.gds_rtnAppRole.rowcount ; j++){
        			if(moduleSeq[i] == application.gds_rtnAppRole.getColumn(j, "appKey")) {
        				appKey = application.gds_rtnAppRole.getColumn(j, "appKey");
        			}
        		}
        		switch(appKey){
        			case "ADMIN":
        			case "TMS":
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "WM":
        				appKey = "WMS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "ICOM":
        				appKey = "OMS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;			
        			case "VIMS":
        				appKey = "VMS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "FIS":
        				appKey = "FIS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "IBS":
        				appKey = "IBS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "CMS":
        				appKey = "CMS";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        			case "KPI":
        				appKey = "KPI";
        				this.fn_addTab(this.gv_module[appKey], appKey);
        				break;
        // 			case "PTL":
        // 				appKey = "PTL";
        // 				this.fn_addTab(this.gv_module[appKey], appKey);
        // 				break;									
        				
        		}
        	}
        }

        this.fn_systemOpen = function(app_id)
        {
        	var url = "";
        	var contenxt = app_id.toLowerCase();
        	
        	if(system.navigatorname == "nexacro"){
        // 		url = "gv_system="    + this.gv_sys[app_id];
        // 		    +",gv_activeApp=" + app_id;
        		system.execNexacro("-S '"+application.services["svc"].url+contenxt+"/"+contenxt+"/start.json' -K 'runtime' -V '" + url + "'");
        	}else{
        		url = application.services["svc"].url + contenxt+"/" + "index.html";
        		system.execBrowser(url);
        	}
        }

        /**
         * 메뉴 tab Button 동적생성
         * @public
         * @param {string} 메뉴키값
         * @param {string} 메뉴명
         * @return
         * @example
         * @memberOf 
         */
        this.fn_addTab = function (tabID,sysID)
        {
        	var nRow = this.ds_Tab.findRow("TAB_ID", "btn_"+tabID);
            
            if (nRow > -1) return nRow;
            
        	nRow = this.ds_Tab.addRow();
        	this.ds_Tab.setColumn(nRow, "TAB_ID", "btn_"+tabID);
            
            this.fn_addTabBtn("btn_"+tabID, sysID);
            this.fn_setTabSpinBtnShow();
            return nRow;
        }

        /**
         * 메뉴 tab 신규 동적 생성
         * @public
         * @param {string} 메뉴키값
         * @param {string} 메뉴명
         * @return
         * @example
         * @memberOf 
         */
        this.fn_addTabBtn = function (tabID,sysID)
        {
        	var tabObj;
            var BtnObj;
            var objTextWidth =  "";

            // Tab 버튼 생성
        	tabObj = new Button();
        	tabObj.init(tabID, "absolute", 0, this.fn_getTop(tabID), this.TAB_WIDTH, this.TAB_HEIGHT, null, null);
        	this.div_Tab.addChild(tabObj.name, tabObj);

        	tabObj.setEventHandler("onclick", this.btn_Tab_OnClick, this);
            tabObj.set_visible(true);
            tabObj.show();
            
        	tabObj.set_tooltiptext(this.gv_module[sysID]);
        	tabObj.set_text(this.gv_module[sysID]);
        	
        	if(application.gv_activeApp == sysID) tabObj.set_cssclass("btn_LF_"+sysID.toLowerCase()+"_selected");
        	else tabObj.set_cssclass("btn_LF_"+sysID.toLowerCase());
        	
        	if(application.gv_activeApp == "AMS" && sysID == "ADMIN") tabObj.set_cssclass("btn_LF_"+sysID.toLowerCase()+"_selected");
        	if(application.gv_activeApp == "VIMS" && sysID == "VMS") tabObj.set_cssclass("btn_LF_"+sysID.toLowerCase()+"_selected");
        	if(application.gv_activeApp == "ICOM" && sysID == "OMS") tabObj.set_cssclass("btn_LF_"+sysID.toLowerCase()+"_selected");
        }

        /**
         *  메뉴 tab onclick event 
         * @public
         * @param  
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_Tab_OnClick = function (obj,e)
        {
        	this.fn_systemOpen(obj.name.substring(4, obj.name.length));
        }

        /**
         * left size조정 
         * @public
         * @param 
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_getTop = function (tabID)
        {
            var curRow = this.ds_Tab.findRow("TAB_ID", tabID);
            if (curRow == 0) return this.FIRST_GAP;

            var prevTab = this.fn_findObj(this.ds_Tab.getColumn(curRow - 1, "TAB_ID"));
            return prevTab.getOffsetBottom() + this.BTN_GAP;
        }

        /**
         * menu tab 찾는 함수
         * @public
         * @param {string} tabID
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_findObj = function (strId)
        {
            return this.div_Tab.components[strId];
        }

        /**
         * 메뉴 Mdi 다음  split버튼 
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_NexMdi_onclick = function (obj,e) 
        {
            this.fn_moveFirst(this.fn_getFirstTabIndex() + 1);
            this.fn_setTabSpinBtnShow();
        }
         
        /**
         * 메뉴 Mdi 이전   split버튼 
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.btn_PreMdi_onclick = function (obj,e)
        {
            this.fn_moveFirst(this.fn_getFirstTabIndex() - 1);
            this.fn_setTabSpinBtnShow();
        }

        /**
         * menu tab 버튼 첫번째 index 가져오는 함수
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_getFirstTabIndex = function ()
        {
            for(var i = 0 ; i < this.ds_Tab.rowcount ; i++){
                var tabID = this.ds_Tab.getColumn(i, "TAB_ID");
                var tabObj = this.fn_findObj(tabID);
                
                if(0 <= tabObj.top){
                    return i;
                }
            }
            
            return -1;
        }

        /**
         * menu tab 버튼 첫번째 이동 함수
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_moveFirst = function (nMoveIdx)
        {
            var nIndex;
            var tabID;
            var tabObj;
            var btnObj;
            var tabFirstObj;

            nIndex = this.fn_getFirstTabIndex();
            
            if (nIndex < 0){
                return;
            }else if (nMoveIdx < 0){
                return;
            }else if (nMoveIdx > this.ds_Tab.rowcount){
                return;
            }

            tabID = this.ds_Tab.getColumn(nIndex, "TAB_ID");
            var tabFirstObj = this.fn_findObj(tabID);

            tabID = this.ds_Tab.getColumn(nMoveIdx, "TAB_ID");
            tabObj = this.fn_findObj(tabID);

            var nShiftPos = tabObj.getOffsetTop() - tabFirstObj.getOffsetTop();

            for (var i = 0; i < this.ds_Tab.rowcount; i++){
                tabID = this.ds_Tab.getColumn(i, "TAB_ID");
                tabObj = this.fn_findObj(tabID);
                tabObj.move(tabObj.getOffsetLeft(), tabObj.getOffsetTop() - nShiftPos);
            }
        }

        /**
         * mdi spin 버튼 visible 처리
         * @public
         * @return 
         * @example
         * @memberOf 
         */
        this.fn_setTabSpinBtnShow = function ()
        {
        	var tabObj;
            
            if(this.ds_Tab.rowcount == 0){
                this.btn_PreMdi.set_visible(false);
                this.btn_NexMdi.set_visible(false);
                return;
            }

            tabObj = this.fn_findObj(this.ds_Tab.getColumn(this.ds_Tab.rowcount - 1, "TAB_ID"));

            if(this.div_Tab.getOffsetHeight() < tabObj.getOffsetBottom()){
                this.btn_NexMdi.set_visible(true);
            }else{
                this.btn_NexMdi.set_visible(false);
            }

            tabObj = this.fn_findObj(this.ds_Tab.getColumn(0, "TAB_ID"));

            if(tabObj.getOffsetTop() < 0){
                this.btn_PreMdi.set_visible(true);
            }else{
                this.btn_PreMdi.set_visible(false);
            }
        }

        /* LeftFrame_onsize */
        this.LeftFrame_onsize = function(obj,e)
        {
        	this.fn_setTabSpinBtnShow();
        }

        /* LeftFrame_onkeydown */
        this.LeftFrame_onkeydown = function(obj,e)
        {
        	if(obj == e.fromobject){
        		if(e.ctrlKey && e.shiftKey && e.keycode == 68 && application.gv_sServer == "L"){
        			this.gfn_showDegug();
        		}
        	}
        }

        this.btn_hide_onclick = function(obj,e)
        {
        	application.gv_showHideYn = "N";
        	application.gv_HFrameSet.set_separatesize("10,*");
        	this.btn_show.set_left(0);
        	this.btn_show.set_visible(true);
        }

        this.btn_show_onclick = function(obj,e)
        {
        	application.gv_showHideYn = "Y";
        	application.gv_HFrameSet.set_separatesize("48,*");
        	this.btn_show.set_left(38);
        	this.btn_show.set_visible(false);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.LeftFrame_onsize, this);
            this.addEventHandler("onkeydown", this.LeftFrame_onkeydown, this);
            this.btn_PreMdi.addEventHandler("onclick", this.btn_PreMdi_onclick, this);
            this.btn_NexMdi.addEventHandler("onclick", this.btn_NexMdi_onclick, this);
            this.btn_hide.addEventHandler("onclick", this.btn_hide_onclick, this);
            this.btn_show.addEventHandler("onclick", this.btn_show_onclick, this);

        };

        this.loadIncludeScript("Left.xfdl", true);

       
    };
}
)();
