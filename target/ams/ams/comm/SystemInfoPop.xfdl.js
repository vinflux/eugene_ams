﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("SystemInfoPop");
                this.set_classname("style01");
                this.set_titletext("메뉴 화면 시스템 정보");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,248);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "9", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "35", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "9", "26", "20", "22", null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "44", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "107", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "107", null, "31", "22", null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "76", null, "31", "22", null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "263", "12", this);
            obj.set_taborder("8");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "76", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "138", null, "31", "22", null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "138", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "45", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "45", null, "31", "22", null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_mekey", "absolute", "222", "142", null, "22", "37", null, this);
            obj.set_taborder("4");
            obj.set_maxlength("0");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_title", "absolute", "222", "49", null, "22", "37", null, this);
            obj.set_taborder("251");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_appname", "absolute", "222", "80", null, "22", "37", null, this);
            obj.set_taborder("252");
            obj.set_maxlength("0");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_path", "absolute", "222", "111", null, "22", "37", null, this);
            obj.set_taborder("253");
            obj.set_maxlength("0");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "169", null, "31", "22", null, this);
            obj.set_taborder("254");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "169", "194", "31", null, null, this);
            obj.set_taborder("255");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "22", null, null, "1", "22", "48", this);
            obj.set_taborder("256");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_remark", "absolute", "222", "173", null, "22", "37", null, this);
            obj.set_taborder("257");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 248, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("메뉴 화면 시스템 정보");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","MENU SCRREEN SYSTEM  INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","APKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","USPATH");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","MENAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","MEKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","static06","text","gds_lang","REMARK");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("SystemInfoPop.xfdl", "lib::Comm.xjs");
        this.registerScript("SystemInfoPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : SystemInfoPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 메뉴 화면 시스템 정보
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_menuId = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            
            var nRow = application.gds_menu.findRowExpr("mekey == '" + this.gv_menuId + "'");
            this.edt_title.set_value(application.gds_menu.getColumn(nRow, "mename"));
            this.edt_appname.set_value(application.gds_menu.getColumn(nRow, "apkey"));
            this.edt_path.set_value(application.gds_menu.getColumn(nRow, "uspath"));
            this.edt_mekey.set_value(application.gds_menu.getColumn(nRow, "mekey"));
            this.edt_remark.set_value(application.gds_menu.getColumn(nRow, "remark"));
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("SystemInfoPop.xfdl", true);

       
    };
}
)();
