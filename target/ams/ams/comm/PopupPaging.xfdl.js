﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("PopupPaging");
                this.set_classname("style01");
                this.set_titletext("페이징 팝업용");
                this._setFormPosition(0,0,1165,29);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("paging", "absolute", "0", "0", "100%", "29", null, null, this);
            obj.set_taborder("97");
            obj.set_cssclass("sta_WF_paging");
            this.addChild(obj.name, obj);

            obj = new Static("sta_totcnt", "absolute", null, "6", "148", "19", "13", null, this);
            obj.set_taborder("113");
            obj.set_text("Total <fc v='#ff1414ff'><b v='true'>0</b></fc> Row");
            obj.set_usedecorate("true");
            obj.style.set_align("right middle");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "252", "6", "61", "19", null, null, this);
            obj.set_taborder("114");
            obj.style.set_align("right middle");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Button("btn_next", "absolute", "194", "5", "19", "19", null, null, this);
            obj.set_taborder("116");
            obj.set_text(">");
            obj.set_cssclass("btn_WF_page");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_before", "absolute", "37", "5", "19", "19", null, null, this);
            obj.set_taborder("122");
            obj.set_text("<");
            obj.set_cssclass("btn_WF_page");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_first", "absolute", "13", "5", "22", "19", null, null, this);
            obj.set_taborder("125");
            obj.set_text("<<");
            obj.set_cssclass("btn_WF_page");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_last", "absolute", "215", "5", "22", "19", null, null, this);
            obj.set_taborder("126");
            obj.set_text(">>");
            obj.set_cssclass("btn_WF_page");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "66", "6", "31", "19", null, null, this);
            obj.set_taborder("127");
            obj.set_text("Page");
            obj.style.set_font("8 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("sta_pageCnt", "absolute", "146", "6", "51", "19", null, null, this);
            obj.set_taborder("128");
            obj.set_text("of <fc v='#ff1414ff'><b v='true'>0</b></fc>");
            obj.style.set_font("8 dotum");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Spin("spn_cnt", "absolute", "98", "5", "45", "19", null, null, this);
            obj.set_taborder("129");
            obj.set_value("1");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_pageMove", "absolute", "316", "5", "58", "19", null, null, this);
            this.addChild(obj.name, obj);
            var cbo_pageMove_innerdataset = new Dataset("cbo_pageMove_innerdataset", this.cbo_pageMove);
            cbo_pageMove_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">5</Col><Col id=\"datacolumn\">5</Col></Row><Row><Col id=\"codecolumn\">10</Col><Col id=\"datacolumn\">10</Col></Row><Row><Col id=\"codecolumn\">20</Col><Col id=\"datacolumn\">20</Col></Row><Row><Col id=\"codecolumn\">50</Col><Col id=\"datacolumn\">50</Col></Row><Row><Col id=\"codecolumn\">100</Col><Col id=\"datacolumn\">100</Col></Row><Row><Col id=\"codecolumn\">200</Col><Col id=\"datacolumn\">200</Col></Row><Row><Col id=\"codecolumn\">500</Col><Col id=\"datacolumn\">500</Col></Row><Row><Col id=\"codecolumn\">1000</Col><Col id=\"datacolumn\">1000</Col></Row></Rows>");
            obj.set_innerdataset(cbo_pageMove_innerdataset);
            obj.set_taborder("130");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_value("100");
            obj.set_text("100");
            obj.set_enable("false");
            obj.set_index("4");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 1165, 29, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("페이징 팝업용");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","Static05","text","gds_lang","");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("PopupPaging.xfdl", "lib::Comm.xjs");
        this.registerScript("PopupPaging.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : PopupPaging.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 팝업 화면 페이징
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_popId;
        this.gv_flag; // Main, Middle, Detail

        this.gv_count;
        this.gv_totPage;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* page 셋팅
        * @return
        * @param pagingLimit : 한 페이지 표시 개수
                 currentPage : 현재 페이지
                 count       : 총 개수
        */
        this.fn_pageSet = function(pagingLimit,currentPage,count)
        {
        	this.gv_totPage = 0;
        	if(this.gfn_isNotNull(count)) this.gv_count = count;
        	else this.gv_count = 0;
        	
        	if(this.gv_count%pagingLimit == 0) this.gv_totPage = this.gv_count/pagingLimit;
        	else this.gv_totPage = parseInt(this.gv_count/pagingLimit) + 1;
        	
        	this.spn_cnt.setRange(1,this.gv_totPage); //입력 가능한 숫자 범위
        	
        	if(this.gfn_isNull(this.gv_totPage)) this.gv_totPage = 0;
        	
        	this.sta_totcnt.set_text("Total <fc v='#ff1414ff'><b v='true'>"+this.gfn_formatComma(this.gv_count.toString())+"</b></fc> Row");
        	this.spn_cnt.set_value(currentPage);
        	this.sta_pageCnt.set_text("of <fc v='#ff1414ff'><b v='true'>"+this.gv_totPage+"</b></fc>");
        	this.cbo_pageMove.set_value(pagingLimit);
        	
        	if(currentPage == 1){
        		this.btn_first.set_enable(false);
        		this.btn_before.set_enable(false);
        		
        		if(this.gv_totPage == 1){
        			this.btn_next.set_enable(false);
        			this.btn_last.set_enable(false);
        		}else{
        			this.btn_next.set_enable(true);
        			this.btn_last.set_enable(true);
        		}
        	}else if(currentPage == this.gv_totPage){
        		this.btn_first.set_enable(true);
        		this.btn_before.set_enable(true);
        		this.btn_next.set_enable(false);
        		this.btn_last.set_enable(false);
        	}else{
        		this.btn_first.set_enable(true);
        		this.btn_before.set_enable(true);
        		this.btn_next.set_enable(true);
        		this.btn_last.set_enable(true);
        	}
        	
        	this.spn_cnt.set_enable(true);
        	this.cbo_pageMove.set_enable(true);
        }

        /* pageSearch 셋팅
        * @return
        * @param
        */
        this.fn_pageSearch = function()
        {
            if(this.gv_flag == "Main") this.gv_popId.fn_mainSearch(this.spn_cnt.value, this.cbo_pageMove.value, this.gv_count);
            else if(this.gv_flag == "Middle") this.gv_popId.fn_middleSearch(this.spn_cnt.value, this.cbo_pageMove.value, this.gv_count);
            else if(this.gv_flag == "Detail") this.gv_popId.fn_detailSearch(this.spn_cnt.value, this.cbo_pageMove.value, this.gv_count);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_first_onclick 실행 */
        this.btn_first_onclick = function(obj,e)
        {
        	this.spn_cnt.set_value(1);
        	this.fn_pageSearch();
        }

        /* btn_before_onclick 실행 */
        this.btn_before_onclick = function(obj,e)
        {
        	this.spn_cnt.set_value(this.spn_cnt.value - 1);
        	this.fn_pageSearch();
        }

        /* spn_cnt_canchange 실행 */
        this.spn_cnt_canchange = function(obj,e)
        {
        	if(e.postvalue < 1) return false;
        	else if(e.postvalue > this.gv_totPage) return false;
        }

        /* spn_cnt_onchanged 실행 */
        this.spn_cnt_onchanged = function(obj,e)
        {
        	this.fn_pageSearch();
        }

        /* btn_next_onclick 실행 */
        this.btn_next_onclick = function(obj,e)
        {
        	this.spn_cnt.set_value(this.spn_cnt.value + 1);
        	this.fn_pageSearch();
        }

        /* btn_last_onclick 실행 */
        this.btn_last_onclick = function(obj,e)
        {
        	this.spn_cnt.set_value(this.gv_totPage);
        	this.fn_pageSearch();
        }

        /* cbo_pageMove_onitemchanged 실행 */
        this.cbo_pageMove_onitemchanged = function(obj,e)
        {
        	this.spn_cnt.set_value(1);
        	this.fn_pageSearch();
        }

        
        /* spn_cnt_onspin 실행 */
        this.spn_cnt_onspin = function(obj,e)
        {
        	if(e.postvalue < 1) return false;
        	else if(e.postvalue > this.gv_totPage) return false;
        	
        	this.fn_pageSearch();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_next.addEventHandler("onclick", this.btn_next_onclick, this);
            this.btn_before.addEventHandler("onclick", this.btn_before_onclick, this);
            this.btn_first.addEventHandler("onclick", this.btn_first_onclick, this);
            this.btn_last.addEventHandler("onclick", this.btn_last_onclick, this);
            this.spn_cnt.addEventHandler("onchanged", this.spn_cnt_onchanged, this);
            this.spn_cnt.addEventHandler("canchange", this.spn_cnt_canchange, this);
            this.spn_cnt.addEventHandler("onspin", this.spn_cnt_onspin, this);
            this.cbo_pageMove.addEventHandler("onitemchanged", this.cbo_pageMove_onitemchanged, this);

        };

        this.loadIncludeScript("PopupPaging.xfdl", true);

       
    };
}
)();
