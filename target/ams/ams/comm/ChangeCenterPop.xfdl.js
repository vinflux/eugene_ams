﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ChangeCenterPop");
                this.set_classname("style01");
                this.set_titletext("센터 변경");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,380,160);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_center", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"name\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("센터 변경");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("5");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            obj.set_tabstop("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "195", "23", this);
            obj.set_taborder("3");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "166", "58", "192", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "89", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "135", "23", this);
            obj.set_taborder("4");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("sta_urkey", "absolute", "22", "58", "144", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("센터");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_center", "absolute", "172", "62", "171", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("228");
            obj.set_innerdataset("@ds_center");
            obj.set_codecolumn("code");
            obj.set_datacolumn("name");
            obj.set_index("-1");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 380, 160, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("센터 변경");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item9","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","btn_close","tooltiptext","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ChangeCenterPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ChangeCenterPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ChangeCenterPop.xfdl
        * PROGRAMMER  : hckim
        * DATE        : 2018.12.13
        * DESCRIPTION : WMS 진입시 센터 변경 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        var userInfoCtkey;
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	//var userInfoCtkey = application.gds_userInfo.getColumn(0, "ctKey");
        	userInfoCtkey = this.gfn_getUserInfo("ctKey");
        	var ctkeyIndex = 0;
        	for(var i = 0 ; i < application.gds_rtnWHRole.rowcount ; i++){
        		this.ds_center.addRow();
        		this.ds_center.setColumn(i, "code", application.gds_rtnWHRole.getColumn(i, "centerKey"));
        		this.ds_center.setColumn(i, "name", application.gds_rtnWHRole.getColumn(i, "centerName"));
        		
        		if(userInfoCtkey == application.gds_rtnWHRole.getColumn(i, "centerKey")){
        			ctkeyIndex = i;
        		}
        	}
        	
        	this.cbo_center.set_index(ctkeyIndex);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.changeCenter = function()
        {
        	for(var i = application.gv_TabFrame.form.ds_Tab.getRowCount()-1 ; i >= 0 ; i--){
        		application.gv_TabFrame.form.fn_TabOnClose(application.gv_TabFrame.form.ds_Tab.getColumn(i, "WINID"));
        	}
        	
        	application.gds_openMenu.clearData();
        	application.gfn_setFrame("M");

        	if(this.cbo_center.value == null || this.cbo_center.value == ""){
        		return;
        	}

        	this.gfn_setParam("ctkey", this.cbo_center.value);
        	this.gfn_setParam("urKey", this.gfn_getUserInfo("urKey"));
        	this.gfn_setParam("appKey", this.gfn_getCommon("APKEY"));
        	
        	this.gfn_setUserInfo("ctKey", this.cbo_center.value);
        	this.gfn_setUserInfo("ctKey_desc", this.cbo_center.text);

        	this.gfn_setCommon("BEANID", "userByRoleAdmController");
        	this.gfn_setCommon("METHODNM", "selectUserRole");
        	
        	var sSvcId   = "userChangeCenter";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "gds_userInfo=OUT_USER_INFO";
        	var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
         	if(sSvcId == "userChangeCenter"){
        		application.gv_LoginFrame.form.fn_selectCommInfo();
        		this.close();
         	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	if(userInfoCtkey == this.cbo_center.value) {
        		this.gfn_alert("현재 설정 되어있는 센터입니다.\n현재 센터로 업무를 진행하시려면\n센터 변경 팝업에서 [닫기]를 선택하여 주십시오.");
        		return;
        	}
        	
        	//현재 선택된콤보와 같은 콤보 선택시 패스
        	this.gfn_confirm("MSG_ASK_CENTER_CHANGE", "", function(msg, flag){
        		if(flag){
        			this.changeCenter(msg);
        		}else{
        			this.cbo_center.set_value(this.gfn_getUserInfo("ctKey"));
        		}
        	});
        	
        }

        

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.cbo_center.addEventHandler("onitemchanged", this.cbo_center_onitemchanged, this);

        };

        this.loadIncludeScript("ChangeCenterPop.xfdl", true);

       
    };
}
)();
