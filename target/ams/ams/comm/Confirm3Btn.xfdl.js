﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Confirm");
                this.set_classname("Template10");
                this.set_titletext("confirm");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,380,208);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("Static02", "absolute", "22", "14", "250", "20", null, null, this);
            obj.set_taborder("1");
            obj.set_text("Confirm");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closePop", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_ok", "absolute", "59", null, "84", "28", null, "23", this);
            obj.set_taborder("6");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "237", null, "84", "28", null, "23", this);
            obj.set_taborder("7");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_msg", "absolute", "22", "57", "336", "85", null, null, this);
            obj.set_taborder("8");
            obj.set_scrollbars("autovert");
            obj.set_wordwrap("char");
            obj.set_dragscrolltype("vert");
            obj.set_cssclass("txt_WF_Alert");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("9");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_msg", "absolute", "472", "83", "336", "85", null, null, this);
            obj.set_taborder("10");
            obj.set_readonly("true");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);

            obj = new Button("btn_cancel", "absolute", "148", null, "84", "28", null, "23", this);
            obj.set_taborder("11");
            obj.set_text("취소");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 380, 208, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Template10");
            		p.set_titletext("confirm");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_ok","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","btn_cancel","text","gds_lang","CANCEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static02","text","gds_lang","MESSAGE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Confirm3Btn.xfdl", "lib::Comm.xjs");
        this.registerScript("Confirm3Btn.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Confirm.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Confirm msg 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	var arrBtnText = this.parent.btnText;
        	var sMsgCode = this.gfn_isNotNull(this.parent.msgCode)?this.parent.msgCode:"MSG_ALERT_ALLDATACHKRUN";
        	var arrMsgText = this.parent.msgtext;

        	var sMsgText = "";
        	//var arrMsg = this.gfn_getMsgToArray("MSG_ALERT_ALLDATACHKRUN");
        	var arrMsg = this.gfn_getMsgToArray(sMsgCode);
        	var sMsg = arrMsg[1];

        	if (this.gfn_isNotNull(sMsg)) {
        		// 메시지 치환 처리
        		if (this.gfn_isNotNull(arrMsgText)) {
        			// 메시지 치환 함수 인수생성
        			for (var i = 0; i < arrMsgText.length; i++) {
        				if (i == 0) {
        					sMsgText += arrMsgText[i];
        				} else {
        					sMsgText += "|" + arrMsgText[i];
        				}
        			}

        			// 메시지 치환
        			sMsg = this.gfn_getTransMsg(sMsg, sMsgText);
        		}
        	} else {
        		sMsg = sMsgCode;
        	}

        	this.txa_msg.set_value(sMsg); // 메시지 코드가 없을 경우

        	// Button 처리

        	var objBinds = this.binds;

        	for (var i = 0; i < objBinds.length; i++) {
        		if (objBinds[i].compid == "btn_ok") {
        			objBinds[i].columnid = arrBtnText[0];
        			objBinds[i].bind();
        		}

        		if (objBinds[i].compid == "btn_cancel") {
        			objBinds[i].columnid = arrBtnText[1];
        			objBinds[i].bind();
        		}

        		if (objBinds[i].compid == "btn_close") {
        			objBinds[i].columnid = arrBtnText[2];
        			objBinds[i].bind();
        		}
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* 확인 Button 실행 */
        this.btn_ok_onclick = function(obj,e)
        {
            this.close("1");
        }

        /* 취소 Button 실행 */
        this.btn_cancel_onclick = function(obj,e)
        {
            this.close("2");
        }

        /* 닫기 Button 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close("0");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closePop.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_ok.addEventHandler("onclick", this.btn_ok_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.txa_msg.addEventHandler("oneditclick", this.TextArea00_oneditclick, this);
            this.btn_cancel.addEventHandler("onclick", this.btn_cancel_onclick, this);

        };

        this.loadIncludeScript("Confirm3Btn.xfdl", true);

       
    };
}
)();
