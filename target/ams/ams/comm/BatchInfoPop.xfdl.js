﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("BatchInfoPop");
                this.set_classname("style01");
                this.set_titletext("배치작업 정보");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,374,279);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "130", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "152", "120", null, "31", "22", null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "152", "89", null, "31", "22", null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "161", "23", this);
            obj.set_taborder("8");
            obj.set_text("확인");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "130", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "152", "151", null, "62", "22", null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "130", "62", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "130", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "152", "58", null, "31", "22", null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_workCnt", "absolute", "158", "62", null, "22", "37", null, this);
            obj.set_taborder("251");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_successCnt", "absolute", "158", "93", null, "22", "37", null, this);
            obj.set_taborder("252");
            obj.set_maxlength("0");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_failCnt", "absolute", "158", "124", null, "22", "37", null, this);
            obj.set_taborder("253");
            obj.set_maxlength("0");
            obj.set_readonly("true");
            obj.set_cssclass("edt_pop_con");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_failWorkList", "absolute", "158", "155", null, "53", "37", null, this);
            obj.set_taborder("254");
            obj.set_readonly("true");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 374, 279, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("배치작업 정보");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","WORK_INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","SUCCESS_TOTAL_CNT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","FAIL_TOTAL_CNT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","WORK_TOTAL_CNT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","FAIL_WORK_LIST");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","txa_failWorkList","value","ds_header","addr");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("BatchInfoPop.xfdl", "lib::Comm.xjs");
        this.registerScript("BatchInfoPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : BatchInfoPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 배치작업 정보
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	var workCnt = this.gfn_isNullEmpty(""+this.parent.workCnt);
        	var successCnt = this.gfn_isNullEmpty(""+this.parent.successCnt);
        	var failCnt = this.gfn_isNullEmpty(""+this.parent.failCnt);
        	var failWorkList = this.gfn_isNullEmpty(""+this.parent.failWorkList);
        	
            this.edt_workCnt.set_value(workCnt);
            this.edt_successCnt.set_value(successCnt);
            this.edt_failCnt.set_value(failCnt);
            this.txa_failWorkList.set_value(failWorkList);
            
            this.btn_close.setFocus();
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	this.close();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("BatchInfoPop.xfdl", true);

       
    };
}
)();
