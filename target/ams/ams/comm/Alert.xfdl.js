﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Alert");
                this.set_classname("Template10");
                this.set_titletext("Alert");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,380,208);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Button("btn_ok", "absolute", "165", null, "52", "28", null, "23", this);
            obj.set_taborder("4");
            obj.set_text("OK");
            obj.set_cssclass("btn_WF_PopupBtn");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_msg", "absolute", "442", "240", "336", "85", null, null, this);
            obj.set_taborder("5");
            obj.set_wordwrap("char");
            obj.set_scrollbars("autovert");
            obj.style.set_align("center middle");
            obj.set_cssclass("txt_WF_Alert");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("7");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "14", "250", "20", null, null, this);
            obj.set_taborder("9");
            obj.set_text("Alert");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_msg", "absolute", "464", "80", "336", "85", null, null, this);
            obj.set_taborder("10");
            obj.style.set_align("center middle");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("static_msg", "absolute", "6.58%", "55", "336", "85", null, null, this);
            obj.set_taborder("11");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.style.set_align("center middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 380, 208, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Template10");
            		p.set_titletext("Alert");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_ok","text","gds_lang","OK");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Alert.xfdl", "lib::Comm.xjs");
        this.registerScript("Alert.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Alert.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : Alert msg 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            var sMsgCd = this.gfn_isNullEmpty(this.parent.argMsgCd);
            var sTrans = this.gfn_isNullEmpty(this.parent.argTrans);
            var sBeforeMsg = this.gfn_isNullEmpty(this.parent.argBeforeMsg);
            var tranBeforeMsg = application.gds_lang.getColumn(0, sBeforeMsg);
            if(tranBeforeMsg === undefined || tranBeforeMsg == ""){
        		tranBeforeMsg = sBeforeMsg;
            }
            var aMsg = this.gfn_getMsgToArray(sMsgCd);
            var sMsg = "";
            
            if(this.gfn_isNotNull(aMsg)) {
                if(sMsgCd == "MSG_WELCOME") this.static_msg.set_text(sTrans + this.gfn_getTransMsg(aMsg[1]));
                else if(sMsgCd == "MSG_CHK_ENABLEMAXUSERSCREEN") this.static_msg.set_text(this.gfn_getTransMsg(aMsg[1]) + sTrans);
                else this.static_msg.set_text(tranBeforeMsg+this.gfn_getTransMsg(aMsg[1], sTrans));
            }else{
                sMsg = this.gfn_replaceAll(sMsgCd, "\n", String.fromCharCode(10) + String.fromCharCode(13));
                this.static_msg.set_text(tranBeforeMsg+sMsg + sTrans); // 메시지 코드가 없을 경우
            }
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* ok Button 실행 */
        this.btn_ok_onclick = function(obj,e)
        {
            var flag = true;
            this.close(flag);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_ok.addEventHandler("onclick", this.btn_ok_onclick, this);
            this.txa_msg.addEventHandler("oneditclick", this.TextArea00_oneditclick, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_ok_onclick, this);

        };

        this.loadIncludeScript("Alert.xfdl", true);

       
    };
}
)();
