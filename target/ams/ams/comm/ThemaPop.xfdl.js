﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ThemaPop");
                this.set_classname("style01");
                this.set_titletext("테마 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,250,400);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Radio("rdo_thema", "absolute", "18", "29", "252", "295", null, null, this);
            this.addChild(obj.name, obj);
            var rdo_thema_innerdataset = new Dataset("rdo_thema_innerdataset", this.rdo_thema);
            rdo_thema_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">dg_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">bl_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">re_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">gn_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">nv_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">vi_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">cy_</Col><Col id=\"datacolumn\"/></Row><Row><Col id=\"codecolumn\">gy_</Col><Col id=\"datacolumn\"/></Row></Rows>");
            obj.set_innerdataset(rdo_thema_innerdataset);
            obj.set_taborder("124");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_rowcount("0");
            obj.set_columncount("2");
            obj.set_direction("horizontal");
            obj.set_index("-1");

            obj = new Static("label00", "absolute", "15", "14", "140", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "40", null, "1", "0", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "9", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "100", null, "52", "28", null, "17", this);
            obj.set_taborder("123");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "37", "101", "58", "15", null, null, this);
            obj.set_taborder("125");
            obj.set_text("다크그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_dark", "absolute", "37", "57", "58", "39", null, null, this);
            obj.set_taborder("126");
            obj.set_image("URL('img::skin_darkgray.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_blue", "absolute", "163", "57", "58", "39", null, null, this);
            obj.set_taborder("128");
            obj.set_image("URL('img::skin_blue.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "163", "101", "58", "15", null, null, this);
            obj.set_taborder("129");
            obj.set_text("블루");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "37", "174", "58", "15", null, null, this);
            obj.set_taborder("131");
            obj.set_text("레드");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_red", "absolute", "37", "130", "58", "39", null, null, this);
            obj.set_taborder("132");
            obj.set_image("URL('img::skin_red.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_green", "absolute", "163", "130", "58", "39", null, null, this);
            obj.set_taborder("134");
            obj.set_image("URL('img::skin_green.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "163", "174", "58", "15", null, null, this);
            obj.set_taborder("135");
            obj.set_text("그린");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "37", "247", "58", "15", null, null, this);
            obj.set_taborder("136");
            obj.set_text("네이비");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_navy", "absolute", "37", "203", "58", "39", null, null, this);
            obj.set_taborder("137");
            obj.set_image("URL('img::skin_navy.png')");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_violet", "absolute", "163", "203", "58", "39", null, null, this);
            obj.set_taborder("138");
            obj.set_image("URL('img::skin_violet.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "163", "247", "58", "15", null, null, this);
            obj.set_taborder("139");
            obj.set_text("바이올렛");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "37", "320", "58", "15", null, null, this);
            obj.set_taborder("140");
            obj.set_text("청록색");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_bluegreen", "absolute", "37", "276", "58", "39", null, null, this);
            obj.set_taborder("141");
            obj.set_image("URL('img::skin_cyan.png')");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "163", "320", "58", "15", null, null, this);
            obj.set_taborder("142");
            obj.set_text("그레이");
            obj.set_cssclass("sta_WF_stext");
            this.addChild(obj.name, obj);

            obj = new ImageViewer("img_grey", "absolute", "163", "276", "58", "39", null, null, this);
            obj.set_taborder("143");
            obj.set_image("URL('img::skin_gray.png')");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 250, 400, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("테마 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","label00","text","gds_lang","THEMA");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","Static01","text","gds_lang","THEMA_DG");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static02","text","gds_lang","THEMA_BL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Static03","text","gds_lang","THEMA_RD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","Static04","text","gds_lang","THEMA_GR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","Static05","text","gds_lang","THEMA_NY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","Static06","text","gds_lang","THEMA_VT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","Static07","text","gds_lang","THEMA_BG");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","Static08","text","gds_lang","THEMA_GY");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ThemaPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ThemaPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ThemaPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 테마 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_thema;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	var sThema = "";
        	
        	if(system.navigatorname == "nexacro"){
        		sThema = this.gfn_isNullEmpty(application.getPrivateProfile("SAVE_TEMA_"+application.gv_activeApp));
        	}else{
        		sThema = this.gfn_isNullEmpty(window.localStorage.getItem("SAVE_TEMA_"+application.gv_activeApp));
        	}
        	
        	if(sThema != "" && sThema.substr(0, 1) != "#") this.rdo_thema.set_value(sThema);
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* 테마 설정
        * @param
        * @return
        */
        this.fn_setThema = function()
        {
        	if(system.navigatorname == "nexacro") application.setPrivateProfile("SAVE_TEMA_"+application.gv_activeApp, this.gv_thema);
        	else window.localStorage.setItem("SAVE_TEMA_"+application.gv_activeApp, this.gv_thema);
        	this.gfn_thema(this.gv_thema);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* cancel Button 실행 */
        this.btn_close_onclick = function(obj,e)
        {
            this.close();
        }

        /* rdo_thema_onitemchanged 실행 */
        this.rdo_thema_onitemchanged = function(obj,e)
        {
        	if(this.rdo_thema.value == 1) this.gv_thema = "";
        	else this.gv_thema = this.rdo_thema.value;
        	
        	this.fn_setThema();
        }

        /* img_dark_onclick 실행 */
        this.img_onclick = function(obj,e)
        {
        	if(obj.name == "img_dark") this.rdo_thema.set_value("dg_");
        	else if(obj.name == "img_blue") this.rdo_thema.set_value("bl_");
        	else if(obj.name == "img_red") this.rdo_thema.set_value("re_");
        	else if(obj.name == "img_green") this.rdo_thema.set_value("gn_");
        	else if(obj.name == "img_navy") this.rdo_thema.set_value("nv_");
        	else if(obj.name == "img_violet") this.rdo_thema.set_value("vi_");
        	else if(obj.name == "img_bluegreen") this.rdo_thema.set_value("cy_");
        	else if(obj.name == "img_grey") this.rdo_thema.set_value("gy_");
        	
        	this.gv_thema = this.rdo_thema.value;
        	
        	this.fn_setThema();
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.rdo_thema.addEventHandler("onitemchanged", this.rdo_thema_onitemchanged, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_cancel_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.img_dark.addEventHandler("onclick", this.img_onclick, this);
            this.img_blue.addEventHandler("onclick", this.img_onclick, this);
            this.img_red.addEventHandler("onclick", this.img_onclick, this);
            this.img_green.addEventHandler("onclick", this.img_onclick, this);
            this.img_navy.addEventHandler("onclick", this.img_onclick, this);
            this.img_violet.addEventHandler("onclick", this.img_onclick, this);
            this.img_bluegreen.addEventHandler("onclick", this.img_onclick, this);
            this.img_grey.addEventHandler("onclick", this.img_onclick, this);

        };

        this.loadIncludeScript("ThemaPop.xfdl", true);

       
    };
}
)();
