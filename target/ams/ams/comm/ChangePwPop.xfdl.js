﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("ChangePwPop");
                this.set_classname("style01");
                this.set_titletext("비밀번호 변경");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,249);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_changePw", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"urpw1\" type=\"STRING\" size=\"256\"/><Column id=\"urpw2\" type=\"STRING\" size=\"256\"/><Column id=\"owkeym\" type=\"STRING\" size=\"256\"/><Column id=\"urpw\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"urkey\"/><Col id=\"urpw1\"/><Col id=\"urpw2\"/><Col id=\"owkeym\"/><Col id=\"urpw\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("사용자 정보");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("5");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            obj.set_tabstop("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("3");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urkey", "absolute", "222", "62", "321", "22", null, null, this);
            obj.set_taborder("180");
            obj.set_enable("false");
            obj.set_tabstop("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "182", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("4");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("sta_urkey", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pw1", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_password("true");
            this.addChild(obj.name, obj);

            obj = new Static("sta_pw1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("sta_owkeym", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_search", "absolute", "518", "155", "24", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkeym", "absolute", "222", "155", "294", "22", null, null, this);
            obj.set_taborder("234");
            obj.set_enable("false");
            obj.set_tabstop("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("238");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pw2", "absolute", "222", "124", "321", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_password("true");
            this.addChild(obj.name, obj);

            obj = new Static("sta_pw2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("240");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 249, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("비밀번호 변경");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_urkey","value","ds_changePw","urkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","USER INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_pw1","value","ds_changePw","urpw1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_owkeym","value","ds_changePw","owkeym");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_pw2","value","ds_changePw","urpw2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","sta_urkey","text","gds_lang","URKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","sta_owkeym","text","gds_lang","OWKEYM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","sta_pw1","text","gds_lang","URPW");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","sta_pw2","text","gds_lang","URPW2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","ChangePwPop","titletext","gds_lang","USER INFO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","btn_close","tooltiptext","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("ChangePwPop.xfdl", "lib::Comm.xjs");
        this.registerScript("ChangePwPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : ChangePwPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 비밀번호 변경 팝업 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.ds_changePw.setColumn(0, "urkey" , this.gfn_getUserInfo("urKey"));
        	this.ds_changePw.setColumn(0, "owkeym", this.gfn_getUserInfo("owkeym"));
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_Save = function()
        {
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "updateUserPw");
        		
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_changePw";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var chkCnt = 0;
        	var nNum = 0;
        	var nEng = 0;
        	var sPw = this.edt_pw1.value;
        	
        	if(this.gfn_isNull(this.edt_pw1.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_pw1.setFocus();
        		});
        	}else if(this.gfn_isNotNull(sPw)){
        		// MSG_ALERT_CHECK_PW : #{pw}는 사용자ID #{id}와 3자리이상 동일한 값을 포함하므로 사용할수 없습니다.
        		for(var i = 0 ; i+2 < sPw.length ; i++){
        			if(this.ds_changePw.getColumn(0, "urkey").indexOf(sPw.substr(i, 3)) >= 0){
        				chkCnt++;
        			}
        		}
        		
        		if(chkCnt > 0){
        			this.gfn_alert("MSG_ALERT_CHECK_PW", "pw," + sPw + "|id," + this.ds_changePw.getColumn(0, "urkey"), function(msg, flag){
        				this.edt_pw1.setFocus();
        			});
        			
        			return;
        		}
        		
        		// MSG_ALERT_CHECK_PW_ENGNUM : 비밀번호는 영문과 숫자가 최소 1자리이상은 포함되어야 합니다.
        		nNum = sPw.search(/[0-9]/g);
        		nEng = sPw.search(/[A-Za-z]/ig);
        		
        		if(nNum < 0 || nEng < 0){
        			this.gfn_alert("MSG_ALERT_CHECK_PW_ENGNUM", "", function(msg, flag){
        				this.edt_pw1.setFocus();
        			});
        			
        			return;
        		}
        		
        		if(this.gfn_isNull(this.edt_pw2.value)){
        			this.gfn_alert("MSG_10001", "", function(msg, flag){
        				this.edt_pw2.setFocus();
        			});
        		}else if(this.edt_pw1.value != this.edt_pw2.value){
        			this.gfn_alert("입력하신 비밀번호가 서로 다릅니다.", "", function(msg, flag){
        				this.ds_changePw.setColumn(0, "urpw1", "");
        				this.ds_changePw.setColumn(0, "urpw2", "");
        				this.edt_pw1.setFocus();
        			});
        		}else{
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.ds_changePw.setColumn(0, "urpw", this.ds_changePw.getColumn(0, "urpw1"));
        					this.fn_Save();
        				}
        			});
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_pw1.value) || this.gfn_isNotNull(this.edt_pw2.value) || this.edt_owkeym.value != this.gfn_getUserInfo("owkeym")){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_search_onclick = function(obj,e)
        {
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkeym
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_search.addEventHandler("onclick", this.btn_search_onclick, this);
            this.edt_owkeym.addEventHandler("onchanged", this.edt_code_onchanged, this);

        };

        this.loadIncludeScript("ChangePwPop.xfdl", true);

       
    };
}
)();
