﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CompGuide01");
                this.set_classname("CompGuide01");
                this.set_titletext("Button, Spin, CheckBox, Radio");
                this._setFormPosition(0,0,765,540);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_radio", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"code\" type=\"STRING\" size=\"256\"/><Column id=\"data\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"code\">1</Col><Col id=\"data\">남</Col></Row><Row><Col id=\"code\">2</Col><Col id=\"data\">여</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Spin("Spin00", "absolute", "139", "260", "120", "22", null, null, this);
            obj.set_taborder("35");
            obj.set_value("100");
            this.addChild(obj.name, obj);

            obj = new Spin("Spin01", "absolute", "312", "260", "120", "22", null, null, this);
            obj.set_taborder("36");
            obj.set_value("0");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "0", "0", "124", "30", null, null, this);
            obj.set_taborder("48");
            obj.set_text("Attribute");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "123", "0", "642", "30", null, null, this);
            obj.set_taborder("49");
            obj.set_text("Style");
            obj.set_cssclass("sta_GA_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "0", "29", "124", "185", null, null, this);
            obj.set_taborder("50");
            obj.set_text("Button");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "0", "213", "765", "1", null, null, this);
            obj.set_taborder("52");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "311", "35", "100", "25", null, null, this);
            obj.set_taborder("54");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "135", "35", "100", "25", null, null, this);
            obj.set_taborder("55");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "139", "69", "42", "24", null, null, this);
            obj.set_taborder("56");
            obj.set_text("가나");
            this.addChild(obj.name, obj);

            obj = new Button("Button03", "absolute", "312", "69", "42", "24", null, null, this);
            obj.set_taborder("57");
            obj.set_text("가나");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button00", "absolute", "139", "104", "54", "24", null, null, this);
            obj.set_taborder("58");
            obj.set_text("가나다");
            this.addChild(obj.name, obj);

            obj = new Button("Button01", "absolute", "312", "104", "54", "24", null, null, this);
            obj.set_taborder("59");
            obj.set_text("가나다");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button04", "absolute", "139", "139", "66", "24", null, null, this);
            obj.set_taborder("60");
            obj.set_text("가나다라");
            this.addChild(obj.name, obj);

            obj = new Button("Button05", "absolute", "312", "139", "66", "24", null, null, this);
            obj.set_taborder("61");
            obj.set_text("기나다라");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("Button06", "absolute", "139", "174", "78", "24", null, null, this);
            obj.set_taborder("62");
            obj.set_text("가나다라마");
            this.addChild(obj.name, obj);

            obj = new Button("Button07", "absolute", "312", "174", "78", "24", null, null, this);
            obj.set_taborder("63");
            obj.set_text("가나다라마");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "0", "213", "124", "90", null, null, this);
            obj.set_taborder("65");
            obj.set_text("Spin");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "140", "227", "100", "25", null, null, this);
            obj.set_taborder("66");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "316", "227", "100", "25", null, null, this);
            obj.set_taborder("67");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "0", "302", "765", "1", null, null, this);
            obj.set_taborder("68");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "302", "124", "135", null, null, this);
            obj.set_taborder("69");
            obj.set_text("CheckBox");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "140", "316", "100", "25", null, null, this);
            obj.set_taborder("70");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "316", "316", "100", "25", null, null, this);
            obj.set_taborder("71");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox00", "absolute", "139", "352", "77", "21", null, null, this);
            obj.set_taborder("72");
            obj.set_text("체크박스");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox01", "absolute", "312", "352", "120", "21", null, null, this);
            obj.set_taborder("73");
            obj.set_text("체크박스");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "0", "436", "765", "1", null, null, this);
            obj.set_taborder("74");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "0", "436", "124", "90", null, null, this);
            obj.set_taborder("75");
            obj.set_text("Radio");
            obj.set_cssclass("sta_GA_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "136", "446", "100", "25", null, null, this);
            obj.set_taborder("76");
            obj.set_text("Normal");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "312", "446", "100", "25", null, null, this);
            obj.set_taborder("77");
            obj.set_text("Disabled");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Radio("Radio00", "absolute", "139", "487", "120", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("78");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_direction("vertical");
            obj.set_value("2");
            obj.set_index("1");

            obj = new Radio("Radio01", "absolute", "312", "487", "120", "21", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("79");
            obj.set_innerdataset("@ds_radio");
            obj.set_codecolumn("code");
            obj.set_datacolumn("data");
            obj.set_direction("vertical");
            obj.set_enable("false");
            obj.set_value("2");
            obj.set_index("1");

            obj = new Static("Static13", "absolute", "0", "525", "765", "1", null, null, this);
            obj.set_taborder("80");
            obj.style.set_background("#c6cbceff");
            obj.style.set_border("0 none #808080ff");
            obj.style.set_color("#393939ff");
            obj.style.set_align("center middle");
            obj.style.set_font("bold 10 verdana");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "221", "391", "90", "15", null, null, this);
            obj.set_taborder("81");
            obj.set_text("value = true");
            obj.style.set_linespace("3");
            obj.style.set_color("lightcoral");
            obj.style.set_font("10 dotum");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "221", "355", "90", "15", null, null, this);
            obj.set_taborder("82");
            obj.set_text("value = false");
            obj.style.set_linespace("3");
            obj.style.set_color("lightcoral");
            obj.style.set_font("10 dotum");
            this.addChild(obj.name, obj);

            obj = new CheckBox("CheckBox02", "absolute", "139", "388", "77", "21", null, null, this);
            obj.set_taborder("83");
            obj.set_text("체크박스");
            obj.set_value("true");
            this.addChild(obj.name, obj);

            obj = new Button("Button08", "absolute", "534", "69", "33", "24", null, null, this);
            obj.set_taborder("85");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.addChild(obj.name, obj);

            obj = new Button("Button09", "absolute", "495", "69", "33", "24", null, null, this);
            obj.set_taborder("86");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.addChild(obj.name, obj);

            obj = new Button("Button10", "absolute", "456", "69", "33", "24", null, null, this);
            obj.set_taborder("87");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Button("Button13", "absolute", "574", "69", "33", "24", null, null, this);
            obj.set_taborder("89");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.addChild(obj.name, obj);

            obj = new Button("Button14", "absolute", "615", "69", "33", "24", null, null, this);
            obj.set_taborder("90");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "456", "35", "100", "25", null, null, this);
            obj.set_taborder("91");
            obj.set_text("필수 버튼");
            obj.style.set_border("0 none #808080");
            obj.style.set_color("#393939ff");
            obj.style.set_font("bold 9 verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button11", "absolute", "654", "69", "33", "24", null, null, this);
            obj.set_taborder("92");
            obj.set_cssclass("btn_WF_report");
            obj.set_tooltiptext("리포터");
            this.addChild(obj.name, obj);

            obj = new Button("Button12", "absolute", "456", "101", "33", "24", null, null, this);
            obj.set_taborder("93");
            obj.set_cssclass("btn_WF_excelup");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Button("Button15", "absolute", "495", "101", "33", "24", null, null, this);
            obj.set_taborder("94");
            obj.set_cssclass("btn_WF_sampledown");
            obj.set_tooltiptext("수정");
            this.addChild(obj.name, obj);

            obj = new Button("Button16", "absolute", "534", "101", "33", "24", null, null, this);
            obj.set_taborder("95");
            obj.set_cssclass("btn_WF_list");
            obj.set_tooltiptext("수정");
            this.addChild(obj.name, obj);

            obj = new Button("Button17", "absolute", "574", "101", "33", "24", null, null, this);
            obj.set_taborder("96");
            obj.set_cssclass("btn_WF_print");
            obj.set_tooltiptext("수정");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 765, 540, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("CompGuide01");
            		p.set_titletext("Button, Spin, CheckBox, Radio");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script

        
        // Regist UI Components Event
        this.on_initEvent = function()
        {

        };

        this.loadIncludeScript("CompGuide02.xfdl", true);

       
    };
}
)();
