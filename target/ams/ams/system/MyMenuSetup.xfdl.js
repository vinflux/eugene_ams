﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MyMenuSetup");
                this.set_classname("style01");
                this.set_titletext("나의 메뉴 설정");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/><Column id=\"Column0\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_tree", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"mename\" type=\"STRING\" size=\"32\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"meorder\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"melvl\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"URKEY\" type=\"STRING\" size=\"32\"/><Column id=\"uskey\" type=\"STRING\" size=\"32\"/><Column id=\"uppermekey\" type=\"STRING\" size=\"32\"/><Column id=\"mekey\" type=\"STRING\" size=\"32\"/><Column id=\"isleaf\" type=\"STRING\" size=\"32\"/><Column id=\"eqtype\" type=\"STRING\" size=\"32\"/><Column id=\"isseparator\" type=\"STRING\" size=\"32\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_tree", "absolute", null, "29", "612", null, "0", "0", this.div_splitTop);
            obj.set_binddataset("ds_tree");
            obj.set_scrollbars("autoboth");
            obj.set_treeuseimage("false");
            obj.set_treeuseline("true");
            obj.set_treeinitstatus("expand,all");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"612\"/></Columns><Rows><Row size=\"26\"/></Rows><Band id=\"body\"><Cell displaytype=\"tree\" edittype=\"tree\" style=\"align:left middle;\" text=\"bind:mename\" expr=\"mename + &quot; ( &quot; + mekey + &quot; ) &quot;\" editlimit=\"50\" editdisplay=\"edit\" treelevel=\"bind:melvl\" treecheck=\"bind:CHK\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", "612", null, null, "0", this.div_splitTop);
            obj.set_binddataset("ds_header");
            obj.set_treeuseline("true");
            obj.set_treeuseimage("true");
            obj.set_treeinitstatus("expand,all");
            obj.set_treeusecheckbox("false");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"612\"/></Columns><Rows><Row size=\"26\"/></Rows><Band id=\"body\"><Cell displaytype=\"tree\" edittype=\"tree\" style=\"align:left middle;\" text=\"bind:mename\" expr=\"mename + &quot; ( &quot; + mekey + &quot; ) &quot;\" editlimit=\"50\" editdisplay=\"edit\" treestartlevel=\"0\" treelevel=\"bind:melvl\" treecheck=\"bind:checked\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("21");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("22");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("23");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("24");
            obj.set_text("메뉴 설정 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("나의 메뉴 설정");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop.Static00","text","gds_lang","ME00000940_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("MyMenuSetup.xfdl", "lib::Comm.xjs");
        this.registerScript("MyMenuSetup.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : MyMenuSetup.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 나의 메뉴 설정
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_tree = this.div_splitTop.grd_tree;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.fn_calculateDetailPosition();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	this.ds_tree.clearData();
        	
        	this.gfn_setCommon("BEANID", "myMenuSetupController");
        	this.gfn_setCommon("METHODNM", "selectMyMenuListLevel");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_tree=OUT_USER_GRID ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "myMenuSetupController");
        	this.gfn_setCommon("METHODNM", "updateMyMenu");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_list=ds_tree";
            var sOutData = "";
        	var sParam   = "urkey=" + this.gfn_getUserInfo("urKey")
        	             ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0){
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		this.ds_header.copyData(application.gds_menu);
        		
        		for(var i = 0 ; i < this.ds_header.rowcount ; i++){
        			this.ds_header.setColumn(i, "melvl", this.ds_header.getColumn(i, "melvl")+1);
        		}
        		
        // 		this.ds_header.insertRow(0);
        // 		this.ds_header.setColumn(0, "mekey", "Root");
        // 		this.ds_header.setColumn(0, "melvl", "0");
        // 		this.ds_header.setColumn(0, "mename", application.gv_activeApp);
        // 		this.ds_header.setColumn(0, "isleaf", "N");
        		
        		if(this.ds_tree.rowcount > 0){
        			this.ds_tree.addColumn("CHK");
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.gv_tree.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.ds_header.filter("mekey != '9999999999'");
        	}else if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		application.gds_userMenu.copyData(this.ds_tree);
        		this.parent.div_cond.btn_search.click();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        // bottom 크기 조절
        this.fn_calculateDetailPosition = function(){
        	var btn_Gap = 3;
        	var btn_HGap = 47;
        	
        	var v_nX = nexacro.round(this.div_splitTop.getOffsetWidth() / 2) - btn_Gap;
        	
        	this.gv_header.set_width(v_nX);
        	this.gv_tree.set_width(v_nX);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        		if(flag){
        			this.fn_save();
        		}
        	});
        }

        /* div_splitBottom_onsize 실행 */
        this.div_splitTop_onsize = function(obj,e)
        {
        	this.fn_calculateDetailPosition();
        }

        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_tree.findRowExpr("CHK == '1'");
        	var nExtDelRow = nRow;
        	var sMelvl = "";
        	
        	
        	if ( nExtDelRow != -1 ) {
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        			
        				while(nRow != -1){
        					sMelvl = this.ds_tree.getColumn(nRow, "melvl");
        					this.ds_tree.deleteRow(nRow);
        					
        					while(true){
        						if(sMelvl < this.ds_tree.getColumn(nRow, "melvl")) this.ds_tree.deleteRow(nRow);
        						else break;
        					}
        					
        					nRow = this.ds_tree.findRowExpr("CHK == '1'", nRow);
        				}
        				this.fn_save();
        			}
        		});
        	} else { 
        	   this.gfn_confirm("MSG_CHKDATA_NOSELECT")
        	}
        }

        /* div_splitTop_grd_header_ondrag 실행 */
        this.div_splitTop_grd_header_ondrag = function(obj,e)
        {
        	if(obj == e.fromobject){
        		var arrData = [];
        		arrData[0] = e.row;
        		arrData[1] = obj.name;
        		arrData[2] = obj;
        		
        		e.dragdata = arrData;
        		
        		return true;
        	}
        }

        /* div_splitTop_grd_tree_ondrop 실행 */
        this.div_splitTop_grd_tree_ondrop = function(obj,e)
        {
        	
        	if(obj == e.fromobject){
        	
        		var sSelectRows = e.dragdata[0];
        		var objDragName = e.dragdata[1];
        		var objDragGrid = e.dragdata[2];
        		var objDragDs = objDragGrid.getBindDataset();
        		var objDs = obj.getBindDataset();
        		
        		var sMekey = objDragDs.getColumn(sSelectRows, "mekey");
        		var sIsLeaf = objDragDs.getColumn(sSelectRows, "isleaf");
        		var nLevel = objDragDs.getColumn(sSelectRows, "melvl");
        		var nRow = objDragDs.findRowExpr("mekey == '" + sMekey + "'");
        		var nextMekey = "";
        		var nextLvl = 0;
        		var newRow = 0;
        		
        		if (objDragName != obj.name){
        		
        			if(this.ds_tree.findRowExpr("mekey == '" + sMekey + "'") != -1) {
        			    this.gfn_alert("MSG_ALERT_EXIST_MYMENU");
        				return;
        			}
        			
        			if(nLevel == 0){
        			}else if(sIsLeaf == "N"){
        				if(e.row != -9){
        					this.gfn_alert("MSG_ALERT_CANNOT_ADD_MYMENU");
        				}else{
        					newRow = objDs.addRow();
        					objDs.copyRow(newRow, objDragDs, nRow);
        					objDs.setColumn(newRow, "melvl", 0);
        					objDs.setColumn(newRow, "uppermekey", 0);
        					
        					for(var i = nRow+1 ; i < objDragDs.rowcount ; i++){
        						nextMekey = objDragDs.getColumn(i, "mekey");
        						nextLvl = objDragDs.getColumn(i, "melvl");
        						
        						if(this.ds_tree.findRowExpr("mekey == '" + nextMekey + "'") != -1) continue;
        						
        						if(nLevel < nextLvl){
        							newRow = objDs.addRow();
        							objDs.copyRow(newRow, objDragDs, i);
        							objDs.setColumn(newRow, "melvl", nextLvl - nLevel);
        						}else{
        							break;
        						}
        					}
        				}
        			}else{
        				if(e.row != -9){
        					var newLvl = objDs.getColumn(e.row, "melvl");
        					var newMekey = objDs.getColumn(e.row, "mekey");
        					var newIsLeaf = objDs.getColumn(e.row, "isleaf");
        					var newUppermekey = objDs.getColumn(e.row, "uppermekey");
        					var stKey = "";
        					
        					if(this.ds_tree.findRowExpr("uppermekey == '" + newUppermekey + "' && mekey == '" + sMekey + "'") != -1) return;
        					
        					if(newIsLeaf == "N"){
        						newRow = objDs.insertRow(e.row+1);
        						objDs.copyRow(newRow, objDragDs, nRow);
        						objDs.setColumn(newRow, "melvl", newLvl+1);
        						objDs.setColumn(newRow, "uppermekey", newMekey);
        						stKey = newMekey;
        					}else{
        						newRow = objDs.insertRow(e.row);
        						objDs.copyRow(newRow, objDragDs, nRow);
        						objDs.setColumn(newRow, "melvl", newLvl);
        						objDs.setColumn(newRow, "uppermekey", newUppermekey);
        						stKey = newUppermekey;
        					}
        					
        					var stRow = objDs.findRowExpr("mekey == '" + stKey + "'");
        					var stLvl = objDs.getColumn(stRow, "melvl");
        					var nCnt = 1;
        					
        					for(var i = stRow+1 ; i < objDs.rowcount ; i++){
        						nextLvl = objDs.getColumn(i, "melvl");
        						
        						if(stLvl < nextLvl){
        							objDs.setColumn(i, "meorder", nCnt * 10);
        						}else{
        							break;
        						}
        						
        						nCnt++;
        					}
        				}
        			}
        		}else{
        			if(sIsLeaf == "N"){
        			}else{
        				if(e.row != -9){
        					var newLvl = objDs.getColumn(e.row, "melvl");
        					var newMekey = objDs.getColumn(e.row, "mekey");
        					var newIsLeaf = objDs.getColumn(e.row, "isleaf");
        					var newUppermekey = objDs.getColumn(e.row, "uppermekey");
        					
        					if(newIsLeaf == "N"){
        						if(nRow > e.row) newRow = objDs.moveRow(nRow, e.row + 1);
        						else newRow = objDs.moveRow(nRow, e.row);
        						
        						objDs.setColumn(newRow, "melvl", newLvl + 1);
        						objDs.setColumn(newRow, "uppermekey", newMekey);
        						
        						var stRow = objDs.findRowExpr("mekey == '" + newMekey + "'");
        						var stLvl = objDs.getColumn(stRow, "melvl");
        						var nCnt = 1;
        						
        						for(var i = stRow+1 ; i < objDs.rowcount ; i++){
        							nextLvl = objDs.getColumn(i, "melvl");
        							
        							if(stLvl < nextLvl){
        								objDs.setColumn(i, "meorder", nCnt * 10);
        							}else{
        								break;
        							}
        							
        							nCnt++;
        						}
        					}else{
        						newRow = objDs.moveRow(nRow, e.row);
        						objDs.setColumn(newRow, "melvl", newLvl);
        						objDs.setColumn(newRow, "uppermekey", newUppermekey);
        						
        						var stRow = objDs.findRowExpr("mekey == '" + newUppermekey + "'");
        						var stLvl = objDs.getColumn(stRow, "melvl");
        						var nCnt = 1;
        						
        						for(var i = stRow+1 ; i < objDs.rowcount ; i++){
        							nextLvl = objDs.getColumn(i, "melvl");
        							
        							if(stLvl < nextLvl){
        								objDs.setColumn(i, "meorder", nCnt * 10);
        							}else{
        								break;
        							}
        							
        							nCnt++;
        						}
        					}
        				}
        			}
        		}
        		
        		this.gv_tree.setCellProperty("body", 0, "displaytype", "normal");
        		this.gv_tree.setCellProperty("body", 0, "displaytype", "tree");
        		
        		return true;
        	}
        }

        /* div_splitTop_grd_tree_ondrag 실행 */
        this.div_splitTop_grd_tree_ondrag = function(obj,e)
        {
        	if(obj == e.fromobject){
        		var arrData = [];
        		arrData[0] = e.row;
        		arrData[1] = obj.name;
        		arrData[2] = obj;
        		
        		e.dragdata = arrData;
        		
        		return true;
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {

        	if ( this.ds_header.rowcount < 1) {
        		this.gfn_alert("MSG_90001");
        		return;
        	}
        	
        	var oArg = { argFlag:"H"
        	            ,menuId:this.parent.gv_menuId
        	           };
        	this.gfn_popup("MyMenuSetupPop", "system::MyMenuSetupPop.xfdl", oArg, 580, 279, "");
        }

        /* div_splitTop_btn_update_onclick 실행 */
        this.div_splitTop_btn_update_onclick = function(obj,e)
        {
        	if (this.ds_tree.rowposition < 0 || this.ds_tree.rowposition > this.ds_tree.rowcount) {
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		return;
        	}
        	var oArg = { argFlag:"U"
        				,menuId:this.parent.gv_menuId
        				,row:this.ds_tree.rowposition
        			   };
        	this.gfn_popup("MyMenuSetupPop", "system::MyMenuSetupPop.xfdl", oArg, 580, 279, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.addEventHandler("onsize", this.div_splitTop_onsize, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitTop.grd_tree.addEventHandler("ondrop", this.div_splitTop_grd_tree_ondrop, this);
            this.div_splitTop.grd_tree.addEventHandler("ondrag", this.div_splitTop_grd_tree_ondrag, this);
            this.div_splitTop.grd_header.addEventHandler("ondrag", this.div_splitTop_grd_header_ondrag, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.btn_update.addEventHandler("onclick", this.div_splitTop_btn_update_onclick, this);

        };

        this.loadIncludeScript("MyMenuSetup.xfdl", true);

       
    };
}
)();
