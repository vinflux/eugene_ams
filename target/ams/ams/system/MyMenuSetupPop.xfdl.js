﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("MyMenuSetupPop");
                this.set_classname("style01");
                this.set_titletext("나의 메뉴 설정 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,279);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_tree", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"mename\" type=\"STRING\" size=\"32\"/><Column id=\"comments\" type=\"STRING\" size=\"256\"/><Column id=\"meorder\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"melvl\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"URKEY\" type=\"STRING\" size=\"32\"/><Column id=\"uskey\" type=\"STRING\" size=\"32\"/><Column id=\"uppermekey\" type=\"STRING\" size=\"32\"/><Column id=\"mekey\" type=\"STRING\" size=\"32\"/><Column id=\"isleaf\" type=\"STRING\" size=\"32\"/><Column id=\"eqtype\" type=\"STRING\" size=\"32\"/><Column id=\"isseparator\" type=\"STRING\" size=\"32\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"mename\"/><Col id=\"comments\"/><Col id=\"meorder\"/><Col id=\"melvl\"/><Col id=\"URKEY\"/><Col id=\"uskey\"/><Col id=\"uppermekey\"/><Col id=\"mekey\"/><Col id=\"isleaf\"/><Col id=\"eqtype\"/><Col id=\"isseparator\"/><Col id=\"CHK\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("6");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_mekey", "absolute", "222", "62", "321", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("7");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_mename", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchEqtype", "absolute", "334", "124", "24", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_eqtype", "absolute", "222", "124", "110", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "151", "342", "62", null, null, this);
            obj.set_taborder("238");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static5", "absolute", "22", "151", "194", "62", null, null, this);
            obj.set_taborder("240");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_eqtypename", "absolute", "360", "124", "183", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_comments", "absolute", "222", "155", "321", "53", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 279, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("나의 메뉴 설정 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_mekey","value","ds_tree","mekey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","MENU");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_mename","value","ds_tree","mename");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_eqtype","value","ds_tree","eqtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static","text","gds_lang","MEKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static2","text","gds_lang","EQTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","MENAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","static5","text","gds_lang","COMMENTS");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","txa_comments","value","ds_tree","comments");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("MyMenuSetupPop.xfdl", "lib::Comm.xjs");
        this.registerScript("MyMenuSetupPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : MyMenuSetupPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 나의 메뉴 설정 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *  2018.10.02   김진희       즉시저장되도록 수정
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_row = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_row      = this.gfn_isNullEmpty(this.parent.row);
        	
            if(this.gv_flag == "U"){
        		this.ds_tree.copyRow(0, this.opener.ds_tree, this.gv_row);
        		
        		var key = "adcd_hdkey";
        		var value = "EQTYPE";
        		
        		if(this.gfn_isNotNull(this.edt_eqtype.value)){
        			this.gv_code = "";
        			this.gfn_codeSearch(key, value, this.edt_eqtype.value, "20", application.gv_ams, "", "", this.edt_eqtype, this.edt_eqtypename);
        		}
            }/* 2018.10.02 수정되도록 요청else{
        		this.txa_comments.set_enable(false);
            }*/

        	this.ds_tree.applyChange();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_tree;
        	var dsCol = "mename|eqtype";
        	var sComp = "edt_mename|edt_eqtype";
        	
        	if(this.gfn_isNull(this.edt_eqtypename.value) && !this.gfn_isNull(this.edt_eqtype.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_eqtype.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_tree)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						if(this.gv_flag == "H"){
        							var newId = 1;
        							var newmeorder = 0;
        							
        							for(var i = 0 ; i < this.opener.ds_tree.rowcount ; i++){
        								if(this.opener.ds_tree.getColumn(i, "mekey").substr(0, 2) == "MM"){
        									if(nexacro.toNumber(this.opener.ds_tree.getColumn(i, "mekey").substr(2, 8)) >= newId){
        										newId = nexacro.toNumber(this.opener.ds_tree.getColumn(i, "mekey").substr(2, 8)) + 1;
        									}
        								}
        								
        								if(this.opener.ds_tree.getColumn(i, "melvl") == "0"){
        									newmeorder = nexacro.toNumber(this.opener.ds_tree.getColumn(i, "meorder")) + 10;
        								}
        							}
        							
        							if(newId == 1) newId = "MM" + (newId + 10000000);
        							else newId = "MM" + newId;
        							
        							var nRow = this.opener.ds_tree.addRow();
        							this.opener.ds_tree.setColumn(nRow, "mekey", newId);
        							this.opener.ds_tree.setColumn(nRow, "mename", this.edt_mename.value);
        							this.opener.ds_tree.setColumn(nRow, "meorder", newmeorder);
        							this.opener.ds_tree.setColumn(nRow, "eqtype", this.edt_eqtype.value);
        							this.opener.ds_tree.setColumn(nRow, "melvl", "0");
        							this.opener.ds_tree.setColumn(nRow, "uskey", "DIR");
        							this.opener.ds_tree.setColumn(nRow, "isleaf", "N");
        							this.opener.ds_tree.setColumn(nRow, "comments", this.txa_comments.value);
        							this.opener.ds_tree.setColumn(nRow, "uppermekey", "0");
        							this.opener.ds_tree.setColumn(nRow, "isseparator", "N");
        						}else if(this.gv_flag == "U"){
        							this.opener.ds_tree.setColumn(this.gv_row, "mename", this.edt_mename.value);
        							this.opener.ds_tree.setColumn(this.gv_row, "eqtype", this.edt_eqtype.value);
        							this.opener.ds_tree.setColumn(this.gv_row, "comments", this.txa_comments.value);
        						}
        						
        						this.opener.fn_save();
        						this.close();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_tree)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchEqtype_onclick 실행 */
        this.btn_searchEqtype_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_eqtype
        				,putKey:"adcd_hdkey"
        				,putValue:"EQTYPE"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_tree_oncolumnchanged 실행 */
        this.ds_tree_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "eqtype"){
        		this.edt_eqtypename.set_value(this.gv_Pvalue[1]);
        		this.edt_eqtypename.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        }

        /* edt_eqtype_onchanged 실행 */
        this.edt_eqtype_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "EQTYPE";
        	
        	if(this.gfn_isNotNull(this.edt_eqtype.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_eqtype.value, "20", application.gv_ams, "", "", this.edt_eqtype, this.edt_eqtypename);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_tree.addEventHandler("oncolumnchanged", this.ds_tree_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchEqtype.addEventHandler("onclick", this.btn_searchEqtype_onclick, this);
            this.edt_eqtype.addEventHandler("onchanged", this.edt_eqtype_onchanged, this);

        };

        this.loadIncludeScript("MyMenuSetupPop.xfdl", true);

       
    };
}
)();
