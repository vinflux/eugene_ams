﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("PtlUser");
                this.set_classname("style01");
                this.set_titletext("포탈 사용자관리");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj._setContents("<ColumnInfo><Column id=\"cd_company\" type=\"STRING\" size=\"256\"/><Column id=\"nm_company\" type=\"STRING\" size=\"256\"/><Column id=\"urgrkey\" type=\"STRING\" size=\"32\"/><Column id=\"remark\" type=\"STRING\" size=\"0\"/><Column id=\"TOTAL_COUNT\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"regno\" type=\"STRING\" size=\"0\"/><Column id=\"urkey\" type=\"STRING\" size=\"32\"/><Column id=\"quiescenceyn\" type=\"STRING\" size=\"32\"/><Column id=\"use_actor\" type=\"STRING\" size=\"0\"/><Column id=\"cp\" type=\"STRING\" size=\"0\"/><Column id=\"email\" type=\"STRING\" size=\"0\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"urgrname\" type=\"STRING\" size=\"32\"/><Column id=\"apprflag\" type=\"STRING\" size=\"32\"/><Column id=\"urname\" type=\"STRING\" size=\"32\"/><Column id=\"rep_ackey\" type=\"STRING\" size=\"0\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"urpw\" type=\"STRING\" size=\"256\"/><Column id=\"usrtyp\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"owkeym\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_save", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_urgName", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "0", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj.set_autoupdatetype("comboselect");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"0\" band=\"left\"/><Column size=\"30\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"152\"/><Column size=\"110\"/><Column size=\"110\"/><Column size=\"120\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"120\"/><Column size=\"130\"/><Column size=\"249\"/><Column size=\"145\"/><Column size=\"120\"/><Column size=\"145\"/><Column size=\"120\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"normal\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\"/><Cell col=\"2\" displaytype=\"normal\" text=\"NO\"/><Cell col=\"3\" displaytype=\"normal\" text=\"APPROVALYN\"/><Cell col=\"4\" displaytype=\"normal\" text=\"INACTIVE_ACCOUNT_YN\"/><Cell col=\"5\" displaytype=\"normal\" text=\"URGRNAME\"/><Cell col=\"6\" displaytype=\"normal\" text=\"CUST_CD\"/><Cell col=\"7\" displaytype=\"normal\" text=\"ACKEY_NAME\"/><Cell col=\"8\" displaytype=\"normal\" text=\"REGNO\"/><Cell col=\"9\" displaytype=\"normal\" text=\"URNAME\"/><Cell col=\"10\" displaytype=\"normal\" text=\"URKEY\"/><Cell col=\"11\" displaytype=\"normal\" text=\"CP\"/><Cell col=\"12\" displaytype=\"normal\" text=\"EMAIL\"/><Cell col=\"13\" displaytype=\"normal\" text=\"REMARK\"/><Cell col=\"14\" text=\"INSERTDATE\"/><Cell col=\"15\" text=\"INSERTURKEY\"/><Cell col=\"16\" text=\"UPDATEDATE\"/><Cell col=\"17\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:center;background2: ;\" expr=\"currow+1\"/><Cell col=\"3\" edittype=\"none\" style=\"align:center;\" text=\"bind:apprflag\" editdisplay=\"edit\"/><Cell col=\"4\" style=\"align:center;\" text=\"bind:quiescenceyn\"/><Cell col=\"5\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;cursor: ;\" text=\"bind:urgrkey\" expandsize=\"20\" combodataset=\"ds_urgName\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"6\" style=\"align:left;background2: ;\" text=\"bind:cd_company\"/><Cell col=\"7\" displaytype=\"normal\" style=\"align:left;background2: ;\" text=\"bind:nm_company\" calendardisplaynulltype=\"default\"/><Cell col=\"8\" displaytype=\"normal\" style=\"align:center;\" text=\"bind:regno\" calendardisplaynulltype=\"default\"/><Cell col=\"9\" style=\"align:center;\" text=\"bind:urname\"/><Cell col=\"10\" style=\"align:left;background2: ;\" text=\"bind:urkey\"/><Cell col=\"11\" style=\"align:center;\" text=\"bind:cp\"/><Cell col=\"12\" style=\"align:left;background2: ;\" text=\"bind:email\"/><Cell col=\"13\" style=\"align:left;background2: ;\" text=\"bind:remark\"/><Cell col=\"14\" displaytype=\"date\" style=\"align:center;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"15\" text=\"bind:inserturkey\"/><Cell col=\"16\" displaytype=\"date\" style=\"align:center;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"17\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_addUser", "absolute", null, "0", "80", "24", "121", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_tooltiptext("추가");
            obj.set_text("승인");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_inActiveUserClear", "absolute", null, "0", "80", "24", "37", null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("휴면해제");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("3");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "0", "7", "190", "20", null, null, this);
            obj.set_taborder("4");
            obj.set_text("포탈 회원 내역");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("포탈 사용자관리");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_addUser","tooltiptext","gds_lang","APPROVAL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitTop.btn_addUser","text","gds_lang","APPROVAL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_inActiveUserClear","tooltiptext","gds_lang","INACTIVE_ACCOUNT_CLEAR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitTop.btn_inActiveUserClear","text","gds_lang","INACTIVE_ACCOUNT_CLEAR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","Static00","text","gds_lang","ME00111841_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("PotalUser.xfdl", "lib::Comm.xjs");
        this.registerScript("PotalUser.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME : PotalUser.xfdl
        * PROGRAMMER : jkc
        * DATE : 2018.06.11
        * DESCRIPTION : 포탈 사용자
        *------------------------------------------------------------------
        * MODIFY DATE PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_page = this.div_splitTop.div_Paging;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        	
        	//this.gfn_getCode("USE_ACTOR", this.ds_useActor, "");

        
        /*사용자 그룹명.*/
        	this.gfn_getCommCodeTran( ""
        						, "1136"
        						, "ds_urgName"
        						, ""
        						, ""
        						, ""
        						);
        	
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gv_header.set_nodatatext("");
        	this.ds_header.clearData();
        	
        	this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        	
        	this.gfn_setCommon("BEANID", "ptlUserController");
        	this.gfn_setCommon("METHODNM", "selectPtlUserInfoList");
        	
        var sSvcId = "select";
        var sSvcUrl = application.gv_ams + application.gv_sUrl;
        var sInData = "";
        var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        var sParam = "";
        this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        /* 승인처리 */
        this.fn_save= function()
        {
        this.ds_header.set_enableevent( false );
        for(var i = 0 ; i<this.ds_header.rowcount;i++) {
        if( this.ds_header.getColumn( i, "CHK" ) != "1") { continue; }
        this.ds_header.setColumn( i , "apprflag" , "Y" );
        }
        this.ds_header.set_enableevent( true );

        	this.gfn_setCommon("BEANID", "ptlUserController");
        	this.gfn_setCommon("METHODNM", "authUser");
         	
        var sSvcId = "save";
        var sSvcUrl = application.gv_ams + application.gv_sUrl;
        var sInData = "IN_FORM_DATA=ds_header:U";
        var sOutData = "";
        var sParam = "";
        this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        /* 휴면해제처리*/
        this.fn_inActiveUserClear = function (){
        this.ds_header.set_enableevent( false );
        for(var i = 0 ; i<this.ds_header.rowcount;i++) {
        			if( this.ds_header.getColumn( i, "CHK" ) != "1") { continue; }
        			this.ds_header.setColumn( i , "quiescenceyn" , "N" );
        }
        this.ds_header.set_enableevent( true );

        	this.gfn_setCommon("BEANID", "ptlUserController");
        	this.gfn_setCommon("METHODNM", "inActiveUserClear");
         	
        var sSvcId = "clear";
        var sSvcUrl = application.gv_ams + application.gv_sUrl;
        var sInData = "IN_FORM_DATA=ds_header:U";
        var sOutData = "";
        var sParam = "";
        this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {

        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        			
        			this.ds_header.addColumn("CHK");
        		}else{
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "save"){
        		this.gfn_alert("MSG_ALERT_SUCCAPPROVAL");
        		this.fn_search();
        	}else if(sSvcId == "clear"){
        		this.gfn_alert("MSG_ALERT_SUCCCLEAR");//정상적으로 해제 처리되었습니다.
        		this.fn_search();
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	 this.gfn_grdHeaderClick(obj, e, this);
        }

        /* div_splitTop_btn_excel_onclick 실행 */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {};
        	this.parent.div_cond.fn_excelSearch("ptlUserController/excelDown.do", this.gv_header, this, oValue);
        }

        /* 승인버튼 클릭 */
        this.div_splitTop_btn_addUser_onclick = function(obj,e)
        {
        	var func_MSG_ALREADY_CONFIRMED = function( form ){ // 선택 안됨.
        		var flagN = form.ds_header.findRowExpr ("apprflag == 'N' && CHK == '1'");
        		var flagY = form.ds_header.findRowExpr ("apprflag == 'Y' && CHK == '1'");
        		var nRow = form.ds_header.findRowExpr("CHK==1");
        		trace("flagN:::"+flagN);
        		trace("flagY:::"+flagY);
        		trace("nRow:::"+nRow);
        		
        		
        		if(nRow >-1){
        			if(flagN > -1 && flagY > -1){
        				form.gfn_alert("MSG_MIX_ALREADY_CONFIRMED");
        			}else if(flagY > -1){
        				form.gfn_alert("MSG_ALREADY_CONFIRMED");
        			}else{
        				func_MSG_CHKDATA_NOTUPDATE(form);
        			}
        		}else{
        			func_MSG_CHKDATA_NOSELECT(form)
        		}
        	
        	}
        	
        	
        	var func_MSG_CHKDATA_NOSELECT = function( form ){ // 선택 안됨.
        		 var nRow = form.ds_header.findRow("CHK", "1");
        		 if(nRow == -1){
        			 form.gfn_alert("MSG_CHKDATA_NOSELECT","", function(){
        					
        			 });
        		 }else{
        		 func_MSG_CHKDATA_NOTUPDATE(form);
        		 }
        	};
        var func_MSG_CHKDATA_NOTUPDATE = function( form) {
        			var flagN = form.ds_header.findRowExpr ("apprflag == 'N' && CHK == '1'");
        			if(flagN > -1){
        				func_MSG_NULL_CHECK( form );
        			}else{
        				 if( !form.gfn_isUpdate(form.ds_header)){
        					 form.gfn_alert("MSG_CHKDATA_NOTUPDATE" ); // 수정한 내역
        				 }else{
        					 func_MSG_NULL_CHECK( form );
        				 }
        			}
        };
        var func_MSG_NULL_CHECK = function( form) { // null 체크
        			 var dsObj = form.gv_header.getBindDataset();
        			 var sDsCol = "urgrkey";

        			 if( form.gfn_isDataNullCheck(form.gv_header, dsObj, sDsCol)){
        func_MSG_ALERT_APPROVAL( form );
        			 }
        			
        };
        var func_MSG_ALERT_APPROVAL = function( form ) {
        			 form.gfn_confirm("MSG_ALERT_APPROVAL", "", function(msg, flag){// 승인요청 confirm
        					if(flag){
        						 form.fn_save();
        					}
        			 });
        };
        func_MSG_ALREADY_CONFIRMED( this );
        }

        /* 휴면해제클릭시 */
        this.btn_inActiveUserClear_onclick = function(obj,e)
        {

        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ALERT_INACTCLEAR", "", function(msg, flag){//휴면해제 하시겠습니까?
        			if(flag){
        				this.fn_inActiveUserClear();
        			}
        		});
        	}
        }

        
        /* div_splitTop_btn_cancelUser_onclick 실행 */
        this.div_splitTop_btn_cancelUser_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		var dFlag = true;
        		for(var i =0; i< this.ds_header.rowcount; i++) {
        			if('1' == this.ds_header.getColumn(i,"CHK") && 'N' == this.ds_header.getColumn(i,"scm_user_yn"))
        			{
        				dFlag = false;
        				break;
        			}
        		}
        		
        		if(dFlag) {
        			this.gfn_confirm("MSG_ALERT_SCMUSER_CANCEL", "", function(msg, flag){
        				if(flag){
        					this.fn_delete();
        				}
        			});
        		} else {
        			this.gfn_alert("MSG_ALERT_SCMUSER_CANCEL_NO");
        		}
        		
        	}
        }

        this.div_splitTop_grd_header_oncellclick = function(obj,e)
        {
        obj.dropdownCombo();
        }

        /* 사용자그룹명 팝업 호출 */
        this.div_splitTop_grd_header_onexpandup = function(obj,e)
        {
         	
        		var oArg = { divId:"Grid"
        					,searchId:70
        					,putObj:this.ds_header
        					,putCol:"urgrkey"
        					,putRow:""+e.row
        					,putKey:""
        					,putValue:this.gfn_getUserInfo("ctKey")
        				 };
        		this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }
        /* 팝업에서 그룹키 가져온 후 그룹명 조회 ㅂㅅ*/
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var key = "ctkey";
        	var value = this.gfn_getUserInfo("ctKey");
        	
        	if( e.columnid == "urgrkey" ) {
        this.gfn_codeSearch(key, value, e.newvalue, "70", application.gv_ams, "", "", "", "", obj, "urgrname", e.row);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_header.addEventHandler("onexpandup", this.div_splitTop_grd_header_onexpandup, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.btn_addUser.addEventHandler("onclick", this.div_splitTop_btn_addUser_onclick, this);
            this.div_splitTop.btn_inActiveUserClear.addEventHandler("onclick", this.btn_inActiveUserClear_onclick, this);

        };

        this.loadIncludeScript("PotalUser.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
