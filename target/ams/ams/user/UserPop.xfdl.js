﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UserPop");
                this.set_classname("style01");
                this.set_titletext("사용자 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,781);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_userInfo", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"divcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"tzkey\" type=\"STRING\" size=\"32\"/><Column id=\"urkey\" type=\"STRING\" size=\"32\"/><Column id=\"dst\" type=\"STRING\" size=\"32\"/><Column id=\"loginfailcount\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"usrtyp\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"rstclgst\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"urname\" type=\"STRING\" size=\"32\"/><Column id=\"expiredate\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"lakey\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcdname\" type=\"STRING\" size=\"32\"/><Column id=\"ipaddr\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"ctkey\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"cp\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"rep_ackey\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"32\"/><Column id=\"urgrkey\" type=\"STRING\" size=\"32\"/><Column id=\"rstcdiv\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctname\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"urgrname\" type=\"STRING\" size=\"32\"/><Column id=\"owkeym\" type=\"STRING\" size=\"32\"/><Column id=\"urpw1\" type=\"STRING\" size=\"256\"/><Column id=\"urpw2\" type=\"STRING\" size=\"256\"/><Column id=\"urpw\" type=\"STRING\" size=\"256\"/><Column id=\"urpartner\" type=\"STRING\" size=\"256\"/><Column id=\"urpartnername\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"divcd\"/><Col id=\"tzkey\"/><Col id=\"urkey\"/><Col id=\"dst\"/><Col id=\"loginfailcount\"/><Col id=\"usrtyp\"/><Col id=\"rstclgst\"/><Col id=\"inserturkey\"/><Col id=\"urname\"/><Col id=\"expiredate\"/><Col id=\"delyn\"/><Col id=\"updatedate\"/><Col id=\"lakey\"/><Col id=\"loggrpcdname\"/><Col id=\"ipaddr\"/><Col id=\"closingdate\"/><Col id=\"ctkey\"/><Col id=\"cp\"/><Col id=\"rep_ackey\"/><Col id=\"loggrpcd\"/><Col id=\"urgrkey\"/><Col id=\"rstcdiv\"/><Col id=\"ctname\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/><Col id=\"urgrname\"/><Col id=\"owkeym\"/><Col id=\"urpw1\"/><Col id=\"urpw2\"/><Col id=\"urpw\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_usrtyp", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_popData", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("25");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("26");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("24");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("22");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("27");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("28");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("29");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_id", "absolute", "222", "62", "260", "22", null, null, this);
            obj.set_taborder("0");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("30");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_name", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("2");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "72", this);
            obj.set_taborder("31");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("23");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("32");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "62", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("33");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("34");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchUrgr", "absolute", "334", "124", "24", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urgr", "absolute", "222", "124", "110", "22", null, null, this);
            obj.set_taborder("35");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("36");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pw1", "absolute", "222", "155", "321", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_password("true");
            this.addChild(obj.name, obj);

            obj = new Static("sta_pw1", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("37");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("38");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_pw2", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_password("true");
            this.addChild(obj.name, obj);

            obj = new Static("sta_pw2", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("39");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "275", "342", "31", null, null, this);
            obj.set_taborder("40");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static5", "absolute", "22", "275", "194", "31", null, null, this);
            obj.set_taborder("41");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_expire", "absolute", "222", "279", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("6");
            obj.set_enable("false");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.set_value("20391231235900000");
            obj.set_editformat("yyyy-MM-dd HH:mm");

            obj = new Static("Static07", "absolute", "216", "306", "342", "31", null, null, this);
            obj.set_taborder("42");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static6", "absolute", "22", "306", "194", "31", null, null, this);
            obj.set_taborder("43");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_close", "absolute", "222", "310", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("7");
            obj.set_dateformat("yyyy-MM-dd HH:mm");
            obj.set_editformat("yyyy-MM-dd HH:mm");
            obj.set_enable("true");
            obj.set_value("null");

            obj = new Static("static7", "absolute", "22", "337", "194", "31", null, null, this);
            obj.set_taborder("44");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "216", "337", "342", "31", null, null, this);
            obj.set_taborder("45");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchLang00", "absolute", "334", "341", "24", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_lang", "absolute", "222", "341", "110", "22", null, null, this);
            obj.set_taborder("46");
            obj.set_enable("false");
            obj.set_value("KOR");
            this.addChild(obj.name, obj);

            obj = new Static("static8", "absolute", "22", "368", "194", "31", null, null, this);
            obj.set_taborder("47");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "216", "368", "342", "31", null, null, this);
            obj.set_taborder("48");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchLang01", "absolute", "334", "372", "24", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_utc", "absolute", "222", "372", "110", "22", null, null, this);
            obj.set_taborder("49");
            obj.set_enable("false");
            obj.set_value("UTC+0900");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "216", "399", "342", "31", null, null, this);
            obj.set_taborder("50");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static9", "absolute", "22", "399", "194", "31", null, null, this);
            obj.set_taborder("51");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_dst", "absolute", "222", "403", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("10");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_enable("false");

            obj = new Static("static10", "absolute", "22", "430", "194", "31", null, null, this);
            obj.set_taborder("52");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "216", "430", "342", "31", null, null, this);
            obj.set_taborder("53");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCtkey", "absolute", "334", "434", "24", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctkey", "absolute", "222", "434", "110", "22", null, null, this);
            obj.set_taborder("54");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("static11", "absolute", "22", "461", "194", "31", null, null, this);
            obj.set_taborder("55");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "216", "461", "342", "31", null, null, this);
            obj.set_taborder("56");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "334", "465", "24", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkeym", "absolute", "222", "465", "110", "22", null, null, this);
            obj.set_taborder("57");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "22", "492", "194", "31", null, null, this);
            obj.set_taborder("58");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "216", "492", "342", "31", null, null, this);
            obj.set_taborder("59");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchLog", "absolute", "334", "496", "24", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_loggrpcd", "absolute", "222", "496", "110", "22", null, null, this);
            obj.set_taborder("60");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "216", "523", "342", "31", null, null, this);
            obj.set_taborder("61");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static13", "absolute", "22", "523", "194", "31", null, null, this);
            obj.set_taborder("62");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_usrtyp", "absolute", "222", "527", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("14");
            obj.set_innerdataset("@ds_usrtyp");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static16", "absolute", "216", "584", "342", "31", null, null, this);
            obj.set_taborder("63");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_rep_ackey", "absolute", "222", "588", "321", "22", null, null, this);
            obj.set_taborder("17");
            this.addChild(obj.name, obj);

            obj = new Static("static14", "absolute", "22", "584", "194", "31", null, null, this);
            obj.set_taborder("64");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "216", "615", "342", "31", null, null, this);
            obj.set_taborder("65");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cp", "absolute", "222", "619", "321", "22", null, null, this);
            obj.set_taborder("18");
            this.addChild(obj.name, obj);

            obj = new Static("static15", "absolute", "22", "615", "194", "31", null, null, this);
            obj.set_taborder("66");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "216", "646", "342", "31", null, null, this);
            obj.set_taborder("67");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static16", "absolute", "647", "564", "194", "31", null, null, this);
            obj.set_taborder("68");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "216", "677", "342", "31", null, null, this);
            obj.set_taborder("69");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static17", "absolute", "22", "677", "194", "31", null, null, this);
            obj.set_taborder("70");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_delyn", "absolute", "222", "681", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("21");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_index("-1");

            obj = new Static("sta_detname09", "absolute", "22", "646", "194", "31", null, null, this);
            obj.set_taborder("71");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_rstcd", "absolute", "847", "568", "22", "22", null, null, this);
            obj.set_taborder("19");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_rstclgst", "absolute", "222", "649", "22", "22", null, null, this);
            obj.set_taborder("20");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urgrname", "absolute", "360", "124", "183", "22", null, null, this);
            obj.set_taborder("72");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_langname", "absolute", "360", "341", "183", "22", null, null, this);
            obj.set_taborder("73");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_utcname", "absolute", "360", "372", "183", "22", null, null, this);
            obj.set_taborder("74");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctname", "absolute", "360", "434", "183", "22", null, null, this);
            obj.set_taborder("75");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "360", "465", "183", "22", null, null, this);
            obj.set_taborder("76");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_loggrpname", "absolute", "360", "496", "183", "22", null, null, this);
            obj.set_taborder("77");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static20", "absolute", "22", "244", "194", "31", null, null, this);
            obj.set_taborder("78");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "216", "244", "342", "31", null, null, this);
            obj.set_taborder("79");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_loginfailcount", "absolute", "222", "248", "212", "22", null, null, this);
            obj.set_taborder("80");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_pwInit", "absolute", "437", "248", "105", "22", null, null, this);
            obj.set_taborder("81");
            obj.set_text("비밀번호 초기화");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "22", "213", "536", "31", null, null, this);
            obj.set_taborder("82");
            obj.set_cssclass("sta_WF_bg");
            obj.set_text("영문과 숫자가 최소 1자리이상은 포함, 사용자ID와 3자리이상 동일한 값을 포함할 수 없음");
            obj.set_wordwrap("char");
            this.addChild(obj.name, obj);

            obj = new Static("static23", "absolute", "22", "554", "194", "31", null, null, this);
            obj.set_taborder("83");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "216", "554", "342", "31", null, null, this);
            obj.set_taborder("84");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchUrpt", "absolute", "334", "558", "24", "22", null, null, this);
            obj.set_taborder("16");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urpartner", "absolute", "222", "558", "110", "22", null, null, this);
            obj.set_taborder("15");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urpartnername", "absolute", "360", "558", "183", "22", null, null, this);
            obj.set_taborder("85");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 781, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_id","value","ds_userInfo","urkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_name","value","ds_userInfo","urname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","edt_urgr","value","ds_userInfo","urgrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_pw1","value","ds_userInfo","urpw1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_pw2","value","ds_userInfo","urpw2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","cal_expire","value","ds_userInfo","expiredate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cal_close","value","ds_userInfo","closingdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_lang","value","ds_userInfo","lakey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","edt_utc","value","ds_userInfo","tzkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","cbo_dst","value","ds_userInfo","dst");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","edt_ctkey","value","ds_userInfo","ctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","edt_owkeym","value","ds_userInfo","owkeym");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_loggrpcd","value","ds_userInfo","loggrpcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","cbo_usrtyp","value","ds_userInfo","usrtyp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","edt_rep_ackey","value","ds_userInfo","rep_ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","edt_cp","value","ds_userInfo","cp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","cbo_delyn","value","ds_userInfo","delyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","sta_text00","text","gds_lang","");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","static","text","gds_lang","URKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","static1","text","gds_lang","URNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","static2","text","gds_lang","URGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","sta_pw1","text","gds_lang","URPW1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","sta_pw2","text","gds_lang","URPW2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","static5","text","gds_lang","EXPIREDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","static6","text","gds_lang","CLOSINGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","static7","text","gds_lang","LAKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","static8","text","gds_lang","TZKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","static9","text","gds_lang","DST");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item30","static10","text","gds_lang","CTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","static11","text","gds_lang","OWKEYM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item32","static12","text","gds_lang","LOGGRPCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","static13","text","gds_lang","USRTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","static14","text","gds_lang","REP_ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item35","static15","text","gds_lang","CP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item36","static16","text","gds_lang","RSTCDIV");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","sta_detname09","text","gds_lang","RSTCLGST");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","static17","text","gds_lang","DELYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","lab_title","text","gds_lang","USER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static20","text","gds_lang","LOGINFAILCOUNT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","edt_loginfailcount","value","ds_userInfo","loginfailcount");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item40","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item41","btn_pwInit","text","gds_lang","USR_PWD_INIT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","static23","text","gds_lang","URPARTNER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","edt_urpartner","value","ds_userInfo","urpartner");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","edt_urpartnername","value","ds_userInfo","urpartnername");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("UserPop.xfdl", "lib::Comm.xjs");
        this.registerScript("UserPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : UserPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 사용자 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_urkey = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("USRTYP", this.ds_usrtyp, "");
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	
        	this.ds_userInfo.setColumn(0, "expiredate", "203912312359");
        	this.ds_userInfo.setColumn(0, "lakey", "KOR");
        	this.ds_userInfo.setColumn(0, "tzkey", "UTC+0900");
        	this.ds_userInfo.setColumn(0, "dst", "N");
        	this.ds_userInfo.setColumn(0, "delyn", "N");
        	
            this.gv_flag = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_urkey = this.gfn_isNullEmpty(this.parent.urkey);

        	this.ds_userInfo.applyChange();
            
            if(this.gv_flag == "U"){
        		this.edt_id.set_enable(false);
        		this.fn_search();
            }else{
        		this.btn_pwInit.set_enable(false);
        		this.sta_pw1.set_cssclass("sta_WF_head_duty");
        		this.sta_pw2.set_cssclass("sta_WF_head_duty");
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_userInfo.clearData();
        	
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "selectUserForUpdate");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_userInfo=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   = "urkey="+this.gv_urkey;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "checkUserInfo");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "urkey="+this.ds_userInfo.getColumn(0, "urkey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_PwInit = function()
        {
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "actionInitUrpwInit");
        	
            var sSvcId   = "pwinit";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "";
            var sParam   = "urkey="+this.ds_userInfo.getColumn(0, "urkey");
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "saveUser");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_userInfo";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "userController");
        	this.gfn_setCommon("METHODNM", "saveUser");
        	
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_userInfo";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_userInfo.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}else{
        			this.chk_rstcd.set_value(this.gfn_booleanYNConvert(this.ds_userInfo.getColumn(0, "rstcdiv")));
        			this.chk_rstclgst.set_value(this.gfn_booleanYNConvert(this.ds_userInfo.getColumn(0, "rstclgst")));
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_urgr.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_urgr.value, "70", application.gv_ams, "", "", this.edt_urgr, this.edt_urgrname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "LAKEY";
        		
        		if(this.gfn_isNotNull(this.edt_lang.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_lang.value, "20", application.gv_ams, "", "", this.edt_lang, this.edt_langname);
        		}
        		
        		key = "";
        		value = "";
        		
        		if(this.gfn_isNotNull(this.edt_utc.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_utc.value, "180", application.gv_ams, "", "", this.edt_utc, this.edt_utcname);
        		}
        		
        		key = "";
        		value = "";
        		
        		if(this.gfn_isNotNull(this.edt_ctkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_ctkey.value, "60", application.gv_ams, "", "", this.edt_ctkey, this.edt_ctname);
        		}
        		
        		key = "";
        		value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkeym.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkeym.value, "50", application.gv_ams, "", "", this.edt_owkeym, this.edt_owname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "LOGGRPCD";
        		
        		if(this.gfn_isNotNull(this.edt_loggrpcd.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_loggrpcd.value, "20", application.gv_ams, "", "", this.edt_loggrpcd, this.edt_loggrpname);
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        //				this.edt_id.set_value("");
        				this.edt_id.setFocus();
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        		}
        	}else if(sSvcId == "pwinit"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_search();
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_userInfo;
        	var dsCol = "";
        	var sComp = "";
        	var chkCnt = 0;
        	var nNum = 0;
        	var nEng = 0;
        	var sPw = this.edt_pw1.value;
        	
        	if(this.gv_flag == "H"){
        		dsCol = "urkey|urname|urgrkey|urpw1|urpw2|delyn|urpartner";
        		sComp = "edt_id|edt_name|edt_urgr|edt_pw1|edt_pw2|cbo_delyn|edt_urpartner";
        	}else{
        		dsCol = "urkey|urname|urgrkey|delyn|urpartner";
        		sComp = "edt_id|edt_name|edt_urgr|cbo_delyn|edt_urpartner";
        	}
        	 
        	/*
        	if(this.gfn_isNull(this.edt_urgrname.value) && !this.gfn_isNull(this.edt_urgr.value)){
        			//사용자 그룹코드 
        		//this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_urgr.setFocus();
        			
        		//});
        			return;
        	}else if(this.gfn_isNull(this.edt_ctname.value) && !this.gfn_isNull(this.edt_ctkey.value)){	
        		//센터코드
        	//	this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_ctkey.setFocus();
        		//});
        			return;
         	}else if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkeym.value)){
        		//화주 코드
        // 		this.gfn_alert("MSG_80300", "", function(msg, flag){
         			this.edt_owkeym.setFocus(); 			
        // 		});
        			return;
         	}else if(this.gfn_isNull(this.edt_loggrpname.value) && !this.gfn_isNull(this.edt_loggrpcd.value)){
        		 //상/저온 구분
        // 		this.gfn_alert("MSG_80300", "", function(msg, flag){
         			this.edt_loggrpcd.setFocus();
        // 		});
        			return;
        	}
        	*/
        	
        	if((this.ds_param.rowcount == 0 || this.ds_param.getColumn(0, "isDuplicate") == "true") && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_id.setFocus();
        		});
        	}else if(this.gfn_isNotNull(sPw)){
        		// MSG_ALERT_CHECK_PW : #{pw}는 사용자ID #{id}와 3자리이상 동일한 값을 포함하므로 사용할수 없습니다.
        		for(var i = 0 ; i+2 < sPw.length ; i++){
        			if(dsObj.getColumn(0, "urkey").indexOf(sPw.substr(i, 3)) >= 0){
        				chkCnt++;
        			}
        		}
        		
        		if(chkCnt > 0){
        			this.gfn_alert("MSG_ALERT_CHECK_PW", "pw," + sPw + "|id," + dsObj.getColumn(0, "urkey"), function(msg, flag){
        				this.edt_pw1.setFocus();
        			});
        			
        			return;
        		}
        		
        		// MSG_ALERT_CHECK_PW_ENGNUM : 비밀번호는 영문과 숫자가 최소 1자리이상은 포함되어야 합니다.
        		nNum = sPw.search(/[0-9]/g);
        		nEng = sPw.search(/[A-Za-z]/ig);
        		
        		if(nNum < 0 || nEng < 0){
        			this.gfn_alert("MSG_ALERT_CHECK_PW_ENGNUM", "", function(msg, flag){
        				this.edt_pw1.setFocus();
        			});
        			
        			return;
        		}
        		
        		if(this.gfn_isNull(this.edt_pw2.value)){
        			this.gfn_alert("MSG_10001", "", function(msg, flag){
        				this.edt_pw2.setFocus();
        			});
        		}else if(this.edt_pw1.value != this.edt_pw2.value){
        			this.gfn_alert("입력하신 비밀번호가 서로 다릅니다.", "", function(msg, flag){
        				dsObj.setColumn(0, "urpw1", "");
        				dsObj.setColumn(0, "urpw2", "");
        				this.edt_pw1.setFocus();
        			});
        		}else{
        			this.ds_userInfo.setColumn(0, "urpw", sPw);
        			
        			if(this.gfn_isUpdate(dsObj)){
        				if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        					this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        						if(flag){
        							this.ds_userInfo.setColumn(0, "rstcdiv", this.gfn_booleanYNConvert(this.chk_rstcd.value));
        							this.ds_userInfo.setColumn(0, "rstclgst", this.gfn_booleanYNConvert(this.chk_rstclgst.value));
        							
        							if(this.gv_flag == "H"){
        								this.fn_Insert();
        							}else if(this.gv_flag == "U"){
        								this.fn_Update();
        							}
        						}
        					});
        				}
        			}else{
        				this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        			}
        		}
        	}else{
        		if(this.gfn_isUpdate(dsObj)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						this.ds_userInfo.setColumn(0, "rstcdiv", this.gfn_booleanYNConvert(this.chk_rstcd.value));
        						this.ds_userInfo.setColumn(0, "rstclgst", this.gfn_booleanYNConvert(this.chk_rstclgst.value));
        						
        						if(this.gv_flag == "H"){
        							this.fn_Insert();
        						}else if(this.gv_flag == "U"){
        							this.fn_Update();
        						}
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_userInfo)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* edt_id_onchanged 실행 */
        this.edt_id_onchanged = function(obj,e)
        {
        	if(this.ds_param.rowcount > 0) this.ds_param.setColumn(0, "isDuplicate", "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.ds_param.getColumn(0, "isDuplicate") == "false"){
        		return;
        	}
        	
        	if(this.gfn_isNull(this.edt_id.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_id.setFocus();
        		});
        	}else{
        		// 아이디 공백 제거 처리 - 2017.11.22 : ksh
        		var urkey = this.ds_userInfo.getColumn(0,"urkey").trim();
        		this.ds_userInfo.setColumn(0,"urkey",urkey);
        		this.fn_CheckCode();
        	}
        }

        /* btn_pwInit_onclick 실행 */
        this.btn_pwInit_onclick = function(obj,e)
        {
        	this.gfn_confirm("MSG_ASK_URPW_FAILCNT_INIT", "", function(msg, flag){
        		if(flag){
        			this.fn_PwInit();
        		}
        	});
        }

        /* btn_searchUrgr_onclick 실행 */
        this.btn_searchUrgr_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:70
        				,putObj:this.edt_urgr
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchCtkey_onclick 실행 */
        this.btn_searchCtkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:60
        				,putObj:this.edt_ctkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:""
        				,searchId:50
        				,putObj:this.edt_owkeym
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 547, "");
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchLog_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:""
        				,searchId:20
        				,putObj:this.edt_loggrpcd
        				,putKey:"adcd_hdkey"
        				,putValue:"LOGGRPCD"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 547, "");
        }

        /* chk_rstcd_onchanged 실행 */
        this.chk_rstcd_onchanged = function(obj,e)
        {
        	this.ds_userInfo.setColumn(0, "rstcdiv", this.gfn_booleanYNConvert(obj.value));
        }

        /* chk_rstclgst_onchanged 실행 */
        this.chk_rstclgst_onchanged = function(obj,e)
        {
        	this.ds_userInfo.setColumn(0, "rstclgst", this.gfn_booleanYNConvert(obj.value));
        }

        /* ds_userInfo_oncolumnchanged 실행 */
        this.ds_userInfo_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "urgrkey"){
        		this.edt_urgrname.set_value(this.gv_Pvalue[1]);
        		this.edt_urgrname.set_tooltiptext(this.gv_Pvalue[1]);
        		this.gv_Pvalue = "";
        	}else if(e.columnid == "ctkey"){
        		this.edt_ctname.set_value(this.gv_Pvalue[1]);
        		this.edt_ctname.set_tooltiptext(this.gv_Pvalue[1]);
        		this.gv_Pvalue = "";
        	}else if(e.columnid == "owkeym"){
        		this.edt_owname.set_value(this.gv_Pvalue[1]);
        		this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        		this.gv_Pvalue = "";
        	}else if(e.columnid == "loggrpcd"){
        		this.edt_loggrpname.set_value(this.gv_Pvalue[1]);
        		this.edt_loggrpname.set_tooltiptext(this.gv_Pvalue[1]);
        		this.gv_Pvalue = "";
        	}else if(e.columnid == "urpartner"){
        		this.edt_urpartnername.set_value(this.gv_Pvalue[1]);
        		this.edt_urpartnername.set_tooltiptext(this.gv_Pvalue[1]);
        		this.gv_Pvalue = "";
        	}
        }

        /* edt_urgr_onchanged 실행 */
        this.edt_urgr_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_urgr.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_urgr.value, "70", application.gv_ams, "", "", this.edt_urgr, this.edt_urgrname);
        	}
        }

        /* edt_ctkey_onchanged 실행 */
        this.edt_ctkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_ctkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_ctkey.value, "60", application.gv_ams, "", "", this.edt_ctkey, this.edt_ctname);
        	}
        }

        /* edt_owkeym_onchanged 실행 */
        this.edt_owkeym_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkeym.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkeym.value, "50", application.gv_ams, "", "", this.edt_owkeym, this.edt_owname);
        	}
        }

        /* edt_loggrpcd_onchanged 실행 */
        this.edt_loggrpcd_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "LOGGRPCD";
        	
        	if(this.gfn_isNotNull(this.edt_loggrpcd.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_loggrpcd.value, "20", application.gv_ams, "", "", this.edt_loggrpcd, this.edt_loggrpname);
        	}
        }

        this.btn_searchUrpt_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";

        	var oArg = { divId:"Single"
        				,searchId:1600
        				,putObj:this.edt_urpartner
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402);
        }

        this.cbo_usrtyp_onitemchanged = function(obj,e)
        {
        	if(e.postvalue == "0" || e.postvalue == "1"){
        		this.edt_urpartner.set_value("EUGENESF");
        		this.edt_urpartnername.set_value("유진초저온");
        	}else{
        		this.edt_urpartner.set_value("");
        		this.edt_urpartnername.set_value("");
        	}
        }

        this.edt_urpt_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_urgr.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_urpartner.value, "1600", application.gv_ams, "", "", this.edt_urpartner, this.edt_urpartnername);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_userInfo.addEventHandler("oncolumnchanged", this.ds_userInfo_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_id.addEventHandler("onchanged", this.edt_id_onchanged, this);
            this.edt_id.addEventHandler("oneditclick", this.edt_id_oneditclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.btn_searchUrgr.addEventHandler("onclick", this.btn_searchUrgr_onclick, this);
            this.edt_urgr.addEventHandler("onchanged", this.edt_urgr_onchanged, this);
            this.edt_urgr.addEventHandler("oneditclick", this.edt_urgr_oneditclick, this);
            this.btn_searchLang00.addEventHandler("onclick", this.btn_searchLang_onclick, this);
            this.edt_lang.addEventHandler("oneditclick", this.edt_lang_oneditclick, this);
            this.btn_searchLang01.addEventHandler("onclick", this.btn_searchLang_onclick, this);
            this.edt_utc.addEventHandler("oneditclick", this.edt_utc_oneditclick, this);
            this.btn_searchCtkey.addEventHandler("onclick", this.btn_searchCtkey_onclick, this);
            this.edt_ctkey.addEventHandler("onchanged", this.edt_ctkey_onchanged, this);
            this.edt_ctkey.addEventHandler("oneditclick", this.edt_ctkey_oneditclick, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.edt_owkeym.addEventHandler("onchanged", this.edt_owkeym_onchanged, this);
            this.btn_searchLog.addEventHandler("onclick", this.btn_searchLog_onclick, this);
            this.edt_loggrpcd.addEventHandler("onchanged", this.edt_loggrpcd_onchanged, this);
            this.cbo_usrtyp.addEventHandler("onitemchanged", this.cbo_usrtyp_onitemchanged, this);
            this.chk_rstcd.addEventHandler("onchanged", this.chk_rstcd_onchanged, this);
            this.chk_rstclgst.addEventHandler("onchanged", this.chk_rstclgst_onchanged, this);
            this.edt_urgrname.addEventHandler("oneditclick", this.edt_urgrname_oneditclick, this);
            this.edt_langname.addEventHandler("oneditclick", this.edt_langname_oneditclick, this);
            this.edt_loginfailcount.addEventHandler("onchanged", this.edt_id_onchanged, this);
            this.btn_pwInit.addEventHandler("onclick", this.btn_pwInit_onclick, this);
            this.btn_searchUrpt.addEventHandler("onclick", this.btn_searchUrpt_onclick, this);
            this.edt_urpartner.addEventHandler("oneditclick", this.edt_urgr_oneditclick, this);
            this.edt_urpartner.addEventHandler("onchanged", this.edt_urpt_onchanged, this);
            this.edt_urpartnername.addEventHandler("oneditclick", this.edt_urgrname_oneditclick, this);

        };

        this.loadIncludeScript("UserPop.xfdl", true);

       
    };
}
)();
