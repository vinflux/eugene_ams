﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("agreementPhrase");
                this.set_classname("notice");
                this.set_titletext("약관목록");
                this._setFormPosition(0,0,1230,560);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"usegb\" type=\"STRING\" size=\"256\"/><Column id=\"usegb_desc\" type=\"STRING\" size=\"256\"/><Column id=\"agreegb\" type=\"STRING\" size=\"256\"/><Column id=\"agreegb_desc\" type=\"STRING\" size=\"256\"/><Column id=\"startdt\" type=\"STRING\" size=\"256\"/><Column id=\"content\" type=\"STRING\" size=\"999999\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"applygb\" type=\"STRING\" size=\"256\"/><Column id=\"applygb_desc\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_apkey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_usegb", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_agreegb", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop01", "absolute", "0", "0", null, "229", "0", null, this);
            obj.set_taborder("0");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop01);
            obj.set_taborder("24");
            obj.set_scrollbars("none");
            obj.style.set_gradation("linear 0,0 #ffffffff 0,100 #fafafaff");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, "171", "0", null, this.div_splitTop01);
            obj.set_taborder("25");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_treeuseline("false");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"29\"/><Column size=\"40\"/><Column size=\"135\"/><Column size=\"80\"/><Column size=\"153\"/><Column size=\"93\"/><Column size=\"136\"/><Column size=\"120\"/><Column size=\"100\"/><Column size=\"120\"/><Column size=\"100\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" text=\"NO\"/><Cell col=\"2\" text=\"APKEY\"/><Cell col=\"3\" text=\"USE_GB\"/><Cell col=\"4\" text=\"AGREE_GB\"/><Cell col=\"5\" text=\"START_DT\"/><Cell col=\"6\" text=\"AP_STATUS\"/><Cell col=\"7\" text=\"INSERTDATE\"/><Cell col=\"8\" text=\"INSERTURKEY\"/><Cell col=\"9\" text=\"UPDATEDATE\"/><Cell col=\"10\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" expr=\"currow+1\"/><Cell col=\"2\" text=\"bind:apkey\"/><Cell col=\"3\" text=\"bind:usegb_desc\"/><Cell col=\"4\" text=\"bind:agreegb_desc\"/><Cell col=\"5\" displaytype=\"date\" text=\"bind:startdt\" mask=\"yyyy-MM-dd\"/><Cell col=\"6\" text=\"bind:applygb_desc\"/><Cell col=\"7\" displaytype=\"date\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\"/><Cell col=\"8\" text=\"bind:inserturkey_desc\"/><Cell col=\"9\" displaytype=\"date\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\"/><Cell col=\"10\" text=\"bind:updateurkey_desc\"/></Band></Format></Formats>");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Static("NoticeCheck01", "absolute", "3", "1", "117", "20", null, null, this.div_splitTop01);
            obj.set_taborder("30");
            obj.set_text("◈가입 약관");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop01);
            obj.set_taborder("31");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_update", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop01);
            obj.set_taborder("32");
            obj.set_cssclass("btn_WF_modify");
            obj.set_tooltiptext("수정");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop01);
            obj.set_taborder("33");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop01.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop01);
            obj.set_taborder("34");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop01.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("2");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "194", "327", null, "230", "4", null, this);
            obj.set_taborder("3");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "194", "296", null, "31", "2", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("NoticeCheck02", "absolute", "0", "235", "194", "20", null, null, this);
            obj.set_taborder("5");
            obj.set_text("◈약관 내용");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "0", "264", "194", "31", null, null, this);
            obj.set_taborder("12");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            this.addChild(obj.name, obj);

            obj = new Static("static07", "absolute", "0", "328", "194", "229", null, null, this);
            obj.set_taborder("13");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new TextArea("edt_notice_content", "absolute", "200", "329", null, "224", "8", null, this);
            obj.set_taborder("17");
            obj.set_maxlength("999999");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static12", "absolute", "0", "296", "194", "31", null, null, this);
            obj.set_taborder("35");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "194", "264", null, "31", "2", null, this);
            obj.set_taborder("38");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_start_dt", "absolute", "815", "268", "130", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("39");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");
            obj.set_enable("false");
            obj.set_value("null");

            obj = new Static("static01", "absolute", "616", "262", "194", "31", null, null, this);
            obj.set_taborder("61");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            this.addChild(obj.name, obj);

            obj = new Static("static09", "absolute", "616", "296", "194", "31", null, null, this);
            obj.set_taborder("62");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("black");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_apkey", "absolute", "200", "268", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("63");
            obj.set_innerdataset("@ds_apkey");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");

            obj = new Combo("cbo_agreegb", "absolute", "815", "300", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("64");
            obj.set_innerdataset("@ds_agreegb");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");

            obj = new Combo("cbo_usegb", "absolute", "200", "300", "130", "24", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("65");
            obj.set_innerdataset("@ds_usegb");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_displayrowcount("15");
            obj.set_enable("false");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 29, this.div_splitTop01.div_Paging,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("24");
            		p.set_url("comm::MainPaging.xfdl");
            		p.set_scrollbars("none");
            		p.style.set_gradation("linear 0,0 #ffffffff 0,100 #fafafaff");

            	}
            );
            this.div_splitTop01.div_Paging.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 229, this.div_splitTop01,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop01.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 560, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("약관목록");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item10","div_splitTop.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","NoticeCheck02","text","gds_lang","ME00112681_08");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","ME00112681_02");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static07","text","gds_lang","ME00112681_06");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","div_splitTop.Static06","text","gds_lang","NOTICEBOARD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","static12","text","gds_lang","ME00112681_04");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cal_start_dt","value","ds_header","startdt");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_notice_content","value","ds_header","content");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","static01","text","gds_lang","ME00112681_03");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","static09","text","gds_lang","ME00112681_05");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","cbo_apkey","value","ds_header","apkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","cbo_agreegb","value","ds_header","agreegb");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","cbo_usegb","value","ds_header","usegb");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitTop01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_splitTop01.NoticeCheck01","text","gds_lang","ME00112681_07");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("agreementPhrase.xfdl", "lib::Comm.xjs");
        this.registerScript("agreementPhrase.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : notice.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 공지사항
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *  
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs"; 

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;
        this.gv_header = this.div_splitTop01.grd_header;
        this.gv_page = this.div_splitTop01.div_Paging;
        this.gv_addRow = false;
        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y"; 
        var dtlsearchcnt = 0;
         
        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {	

        	this.gfn_getCommCodeTran( ""
        					, "11"
        					, "ds_apkey"
        					, ""
        					, ""
        					, ""
        					);
        	this.gfn_getCode("PTL_AGREE_USE_GB", this.ds_usegb, "");
        	this.gfn_getCode("PTL_AGREE_SIGN_GB", this.ds_agreegb, "");
        	
        	this.gv_grdList = [this.gv_header]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);

        	
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {

        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "apController");
        	this.gfn_setCommon("METHODNM", "selectApInfoList");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
        	
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        								
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        				
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        		
        	}
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_constDsSet(this.gv_header);
        	
        		if(this.ds_header.rowcount > 0){
        			this.searchFalg = "save";
        			this.parent.fn_searchAfter(); // 검색조건 숨김

        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        				
        	}else if(sSvcId == "delete"){
        		this.parent.div_cond.btn_search.click();
        	}
        }

        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var nChkCnt = this.ds_header.getCaseCount("STATUS == 'D'");
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(nChkCnt == this.ds_header.rowcount){

        		}else{
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.fn_delete("");
        				}else {   // 취소시 변경내용 reset
        					if(obj == null){
        						this.ds_header.reset();
        					}
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.fn_delete = function(sAll)
        {
        	this.gfn_setCommon("BEANID", "apController");
        	this.gfn_setCommon("METHODNM", "save");
        	
            var sSvcId   = "delete";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DELETE_LIST=ds_header:U";
            var sOutData = "";
            var sParam   =  "workType=DELETE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.div_splitTop01_btn_add_onclick = function(obj,e)
        {
        	var oArg = { argFlag:"I"
        				,menuId:this.parent.gv_menuId
        			   };
        	this.gfn_popup("agreementPop", "user::agreementPhrasePop.xfdl", oArg, 935, 810, "");
        }

        this.div_splitTop01_btn_update_onclick = function(obj,e)
        {
        	if(this.ds_header.rowcount > 0){
        		var oArg = { argFlag:"U"
        					,menuId:		this.parent.gv_menuId
        					,apkey:			this.ds_header.getColumn(this.ds_header.rowposition, "apkey")
        					,usegb:	this.ds_header.getColumn(this.ds_header.rowposition, "usegb")
        					,agreegb:		this.ds_header.getColumn(this.ds_header.rowposition, "agreegb")
        					,startdt:		this.ds_header.getColumn(this.ds_header.rowposition, "startdt")
        					,content:		this.ds_header.getColumn(this.ds_header.rowposition, "content")
        				   };
        		this.gfn_popup("agreementPop", "user::agreementPhrasePop.xfdl", oArg, 935, 810, "");

        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}
        }

        this.div_splitTop01_btn_delete_onclick = function(obj,e)
        {
        // 	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 	
        // 	if(nRow == -1){
        // 		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        // 	}else{
        // 
        // 			var arrBtnText = new Array("CHECK_DATA", "TOTAL_DATA", "CANCEL");
        // 			var sMsgCode = "MSG_ALERT_ALLDATACHKRUN";
        // 			var sMsgText = new Array("TOTAL_COUNT," + this.ds_param.getColumn(0, "COUNT"));
        // 			
        // 			this.gfn_confirm3Btn (
        // 				arrBtnText,
        // 				sMsgCode,
        // 				sMsgText,
        // 				function(msg, flag) {
        // 					if (flag != "0") {
        // 						if (flag == "1") this.fn_delete("");
        // 						if (flag == "2") this.fn_delete("ALL");
        // 					}
        // 				}
        // 			);
        // 
        // 	}
        	var nRow = this.ds_header.findRow("CHK", "1");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_delete();
        			}
        		});
        	}
        }

        this.div_splitTop01_btn_excel_onclick = function(obj,e)
        {

        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("apController/excelDown.do", this.gv_header, this, oValue);

        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop01.btn_add.addEventHandler("onclick", this.div_splitTop01_btn_add_onclick, this);
            this.div_splitTop01.btn_update.addEventHandler("onclick", this.div_splitTop01_btn_update_onclick, this);
            this.div_splitTop01.btn_delete.addEventHandler("onclick", this.div_splitTop01_btn_delete_onclick, this);
            this.div_splitTop01.btn_excel.addEventHandler("onclick", this.div_splitTop01_btn_excel_onclick, this);
            this.Static08.addEventHandler("onclick", this.Static08_onclick, this);
            this.cal_start_dt.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_start_dt.addEventHandler("canchange", this.cal_to_canchange, this);
            this.cbo_apkey.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cbo_agreegb.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);
            this.cbo_usegb.addEventHandler("onitemchanged", this.cbo_icgrkey_onitemchanged, this);

        };

        this.loadIncludeScript("agreementPhrase.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
