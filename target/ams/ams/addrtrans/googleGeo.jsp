<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	String address = request.getParameter("address");
%>
<html>
    <head>
	<meta name="viewport" content="initial-scale=1.0">
	<meta charset="utf-8">
        <title>TmapRoute</title>	
		<script type="text/javascript">
			
			var address = "<%=address%>";
			var objMap = new ObjMap();
			
			//초기화.
			function initialize() {
				var latlng = new google.maps.LatLng(37.5240220, 126.9265940);
				var map = new google.maps.Map(document.getElementById('googlemap'), {
				  zoom: 8,
				  center: latlng
				});

				//DOM 리스너(넥사로 콜백하는 이벤트를 위해 필요.)
				google.maps.event.addDomListener(window, 'load', initialize);
				
				geocode(address);
			}
	
			//위도 경도 주소정보 가져오기.
			function geocode(address){

				var geocoder = new google.maps.Geocoder();
				
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == 'OK') {
				  
					objMap.put("lat",results[0].geometry.location.lat());
					objMap.put("lon",results[0].geometry.location.lng());
					objMap.put("newaddress",results[0].formatted_address);
					
					callNexacro(objMap);
						
				  } else {
					if(status == "ZERO_RESULTS"){
						alert('잘못된 주소정보 입니다. 주소를 확인해 주세요.');
					}else{
						alert('Geocode was not successful for the following reason: ' + status);
					}
					
				  }
				});
				
				
			}
			
			//넥사크로로 결과값 객체 리턴.
			function callNexacro(userdata)
			{
				var wb = window.NEXACROWEBBROWSER;

				if ( wb )
				{
					// HTML
					wb.on_fire_onusernotify(wb, userdata);
				}
				else
				{
					// Runtime
					window.document.title = userdata;
				}
			}
			
				
			//맵 객체 만들기.
			function ObjMap() {

				 this.elements = {};
				 this.length = 0;

			}
			
			ObjMap.prototype.put = function(key,value) {

			 this.length++;
			 this.elements[key] = value;

			}
			
			ObjMap.prototype.get = function(key) {

			 return this.elements[key];

			}
			
		</script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKhu4HLbVJua2Me-4AU7Eojh_lv3GA5eA&callback=initialize">
		</script>					
    </head>
	<body>
		<div id="googlemap"></div>
	</body>
</html>
