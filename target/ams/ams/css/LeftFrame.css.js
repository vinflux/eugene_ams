﻿//CSS=LeftFrame.css
    
(function() {
  return function() {
    var obj;   
    
    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_admin", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_wms", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_tms", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_vms", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_oms", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_kpi", "background", obj, ["normal"]);
    this._addCss("Button.btn_LF_ibs", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#54555cff","","0","none","","");
    this._addCss("Button.btn_LF_admin", "border", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "border", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Button.btn_LF_admin", "bordertype", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "bordertype", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_admin.png')");
    this._addCss("Button.btn_LF_admin", "image", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "image", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_align("center toptext");
    this._addCss("Button.btn_LF_admin", "imagealign", obj, ["normal", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "imagealign", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "imagealign", obj, ["normal", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "imagealign", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_font("8 dotum");
    this._addCss("Button.btn_LF_admin", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_wms", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_tms", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_vms", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_oms", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_kpi", "font", obj, ["normal", "mouseover"]);
    this._addCss("Button.btn_LF_ibs", "font", obj, ["normal", "mouseover"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Button.btn_LF_admin", "color", obj, ["normal", "mouseover", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "color", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("hand");
    this._addCss("Button.btn_LF_admin", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_wms", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_tms", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_vms", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_oms", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_kpi", "cursor", obj, ["normal"]);
    this._addCss("Button.btn_LF_ibs", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_value("25");
    this._addCss("Button.btn_LF_admin", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_wms", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_tms", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_vms", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_oms", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_kpi", "opacity", obj, ["normal"]);
    this._addCss("Button.btn_LF_ibs", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("#62636bff","","","0","0","0","0","true");
    this._addCss("Button.btn_LF_admin", "background", obj, ["mouseover", "pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "background", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("70");
    this._addCss("Button.btn_LF_admin", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_wms", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_tms", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_vms", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_oms", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_kpi", "opacity", obj, ["mouseover"]);
    this._addCss("Button.btn_LF_ibs", "opacity", obj, ["mouseover"]);

    obj = new nexacro.Style_font("bold 8 dotum");
    this._addCss("Button.btn_LF_admin", "font", obj, ["pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "font", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs", "font", obj, ["pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("-1");
    this._addCss("Button.btn_LF_admin", "letterspace", obj, ["pushed"]);
    this._addCss("Button.btn_bl_LF_admin", "letterspace", obj, ["focused", "selected"]);
    this._addCss("Button.btn_LF_admin_selected", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_wms.png')");
    this._addCss("Button.btn_LF_wms", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_wms_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_tms.png')");
    this._addCss("Button.btn_LF_tms", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_tms_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Button.btn_LF_tms", "color", obj, ["normal"]);
    this._addCss("Button.btn_LF_vms", "color", obj, ["normal"]);
    this._addCss("Button.btn_LF_oms", "color", obj, ["normal"]);
    this._addCss("Button.btn_LF_kpi", "color", obj, ["normal"]);
    this._addCss("Button.btn_LF_ibs", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::L_ico_vms.png')");
    this._addCss("Button.btn_LF_vms", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_vms_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_oms.png')");
    this._addCss("Button.btn_LF_oms", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_oms_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_bbs.png')");
    this._addCss("Button.btn_LF_kpi", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_kpi_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::L_ico_pms.png')");
    this._addCss("Button.btn_LF_ibs", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_LF_ibs_selected", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#76767eff","","","0","0","0","0","true");
    this._addCss("Static.sta_LF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Static.sta_LF_back", "border", obj, ["normal"]);

    obj = null;
    
//[add theme images]
  };
})();
