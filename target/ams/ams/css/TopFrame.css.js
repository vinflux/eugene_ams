﻿//CSS=TopFrame.css
    
(function() {
  return function() {
    var obj;   
    
    obj = new nexacro.Style_align("center middle");
    this._addCss("Button.btn_TF_Tmenu", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tmenu2", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite2", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "align", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Static.sta_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_module", "align", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_module", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spindownbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#incbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "align", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "align", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "align", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "align", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "align", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "align", obj, ["normal"]);

    obj = new nexacro.Style_background("#494f67ff","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_Tmenu", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Button.btn_TF_Tmenu", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tmenu2", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite2", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "border", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Static.sta_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_back", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_menu", "border", obj, ["normal"]);
    this._addCss("Static.sta_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_module", "border", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_module", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "border", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "border", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "border", obj, ["normal", "mouseover", "pushed", "disabled"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "border", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_bordertype("round","2","2","true","true","true","true");
    this._addCss("Button.btn_TF_Tmenu", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tmenu2", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite2", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "bordertype", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Static.sta_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_module", "bordertype", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_module", "bordertype", obj, ["normal"]);

    obj = new nexacro.Style_font("9 Gulim");
    this._addCss("Button.btn_TF_Tmenu", "font", obj, ["normal"]);
    this._addCss("Button.btn_TF_Tfavorite", "font", obj, ["normal"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "font", obj, ["normal"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "font", obj, ["normal"]);

    obj = new nexacro.Style_color("#ffffff");
    this._addCss("Button.btn_TF_Tmenu", "color", obj, ["normal"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "color", obj, ["normal"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::T_ico_folder.png')");
    this._addCss("Button.btn_TF_Tmenu", "image", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("hand");
    this._addCss("Button.btn_TF_Tmenu", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tmenu2", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite2", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "cursor", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Menu.meu_TF", "cursor", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spindownbutton", "cursor", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "cursor", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "cursor", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "cursor", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "cursor", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_value("-1");
    this._addCss("Button.btn_TF_Tmenu", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tmenu2", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite2", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "letterspace", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("35");
    this._addCss("Button.btn_TF_Tmenu", "opacity", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite", "opacity", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ffbb00ff","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_Tmenu", "background", obj, ["mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_font("bold 9 Gulim");
    this._addCss("Button.btn_TF_Tmenu", "font", obj, ["mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tmenu2", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "font", obj, ["mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_TF_Tfavorite2", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "font", obj, ["mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "font", obj, ["mouseover", "pushed", "focused", "selected", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "font", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_color("#ffffc8");
    this._addCss("Button.btn_TF_Tmenu", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tmenu2", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite2", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("URL('img::T_ico_folder_on.png')");
    this._addCss("Button.btn_TF_Tmenu", "image", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tmenu2", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "image", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_value("100");
    this._addCss("Button.btn_TF_Tmenu", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tmenu2", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite2", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "opacity", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "opacity", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#6c6c6cff","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_Tmenu", "background", obj, ["disabled"]);
    this._addCss("Button.btn_TF_Tfavorite", "background", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "background", obj, ["disabled"]);

    obj = new nexacro.Style_color("#838387");
    this._addCss("Button.btn_TF_Tmenu", "color", obj, ["disabled"]);
    this._addCss("Button.btn_TF_Tfavorite", "color", obj, ["normal", "disabled"]);
    this._addCss("Button.btn_bl_TF_Tmenu", "color", obj, ["disabled"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "color", obj, ["disabled"]);

    obj = new nexacro.Style_background("#ffbb00ff","","","0","0","0","0","true");
    this._addCss("Button.btn_TF_Tmenu2", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_TF_Tfavorite2", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#000000ff","","","0","0","0","0","true");
    this._addCss("Button.btn_bl_TF_Tmenu", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("20");
    this._addCss("Button.btn_bl_TF_Tmenu", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_background("#00000045","","","0","0","0","0","true");
    this._addCss("Button.btn_bl_TF_Tmenu", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "background", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "background", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_color("#ffffffff");
    this._addCss("Button.btn_bl_TF_Tmenu", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tmenu2", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite", "color", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "color", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);
    this._addCss("Static.sta_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_module", "color", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_module", "color", obj, ["normal"]);
    this._addCss("Menu.meu_TF", "color", obj, ["normal", "mouseover", "selected", "focused"]);
    this._addCss("Menu.meu_TF2", "color", obj, ["mouseover", "focused", "selected", "pushed"]);
    this._addCss("Menu.ptl_meu_TF", "color", obj, ["normal", "mouseover", "selected", "focused"]);

    obj = new nexacro.Style_background("#00000020","","","0","0","0","0","true");
    this._addCss("Button.btn_bl_TF_Tfavorite", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spindownbutton", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "background", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "background", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::T_ico_star.png')");
    this._addCss("Button.btn_bl_TF_Tfavorite", "image", obj, ["normal", "disabled"]);

    obj = new nexacro.Style_value("URL('img::T_ico_star_on.png')");
    this._addCss("Button.btn_bl_TF_Tfavorite", "image", obj, ["mouseover", "pushed", "focused", "selected"]);
    this._addCss("Button.btn_bl_TF_Tfavorite2", "image", obj, ["normal", "mouseover", "pushed", "focused", "selected"]);

    obj = new nexacro.Style_background("#6c6c6c30","","","0","0","0","0","true");
    this._addCss("Button.btn_bl_TF_Tfavorite", "background", obj, ["disabled"]);

    obj = new nexacro.Style_background("#3d3d46ff","","","0","0","0","0","true");
    this._addCss("Static.sta_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#3e77ceff","","","0","0","0","0","true");
    this._addCss("Static.sta_bl_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#00a86aff","","","0","0","0","0","true");
    this._addCss("Static.sta_gn_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#be2e45ff","","","0","0","0","0","true");
    this._addCss("Static.sta_re_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#293364ff","","","0","0","0","0","true");
    this._addCss("Static.sta_nv_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#6670adff","","","0","0","0","0","true");
    this._addCss("Static.sta_vi_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#009ca8ff","","","0","0","0","0","true");
    this._addCss("Static.sta_cy_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#6f737eff","","","0","0","0","0","true");
    this._addCss("Static.sta_gy_TF_back", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#4c4c56ff","","","0","0","0","0","true");
    this._addCss("Static.sta_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#5390e1ff","","","0","0","0","0","true");
    this._addCss("Static.sta_bl_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#03bc70ff","","","0","0","0","0","true");
    this._addCss("Static.sta_gn_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#d64353ff","","","0","0","0","0","true");
    this._addCss("Static.sta_re_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#3b467cff","","","0","0","0","0","true");
    this._addCss("Static.sta_nv_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#8696d1ff","","","0","0","0","0","true");
    this._addCss("Static.sta_vi_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#04b7baff","","","0","0","0","0","true");
    this._addCss("Static.sta_cy_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#838995ff","","","0","0","0","0","true");
    this._addCss("Static.sta_gy_TF_menu", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#5c17ffff","","","0","0","0","0","true");
    this._addCss("Static.sta_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 8 Dotum");
    this._addCss("Static.sta_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_bl_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_gn_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_re_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_nv_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_vi_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_cy_TF_module", "font", obj, ["normal"]);
    this._addCss("Static.sta_gy_TF_module", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#2d3391ff","","","0","0","0","0","true");
    this._addCss("Static.sta_bl_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#505050ff","","","0","0","0","0","true");
    this._addCss("Static.sta_gn_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#4f4f4fff","","","0","0","0","0","true");
    this._addCss("Static.sta_re_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#fb3f72ff","","","0","0","0","0","true");
    this._addCss("Static.sta_nv_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#f02380ff","","","0","0","0","0","true");
    this._addCss("Static.sta_vi_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#1229a3ff","","","0","0","0","0","true");
    this._addCss("Static.sta_cy_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("#6a28ecff","","","0","0","0","0","true");
    this._addCss("Static.sta_gy_TF_module", "background", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "background", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "background", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu.meu_TF", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "bordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "bordertype", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_value("URL('img::mnu_TF_expand.png')");
    this._addCss("Menu.meu_TF", "expandimage", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "expandimage", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "expandimage", obj, ["normal"]);

    obj = new nexacro.Style_font("bold 9 dotum");
    this._addCss("Menu.meu_TF", "font", obj, ["normal", "mouseover", "selected", "focused"]);
    this._addCss("Menu.meu_TF2", "font", obj, ["normal"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Menu.meu_TF", "itemalign", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "itemalign", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "itemalign", obj, ["normal"]);

    obj = new nexacro.Style_background("","img::T_menu_line.png","","0","0","100","50","true");
    this._addCss("Menu.meu_TF", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 35 0 35");
    this._addCss("Menu.meu_TF", "itempadding", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "itempadding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "itempadding", obj, ["normal"]);

    obj = new nexacro.Style_padding("");
    this._addCss("Menu.meu_TF", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "padding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "padding", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "padding", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF", "popupbackground", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "popupbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#818185ff","");
    this._addCss("Menu.meu_TF", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu.meu_TF", "popupbordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "popupbordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "popupbordertype", obj, ["normal"]);

    obj = new nexacro.Style_color("#6e6e6eff");
    this._addCss("Menu.meu_TF", "popupcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Menu.meu_TF", "popupfont", obj, ["normal"]);

    obj = new nexacro.Style_background("","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF", "popupitembackground", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "popupitembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","0","solid","#e6e6e6ff","","0","none","","");
    this._addCss("Menu.meu_TF", "popupitemborder", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu.meu_TF", "popupitembordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "popupitembordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "popupitembordertype", obj, ["normal"]);

    obj = new nexacro.Style_value("26");
    this._addCss("Menu.meu_TF", "popupitemheight", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "popupitemheight", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF", "popupitemheight", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 3 0 8");
    this._addCss("Menu.meu_TF", "popupitempadding", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "popupitempadding", obj, ["normal"]);

    obj = new nexacro.Style_shadow("outer 1,1 1 gray");
    this._addCss("Menu.meu_TF", "shadow", obj, ["normal"]);
    this._addCss("Menu.meu_TF2", "shadow", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffff20","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF", "itembackground", obj, ["mouseover", "selected", "focused"]);
    this._addCss("Menu.meu_TF2", "itembackground", obj, ["mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_color("#3d3d46ff");
    this._addCss("Menu.meu_TF", "popupcolor", obj, ["mouseover", "selected", "focused"]);
    this._addCss("Menu.meu_TF2", "popupcolor", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_background("#f1f1edff","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF", "popupitembackground", obj, ["mouseover", "selected", "focused"]);
    this._addCss("Menu.meu_TF2", "popupitembackground", obj, ["normal", "mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_color("#939393ff");
    this._addCss("Menu.meu_TF", "color", obj, ["disabled"]);
    this._addCss("Menu.meu_TF2", "color", obj, ["disabled"]);
    this._addCss("Menu.ptl_meu_TF", "color", obj, ["disabled"]);

    obj = new nexacro.Style_color("#747474ff");
    this._addCss("Menu.meu_TF", "popupcolor", obj, ["disabled"]);
    this._addCss("Menu.meu_TF2", "popupcolor", obj, ["disabled"]);
    this._addCss("Menu.ptl_meu_TF", "popupcolor", obj, ["disabled"]);

    obj = new nexacro.Style_value("50");
    this._addCss("Menu.meu_TF", "opacity", obj, ["disabled"]);
    this._addCss("Menu.meu_TF2", "opacity", obj, ["disabled"]);
    this._addCss("Menu.ptl_meu_TF", "opacity", obj, ["disabled"]);

    obj = new nexacro.Style_border("0","solid","","");
    this._addCss("Menu.meu_TF>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "border", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "border", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_bordertype("normal","0","0","true","true","true","true");
    this._addCss("Menu.meu_TF>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "bordertype", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "bordertype", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "bordertype", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("URL('img::btn_hinc_N.png')");
    this._addCss("Menu.meu_TF>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#incbutton", "image", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "image", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "image", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_align("cetner middle");
    this._addCss("Menu.meu_TF>#spindownbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "imagealign", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "imagealign", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 0 0 0");
    this._addCss("Menu.meu_TF>#spindownbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#spinupbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#incbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spindownbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "padding", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "padding", obj, ["normal"]);

    obj = new nexacro.Style_value("URL('img::btn_hdec_N.png')");
    this._addCss("Menu.meu_TF>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "image", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "image", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#spinupbutton", "image", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "image", obj, ["normal"]);

    obj = new nexacro.Style_background("#e7e5e5ff","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF>#incbutton", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "background", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "background", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "background", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "background", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#c0bbbbff","");
    this._addCss("Menu.meu_TF>#incbutton", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF>#decbutton", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#incbutton", "border", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "border", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#incbutton", "border", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "border", obj, ["normal"]);

    obj = new nexacro.Style_gradation("linear 0,0 #fbfbfa 100,0 #deded7");
    this._addCss("Menu.meu_TF>#decbutton", "gradation", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "gradation", obj, ["normal"]);
    this._addCss("Menu.ptl_meu_TF>#decbutton", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_color("#3d3d46ff");
    this._addCss("Menu.meu_TF2", "color", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Menu.meu_TF2", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#67676dff","");
    this._addCss("Menu.meu_TF2", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias 9 Helvetica dotum");
    this._addCss("Menu.meu_TF2", "popupfont", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#e6e6e6ff","","0","none","","");
    this._addCss("Menu.meu_TF2", "popupitemborder", obj, ["normal"]);

    obj = new nexacro.Style_font("antialias bold 9 dotum");
    this._addCss("Menu.meu_TF2", "font", obj, ["mouseover", "focused", "selected", "pushed"]);

    obj = new nexacro.Style_accessibility("","true","all","","","");
    this._addCss("Menu.meu_TF2>#incbutton", "accessibility", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "accessibility", obj, ["normal"]);

    obj = new nexacro.Style_color("");
    this._addCss("Menu.meu_TF2>#incbutton", "color", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "color", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Menu.meu_TF2>#incbutton", "cursor", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "cursor", obj, ["normal"]);

    obj = new nexacro.Style_font("");
    this._addCss("Menu.meu_TF2>#incbutton", "font", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "font", obj, ["normal"]);

    obj = new nexacro.Style_gradation("");
    this._addCss("Menu.meu_TF2>#incbutton", "gradation", obj, ["normal"]);

    obj = new nexacro.Style_value("");
    this._addCss("Menu.meu_TF2>#incbutton", "opacity", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "opacity", obj, ["normal"]);

    obj = new nexacro.Style_shadow("");
    this._addCss("Menu.meu_TF2>#incbutton", "shadow", obj, ["normal"]);
    this._addCss("Menu.meu_TF2>#decbutton", "shadow", obj, ["normal"]);

    obj = new nexacro.Style_font("10 NanumGothic");
    this._addCss("Menu.ptl_meu_TF", "font", obj, ["normal", "mouseover", "selected", "focused"]);

    obj = new nexacro.Style_border("0","none","","","1","solid","#3b65a5ff","","0","none","","","0","none","","");
    this._addCss("Menu.ptl_meu_TF", "itemborder", obj, ["normal"]);

    obj = new nexacro.Style_background("#0a3f8eff","","","0","0","0","0","true");
    this._addCss("Menu.ptl_meu_TF", "itembackground", obj, ["normal"]);

    obj = new nexacro.Style_background("#ffffffff","","","0","0","0","0","true");
    this._addCss("Menu.ptl_meu_TF", "popupbackground", obj, ["normal"]);

    obj = new nexacro.Style_border("1","solid","#2a579aff","");
    this._addCss("Menu.ptl_meu_TF", "popupborder", obj, ["normal"]);

    obj = new nexacro.Style_color("#666666");
    this._addCss("Menu.ptl_meu_TF", "popupcolor", obj, ["normal"]);

    obj = new nexacro.Style_font("10 NanumGothic");
    this._addCss("Menu.ptl_meu_TF", "popupfont", obj, ["normal"]);

    obj = new nexacro.Style_border("0","none","","","0","none","","","1","solid","#ecedf1ff","","0","none","","");
    this._addCss("Menu.ptl_meu_TF", "popupitemborder", obj, ["normal"]);

    obj = new nexacro.Style_padding("0 10 0 8");
    this._addCss("Menu.ptl_meu_TF", "popupitempadding", obj, ["normal"]);

    obj = new nexacro.Style_background("#2a579aff","","","0","0","0","0","true");
    this._addCss("Menu.ptl_meu_TF", "itembackground", obj, ["mouseover", "selected", "focused"]);

    obj = new nexacro.Style_color("#000000");
    this._addCss("Menu.ptl_meu_TF", "popupcolor", obj, ["mouseover", "selected", "focused"]);

    obj = new nexacro.Style_background("#f0f0f0ff","","","0","0","0","0","true");
    this._addCss("Menu.ptl_meu_TF", "popupitembackground", obj, ["mouseover", "selected", "focused"]);

    obj = new nexacro.Style_background("#f2f1faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh", "background", obj, ["normal"]);
    this._addCss("Grid.grd_TF_seh>#body", "background", obj, ["normal", "focused"]);

    obj = new nexacro.Style_border("1","solid","#cad0d9ff","");
    this._addCss("Grid.grd_TF_seh", "border", obj, ["normal"]);

    obj = new nexacro.Style_color("#5b6473ff");
    this._addCss("Grid.grd_TF_seh", "color", obj, ["normal"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Grid.grd_TF_seh", "font", obj, ["normal"]);

    obj = new nexacro.Style_background("#f2f1faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_background("#e4e9f3ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#body", "cellbackground2", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_color("#5b6473ff");
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_color("#5b6473ff");
    this._addCss("Grid.grd_TF_seh>#body", "cellcolor2", obj, ["normal", "focused"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Grid.grd_TF_seh>#body", "cellfont", obj, ["normal", "focused"]);

    obj = new nexacro.Style_border("0","none","","");
    this._addCss("Grid.grd_TF_seh>#body", "cellline", obj, ["normal", "focused"]);

    obj = new nexacro.Style_padding("0 0 0 3");
    this._addCss("Grid.grd_TF_seh>#body", "cellpadding", obj, ["normal", "focused"]);

    obj = new nexacro.Style_value("#cad0d9ff");
    this._addCss("Grid.grd_TF_seh>#body", "selectbackground", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_font("9 dotum");
    this._addCss("Grid.grd_TF_seh>#body", "selectfont", obj, ["normal", "focused"]);

    obj = new nexacro.Style_color("#000000ff");
    this._addCss("Grid.grd_TF_seh>#body", "selectcolor", obj, ["normal", "focused", "mouseover"]);

    obj = new nexacro.Style_background("#f0f0f0ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "background", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("11");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "scrollbarsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("0");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "incbtnsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("0");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "decbtnsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_value("60");
    this._addCss("Grid.grd_TF_seh>#vscrollbar", "trackbarsize", obj, ["normal", "mouseover", "pushed", "disabled"]);

    obj = new nexacro.Style_background("#888383ff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "background", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_align("center middle");
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "imagealign", obj, ["normal", "mouseover", "pushed"]);

    obj = new nexacro.Style_background("#f1f4faff","","","0","0","0","0","true");
    this._addCss("Grid.grd_TF_seh>#vscrollbar>#trackbar", "background", obj, ["disabled"]);

    obj = null;
    
//[add theme images]
  };
})();
