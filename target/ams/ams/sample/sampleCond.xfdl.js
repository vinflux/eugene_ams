﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("sampleCond");
                this.set_classname("sampleCond");
                this._setFormPosition(0,0,230,602);
            }
            this.style.set_background("#b3bbdaff");

            
            // Object(Dataset, ExcelExportObject) Initialize

            
            // UI Components Initialize
            obj = new Button("btn_search", "absolute", "43", "130", null, "28", "69", null, this);
            obj.set_taborder("0");
            obj.set_text("Search");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mae_userNm", "absolute", "31", "71", null, "33", "56", null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 230, 602, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("sampleCond");
            		p.style.set_background("#b3bbdaff");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","btn_search","text","gds_lang","SEARCH");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("sampleCond.xfdl", "lib::Comm.xjs");
        this.registerScript("sampleCond.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : sampleCond.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 샘플 조회조건 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER            DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_userNm;
        this.gv_menuId;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* 이벤트 명
        * @return
        * @param
        */

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* ShowHide Button 실행 */
        this.btn_showHide_onclick = function(obj,e)
        {
            if(obj.viewYn == "Y") {
                obj.viewYn = "N";
                application.gv_HFrameSet.set_separatesize("0,20,*,20");
            } else {
                obj.viewYn = "Y";
                application.gv_HFrameSet.set_separatesize("0,230,*,20");
            }
        }

        /* Search Button 실행 */
        this.btn_Search_onclick = function(obj,e)
        {
            this.gv_userNm = this.mae_userNm.value;
            var aCond = [this.gv_userNm];
            
            var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.parent.gv_menuId];
            oWorkFrame.form.setFocus();
            oWorkFrame.form.div_work.fn_Search(aCond);
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_search.addEventHandler("onclick", this.btn_Search_onclick, this);

        };

        this.loadIncludeScript("sampleCond.xfdl", true);

       
    };
}
)();
