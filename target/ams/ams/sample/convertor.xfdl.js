﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("convertor");
                this.set_classname("convertor");
                this.set_titletext("New Form");
                this._setFormPosition(0,0,1024,768);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_comps", this);
            obj._setContents("<ColumnInfo><Column id=\"row\" type=\"STRING\" size=\"256\"/><Column id=\"col\" type=\"STRING\" size=\"256\"/><Column id=\"pos\" type=\"STRING\" size=\"256\"/><Column id=\"compCol\" type=\"STRING\" size=\"256\"/><Column id=\"allowBlank\" type=\"STRING\" size=\"256\"/><Column id=\"allowOnlyWhitespace\" type=\"STRING\" size=\"256\"/><Column id=\"cls\" type=\"STRING\" size=\"256\"/><Column id=\"disabled\" type=\"STRING\" size=\"256\"/><Column id=\"displayField\" type=\"STRING\" size=\"256\"/><Column id=\"enforceMaxLength\" type=\"STRING\" size=\"256\"/><Column id=\"fieldLabel\" type=\"STRING\" size=\"256\"/><Column id=\"flex\" type=\"STRING\" size=\"256\"/><Column id=\"format\" type=\"STRING\" size=\"256\"/><Column id=\"height\" type=\"STRING\" size=\"256\"/><Column id=\"itemId\" type=\"STRING\" size=\"256\"/><Column id=\"labelStyle\" type=\"STRING\" size=\"256\"/><Column id=\"labelWidth\" type=\"STRING\" size=\"256\"/><Column id=\"margin\" type=\"STRING\" size=\"256\"/><Column id=\"maxLength\" type=\"STRING\" size=\"256\"/><Column id=\"maxWidth\" type=\"STRING\" size=\"256\"/><Column id=\"minWidth\" type=\"STRING\" size=\"256\"/><Column id=\"msgTarget\" type=\"STRING\" size=\"256\"/><Column id=\"readOnly\" type=\"STRING\" size=\"256\"/><Column id=\"style\" type=\"STRING\" size=\"256\"/><Column id=\"valueField\" type=\"STRING\" size=\"256\"/><Column id=\"vinMaxLength\" type=\"STRING\" size=\"256\"/><Column id=\"width\" type=\"STRING\" size=\"256\"/><Column id=\"xtype\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Tab("tb_convert", "absolute", "0", "22", null, null, "0", "0", this);
            obj.set_taborder("4");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tp_grid", this.tb_convert);
            obj.set_text("Convert Grid");
            this.tb_convert.addChild(obj.name, obj);
            obj = new CheckBox("chk_chkCol", "absolute", "18", "2", "221", "29", null, null, this.tb_convert.tp_grid);
            obj.set_taborder("0");
            obj.set_text("Check Column");
            obj.set_value("Y");
            obj.set_truevalue("Y");
            obj.set_falsevalue("N");
            this.tb_convert.tp_grid.addChild(obj.name, obj);
            obj = new TextArea("txa_src", "absolute", "2", "34", "350", null, null, "100", this.tb_convert.tp_grid);
            obj.set_taborder("1");
            obj.set_value("columns: [\r\n    {\r\n        xtype: 'rownumberer',\r\n        resizable: true,\r\n        width: 50,\r\n        dataIndex: 'No',\r\n        locked: true,\r\n        text: 'NO'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'ibexpectdate',\r\n        text: 'IBEXPECTDATE'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'ibconfirmdate',\r\n        text: 'IBCONFIRMDATE'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'orgchitymd_date',\r\n        text: 'ORGCHITYMD',\r\n        format: 'Y-m-d'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'icgrname',\r\n        text: 'ICGRNAME'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'ibhdkey',\r\n        text: 'IBHDKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'ibdtkey',\r\n        text: 'IBDTKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'owkey',\r\n        text: 'OWKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'ickey',\r\n        text: 'ICKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 200,\r\n        dataIndex: 'icname',\r\n        text: 'ICNAME'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'ackey',\r\n        text: 'ACKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 200,\r\n        dataIndex: 'acname',\r\n        text: 'ACNAME'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 150,\r\n        dataIndex: 'urkey_desc',\r\n        text: 'CONTACTXAC'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'store_key',\r\n        text: 'STORE_KEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 200,\r\n        dataIndex: 'store_name',\r\n        text: 'STORE_NAME'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 200,\r\n        dataIndex: 'fr_ctkey',\r\n        text: 'FR_CTKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'ibhdtype_desc',\r\n        text: 'IBHDTYPE'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'chitid_desc',\r\n        text: 'CHITID'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'temperature',\r\n        text: 'TEMPERATURE'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'icutkey',\r\n        text: 'ICUTKEY'\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'initqty',\r\n        text: 'INITQTY',\r\n        listeners: {\r\n            beforerender: 'onInitqtyBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        hidden: true,\r\n        dataIndex: 'initqty_ea',\r\n        text: 'INITQTY',\r\n        listeners: {\r\n            beforerender: 'onInitqtyBeforeRender1'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'exqty',\r\n        text: 'EXQTY',\r\n        listeners: {\r\n            beforerender: 'onExqtyBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        hidden: true,\r\n        dataIndex: 'exqty_ea',\r\n        text: 'EXQTY',\r\n        listeners: {\r\n            beforerender: 'onExqtyBeforeRender1'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'cfqty',\r\n        text: 'CFQTY',\r\n        listeners: {\r\n            beforerender: 'onCfqtyBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        hidden: true,\r\n        dataIndex: 'cfqty_ea',\r\n        text: 'CFQTY',\r\n        listeners: {\r\n            beforerender: 'onCfqtyBeforeRender1'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'rjqty',\r\n        text: 'RJQTY',\r\n        listeners: {\r\n            beforerender: 'onRjqtyBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        hidden: true,\r\n        dataIndex: 'rjqty_ea',\r\n        text: 'RJQTY',\r\n        listeners: {\r\n            beforerender: 'onRjqtyBeforeRender1'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        width: 150,\r\n        dataIndex: 'ibnrcd_desc',\r\n        text: 'IBNRCD'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'uom_desc',\r\n        text: 'UOM'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        hidden: true,\r\n        dataIndex: 'uom_ea_desc',\r\n        text: 'UOM'\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'costprice',\r\n        text: 'COSTPRICE'\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'exqtyprice',\r\n        text: 'EXQTYPRICE',\r\n        listeners: {\r\n            beforerender: 'onExqtypriceBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        text: 'CFQTYPRICE',\r\n        columns: [\r\n            {\r\n                xtype: 'numbercolumn',\r\n                dataIndex: 'cfqtyprice',\r\n                text: 'CFQTYPRICE',\r\n                listeners: {\r\n                    beforerender: 'onCfqtypriceBeforeRender'\r\n                }\r\n            },\r\n            {\r\n                xtype: 'numbercolumn',\r\n                dataIndex: 'cfqtypricevat',\r\n                text: 'VAT',\r\n                listeners: {\r\n                    beforerender: 'onCfqtypriceBeforeRender1'\r\n                }\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'rjqtyprice',\r\n        text: 'RJQTYPRICE',\r\n        listeners: {\r\n            beforerender: 'onRjqtypriceBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'to_lckey',\r\n        text: 'TO_LCKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'wkstatus_desc',\r\n        text: 'WKSTATUS'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'productdate',\r\n        text: 'PRODUCTDATE'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'expiredate',\r\n        text: 'EXPIREDATE'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'externorderkey',\r\n        text: 'EXTERNORDERKEY'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'externorderlinenum',\r\n        text: 'EXTERNORDERLINENUM'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'insertdate',\r\n        text: 'INSERTDATE'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'inserturkey',\r\n        text: 'INSERTURKEY'\r\n    },\r\n    {\r\n        xtype: 'datecolumn',\r\n        dataIndex: 'updatedate',\r\n        text: 'UPDATEDATE'\r\n    },\r\n    {\r\n        xtype: 'gridcolumn',\r\n        dataIndex: 'updateurkey',\r\n        text: 'UPDATEURKEY'\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'boxprice',\r\n        text: 'BOXPRICE_SUM',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'box_routfee',\r\n        text: 'BOX_ROUT_FEE',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender1'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'box_rin_fee',\r\n        text: 'BOX_RIN_FEE',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender2'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'bottleprice',\r\n        text: 'BOTTLEPRICE_SUM',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender3'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'bottle_routfee',\r\n        text: 'BOTTLE_ROUT_FEE',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender4'\r\n        }\r\n    },\r\n    {\r\n        xtype: 'numbercolumn',\r\n        dataIndex: 'bottle_rin_fee',\r\n        text: 'BOTTLE_RIN_FEE',\r\n        listeners: {\r\n            beforerender: 'onNumbercolumnBeforeRender5'\r\n        }\r\n    }\r\n]");
            this.tb_convert.tp_grid.addChild(obj.name, obj);
            obj = new TextArea("txa_tgt", "absolute", "358", "34", null, null, "4", "100", this.tb_convert.tp_grid);
            obj.set_taborder("2");
            this.tb_convert.tp_grid.addChild(obj.name, obj);
            obj = new Button("btn_convert", "absolute", null, "7", "95", "24", "4", null, this.tb_convert.tp_grid);
            obj.set_taborder("3");
            obj.set_text("Convert");
            this.tb_convert.tp_grid.addChild(obj.name, obj);
            obj = new Grid("grd_view", "absolute", "2", null, null, "92", "4", "2", this.tb_convert.tp_grid);
            obj.set_taborder("4");
            obj._setContents("<Formats></Formats>");
            this.tb_convert.tp_grid.addChild(obj.name, obj);
            obj = new Tabpage("tp_comp", this.tb_convert);
            obj.set_text("Convert Component");
            this.tb_convert.addChild(obj.name, obj);
            obj = new Button("btn_convert", "absolute", null, "7", "95", "24", "4", null, this.tb_convert.tp_comp);
            obj.set_taborder("0");
            obj.set_text("Convert");
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            obj = new TextArea("txa_tgt", "absolute", "358", "477", null, null, "4", "1", this.tb_convert.tp_comp);
            obj.set_taborder("1");
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            obj = new TextArea("txa_src", "absolute", "2", "34", "350", null, null, "1", this.tb_convert.tp_comp);
            obj.set_taborder("2");
            obj.set_value("items: [\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        margin: '5 0 5 0',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'textfield',\r\n                vinMaxLength: '50',\r\n                flex: 1,\r\n                disabled: true,\r\n                itemId: 'txt_reqhdkey',\r\n                margin: '0 5 0 5',\r\n                fieldLabel: 'REQHDKEY',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                msgTarget: 'side'\r\n            },\r\n            {\r\n                xtype: 'textfield',\r\n                itemId: 'txt_owkey',\r\n                margin: '0 5 0 5',\r\n                style: 'margin-right: 5px;',\r\n                width: 286,\r\n                fieldLabel: 'OWKEY',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                readOnly: true,\r\n                allowBlank: false,\r\n                allowOnlyWhitespace: false\r\n            },\r\n            {\r\n                xtype: 'button',\r\n                cls: 'x-btn-search',\r\n                height: 24,\r\n                itemId: 'btn_owkey',\r\n                margin: '0 5 0 5',\r\n                width: 24,\r\n                listeners: {\r\n                    click: 'onBtn_owkeyClick'\r\n                }\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        margin: '5 0 5 0',\r\n        style: 'margin-bottom: 5px;',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'textfield',\r\n                itemId: 'txt_loggrpcd',\r\n                margin: '0 5 0 5',\r\n                width: 286,\r\n                fieldLabel: 'LOGGRPCD',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                allowBlank: false,\r\n                allowOnlyWhitespace: false,\r\n                enforceMaxLength: true,\r\n                maxLength: 20\r\n            },\r\n            {\r\n                xtype: 'button',\r\n                cls: 'x-btn-search',\r\n                itemId: 'btn_loggrpcd',\r\n                margin: '0 5 0 5',\r\n                maxWidth: 24,\r\n                minWidth: 24,\r\n                width: 4,\r\n                listeners: {\r\n                    click: 'onBtn_loggrpcdClick'\r\n                }\r\n            },\r\n            {\r\n                xtype: 'combobox',\r\n                flex: 1,\r\n                itemId: 'cmb_reqhdtype',\r\n                margin: '0 5 0 5',\r\n                style: 'margin-right: 5px;',\r\n                fieldLabel: 'REQHDTYPE',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                allowBlank: false,\r\n                allowOnlyWhitespace: false,\r\n                displayField: 'field2',\r\n                valueField: 'field1',\r\n                listeners: {\r\n                    change: 'onCmb_reqhdtypeChange'\r\n                }\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        margin: '5 0 5 0',\r\n        style: 'margin-bottom: 5px;',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'datefield',\r\n                flex: 1,\r\n                itemId: 'cal_requestdate',\r\n                margin: '0 5 0 5',\r\n                fieldLabel: 'REQUESTDATE',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                msgTarget: 'side',\r\n                allowBlank: false,\r\n                allowOnlyWhitespace: false,\r\n                format: 'Y/m/d'\r\n            },\r\n            {\r\n                xtype: 'textfield',\r\n                vinMaxLength: '50',\r\n                itemId: 'txt_reqreason',\r\n                margin: '0 5 0 5',\r\n                width: 320,\r\n                fieldLabel: 'REQREASON',\r\n                labelWidth: 130,\r\n                msgTarget: 'side'\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        flex: 1,\r\n        margin: '5 0 5 0',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'combobox',\r\n                flex: 1,\r\n                itemId: 'cmb_dckey',\r\n                margin: '0 5 0 5',\r\n                style: 'margin-right: 5px;',\r\n                fieldLabel: 'DCKEY',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                allowBlank: false,\r\n                allowOnlyWhitespace: false,\r\n                displayField: 'field2',\r\n                valueField: 'field1'\r\n            },\r\n            {\r\n                xtype: 'textfield',\r\n                vinMaxLength: '50',\r\n                flex: 1,\r\n                itemId: 'txt_externorderkey',\r\n                margin: '0 5 0 5',\r\n                width: 295,\r\n                fieldLabel: 'EXTERNORDERKEY',\r\n                labelWidth: 130,\r\n                msgTarget: 'side'\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        flex: 1,\r\n        margin: '5 0 5 0',\r\n        style: 'margin-bottom: 5px;',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'textfield',\r\n                itemId: 'txt_ackey',\r\n                margin: '0 5 0 5',\r\n                width: 286,\r\n                fieldLabel: 'ACKEY',\r\n                labelWidth: 130,\r\n                enforceMaxLength: true,\r\n                maxLength: 20\r\n            },\r\n            {\r\n                xtype: 'button',\r\n                cls: 'x-btn-search',\r\n                itemId: 'btn_ackey',\r\n                margin: '0 5 0 5',\r\n                maxWidth: 24,\r\n                minWidth: 24,\r\n                width: 4,\r\n                listeners: {\r\n                    click: 'onBtn_ackeyClick'\r\n                }\r\n            },\r\n            {\r\n                xtype: 'textfield',\r\n                itemId: 'txt_store_key',\r\n                margin: '0 5 0 5',\r\n                width: 286,\r\n                fieldLabel: 'STORE_KEY',\r\n                labelWidth: 130,\r\n                enforceMaxLength: true,\r\n                maxLength: 20\r\n            },\r\n            {\r\n                xtype: 'button',\r\n                cls: 'x-btn-search',\r\n                itemId: 'btn_store_key',\r\n                margin: '0 5 0 5',\r\n                maxWidth: 24,\r\n                minWidth: 24,\r\n                width: 4,\r\n                listeners: {\r\n                    click: 'onBtn_store_keyClick'\r\n                }\r\n            }\r\n        ]\r\n    },\r\n    {\r\n        xtype: 'fieldcontainer',\r\n        flex: 1,\r\n        margin: '5 0 5 0',\r\n        style: 'margin-bottom: 5px;',\r\n        layout: {\r\n            type: 'hbox',\r\n            align: 'middle'\r\n        },\r\n        items: [\r\n            {\r\n                xtype: 'textfield',\r\n                disabled: true,\r\n                itemId: 'txt_fromtoctkey',\r\n                margin: '0 5 0 5',\r\n                width: 286,\r\n                fieldLabel: 'FROM_TO_CTKEY',\r\n                labelStyle: 'color:#ff2400; font-weight: bold;',\r\n                labelWidth: 130,\r\n                enforceMaxLength: true,\r\n                maxLength: 20\r\n            },\r\n            {\r\n                xtype: 'button',\r\n                cls: 'x-btn-search',\r\n                disabled: true,\r\n                itemId: 'btn_ctkey',\r\n                margin: '0 5 0 5',\r\n                maxWidth: 24,\r\n                minWidth: 24,\r\n                width: 4,\r\n                listeners: {\r\n                    click: 'onBtn_ackeyClick1'\r\n                }\r\n            }\r\n        ]\r\n    }\r\n]");
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            obj = new Radio("Radio00", "absolute", "44", "7", "144", "24", null, null, this.tb_convert.tp_comp);
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            var Radio00_innerdataset = new Dataset("Radio00_innerdataset", this.tb_convert.tp_comp.Radio00);
            Radio00_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">1</Col><Col id=\"datacolumn\">1</Col></Row><Row><Col id=\"codecolumn\">2</Col><Col id=\"datacolumn\">2</Col></Row><Row><Col id=\"codecolumn\">3</Col><Col id=\"datacolumn\">3</Col></Row></Rows>");
            obj.set_innerdataset(Radio00_innerdataset);
            obj.set_taborder("3");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_value("2");
            obj.set_direction("vertical");
            obj.set_index("1");
            obj = new Static("Static00", "absolute", "2", "7", "40", "24", null, null, this.tb_convert.tp_comp);
            obj.set_taborder("4");
            obj.set_text("Cols :");
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            obj = new Grid("Grid00", "absolute", "358", "34", null, "166", "4", null, this.tb_convert.tp_comp);
            obj.set_taborder("5");
            obj.set_binddataset("ds_comps");
            obj.set_cellsizingtype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"40\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"80\" band=\"left\"/><Column size=\"98\"/><Column size=\"180\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"117\"/><Column size=\"129\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"106\"/><Column size=\"80\"/><Column size=\"80\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"row\"/><Cell col=\"1\" text=\"col\"/><Cell col=\"2\" text=\"pos\"/><Cell col=\"3\" text=\"compCol\"/><Cell col=\"4\" text=\"allowBlank\"/><Cell col=\"5\" text=\"allowOnlyWhitespace\"/><Cell col=\"6\" text=\"cls\"/><Cell col=\"7\" text=\"disabled\"/><Cell col=\"8\" text=\"displayField\"/><Cell col=\"9\" text=\"enforceMaxLength\"/><Cell col=\"10\" text=\"fieldLabel\"/><Cell col=\"11\" text=\"flex\"/><Cell col=\"12\" text=\"format\"/><Cell col=\"13\" text=\"height\"/><Cell col=\"14\" text=\"itemId\"/><Cell col=\"15\" text=\"labelStyle\"/><Cell col=\"16\" text=\"labelWidth\"/><Cell col=\"17\" text=\"margin\"/><Cell col=\"18\" text=\"maxLength\"/><Cell col=\"19\" text=\"maxWidth\"/><Cell col=\"20\" text=\"minWidth\"/><Cell col=\"21\" text=\"msgTarget\"/><Cell col=\"22\" text=\"readOnly\"/><Cell col=\"23\" text=\"style\"/><Cell col=\"24\" text=\"valueField\"/><Cell col=\"25\" text=\"vinMaxLength\"/><Cell col=\"26\" text=\"width\"/><Cell col=\"27\" text=\"xtype\"/></Band><Band id=\"body\"><Cell style=\"align:right;\" text=\"bind:row\"/><Cell col=\"1\" style=\"align:right;\" text=\"bind:col\"/><Cell col=\"2\" style=\"align:right;\" text=\"bind:pos\"/><Cell col=\"3\" style=\"align:right;\" text=\"bind:compCol\"/><Cell col=\"4\" text=\"bind:allowBlank\"/><Cell col=\"5\" text=\"bind:allowOnlyWhitespace\"/><Cell col=\"6\" text=\"bind:cls\"/><Cell col=\"7\" text=\"bind:disabled\"/><Cell col=\"8\" text=\"bind:displayField\"/><Cell col=\"9\" text=\"bind:enforceMaxLength\"/><Cell col=\"10\" text=\"bind:fieldLabel\"/><Cell col=\"11\" text=\"bind:flex\"/><Cell col=\"12\" text=\"bind:format\"/><Cell col=\"13\" text=\"bind:height\"/><Cell col=\"14\" text=\"bind:itemId\"/><Cell col=\"15\" text=\"bind:labelStyle\"/><Cell col=\"16\" text=\"bind:labelWidth\"/><Cell col=\"17\" text=\"bind:margin\"/><Cell col=\"18\" text=\"bind:maxLength\"/><Cell col=\"19\" text=\"bind:maxWidth\"/><Cell col=\"20\" text=\"bind:minWidth\"/><Cell col=\"21\" text=\"bind:msgTarget\"/><Cell col=\"22\" text=\"bind:readOnly\"/><Cell col=\"23\" text=\"bind:style\"/><Cell col=\"24\" text=\"bind:valueField\"/><Cell col=\"25\" text=\"bind:vinMaxLength\"/><Cell col=\"26\" text=\"bind:width\"/><Cell col=\"27\" text=\"bind:xtype\"/></Band></Format></Formats>");
            this.tb_convert.tp_comp.addChild(obj.name, obj);
            obj = new Div("div_preview", "absolute", "358", "213", null, "260", "4", null, this.tb_convert.tp_comp);
            obj.set_taborder("6");
            obj.set_text("Div00");
            this.tb_convert.tp_comp.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tb_convert.tp_grid,
            	//-- Layout function
            	function(p) {
            		p.set_text("Convert Grid");

            	}
            );
            this.tb_convert.tp_grid.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.tb_convert.tp_comp,
            	//-- Layout function
            	function(p) {
            		p.set_text("Convert Component");

            	}
            );
            this.tb_convert.tp_comp.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1024, 768, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("convertor");
            		p.set_titletext("New Form");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("convertor.xfdl", "lib::Comm.xjs");
        this.registerScript("convertor.xfdl", function(exports) {
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        this.convertor_onload = function(obj,e)
        {
        	
        }

        this.tb_convert_tp_grid_btn_convert_onclick = function(obj,e)
        {
        	var objValue = eval(String(this.tb_convert.tp_grid.txa_src.value).replace("columns: ", ""));
        	var nColNo = 0;

        	var sFormatStr = '<Formats>\n';
        	sFormatStr     += '	<Format id="default">\n[FORMAT_CONTENTS]';
        	sFormatStr     += '	</Format>\n';
        	sFormatStr     += '</Formats>';

        	var sColumnsStr = '		<Columns>\n[COLUMNS]		</Columns>\n';
        	var sColumnStr  = '';

        	var sRowsStr = "";
        	sRowsStr += '		<Rows>\n';
        	sRowsStr += '			<Row size="26" band="head" />\n';
        	sRowsStr += '			<Row size="26" />\n';
        	sRowsStr += '		</Rows>\n';

        	var sHeadBandsStr = '';
        	sHeadBandsStr += '		<Band id="head">\n[HEADS]		</Band>\n';
        	sHeadBandStr = '';

        	var sBodyBandsStr = '';
        	sBodyBandsStr += '		<Band id="body">\n[BODYS]		</Band>\n';
        	sBodyBandStr = '';

        	if (this.tb_convert.tp_grid.chk_chkCol.value == "N") {
        		nColNo = -1;
        	} else {
        		sColumnStr  += '			<Column size="25" band="left" />\n';
        		sHeadBandStr += '			<Cell displaytype="checkbox"/>\n';
        		sBodyBandStr += '			<Cell displaytype="checkbox" edittype="checkbox" text="bind:CHK"/>\n';
        	}

        	for (keys in objValue) {
        		var objInner = objValue[keys];
        		if (objInner instanceof Object) {
        	
        			if (objInner.columns instanceof Array) {
        				var arrColumns = objInner.columns;
        				for (var i = 0; i < arrColumns.length; i++) {
        					nColNo++;

        					if (String(arrColumns[i].xtype) == "rownumberer") {
        						var sColSize = "40";
        						if (String(arrColumns[i].hidden).trim() == "true") sColSize = "0";
        						sColumnStr  += '			<Column size="' + sColSize + '" band="left" />\n';
        					} else if (String(arrColumns[i].locked) == "true") {
        						var sColSize = "1000";
        						if (String(arrColumns[i].hidden).trim() == "true") sColSize = "0";
        						sColumnStr  += '			<Column size="' + sColSize + '" band="left" />\n';
        					} else if (String(arrColumns[i].xtype) == "datecolumn") {
        						var sColSize = "140";
        						if (String(arrColumns[i].hidden).trim() == "true") sColSize = "0";
        						sColumnStr  += '			<Column size="' + sColSize + '" />\n';
        					} else {
        						var sColSize = "100";
        						if (this.gfn_isNotNull(arrColumns[i].width)) sColSize = String(arrColumns[i].width);
        						if (String(arrColumns[i].hidden).trim() == "true") sColSize = "0";
        						sColumnStr  += '			<Column size="' + sColSize + '" />\n';
        					}

        					sHeadBandStr += '			<Cell col="' + String(nColNo) + '" text="' + arrColumns[i].text + '" />\n';

        					if (String(arrColumns[i].xtype) == "rownumberer") {
        						sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="number" style="align:right;padding:0 2 0 2;" text="bind:' + arrColumns[i].dataIndex + '" expr="expr:currow&#32;+&#32;1"/>\n';
        					} else if (String(arrColumns[i].xtype) == "datecolumn") {
        						sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="date" style="align:center;padding:0 2 0 2;" text="bind:' + arrColumns[i].dataIndex + '" mask="yyyy-MM-dd HH:mm:ss" calendardisplaynulltype="none"/>\n';
        					} else if (String(arrColumns[i].xtype) == "numbercolumn") {
        						var sNumberColContents = "";

        						if (this.gfn_isNotNull(arrColumns[i].editor)) {
        							if (arrColumns[i].editor.xtype == "numberfield") {
        								sNumberColContents = 'edittype="number" ';
        							}
        						}

        						sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="number" ' + sNumberColContents + 'style="align:right;padding:0 2 0 2;" text="bind:' + arrColumns[i].dataIndex + '"/>\n';
        					} else {
        						if (this.gfn_isNotNull(arrColumns[i].editor)) {
        							if (arrColumns[i].editor.xtype == "combobox") {
        								var sEditType = (String(arrColumns[i].editor.editable) == "true")?'edittype="combo" ':"";
        								var sComboColContents = 'combodataset="ds_' + arrColumns[i].dataIndex + '" combocodecol="' + String(arrColumns[i].editor.valueField) + '" combodatacol="' + String(arrColumns[i].editor.displayField) + '"';
        								sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="combo" ' + sEditType + sComboColContents + ' style="align:left;padding:0 2 0 2;" text="bind:' + arrColumns[i].dataIndex + '"/>\n';
        							}
        						} else {
        							sBodyBandStr += '			<Cell col="' + String(nColNo) + '" style="align:left;padding:0 2 0 2;" text="bind:' + arrColumns[i].dataIndex + '"/>\n';
        						}
        					}
        				}

        			} else {
        				nColNo++;

        				if (String(objInner.xtype) == "rownumberer") {
        					var sColSize = "40";
        					if (String(objInner.hidden).trim() == "true") sColSize = "0";
        					sColumnStr  += '			<Column size="' + sColSize + '" band="left" />\n';
        				} else if (String(objInner.locked) == "true") {
        					var sColSize = "100";
        					if (String(objInner.hidden).trim() == "true") sColSize = "0";
        					sColumnStr  += '			<Column size="' + sColSize + '" band="left" />\n';
        				} else if (String(objInner.xtype) == "datecolumn") {
        					var sColSize = "140";
        					if (String(objInner.hidden).trim() == "true") sColSize = "0";
        					sColumnStr  += '			<Column size="' + sColSize + '" />\n';
        				} else {
        					var sColSize = "140";
        					if (this.gfn_isNotNull(objInner.width)) sColSize = String(objInner.width);
        					if (String(objInner.hidden).trim() == "true") sColSize = "0";
        					sColumnStr  += '			<Column size="' + sColSize + '" />\n';
        				}

        				sHeadBandStr += '			<Cell col="' + String(nColNo) + '" text="' + objInner.text + '" />\n';

        				if (String(objInner.xtype) == "rownumberer") {
        					sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="number" style="align:right;padding:0 2 0 2;" text="bind:NO" expr="expr:currow&#32;+&#32;1"/>\n';
        				} else if (String(objInner.xtype) == "datecolumn") {
        					sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="date" style="align:center;padding:0 2 0 2;" text="bind:' + objInner.dataIndex + '" mask="yyyy-MM-dd HH:mm:ss" calendardisplaynulltype="none"/>\n';
        				} else if (String(objInner.xtype) == "numbercolumn") {
        					var sNumberColContents = "";
        					if (this.gfn_isNotNull(objInner.editor)) {
        						if (objInner.editor.xtype == "numberfield") {
        							sNumberColContents = 'edittype="number" ';
        						}
        					}

        					sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="number" ' + sNumberColContents + 'style="align:right;padding:0 2 0 2;" text="bind:' + objInner.dataIndex + '"/>\n';
        				} else {

        					if (this.gfn_isNotNull(objInner.editor)) {
        						if (objInner.editor.xtype == "combobox") {
        							var sEditType = (String(objInner.editor.editable) == "true")?'edittype="combo" ':"";
        							var sComboColContents = 'combodataset="ds_' + objInner.dataIndex + '" combocodecol="' + String(objInner.editor.valueField) + '" combodatacol="' + String(objInner.editor.displayField) + '"';
        							sBodyBandStr += '			<Cell col="' + String(nColNo) + '" displaytype="combo" ' + sEditType + sComboColContents + ' style="align:left;padding:0 2 0 2;" text="bind:' + objInner.dataIndex + '"/>\n';
        						}
        					} else {
        						sBodyBandStr += '			<Cell col="' + String(nColNo) + '" style="align:left;padding:0 2 0 2;" text="bind:' + objInner.dataIndex + '"/>\n';
        					}
        				}
        			}

        // 			for (innrKeys in objInner) {
        // 				trace(nColNo + ". objInner[" + innrKeys + "] : " + objInner[innrKeys]);
        // 			}

        		} else {
        //			trace(nColNo + ". objValue[" + keys + "] : " + objValue[keys]);
        		}
        	}

        	sColumnsStr = String(sColumnsStr).replace("[COLUMNS]", sColumnStr);
        	sHeadBandsStr = String(sHeadBandsStr).replace("[HEADS]", sHeadBandStr);
        	sBodyBandsStr = String(sBodyBandsStr).replace("[BODYS]", sBodyBandStr);

        	sFormatStr = String(sFormatStr).replace("[FORMAT_CONTENTS]", (sColumnsStr + sRowsStr + sHeadBandsStr + sBodyBandsStr));

        	this.tb_convert.tp_grid.txa_tgt.set_value(sFormatStr);

        	this.tb_convert.tp_grid.grd_view.set_formats(sFormatStr);
        }

        this.tb_convert_tp_comp_btn_convert_onclick = function(obj,e)
        {
        	var arrSenchaComps = eval(this.tb_convert.tp_comp.txa_src.value);

        	this.ds_comps.clearData();

        	for (var i = 0; i < arrSenchaComps.length; i++) {
        		for (keys in arrSenchaComps[i]) {

        			if (keys == "items") {
        				var arrItems = arrSenchaComps[i][keys];
        				var nCol = -1;
        				var nPos = -1;

        				for (var j = 0; j < arrItems.length; j++) {
        					var nRow = this.ds_comps.addRow();
        					this.ds_comps.setColumn(nRow, "row", i);
        					nPos++;

        					var nCompCol = 0;

        					if (this.gfn_isNotNull(arrItems[j].fieldLabel)) {
        						nCol++;
        					} else {
        						nCompCol++;
        					}

        					this.ds_comps.setColumn(nRow, "col", nCol);
        					this.ds_comps.setColumn(nRow, "pos", nPos);
        					this.ds_comps.setColumn(nRow, "compCol", nCompCol);

        					for (itemKey in arrItems[j]) {
        						this.ds_comps.setColumn(nRow, itemKey, arrItems[j][itemKey]);
        					}
        				}
        			}
        		}
        	}

        	var sContents = "";
        /*
        	SEARCH_ID_APPLICATION : "10",
        	SEARCH_ID_CODE : "20",
        	SEARCH_ID_MESSAGE : "30",
        	SEARCH_ID_ACCOUNT : "40",
        	SEARCH_ID_OWNER : "50",
        	SEARCH_ID_CENTER : "60",
        	SEARCH_ID_USERGROUP : "70",
        	SEARCH_ID_USER : "80",
        	SEARCH_ID_ICGRKEY : "90",
        	SEARCH_ID_ICUTKEY : "100",
        	SEARCH_ID_ICKEY : "110",
        	SEARCH_ID_SUBSTITUTION : "120",
        	SEARCH_ID_ZCKEY : "230",
        	SEARCH_ID_WAHDKEY : "240",
        	SEARCH_ID_UOM : "270",
        	SEARCH_ID_STORE : "720",
        	SEARCH_ID_CODE+';LOGGRPCD'
        */
        	var nTabOrder = -1;
        	var nColWidth = 305;

        	var nBgTop = 58;
        	var nBgLeft = 142;
        	var nBgWidth = 490;
        	var nBgHeight = 31;

        	var nLabelTop = 58;
        	var nLabelLeft = 22;
        	var nLabelWidth = 120;
        	var nLabelHeight = 31;
        	var nLabelGap = 14;

        	var nCompTop = 62;
        	var nCompLeft = 148;
        	var nCompWidth = 165;
        	var nCompHeight = 22;
        	var nCompVGap = 9;
        	var nCompHGap = 6;

        	var nCompBtnWidth = 24;
        	var nCompBtnGap = 3;

        	for (var i = 0; i < this.ds_comps.getRowCount(); i++) {
        		var nRow = this.ds_comps.getColumn(i, "row");
        		var nCol = this.ds_comps.getColumn(i, "col");
        		var nPos = this.ds_comps.getColumn(i, "pos");
        		var sItemId = this.ds_comps.getColumn(i, "itemId");
        		var sFieldLabel = this.ds_comps.getColumn(i, "fieldLabel");
        		var nCompCol = this.ds_comps.getColumn(i, "compCol");
        		var sAllowBlank = this.ds_comps.getColumn(i, "allowBlank");
        		var sFormat = this.ds_comps.getColumn(i, "format");
        		var sDisabled = this.ds_comps.getColumn(i, "disabled");
        		var sEnforceMaxLength = this.ds_comps.getColumn(i, "enforceMaxLength");
        		var sMaxLength = this.ds_comps.getColumn(i, "maxLength");
        		var sReadOnly = this.ds_comps.getColumn(i, "readOnly");

        		if (this.gfn_isNull(sReadOnly)) sReadOnly = "false";

        		nTabOrder++;

        		if (nPos == "0") {
        			var nCurBgWidth = nBgWidth;
        			var nCurBgTop = nBgTop + (Number(nRow) * nBgHeight);

        			sContents += '		<Static id="st_bg' + String(nRow) + '" taborder="' + String(nTabOrder) + '" cssclass="sta_WF_bg" left="' + String(nBgLeft) + '" top="' + String(nCurBgTop) + '" width="' + String(nCurBgWidth) + '" height="' + String(nBgHeight) + '"/>\n';
        		}

         		if (nCompCol == "0") {
        			var nCurLabelTop = nLabelTop + (Number(nRow) * nLabelHeight);
        			var nCurLabelLeft = nLabelLeft + (Number(nCol) * nColWidth);

        			var sCss = "sta_WF_head";
        			if (sAllowBlank == "false") sCss = "sta_WF_head_duty";

        			sContents += '		<Static id="stc_' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" cssclass="' + sCss + '" left="' + String(nCurLabelLeft) + '" top="' + String(nCurLabelTop) + '" width="' + String(nLabelWidth) + '" height="' + String(nLabelHeight) +'" usedecorate="true" wordwrap="char"/>\n';
        			sContents += '		<BindItem id="item_' + String(sItemId).split("_")[1] + '" compid="stc_' + String(sItemId).split("_")[1] + '" propid="text" datasetid="gds_lang" columnid="' + sFieldLabel + '"/>\n';
         		}

        		var sXType = this.ds_comps.getColumn(i, "xtype");
        		var sEnable = (sDisabled == "true")?"false":"true";
        		var sMaxLengthStr = (sEnforceMaxLength == "true")?("maxlength='" + sMaxLength + "'"):"";

        		if (sXType == "textfield") {
        			var nCurCompTop = nCompTop + (Number(nRow) * nCompHeight) + (Number(nRow) * nCompVGap);
        			var nCurCompLeft = nCompLeft + (Number(nCol) * nColWidth);
        			var nCurrCompWidth = nCompWidth;

        			var nFindRow = this.ds_comps.findRowExpr("row == '" + nRow + "' && col == '" + nCol + "' && pos == '" + (Number(nPos) + 1) + "'");

        			if (nFindRow > 0) {
        				var sNextXType = this.ds_comps.getColumn(nFindRow, "xtype");
        				if (sNextXType == "button") {
        					nCurrCompWidth -= (nCompBtnWidth + nCompBtnGap)
        				}
        			}

        			sContents += '		<Edit id="edt_' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" left="' + String(nCurCompLeft) + '" top="' + String(nCurCompTop) + '" width="' + String(nCurrCompWidth) + '" height="' + String(nCompHeight) + '" enable="' + sEnable + '" readonly="' + sReadOnly + '" ' + sMaxLengthStr + '/>\n';
        		} else if (sXType == "combobox") {
        			var nCurCompTop = nCompTop + (Number(nRow) * nCompHeight) + (Number(nRow) * nCompVGap);
        			var nCurCompLeft = nCompLeft + (Number(nCol) * nColWidth);

        			sContents += '		<Combo id="cbo_' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" left="' + String(nCurCompLeft) + '" top="' + String(nCurCompTop) + '" width="' + String(nCompWidth) + '" height="' + String(nCompHeight) + '" enable="' + sEnable + '" readonly="' + sReadOnly + '" innerdataset="ds_' + String(sItemId).split("_")[1] + '" codecolumn="field1" datacolumn="field2"/>\n';

        		} else if (sXType == "datefield") {
        			var nCurCompTop = nCompTop + (Number(nRow) * nCompHeight) + (Number(nRow) * nCompVGap);
        			var nCurCompLeft = nCompLeft + (Number(nCol) * nColWidth);
        			var sDateFormat = "yyyy-MM-dd HH:mm";
        			if (sFormat == "Y/m/d") sDateFormat = "yyyy-MM-dd";

        			sContents += '		<Calendar id="cal' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" left="' + String(nCurCompLeft) + '" top="' + String(nCurCompTop) + '" width="' + String(nCompWidth) + '" height="' + String(nCompHeight) + '" enable="' + sEnable + '" readonly="' + sReadOnly + '" dateformat="' + sDateFormat + '" editformat="' + sDateFormat + '"/>\n';

        		} else if (sXType == "button") {
        			var nCurCompTop = nCompTop + (Number(nRow) * nCompHeight) + (Number(nRow) * nCompVGap);
        			var nCurCompLeft = nCompLeft + (Number(nCol)* nColWidth) + nCompWidth - nCompBtnWidth;
        			sContents += '		<Button id="btn_' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" left="' + String(nCurCompLeft) + '" top="' + String(nCurCompTop) + '" width="' + String(nCompBtnWidth) + '" height="' + String(nCompHeight) + '" enable="' + sEnable + '" readonly="' + sReadOnly + '" cssclass="btn_WF_srh_s"/>\n';
        		} else if (sXType == "numberfield") {
        			var nCurCompTop = nCompTop + (Number(nRow) * nCompHeight) + (Number(nRow) * nCompVGap);
        			var nCurCompLeft = nCompLeft + (Number(nCol) * nColWidth);

        			sContents += '		<Spin id="spn_' + String(sItemId).split("_")[1] + '" taborder="' + String(nTabOrder) + '" left="' + String(nCurCompLeft) + '" top="' + String(nCurCompTop) + '" width="' + String(nCompWidth) + '" height="' + String(nCompHeight) + '" enable="' + sEnable + '" readonly="' + sReadOnly + '" value="0"/>\n';
        		}
        	}

        	sContents = '<?xml version="1.0" encoding="utf-8"?>\n' +
        	'<UxFormClipBoard version="1.5">\n'+
        	'	<CopyComponent>\n' +
        	sContents +
        	'	</CopyComponent>\n'+
        	'</UxFormClipBoard>';

        	this.tb_convert.tp_comp.txa_tgt.set_value(sContents);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.convertor_onload, this);
            this.tb_convert.tp_grid.btn_convert.addEventHandler("onclick", this.tb_convert_tp_grid_btn_convert_onclick, this);
            this.tb_convert.tp_comp.btn_convert.addEventHandler("onclick", this.tb_convert_tp_comp_btn_convert_onclick, this);

        };

        this.loadIncludeScript("convertor.xfdl", true);

       
    };
}
)();
