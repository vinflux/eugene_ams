﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Scheduler");
                this.set_classname("Scheduler");
                this.set_titletext("Scheduler - Day, Week and Month Views");
                this._setFormPosition(0,0,1024,669);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("Dataset00", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"owner\" type=\"STRING\" size=\"256\"/><Column id=\"startTime\" type=\"DATETIME\" size=\"256\"/><Column id=\"endTime\" type=\"DATETIME\" size=\"256\"/><Column id=\"categories\" type=\"STRING\" size=\"256\"/><Column id=\"summary\" type=\"STRING\" size=\"256\"/><Column id=\"status\" type=\"STRING\" size=\"256\"/><Column id=\"description\" type=\"STRING\" size=\"256\"/><Column id=\"location\" type=\"STRING\" size=\"256\"/><Column id=\"cd\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("Div00", "absolute", "10", "65", "987", "527", null, null, this);
            obj.set_taborder("0");
            obj.set_applystyletype("cascade,keep");
            obj.set_text("Div00");
            obj.style.set_border("1 solid #808080ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_month", "absolute", "926", "19", "71", "32", null, null, this);
            obj.set_taborder("4");
            obj.set_text("MONTH");
            obj.style.set_font("antialias bold 10 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button06", "absolute", "11", "18", "141", "35", null, null, this);
            obj.set_taborder("19");
            obj.set_text("TODAY");
            obj.style.set_font("antialias 9 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Calendar("Calendar03", "absolute", "233", "19", "34", "34", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("20");
            obj.set_cssclass("schedulercss");

            obj = new Static("Static05", "absolute", "284", "19", "396", "34", null, null, this);
            obj.set_taborder("21");
            obj.set_text("Monday, April 13 - Friday, April 17");
            obj.style.set_font("antialias 12 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button02", "absolute", "193", "18", "36", "35", null, null, this);
            obj.set_taborder("22");
            obj.style.set_background("@gradation URL('Images::btn_CalNext_N.png')");
            obj.style.set_font("antialias 9 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button03", "absolute", "155", "18", "36", "35", null, null, this);
            obj.set_taborder("23");
            obj.style.set_background("@gradation URL('Images::btn_CalPrev_N.png')");
            obj.style.set_font("antialias 9 malgun gothic,Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button04", "absolute", "872", "598", "125", "38", null, null, this);
            obj.set_taborder("24");
            obj.set_text("delete Event");
            obj.style.set_font("antialias bold 11 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button05", "absolute", "10", "600", "120", "38", null, null, this);
            obj.set_taborder("25");
            obj.set_text("create Event");
            obj.style.set_font("antialias bold 11 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Button("Button08", "absolute", "736", "598", "117", "38", null, null, this);
            obj.set_taborder("26");
            obj.set_text("modify Event");
            obj.style.set_font("antialias bold 11 malgun gothic, Verdana");
            this.addChild(obj.name, obj);

            obj = new Static("st_selected", "absolute", "142", "601", "586", "34", null, null, this);
            obj.set_taborder("27");
            obj.set_text("not Selected!");
            obj.style.set_align("right middle");
            obj.style.set_font("antialias 12 malgun gothic,Verdana");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 987, 527, this.Div00,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");
            		p.set_applystyletype("cascade,keep");
            		p.set_text("Div00");
            		p.style.set_border("1 solid #808080ff");

            	}
            );
            this.Div00.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1024, 669, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("Scheduler");
            		p.set_titletext("Scheduler - Day, Week and Month Views");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("Scheduler.xfdl", "lib::Comm.xjs");
        this.registerScript("Scheduler.xfdl", function(exports) {
        /*
         * Scheduler Sample at nexacro® software.
         *
         * The Scheduler Sample demonstrates how you can manage calendar events.
         * 
        이 캘린더는 Google, Outlook과 유사한 스케줄러를 nexacro 앱에 추가 할 수있게 해주는 이벤트 캘린더입니다.
         * day, week and month view.
         * 이벤트 데이터를 dataset로 바인딩합니다
         * 작업 : 이벤트 표시 줄 크기 조정, 이벤트 표시 줄 이동, 모달 대화 상자를 사용하는 이벤트 편집.
         *
         * Copyright (c) 2015 EcoSystem of nexacro.
         * Licensed Free under nexacro® software.
        */

        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /**
         * 폼이로드되면 이벤트 핸들러를 시작합니다.
         * 배열을 참조하여 dataset에 새 레코드 추가(named defaultData).
         * 월별보기 및 현재 날짜를 표시합니다.
        **/
        this.Scheduler_onload = function(obj,e)
        {
        	createDefaultData(this.Dataset00);
        	var sche = this._getSchedulerComp();
        	var dt = this.setDefaultDate(new Date());
        	sche.setVisiblearea(dt, "month");
        	sche.draw();
        }

        /* 
         * 주어진 date 값을 scheduler_default_date 변수 및 this.Calendar03의 value 속성에 저장합니다. 
         * 스케쥴러에서 표시된 날짜 범위를 가져 오려면 scheduler_default_date가 필요합니다.
        */
        var scheduler_default_date;
        this.setDefaultDate = function(dt)
        {
        	scheduler_default_date = dt;
        	if ( this.Calendar03.value )
        	{
        		if ( this.Calendar03.value.date.getTime() != dt.getTime() )
        		{
        			this.Calendar03.set_value(Eco.date.getMaskFormatString(dt,"yyyyMMddhhmmss"));
        		}
        	}
        	else
        	{
        		this.Calendar03.set_value(Eco.date.getMaskFormatString(dt,"yyyyMMddhhmmss"));
        	}
        	return dt;
        }

        /* 
         *「defaultData」라고 이름이 붙여진 배열의 각 요소 인 객체를 기술합니다.
         * dataset으로 행을 만들 때 배열의 각 객체를 참조 할 수 있습니다.
         * weekDay : offset value of current week.
         * startHour : hour of Start Date 
         * durHour : duration hours
         * taskNm : task's name
        */
        var defaultData = [
        	{
        		weekDay: 1,
        		startHour: 8,
        		durHour: 1,
        		taskNm: "Dentist Appointment"
        	},
        	{
        		weekDay: 1,
        		startHour: 12,
        		durHour: 2,
        		taskNm: "Architecture Review"
        	},
        	{
        		weekDay: 1,
        		startHour: 14,
        		durHour: 2,
        		taskNm: "Documentation Review"
        	},
        	{
        		weekDay: 1,
        		startHour: 15,
        		durHour: 3,
        		taskNm: "Translation Review"
        	},
        	{
        		weekDay: 1,
        		startHour: 20,
        		durHour: 2,
        		taskNm: "Basketball Game"
        	},
        	{
        		weekDay: 2,
        		startHour: 8,
        		durHour: 1,
        		taskNm: "Morning Fitness"
        	},
        	{
        		weekDay: 2,
        		startHour: 9,
        		durHour: 1,
        		taskNm: "Product planning"
        	},
        	{
        		weekDay: 2,
        		startHour: 10,
        		durHour: 2,
        		taskNm: "Review Ad Campaign Results"
        	},
        	{
        		weekDay: 2,
        		startHour: 12,
        		durHour: 2,
        		taskNm: "Nexacro Presentation"
        	},
        	{
        		weekDay: 2,
        		startHour: 14,
        		durHour: 4,
        		taskNm: "Nexacro Online Traning"
        	},
        	{
        		weekDay: 3,
        		startHour: 0,
        		durHour: 24,
        		taskNm: "Nexacro UI&UX Conference"
        	},
        	{
        		weekDay: 3,
        		startHour: 0,
        		durHour: 48,
        		taskNm: "Planning For developping Diagram like Gliffy"
        	},
        	{
        		weekDay: 3,
        		startHour: 8,
        		durHour: 3,
        		taskNm: "playnexacro Integration Conference"
        	},
        	{
        		weekDay: 3,
        		startHour: 13,
        		durHour: 3,
        		taskNm: "Budget Committee Meeting"
        	},
        	{
        		weekDay: 4,
        		startHour: 9,
        		durHour: 3,
        		taskNm: "Evaluation Meeting"
        	},
        	{
        		weekDay: 4,
        		startHour: 12,
        		durHour: 1.5,
        		taskNm: "Interview with nexacro Developer"
        	},
        	{
        		weekDay: 4,
        		startHour: 13,
        		durHour: 3,
        		taskNm: "playnexacro User Group Meeting"
        	},
        	{
        		weekDay: 4,
        		startHour: 14,
        		durHour: 3,
        		taskNm: "Entity Framework Presentation"
        	},
        	{
        		weekDay: 4,
        		startHour: 19,
        		durHour: 2,
        		taskNm: "celebrate birthday of playnexacro"
        	},
        	{
        		weekDay: 5,
        		startHour: 9,
        		durHour: 3,
        		taskNm: "Employee Training"
        	},
        	{
        		weekDay: 5,
        		startHour: 12,
        		durHour: 2,
        		taskNm: "Code Change Impact Analysis"
        	},
        	{
        		weekDay: 5,
        		startHour: 15,
        		durHour: 3,
        		taskNm: "PM Weekly Meeting"
        	},
        	{
        		weekDay: 5,
        		startHour: 20,
        		durHour: 4,
        		taskNm: "drink beers with friends"
        	},
        	{
        		weekDay: -8,
        		startHour: 0,
        		durHour: 360,
        		taskNm: "Developing User Component(Chart)"
        	}
        ];

        /* 
         * add rows to ds. 
         * 추가 된 행의 열을 배열의 각 객체의 속성에서 참조하는 값으로 업데이트합니다. 
        */
        function createDefaultData(ds)
        {
        	var calendar = new Eco.GregorianCalendar();
        	var curDt = new Date();
        	curDt.setHours(0,0,0,0);
        	var startWeekDt = calendar.floor(curDt, Eco.TimeUnit.WEEK, 1),
        		startTime, endTime, data, nexacroDt;

        	ds.set_enableevent(false);
        	for ( var i = 0, len = defaultData.length ; i < len ; i++ )
        	{
        		data = defaultData[i];
        		startTime = calendar.addUnits(startWeekDt, Eco.TimeUnit.DAY, data.weekDay);
        		startTime = calendar.addUnits(startTime, Eco.TimeUnit.HOUR, data.startHour, true);
        		endTime = calendar.addUnits(startTime, Eco.TimeUnit.HOUR, data.durHour);
        		ds.addRow();
        		nexacroDt = new nexacro.Date();
        		nexacroDt.date = new Date(startTime.getTime());
        		ds.setColumn(i,"startTime", nexacroDt);
        		nexacroDt = new nexacro.Date();
        		nexacroDt.date = new Date(endTime.getTime());
        		ds.setColumn(i,"endTime", nexacroDt);
        		ds.setColumn(i,"summary",data.taskNm);
        		ds.setColumn(i,"cd", i);
        	}
        	ds.set_enableevent(true);
        }

        /* 
         * Eco.Scheduler 객체가 없으면 Eco.Scheduler 객체를 생성하고 Eco.Scheduler 객체를 반환합니다.
        */
        this._getSchedulerComp = function()
        {
        	var sche = this.schedulerCal;
        	if ( !sche )
        	{
        		sche = new Eco.Scheduler(this.Div00);
        		sche.setRowHeaderBackground("gradation", "linear 0,0 #fefeffff 100,0 #d9d9daff");
        		sche.setRowHeaderFont("Verdana,-10,bold");
        		sche.setRowHeaderColor("#46586eff");
        		sche.setColHeaderBackground("gradation", "linear 0,0 #fefeffff 0,100 #d9d9daff");
        		sche.setColHeaderFont("Verdana,-10,bold");
        		sche.setColHeaderColor("#46586eff");
        		sche.setColHeaderPadding("0 0 0 0");
        		sche.setAllDayBackground("#ffffffff");
        		sche.setTimeRangeBackground("#B2E1FFff");
        		sche.setCellBackground("#ffffffff");
        		sche.setCellHeadBackground("gradation", "linear 0,0 #fefeffff 0,100 #d9d9daff");
        		sche.setCellFont("Verdana,-10");
        		sche.setCellHeadPadding("2 3 2 0"); //top, right, bottom, left
        		sche.setCellColor("#46586eff");
        		sche.setTodayFontBold(true);
        		sche.setTodayFontColor("#000000ff");
        		sche.setTodayHeaderBackground("gradation", "linear 0,0 #ffffb2ff 0,100 #eeee5fff");
        		sche.setNonWorkingBackground("#f5f5f5ff");
        		sche.setLineColor("#b1b1b1ff");
        		sche.setEventItemBorderColor("#444444ff");
        		sche.setEventHorzBackground("gradation", "linear 50,0 #fcfdfeff 50,100 #c2d4ebff");
        		//sche.setEventVertBackground(["#FFCCCC","#FF9999","#FF6666","#CC3333"]);
        		sche.setEventVertBackground(["#d6e4bd","#ccc1da","#b7dee8","#fcd5b5","#b9cde5"]);
        		sche.setEventFont("Verdana,-10");
        		sche.setEventFontColor("#34495eff", "#FF0000");
        		sche.setEventPadding("2 2 2 2");
        		//sche.setEventItemBottomPadding(4);
        		sche.setBinddataset(this.Dataset00);
        		sche.addEventHandler("onviewrangechanged", this.onviewrangechanged_handler, this);
        		sche.addEventHandler("oneventitemselectchanged", this.oneventitemselectchanged_handler, this);
        		this.schedulerCal = sche;
        	}
        	return sche;
        }

        //Day Name Strings For English Locale Support(지원).
        var weekName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        var weekShortName = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];

        //Month Name Strings For English Locale Support.
        var monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var monthShortName = ["Jan ", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        // AM/PM Designators
        var ttName = ["AM", "PM"];

        //
        /* 
         * 이벤트 핸들러는 현재 뷰가 변경 될 때마다 호출됩니다. 
        */
        this.onviewrangechanged_handler = function(obj,e)
        {
        	var orgWeekName = Eco.date.weekName,
        		orgWeekShortName = weekShortName,
        		orgMonthName = Eco.date.monthName,
        		orgMonthShortName = Eco.date.monthShortName;

        	//Changing en-US locale of the Eco library. 
        	//Eco 라이브러리의 en-US 로케일 변경. 날짜의 서식 지정 함수가 호출 될 때

        	Eco.date.weekName = weekName;
        	Eco.date.weekShortName = weekShortName;
        	Eco.date.monthName = monthName;
        	Eco.date.monthShortName = monthShortName;

        	var str;
        	
        	if ( e.mode == "month" )
        	{
        		var sche = this._getSchedulerComp();
        		var data = sche._data;
        		var dt = data._calendar.addUnits(e.startdate, Eco.TimeUnit.DAY, 6);
        		str = Eco.date.getMaskFormatString(dt, "MMMM yyyy");
        	}
        	//else if ( e.mode == "day" )
        	//{
        	//	str = Eco.date.getMaskFormatString(e.startdate, "dddd, MMMM d");
        	//}
        	//else
        	//{
        	//	str = Eco.date.getMaskFormatString(e.startdate, "dddd, MMMM d") + " - " + 
        	//		Eco.date.getMaskFormatString(e.enddate, "dddd, MMMM d");
        	//}

        	this.Static05.set_text(str);

        	Eco.date.weekName = orgWeekName;
        	Eco.date.weekShortName = orgWeekShortName;
        	Eco.date.monthName = orgMonthName;
        	Eco.date.monthShortName = orgMonthShortName;
        }

        /* 
         * 선택한 이벤트가 변경된 후 이벤트 핸들러가 호출됩니다 (이벤트 바를 클릭 한 후).
        */
        this.oneventitemselectchanged_handler = function(obj,e)
        {
        	if ( e.newEventItem )
        	{
        		this.st_selected.set_text("\"" + e.newEventItem.key + "::" + e.newEventItem.summary + "\" be selected!");
        	}
        	else
        	{
        		this.st_selected.set_text("not Selected!");
        	}
        }

        /**
         * Moves the time axis backward in time in units specified by view mode(week, month, day).
          (week,month,day)보기 모드로 지정된 단위로 시간 축을 뒤로 이동합니다.
         
        **/
        this.Button03_onclick = function(obj,e)
        {
        	var sche = this._getSchedulerComp();
        	var data = sche._data;
        	if ( data )
        	{
        		var mode = data._mode,
        			calendar = data._calendar,
        			defaultdt = data._cloneDate(data._visibleStartdate);
        		if ( mode == "month" )
        		{
        			defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, 6);
        			defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.MONTH, -1);
        		}
        		//else if ( mode == "week" )
        		//{
        		//	defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.WEEK, -1);
        		//}
        		//else if ( mode == "workweek" )
        		//{
        		//	var workweekrange = data.workWeekRange;
        		//	defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.WEEK, -1);
        		//	defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, 0-(defaultdt.getDay() - workweekrange[0]));
        		//}
        		
        		
        		//else if ( mode == "day" )
        		//{
        		//	defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, -1);
        		//}

        		var dt = this.setDefaultDate(defaultdt);
        		sche.setVisiblearea(dt, mode);
        		sche.draw();
        	}
        }

        
        /**
         * Moves the time axis forward in time in units specified by view mode(week, month, day).
        **/
        this.Button02_onclick = function(obj,e)
        {
        	var sche = this._getSchedulerComp();
        	var data = sche._data;
        	if ( data )
        	{
        		var mode = data._mode,
        			calendar = data._calendar,
        			defaultdt = data._cloneDate(data._visibleStartdate);
        		if ( mode == "month" )
        		{
        			defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, 6);
        			defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.MONTH, 1);
        		}
        		//else if ( mode == "week" )
        	//	{
        	//		defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.WEEK, 1);
        	//	}
        	//	else if ( mode == "workweek" )
        	//	{
        	//		var workweekrange = data.workWeekRange;
        	//		defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.WEEK, 1);
        	//		defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, 0-(defaultdt.getDay() - workweekrange[0]));
        	//	}
        	//	else if ( mode == "day" )
        	// 	{
        	//		defaultdt = calendar.addUnits(defaultdt, Eco.TimeUnit.DAY, 1);
        	//	}
        		var dt = this.setDefaultDate(defaultdt);
        		sche.setVisiblearea(dt, mode);
        		sche.draw();
        	}
        }

        /**
         * Calendar (datepicker) 구성 요소의 onchanged 이벤트 핸들러입니다.
         * 구성 요소 값이 변경된 후에 발생합니다.
        **/
        this.Calendar03_onchanged = function(obj,e)
        {
        	var val = e.postvalue;
        	if ( val )
        	{
        		var sche = this._getSchedulerComp(),
        			data = sche._data;
        		var dt = data._cloneDate(val.date);
        		this.setDefaultDate(dt);
        		// 현재 활성보기에서 달력 구성 요소의 값 (입력 된 날짜)을 표시합니다.
        		sche.setVisiblearea(dt, sche._data._mode);
        		sche.draw();
        	}
        }

        /**
         * when "DAY" button click, fire event handler.
         * The view mode in a scheduler set as day.
        **/
        /**this.Button00_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        {
        	var sche = this._getSchedulerComp(),
        		data = sche._data;
        	if ( data._mode != "day" )
        	{
        		var dt = data._cloneDate(scheduler_default_date);
        		sche.setVisiblearea(dt, "day");
        		sche.draw();
        	}
        }**/

        /**
         * when "WORKWEEK" button click, fire event handler.
         * The view mode in a scheduler set as workweek.
        **/
        /**
        this.Button01_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        {
        	var sche = this._getSchedulerComp(),
        		data = sche._data;
        	if ( data._mode != "workweek" )
        	{
        		var dt = data._cloneDate(scheduler_default_date);
        		sche.setVisiblearea(dt, "workweek");
        		sche.draw();
        	}	
        }
        **/
        /**
         * when "WEEK" button click, fire event handler.
         * The view mode in a scheduler set as week.
           주 단위로 설정된 스케줄러의보기 모드.
        **/
        /**
        this.btn_week_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        {
        	var sche = this._getSchedulerComp(),
        		data = sche._data;
        	if ( data._mode != "week" )
        	{
        		var dt = data._cloneDate(scheduler_default_date);
        		sche.setVisiblearea(dt, "week");
        		sche.draw();
        	}
        }
        **/
        /**
         * when "MONTH" button click, fire event handler.
         * The view mode in a scheduler set as month.
            월로 설정된 스케줄러의보기 모드.
        **/
        this.btn_month_onclick = function(obj,e)
        {
        	var sche = this._getSchedulerComp(),
        		data = sche._data;
        	if ( data._mode != "month" )
        	{
        		var dt = data._cloneDate(scheduler_default_date);
        		sche.setVisiblearea(dt, "month");
        		sche.draw();
        	}

        }

        /**
         * when "TODAY" button click, fire event handler.
         * The default date set as current day in a scheduler.
            스케줄러에서 현재 날짜로 설정된 기본 날짜.
        **/
        this.Button06_onclick = function(obj,e)
        {
        	var dt = this.setDefaultDate(new Date());
        	var sche = this._getSchedulerComp();
        	sche.setVisiblearea(dt, sche._data._mode);
        	sche.draw();	
        }

        var new_seq = 0;
        /**
         * 선택한 날짜 / 시간 범위를 사용하여 새 이벤트를 만듭니다.
        **/
        this.Button05_onclick = function(obj,e)
        {
        	var ownerFrame = this.getOwnerFrame();

        	var sche = this._getSchedulerComp();
        	var timeRange = sche.getTimeRangeSelection();
        	//create modal dialog.
        	var popup = new ChildFrame();
        	popup.init("addEventRow", "absolute", 0, 0, 503, 439, null, null, "sample::SchedulerEvent.xfdl");	
        	//popup.set_autosize(true);
        	popup.set_openalign("center middle");		
        	popup.set_showtitlebar(true);
        	popup.set_resizable(false);
        	popup.showModal(ownerFrame, {"jobGbn": "add", "timeRange": timeRange }, this, this.popup_callBack); //arg
        }

        /**
           modify the selected event using a modal dialog
         * 모달 대화 상자를 사용하여 선택한 이벤트를 수정하십시오.
        **/
        this.Button08_onclick = function(obj,e)
        {
        	var ownerFrame = this.getOwnerFrame();

        	var sche = this._getSchedulerComp();
        	var evtItem = sche.getSelectedEvent();
        	if ( evtItem )
        	{
        		//create modal dialog.
        		var popup = new ChildFrame();
        		popup.init("modifyEventRow", "absolute", 0, 0, 503, 439, null, null, "sample::SchedulerEvent.xfdl");	
        		//popup.set_autosize(true);
        		popup.set_openalign("center middle");
        		popup.set_showtitlebar(true);
        		popup.set_resizable(false);
        		popup.showModal(ownerFrame, {"jobGbn": "modify", "selectEvent": evtItem}, this, this.popup_callBack); //arg
        	}
        }

        /**
         * delete the selected event using a modal dialog.
            모달 대화 상자를 사용하여 선택한 이벤트를 삭제하십시오.
        **/
        this.Button04_onclick = function(obj,e)
        {
        	var sche = this._getSchedulerComp();
        	var evtItem = sche.getSelectedEvent();
        	if ( evtItem )
        	{
        		if ( this.confirm("Do you delete selected event [ key: " + evtItem.key + " name : " + evtItem.summary + " ] ?" ) )
        		{
        			var data = sche._data;
        			var row = data.getRowToDs(evtItem.key);
        			this.Dataset00.deleteRow(row);
        			data.deleteEventItem(evtItem.key);
        			data._calcVisibleEventItems();
        			sche.draw();
        			sche.setSelectedEvent(null);
        		}
        	}
        	else
        	{
        		alert("There is nothing selected!");
        	}
        }

        var showmodalRetVal;
        /**
         * we save return's value in a modal dialog to variable showmodalRetVal. 
            우리는 반환 값을 모달 대화 상자의 변수 showmodalRetVal에 저장합니다.
         * I need this. because the argument of method(close) in a modal dialog can allows only string value. => close(val)
           나는 이것을 필요로한다. 왜냐하면 모달 대화 상자의 메소드 (닫기) 인수는 문자열 값만 허용 할 수 있기 때문입니다. => 닫기 (val)
         * @param {object} val return object. @param {object} val 반환 객체입니다.
        **/
        this.setRetVal = function(val)
        {
        	showmodalRetVal = val;
        }

        /*
         * Modal dialog is closed and called callback function below. 
            모달 대화 상자가 닫히고 아래의 콜백 함수가 호출됩니다.
         * @param {string} strId The first argument to callback function is The first argument to the showModal method.    
            @param {string} strId 콜백 함수의 첫 번째 인수는 showModal 메서드의 첫 번째 인수입니다.
         * @param {object} res close.
        */
        this.popup_callBack = function(strId,res)
        {
        	//trace(strId);
        	var res = showmodalRetVal;
        	if ( res && res.returnVal == "OK" )
        	{
        		var ds = this.Dataset00,
        			sche = this._getSchedulerComp(),
        			data = sche._data;
        		if ( strId == "addEventRow" )
        		{
        			var row = ds.addRow(),
        				key = "new_" + new_seq;
        			
        			ds.setColumn(row, "cd", key);
        			ds.setColumn(row, "startTime", data._convertNexacroDate(res.startdate));
        			ds.setColumn(row, "endTime", data._convertNexacroDate(res.enddate));
        			ds.setColumn(row, "summary", res.summary);
        			ds.setColumn(row, "description", res.description);
        			data.insertEventItem(key, row);
        			sche.setTimeRangeSelection(null);
        			new_seq++;
        			data._calcVisibleEventItems();
        			sche.draw();
        			sche.setSelectedEvent(data.getEventItem(key));
        		}
        		else if ( strId == "modifyEventRow" )
        		{
        			var row = data.getRowToDs(res.key);
        			ds.setColumn(row, "startTime", data._convertNexacroDate(res.startdate));
        			ds.setColumn(row, "endTime", data._convertNexacroDate(res.enddate));
        			ds.setColumn(row, "summary", res.summary);
        			ds.setColumn(row, "description", res.description);
        			data.updateEventItemFromDs(row);
        			data._calcVisibleEventItems();
        			sche.draw();
        		}
        	}
        	showmodalRetVal = null;
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.Scheduler_onload, this);
            this.btn_month.addEventHandler("onclick", this.btn_month_onclick, this);
            this.Button06.addEventHandler("onclick", this.Button06_onclick, this);
            this.Calendar03.addEventHandler("onchanged", this.Calendar03_onchanged, this);
            this.Calendar03.addEventHandler("oneditclick", this.Calendar03_oneditclick, this);
            this.Static05.addEventHandler("onclick", this.Static05_onclick, this);
            this.Button02.addEventHandler("onclick", this.Button02_onclick, this);
            this.Button03.addEventHandler("onclick", this.Button03_onclick, this);
            this.Button04.addEventHandler("onclick", this.Button04_onclick, this);
            this.Button05.addEventHandler("onclick", this.Button05_onclick, this);
            this.Button08.addEventHandler("onclick", this.Button08_onclick, this);
            this.st_selected.addEventHandler("onclick", this.st_selected_onclick, this);

        };

        this.loadIncludeScript("Scheduler.xfdl", true);

       
    };
}
)();
