﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("PushSendPop");
                this.set_classname("style01");
                this.set_scrollbars("none");
                this.set_titletext("푸시 발송 팝업");
                this._setFormPosition(0,0,700,750);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_code", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"ackey\" type=\"STRING\" size=\"256\"/><Column id=\"acname\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_usgr", this);
            obj._setContents("<ColumnInfo><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"pushid\" type=\"STRING\" size=\"256\"/><Column id=\"urname\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_eqtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"pushhdkey\" type=\"STRING\" size=\"256\"/><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"urname\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"pushid\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_push_temp", this);
            obj._setContents("<ColumnInfo><Column id=\"title\" type=\"STRING\" size=\"256\"/><Column id=\"pushmsg\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("label00", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("Title");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Tab("tab_data", "absolute", "22", "51", null, null, "22", "542", this);
            obj.set_taborder("131");
            obj.set_tabindex("0");
            obj.set_scrollbars("autoboth");
            this.addChild(obj.name, obj);
            obj = new Tabpage("tpg_data", this.tab_data);
            obj.set_text("USER");
            obj.style.set_background("#ffffffff");
            this.tab_data.addChild(obj.name, obj);
            obj = new Div("div_splitTop", "absolute", "0.91%", "10", null, "106", "0.91%", null, this.tab_data.tpg_data);
            obj.set_taborder("0");
            this.tab_data.tpg_data.addChild(obj.name, obj);
            obj = new Static("Static08", "absolute", "22", "18", "120", "31", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("0");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("URKEY");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_searchUrkey", "absolute", "604", "22", "24", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_srh_s");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static42", "absolute", "314", "49", "120", "31", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("5");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("PUSHID");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "23", "48", "120", "31", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("8");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("EQTYPE");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_urkey", "absolute", "148", "22", "157", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("9");
            obj.set_enable("false");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_urname", "absolute", "311", "22", "285", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("11");
            obj.set_enable("false");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_eqtype", "absolute", "145", "56", "75", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("12");
            obj.set_enable("false");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_eqtype_desc", "absolute", "226", "56", "79", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("13");
            obj.set_enable("false");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_pushid", "absolute", "447", "56", "181", "22", null, null, this.tab_data.tpg_data.div_splitTop);
            obj.set_taborder("14");
            obj.set_enable("false");
            this.tab_data.tpg_data.div_splitTop.addChild(obj.name, obj);
            obj = new Tabpage("tpg_usgr", this.tab_data);
            obj.set_text("USER GROUP");
            obj.style.set_background("#ffffffff");
            this.tab_data.addChild(obj.name, obj);
            obj = new Div("div_splitTop", "absolute", "0.91%", "10", null, "94", "1.37%", null, this.tab_data.tpg_usgr);
            obj.set_taborder("6");
            this.tab_data.tpg_usgr.addChild(obj.name, obj);
            obj = new Static("Static08", "absolute", "21", "18", "120", "31", null, null, this.tab_data.tpg_usgr.div_splitTop);
            obj.set_taborder("10");
            obj.set_text("URGRKEY");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_searchUrGroup", "absolute", "596", "22", "24", "22", null, null, this.tab_data.tpg_usgr.div_splitTop);
            obj.set_taborder("11");
            obj.set_cssclass("btn_WF_srh_s");
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "21", "48", "120", "31", null, null, this.tab_data.tpg_usgr.div_splitTop);
            obj.set_taborder("17");
            obj.set_text("EQTYPE");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);
            obj = new Edit("edt_urgrkey", "absolute", "148", "22", "155", "22", null, null, this.tab_data.tpg_usgr.div_splitTop);
            obj.set_taborder("18");
            obj.set_enable("false");
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);
            obj = new Combo("cmb_eqtype", "absolute", "22.93%", "54", null, "24", "52.73%", null, this.tab_data.tpg_usgr.div_splitTop);
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);
            obj.set_taborder("19");
            obj.set_value("null");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_innerdataset("@ds_eqtype");
            obj = new Edit("edt_urgrname", "absolute", "309", "22", "279", "22", null, null, this.tab_data.tpg_usgr.div_splitTop);
            obj.set_taborder("20");
            obj.set_enable("false");
            this.tab_data.tpg_usgr.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "3.14%", "504", null, "166", "3.14%", null, this);
            obj.set_taborder("132");
            this.addChild(obj.name, obj);
            obj = new Static("Static42", "absolute", "8", "5", "120", "31", null, null, this.div_splitBottom);
            obj.set_taborder("0");
            obj.set_text("TITLE");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_title", "absolute", "134", "9", "508", "22", null, null, this.div_splitBottom);
            obj.set_taborder("1");
            obj.set_maxlength("33");
            obj.set_enable("false");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "8", "43", "120", "101", null, null, this.div_splitBottom);
            obj.set_taborder("2");
            obj.set_text("NOTIMSG");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Edit("edt_pushmsg", "absolute", "134", "46", "508", "98", null, null, this.div_splitBottom);
            obj.set_taborder("3");
            obj.set_maxlength("33");
            obj.set_enable("false");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "356", "23", this);
            obj.set_taborder("133");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "299", "23", this);
            obj.set_taborder("134");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Button("btn_delete", "absolute", null, "216", "33", "24", "23", null, this);
            obj.set_taborder("135");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.addChild(obj.name, obj);

            obj = new Button("btn_add", "absolute", null, "216", "33", "24", "60", null, this);
            obj.set_taborder("136");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.addChild(obj.name, obj);

            obj = new Grid("grd_detail", "absolute", "16", "245", null, null, "22", "262", this);
            obj.set_taborder("137");
            obj.set_binddataset("ds_detail");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"0\"/><Column size=\"0\"/><Column size=\"210\"/><Column size=\"180\"/><Column size=\"180\"/><Column size=\"0\"/><Column size=\"0\"/><Column size=\"0\"/><Column size=\"0\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"NO\"/><Cell col=\"2\" text=\"URKEY\"/><Cell col=\"3\" text=\"PUSHHDKEY\"/><Cell col=\"4\" text=\"URNAME\"/><Cell col=\"5\" text=\"EQTYPE\"/><Cell col=\"6\" text=\"PUSHID\"/><Cell col=\"7\" text=\"PUSHHDKEY\"/><Cell col=\"8\" text=\"USSC_ACTIVE\"/><Cell col=\"9\" text=\"FR_DATE_VALUE\"/><Cell col=\"10\" text=\"TO_DATE_VALUE\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"2\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:urkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"3\" text=\"bind:pushhdkey\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:urname\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:right;padding: ;\" text=\"bind:eqtype_desc\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" style=\"align:right;padding: ;\" text=\"bind:pushid\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"7\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:pushhdkey\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"8\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ussc_active_desc\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"9\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:fr_date_value\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:to_date_value\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 644, 106, this.tab_data.tpg_data.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("0");

            	}
            );
            this.tab_data.tpg_data.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 680, 208, this.tab_data.tpg_data,
            	//-- Layout function
            	function(p) {
            		p.set_text("USER");
            		p.style.set_background("#ffffffff");

            	}
            );
            this.tab_data.tpg_data.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 641, 94, this.tab_data.tpg_usgr.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("6");

            	}
            );
            this.tab_data.tpg_usgr.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 680, 125, this.tab_data.tpg_usgr,
            	//-- Layout function
            	function(p) {
            		p.set_text("USER GROUP");
            		p.style.set_background("#ffffffff");

            	}
            );
            this.tab_data.tpg_usgr.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 166, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("132");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 700, 750, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_scrollbars("none");
            		p.set_titletext("푸시 발송 팝업");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","label00","text","gds_lang","ADDACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","tab_data.tpg_data.div_splitTop.Static08","text","gds_lang","URKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","tab_data.tpg_data.div_splitTop.Static42","text","gds_lang","PUSHID");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","tab_data.tpg_data.div_splitTop.Static00","text","gds_lang","EQTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_splitBottom.Static42","text","gds_lang","TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitBottom.Static00","text","gds_lang","NOTIMSG");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","tab_data.tpg_xml.div_splitTop.Static08","text","gds_lang","URGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","tab_data.tpg_xml.div_splitTop.Static00","text","gds_lang","EQTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","tab_data.tpg_usgr","text","gds_lang","USER GROUP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","tab_data.tpg_data","text","gds_lang","USER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","tab_data.tpg_usgr.div_splitTop.Static08","text","gds_lang","URGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","tab_data.tpg_usgr.div_splitTop.Static00","text","gds_lang","EQTYPE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("PushSendPop.xfdl", "lib::Comm.xjs");
        this.registerScript("PushSendPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : PushSendPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : push 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main";
        this.gv_middle = "Middle";
        this.gv_detral = "Detail";
        this.gv_obj;
        this.gv_pushhdkey = "";
        this.gv_title = "";
        this.gv_pushmsg = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	var sSize = "";
        	var sIndex = "";
        	
        	this.gfn_getCode("EQTYPE", this.ds_eqtype, "");	
        	
        	this.ds_eqtype.getColumn(this.tab_data.tpg_usgr.div_splitTop.cmb_eqtype.index,"field1");
        	this.tab_data.tpg_usgr.div_splitTop.cmb_eqtype.set_index(3);
        	
        	this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_pushhdkey = this.gfn_isNullEmpty(this.parent.pushhdkey);
            this.gv_title  = this.gfn_isNullEmpty(this.parent.title);
            this.gv_pushmsg = this.gfn_isNullEmpty(this.parent.pushmsg);
            
            this.div_splitBottom.edt_pushmsg.set_value(this.parent.pushmsg);
            this.div_splitBottom.edt_title.set_value(this.parent.title);    
            
            
            this.gv_grdList = [this.grd_detail]; // 그리드 오브젝트
        	var divPaging = []; // 페이지 오브젝트
        	var searchFunc = []; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);
        //	this.ds_header.applyChange();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.grd_code.set_nodatatext("");
        	this.ds_code.set_keystring("");
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "accountController");
        	this.gfn_setCommon("METHODNM", "selectRepAccountInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            if(this.gfn_isNotNull(this.edt_search.value)) sParam = "searchValue="+this.edt_search.value;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /* USER GROUP TAB에서 ADD 할 때 */
        this.fn_addUserGroup = function(){

        	this.ds_usgr.clearData();

        	var divTop = this.tab_data.tpg_usgr.div_splitTop;
        	var urgrkey = divTop.edt_urgrkey;
            var cmb_eqtype = divTop.cmb_eqtype;
            
            var eqtype_val = this.ds_eqtype.getColumn(cmb_eqtype.index,"field1");
            if(this.gfn_isNull(urgrkey.value)){
        		this.gfn_alert("MSG_CHKDATA_NOWRITE", "", "", this.gfn_getTxtLang("URGRKEY"));
        		return;
        	} else {	
        		if(this.gfn_isNull(eqtype_val)){
        			this.gfn_alert("MSG_CHKDATA_NOWRITE", "", "", this.gfn_getTxtLang("EQTYPE"));
        			return;
        		} else {
        			this.ds_usgr.clearData();
        			this.gfn_setCommon("BEANID"  , "pushController");
        			this.gfn_setCommon("METHODNM", "selectUserListByUserGroup");
        			
        			var sSvcId   = "select";
        			var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        			var sInData  = "";
        			var sOutData = "ds_usgr=OUT_DATA_LIST ds_param=OUT_PARAM";
        			var sParam   = "urgrkey="+urgrkey.value
        						 + " eqtype="+eqtype_val;
        				
        			this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");			
        		}
        	}
        }

        /* PUSH 전송 */
        this.fn_sendDtl = function(){

        	this.ds_push_temp.addRow();
        	this.ds_push_temp.setColumn(0,"title", this.parent.title);
        	this.ds_push_temp.setColumn(0,"pushmsg", this.parent.pushmsg);

        	var sInData  = "IN_DATA_LIST=ds_detail IN_PUSH_CON=ds_push_temp";
            var sSvcId   = "sendPush";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sOutData = "";
            var sParam   = "pushhdkey="+this.parent.pushhdkey
        				 + " title="+this.parent.title
        				 + " pushmsg="+this.parent.pushmsg
        				 + " workType=INSERT";

        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "sendPush");	
        	
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");	
        }
        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}	
        	if(sSvcId == "select") {
        		if(this.ds_usgr.rowcount > 0){
        			for(var i = 0 ; i < this.ds_usgr.rowcount; i++){
        				var urkey = this.ds_usgr.getColumn(i,"urkey");
        				var eqtype = this.ds_usgr.getColumn(i,"eqtype");

        				for(var j = 0; j < this.ds_detail.rowcount; j++){
        					if (this.ds_detail.getColumn(j,"urkey") == urkey && this.ds_detail.getColumn(j,"eqtype") == eqtype) {						
        						this.ds_detail.deleteRow(j);
        						break;
        					}// if
        				}//for var j
        			}// for var i
        			this.ds_detail.mergeData(this.ds_usgr);
        			this.gfn_constDsSet(this.grd_detail);
        		}//if rowcount
        	} else if (sSvcId == "sendPush"){
        		var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			this.close();
        		});
        	}
        }//fn_callback

        
        /***********************************************************************************
        * user function
        ***********************************************************************************/
        /* btn_searchUrkey callback */
        this.urkeyPopCallback = function(obj,valueArr)
        {	
        	var divTop = this.tab_data.tpg_data.div_splitTop;
        	var urname = divTop.edt_urname;
        	var eqtype = divTop.edt_eqtype;
        	var eqtype_desc = divTop.edt_eqtype_desc;
        	var pushid = divTop.edt_pushid;
        	
        	urname.set_value(valueArr[1]);
        	eqtype.set_value(valueArr[2]);
        	eqtype_desc.set_value(valueArr[2]);
        	pushid.set_value(valueArr[3]);
        }

        /* btn_searchUrkey callback */
        this.urgrkeyPopCallback = function(obj,valueArr)
        {	
        	var divTop = this.tab_data.tpg_usgr.div_splitTop;
        	var urgrkey = divTop.edt_urgrkey;
        	var urgrname = divTop.edt_urgrname;
        		
        	urgrname.set_value(valueArr[1]);
        }

        /* 추가 */
        this.div_splitBottom_btn_add_onclick = function(obj,e)
        {
        	var tabindex = this.tab_data.tabindex;
        	
        	//USER TAB
        	if(0 == tabindex){
        		this.fn_addUser();
        	} else {//USERGROUP TAB
        		this.fn_addUserGroup();
        	}
        }

        /* USER TAB에서 ADD 할 때 */
        this.fn_addUser = function(){
        	var divTop = this.tab_data.tpg_data.div_splitTop;
        	var urkey = divTop.edt_urkey;
        	var urname = divTop.edt_urname;
        	var eqtype = divTop.edt_eqtype;
        	var eqtype_desc = divTop.edt_eqtype_desc;
        	var pushid = divTop.edt_pushid;
        	
        	if(this.gfn_isNull(urkey.value)){
        		this.gfn_alert("MSG_CHKDATA_NOWRITE", "", "", this.gfn_getTxtLang("URKEY"));
        		return;
        	} else {
        		for(var i = 0 ; i < this.ds_detail.rowcount; i++ ){
        			if(urkey.value == this.ds_detail.getColumn(i,"urkey") && eqtype.value == this.ds_detail.getColumn(i,"eqtype")){
        				this.gfn_alert("MSG_CHK_ALREADY_ADDED");
        				return;
        			}
        		}
        	}
        	var nRow = this.ds_detail.addRow();
        	this.ds_detail.set_rowposition(nRow);
        	this.grd_detail.setCellPos(this.grd_detail.getBindCellIndex("body", "pushhdkey"));
        	this.ds_detail.setColumn(nRow, "urkey", urkey.value);
        	this.ds_detail.setColumn(nRow, "urname", urname.value);
        	this.ds_detail.setColumn(nRow, "eqtype", eqtype.value);
        	this.ds_detail.setColumn(nRow, "eqtype_desc", eqtype_desc.value);
        	this.ds_detail.setColumn(nRow, "pushid",pushid.value);
        }

        /* 체크된 데이터 삭제 */
        this.fn_delete = function()
        {	
        	var checkList = this.gfn_getCheckedIndex(this.grd_detail);	
        	this.ds_detail.deleteMultiRows(checkList);
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        /* btn_closeAll_onclick 실행 */
        this.btn_closeAll_onclick = function(obj,e)
        {
        	this.close();
        }

        /* div_splitBottom_grd_detail_onheadclick 실행 */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var sType = obj.getCellProperty("head", e.cell, "displaytype");
        	
        	if(sType == "checkbox"){
        		this.ds_detail.set_updatecontrol(false);
        		this.gfn_gridProc(obj, e);
        		this.ds_detail.set_updatecontrol(true);
        	}
        }

        /* btn_searchUrkey click */
        this.tab_data_tpg_data_div_splitTop_btn_searchUrkey_onclick = function(obj,e)
        {	
        	this.gv_Pvalue = "";	
        	var oArg = { divId:"Single"
        				,searchId:790
        				,putObj:this.tab_data.tpg_data.div_splitTop.edt_urkey
        				,putKey:"urkey"
        				,putCallback:"urkeyPopCallback"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");	
        }

        

        /* btn_searchUrGroup 클릭 */
        this.tab_data_tpg_usgr_div_splitTop_btn_searchUrGroup_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";	
        	var oArg = { divId:"Single"
        				,searchId:70
        				,putObj:this.tab_data.tpg_usgr.div_splitTop.edt_urgrkey
        				,putKey:"urgrkey"
        				,putCallback:"urgrkeyPopCallback"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");		
        }

        

        

        
        /* 삭제 버튼 클릭 */
        this.btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.findRow("CHK", "1");	
        		if(nRow == -1){
        			this.gfn_alert("MSG_CHKDATA_NOSELECT");
        		}else{
        			this.fn_delete();
        		}
        }

        

        /* 저장버튼 클릭 */
        this.btn_save_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail.rowcount;
        	if(nRow < 0 ){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.fn_sendDtl();
        				}
        			});
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.tab_data.tpg_data.div_splitTop.btn_searchUrkey.addEventHandler("onclick", this.tab_data_tpg_data_div_splitTop_btn_searchUrkey_onclick, this);
            this.tab_data.tpg_usgr.div_splitTop.btn_searchUrGroup.addEventHandler("onclick", this.tab_data_tpg_usgr_div_splitTop_btn_searchUrGroup_onclick, this);
            this.tab_data.tpg_usgr.div_splitTop.cmb_eqtype.addEventHandler("onitemchanged", this.tab_data_tpg_usgr_div_splitTop_cmb_eqtype_onitemchanged, this);
            this.div_splitBottom.Static42.addEventHandler("onclick", this.div_splitBottom_Static42_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.btn_delete.addEventHandler("onclick", this.btn_delete_onclick, this);
            this.btn_add.addEventHandler("onclick", this.div_splitBottom_btn_add_onclick, this);
            this.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);

        };

        this.loadIncludeScript("PushSendPop.xfdl", true);

       
    };
}
)();
