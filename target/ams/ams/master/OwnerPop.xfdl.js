﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("OwnerPop");
                this.set_classname("style01");
                this.set_titletext("화주 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,468);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"divcd\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"sttcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd1\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"256\"/><Column id=\"stacd\" type=\"STRING\" size=\"256\"/><Column id=\"boxtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"corp2id\" type=\"STRING\" size=\"32\"/><Column id=\"businesstype\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"contact\" type=\"STRING\" size=\"32\"/><Column id=\"memoid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"delsttvrftyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd2\" type=\"STRING\" size=\"32\"/><Column id=\"fax\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"regno\" type=\"STRING\" size=\"32\"/><Column id=\"addnt\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"lakey_desc\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"256\"/><Column id=\"zipcd\" type=\"STRING\" size=\"256\"/><Column id=\"brcdtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"billtocustcd\" type=\"STRING\" size=\"32\"/><Column id=\"custtype\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"currtyp\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"STRING\" size=\"256\"/><Column id=\"regno1\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"32\"/><Column id=\"regno2\" type=\"STRING\" size=\"32\"/><Column id=\"regno3\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"dimension_uom\" type=\"STRING\" size=\"32\"/><Column id=\"currtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"tel1\" type=\"STRING\" size=\"32\"/><Column id=\"tel2\" type=\"STRING\" size=\"32\"/><Column id=\"dftdestshpgloccd\" type=\"STRING\" size=\"32\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/><Column id=\"alwmxdyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ceo\" type=\"STRING\" size=\"32\"/><Column id=\"corp1id\" type=\"STRING\" size=\"32\"/><Column id=\"custtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ctynm\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"32\"/><Column id=\"weburl\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"256\"/><Column id=\"owname\" type=\"STRING\" size=\"32\"/><Column id=\"boxtype\" type=\"STRING\" size=\"32\"/><Column id=\"email\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd\" type=\"STRING\" size=\"32\"/><Column id=\"weight_uom\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"256\"/><Column id=\"stacd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"32\"/><Column id=\"dftorishpgloccd\" type=\"STRING\" size=\"32\"/><Column id=\"udf1\" type=\"STRING\" size=\"32\"/><Column id=\"udf2\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp\" type=\"STRING\" size=\"32\"/><Column id=\"delsttvrftyp\" type=\"STRING\" size=\"32\"/><Column id=\"businesscategory\" type=\"STRING\" size=\"32\"/><Column id=\"brcdtyp\" type=\"STRING\" size=\"32\"/><Column id=\"alwmxdyn\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addr\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"256\"/><Column id=\"address2\" type=\"STRING\" size=\"256\"/><Column id=\"master_update_impos\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"divcd\"/><Col id=\"cntrycd_desc\"/><Col id=\"sttcd_desc\"/><Col id=\"extcd1\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"stacd\"/><Col id=\"boxtype_desc\"/><Col id=\"corp2id\"/><Col id=\"businesstype\"/><Col id=\"updatedate\"/><Col id=\"contact\"/><Col id=\"memoid\"/><Col id=\"delsttvrftyp_desc\"/><Col id=\"extcd2\"/><Col id=\"fax\"/><Col id=\"loggrpcd_desc\"/><Col id=\"lat\"/><Col id=\"regno\"/><Col id=\"addnt\"/><Col id=\"owkey\"/><Col id=\"lakey_desc\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"brcdtyp_desc\"/><Col id=\"billtocustcd\"/><Col id=\"custtype\"/><Col id=\"currtyp\"/><Col id=\"unit\"/><Col id=\"regno1\"/><Col id=\"loggrpcd\"/><Col id=\"regno2\"/><Col id=\"regno3\"/><Col id=\"uomtyp_desc\"/><Col id=\"dimension_uom\"/><Col id=\"currtyp_desc\"/><Col id=\"tel1\"/><Col id=\"tel2\"/><Col id=\"dftdestshpgloccd\"/><Col id=\"lon\"/><Col id=\"alwmxdyn_desc\"/><Col id=\"ceo\"/><Col id=\"corp1id\"/><Col id=\"custtype_desc\"/><Col id=\"ctynm\"/><Col id=\"lakey\"/><Col id=\"weburl\"/><Col id=\"stnm\"/><Col id=\"owname\"/><Col id=\"boxtype\"/><Col id=\"email\"/><Col id=\"sttcd\"/><Col id=\"weight_uom\"/><Col id=\"addrsttcd\"/><Col id=\"stacd_desc\"/><Col id=\"udf3\"/><Col id=\"dftorishpgloccd\"/><Col id=\"udf1\"/><Col id=\"udf2\"/><Col id=\"uomtyp\"/><Col id=\"delsttvrftyp\"/><Col id=\"businesscategory\"/><Col id=\"brcdtyp\"/><Col id=\"alwmxdyn\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/><Col id=\"addr\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_boxtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_custtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_addr", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_rel_duedate", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_cartype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("36");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "23", this);
            obj.set_taborder("34");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("화주명");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "89", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사업자등록 번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("팩스 번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "151", "490", "62", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "453", "62", "165", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "124", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업태");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "120", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업종");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesstype", "absolute", "148", "124", "165", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            obj.set_maxlength("16");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "23", this);
            obj.set_taborder("35");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesscategory", "absolute", "453", "124", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            obj.set_maxlength("16");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "105", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            obj.set_inputmode("upper");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fax", "absolute", "453", "93", "165", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_enable("true");
            obj.set_maxlength("33");
            obj.set_inputtype("number,sign");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "141", "238", "490", "31", null, null, this);
            obj.set_taborder("433");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "238", "120", "31", null, null, this);
            obj.set_taborder("435");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("대표 이사");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "269", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호1");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "269", "490", "31", null, null, this);
            obj.set_taborder("439");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "269", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호2");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel2", "absolute", "453", "273", "165", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "327", "238", "120", "31", null, null, this);
            obj.set_taborder("448");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("담당자");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_contact", "absolute", "453", "242", "165", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_enable("true");
            obj.set_maxlength("16");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel1", "absolute", "148", "273", "165", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ceo", "absolute", "148", "242", "165", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_enable("true");
            obj.set_maxlength("16");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "256", "62", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAddr", "absolute", "289", "155", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "22", "300", "120", "31", null, null, this);
            obj.set_taborder("486");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("수주마감시간");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "142", "300", "490", "31", null, null, this);
            obj.set_taborder("487");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "73", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "696", "62", "165", "22", null, null, this);
            obj.set_taborder("515");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_unit", "absolute", "696", "86", "165", "22", null, null, this);
            obj.set_taborder("516");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "696", "110", "165", "22", null, null, this);
            obj.set_taborder("517");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "696", "134", "165", "22", null, null, this);
            obj.set_taborder("518");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "696", "158", "165", "22", null, null, this);
            obj.set_taborder("519");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "696", "182", "165", "22", null, null, this);
            obj.set_taborder("520");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "696", "206", "165", "22", null, null, this);
            obj.set_taborder("521");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "696", "230", "165", "22", null, null, this);
            obj.set_taborder("522");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_type("string");
            obj.set_mask("###-##-#####");
            obj.set_limitbymask("integer");
            obj.style.set_background("#edededff");
            obj.style.set_align("right middle");
            obj.set_readonly("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "150", "120", "31", null, null, this);
            obj.set_taborder("530");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "148", "154", "138", "22", null, null, this);
            obj.set_taborder("531");
            obj.set_maxlength("10");
            obj.set_visible("true");
            obj.set_enable("false");
            obj.set_imemode("none");
            obj.set_inputtype("number");
            this.addChild(obj.name, obj);

            obj = new Static("Static51", "absolute", "22", "177", "120", "30", null, null, this);
            obj.set_taborder("532");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static52", "absolute", "22", "208", "120", "30", null, null, this);
            obj.set_taborder("533");
            obj.set_text("상세 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_background("#f6f6f6ff");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static53", "absolute", "141", "177", "490", "30", null, null, this);
            obj.set_taborder("534");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static54", "absolute", "141", "209", "490", "30", null, null, this);
            obj.set_taborder("535");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "212", "470", "22", null, null, this);
            obj.set_taborder("536");
            obj.set_maxlength("16");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "181", "469", "22", null, null, this);
            obj.set_taborder("537");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_udf1ordend", "absolute", "22.53%", "304", null, "22", "52.36%", null, this);
            obj.set_taborder("541");
            obj.set_type("string");
            obj.set_mask("99:99:99");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "142", "332", "490", "31", null, null, this);
            obj.set_taborder("542");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "328", "332", "120", "31", null, null, this);
            obj.set_taborder("538");
            obj.set_text("삭제");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_alwmxdyn00", "absolute", "454", "336", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("540");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_readonly("true");
            obj.style.set_background("#edededff");
            obj.set_enable("false");

            obj = new Static("Static03", "absolute", "326", "300", "120", "31", null, null, this);
            obj.set_taborder("543");
            obj.set_text("변경 수주마감시간");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_udf2ordend", "absolute", "68.8%", "304", null, "22", "6.09%", null, this);
            obj.set_taborder("544");
            obj.set_type("string");
            obj.set_mask("99:99:99");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static62", "absolute", "632", "655", "122", "31", null, null, this);
            obj.set_taborder("545");
            obj.set_text("출고마감일자");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "632", "647", "122", "31", null, null, this);
            obj.set_taborder("548");
            obj.set_text("출고마감일자");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "640", "655", "122", "31", null, null, this);
            obj.set_taborder("549");
            obj.set_text("출고마감일자");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "22", "332", "120", "31", null, null, this);
            obj.set_taborder("550");
            obj.set_text("출고마감일자");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("edt_release_duedate", "absolute", "148", "336", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("551");
            obj.set_readonly("false");
            obj.set_innerdataset("@ds_rel_duedate");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_enable("true");
            obj.style.set_background("#edededff");

            obj = new Static("Static13", "absolute", "142", "363", "490", "31", null, null, this);
            obj.set_taborder("552");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "22", "363", "120", "31", null, null, this);
            obj.set_taborder("553");
            obj.set_text("마스터수정 여부");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new CheckBox("chk_rstclgst", "absolute", "22.53%", "371", null, "20", "54.64%", null, this);
            obj.set_taborder("554");
            obj.set_value("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "327", "151", "120", "31", null, null, this);
            obj.set_taborder("555");
            obj.set_text("청구고객코드");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_billtocustcd", "absolute", "453", "155", "165", "22", null, null, this);
            obj.set_taborder("557");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "328", "363", "120", "31", null, null, this);
            obj.set_taborder("558");
            obj.set_text("운송구분");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Combo("edt_cartype", "absolute", "454", "367", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("559");
            obj.set_innerdataset("@ds_cartype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.style.set_background("#edededff");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 468, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("화주 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_owname","value","ds_header","owname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_businesstype","value","ds_header","businesstype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","OWNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","FAX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","BUSINESSTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","BUSINESSCATEGORY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_businesscategory","value","ds_header","businesscategory");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_fax","value","ds_header","fax");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","CEO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","TEL1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","TEL2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_tel2","value","ds_header","tel2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static31","text","gds_lang","CONTACT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item54","edt_contact","value","ds_header","contact");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_tel1","value","ds_header","tel1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","edt_ceo","value","ds_header","ceo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","OWNER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","Static24","text","gds_lang","UDF1ORDEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","edt_unit","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","edt_ctynm","value","ds_header","ctynm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item72","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Static05","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item47","Static51","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item73","Static52","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item74","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item75","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item77","Static07","text","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item78","cbo_alwmxdyn00","value","ds_header","delyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","mdt_udf1ordend","value","ds_header","udf1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","Static03","text","gds_lang","UDF2ORDEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","mdt_udf2ordend","value","ds_header","udf2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item87","Static62","text","gds_lang","RELEASE_DUEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","Static06","text","gds_lang","RELEASE_DUEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","Static11","text","gds_lang","RELEASE_DUEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","Static12","text","gds_lang","RELEASE_DUEDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","edt_release_duedate","value","ds_header","udf3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Static17","text","gds_lang","MASTER_UPDATE_IMPOS");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","Static19","text","gds_lang","BILLTOCUSTCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","edt_billtocustcd","value","ds_header","billtocustcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","Static20","text","gds_lang","CARTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","edt_cartype","value","ds_header","cartype");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("OwnerPop.xfdl", "lib::Comm.xjs");
        this.registerScript("OwnerPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : OwnerPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 화주 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("RELEASE_DUEDATE", this.ds_rel_duedate, "");
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd, "");
        	this.gfn_getCode("CARTYPE", this.ds_cartype, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);

        	this.ds_header.applyChange();
            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "selectOwner");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_FORM_DATA_MAP ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "checkOwnerCount");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.ds_header.getColumn(0, "owkey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "saveOwner");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "saveOwner");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        // 		var sAddr = this.gfn_isNullEmpty(this.ds_header.getColumn(0, "cntrycd_desc")) + " " + 
        // 					this.gfn_isNullEmpty(this.ds_header.getColumn(0, "stacd_desc")) + " " + 
        // 					this.gfn_isNullEmpty(this.ds_header.getColumn(0, "zipNo"))
        // 				  ;
        // 		this.txa_addr.set_value(sAddr);

        		//this.ds_header.applyChange();
        		
        		//this.chk_rstclgst.set_value(this.gfn_booleanYNConvert(this.ds_header.getColumn(0, "master_update_impos")));
        		
        		
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}else{
        			this.chk_rstclgst.set_value(this.gfn_booleanYNConvert(this.ds_header.getColumn(0, "master_update_impos")));
        		
        		}
        		
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_owkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|owname|zipNo|address2";
        	var sComp = "edt_owkey|edt_owname|btn_searchAddr|text_address2|btn_searchLakey|btn_searchSttcd";
        	
        	/*수주마감시간 시간 체크 루틴*/
        	var nDtChk = "";
        	if (!this.gfn_isNull(this.mdt_udf1ordend.value))
        	    nDtChk = this.mdt_udf1ordend.value;
        	    
        	if(nDtChk.length > 0 && Number(nDtChk) > 0 ) {
        		if(nDtChk.substring(0,1) == "0"){
        			if(nDtChk.substring(1,2) == "0")
        				nDtChk = "00"+Number(this.mdt_udf1ordend.value)+"";
        			else
        				nDtChk = "0"+Number(this.mdt_udf1ordend.value)+"";
        		} else 
        		  nDtChk = Number(this.mdt_udf1ordend.value)+"";
        		if( nDtChk.length != 6 || 
        		   ( Number(nDtChk.substring(0,2)) > 23  ||  Number(nDtChk.substring(0,2)) < 0)||
        		   ( Number(nDtChk.substring(2,4)) < 0 || Number(nDtChk.substring(2,4)) > 59)||
        		   ( Number(nDtChk.substring(4,6)) < 0 || Number(nDtChk.substring(4,6)) > 59)){
        		  this.gfn_alert("MSG_CHKDATA_NOTIME", "", function(msg, flag){
        					this.mdt_udf1ordend.setFocus();
        				});
        		  return;
        		 }
        	}
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        			console.log();
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						this.ds_header.setColumn(0, "master_update_impos", this.gfn_booleanYNConvert(this.chk_rstclgst.value));
        						
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") this.fn_Update();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchAddr_onclick 실행 */
        this.btn_searchAddr_onclick = function(obj,e)
        {
        // 	this.gv_Pvalue = "";
        // 	
        // 	var oArg = { 
        // 				putDs:this.ds_header,
        // 				putKey:"zipNo|lnmAdres",
        // 				oRow:""+0
        // 			   };
        // 	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }

        /* btn_searchLakey_onclick 실행 */
        this.btn_searchLakey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:"20"
        				,putObj:this.edt_lakey
        				,putKey:"adcd_hdkey"
        				,putValue:"LAKEY"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchCurrtyp_onclick 실행 */
        this.btn_searchCurrtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_currtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"CURRTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchBrcdtyp_onclick 실행 */
        this.btn_searchBrcdtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_brcdtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"BRCDTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchUomtyp_onclick 실행 */
        this.btn_searchUomtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_uomtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"UOMTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchSttcd_onclick 실행 */
        this.btn_searchSttcd_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_sttcd
        				,putKey:"adcd_hdkey"
        				,putValue:"STTCD"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey";
        	var sComp = "edt_owkey";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey"){
        		this.gv_dupChk = false;
        	}else if(e.columnid == "lakey"){
        		this.edt_laname.set_value(this.gv_Pvalue[1]);
        		this.edt_laname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "currtyp"){
        		this.edt_currtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_currtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "brcdtyp"){
        		this.edt_brcdtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_brcdtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "uomtyp"){
        		this.edt_uomtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_uomtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "sttcd"){
        		this.edt_sttname.set_value(this.gv_Pvalue[1]);
        		this.edt_sttname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        }

        /* edt_lakey_onchanged 실행 */
        this.edt_lakey_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "LAKEY";
        	
        	if(this.gfn_isNotNull(this.edt_lakey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_lakey.value, "20", application.gv_ams, "", "", this.edt_lakey, this.edt_laname);
        	}
        }

        /* edt_currtyp_onchanged 실행 */
        this.edt_currtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "CURRTYP";
        	
        	if(this.gfn_isNotNull(this.edt_currtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_currtyp.value, "20", application.gv_ams, "", "", this.edt_currtyp, this.edt_currtypname);
        	}
        }

        /* edt_brcdtyp_onchanged 실행 */
        this.edt_brcdtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "BRCDTYP";
        	
        	if(this.gfn_isNotNull(this.edt_brcdtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_brcdtyp.value, "20", application.gv_ams, "", "", this.edt_brcdtyp, this.edt_brcdtypname);
        	}
        }

        /* edt_uomtyp_onchanged 실행 */
        this.edt_uomtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "UOMTYP";
        	
        	if(this.gfn_isNotNull(this.edt_uomtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_uomtyp.value, "20", application.gv_ams, "", "", this.edt_uomtyp, this.edt_uomtypname);
        	}
        }

        /* edt_sttcd_onchanged 실행 */
        this.edt_sttcd_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "STTCD";
        	
        	if(this.gfn_isNotNull(this.edt_sttcd.value)){
        		this.gv_code = "Y";
        		this.gfn_codeSearch(key, value, this.edt_sttcd.value, "20", application.gv_ams, "", "", this.edt_sttcd, this.edt_sttname);
        	}
        }

        /*수주마감시간 시간 체크 루틴*/
        this.mdt_udf1ordend_onchanged = function(obj,e)
        {
            var nDtChk = "";
        	if (!this.gfn_isNull(this.mdt_udf1ordend.value))
        	    nDtChk = this.mdt_udf1ordend.value;
        	if( nDtChk.length < 1 || Number(nDtChk) < 1) return;
        	if(nDtChk.substring(0,1) == "0"){
        		if(nDtChk.substring(1,2) == "0")
        			nDtChk = "00"+Number(this.mdt_udf1ordend.value)+"";
        		else
        			nDtChk = "0"+Number(this.mdt_udf1ordend.value)+"";
        	} else 
        	  nDtChk = Number(this.mdt_udf1ordend.value)+"";
        	if( nDtChk.length != 6 || 
        	   ( Number(nDtChk.substring(0,2)) > 23  ||  Number(nDtChk.substring(0,2)) < 0)||
        	   ( Number(nDtChk.substring(2,4)) < 0 || Number(nDtChk.substring(2,4)) > 59)||
        	   ( Number(nDtChk.substring(4,6)) < 0 || Number(nDtChk.substring(4,6)) > 59)){
        	  this.gfn_alert("MSG_CHKDATA_NOTIME", "", function(msg, flag){
        				this.mdt_udf1ordend.setFocus();
        			});
        	  return;
        	 }
        }

        /*변경 수주마감시간 시간 체크 루틴*/
        this.mdt_udf2ordend_onchanged = function(obj,e)
        {
            var nDtChk = "";
        	if (!this.gfn_isNull(this.mdt_udf2ordend.value))
        	    nDtChk = this.mdt_udf2ordend.value;
        	if( nDtChk.length < 1 || Number(nDtChk) < 1) return;
        	if(nDtChk.substring(0,1) == "0"){
        		if(nDtChk.substring(1,2) == "0")
        			nDtChk = "00"+Number(this.mdt_udf2ordend.value)+"";
        		else
        			nDtChk = "0"+Number(this.mdt_udf2ordend.value)+"";
        	} else 
        	  nDtChk = Number(this.mdt_udf2ordend.value)+"";
        	if( nDtChk.length != 6 || 
        	   ( Number(nDtChk.substring(0,2)) > 23  ||  Number(nDtChk.substring(0,2)) < 0)||
        	   ( Number(nDtChk.substring(2,4)) < 0 || Number(nDtChk.substring(2,4)) > 59)||
        	   ( Number(nDtChk.substring(4,6)) < 0 || Number(nDtChk.substring(4,6)) > 59)){
        	  this.gfn_alert("MSG_CHKDATA_NOTIME", "", function(msg, flag){
        				this.mdt_udf2ordend.setFocus();
        			});
        	  return;
        	 }
        }
        /* chk_rstclgst_onchanged 실행 */
        this.chk_rstclgst_onchanged = function(obj,e)
        {
        	this.ds_header.setColumn(0, "master_update_impos", this.gfn_booleanYNConvert(obj.value));
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_owname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_fax.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_tel2.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_ceo.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.btn_searchAddr.addEventHandler("onclick", this.btn_searchAddr_onclick, this);
            this.edt_zipcode.addEventHandler("oneditclick", this.edt_zipcode_oneditclick, this);
            this.mdt_udf1ordend.addEventHandler("onchanged", this.mdt_udf1ordend_onchanged, this);
            this.Static07.addEventHandler("onclick", this.Static07_onclick, this);
            this.mdt_udf2ordend.addEventHandler("onchanged", this.mdt_udf2ordend_onchanged, this);
            this.chk_rstclgst.addEventHandler("onchanged", this.chk_rstclgst_onchanged, this);

        };

        this.loadIncludeScript("OwnerPop.xfdl", true);

       
    };
}
)();
