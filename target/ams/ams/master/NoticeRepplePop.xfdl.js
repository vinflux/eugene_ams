﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("noticeCreate");
                this.set_classname("notice");
                this.set_titletext("공지사항 등록");
                this._setFormPosition(0,0,588,527);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"INSERTURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEDATE\" type=\"STRING\" size=\"256\"/><Column id=\"PARENT_MSG_NO\" type=\"STRING\" size=\"16\"/><Column id=\"TITLE\" type=\"STRING\" size=\"32\"/><Column id=\"INSERTDATE\" type=\"STRING\" size=\"32\"/><Column id=\"CONTENT\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"UPPER_MSG_NO\" type=\"STRING\" size=\"256\"/><Column id=\"MSG_TYPE\" type=\"STRING\" size=\"256\"/><Column id=\"START_DT\" type=\"STRING\" size=\"256\"/><Column id=\"END_DT\" type=\"STRING\" size=\"256\"/><Column id=\"TARGET\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/><Column id=\"OPEN_YN\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row/></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static08", "absolute", "160", "86", null, "401", "17", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "160", "54", null, "31", "14", null, this);
            obj.set_taborder("1");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("2");
            obj.set_text("댓글입력");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "21", "40", null, "2", "23", null, this);
            obj.set_taborder("3");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "302", "8", this);
            obj.set_taborder("5");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "20", "52", null, "1", "24", null, this);
            obj.set_taborder("6");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "245", "8", this);
            obj.set_taborder("8");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "22", "54", "138", "31", null, null, this);
            obj.set_taborder("12");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static07", "absolute", "22", "86", "138", "401", null, null, this);
            obj.set_taborder("13");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_notice_title", "absolute", "166", "58", "402", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_maxlength("200");
            this.addChild(obj.name, obj);

            obj = new TextArea("edt_notice_content", "absolute", "165", "86", "402", "396", null, null, this);
            obj.set_taborder("18");
            obj.set_maxlength("3000");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 588, 527, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("공지사항 등록");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","RIPPLE_POP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","NOTICE_TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static07","text","gds_lang","NOTICE_CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_notice_title","value","ds_header","TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_notice_content","value","ds_header","CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("NoticeRepplePop.xfdl", "lib::Comm.xjs");
        this.registerScript("NoticeRepplePop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : boardPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 게시판 팝업 
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ackey = "";
        this.gv_loggrpcd = "";
        this.gv_dupChk = false;
        this.gv_Pvalue = "";
        this.gv_upper_no_no = "";
        this.gv_reply = "";
        this.gv_status = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
                   
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.ds_header.setColumn(0,"PARENT_MSG_NO",this.parent.parent_msg_no);
            this.ds_header.setColumn(0,"UPPER_MSG_NO",this.parent.msg_no);
            this.ds_header.setColumn(0,"MSG_TYPE",this.parent.msg_type);
            this.ds_header.setColumn(0,"START_DT",this.parent.start_dt);
            this.ds_header.setColumn(0,"END_DT",this.parent.end_dt);
            this.ds_header.setColumn(0,"OPEN_YN","Y");
            this.ds_header.setColumn(0,"TARGET","3");
            this.ds_header.setColumn(0,"STATUS","I");
        	this.edt_notice_title.setFocus();
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/

        
        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "saveNoticeRepple");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT"
                         + " ctkey="+this.gfn_getUserInfo("ctKey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {	
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "TITLE|CONTENT";
        	var sComp = "edt_notice_title|edt_notice_content";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        					this.fn_Insert();
        				}
        				else 
        					trace(this.ds_header.saveXML());
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        	
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);

        };

        this.loadIncludeScript("NoticeRepplePop.xfdl", true);

       
    };
}
)();
