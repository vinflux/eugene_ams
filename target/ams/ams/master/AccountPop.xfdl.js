﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("AccountPop");
                this.set_classname("style01");
                this.set_titletext("거래처 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,630);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"tel1\" type=\"STRING\" size=\"256\"/><Column id=\"acname\" type=\"STRING\" size=\"32\"/><Column id=\"tel2\" type=\"STRING\" size=\"256\"/><Column id=\"rep_actype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkey\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ceo\" type=\"STRING\" size=\"32\"/><Column id=\"businesstype\" type=\"STRING\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"pltaplyyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"contact\" type=\"STRING\" size=\"256\"/><Column id=\"fax\" type=\"STRING\" size=\"256\"/><Column id=\"icgrkey_desc\" type=\"STRING\" size=\"256\"/><Column id=\"asnuseyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"email\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"scmcheck\" type=\"STRING\" size=\"32\"/><Column id=\"regno\" type=\"STRING\" size=\"32\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"address1\" type=\"STRING\" size=\"32\"/><Column id=\"actype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"rep_actype\" type=\"STRING\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"256\"/><Column id=\"pltmaxrt\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"udf1\" type=\"STRING\" size=\"256\"/><Column id=\"pltaplyyn\" type=\"STRING\" size=\"32\"/><Column id=\"udf2\" type=\"STRING\" size=\"256\"/><Column id=\"rep_ackey\" type=\"STRING\" size=\"32\"/><Column id=\"zipcode\" type=\"STRING\" size=\"256\"/><Column id=\"actype\" type=\"STRING\" size=\"32\"/><Column id=\"scmurkey\" type=\"STRING\" size=\"256\"/><Column id=\"businesscategory\" type=\"STRING\" size=\"256\"/><Column id=\"ackey\" type=\"STRING\" size=\"32\"/><Column id=\"regdate\" type=\"STRING\" size=\"256\"/><Column id=\"addrid\" type=\"STRING\" size=\"256\"/><Column id=\"asnuseyn\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"pltgrrt\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"address2\" type=\"STRING\" size=\"256\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"tel1\"/><Col id=\"acname\"/><Col id=\"tel2\"/><Col id=\"rep_actype_desc\"/><Col id=\"icgrkey\"/><Col id=\"inserturkey\"/><Col id=\"scmcheck_desc\"/><Col id=\"ceo\"/><Col id=\"businesstype\"/><Col id=\"delyn_desc\"/><Col id=\"delyn\"/><Col id=\"pltaplyyn_desc\"/><Col id=\"updatedate\"/><Col id=\"contact\"/><Col id=\"fax\"/><Col id=\"icgrkey_desc\"/><Col id=\"asnuseyn_desc\"/><Col id=\"email\"/><Col id=\"closingdate\"/><Col id=\"scmcheck\"/><Col id=\"regno\"/><Col id=\"owkey\"/><Col id=\"address1\"/><Col id=\"actype_desc\"/><Col id=\"rep_actype\"/><Col id=\"udf3\"/><Col id=\"pltmaxrt\"/><Col id=\"udf1\"/><Col id=\"pltaplyyn\"/><Col id=\"udf2\"/><Col id=\"rep_ackey\"/><Col id=\"zipcode\"/><Col id=\"actype\"/><Col id=\"scmurkey\"/><Col id=\"businesscategory\"/><Col id=\"ackey\"/><Col id=\"regdate\"/><Col id=\"addrid\"/><Col id=\"asnuseyn\"/><Col id=\"insertdate\"/><Col id=\"pltgrrt\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_repActype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_actype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static07", "absolute", "142", "400", "490", "31", null, null, this);
            obj.set_taborder("479");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "142", "493", "490", "31", null, null, this);
            obj.set_taborder("422");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "142", "151", "490", "31", null, null, this);
            obj.set_taborder("385");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("34");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", "578", "52", "28", null, null, this);
            obj.set_taborder("26");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "694", "41", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("매입처 코드");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "23", "89", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("매입처명");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "182", "490", "30", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "213", "120", "30", null, null, this);
            obj.set_taborder("325");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("상세 주소");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "22", "462", "120", "31", null, null, this);
            obj.set_taborder("331");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사용자 정의 필드1");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit03", "absolute", "148", "466", "373", "22", null, null, this);
            obj.set_taborder("336");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "142", "462", "490", "31", null, null, this);
            obj.set_taborder("337");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "124", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "120", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업태");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("384");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업종");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "148", "373", "373", "22", null, null, this);
            obj.set_taborder("19");
            this.addChild(obj.name, obj);

            obj = new Static("Static36", "absolute", "142", "369", "490", "31", null, null, this);
            obj.set_taborder("388");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static37", "absolute", "22", "369", "120", "31", null, null, this);
            obj.set_taborder("389");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("상품 그룹 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static38", "absolute", "327", "400", "120", "31", null, null, this);
            obj.set_taborder("390");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("반영일자");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "141", "431", "490", "31", null, null, this);
            obj.set_taborder("391");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static55", "absolute", "22", "431", "120", "31", null, null, this);
            obj.set_taborder("401");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("이메일");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesstype", "absolute", "453", "124", "165", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static35", "absolute", "327", "151", "120", "31", null, null, this);
            obj.set_taborder("386");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("Asn 자동생성 여부");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", "578", "52", "28", null, null, this);
            obj.set_taborder("27");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesscategory", "absolute", "148", "124", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "138", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_acname", "absolute", "149", "93", "165", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("true");
            obj.set_maxlength("33");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "493", "120", "31", null, null, this);
            obj.set_taborder("416");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사용자 정의 필드2");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icgrkey", "absolute", "148", "373", "138", "22", null, null, this);
            obj.set_taborder("18");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchIcgrkey", "absolute", "289", "373", "24", "22", null, null, this);
            obj.set_taborder("28");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "141", "245", "490", "31", null, null, this);
            obj.set_taborder("433");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "276", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호1");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "276", "490", "31", null, null, this);
            obj.set_taborder("439");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "276", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "148", "311", "373", "22", null, null, this);
            obj.set_taborder("441");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "142", "307", "490", "31", null, null, this);
            obj.set_taborder("442");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "22", "307", "120", "31", null, null, this);
            obj.set_taborder("443");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("팩스 번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "327", "307", "120", "31", null, null, this);
            obj.set_taborder("444");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("대표 이사");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel2", "absolute", "453", "280", "165", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "23", "244", "120", "31", null, null, this);
            obj.set_taborder("448");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("담당자");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_contact", "absolute", "149", "248", "165", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("true");
            obj.set_maxlength("10");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel1", "absolute", "148", "280", "165", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ceo", "absolute", "453", "311", "165", "22", null, null, this);
            obj.set_taborder("15");
            obj.set_enable("true");
            obj.set_maxlength("10");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "141", "338", "490", "31", null, null, this);
            obj.set_taborder("455");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "22", "400", "120", "31", null, null, this);
            obj.set_taborder("456");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("대표매입처 유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "338", "120", "31", null, null, this);
            obj.set_taborder("457");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("대표매입처 코드");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_rep_actype", "absolute", "148", "404", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("20");
            obj.set_innerdataset("@ds_repActype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Button("btn_searchOwkey", "absolute", "289", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "928", "45", "57", "22", null, null, this);
            obj.set_taborder("30");
            obj.set_text("중복체크");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Radio("rdo_asnuseynComp", "absolute", "453", "155", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            var rdo_asnuseynComp_innerdataset = new Dataset("rdo_asnuseynComp_innerdataset", this.rdo_asnuseynComp);
            rdo_asnuseynComp_innerdataset._setContents("<ColumnInfo><Column id=\"codecolumn\" size=\"256\"/><Column id=\"datacolumn\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"codecolumn\">Y</Col><Col id=\"datacolumn\">YES</Col></Row><Row><Col id=\"codecolumn\">N</Col><Col id=\"datacolumn\">NO</Col></Row></Rows>");
            obj.set_innerdataset(rdo_asnuseynComp_innerdataset);
            obj.set_taborder("10");
            obj.set_codecolumn("codecolumn");
            obj.set_datacolumn("datacolumn");
            obj.set_columncount("2");
            obj.set_value("Y");

            obj = new Edit("edt_zipcode", "absolute", "148", "155", "138", "22", null, null, this);
            obj.set_taborder("31");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fax", "absolute", "148", "311", "165", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_rep_ackey", "absolute", "148", "342", "138", "22", null, null, this);
            obj.set_taborder("16");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchRepAckey", "absolute", "289", "342", "24", "22", null, null, this);
            obj.set_taborder("17");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_regdate", "absolute", "453", "404", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("21");
            obj.set_dateformat("yyyy-MM-dd");
            obj.style.set_cursor("hand");

            obj = new Edit("edt_email", "absolute", "148", "435", "470", "22", null, null, this);
            obj.set_taborder("22");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf1", "absolute", "148", "466", "470", "22", null, null, this);
            obj.set_taborder("23");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf2", "absolute", "148", "497", "470", "22", null, null, this);
            obj.set_taborder("24");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "142", "524", "490", "31", null, null, this);
            obj.set_taborder("477");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "524", "120", "31", null, null, this);
            obj.set_taborder("478");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사용자 정의 필드3");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf3", "absolute", "148", "528", "470", "22", null, null, this);
            obj.set_taborder("25");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", "555", null, "1", "22", null, this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_rep_acname", "absolute", "327", "342", "291", "22", null, null, this);
            obj.set_taborder("32");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_icgrname", "absolute", "327", "373", "291", "22", null, null, this);
            obj.set_taborder("33");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "327", "62", "291", "22", null, null, this);
            obj.set_taborder("29");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "22", "182", "120", "30", null, null, this);
            obj.set_taborder("482");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "141", "214", "490", "30", null, null, this);
            obj.set_taborder("484");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchZipcd", "absolute", "289", "155", "24", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "151", "120", "31", null, null, this);
            obj.set_taborder("489");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "186", "469", "22", null, null, this);
            obj.set_taborder("490");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "217", "470", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ackey", "absolute", "820", "45", "105", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_inputtype("number,english");
            obj.set_maxlength("20");
            obj.set_inputmode("upper");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "686", "243", "120", "31", null, null, this);
            obj.set_taborder("493");
            obj.set_text("위도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "805", "274", "490", "31", null, null, this);
            obj.set_taborder("494");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lat", "absolute", "812", "278", "165", "22", null, null, this);
            obj.set_taborder("495");
            obj.set_mask("##.####");
            obj.set_limitbymask("both");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "991", "274", "120", "31", null, null, this);
            obj.set_taborder("496");
            obj.set_text("경도");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_lon", "absolute", "1117", "278", "165", "22", null, null, this);
            obj.set_taborder("497");
            obj.set_mask("###.####");
            obj.set_limitbymask("both");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fn_get_master_impos", "absolute", "695", "102", "291", "22", null, null, this);
            obj.set_taborder("498");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("499");
            obj.set_text("매입처 유형");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_actype", "absolute", "453", "93", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("500");
            obj.set_innerdataset("@ds_actype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static28", "absolute", "327", "245", "120", "31", null, null, this);
            obj.set_taborder("501");
            obj.set_text("사업자등록 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "453", "249", "165", "22", null, null, this);
            obj.set_taborder("502");
            obj.set_type("string");
            obj.set_mask("###-##-#####");
            obj.set_limitbymask("integer");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 630, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("거래처 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_ackey","value","ds_header","ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","rdo_asnuseynComp","value","ds_header","asnuseyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item56","edt_fax","value","ds_header","fax");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_businesstype","value","ds_header","businesstype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","ACNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static05","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","Static11","text","gds_lang","UDF1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","BUSINESSTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","Static25","text","gds_lang","BUSINESSCATEGORY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","Static37","text","gds_lang","ICGRKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","Static55","text","gds_lang","EMAIL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item40","Static38","text","gds_lang","REGDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item41","Static35","text","gds_lang","ASNUSEYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_businesscategory","value","ds_header","businesscategory");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_acname","value","ds_header","acname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static01","text","gds_lang","UDF2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item45","edt_icgrkey","value","ds_header","icgrkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","TEL1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","TEL2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","FAX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","CEO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_tel2","value","ds_header","tel2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static31","text","gds_lang","CONTACT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item54","edt_contact","value","ds_header","contact");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_tel1","value","ds_header","tel1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","edt_ceo","value","ds_header","ceo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static43","text","gds_lang","REP_ACTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item59","Static44","text","gds_lang","REP_ACKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_rep_actype","value","ds_header","rep_actype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","ACCOUNT HEADER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","edt_rep_ackey","value","ds_header","rep_ackey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","cal_regdate","value","ds_header","regdate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_email","value","ds_header","email");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_udf1","value","ds_header","udf1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","edt_udf2","value","ds_header","udf2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","Static06","text","gds_lang","UDF3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","edt_udf3","value","ds_header","udf3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","Static02","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","Static30","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","mdt_lat","value","ds_header","lat");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","Static34","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","mdt_lon","value","ds_header","lon");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","Static19","text","gds_lang","ACTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","cbo_actype","value","ds_header","actype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("AccountPop.xfdl", "lib::Comm.xjs");
        this.registerScript("AccountPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : AccountPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 거래처 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ackey = "";
        this.gv_zipcode = "";
        this.gv_address2 = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("REP_ACTYPE", this.ds_repActype, "");
        	this.gfn_getCode("ACTYPE", this.ds_actype, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_ackey    = this.gfn_isNullEmpty(this.parent.ackey);
            this.gv_zipcode    = this.gfn_isNullEmpty(this.parent.zipcode);
            this.gv_address2    = this.gfn_isNullEmpty(this.parent.address2);
        	var owkey = this.gfn_getUserInfo("owkeym").split(",");
        	this.ds_header.setColumn(0, "owkey", owkey[0]);
            this.ds_header.setColumn(0, "asnuseyn", "Y");
            this.ds_header.setColumn(0, "delyn", "N");
        	this.ds_header.setColumn(0, "lon", 0);
        	this.ds_header.setColumn(0, "lat", 0);

        	this.ds_header.applyChange();

            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_ackey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.cbo_actype.setFocus();
        		this.fn_search();
        	}else{
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        	}
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "accountController");
        	this.gfn_setCommon("METHODNM", "selectAccount");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " ackey="+this.gv_ackey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchCode = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "accountController");
        	this.gfn_setCommon("METHODNM", "selectRepAccountInfo");
        	
            var sSvcId   = "selectCode";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "searchValue="+this.edt_rep_ackey.value
        				 + " owkey="+this.gv_owkey
                         + " opval=eq"
        				 ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "accountController");
        	this.gfn_setCommon("METHODNM", "selectAccount");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   =  "owkey="+this.ds_header.getColumn(0, "owkey")
                         + " ackey="+this.ds_header.getColumn(0, "ackey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "accountController");
        	this.gfn_setCommon("METHODNM", "saveAccount");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "accountController");
        	this.gfn_setCommon("METHODNM", "saveAccount");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        		
        		if(this.gfn_isNotNull(this.edt_rep_ackey.value)){
        			this.gv_code = "Y";
        			this.fn_searchCode();
        		}
        		
        		key = "icgrkey|adcd_hdkey|OWKEY_SELECTION_LIST";
        		value = this.ds_header.getColumn(0, "icgrkey")+"|ICGRKEY"+"|"+this.edt_owkey.value;
        		
        		if(this.gfn_isNotNull(this.edt_icgrkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_icgrkey.value, "20", application.gv_ams, "", "", this.edt_icgrkey, this.edt_icgrname);
        		}
        	}else if(sSvcId == "selectCode"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_rep_acname.set_value(this.ds_code.getColumn(0, "acname"));
        			this.edt_rep_acname.set_tooltiptext(this.ds_code.getColumn(0, "acname"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_rep_acname.set_value("");
        					this.edt_rep_ackey.setFocus();
        				});
        			}else{
        				this.edt_rep_acname.set_value("");
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cbo_icuttype.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|actype|acname|zipNo|address2";
        	var sComp = "edt_owkey|cbo_actype|edt_acname|edt_zipcode|text_address2";
        	
        // 	if(!this.gv_dupChk && this.gv_flag == "H"){
        // 		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        // 			this.cbo_icuttype.setFocus();
        // 		});
        	if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_rep_acname.value) && !this.gfn_isNull(this.edt_rep_ackey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_rep_ackey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_icgrname.value) && !this.gfn_isNull(this.edt_icgrkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_icgrkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") {
        							//주소가 변경되면 새로운 주소ID 생성하기 위한 로직
        							if(this.gv_zipcode != this.edt_zipcode.value || this.gv_address2 != this.text_address2.value){
        								this.ds_header.setColumn(0, "addrid", "");
        							}
        							this.fn_Update();
        						}
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        	
        }

        /* btn_searchRepAckey_onclick 실행 */
        this.btn_searchRepAckey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	var vOwkey = this.gfn_isNullEmpty(this.edt_owkey.value);
        	if (vOwkey == ""){
        		this.gfn_alert("MSG_INPUT_OWKEY");
        		return;
        	}
        	var oArg = { owkey:vOwkey
        	             ,putObj:this.edt_rep_ackey
        			   };
        	this.gfn_popup("RepAckeyPop", "master::RepAckeyPop.xfdl", oArg, 700, 497, "");
        }

        /* btn_searchIcgrkey_onclick 실행 */
        this.btn_searchIcgrkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_icgrkey
        				,putKey:"adcd_hdkey|OWKEY_SELECTION_LIST"
        				,putValue:"ICGRKEY"+"|"+this.edt_owkey.value
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchZipcd_onclick 실행 */
        this.btn_searchZipcd_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        // 	if(this.gv_dupChk){
        // 		return;
        // 	}
        // 	else if(this.edt_ackey.value.length < 7){
        // 		this.gfn_alert("MSG_80005", "", function(msg, flag){
        // 			this.edt_ackey.setFocus();
        // 		});
        // 		return;
        // 	}
        	
        // 	var dsObj = this.ds_header;
        // 	var dsCol = "owkey|ackey";
        // 	var sComp = "edt_owkey|edt_ackey";
        // 	
        // 	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        // 		this.fn_CheckCode();
        // 	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {		
        	if(e.columnid == "owkey" || e.columnid == "ackey"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "owkey"){
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_fn_get_master_impos.set_value(this.gv_Pvalue[2]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        		}
        		if(this.edt_fn_get_master_impos.value=='Y'){
        			this.gfn_alert("MSG_MASTER_UPDATE_IMPOS");
        			return;
        		}
        	}else if(e.columnid == "rep_ackey"){
        		this.edt_rep_acname.set_value(this.gv_Pvalue[1]);
        		this.edt_rep_acname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "icgrkey"){
        		this.edt_icgrname.set_value(this.gv_Pvalue[1]);
        		this.edt_icgrname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_owkey_onchanged 실행 */
        this.edt_owkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        	}

        }

        /* edt_rep_ackey_onchanged 실행 */
        this.edt_rep_ackey_onchanged = function(obj,e)
        {
        	if(this.gfn_isNotNull(this.edt_rep_ackey.value)){
        		this.gv_code = "";
        		this.fn_searchCode();
        	}
        }

        /* edt_icgrkey_onchanged 실행 */
        this.edt_icgrkey_onchanged = function(obj,e)
        {
        	var key = "icgrkey|adcd_hdkey|OWKEY_SELECTION_LIST";
        	var value = this.ds_header.getColumn(0, "icgrkey")+"|ICGRKEY"+"|"+this.edt_owkey.value;
        	
        	if(this.gfn_isNotNull(this.edt_icgrkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_icgrkey.value, "20", application.gv_ams, "", "", this.edt_icgrkey, this.edt_icgrname);
        	}
        }

        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_owkey.addEventHandler("onchanged", this.edt_owkey_onchanged, this);
            this.edt_acname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_icgrkey.addEventHandler("onchanged", this.edt_icgrkey_onchanged, this);
            this.btn_searchIcgrkey.addEventHandler("onclick", this.btn_searchIcgrkey_onclick, this);
            this.edt_tel2.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_ceo.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.edt_fax.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_rep_ackey.addEventHandler("onchanged", this.edt_rep_ackey_onchanged, this);
            this.btn_searchRepAckey.addEventHandler("onclick", this.btn_searchRepAckey_onclick, this);
            this.cal_regdate.addEventHandler("onkeydown", this.cal_to_onkeydown, this);
            this.cal_regdate.addEventHandler("canchange", this.cal_to_canchange, this);
            this.btn_searchZipcd.addEventHandler("onclick", this.btn_searchZipcd_onclick, this);

        };

        this.loadIncludeScript("AccountPop.xfdl", true);

       
    };
}
)();
