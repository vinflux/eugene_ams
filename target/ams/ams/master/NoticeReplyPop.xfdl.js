﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("noticeCreate");
                this.set_classname("notice");
                this.set_titletext("댓글 등록");
                this._setFormPosition(0,0,800,600);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"filepath\" type=\"STRING\" size=\"256\"/><Column id=\"INSERTURKEY\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEDATE\" type=\"STRING\" size=\"256\"/><Column id=\"NO_NO\" type=\"STRING\" size=\"16\"/><Column id=\"TITLE\" type=\"STRING\" size=\"32\"/><Column id=\"INSERTDATE\" type=\"STRING\" size=\"32\"/><Column id=\"CONTENT\" type=\"STRING\" size=\"32\"/><Column id=\"UPDATEURKEY\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row/></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static08", "absolute", "216", "86", null, "401", "30", null, this);
            obj.set_taborder("0");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "216", "54", null, "31", "30", null, this);
            obj.set_taborder("1");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("2");
            obj.set_text("Title");
            obj.set_cssclass("sta_WF_title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "21", "40", null, "2", "23", null, this);
            obj.set_taborder("3");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_P_close");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "404", "33", this);
            obj.set_taborder("5");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "20", "52", null, "1", "24", null, this);
            obj.set_taborder("6");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "21", null, null, "1", "23", "79", this);
            obj.set_taborder("7");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "347", "33", this);
            obj.set_taborder("8");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "487", null, "31", "30", null, this);
            obj.set_taborder("9");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "487", "194", "31", null, null, this);
            obj.set_taborder("10");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("static05", "absolute", "22", "54", "194", "31", null, null, this);
            obj.set_taborder("12");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static07", "absolute", "22", "86", "194", "401", null, null, this);
            obj.set_taborder("13");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_align("left middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_notice_title", "absolute", "222", "58", "548", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_maxlength("200");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_attachfile", "absolute", "-5", "616", "218", "22", null, null, this);
            obj.set_taborder("15");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("CLEAR", "absolute", "92.88%", "492", null, "19", "3.75%", null, this);
            obj.set_taborder("16");
            obj.style.set_image("URL('img::btn_delete_on.png')");
            this.addChild(obj.name, obj);

            obj = new FileUpload("FileUpload00", "absolute", "222", "491", null, "21", "63", null, this);
            obj.set_taborder("17");
            obj.getSetter("retry").set("0");
            obj.style.set_buttontext("BROWER");
            obj.set_index("0");
            this.addChild(obj.name, obj);

            obj = new TextArea("edt_notice_content", "absolute", "27.88%", "87", null, "396", "3.88%", null, this);
            obj.set_taborder("18");
            obj.set_maxlength("3000");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 800, 600, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("notice");
            		p.set_titletext("댓글 등록");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","NOTICE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ATTACH FILE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","static05","text","gds_lang","NOTICE_TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","static07","text","gds_lang","NOTICE_CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_notice_title","value","ds_header","TITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_attachfile","value","ds_header","filepath");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","FileUpload00","","filedataset","");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_notice_content","value","ds_header","CONTENT");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("NoticeReplyPop.xfdl", "lib::Comm.xjs");
        this.registerScript("NoticeReplyPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : boardPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 게시판 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ackey = "";
        this.gv_loggrpcd = "";
        this.gv_dupChk = false;
        this.gv_Pvalue = "";
        this.gv_upper_no_no = "";
        this.gv_reply = "";
        this.gv_status = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
                   
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            
            if(this.gv_flag == "U"){
        		this.fn_search();
        	}
        		else 
        			this.edt_notice_title.setFocus();
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "selectNoticeInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_FORM_DATA";// ds_param=OUT_PARAM";
            var sParam   = " NO_NO="+this.parent.no_no
        					+ " ctkey="+this.gfn_getUserInfo("ctKey")
        					;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "saveNotice");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT"
                         + " ctkey="+this.gfn_getUserInfo("ctKey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "boardController");
        	this.gfn_setCommon("METHODNM", "saveNotice");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE"
                         + " ctkey="+this.gfn_getUserInfo("ctKey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }
        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {	
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		trace(this.ds_header.saveXML());
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "TITLE|CONTENT";
        	var sComp = "edt_notice_title|edt_notice_content";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        					if(this.gv_flag == "H")	this.fn_Insert();
        					else if(this.gv_flag == "U") this.fn_Update();
        				}
        				else 
        					trace(this.ds_header.saveXML());
        			}
        		});
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        	
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        

        this.CLEAR_onclick = function(obj,e)
        {
        	this.FileUpload00.deleteItem(0);
        	this.FileUpload00.appendItem();
        	this.ds_header.setColumn(0,"file_url","");
        }

        
        this.FileUpload00_onitemchanged = function(obj,e)
        {
        	this.file = e.newvalue;
        	var sUrl = application.services["svc"].url+application.gv_ams + "/boardController/fileUpload.do";
        	var bSuccess = obj.upload(sUrl);
        	
        	trace(this.FileUpload00.value);
        	
        	this.edt_attachfile.set_value(this.FileUpload00.value);
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_attachfile.addEventHandler("onchanged", this.edt_referencekey_onchanged, this);
            this.CLEAR.addEventHandler("onclick", this.CLEAR_onclick, this);
            this.FileUpload00.addEventHandler("onitemchanged", this.FileUpload00_onitemchanged, this);

        };

        this.loadIncludeScript("NoticeReplyPop.xfdl", true);

       
    };
}
)();
