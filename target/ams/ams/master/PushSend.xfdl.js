﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("PushSend");
                this.set_classname("style01");
                this.set_titletext("푸시 발송");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"256\"/><Column id=\"pushhdkey\" type=\"STRING\" size=\"256\"/><Column id=\"title\" type=\"STRING\" size=\"256\"/><Column id=\"pushmsg\" type=\"STRING\" size=\"256\"/><Column id=\"pushstatus\" type=\"STRING\" size=\"256\"/><Column id=\"pushstatus_desc\" type=\"STRING\" size=\"256\"/><Column id=\"pushdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"pushdtkey\" type=\"STRING\" size=\"256\"/><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype\" type=\"STRING\" size=\"256\"/><Column id=\"eqtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"pushstatus\" type=\"STRING\" size=\"256\"/><Column id=\"pushstatus_desc\" type=\"STRING\" size=\"256\"/><Column id=\"pushhdkey\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_eqtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_push_detail", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excel", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_push_temp", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("1");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("13");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("15");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("16");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("17");
            obj.set_binddataset("ds_header");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"180\"/><Column size=\"180\"/><Column size=\"80\"/><Column size=\"145\"/><Column size=\"145\"/><Column size=\"100\"/><Column size=\"145\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"PUSHHDKEY\"/><Cell col=\"4\" text=\"TITLE\"/><Cell col=\"5\" text=\"PUSHMSG\"/><Cell col=\"6\" text=\"PUSHSTATUS\"/><Cell col=\"7\" text=\"PUSHDATE\"/><Cell col=\"8\" text=\"INSERTDATE\"/><Cell col=\"9\" text=\"INSERTURKEY\"/><Cell col=\"10\" text=\"UPDATEDATE\"/><Cell col=\"11\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell edittype=\"none\" style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:pushhdkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"4\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:title\" editlimit=\"100\" editdisplay=\"edit\"/><Cell col=\"5\" edittype=\"text\" text=\"bind:pushmsg\"/><Cell col=\"6\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:pushstatus_desc\" editlimit=\"100\" editdisplay=\"edit\"/><Cell col=\"7\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:pushdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"8\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"9\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"10\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"11\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("18");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_send", "absolute", null, "0", "82", "24", "111", null, this.div_splitTop);
            obj.set_taborder("19");
            obj.set_text("SEND");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static0", "absolute", "0%", "4", "353", "20", null, null, this.div_splitTop);
            obj.set_taborder("20");
            obj.set_text("푸시 발송 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("2");
            obj.style.set_background("transparent");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "0", "29", null, null, "0", "0", this.div_splitBottom);
            obj.set_taborder("16");
            obj.set_binddataset("ds_detail");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"150\"/><Column size=\"0\"/><Column size=\"0\"/><Column size=\"0\"/><Column size=\"145\"/><Column size=\"100\"/><Column size=\"145\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" text=\"NO\"/><Cell col=\"3\" text=\"PUSHDTKEY\"/><Cell col=\"4\" text=\"URKEY\"/><Cell col=\"5\" text=\"EQTYPE\"/><Cell col=\"6\" text=\"PUSHSTATUS\"/><Cell col=\"7\" text=\"PUSHHDKEY\"/><Cell col=\"8\" text=\"USSC_ACTIVE\"/><Cell col=\"9\" text=\"FR_DATE_VALUE\"/><Cell col=\"10\" text=\"TO_DATE_VALUE\"/><Cell col=\"11\" text=\"INSERTDATE\"/><Cell col=\"12\" text=\"INSERTURKEY\"/><Cell col=\"13\" text=\"UPDATEDATE\"/><Cell col=\"14\" text=\"UPDATEURKEY\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" expr=\"currow+1\"/><Cell col=\"3\" style=\"align:left;padding: ;\" text=\"bind:pushdtkey\"/><Cell col=\"4\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:urkey\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:right;padding: ;\" text=\"bind:eqtype_desc\" editlimit=\"10\" editdisplay=\"edit\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:pushstatus_desc\" editlimit=\"20\" editautoselect=\"false\" editdisplay=\"edit\" combodisplay=\"edit\"/><Cell col=\"7\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:pushhdkey\" editlimit=\"50\" editdisplay=\"edit\"/><Cell col=\"8\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:ussc_active_desc\" combodataset=\"ds_yesno\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"9\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:fr_date_value\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"10\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:to_date_value\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"11\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"12\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"13\" displaytype=\"date\" style=\"padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"14\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("17");
            obj.set_text("푸시 발송 상세 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("20");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_sendDtl", "absolute", null, "0", "82", "24", "39", null, this.div_splitBottom);
            obj.set_taborder("23");
            obj.set_text("SEND");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Static("Static00", "absolute", "11", "-349", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("24");
            obj.set_text("Detail");
            obj.set_cssclass("sta_WF_title");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("4");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("5");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("6");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("1");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 0, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("2");
            		p.style.set_background("transparent");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("푸시 발송");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.btn_send","tooltiptext","gds_lang","SEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","div_splitTop.btn_send","text","gds_lang","SEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","div_splitBottom.btn_sendDtl","tooltiptext","gds_lang","SEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","div_splitBottom.btn_sendDtl","text","gds_lang","SEND");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitBottom.Static06","text","gds_lang","ME00109471_02");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitBottom.Static00","text","gds_lang","ME00109471_02");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.Static0","text","gds_lang","ME00109471_01");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("PushSend.xfdl", "lib::Comm.xjs");
        this.registerScript("PushSend.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : PushSend.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : PUSH
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;

        this.gv_header = this.div_splitTop.grd_header;
        this.gv_detail = this.div_splitBottom.grd_detail;
        this.gv_page = this.div_splitTop.div_Paging;

        this.gv_nRow = 0;
        this.searchFalg = "";
        this.rowChg = "Y";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("EQTYPE", this.ds_eqtype);

        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정
        	
        	// 오름차순 정렬|내림차순 정렬|필터|필터제거|Lock|UnLock|컬럼 목록|초기화
        	this.gv_grdList = [this.gv_header, this.gv_detail]; // 그리드 오브젝트
        	var divPaging = [this.gv_page]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc);

        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {	
        	this.gfn_getScroll(this.gv_header); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "selectPushInfo");
        	
        	var sSvcId   = "select";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
        	var sParam   = "";
        	
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.rowChg = "Y";
        				this.gv_header.set_nodatatext("");
        				this.ds_header.clearData();
        				this.ds_detail.clearData();
        				
        				this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.rowChg = "Y";
        		this.gv_header.set_nodatatext("");
        		this.ds_header.clearData();
        		this.ds_detail.clearData();
        		
        		this.gfn_grdSortSet(this.gv_header); // sort 조회 셋팅
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchDetail = function()
        {
        	this.gv_nRow = this.ds_header.rowposition;
        	
        	this.gv_detail.set_nodatatext("");
        	this.ds_detail.clearData();
        	
        	this.gfn_getScroll(this.gv_detail); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "selectDetailPushInfo");
        	
        	var sSvcId   = "selectDetail";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "";
        	var sOutData = "ds_detail=OUT_rtnGrid";
        	var sParam   = "pushhdkey="+this.ds_header.getColumn(this.ds_header.rowposition, "pushhdkey");
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack", false);
        }

        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "savePushInfo");
        	
            var sSvcId   = "save";
            var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
            var sInData  = "IN_DATA_LIST=ds_header:U";
            var sOutData = "";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_sendDtl = function(nHdRow,nRow){
        	//ds_push_detail
        	this.ds_push_detail.clearData();
        	this.gfn_grdCheckCopyToDataset(this.div_splitBottom.grd_detail, this.ds_detail, this.ds_push_detail);
        	
        	this.ds_push_temp.clear();
        	this.ds_push_temp.copyData(this.ds_header);
        	this.ds_push_temp.clearData();

        	this.ds_push_temp.addRow();
        	this.ds_push_temp.copyRow(0, this.ds_header, nHdRow);
        	this.ds_push_temp.addColumn("title", "STRING", 256);
        	this.ds_push_temp.addColumn("pushmsg", "STRING", 256);
        	
        	///pushController/sendPush.do"
        	var sInData  = "IN_DATA_LIST=ds_push_detail IN_PUSH_CON=ds_push_temp";
            var sSvcId   = "sendPush";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sOutData = "";
            var sParam   = "pushhdkey="+this.ds_detail.getColumn(nRow, "pushhdkey")
        				 + " workType=UPDATE"
        				 + " title="+this.ds_header.getColumn(nHdRow,"title")
        				 + " pushmsg="+this.ds_header.getColumn(nHdRow,"pushmsg");

        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "sendPush");	
        	
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_deleteDtl = function(nRow){
        	//ds_push_detail
        	this.ds_push_detail.clearData();
        	this.gfn_grdCheckCopyToDataset(this.div_splitBottom.grd_detail, this.ds_detail, this.ds_push_detail);
        	
        	var sInData  = "IN_DATA_LIST=ds_push_detail";
            var sSvcId   = "deleteDtl";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sOutData = "";
            var sParam   = "pushhdkey="+this.ds_detail.getColumn(nRow, "pushhdkey");

        	this.gfn_setCommon("BEANID", "pushController");
        	this.gfn_setCommon("METHODNM", "deletePushDtl");	
        	
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		if(this.ds_header.rowcount > 0){
        			this.ds_header.set_rowposition(0);
        			this.searchFalg = "save";
        			this.fn_searchDetail();
        			this.parent.fn_searchAfter(); // 검색조건 숨김
        		}else{
        			this.searchFalg = "";
        			this.gv_header.set_nodatatext(application.gv_nodataMsg);
        		}
        		
        		this.gfn_constDsSet(this.gv_header);
        		
        		this.gv_page.fn_pageSet(this.ds_param.getColumn(0, "pagingLimit"), this.ds_param.getColumn(0, "currentPage"), this.ds_param.getColumn(0, "COUNT"));
        	}else if(sSvcId == "selectDetail"){
        		if(this.ds_detail.rowcount == 0){
        			this.gv_detail.set_nodatatext(application.gv_nodataMsg);
        		}
        		this.gfn_constDsSet(this.gv_detail);
        		this.searchFalg = "";
        	}else if(sSvcId == "save"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "saveDetail"){
        		this.searchFalg = "save";
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_searchDetail();
        	} else if (sSvcId == "sendPush" || sSvcId == "deleteDtl"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.fn_search();
        	} else if (sSvcId == "exceldown"){
        		this.ds_excel.clearData;
        		this.gfn_excelDownload(this.ds_excel);
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/

        /* div_splitTop_grd_header_oncelldblclick 실행 */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_grd_header_onheadclick 실행 */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {

        	this.gfn_grdHeaderClick(obj, e, this);

        }

        
        /* div_splitTop_btn_delete_onclick 실행 */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{	
        		while(nRow != -1){
        			if(this.ds_header.getRowType(nRow) == "2"){
        				this.ds_header.deleteRow(nRow);
        				nRow = this.ds_header.findRowExpr("CHK == '1'");
        			}else{
        				this.ds_header.setColumn(nRow, "STATUS", "D");
        				nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        			}
        		}
        		if(this.ds_header.findRowExpr("STATUS == 'D'") > -1 ) this.div_splitTop_btn_save_onclick ()
        	}
        }

        /* div_splitTop_btn_add_onclick 실행 */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	this.ds_detail.clearData();
        	
        	var nRow = this.ds_header.addRow();
        	this.ds_header.set_rowposition(nRow);
        	this.ds_header.setColumn(nRow, "STATUS", "C");
        	this.gv_header.setCellPos(this.gv_header.getBindCellIndex("body", "pushhdkey"));
        	this.gv_header.setFocus();
        }

        
        /* div_splitBottom_btn_delete_onclick 실행 */
        this.div_splitBottom_btn_delete_onclick = function(obj,e)
        {
        	var dtRow = this.ds_detail.findRow("CHK", "1");
        	if(dtRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	} else {
        		this.gfn_confirm("MSG_ALERT_DELETE", "", function(msg, flag){
        				if(flag){
        					this.fn_deleteDtl(dtRow);
        				}
        			});
        	}
        }

        

        /* ds_header_onrowposchanged 실행 */
        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(this.rowChg == "" && this.gv_nRow != e.newrow){
        		if(nRow != -1){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(flag){
        					this.fn_searchDetail();
        				}else{
        					this.rowChg = "Y";
        					obj.set_rowposition(e.oldrow);
        				}
        			});
        		}else{
        			this.fn_searchDetail();
        		}
        	}
        	
        	this.rowChg = "";
        }

        /* ds_header_cancolumnchange 실행 */
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	var nRow = this.ds_detail.findRowExpr("dataset.getColumn(currow, 'STATUS') == 'C' || dataset.getColumn(currow, 'STATUS') == 'U' || dataset.getColumn(currow, 'STATUS') == 'D'");
        	
        	if(e.columnid != "CHK"){
        		if(nRow != -1 && obj.getRowType(e.row) != 2){
        			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        				if(!flag){
        					obj.setColumn(e.row, e.columnid, e.oldvalue);
        				}
        			});
        		}
        		
        		this.gfn_statusChk(obj, e.row);
        	}else{
        		obj.set_updatecontrol(true);
        	}
        }

        
        /* ds_detail_oncolumnchanged 실행 */
        this.ds_detail_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(true);
        	}else{
        		this.gfn_statusChk(obj, e.row);
        	}
        }

        /* div_splitBottom_grd_detail_oncelldblclick 실행 */
        this.div_splitBottom_grd_detail_oncelldblclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	
        	if(colName == "CHK"){
        		this.gfn_grdDoubleClick(obj);
        	}
        }

        /* div_splitTop_btn_save_onclick 실행 */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "title|pushmsg";
        	
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(this.gfn_isDataNullCheck(this.gv_header, this.ds_header, dsCol)){
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        						this.fn_save();
        				} else {
        				this.ds_header.reset();
        				}
        			});
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        /* 헤더 발송버튼 클릭 */
        this.div_splitTop_btn_send_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header) && this.searchFalg == ""){
        		this.gfn_alert("MSG_ALERT_CANNOT_SEND_BEFORE_SAVE");
        		return;
        	}else{
        		var oArg = "";
        				
        		if(this.gfn_grdSingleRowCheck(this.div_splitTop.grd_header)){
        			var nRow = this.ds_header.findRow("CHK", "1");
        			oArg = { menuId:this.parent.gv_menuId
        					,pushhdkey:this.ds_header.getColumn(nRow, "pushhdkey")
        					,title:this.ds_header.getColumn(nRow, "title")
        					,pushmsg:this.ds_header.getColumn(nRow, "pushmsg")
        				   };
        			this.gfn_popup("PushSendPop", "master::PushSendPop.xfdl", oArg, 700, 750, "");
        		}	
        	}

        }

        /* 메시지 재전송 */
        this.div_splitBottom_btn_sendDtl_onclick = function(obj,e)
        {	
        // 	if(this.gfn_isUpdate(this.ds_detail) && this.searchFalg == ""){
        // 		this.gfn_alert("MSG_ALERT_CANNOT_SEND_BEFORE_SAVE");
        // 		return;
        // 	}else{
        // 		var hdRow = this.ds_header.findRow("CHK", "1");
        // 		var dtRow = this.ds_detail.findRow("CHK", "1");
        // 		if(hdRow == -1 || dtRow == -1){
        // 			this.gfn_alert("MSG_CHKDATA_NOSELECT");
        // 		} else {
        // 			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        // 					if(flag){
        // 						this.fn_sendDtl(hdRow,dtRow);
        // 					}
        // 				});
        // 		}
        // 	}

        
        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{	
        		while(nRow != -1){
        			trace("nRow: " + nRow);
        			if(this.gfn_isUpdate(this.ds_detail)){
        				var nPushStatus = this.ds_detail.getColumn(nRow, "pushstatus");
        				if(nPushStatus == "90" || nPushStatus == "91"){
        					this.gfn_alert("ALREADY_PUSHED");
        					return;
        				}
        			}
        		
        		
        // 			if(this.ds_header.getRowType(nRow) == "2"){
        // 				this.ds_header.deleteRow(nRow);
        // 				nRow = this.ds_header.findRowExpr("CHK == '1'");
        // 			}else{
        // 				this.ds_header.setColumn(nRow, "STATUS", "D");
        // 				nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        // 			}
        		}
        	}
        }

        
        /* 디테일 sort */
        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	var colName = this.gfn_gridBindName(obj, e.cell);
        	var sortValue = this.ds_param.getColumn(0, "sortValue");
        	var sColSpan= obj.getCellProperty("head", e.cell, "colspan");

        	if (colName != "CHK" && colName != "NO"  && sColSpan == "1" && this.ds_header.rowcount > 0) {
        		this.gfn_sortSearch(obj, e.cell, colName, sortValue);
        		var sObhdkey = this.ds_detail.getColumn(0, "pushhdkey");
        		var sObhdtype = null;
        		var sCtKey = this.ds_detail.getColumn(0, "ctkey");
        		this.fn_searchDetail(sCtKey, sObhdkey, sObhdtype);	
        	} else if (colName == "CHK") {
        		this.gfn_gridProc(obj, e);
        	}
        }
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_detail.addEventHandler("cancolumnchange", this.ds_detail_cancolumnchange, this);
            this.ds_detail.addEventHandler("oncolumnchanged", this.ds_detail_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitTop.btn_send.addEventHandler("onclick", this.div_splitTop_btn_send_onclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncelldblclick", this.div_splitBottom_grd_detail_oncelldblclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.btn_delete.addEventHandler("onclick", this.div_splitBottom_btn_delete_onclick, this);
            this.div_splitBottom.btn_sendDtl.addEventHandler("onclick", this.div_splitBottom_btn_sendDtl_onclick, this);

        };

        this.loadIncludeScript("PushSend.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
