﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CtgPop");
                this.set_classname("style01");
                this.set_titletext("상품 대/중/소 구분 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,311);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"owkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctgkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctglevel\" type=\"STRING\" size=\"256\"/><Column id=\"ctgname\" type=\"STRING\" size=\"256\"/><Column id=\"upperctgkey\" type=\"STRING\" size=\"256\"/><Column id=\"urkey\" type=\"STRING\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"owkey\"/><Col id=\"ctgkey\"/><Col id=\"ctgname\"/><Col id=\"upperctgkey\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/><Col id=\"updatedate\"/><Col id=\"inserturkey\"/><Col id=\"urkey\"/><Col id=\"ctglevel\"/><Col id=\"delyn\"/><Col id=\"closingdate\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_ctgLevel", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_code", this);
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("14");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("12");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctgkey", "absolute", "222", "93", "257", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("13");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchOwkey", "absolute", "334", "62", "24", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "222", "62", "110", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "216", "213", "342", "31", null, null, this);
            obj.set_taborder("241");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "213", "194", "31", null, null, this);
            obj.set_taborder("243");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "93", "57", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_ctgLevel", "absolute", "222", "124", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_innerdataset("@ds_ctgLevel");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("249");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctgname", "absolute", "222", "155", "321", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchUpperctgkey", "absolute", "334", "186", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_upperctgkey", "absolute", "222", "186", "110", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urkey", "absolute", "222", "217", "110", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchUrkey", "absolute", "334", "217", "24", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_upperctgname", "absolute", "360", "186", "183", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_urname", "absolute", "360", "217", "183", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "360", "62", "183", "22", null, null, this);
            obj.set_taborder("251");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 311, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("상품 대/중/소 구분 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item1","edt_ctgkey","value","ds_header","ctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","CTGLEVEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","CTGKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","CTGLEVEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","CTGNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","static4","text","gds_lang","MD_EMP_NO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cbo_ctgLevel","value","ds_header","ctglevel");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static06","text","gds_lang","UPPERCTGKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_ctgname","value","ds_header","ctgname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_upperctgkey","value","ds_header","upperctgkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_urkey","value","ds_header","urkey");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CtgPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CtgPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CtgPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 상품 대/중/소 구분 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_ctgkey = "";
        this.gv_ctglevel = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";
        this.gv_ctgLevel="";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("CTGLEVEL", this.ds_ctgLevel, "");
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);
            this.gv_ctgkey   = this.gfn_isNullEmpty(this.parent.ctgkey);
            this.gv_ctglevel = this.gfn_isNullEmpty(this.parent.ctglevel);
            
        	var owkey = this.gfn_getUserInfo("owkeym").split(",");
        	this.ds_header.setColumn(0, "owkey", owkey[0]);
            
            this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.btn_searchOwkey.set_enable(false);
        		this.edt_ctgkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.cbo_ctgLevel.set_enable(false);
        		this.fn_search();
        	}else{
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "ctgController");
        	this.gfn_setCommon("METHODNM", "selectCtg");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         + " ctgkey="+this.gv_ctgkey
                         + " ctglevel="+this.gv_ctglevel
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchCode = function()
        {
        	this.ds_code.clearData();
        	
        	this.gfn_setCommon("BEANID"  , "ctgController");
        	this.gfn_setCommon("METHODNM", "searchCtgInfo");
        	
            var sSvcId   = "selectCode";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_code=OUT_rtnGrid";
            var sParam   = "owkey="+this.edt_owkey.value
                         + " opval=eq"
                         ;
        	
        	if(this.gfn_isNotNull(this.cbo_ctgLevel.value)) {
        		if(this.cbo_ctgLevel.value == "1"){
        			sParam += " searchLevel="+this.cbo_ctgLevel.value;
        		}else if(this.cbo_ctgLevel.value != "1"){
        			sParam += " searchLevel="+this.gv_ctgLevel;
        		}
        		
        	}
            if(this.gfn_isNotNull(this.edt_upperctgkey.value)) sParam += " searchValue="+this.edt_upperctgkey.value;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "ctgController");
        	this.gfn_setCommon("METHODNM", "searchCtgInfo");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "ctgkey="+this.edt_ctgkey.value;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "ctgController");
        	this.gfn_setCommon("METHODNM", "saveCtg");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "ctgController");
        	this.gfn_setCommon("METHODNM", "saveCtg");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}else{
        			if(this.cbo_ctgLevel.value == "1"){
        				this.edt_upperctgkey.set_value("DIR");
        				this.edt_upperctgkey.set_enable(false);
        				this.btn_searchUpperctgkey.set_enable(false);
        			}
        		}
        		
        		var key = "";
        		var value = "";
        		
        		if(this.gfn_isNotNull(this.edt_owkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        		}
        		
        		if(this.edt_upperctgkey.value != "DIR" && this.gfn_isNotNull(this.edt_upperctgkey.value) && this.gfn_isNotNull(this.ds_header.getColumn(0, "ctglevel"))){
        			this.gv_code = "Y";
        			this.fn_searchCode();
        		}
        		
        		key = "";
        		value = "";
        		
        		if(this.gfn_isNotNull(this.edt_urkey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_urkey.value, "80", application.gv_ams, "", "", this.edt_urkey, this.edt_urname);
        		}
        	}else if(sSvcId == "selectCode"){
        		if(this.ds_code.rowcount > 0){
        			this.edt_upperctgname.set_value(this.ds_code.getColumn(0, "ctgname"));
        			this.edt_upperctgname.set_tooltiptext(this.ds_code.getColumn(0, "ctgname"));
        		}else{
        			if(this.gv_code == ""){
        				this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        					this.edt_upperctgname.set_value("");
        					this.edt_upperctgkey.setFocus();
        				});
        			}else{
        				this.edt_upperctgname.set_value("");
        				this.edt_upperctgkey.setFocus();
        			}
        		}
        		
        		this.gv_code = "";
        	}else if(sSvcId == "check"){
        		//if(this.ds_param.getColumn(0, "isDuplicate")){
        		if(this.ds_param.getColumn(0, "COUNT") != 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_ctgkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|ctgkey|ctglevel|upperctgkey";
        	var sComp = "edt_owkey|edt_ctgkey|cbo_ctgLevel|edt_upperctgkey";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_ctgkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_owname.value) && !this.gfn_isNull(this.edt_owkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_upperctgname.value) && !this.gfn_isNull(this.edt_upperctgkey.value) && this.edt_upperctgkey.value != "DIR"){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_upperctgkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_urname.value) && !this.gfn_isNull(this.edt_urkey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_urkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "H")	this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "ctgkey";
        	var sComp = "edt_ctgkey";
        	    
           
            	            
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* btn_searchOwkey_onclick 실행 */
        this.btn_searchOwkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:50
        				,putObj:this.edt_owkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchUpperctgkey_onclick 실행 */
        this.btn_searchUpperctgkey_onclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.ds_header.getColumn(0, "ctglevel"))){
        		this.gfn_alert("상품 대/중/소 구분을 선택해주세요.");
        		return;
        	}
        	this.gv_Pvalue = "";
        	
        	var oArg = { searchLevel:this.gv_ctgLevel
        				,putObj:this.edt_upperctgkey
        	            ,owkey:this.edt_owkey.value
        			   };
        	this.gfn_popup("CtgKeySearchPop", "master::CtgKeySearchPop.xfdl", oArg, 700, 497, "");
        }

        /* btn_searchUrkey_onclick 실행 */
        this.btn_searchUrkey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:80
        				,putObj:this.edt_urkey
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey" || e.columnid == "ctgkey"){
        		this.gv_dupChk = false;
        		
        		if(e.columnid == "owkey"){
        			this.edt_owname.set_value(this.gv_Pvalue[1]);
        			this.edt_owname.set_tooltiptext(this.gv_Pvalue[1]);
        		}
        	}else if(e.columnid == "upperctgkey"){
        		this.edt_upperctgname.set_value(this.gv_Pvalue[1]);
        		this.edt_upperctgname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "urkey"){
        		this.edt_urname.set_value(this.gv_Pvalue[1]);
        		this.edt_urname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        	
        	this.gv_Pvalue = "";
        }

        /* edt_owkey_onchanged 실행 */
        this.edt_owkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_owkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_owkey.value, "50", application.gv_ams, "", "", this.edt_owkey, this.edt_owname);
        	}
        }

        /* edt_urkey_onchanged 실행 */
        this.edt_urkey_onchanged = function(obj,e)
        {
        	var key = "";
        	var value = "";
        	
        	if(this.gfn_isNotNull(this.edt_urkey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_urkey.value, "80", application.gv_ams, "", "", this.edt_urkey, this.edt_urname);
        	}
        }

        /* edt_upperctgkey_onchanged 실행 */
        this.edt_upperctgkey_onchanged = function(obj,e)
        {
        	if(this.edt_upperctgkey.value != "DIR" && this.gfn_isNotNull(this.edt_upperctgkey.value)){
        		if(this.gfn_isNull(this.ds_header.getColumn(0, "ctglevel"))){
        			this.gfn_alert("상품 대/중/소 구분을 선택해주세요.");
        			return;
        		}
        		
        		this.gv_code = "";
        		this.fn_searchCode();
        	}
        }

        /* cbo_ctgLevel_onitemchanged 실행 */
        this.cbo_ctgLevel_onitemchanged = function(obj,e)
        {
        	if(e.postvalue == "1"){
        		this.edt_upperctgkey.set_value("DIR");
        		this.edt_upperctgkey.set_enable(false);
        		this.btn_searchUpperctgkey.set_enable(false);
        		
        		
        	}else if(e.postvalue == "2"){
        		this.gv_ctgLevel = "1";
        		this.edt_upperctgkey.set_value("");
        		this.edt_upperctgkey.set_enable(true);
        		this.btn_searchUpperctgkey.set_enable(true);

        		if(this.gfn_isNotNull(this.edt_upperctgkey.value)){
        			this.gv_code = "";
        			this.fn_searchCode();
        		}
        	}else if(e.postvalue == "3"){
        		this.gv_ctgLevel = "2";
        		this.edt_upperctgkey.set_value("");
        		this.edt_upperctgkey.set_enable(true);
        		this.btn_searchUpperctgkey.set_enable(true);
        		
        		if(this.gfn_isNotNull(this.edt_upperctgkey.value)){
        			this.gv_code = "";
        			this.fn_searchCode();
        		}
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_searchOwkey.addEventHandler("onclick", this.btn_searchOwkey_onclick, this);
            this.edt_owkey.addEventHandler("onchanged", this.edt_owkey_onchanged, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.cbo_ctgLevel.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.btn_searchUpperctgkey.addEventHandler("onclick", this.btn_searchUpperctgkey_onclick, this);
            this.edt_upperctgkey.addEventHandler("onchanged", this.edt_upperctgkey_onchanged, this);
            this.edt_urkey.addEventHandler("onchanged", this.edt_urkey_onchanged, this);
            this.btn_searchUrkey.addEventHandler("onclick", this.btn_searchUrkey_onclick, this);

        };

        this.loadIncludeScript("CtgPop.xfdl", true);

       
    };
}
)();
