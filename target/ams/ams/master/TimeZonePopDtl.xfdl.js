﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("TimeZonePopDtl");
                this.set_classname("style01");
                this.set_titletext("표준 시간대 상세 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,280);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"tzkey\" type=\"STRING\" size=\"256\"/><Column id=\"continentcode\" type=\"STRING\" size=\"256\"/><Column id=\"countrycode\" type=\"STRING\" size=\"256\"/><Column id=\"citycode\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"displaymessage\"/><Col id=\"citycode\"/><Col id=\"countrycode\"/><Col id=\"continentcode\"/><Col id=\"tzkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("12");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("10");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_continentcode", "absolute", "222", "93", "110", "22", null, null, this);
            obj.set_taborder("1");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tzkey", "absolute", "222", "62", "321", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("11");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_citycode", "absolute", "222", "155", "110", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCountrycode", "absolute", "334", "124", "24", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_countrycode", "absolute", "222", "124", "110", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("235");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static4", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("237");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_countryname", "absolute", "360", "124", "183", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCitycode", "absolute", "334", "155", "24", "22", null, null, this);
            obj.set_taborder("245");
            obj.set_cssclass("btn_WF_srh_s");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cityname", "absolute", "360", "155", "183", "22", null, null, this);
            obj.set_taborder("246");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchContinentcode", "absolute", "334", "93", "24", "22", null, null, this);
            obj.set_taborder("247");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_continentname", "absolute", "360", "93", "183", "22", null, null, this);
            obj.set_taborder("248");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_displaymessage", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("249");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 280, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("표준 시간대 상세 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_continentcode","value","ds_detail","continentcode");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_tzkey","value","ds_detail","tzkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","TIMEZONE DETAIL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_citycode","value","ds_detail","citycode");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","edt_countrycode","value","ds_detail","countrycode");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static","text","gds_lang","TZKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static1","text","gds_lang","CONTINENTCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static2","text","gds_lang","COUNTRYCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","CITYCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","static4","text","gds_lang","DISPLAYMESSAGE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","edt_displaymessage","value","ds_detail","displaymessage");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("TimeZonePopDtl.xfdl", "lib::Comm.xjs");
        this.registerScript("TimeZonePopDtl.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : TimeZonePopDtl.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 표준 시간대 상세 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_tzkey = "";

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_tzkey  = this.gfn_isNullEmpty(this.parent.tzkey);
            
        	this.ds_detail.setColumn(0, "tzkey", this.gv_tzkey);
        	this.ds_detail.applyChange();
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "timeZoneController");
        	this.gfn_setCommon("METHODNM", "insertTimezoneInfoDtl");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_TZDATA=ds_detail";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_work.fn_searchDetail();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_detail;
        	var dsCol = "continentcode|countrycode|citycode";
        	var sComp = "edt_continentcode|edt_countrycode|edt_citycode";
        	
        	if(this.gfn_isNull(this.edt_continentname.value) && !this.gfn_isNull(this.edt_continentcode.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_continentcode.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_countryname.value) && !this.gfn_isNull(this.edt_countrycode.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_countrycode.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_cityname.value) && !this.gfn_isNull(this.edt_citycode.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_citycode.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_detail)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							this.fn_Insert();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_10001");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_detail)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* edt_continentcode_onchanged 실행 */
        this.edt_continentcode_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "CONTINENTCODE";
        	
        	if(this.gfn_isNotNull(this.edt_continentcode.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_continentcode.value, "20", application.gv_ams, "", "", this.edt_continentcode, this.edt_continentname);
        	}
        }

        /* btn_searchContinentcode_onclick 실행 */
        this.btn_searchContinentcode_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_continentcode
        				,putKey:"adcd_hdkey"
        				,putValue:"CONTINENTCODE"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* edt_countrycode_onchanged 실행 */
        this.edt_countrycode_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey|value";
        	var value = "COUNTRYCODE|"+this.edt_continentcode.value;
        	
        	if(this.gfn_isNotNull(this.edt_countrycode.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_countrycode.value, "20", application.gv_ams, "", "", this.edt_countrycode, this.edt_countryname);
        	}
        }

        /* btn_searchCountrycode_onclick 실행 */
        this.btn_searchCountrycode_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_countrycode
        				,putKey:"adcd_hdkey|value"
        				,putValue:"COUNTRYCODE|"+this.edt_continentcode.value
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* edt_citycode_onchanged 실행 */
        this.edt_citycode_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey|value";
        	var value = "CITYCODE|"+this.edt_countrycode.value;
        	
        	if(this.gfn_isNotNull(this.edt_citycode.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_citycode.value, "20", application.gv_ams, "", "", this.edt_citycode, this.edt_cityname);
        	}
        }

        /* btn_searchCitycode_onclick 실행 */
        this.btn_searchCitycode_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_citycode
        				,putKey:"adcd_hdkey|value"
        				,putValue:"CITYCODE|"+this.edt_countrycode.value
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* ds_detail_oncolumnchanged 실행 */
        this.ds_detail_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "continentcode"){
        		this.edt_continentname.set_value(this.gv_Pvalue[1]);
        		this.edt_continentname.set_tooltiptext(this.gv_Pvalue[1]);
        		
        		if(e.newvalue != ""){
        			this.edt_countrycode.set_enable(true);
        			this.btn_searchCountrycode.set_enable(true);
        		}else{
        			this.edt_countrycode.set_enable(false);
        			this.btn_searchCountrycode.set_enable(false);
        			this.edt_citycode.set_enable(false);
        			this.btn_searchCitycode.set_enable(false);
        		}
        		
        		this.edt_countrycode.set_value("");
        		this.edt_countryname.set_value("");
        		this.edt_countryname.set_tooltiptext("");
        		this.edt_citycode.set_value("");
        		this.edt_cityname.set_value("");
        		this.edt_cityname.set_tooltiptext("");
        	}else if(e.columnid == "countrycode"){
        		this.edt_countryname.set_value(this.gv_Pvalue[1]);
        		this.edt_countryname.set_tooltiptext(this.gv_Pvalue[1]);
        		
        		if(e.newvalue != ""){
        			this.edt_citycode.set_enable(true);
        			this.btn_searchCitycode.set_enable(true);
        		}else{
        			this.edt_citycode.set_enable(false);
        			this.btn_searchCitycode.set_enable(false);
        		}
        		
        		this.edt_citycode.set_value("");
        		this.edt_cityname.set_value("");
        		this.edt_cityname.set_tooltiptext("");
        	}else if(e.columnid == "citycode"){
        		this.edt_cityname.set_value(this.gv_Pvalue[1]);
        		this.edt_cityname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        	
        	this.gv_Pvalue = "";
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_detail.addEventHandler("oncolumnchanged", this.ds_detail_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_continentcode.addEventHandler("onchanged", this.edt_continentcode_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_citycode.addEventHandler("onchanged", this.edt_citycode_onchanged, this);
            this.btn_searchCountrycode.addEventHandler("onclick", this.btn_searchCountrycode_onclick, this);
            this.edt_countrycode.addEventHandler("onchanged", this.edt_countrycode_onchanged, this);
            this.btn_searchCitycode.addEventHandler("onclick", this.btn_searchCitycode_onclick, this);
            this.btn_searchContinentcode.addEventHandler("onclick", this.btn_searchContinentcode_onclick, this);

        };

        this.loadIncludeScript("TimeZonePopDtl.xfdl", true);

       
    };
}
)();
