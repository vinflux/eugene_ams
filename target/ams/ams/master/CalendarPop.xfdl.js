﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CalendarPop");
                this.set_classname("style01");
                this.set_titletext("캘린더 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,280);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"daytitle\" type=\"STRING\" size=\"256\"/><Column id=\"cfulldate\" type=\"STRING\" size=\"256\"/><Column id=\"isbasicholiday\" type=\"STRING\" size=\"256\"/><Column id=\"cyear\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"isextraholiday\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"dayweek\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"cdate\" type=\"STRING\" size=\"256\"/><Column id=\"cmonth\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"insertdate\"/><Col id=\"daytitle\"/><Col id=\"cfulldate\"/><Col id=\"isbasicholiday\"/><Col id=\"cyear\"/><Col id=\"inserturkey\"/><Col id=\"isextraholiday\"/><Col id=\"updateurkey\"/><Col id=\"dayweek\"/><Col id=\"updatedate\"/><Col id=\"cdate\"/><Col id=\"cmonth\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_dayweek", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("Title");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("8");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("6");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("static2", "absolute", "22", "120", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "120", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("7");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("static1", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "216", "151", "342", "31", null, null, this);
            obj.set_taborder("228");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static3", "absolute", "22", "151", "194", "31", null, null, this);
            obj.set_taborder("230");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("static", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("231");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("232");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "62", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_dayweek", "absolute", "222", "93", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("2");
            obj.set_innerdataset("ds_dayweek");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");
            obj.set_enable("false");

            obj = new Static("Static05", "absolute", "216", "182", "342", "31", null, null, this);
            obj.set_taborder("249");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("static06", "absolute", "22", "182", "194", "31", null, null, this);
            obj.set_taborder("250");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_daytitle", "absolute", "222", "186", "321", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_maxlength("50");
            this.addChild(obj.name, obj);

            obj = new Calendar("cal_cfulldate", "absolute", "222", "62", "257", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("0");
            obj.set_dateformat("yyyy-MM-dd");
            obj.set_editformat("yyyy-MM-dd");
            obj.set_value("null");

            obj = new Combo("cbo_isbasicholiday", "absolute", "222", "124", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("3");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Combo("cbo_isextraholiday", "absolute", "222", "155", "321", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 280, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("캘린더 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item2","lab_title","text","gds_lang","CALENDAR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","static1","text","gds_lang","DAYWEEK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","static2","text","gds_lang","ISBASICHOLIDAY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","static","text","gds_lang","CFULLDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","static3","text","gds_lang","ISEXTRAHOLIDAY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","cbo_dayweek","value","ds_header","dayweek");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","static06","text","gds_lang","DAYTITLE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","edt_daytitle","value","ds_header","daytitle");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","cal_cfulldate","value","ds_header","cfulldate");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","cbo_isbasicholiday","value","ds_header","isbasicholiday");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","cbo_isextraholiday","value","ds_header","isextraholiday");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CalendarPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CalendarPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CalendarPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 캘린더 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_cfulldate = "";
        this.gv_dupChk = false;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("WEEKDAY", this.ds_dayweek, "");
        //	trace(this.ds_dayweek.saveXML());
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	
            this.gv_flag   = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_cfulldate  = this.gfn_isNullEmpty(this.parent.cfulldate);
            
            this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.cal_cfulldate.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "calendarController");
        	this.gfn_setCommon("METHODNM", "selectCalendar");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnMap ds_param=OUT_PARAM";
            var sParam   =  "cfulldate="+this.gv_cfulldate
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "calendarController");
        	this.gfn_setCommon("METHODNM", "selectCalendar");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnMap";
            var sParam   = "cfulldate="+this.ds_header.getColumn(0, "cfulldate")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "calendarController");
        	this.gfn_setCommon("METHODNM", "saveCalendar");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "calendarController");
        	this.gfn_setCommon("METHODNM", "saveCalendar");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.cal_cfulldate.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "cfulldate|isbasicholiday";
        	var sComp = "cal_cfulldate|cbo_isbasicholiday";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.cal_cfulldate.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        							if(this.gv_flag == "H")	this.fn_Insert();
        							else if(this.gv_flag == "U") this.fn_Update();
        						}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "cfulldate";
        	var sComp = "cal_cfulldate";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "cfulldate"){
        		this.gv_dupChk = false;
        	}
        }

        /* cal_cfulldate_onchanged 실행 */
        this.cal_cfulldate_onchanged = function(obj,e)
        {
        	if(e.postvalue == ""){
        		this.cbo_dayweek.set_value("");
        	}else{
        		var dayOfWeek = this.gfn_getDay(""+e.postvalue);
        		var convertData = "";
        		
        		if(dayOfWeek == "0") convertData = "SUN";
        		else if(dayOfWeek == "1") convertData = "MON";
        		else if(dayOfWeek == "2") convertData = "TUE";
        		else if(dayOfWeek == "3") convertData = "WED";
        		else if(dayOfWeek == "4") convertData = "THU";
        		else if(dayOfWeek == "5") convertData = "FRI";
        		else if(dayOfWeek == "6") convertData = "SAT";
        		
        		this.ds_header.setColumn(0, "dayweek", convertData);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.cbo_dayweek.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.cal_cfulldate.addEventHandler("onchanged", this.cal_cfulldate_onchanged, this);
            this.cbo_isbasicholiday.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);
            this.cbo_isextraholiday.addEventHandler("onitemchanged", this.cbo_ctgLevel_onitemchanged, this);

        };

        this.loadIncludeScript("CalendarPop.xfdl", true);

       
    };
}
)();
