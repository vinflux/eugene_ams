﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Message");
                this.set_classname("style01");
                this.set_titletext("메시지");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage_abbr\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"STATUS\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"seq\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">adcd_hdkey</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 코드</Col></Row><Row><Col id=\"DATA_FILED\">adcd_hdname</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_MessageCenter", this);
            obj._setContents("<ColumnInfo><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_apkey", this);
            obj._setContents("<ColumnInfo><Column id=\"appKey\" type=\"STRING\" size=\"256\"/><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_lakey", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_combo_msgtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage_abbr\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"workType\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_detail_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"256\"/><Column id=\"seq\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"id\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_returnValue", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header_check", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"CHK\" type=\"STRING\" size=\"256\"/><Column id=\"PAGING_NUM\" type=\"STRING\" size=\"256\"/><Column id=\"mulaapmsg_hdkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype_desc\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"256\"/><Column id=\"apkey\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage\" type=\"STRING\" size=\"256\"/><Column id=\"displaymessage_abbr\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"256\"/><Column id=\"insertdate\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"256\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"256\"/><Column id=\"msgtype\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Button("btn_work_split_h", "absolute", "0", null, null, "8", "0", "266", this);
            obj.set_taborder("45");
            obj.set_cssclass("btn_WF_SHide");
            obj.style.set_cursor("n-resize");
            this.addChild(obj.name, obj);

            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "274", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "37", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_delete", "absolute", null, "0", "33", "24", "111", null, this.div_splitTop);
            obj.set_taborder("1");
            obj.set_cssclass("btn_WF_delete");
            obj.set_tooltiptext("삭제");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "74", null, this.div_splitTop);
            obj.set_taborder("2");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("수정");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_add", "absolute", null, "0", "33", "24", "148", null, this.div_splitTop);
            obj.set_taborder("3");
            obj.set_cssclass("btn_WF_add");
            obj.set_tooltiptext("추가");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_header");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"24\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"260\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"400\"/><Column size=\"200\"/><Column size=\"130\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" style=\"align: ;\" text=\"NO\"/><Cell col=\"3\" text=\"mulaapmsg_hdkey\"/><Cell col=\"4\" text=\"msgtype\"/><Cell col=\"5\" text=\"lakey\"/><Cell col=\"6\" text=\"apkey\"/><Cell col=\"7\" text=\"displaymessage\"/><Cell col=\"8\" text=\"displaymessage_abbr\"/><Cell col=\"9\" text=\"closingdate\"/><Cell col=\"10\" text=\"insertdate\"/><Cell col=\"11\" text=\"inserturkey\"/><Cell col=\"12\" text=\"updatedate\"/><Cell col=\"13\" text=\"updateurkey\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(STATUS == &quot;C&quot; ? &quot;img::ico_add.png&quot; : STATUS == &quot;U&quot; ? &quot;img::ico_modify.png&quot; : STATUS == &quot;D&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" displaytype=\"number\" style=\"align:right;padding: ;\" text=\"bind:PAGING_NUM\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:mulaapmsg_hdkey\"/><Cell col=\"4\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:msgtype\" editdisplay=\"display\" combodataset=\"ds_combo_msgtype\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"5\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" combodataset=\"ds_combo_lakey\" combocodecol=\"field1\" combodatacol=\"field2\"/><Cell col=\"6\" displaytype=\"combo\" edittype=\"combo\" style=\"align:left;padding: ;\" text=\"bind:apkey\" editdisplay=\"edit\" combodataset=\"ds_combo_apkey\" combocodecol=\"field1\" combodatacol=\"field1\"/><Cell col=\"7\" displaytype=\"text\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:displaymessage\"/><Cell col=\"8\" displaytype=\"text\" edittype=\"text\" style=\"align:left;padding: ;\" text=\"bind:displaymessage_abbr\" editdisplay=\"edit\"/><Cell col=\"9\" displaytype=\"date\" edittype=\"date\" style=\"align:left;padding: ;\" text=\"bind:closingdate\" mask=\"yyyy-MM-dd HH:mm:ss\"/><Cell col=\"10\" displaytype=\"date\" style=\"align:center;padding: ;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"11\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:inserturkey\"/><Cell col=\"12\" displaytype=\"date\" style=\"align:center;padding: ;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"13\" style=\"align:left;padding: ;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_copy", "absolute", null, "0", "100", "24", "185", null, this.div_splitTop);
            obj.set_taborder("6");
            obj.set_text("App Duplicate");
            obj.set_tooltiptext("copy");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_line_duplicate", "absolute", null, "0", "100", "24", "290", null, this.div_splitTop);
            obj.set_taborder("7");
            obj.set_text("Line Duplicate");
            obj.set_tooltiptext("copy");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_excelUpload", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_excelup");
            obj.set_tooltiptext("엑셀 업로드");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "0", "7", "190", "20", null, null, this.div_splitTop);
            obj.set_taborder("9");
            obj.set_text("메시지 내역");
            obj.set_cssclass("sta_WF_title");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Div("div_splitBottom", "absolute", "0", null, null, "256", "0", "0", this);
            obj.set_taborder("47");
            obj.set_scrollbars("none");
            obj.style.set_background("transparent");
            this.addChild(obj.name, obj);
            obj = new Static("Static06", "absolute", "3", "3", "350", "20", null, null, this.div_splitBottom);
            obj.set_taborder("5");
            obj.set_cssclass("sta_WF_title");
            obj.set_text("메시지별 센터 내역");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_save", "absolute", null, "0", "33", "24", "0", null, this.div_splitBottom);
            obj.set_taborder("8");
            obj.set_cssclass("btn_WF_save");
            obj.set_tooltiptext("저장");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Grid("grd_detail", "absolute", "-1", "28", "580", null, null, "0", this.div_splitBottom);
            obj.set_taborder("10");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_binddataset("ds_detail");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"130\"/><Column size=\"250\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" text=\"ctkey\"/><Cell col=\"2\" text=\"ctname\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"1\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"2\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:ctname\" editdisplay=\"edit\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Grid("grd_detail_save", "absolute", null, "27", "580", null, "0", "0", this.div_splitBottom);
            obj.set_taborder("11");
            obj.set_cellsizingtype("col");
            obj.set_cellmovingtype("col,band");
            obj.set_binddataset("ds_detail_save");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"25\" band=\"left\"/><Column size=\"130\"/><Column size=\"250\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell/><Cell col=\"1\" displaytype=\"checkbox\"/><Cell col=\"2\" text=\"ctkey\"/><Cell col=\"3\" text=\"ctname\"/></Band><Band id=\"body\"><Cell style=\"backgroundimage:EXPR(dataset.getRowType(currow) == &quot;2&quot; ? &quot;img::ico_add.png&quot; : dataset.getRowType(currow) == &quot;4&quot; ? &quot;img::ico_modify.png&quot; : dataset.getRowType(currow) == &quot;8&quot; ? &quot;img::ico_delete.png&quot; : &quot;&quot;);\"/><Cell col=\"1\" displaytype=\"checkbox\" edittype=\"checkbox\" style=\"padding: ;\" text=\"bind:CHK\"/><Cell col=\"2\" style=\"align:left;padding: ;\" text=\"bind:ctkey\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding: ;\" text=\"bind:ctname\" editdisplay=\"edit\"/></Band></Format></Formats>");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_left", "absolute", "595", "110", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("12");
            obj.set_text(">");
            this.div_splitBottom.addChild(obj.name, obj);
            obj = new Button("btn_right", "absolute", "595", "143", "42", "28", null, null, this.div_splitBottom);
            obj.set_taborder("13");
            obj.set_text("<");
            this.div_splitBottom.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "0", "361", null, "10", "0", null, this);
            obj.set_taborder("51");
            obj.set_text("H10");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "0", "395", null, "5", "0", null, this);
            obj.set_taborder("52");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_apkey", "absolute", "0", null, "113", "24", null, "-37", this);
            obj.set_taborder("53");
            obj.set_visible("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 256, this.div_splitBottom,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("47");
            		p.set_scrollbars("none");
            		p.style.set_background("transparent");

            	}
            );
            this.div_splitBottom.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("메시지");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","div_splitTop.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","div_splitTop.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","div_splitTop.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","div_splitBottom.btn_save","tooltiptext","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","div_splitTop.btn_copy","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","div_splitTop.btn_line_duplicate","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","div_splitTop.btn_excelUpload","tooltiptext","gds_lang","EXCELUPLOAD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","div_splitTop.Static06","text","gds_lang","ME00000070_01");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","div_splitBottom.Static06","text","gds_lang","ME00000070_02");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Message.xfdl", "lib::Comm.xjs");
        this.registerScript("Message.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : Message.xfdl
        * PROGRAMMER  : eungheon.kim
        * DATE        : 2017.01.06
        * DESCRIPTION : 메세지
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* 폼 로드 */
        this.form_onload = function(obj,e)
        {
        	// 첫번째 컴포넌트|두번째 컴포넌트|움직임(상하,좌우)|버튼 컴포넌트명
        	var splitButton = ["this.div_splitTop|this.div_splitBottom|H|this.btn_work_split_h"];
        	this.gfn_split(this, splitButton); // 화면 div 사이즈 조정

        	var keyField = ["mulaapmsg_hdkey|msgtype|lakey|apkey"];
        	var grdList = [this.div_splitTop.grd_header, this.div_splitBottom.grd_detail, this.div_splitBottom.grd_detail_save]; // 그리드 오브젝트
        	var divPaging = [this.div_splitTop.div_Paging]; // 페이지 오브젝트
        	var searchFunc = [this.MAIN]; // 페이지 검색 영역
        	this.fn_dsComboInit();
        	this.gfn_gridInit(grdList, "", divPaging, searchFunc, keyField);
        	this.calculateDetailPosition();	
        }

        this.fn_dsComboInit = function(){
        	//MSGTYPE 콤보
        	this.gfn_getCode("MSGTYPE", this.ds_combo_msgtype);
        	//국가별 언어코드
        	this.gfn_getCode("LAKEY", this.ds_combo_lakey);
        	//어플리케이션 코드
        	//this.gfn_getAppRole(this.ds_combo_apkey);
        	this.gfn_getCommCodeTran( ""
        						, "11"
        						, "ds_combo_apkey"
        						, ""
        						, ""
        						, ""
        						);
        }

        this.form_onsize = function(obj,e)
        { 
        	this.calculateDetailPosition();
        }

        this.div_splitBottom_onsize = function(obj,e)
        {
        	this.calculateDetailPosition();
        }
        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.gfn_getScroll(this.div_splitTop.grd_header);
        	
        	this.gfn_setCommon("BEANID",   "messageController");
        	this.gfn_setCommon("METHODNM", "selectMessageInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "sortValue=DESC colName=UPDATEDATE";

        	if(this.gfn_isUpdate(this.ds_header) || this.gfn_isUpdate(this.ds_detail_save)){
        		this.gfn_confirm("MSG_10027", "", function(msg, flag){
        			if(flag){
        				this.ds_header.clearData();
        				this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        			}
        		});
        	}else{
        		this.ds_header.clearData();
        		this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        	}
        }

        this.fn_searchDetail = function()
        {
        	this.gfn_getScroll(this.div_splitBottom.grd_detail);
        	
        	var apkey    	   = this.ds_header.getColumn(this.ds_header.rowposition, "apkey");
        	var hdkey          = this.ds_header.getColumn(this.ds_header.rowposition, "mulaapmsg_hdkey");
        	var lakey          = this.ds_header.getColumn(this.ds_header.rowposition, "lakey");
        	var msgtype        = this.ds_header.getColumn(this.ds_header.rowposition, "msgtype");
        	var displaymessage = this.ds_header.getColumn(this.ds_header.rowposition, "displaymessage");
        	var closingdate    = this.ds_header.getColumn(this.ds_header.rowposition, "closingdate");
        				
        	this.ds_detail.clearData();
        	this.ds_detail_save.clearData();
        	
        	this.gfn_setCommon("BEANID", "messageController");
        	this.gfn_setCommon("METHODNM", "selectMessageCenter");
        	
            var sSvcId   = "selectDetail";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_detail=OUT_rtnGrid1 ds_detail_save=OUT_rtnGrid2 ds_param=OUT_PARAM";
            var sParam   = "apkey="+apkey+" msgtype="+msgtype+" displaymessage="+displaymessage+" closingdate="+closingdate;
                sParam  += " mulaapmsg_hdkey="+hdkey+" displaymessage="+displaymessage+" lakey="+lakey;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /* 그리드 해더 클릭(Grid Header Click)  */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	this.gfn_grdHeaderClick(obj, e, this);
        }

        this.div_splitBottom_grd_detail_onheadclick = function(obj,e)
        {
        	this.gfn_grdHeaderClickNotSearch(obj, e, this);
        }

        this.div_splitBottom_grd_detail_save_onheadclick = function(obj,e)
        {
        	this.gfn_grdHeaderClickNotSearch(obj, e, this);
        }

        this.ds_header_onrowposchanged = function(obj,e)
        {
        	var checkModule = this.ds_header.getColumn(this.ds_header.rowposition, "apkey");
        	if(this.isSearchPossible){
        		if(this.ds_header.rowposition >= 0){
        			
        // 	var nRow = this.ds_detail_save.findRowExpr("dataset.getRowType(currow) == '2' || dataset.getRowType(currow) == '4'");
        // 	
        // 	if(e.columnid != "CHK"){
        // 		if(nRow != -1){
        // 			this.gfn_confirm("MSG_10027", "", function(msg, flag){
        // 				if(!flag){
        // 					obj.setColumn(e.row, e.columnid, e.oldvalue);
        // 				}
        // 			});
        // 		}
        // 	}else{
        // 		obj.set_updatecontrol(true);
        // 	}
        			if(checkModule == "WM"){
        				
        					this.div_splitBottom.grd_detail.set_enable(true);
        					this.div_splitBottom.grd_detail_save.set_enable(true);
        					this.div_splitBottom.btn_left.set_enable(true);
        					this.div_splitBottom.btn_right.set_enable(true);
        					
        					var hdkey          = this.ds_header.getColumn(this.ds_header.rowposition, "mulaapmsg_hdkey");
        					var lakey          = this.ds_header.getColumn(this.ds_header.rowposition, "lakey");
        					var msgtype        = this.ds_header.getColumn(this.ds_header.rowposition, "msgtype");
        					var displaymessage = this.ds_header.getColumn(this.ds_header.rowposition, "displaymessage");
        					var closingdate    = this.ds_header.getColumn(this.ds_header.rowposition, "closingdate");
        					
        					this.fn_searchDetail(checkModule, hdkey, lakey, msgtype, displaymessage, closingdate);		
        			
        			}else{
        				this.div_splitBottom.grd_detail.set_enable(false);
        				this.div_splitBottom.grd_detail_save.set_enable(false);
        				this.div_splitBottom.btn_left.set_enable(false);
        				this.div_splitBottom.btn_right.set_enable(false);
        				
        				this.ds_detail.clearData();
        				this.ds_detail_save.clearData();
        			}
        		}
        	}
        }

        /* 그리드 해더 셀 더블클릭(Grid Header Cell DoubleClick)  */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	this.gfn_grdDoubleClick(obj);	
        }

        this.div_splitTop_btn_line_duplicate_onclick = function(obj,e)
        {
        	var currRow = this.ds_header.rowposition;
        	if(currRow < 0){
        		this.gfn_alert("MSG_10003");
        		return;
        	}
        	var idx = this.ds_header.addRow();
        	this.ds_header.copyRow(idx, this.ds_header, currRow);
        	this.ds_header.setColumn(idx, "STATUS", "C");
        }

        this.div_splitTop_btn_copy_onclick = function(obj,e)
        {
        	var oArg = { divId:""
        		,searchId:11
        		,putObj:this.edt_apkey
        		,putKey:"ctkey"
        		,putValue:""+this.gfn_getUserInfo("ctKey")
        		,putAppkey:"ams"
        		,putBean:"commonController"
        		,putMethod:"selectCommonCode"
        		,putCallback:"applicationPopCallback"
        	   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 547, "");
        }

        this.isSearchPossible = true;

        this.applicationPopCallback = function(obj,valueArr,typeCheck){
        	if(this.gfn_isNotNull(valueArr) && valueArr.length > 0){
        		var currRow = this.ds_header.rowposition;
        		if(currRow < 0){
        			this.gfn_alert("MSG_10003");
        			return;
        		}
        		this.isSearchPossible = false;
        		
        		var idx = -1;
        		var checkCount = -1;
        		var mulaapmsg_hdkey = this.ds_header.getColumn(currRow, "mulaapmsg_hdkey");
        		var msgtype = this.ds_header.getColumn(currRow, "msgtype");
        		var lakey = this.ds_header.getColumn(currRow, "lakey");
        		var currApkey = this.ds_header.getColumn(currRow, "apkey");
        		var filterString  = "mulaapmsg_hdkey == '"+mulaapmsg_hdkey+"'";
        		    filterString += "&& msgtype == '"+msgtype+"'";
        		    filterString += "&& lakey == '"+lakey+"'";
        		var checkString = "";
        		
        		this.ds_header_check.clearData();
        		this.ds_header_check.copyData(this.ds_header);

        		this.div_splitTop.grd_header.set_enableredraw(false);
        		if( typeCheck == "multi" ){
        			for(var i = 0 ; i < valueArr.length ; i++){
        				if(currApkey == valueArr[i]){
        					continue;
        				}
        				checkString = "&& apkey == '"+valueArr[i]+"'";
        				
        				this.ds_header_check.filter((filterString+checkString));
        				checkCount = this.ds_header_check.getRowCount();
        				if(checkCount < 1){
        					idx = this.ds_header.addRow();
        					this.ds_header.copyRow(idx, this.ds_header, currRow);
        					this.ds_header.setColumn(idx, "apkey", valueArr[i]);
        					this.ds_header.setColumn(idx, "STATUS", "C");								
        				}
        				this.ds_header_check.filter("");
        			}		
        		}else{
        			if(currApkey != valueArr[0]){
        				checkString = "&& apkey == '"+valueArr[0]+"'";
        				
        					this.ds_header_check.filter((filterString+checkString));
        					checkCount = this.ds_header_check.getRowCount();
        					if(checkCount < 1){
        						this.ds_header.set_enableevent(true);
        						idx = this.ds_header.addRow();
        						this.ds_header.copyRow(idx, this.ds_header, currRow);
        						this.ds_header.setColumn(idx, "apkey", valueArr[0]);			
        						this.ds_header.setColumn(idx, "STATUS", "C");						
        					}
        					this.ds_header_check.filter("");
        			}
        		}

        		
        		this.div_splitTop.grd_header.set_enableredraw(true);
        	}
        	this.isSearchPossible = true;
        }

        /* 추가 버튼 클릭(Add Button Click) */
        this.div_splitTop_btn_add_onclick = function(obj,e)
        {
        	var rowIndex = this.ds_header.addRow();
        	this.ds_header.setColumn(rowIndex, "CHK", 1);
        	this.ds_header.setColumn(rowIndex, "PAGING_NUM", rowIndex+1);
        	this.ds_header.setColumn(rowIndex, "STATUS", "C");
        }

        /* 저장 버튼 클릭(Save Button Click) */
        this.div_splitTop_btn_save_onclick = function(obj,e)
        {
        	var dsCol = "mulaapmsg_hdkey|msgtype|lakey|apkey";    
        	//그리드내 상태 체크(추가, 수정, 삭제 존재 확인)
        	if(this.gfn_isUpdate(this.ds_header)){
        		if(this.gfn_isDataNullCheck(this.div_splitTop.grd_header,this.ds_header,dsCol)){ //그리드 필수 값 체크 
        			this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        				if(flag){
        					this.fn_save();
        				} else {   // 취소시 변경내용 reset
        				if(obj == null){
        					this.ds_header.reset();
        				}
        			}
        			});	
        		}
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.fn_save = function()
        {
        	this.gfn_setCommon("BEANID", "messageController");
        	this.gfn_setCommon("METHODNM", "saveMessageList");
        	
        	var sSvcId   = "save";
        	var sSvcUrl  = application.gv_ams + application.gv_saveUrl;
        	var sInData  = "IN_MESSAGE_LIST=ds_header:U";
        	var sOutData = "ds_returnValue=OUT_rtnMap";
        	var sParam   = "";
        	this.ds_returnValue.clearData();
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /* 삭제 버튼 클릭(Delete Button Click) */
        this.div_splitTop_btn_delete_onclick = function(obj,e)
        {
        // 	var nRow = this.ds_header.findRow("CHK", "1");
        // 	
        // 	if(nRow == -1){
        // 		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        // 	}else{
        // 		this.fn_delete();
        // 	}

        	var nRow = this.ds_header.findRowExpr("CHK == '1'");
        	var nChkCnt = this.ds_header.getCaseCount("CHK == '1'");
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		while(nRow != -1){
        			if(this.ds_header.getRowType(nRow) == "2"){
        				this.ds_header.deleteRow(nRow);
        				nRow = this.ds_header.findRowExpr("CHK == '1'");
        			}else{
        				this.ds_header.setColumn(nRow, "STATUS", "D");
        				nRow = this.ds_header.findRowExpr("CHK == '1'", nRow+1);
        			}
        		}
        		if (this.ds_header.getCaseCount("STATUS == 'D'") > 0) this.div_splitTop_btn_save_onclick();
        	}

        }

        this.fn_delete = function()
        {	
        	//그리드에 체크된 row의 index를 배열로 리턴 
        	var checkList = this.gfn_getCheckedIndex(this.div_splitTop.grd_header);
        	
        	var idx = -1;
        	for(var i=0; i<checkList.length ; i++){
        		idx = this.ds_header_save.addRow();
        		this.ds_header_save.copyRow(idx, this.ds_header, checkList[i]);
        	}

        	this.ds_header.deleteMultiRows(checkList);
        }

        /* 엑셀 버튼 클릭(Excel Button Click) */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	var oValue = {
        	}
        	
        	this.parent.div_cond.fn_excelSearch("messageController/excelDownMessage.do", this.div_splitTop.grd_header , this, oValue);
        }

        /***********************************************************************************
        * 그리드 디테일(Grid Detail)
        ***********************************************************************************/
        /* > 버튼 클릭(> Button Click) */
        this.div_splitBottom_btn_left_onclick = function(obj,e)
        {

        	var nRow = this.ds_detail.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		var checkList = this.gfn_getCheckedIndex(this.div_splitBottom.grd_detail);
        		

        		if(this.ds_detail_save.rowcount == 0){
        			this.ds_detail_save.assign(this.ds_detail);
        			this.ds_detail_save.deleteAll();
        		}

        		var addRowIndex = -1;
        		
        		for(var idx=0;idx<checkList.length;idx++){
        			if(this.ds_detail_save.findRow("ctkey", this.ds_detail.getColumn(checkList[idx], "ctkey")) < 0){
        				addRowIndex = this.ds_detail_save.addRow();
        				this.ds_detail_save.copyRow(addRowIndex, this.ds_detail, checkList[idx]);

        				this.ds_detail_save.setColumn(addRowIndex, "apkey"			, this.ds_header.getColumn(this.ds_header.rowposition, "apkey"));
        				this.ds_detail_save.setColumn(addRowIndex, "mulaapmsg_hdkey", this.ds_header.getColumn(this.ds_header.rowposition, "mulaapmsg_hdkey"));
        				this.ds_detail_save.setColumn(addRowIndex, "lakey"			, this.ds_header.getColumn(this.ds_header.rowposition, "lakey"));
        				this.ds_detail_save.setColumn(addRowIndex, "displaymessage"	, this.ds_header.getColumn(this.ds_header.rowposition, "displaymessage"));
        				this.ds_detail_save.setColumn(addRowIndex, "msgtype"	    , this.ds_header.getColumn(this.ds_header.rowposition, "msgtype"));
        				this.ds_detail_save.setColumn(addRowIndex, "closingdate"	, this.ds_header.getColumn(this.ds_header.rowposition, "closingdate"));

        			}
        		}
        	}
        }

        /* < 버튼 클릭(< Button Click) */
        this.div_splitBottom_btn_right_onclick = function(obj,e)
        {
        	var nRow = this.ds_detail_save.findRowExpr("CHK == '1'");
        	
        	if(nRow == -1){
        		this.gfn_alert("MSG_CHKDATA_NOSELECT");
        	}else{
        		this.gfn_checkedGridDelete(this.div_splitBottom.grd_detail_save);
        	}
        }

        /* 저장 버튼 클릭(Save Button Click) */
        this.div_splitBottom_btn_save_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_detail_save)){
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				this.fn_detail_save();
        			}
        		});	
        	}else{
        		this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        	}
        }

        this.fn_detail_save = function(){

        	var apkey   	   = this.ds_header.getColumn(this.ds_header.rowposition, "apkey");
        	var hdkey          = this.ds_header.getColumn(this.ds_header.rowposition, "mulaapmsg_hdkey");
        	var lakey          = this.ds_header.getColumn(this.ds_header.rowposition, "lakey");
        	var msgtype        = this.ds_header.getColumn(this.ds_header.rowposition, "msgtype");
        	var displaymessage = this.ds_header.getColumn(this.ds_header.rowposition, "displaymessage");
        	var closingdate    = this.ds_header.getColumn(this.ds_header.rowposition, "closingdate");

        	this.gfn_setCommon("BEANID", "messageController");
        	this.gfn_setCommon("METHODNM", "saveMessageCenter");
        	
        	var sSvcId   = "detail_save";
        	var sSvcUrl  = application.gv_ams + application.gv_sUrl;
        	var sInData  = "IN_list=ds_detail_save";
        	var sOutData = "";
        	var sParam   = "apkey="+apkey+" msgtype="+msgtype+" displaymessage="+displaymessage+" closingdate="+closingdate;
        		sParam  += " mulaapmsg_hdkey="+hdkey+" displaymessage="+displaymessage+" lakey="+lakey;
        	
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	if(sSvcId == "select") {
        		this.gfn_selectAfter(this.div_splitTop.grd_header, this);
        		this.gfn_setPage(this.div_splitTop.div_Paging, this.ds_param);
        	}else if(sSvcId == "selectDetail"){
        		this.gfn_selectAfter(this.div_splitBottom.grd_detail, this);
        		this.gfn_selectAfter(this.div_splitBottom.grd_detail_save, this);
        	}else if(sSvcId == "save"){
        		if(this.ds_returnValue.getRowCount() == 0){
        			this.gfn_alert("MSG_ALERT_COMPLETE");
        		}else{
        			var keyValue = "";
        			var apkey = "";
        			for(var i = 0 ; i < this.ds_returnValue.getRowCount() ; i++){
        				if(i != 0){
        					keyValue +="\n";
        				}
        				keyValue += this.ds_returnValue.getColumn(i, "mulaapmsg_hdkey")+"_";
        				keyValue += this.ds_returnValue.getColumn(i, "msgtype")+"_";
        				keyValue += this.ds_returnValue.getColumn(i, "lakey")+"_";
        				keyValue += this.ds_returnValue.getColumn(i, "apkey");
        			}
        			 
        			var arg = "KEYVALUE,"+keyValue;
        			this.gfn_alert("TEST_MSG_DUP", arg);
        		}
        		this.parent.div_cond.btn_search.click();
        	}else if(sSvcId == "detail_save"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		this.ds_detail_save.addColumn("CHK");
        		this.fn_searchDetail();
        	}
        }

        /*******************************************************************************
        * 사용자 정의 함수 (User Definition Function)
        ********************************************************************************/
        this.calculateDetailPosition = function(){
        	var btn_Gap = 10;
        	var btn_HGap = 47;
        	
        	var v_nX = nexacro.round(this.div_splitBottom.getOffsetWidth() / 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetWidth() / 2) - btn_Gap;
        	var v_nY = nexacro.round(this.div_splitBottom.grd_detail.getOffsetHeight() / 2) - nexacro.round(this.div_splitBottom.btn_left.getOffsetHeight() / 2);
        	
        	this.div_splitBottom.grd_detail.set_width(v_nX);
        	this.div_splitBottom.grd_detail_save.set_width(v_nX);
        	this.div_splitBottom.btn_left.set_left(v_nX+btn_Gap);
        	this.div_splitBottom.btn_right.set_left(v_nX+btn_Gap);

        	this.div_splitBottom.btn_left.set_top(v_nY+btn_Gap);
        	this.div_splitBottom.btn_right.set_top(v_nY+btn_HGap);
        }

        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if (e.columnid == 'msgtype' && e.newvalue == "MENU") { 
        		var row = e.row;
        		this.ds_header.setColumn(row,"apkey","ADMIN");
        	}
        	
        	if (e.columnid == 'apkey' && e.newvalue != "ADMIN" ) { 
        		var row = e.row;
        		var msgtype = obj.getColumn(row , "msgtype")
        		if (msgtype == "MENU") { 
        			this.ds_header.setColumn(row,"msgtype",null);
        		} 
        	}
        	
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(true);
        	}else if(e.columnid != "STATUS"){
        		this.gfn_statusChk(obj, e.row);
        	} 
        }
        this.ds_header_cancolumnchange = function(obj,e)
        {
        	if(e.columnid == "CHK"){
        		obj.set_updatecontrol(false);
        	}else{
        		if(obj.getColumn(e.row, "STATUS") != "C" && obj.getColumn(e.row, "STATUS") != "D") obj.setColumn(e.row, "STATUS", "");
        	}
        }

        this.div_splitTop_btn_excelUpload_onclick = function(obj,e)
        {

        	var oArg = {
        		menuId:"ME00109601",
        		uskey:"US00009061",
        		menuNm:"MESSAGE_EXCELUPLOAD",
        		menu: "comm::ExcelUploadPop.xfdl",
        		ultype : this.AMS_ULTYPE_VINFLUX_EXCELUPLOAD_MESSAGE,
        		apkey : application.gv_activeApp,
        		controllerService : "messageUploadController",
        		uploadAfterSearch : "selectMessageTemp",
        		tab2SearchService : "selectMessageUploadInfo",
        		uploadService : "uploadMessageExcel",
        		saveService : "saveMessage",
        		samplePath : "master",
        		sampleDownName : "Master_MSGUP_v1.0.xls",
        		excelDownService : "excelDown"
        	};
        	
        	this.gfn_popup("messageExcelUploadPop", "comm::ExcelUploadPop.xfdl", oArg, 1918, 948, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.ds_header.addEventHandler("cancolumnchange", this.ds_header_cancolumnchange, this);
            this.ds_detail_save.addEventHandler("onrowposchanged", this.ds_detail_save_onrowposchanged, this);
            this.ds_header_check.addEventHandler("onrowposchanged", this.ds_header_onrowposchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.form_onsize, this);
            this.addEventHandler("ontimer", this.Message_ontimer, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.btn_delete.addEventHandler("onclick", this.div_splitTop_btn_delete_onclick, this);
            this.div_splitTop.btn_save.addEventHandler("onclick", this.div_splitTop_btn_save_onclick, this);
            this.div_splitTop.btn_add.addEventHandler("onclick", this.div_splitTop_btn_add_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_copy.addEventHandler("onclick", this.div_splitTop_btn_copy_onclick, this);
            this.div_splitTop.btn_line_duplicate.addEventHandler("onclick", this.div_splitTop_btn_line_duplicate_onclick, this);
            this.div_splitTop.btn_excelUpload.addEventHandler("onclick", this.div_splitTop_btn_excelUpload_onclick, this);
            this.div_splitBottom.addEventHandler("onsize", this.div_splitBottom_onsize, this);
            this.div_splitBottom.btn_save.addEventHandler("onclick", this.div_splitBottom_btn_save_onclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("oncellclick", this.div_splitBottom_grd_detail_oncellclick, this);
            this.div_splitBottom.grd_detail.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_onheadclick, this);
            this.div_splitBottom.grd_detail_save.addEventHandler("onheadclick", this.div_splitBottom_grd_detail_save_onheadclick, this);
            this.div_splitBottom.btn_left.addEventHandler("onclick", this.div_splitBottom_btn_left_onclick, this);
            this.div_splitBottom.btn_right.addEventHandler("onclick", this.div_splitBottom_btn_right_onclick, this);

        };

        this.loadIncludeScript("Message.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
