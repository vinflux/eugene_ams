﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("CenterPop");
                this.set_classname("style01");
                this.set_titletext("센터 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,310);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"cttype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"256\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/><Column id=\"stacd\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"delyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"CHIT_CTKEY\" type=\"STRING\" size=\"256\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"256\"/><Column id=\"tel\" type=\"STRING\" size=\"256\"/><Column id=\"closingdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"regno\" type=\"STRING\" size=\"256\"/><Column id=\"addnt\" type=\"STRING\" size=\"256\"/><Column id=\"cttype\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"256\"/><Column id=\"address1\" type=\"STRING\" size=\"32\"/><Column id=\"stacd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"ctkey\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"zipcode\" type=\"STRING\" size=\"256\"/><Column id=\"reportctname\" type=\"STRING\" size=\"256\"/><Column id=\"unit\" type=\"STRING\" size=\"256\"/><Column id=\"ctname\" type=\"STRING\" size=\"32\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addr\" type=\"STRING\" size=\"256\"/><Column id=\"address2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"cttype_desc\"/><Col id=\"cntrycd_desc\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"lnmAdres\"/><Col id=\"delyn_desc\"/><Col id=\"delyn\"/><Col id=\"CHIT_CTKEY\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"tel\"/><Col id=\"closingdate\"/><Col id=\"lat\"/><Col id=\"regno\"/><Col id=\"addnt\"/><Col id=\"cttype\"/><Col id=\"addrsttcd\"/><Col id=\"address1\"/><Col id=\"stacd_desc\"/><Col id=\"ctkey\"/><Col id=\"shpglocnm\"/><Col id=\"zipNo\"/><Col id=\"zipcode\"/><Col id=\"reportctname\"/><Col id=\"unit\"/><Col id=\"ctname\"/><Col id=\"insertdate\"/><Col id=\"addrid\"/><Col id=\"updateurkey\"/><Col id=\"addr\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_cttype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_addr", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipNo\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipNo\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"lnmAdres\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("20");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "22", this);
            obj.set_taborder("10");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("센터코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("센터명");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "89", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사업자등록번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("센터유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "367", "490", "62", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctname", "absolute", "453", "62", "165", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Static("Static05", "absolute", "22", "367", "120", "62", null, null, this);
            obj.set_taborder("325");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("주소");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "124", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "22", this);
            obj.set_taborder("11");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctkey", "absolute", "148", "62", "105", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            obj.set_maxlength("10");
            obj.set_inputtype("number,english");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "120", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("리포트명");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_reportctname", "absolute", "453", "124", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            obj.set_maxlength("33");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel", "absolute", "148", "124", "165", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_cttype", "absolute", "453", "93", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("4");
            obj.set_innerdataset("ds_cttype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Button("btn_check", "absolute", "256", "62", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new TextArea("txa_addr", "absolute", "148", "371", "442", "53", null, null, this);
            obj.set_taborder("16");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "141", "429", "490", "31", null, null, this);
            obj.set_taborder("480");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "22", "429", "120", "31", null, null, this);
            obj.set_taborder("481");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("위도");
            this.addChild(obj.name, obj);

            obj = new Static("Static19", "absolute", "327", "429", "120", "31", null, null, this);
            obj.set_taborder("482");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("경도");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_lon", "absolute", "453", "433", "165", "22", null, null, this);
            obj.set_taborder("19");
            obj.style.set_align("right middle");
            obj.style.setStyleValue("align", "disabled", "left middle");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_lat", "absolute", "148", "433", "165", "22", null, null, this);
            obj.set_taborder("18");
            obj.style.set_align("right middle");
            obj.style.setStyleValue("align", "disabled", "left middle");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAddr", "absolute", "593", "371", "24", "22", null, null, this);
            obj.set_taborder("17");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "65", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "696", "62", "165", "22", null, null, this);
            obj.set_taborder("515");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_unit", "absolute", "696", "86", "165", "22", null, null, this);
            obj.set_taborder("516");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "696", "110", "165", "22", null, null, this);
            obj.set_taborder("517");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "696", "134", "165", "22", null, null, this);
            obj.set_taborder("518");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "696", "158", "165", "22", null, null, this);
            obj.set_taborder("519");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "696", "182", "165", "22", null, null, this);
            obj.set_taborder("520");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "696", "206", "165", "22", null, null, this);
            obj.set_taborder("521");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "696", "230", "165", "22", null, null, this);
            obj.set_taborder("522");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_type("string");
            obj.set_mask("###-##-#####");
            obj.set_limitbymask("integer");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "141", "214", "490", "30", null, null, this);
            obj.set_taborder("523");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "141", "182", "490", "30", null, null, this);
            obj.set_taborder("524");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "142", "151", "490", "31", null, null, this);
            obj.set_taborder("525");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchZipcd", "absolute", "289", "155", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "148", "155", "138", "22", null, null, this);
            obj.set_taborder("527");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "151", "120", "31", null, null, this);
            obj.set_taborder("528");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "22", "182", "120", "30", null, null, this);
            obj.set_taborder("529");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "213", "120", "30", null, null, this);
            obj.set_taborder("530");
            obj.set_text("상세 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#555555ff");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "217", "470", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "185", "469", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 310, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("센터 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_ctname","value","ds_header","ctname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","CTKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","CTNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","CTTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","Static05","text","gds_lang","ADDR");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_ctkey","value","ds_header","ctkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","TEL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","REPORTCTNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_reportctname","value","ds_header","reportctname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_tel","value","ds_header","tel");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_cttype","value","ds_header","cttype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","CENTER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","Static12","text","gds_lang","LAT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","Static19","text","gds_lang","LON");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","edt_lon","value","ds_header","lon");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","edt_lat","value","ds_header","lat");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","edt_unit","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","edt_ctynm","value","ds_header","ctynm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","txa_addr","value","ds_header","addr");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","Static03","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","Static06","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("CenterPop.xfdl", "lib::Comm.xjs");
        this.registerScript("CenterPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : CenterPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 센터 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_ctkey = "";
        this.gv_zipcode = "";
        this.gv_address2 = "";
        this.gv_dupChk = false;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
            // code, dataset, combo name, combo value, blank name
            this.gfn_getCommCode( "CTTYPE"
        						, "ds_cttype"
        						, ""
        						, ""
        						, ""
        						);
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_ctkey    = this.gfn_isNullEmpty(this.parent.ctkey);
            this.gv_zipcode    = this.gfn_isNullEmpty(this.parent.zipcode);
            this.gv_address2    = this.gfn_isNullEmpty(this.parent.address2);
        	this.ds_header.setColumn(0, "lon", 0);
        	this.ds_header.setColumn(0, "lat", 0);

        	this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.edt_ctkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "centerController");
        	this.gfn_setCommon("METHODNM", "selectCenter");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnList ds_param=OUT_PARAM";
            var sParam   =  "ctkey="+this.gv_ctkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.ds_param.clearData();
        	
        	this.gfn_setCommon("BEANID", "centerController");
        	this.gfn_setCommon("METHODNM", "selectCenter");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_rtnList";
            var sParam   =  "ctkey="+this.ds_header.getColumn(0, "ctkey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "centerController");
        	this.gfn_setCommon("METHODNM", "insertCenter");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_CENTER=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "centerController");
        	this.gfn_setCommon("METHODNM", "updateCenter");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_CENTER=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_searchAddr = function()
        {
        	this.gfn_getScroll(this.gv_headerr); // 스크롤 위치 저장
        	this.gfn_setCommon("BEANID", "");
        	this.gfn_setCommon("METHODNM", "");
        	this.gv_headerr.set_nodatatext("");
        	
            var sSvcId   = "selectHD";
            var sSvcUrl  = application.gv_oms + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_headerr=OUT_rtnDtGrid ds_paramDetail=OUT_PARAM";
            var sParam   = 
            
        	this.rowChg = "Y";
        	this.gfn_grdSortSet(this.gv_headerr); // sort 조회 셋팅
        	this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");	
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
         		var sAddr = this.gfn_isNullEmpty(this.ds_header.getColumn(0, "lnmAdres")) + "\n" + 
        					this.gfn_isNullEmpty(this.ds_header.getColumn(0, "stnm"));
         		this.txa_addr.set_value(sAddr);

        		this.ds_header.applyChange();
        		
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.rowcount > 0){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_ctkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "ctkey|ctname|cttype";
        	var sComp = "edt_ctkey|edt_ctname|cbo_cttype";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_ctkey.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U"){
        						 //주소가 변경되면 새로운 주소ID 생성하기 위한 로직
        							if(this.gv_zipcode != this.edt_zipcode.value || this.gv_address2 != this.text_address2.value){
        								this.ds_header.setColumn(0, "addrid", "");
        							}
        							this.fn_Update();
        						 }
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        // /* btn_searchAddr_onclick 실행 */
        // this.btn_searchAddr_onclick = function(obj:Button,  e:nexacro.ClickEventInfo)
        // {
        // 	if(this.gfn_isNull(this.edt_addrid.value)){
        // 		var oArg = { argFlag:"H"
        // 					,addrid:this.edt_addrid.value
        // 					,putObj:this.txa_addr
        // 					,putDs:this.ds_addr
        // 				   };
        // 	}else{
        // 		var oArg = { argFlag:"U"
        // 					,addrid:this.edt_addrid.value
        // 					,putObj:this.txa_addr
        // 					,putDs:this.ds_addr
        // 				   };
        // 	}
        // 	this.gfn_popup("AddrPop_New", "master::AddrPop_New.xfdl", oArg, 580, 330, "fn_popCallback");
        // }

        
        this.fn_popCallback = function(sSvcId) {
        	if(sSvcId == 'AddrPop_New') {
        		this.ds_header.setColumn(0, "lat", this.ds_addr.getColumn(0, "lat"));
        		this.ds_header.setColumn(0, "lon", this.ds_addr.getColumn(0, "lon"));
        		this.ds_header.setColumn(0, "addrid", this.ds_addr.getColumn(0, "addrid"));
        		this.ds_header.setColumn(0, "lnmAdres", this.ds_addr.getColumn(0, "lnmAdres"));
        		this.ds_header.setColumn(0, "stnm", this.ds_addr.getColumn(0, "stnm"));
        		this.ds_header.setColumn(0, "cntrycd", this.ds_addr.getColumn(0, "cntrycd"));
        		this.ds_header.setColumn(0, "zipNo", this.ds_addr.getColumn(0, "zipNo"));
        	}
        } 

        
        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "ctkey";
        	var sComp = "edt_ctkey";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "ctkey"){
        		this.gv_dupChk = false;
        	}
        }

        this.btn_searchZipcd_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_ctname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_reportctname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.btn_searchAddr.addEventHandler("onclick", this.btn_searchAddr_onclick, this);
            this.btn_searchZipcd.addEventHandler("onclick", this.btn_searchZipcd_onclick, this);

        };

        this.loadIncludeScript("CenterPop.xfdl", true);

       
    };
}
)();
