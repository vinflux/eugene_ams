﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("Test");
                this.set_classname("style01");
                this.set_titletext("주소 테스트");
                this._setFormPosition(0,0,1230,627);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"sta\" type=\"STRING\" size=\"32\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"DELYN_DESC\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"STRING\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_searchList", this);
            obj._setContents("<ColumnInfo><Column id=\"dbColoum\" type=\"STRING\" size=\"256\"/><Column id=\"operator\" type=\"STRING\" size=\"256\"/><Column id=\"value\" type=\"STRING\" size=\"256\"/><Column id=\"fromVal\" type=\"STRING\" size=\"256\"/><Column id=\"toVal\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelSetting", this);
            obj._setContents("<ColumnInfo><Column id=\"DATA_FILED\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME1\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME2\" type=\"STRING\" size=\"256\"/><Column id=\"FILED_NAME3\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"DATA_FILED\">adcd_hdkey</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 코드</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 코드</Col></Row><Row><Col id=\"DATA_FILED\">adcd_hdname</Col><Col id=\"FILED_NAME1\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME2\">관리자 기준 코드 공통 명</Col><Col id=\"FILED_NAME3\">관리자 기준 코드 공통 명</Col></Row><Row><Col id=\"DATA_FILED\">insertdate</Col><Col id=\"FILED_NAME1\">입력 일시</Col><Col id=\"FILED_NAME2\">입력 일시</Col><Col id=\"FILED_NAME3\">입력 일시</Col></Row><Row><Col id=\"DATA_FILED\">inserturkey</Col><Col id=\"FILED_NAME1\">입력 사용자 ID</Col><Col id=\"FILED_NAME2\">입력 사용자 ID</Col><Col id=\"FILED_NAME3\">입력 사용자 ID</Col></Row><Row><Col id=\"DATA_FILED\">updatedate</Col><Col id=\"FILED_NAME1\">수정 일시</Col><Col id=\"FILED_NAME2\">수정 일시</Col><Col id=\"FILED_NAME3\">수정 일시</Col></Row><Row><Col id=\"DATA_FILED\">updateurkey</Col><Col id=\"FILED_NAME1\">수정 사용자 ID</Col><Col id=\"FILED_NAME2\">수정 사용자 ID</Col><Col id=\"FILED_NAME3\">수정 사용자 ID</Col></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_excelFile", this);
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_header_save", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"sta\" type=\"STRING\" size=\"32\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"DELYN_DESC\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"STRING\" size=\"32\"/><Column id=\"PAGING_NUM\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Div("div_splitTop", "absolute", "0", "0", null, null, "0", "100", this);
            obj.set_taborder("46");
            obj.set_scrollbars("none");
            this.addChild(obj.name, obj);
            obj = new Button("btn_excel", "absolute", null, "0", "33", "24", "0", null, this.div_splitTop);
            obj.set_taborder("0");
            obj.set_cssclass("btn_WF_excel");
            obj.set_tooltiptext("엑셀 다운");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Div("div_Paging", "absolute", "0", null, null, "29", "0", "0", this.div_splitTop);
            obj.set_taborder("5");
            obj.set_scrollbars("none");
            obj.set_url("comm::MainPaging.xfdl");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Grid("grd_header", "absolute", "0", "29", null, null, "0", "28", this.div_splitTop);
            obj.set_taborder("4");
            obj.set_binddataset("ds_header");
            obj.set_cellmovingtype("col,band");
            obj.set_autoenter("select");
            obj.set_cellsizingtype("col");
            obj.set_selecttype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"25\" band=\"left\"/><Column size=\"40\" band=\"left\"/><Column size=\"100\"/><Column size=\"150\"/><Column size=\"100\"/><Column size=\"200\"/><Column size=\"200\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/><Column size=\"130\"/><Column size=\"100\"/></Columns><Rows><Row size=\"26\" band=\"head\"/><Row size=\"26\"/></Rows><Band id=\"head\"><Cell displaytype=\"checkbox\"/><Cell col=\"1\" style=\"align: ;\" text=\"NO\"/><Cell col=\"2\" text=\"addrid\"/><Cell col=\"3\" text=\"addnt\"/><Cell col=\"4\" displaytype=\"normal\" text=\"unit\"/><Cell col=\"5\" text=\"stnm\"/><Cell col=\"6\" text=\"ctynm\"/><Cell col=\"7\" text=\"zipcd\"/><Cell col=\"8\" text=\"sta\"/><Cell col=\"9\" text=\"ctry\"/><Cell col=\"10\" text=\"lat\"/><Cell col=\"11\" text=\"lon\"/><Cell col=\"12\" text=\"delyn\"/><Cell col=\"13\" text=\"insertdate\"/><Cell col=\"14\" text=\"inserturkey\"/><Cell col=\"15\" text=\"updatedate\"/><Cell col=\"16\" text=\"updateurkey\"/></Band><Band id=\"body\"><Cell displaytype=\"checkbox\" edittype=\"checkbox\" text=\"bind:CHK\"/><Cell col=\"1\" displaytype=\"number\" style=\"align:right;padding:0 2 0 2;\" expr=\"currow+1\"/><Cell col=\"2\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:addrid\"/><Cell col=\"3\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:addnt\"/><Cell col=\"4\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:unit\"/><Cell col=\"5\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:stnm\"/><Cell col=\"6\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:ctynm\"/><Cell col=\"7\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:zipcd\"/><Cell col=\"8\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:sta\"/><Cell col=\"9\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:ctry\"/><Cell col=\"10\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lat\"/><Cell col=\"11\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:lon\"/><Cell col=\"12\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:delyn\"/><Cell col=\"13\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:insertdate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"14\" displaytype=\"normal\" edittype=\"none\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:inserturkey\"/><Cell col=\"15\" displaytype=\"date\" style=\"align:center;padding:0 2 0 2;\" text=\"bind:updatedate\" mask=\"yyyy-MM-dd HH:mm:ss\" calendardisplaynulltype=\"none\"/><Cell col=\"16\" style=\"align:left;padding:0 2 0 2;\" text=\"bind:updateurkey\"/></Band></Format></Formats>");
            this.div_splitTop.addChild(obj.name, obj);
            obj = new Button("btn_test", "absolute", null, "0", "80", "24", "37", null, this.div_splitTop);
            obj.set_taborder("6");
            obj.set_text("Test_LatLon");
            this.div_splitTop.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "0", "24", null, "5", "0", null, this);
            obj.set_taborder("49");
            obj.set_text("H5");
            obj.set_cssclass("Guide_color");
            obj.set_visible("false");
            obj.style.set_background("#ff000066");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new WebBrowser("web", "absolute", "0", null, null, "100", "0", "0", this);
            obj.set_taborder("50");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 0, 0, this.div_splitTop,
            	//-- Layout function
            	function(p) {
            		p.set_taborder("46");
            		p.set_scrollbars("none");

            	}
            );
            this.div_splitTop.addLayout(obj.name, obj);

            //-- Default Layout
            obj = new Layout("default", "", 1230, 627, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("주소 테스트");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","div_01.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","div_02.btn_add","tooltiptext","gds_lang","ADD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","div_02.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","div_01.btn_update","tooltiptext","gds_lang","UPDATE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","div_01.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","div_02.btn_delete","tooltiptext","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","div_01.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","div_02.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","div_splitTop.btn_excel","tooltiptext","gds_lang","EXCELDOWN");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        
        this.loadPreloadList = function()
        {
            this._addPreloadList("fdl", "comm::MainPaging.xfdl");
        };
        
        // User Script
        this.addIncludeScript("Test.xfdl", "lib::Comm.xjs");
        this.registerScript("Test.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : IntransitInventory.xfdl
        * PROGRAMMER  : eungheon.kim
        * DATE        : 2017.03.08
        * DESCRIPTION : 센터간 이동중 재고 조회
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_main = "Main"; // 검색 영역을 통해 조회
        this.gv_middle = "Middle"; // 업무영역 내 조회
        this.gv_detral = "Detail"; // 업무영역 내 조회
        this.gv_grdList;
        this.gv_keyField = [""];
        this.gv_keyValueList;

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* 폼 로드 */
        this.form_onload = function(obj,e)
        { 
        	this.gv_grdList = [this.div_splitTop.grd_header]; // 그리드 오브젝트
        	var divPaging = [this.div_splitTop.div_Paging]; // 페이지 오브젝트
        	var searchFunc = [this.gv_main]; // 페이지 검색 영역
        	//그리드 데이터 추가시 입력가능 수정시 수정불가능 목록 정의
        	this.gfn_gridInit(this.gv_grdList, "", divPaging, searchFunc, this.gv_keyField);
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.set_keystring(""); // 필터 초기화
        	this.ds_header.clearData();

        	this.gfn_setCommon("BEANID",   "addrController");
        	this.gfn_setCommon("METHODNM", "selectAddrInfo");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_rtnGrid ds_param=OUT_PARAM";
            var sParam   = "";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /***********************************************************************************
        * 그리드 해더
        ***********************************************************************************/
        /* 그리드 해더 클릭(Grid Header Click)  */
        this.div_splitTop_grd_header_onheadclick = function(obj,e)
        {
        	this.gfn_grdHeaderClick(obj, e, this);
        }

        /* 그리드 해더 셀 더블클릭(Grid Header Cell DoubleClick)  */
        this.div_splitTop_grd_header_oncelldblclick = function(obj,e)
        {
        	this.gfn_grdDoubleClick(obj);	
        }

        /* 엑셀 버튼 클릭(Excel Button Click) */
        this.div_splitTop_btn_excel_onclick = function(obj,e)
        {
        	this.gfn_exportExcel(this, this.div_splitTop.grd_header, "Message");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select") {
        		this.gfn_selectAfter(this.div_splitTop.grd_header, this);
        		this.gfn_setPage(this.div_splitTop.div_Paging, this.ds_param);
        	}else if(sSvcId == "exceldown"){
        		this.gfn_excelDownload(this.ds_excelFile);
        	}else if(sSvcId == "getLatLonTest"){
        		this.gfn_alert("MSG_ALERT_COMPLETE");
        		//this.parent.div_cond.btn_search.click();
        	}
        }

        /*******************************************************************************
        * 사용자 정의 함수 (User Definition Function)
        ********************************************************************************/
        this.div_splitTop_btn_test_onclick = function(obj,e)
        {
        	this.getLatLonTest();
        }

        this.getLatLonTest = function()
        {
        	this.gfn_grdCheckCopyToDataset(this.div_splitTop.grd_header, this.ds_header, this.ds_header_save);
        	
        	this.gfn_setCommon("BEANID",   "addrController");
        	this.gfn_setCommon("METHODNM", "saveLatLon");
        	
            var sSvcId   = "getLatLonTest";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_DATA_LIST=ds_header_save";
            var sOutData = "";
            var sParam   = "";
            trace(this.ds_header_save.saveXML());

            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.webBrowserTest = function()
        {
        	this.gfn_grdCheckCopyToDataset(this.div_splitTop.grd_header, this.ds_header, this.ds_header_save);
        	var ctynm = this.gfn_getGridSelectedColumn(this.div_splitTop.grd_header, "ctynm");
        	trace("ctynm : "+ctynm);
        	trace(this.ds_header_save.saveXML());
        	//$address = "전주시 완산구 삼천동1가 306-2";
        	this.web.set_url("http://maps.google.com/maps/api/geocode/xml?address="+encodeURI(ctynm)+"&sensor=false");
        	this.web.show();
        }

        this.web_onloadcompleted = function(obj,e)
        {
        	var test = "";
        	for(var key in this.web){
        		test += key+",";
        		
        	}
        	trace("window : "+this.web.window);	
        	trace("document : "+this.web.document);	
        	trace("_ifrm_elem : "+this.web._ifrm_elem);
        	trace("_ifrm_elem._getDoc() : "+this.web._ifrm_elem._getDoc());
        	trace("_ifrm_elem._winodw : "+this.web._ifrm_elem._winodw);

        	trace("_control_element : "+this.web._control_element);
        	trace("getProperty : "+this.web.getProperty());
        	trace("_getRootComponent : "+this.web._getRootComponent());
        	trace("_getElementRootControl : "+this.web._getElementRootControl());
        	
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.addEventHandler("onsize", this.form_onsize, this);
            this.div_splitTop.btn_excel.addEventHandler("onclick", this.div_splitTop_btn_excel_onclick, this);
            this.div_splitTop.grd_header.addEventHandler("onheadclick", this.div_splitTop_grd_header_onheadclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncellclick", this.div_splitTop_grd_header_oncellclick, this);
            this.div_splitTop.grd_header.addEventHandler("oncelldblclick", this.div_splitTop_grd_header_oncelldblclick, this);
            this.div_splitTop.btn_test.addEventHandler("onclick", this.div_splitTop_btn_test_onclick, this);
            this.web.addEventHandler("onloadcompleted", this.web_onloadcompleted, this);

        };

        this.loadIncludeScript("Test.xfdl", true);
        this.loadPreloadList();
       
    };
}
)();
