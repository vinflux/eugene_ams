﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("UserGroupPop");
                this.set_classname("style01");
                this.set_titletext("사용자그룹 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,580,187);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_adminCode", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("true");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"adcd_hdkey\" type=\"string\" size=\"32\"/><Column id=\"adcd_hdname\" type=\"string\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"adcd_hdname\"/><Col id=\"adcd_hdkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("lab_title", "absolute", "22", "14", "194", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            obj.set_text("관리 기준 코드 헤더");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("115");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", null, null, "52", "28", "292", "23", this);
            obj.set_taborder("121");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("sta_code", "absolute", "22", "58", "194", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head");
            obj.set_usedecorate("true");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "216", "58", "342", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_code", "absolute", "222", "62", "260", "22", null, null, this);
            obj.set_taborder("161");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "216", "89", "342", "31", null, null, this);
            obj.set_taborder("163");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_name", "absolute", "222", "93", "321", "22", null, null, this);
            obj.set_taborder("180");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "120", null, "1", "22", null, this);
            obj.set_taborder("225");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", null, null, "52", "28", "235", "23", this);
            obj.set_taborder("226");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("sta_name", "absolute", "22", "89", "194", "31", null, null, this);
            obj.set_taborder("227");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Button("btn_check", "absolute", "485", "62", "57", "22", null, null, this);
            obj.set_taborder("228");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 580, 187, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("사용자그룹 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item0","edt_code","value","ds_adminCode","adcd_hdkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_name","value","ds_adminCode","adcd_hdname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","lab_title","text","gds_lang","ADMIN CODE HEADER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","btn_save","text","gds_lang","APPROVAL_SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("UserGroupPop.xfdl", "lib::Comm.xjs");
        this.registerScript("UserGroupPop.xfdl", function() {
        /***********************************************************************************
        * FILE NAME   : AdminCodePop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 관리 기준 코드 팝업 화면
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        //include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_code = "";
        this.gv_name = "";
        this.gv_menuId = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_initForm(obj);
        	
        // 	this.sta_code.set_text(this.opener.gfn_getLang("ADCD_HDKEY") + "<fc v='red'>*</fc>");
        // 	this.sta_name.set_text(this.opener.gfn_getLang("ADCD_HDNAME") + "<fc v='red'>*</fc>");

        	this.sta_code.set_text(this.opener.gfn_getLang("URGRKEY") + "<fc v='red'>*</fc>");
        	this.sta_name.set_text(this.opener.gfn_getLang("URGRNAME") + "<fc v='red'>*</fc>");

            this.gv_flag = this.gfn_isNullEmpty(this.parent.argFlag);
            this.gv_menuId = this.gfn_isNullEmpty(this.parent.menuId);
            this.gv_code = this.gfn_isNullEmpty(this.parent.adcd_hdkey);
            this.gv_name = this.gfn_isNullEmpty(this.parent.adcd_hdname);
            
            if(this.gv_flag == "U"){
        		this.edt_code.set_enable(false);
        		this.btn_check.set_enable(false);
        		
        		this.ds_adminCode.setColumn(0, "adcd_hdkey", this.parent.adcd_hdkey);
        		this.ds_adminCode.setColumn(0, "adcd_hdname", this.parent.adcd_hdname);
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "checkAdminCodeInfo");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   = "adcd_hdkey="+this.ds_adminCode.getColumn(0, "adcd_hdkey");
            
            trace(sParam);
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "insertAdminCode");
        	
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_adminCode";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            trace(this.ds_adminCode.saveXML());
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "masterAdminCodeController");
        	this.gfn_setCommon("METHODNM", "updateAdminCode");
        	
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_adminCode";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = "";
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_code.set_value("");
        				this.edt_code.setFocus();
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	if(this.gfn_isNull(this.edt_code.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_code.setFocus();
        		});
        	}else if((this.gfn_isNull(this.ds_param.getColumn(0, "isDuplicate")) || this.ds_param.getColumn(0, "isDuplicate") == "true") && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_code.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_name.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_name.setFocus();
        		});
        	}else{
        		this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        			if(flag){
        				if(this.gv_flag == "H") this.fn_Insert();
        				else if(this.gv_flag == "U") this.fn_Update();
        			}
        		});
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gv_code != this.edt_code.value || this.gv_name != this.edt_name.value){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* edt_code_onchanged 실행 */
        this.edt_code_onchanged = function(obj,e)
        {
        	if(this.ds_param.rowcount > 0) this.ds_param.setColumn(0, "isDuplicate", "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.ds_param.getColumn(0, "isDuplicate") == "false"){
        		return;
        	}
        	
        	if(this.gfn_isNull(this.edt_code.value)){
        		this.gfn_alert("MSG_10001", "", function(msg, flag){
        			this.edt_code.setFocus();
        		});
        	}else{
        		this.fn_CheckCode();
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_closeAll_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_code.addEventHandler("onchanged", this.edt_code_onchanged, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);

        };

        this.loadIncludeScript("UserGroupPop.xfdl");

       
    };
}
)();
