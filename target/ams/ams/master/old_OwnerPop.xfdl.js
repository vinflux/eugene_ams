﻿(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        this.on_create = function()
        {
            // Declare Reference
            var obj = null;
            
            if (Form == this.constructor) {
                this.set_name("OwnerPop");
                this.set_classname("style01");
                this.set_titletext("화주 팝업");
                this.set_scrollbars("none");
                this._setFormPosition(0,0,657,745);
            }

            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("ds_header", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"divcd\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"sttcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd1\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"256\"/><Column id=\"stacd\" type=\"STRING\" size=\"256\"/><Column id=\"boxtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"corp2id\" type=\"STRING\" size=\"32\"/><Column id=\"businesstype\" type=\"STRING\" size=\"32\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"contact\" type=\"STRING\" size=\"32\"/><Column id=\"memoid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"delsttvrftyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"extcd2\" type=\"STRING\" size=\"32\"/><Column id=\"fax\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd_desc\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"STRING\" size=\"256\"/><Column id=\"regno\" type=\"STRING\" size=\"32\"/><Column id=\"addnt\" type=\"STRING\" size=\"256\"/><Column id=\"owkey\" type=\"STRING\" size=\"32\"/><Column id=\"lakey_desc\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"256\"/><Column id=\"zipcd\" type=\"STRING\" size=\"256\"/><Column id=\"brcdtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"billtocustcd\" type=\"STRING\" size=\"32\"/><Column id=\"custtype\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"currtyp\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"STRING\" size=\"256\"/><Column id=\"regno1\" type=\"STRING\" size=\"32\"/><Column id=\"loggrpcd\" type=\"STRING\" size=\"32\"/><Column id=\"regno2\" type=\"STRING\" size=\"32\"/><Column id=\"regno3\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"dimension_uom\" type=\"STRING\" size=\"32\"/><Column id=\"currtyp_desc\" type=\"STRING\" size=\"32\"/><Column id=\"tel1\" type=\"STRING\" size=\"32\"/><Column id=\"tel2\" type=\"STRING\" size=\"32\"/><Column id=\"dftdestshpgloccd\" type=\"STRING\" size=\"32\"/><Column id=\"lon\" type=\"STRING\" size=\"256\"/><Column id=\"alwmxdyn_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ceo\" type=\"STRING\" size=\"32\"/><Column id=\"corp1id\" type=\"STRING\" size=\"32\"/><Column id=\"custtype_desc\" type=\"STRING\" size=\"32\"/><Column id=\"ctynm\" type=\"STRING\" size=\"256\"/><Column id=\"lakey\" type=\"STRING\" size=\"32\"/><Column id=\"weburl\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"256\"/><Column id=\"owname\" type=\"STRING\" size=\"32\"/><Column id=\"boxtype\" type=\"STRING\" size=\"32\"/><Column id=\"email\" type=\"STRING\" size=\"32\"/><Column id=\"sttcd\" type=\"STRING\" size=\"32\"/><Column id=\"weight_uom\" type=\"STRING\" size=\"32\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"256\"/><Column id=\"stacd_desc\" type=\"STRING\" size=\"256\"/><Column id=\"udf3\" type=\"STRING\" size=\"32\"/><Column id=\"dftorishpgloccd\" type=\"STRING\" size=\"32\"/><Column id=\"udf1\" type=\"STRING\" size=\"32\"/><Column id=\"udf2\" type=\"STRING\" size=\"32\"/><Column id=\"uomtyp\" type=\"STRING\" size=\"32\"/><Column id=\"delsttvrftyp\" type=\"STRING\" size=\"32\"/><Column id=\"businesscategory\" type=\"STRING\" size=\"32\"/><Column id=\"brcdtyp\" type=\"STRING\" size=\"32\"/><Column id=\"alwmxdyn\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/><Column id=\"addr\" type=\"STRING\" size=\"256\"/><Column id=\"zipNo\" type=\"STRING\" size=\"256\"/><Column id=\"lnmAdres\" type=\"STRING\" size=\"256\"/><Column id=\"delyn\" type=\"STRING\" size=\"256\"/><Column id=\"address2\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"divcd\"/><Col id=\"cntrycd_desc\"/><Col id=\"sttcd_desc\"/><Col id=\"extcd1\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"stacd\"/><Col id=\"boxtype_desc\"/><Col id=\"corp2id\"/><Col id=\"businesstype\"/><Col id=\"updatedate\"/><Col id=\"contact\"/><Col id=\"memoid\"/><Col id=\"delsttvrftyp_desc\"/><Col id=\"extcd2\"/><Col id=\"fax\"/><Col id=\"loggrpcd_desc\"/><Col id=\"lat\"/><Col id=\"regno\"/><Col id=\"addnt\"/><Col id=\"owkey\"/><Col id=\"lakey_desc\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"brcdtyp_desc\"/><Col id=\"billtocustcd\"/><Col id=\"custtype\"/><Col id=\"currtyp\"/><Col id=\"unit\"/><Col id=\"regno1\"/><Col id=\"loggrpcd\"/><Col id=\"regno2\"/><Col id=\"regno3\"/><Col id=\"uomtyp_desc\"/><Col id=\"dimension_uom\"/><Col id=\"currtyp_desc\"/><Col id=\"tel1\"/><Col id=\"tel2\"/><Col id=\"dftdestshpgloccd\"/><Col id=\"lon\"/><Col id=\"alwmxdyn_desc\"/><Col id=\"ceo\"/><Col id=\"corp1id\"/><Col id=\"custtype_desc\"/><Col id=\"ctynm\"/><Col id=\"lakey\"/><Col id=\"weburl\"/><Col id=\"stnm\"/><Col id=\"owname\"/><Col id=\"boxtype\"/><Col id=\"email\"/><Col id=\"sttcd\"/><Col id=\"weight_uom\"/><Col id=\"addrsttcd\"/><Col id=\"stacd_desc\"/><Col id=\"udf3\"/><Col id=\"dftorishpgloccd\"/><Col id=\"udf1\"/><Col id=\"udf2\"/><Col id=\"uomtyp\"/><Col id=\"delsttvrftyp\"/><Col id=\"businesscategory\"/><Col id=\"brcdtyp\"/><Col id=\"alwmxdyn\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"updateurkey\"/><Col id=\"addr\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_param", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_boxtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_yesno", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_custtype", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_addr", this);
            obj.set_firefirstcount("0");
            obj.getSetter("firenextcount").set("0");
            obj.set_useclientlayout("false");
            obj.set_updatecontrol("true");
            obj.set_enableevent("true");
            obj.set_loadkeymode("keep");
            obj.set_loadfiltermode("keep");
            obj.set_reversesubsum("false");
            obj._setContents("<ColumnInfo><Column id=\"sta\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addnt\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"addrsttcd\" type=\"STRING\" size=\"32\"/><Column id=\"inserturkey\" type=\"STRING\" size=\"32\"/><Column id=\"cntrycd\" type=\"STRING\" size=\"32\"/><Column id=\"shpglocnm\" type=\"STRING\" size=\"32\"/><Column id=\"zipcd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"lon\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"stacd\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"ctynm\" type=\"STRING\" size=\"32\"/><Column id=\"delyn\" type=\"STRING\" size=\"32\"/><Column id=\"unit\" type=\"UNDEFINED\" size=\"0\"/><Column id=\"updatedate\" type=\"STRING\" size=\"32\"/><Column id=\"stnm\" type=\"STRING\" size=\"32\"/><Column id=\"ctry\" type=\"STRING\" size=\"32\"/><Column id=\"addrid\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"insertdate\" type=\"STRING\" size=\"32\"/><Column id=\"lat\" type=\"BIGDECIMAL\" size=\"16\"/><Column id=\"updateurkey\" type=\"STRING\" size=\"32\"/></ColumnInfo><Rows><Row><Col id=\"sta\"/><Col id=\"addnt\"/><Col id=\"addrsttcd\"/><Col id=\"inserturkey\"/><Col id=\"cntrycd\"/><Col id=\"shpglocnm\"/><Col id=\"zipcd\"/><Col id=\"lon\"/><Col id=\"stacd\"/><Col id=\"ctynm\"/><Col id=\"delyn\"/><Col id=\"unit\"/><Col id=\"updatedate\"/><Col id=\"stnm\"/><Col id=\"ctry\"/><Col id=\"addrid\"/><Col id=\"insertdate\"/><Col id=\"lat\"/><Col id=\"updateurkey\"/></Row></Rows>");
            this.addChild(obj.name, obj);

            obj = new Dataset("ds_loggrpcd", this);
            obj._setContents("<ColumnInfo><Column id=\"field1\" type=\"STRING\" size=\"256\"/><Column id=\"field2\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            
            // UI Components Initialize
            obj = new Static("Static46", "absolute", "142", "610", "490", "31", null, null, this);
            obj.set_taborder("510");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static41", "absolute", "22", "610", "120", "31", null, null, this);
            obj.set_taborder("503");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("물류 그룹 코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static50", "absolute", "142", "455", "490", "31", null, null, this);
            obj.set_taborder("524");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static13", "absolute", "142", "362", "490", "31", null, null, this);
            obj.set_taborder("422");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("lab_title", "absolute", "22", "14", "120", "20", null, null, this);
            obj.set_taborder("109");
            obj.set_text("TITLE");
            obj.set_usedecorate("false");
            obj.set_cssclass("sta_WF_label");
            this.addChild(obj.name, obj);

            obj = new Static("Static00", "absolute", "22", "40", null, "2", "22", null, this);
            obj.set_taborder("113");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Button("btn_closeAll", "absolute", null, "14", "26", "20", "22", null, this);
            obj.set_taborder("36");
            obj.set_cssclass("btn_P_close");
            obj.set_visible("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_save", "absolute", "273", null, "52", "28", null, "23", this);
            obj.set_taborder("34");
            obj.set_text("저장");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Static("Static04", "absolute", "22", "57", null, "1", "22", null, this);
            obj.set_taborder("128");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static08", "absolute", "22", "58", "120", "31", null, null, this);
            obj.set_taborder("157");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_text("화주코드");
            this.addChild(obj.name, obj);

            obj = new Static("Static09", "absolute", "142", "58", "490", "31", null, null, this);
            obj.set_taborder("158");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static02", "absolute", "327", "58", "120", "31", null, null, this);
            obj.set_taborder("226");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_wordwrap("char");
            obj.set_text("화주명");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit14", "absolute", "148", "93", "373", "22", null, null, this);
            obj.set_taborder("293");
            this.addChild(obj.name, obj);

            obj = new Static("Static39", "absolute", "142", "89", "490", "31", null, null, this);
            obj.set_taborder("294");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static40", "absolute", "22", "89", "120", "31", null, null, this);
            obj.set_taborder("295");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사업자등록 번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static42", "absolute", "327", "89", "120", "31", null, null, this);
            obj.set_taborder("297");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("팩스 번호");
            this.addChild(obj.name, obj);

            obj = new Static("Static49", "absolute", "141", "151", "490", "62", null, null, this);
            obj.set_taborder("305");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owname", "absolute", "453", "62", "165", "22", null, null, this);
            obj.set_taborder("2");
            obj.set_enable("true");
            obj.set_maxlength("30");
            this.addChild(obj.name, obj);

            obj = new Static("Static11", "absolute", "22", "331", "120", "31", null, null, this);
            obj.set_taborder("331");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("사용자 정의 필드1");
            this.addChild(obj.name, obj);

            obj = new Static("Static17", "absolute", "142", "331", "490", "31", null, null, this);
            obj.set_taborder("337");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit05", "absolute", "148", "124", "373", "22", null, null, this);
            obj.set_taborder("365");
            this.addChild(obj.name, obj);

            obj = new Static("Static27", "absolute", "142", "120", "490", "31", null, null, this);
            obj.set_taborder("366");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static28", "absolute", "22", "120", "120", "31", null, null, this);
            obj.set_taborder("367");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업태");
            this.addChild(obj.name, obj);

            obj = new Static("Static29", "absolute", "327", "120", "120", "31", null, null, this);
            obj.set_taborder("368");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("업종");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit02", "absolute", "148", "521", "373", "22", null, null, this);
            obj.set_taborder("387");
            this.addChild(obj.name, obj);

            obj = new Static("Static36", "absolute", "142", "517", "490", "31", null, null, this);
            obj.set_taborder("388");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static37", "absolute", "22", "517", "120", "31", null, null, this);
            obj.set_taborder("389");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("운송 상태확인");
            this.addChild(obj.name, obj);

            obj = new Static("Static38", "absolute", "327", "517", "120", "31", null, null, this);
            obj.set_taborder("390");
            obj.set_usedecorate("true");
            obj.set_cssclass("sta_WF_head");
            obj.set_wordwrap("char");
            obj.set_text("혼적허용");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesstype", "absolute", "148", "124", "165", "22", null, null, this);
            obj.set_taborder("5");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Button("btn_close", "absolute", "330", null, "52", "28", null, "23", this);
            obj.set_taborder("35");
            obj.set_text("닫기");
            obj.set_cssclass("btn_p");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_businesscategory", "absolute", "453", "124", "165", "22", null, null, this);
            obj.set_taborder("6");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_owkey", "absolute", "148", "62", "105", "22", null, null, this);
            obj.set_taborder("0");
            obj.set_enable("true");
            obj.set_maxlength("20");
            obj.set_inputtype("number,english");
            obj.set_inputmode("upper");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_fax", "absolute", "453", "93", "165", "22", null, null, this);
            obj.set_taborder("4");
            obj.set_enable("true");
            obj.set_maxlength("33");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Static("Static01", "absolute", "22", "362", "120", "31", null, null, this);
            obj.set_taborder("416");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사용자 정의 필드2");
            this.addChild(obj.name, obj);

            obj = new Static("Static10", "absolute", "141", "238", "490", "31", null, null, this);
            obj.set_taborder("433");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static14", "absolute", "22", "238", "120", "31", null, null, this);
            obj.set_taborder("435");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("대표 이사");
            this.addChild(obj.name, obj);

            obj = new Static("Static15", "absolute", "22", "269", "120", "31", null, null, this);
            obj.set_taborder("438");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호1");
            this.addChild(obj.name, obj);

            obj = new Static("Static16", "absolute", "142", "269", "490", "31", null, null, this);
            obj.set_taborder("439");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static18", "absolute", "327", "269", "120", "31", null, null, this);
            obj.set_taborder("440");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("전화 번호2");
            this.addChild(obj.name, obj);

            obj = new Edit("Edit00", "absolute", "148", "428", "373", "22", null, null, this);
            obj.set_taborder("441");
            this.addChild(obj.name, obj);

            obj = new Static("Static20", "absolute", "142", "424", "490", "31", null, null, this);
            obj.set_taborder("442");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static21", "absolute", "22", "424", "120", "31", null, null, this);
            obj.set_taborder("443");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("박스 유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static22", "absolute", "22", "455", "120", "31", null, null, this);
            obj.set_taborder("444");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("언어");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel2", "absolute", "453", "273", "165", "22", null, null, this);
            obj.set_taborder("11");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Static("Static31", "absolute", "327", "238", "120", "31", null, null, this);
            obj.set_taborder("448");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("담당자");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_contact", "absolute", "453", "242", "165", "22", null, null, this);
            obj.set_taborder("9");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_tel1", "absolute", "148", "273", "165", "22", null, null, this);
            obj.set_taborder("10");
            obj.set_enable("true");
            obj.set_maxlength("30");
            obj.set_inputtype("number,sign");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ceo", "absolute", "148", "242", "165", "22", null, null, this);
            obj.set_taborder("8");
            obj.set_enable("true");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static33", "absolute", "141", "486", "490", "31", null, null, this);
            obj.set_taborder("455");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static43", "absolute", "327", "486", "120", "31", null, null, this);
            obj.set_taborder("456");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("바코드 유형");
            this.addChild(obj.name, obj);

            obj = new Static("Static44", "absolute", "22", "486", "120", "31", null, null, this);
            obj.set_taborder("457");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("통화");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_alwmxdyn", "absolute", "453", "521", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("26");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Button("btn_check", "absolute", "256", "62", "57", "22", null, null, this);
            obj.set_taborder("1");
            obj.set_text("중복체크");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_currtyp", "absolute", "148", "490", "42", "22", null, null, this);
            obj.set_taborder("21");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchCurrtyp", "absolute", "193", "490", "24", "22", null, null, this);
            obj.set_taborder("22");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf1", "absolute", "148", "335", "470", "22", null, null, this);
            obj.set_taborder("14");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf2", "absolute", "148", "366", "470", "22", null, null, this);
            obj.set_taborder("15");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Static("Static03", "absolute", "142", "393", "490", "31", null, null, this);
            obj.set_taborder("477");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static06", "absolute", "22", "393", "120", "31", null, null, this);
            obj.set_taborder("478");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("사용자 정의 필드3");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_udf3", "absolute", "148", "397", "470", "22", null, null, this);
            obj.set_taborder("16");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchAddr", "absolute", "289", "155", "24", "22", null, null, this);
            obj.set_taborder("7");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static24", "absolute", "22", "300", "120", "31", null, null, this);
            obj.set_taborder("486");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("크기 UOM");
            this.addChild(obj.name, obj);

            obj = new Static("Static25", "absolute", "142", "300", "490", "31", null, null, this);
            obj.set_taborder("487");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static26", "absolute", "327", "300", "120", "31", null, null, this);
            obj.set_taborder("488");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("중량 UOM");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_weight_uom", "absolute", "453", "304", "165", "22", null, null, this);
            obj.set_taborder("13");
            obj.set_maxlength("10");
            obj.set_inputtype("number,english,dot,comma,sign");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_dimension_uom", "absolute", "148", "304", "165", "22", null, null, this);
            obj.set_taborder("12");
            obj.set_maxlength("10");
            obj.set_inputtype("number,english,dot,comma,sign");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_boxtype", "absolute", "148", "428", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("17");
            obj.set_innerdataset("ds_boxtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Edit("edt_lakey", "absolute", "148", "459", "138", "22", null, null, this);
            obj.set_taborder("19");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchLakey", "absolute", "289", "459", "24", "22", null, null, this);
            obj.set_taborder("20");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_brcdtyp", "absolute", "453", "490", "42", "22", null, null, this);
            obj.set_taborder("23");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchBrcdtyp", "absolute", "498", "490", "24", "22", null, null, this);
            obj.set_taborder("24");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_delsttvrftyp", "absolute", "148", "521", "165", "22", null, null, this);
            obj.set_taborder("25");
            obj.set_maxlength("10");
            this.addChild(obj.name, obj);

            obj = new Static("Static30", "absolute", "22", "548", "120", "31", null, null, this);
            obj.set_taborder("497");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("이메일");
            this.addChild(obj.name, obj);

            obj = new Static("Static32", "absolute", "142", "548", "490", "31", null, null, this);
            obj.set_taborder("498");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static34", "absolute", "327", "548", "120", "31", null, null, this);
            obj.set_taborder("499");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("웹사이트");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_weburl", "absolute", "453", "552", "165", "22", null, null, this);
            obj.set_taborder("28");
            obj.set_inputtype("number,english,symbol");
            obj.set_maxlength("256");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_email", "absolute", "148", "552", "165", "22", null, null, this);
            obj.set_taborder("27");
            obj.set_inputtype("number,english,symbol");
            obj.set_maxlength("100");
            this.addChild(obj.name, obj);

            obj = new Static("Static35", "absolute", "141", "579", "490", "31", null, null, this);
            obj.set_taborder("502");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static45", "absolute", "22", "579", "120", "31", null, null, this);
            obj.set_taborder("504");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.set_text("UOM타입");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_uomtyp", "absolute", "148", "583", "138", "22", null, null, this);
            obj.set_taborder("29");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchUomtyp", "absolute", "289", "583", "24", "22", null, null, this);
            obj.set_taborder("30");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static47", "absolute", "327", "424", "120", "31", null, null, this);
            obj.set_taborder("511");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static48", "absolute", "327", "610", "120", "31", null, null, this);
            obj.set_taborder("512");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.set_text("상태");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_custtype", "absolute", "453", "428", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("18");
            obj.set_innerdataset("ds_custtype");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Edit("edt_sttcd", "absolute", "453", "614", "42", "22", null, null, this);
            obj.set_taborder("32");
            obj.set_enable("true");
            this.addChild(obj.name, obj);

            obj = new Button("btn_searchSttcd", "absolute", "498", "614", "24", "22", null, null, this);
            obj.set_taborder("33");
            obj.set_cssclass("btn_WF_srh_s");
            this.addChild(obj.name, obj);

            obj = new Static("Static23", "absolute", "22", null, null, "1", "22", "66", this);
            obj.set_taborder("345");
            obj.style.set_background("#888888ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrid", "absolute", "696", "62", "165", "22", null, null, this);
            obj.set_taborder("515");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_unit", "absolute", "696", "86", "165", "22", null, null, this);
            obj.set_taborder("516");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stacd", "absolute", "696", "110", "165", "22", null, null, this);
            obj.set_taborder("517");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_stnm", "absolute", "696", "134", "165", "22", null, null, this);
            obj.set_taborder("518");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcd", "absolute", "696", "158", "165", "22", null, null, this);
            obj.set_taborder("519");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_ctynm", "absolute", "696", "182", "165", "22", null, null, this);
            obj.set_taborder("520");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_cntrycd", "absolute", "696", "206", "165", "22", null, null, this);
            obj.set_taborder("521");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_addrsttcd", "absolute", "696", "230", "165", "22", null, null, this);
            obj.set_taborder("522");
            obj.set_enable("false");
            obj.style.set_align("right middle");
            obj.set_visible("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_laname", "absolute", "327", "459", "291", "22", null, null, this);
            obj.set_taborder("525");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_uomtypname", "absolute", "327", "583", "291", "22", null, null, this);
            obj.set_taborder("526");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_currtypname", "absolute", "220", "490", "93", "22", null, null, this);
            obj.set_taborder("527");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_brcdtypname", "absolute", "525", "490", "93", "22", null, null, this);
            obj.set_taborder("528");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_sttname", "absolute", "525", "614", "93", "22", null, null, this);
            obj.set_taborder("529");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new MaskEdit("mdt_regno", "absolute", "148", "93", "165", "22", null, null, this);
            obj.set_taborder("3");
            obj.set_type("string");
            obj.set_mask("###-##-#####");
            obj.set_limitbymask("integer");
            obj.style.set_align("right middle");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_loggrpcd", "absolute", "148", "614", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("31");
            obj.set_innerdataset("@ds_loggrpcd");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");

            obj = new Static("Static05", "absolute", "22", "150", "120", "31", null, null, this);
            obj.set_taborder("530");
            obj.set_text("우편 번호");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_zipcode", "absolute", "148", "154", "138", "22", null, null, this);
            obj.set_taborder("531");
            obj.set_maxlength("10");
            obj.set_visible("true");
            obj.set_enable("false");
            obj.set_imemode("none");
            obj.set_inputtype("number");
            this.addChild(obj.name, obj);

            obj = new Static("Static51", "absolute", "22", "177", "120", "30", null, null, this);
            obj.set_taborder("532");
            obj.set_text("기본 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static52", "absolute", "22", "208", "120", "30", null, null, this);
            obj.set_taborder("533");
            obj.set_text("상세 주소");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head_duty");
            obj.style.set_background("#f6f6f6ff");
            obj.style.set_color("#ff5a00ff");
            this.addChild(obj.name, obj);

            obj = new Static("Static53", "absolute", "141", "177", "490", "30", null, null, this);
            obj.set_taborder("534");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Static("Static54", "absolute", "141", "209", "490", "30", null, null, this);
            obj.set_taborder("535");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Edit("text_address2", "absolute", "148", "212", "470", "22", null, null, this);
            obj.set_taborder("536");
            obj.set_maxlength("16");
            this.addChild(obj.name, obj);

            obj = new Edit("edt_address1", "absolute", "148", "181", "469", "22", null, null, this);
            obj.set_taborder("537");
            obj.set_maxlength("10");
            obj.set_enable("false");
            this.addChild(obj.name, obj);

            obj = new Static("Static07", "absolute", "22", "641", "120", "31", null, null, this);
            obj.set_taborder("538");
            obj.set_text("삭제");
            obj.set_usedecorate("true");
            obj.set_wordwrap("char");
            obj.set_cssclass("sta_WF_head");
            this.addChild(obj.name, obj);

            obj = new Static("Static12", "absolute", "142", "641", "490", "31", null, null, this);
            obj.set_taborder("539");
            obj.set_cssclass("sta_WF_bg");
            this.addChild(obj.name, obj);

            obj = new Combo("cbo_alwmxdyn00", "absolute", "148", "645", "165", "22", null, null, this);
            this.addChild(obj.name, obj);
            obj.set_taborder("540");
            obj.set_innerdataset("@ds_yesno");
            obj.set_codecolumn("field1");
            obj.set_datacolumn("field2");


            
            // Layout Functions
            //-- Default Layout
            obj = new Layout("default", "", 657, 745, this,
            	//-- Layout function
            	function(p) {
            		p.set_classname("style01");
            		p.set_titletext("화주 팝업");
            		p.set_scrollbars("none");

            	}
            );
            this.addLayout(obj.name, obj);


            
            // BindItem Information
            obj = new BindItem("item5","btn_close","text","gds_lang","CLOSE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item1","edt_owname","value","ds_header","owname");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item12","edt_businesstype","value","ds_header","businesstype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item19","Static08","text","gds_lang","OWKEY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item20","Static02","text","gds_lang","OWNAME");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item21","Static40","text","gds_lang","REGNO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item22","Static42","text","gds_lang","FAX");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item29","Static11","text","gds_lang","UDF1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item33","Static28","text","gds_lang","BUSINESSTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item34","Static29","text","gds_lang","BUSINESSCATEGORY");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item38","Static37","text","gds_lang","DELSTTVRFTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item40","Static38","text","gds_lang","ALWMXDYN");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item48","btn_save","text","gds_lang","SAVE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item51","edt_businesscategory","value","ds_header","businesscategory");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item26","edt_owkey","value","ds_header","owkey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item0","edt_fax","value","ds_header","fax");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item3","Static01","text","gds_lang","UDF2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item31","Static14","text","gds_lang","CEO");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item42","Static15","text","gds_lang","TEL1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item43","Static18","text","gds_lang","TEL2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item44","Static21","text","gds_lang","BOXTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item46","Static22","text","gds_lang","LANGTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item49","edt_tel2","value","ds_header","tel2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item50","Static31","text","gds_lang","CONTACT");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item54","edt_contact","value","ds_header","contact");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item55","edt_tel1","value","ds_header","tel1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item57","edt_ceo","value","ds_header","ceo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item58","Static43","text","gds_lang","BRCDTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item59","Static44","text","gds_lang","CURRTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item60","cbo_alwmxdyn","value","ds_header","alwmxdyn");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item62","lab_title","text","gds_lang","OWNER");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item64","btn_check","text","gds_lang","CHECK");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item17","edt_currtyp","value","ds_header","currtyp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item7","edt_udf1","value","ds_header","udf1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item8","edt_udf2","value","ds_header","udf2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item10","Static06","text","gds_lang","UDF3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item11","edt_udf3","value","ds_header","udf3");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item14","Static24","text","gds_lang","DIMENSION_UOM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item24","Static26","text","gds_lang","WEIGHT_UOM");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item25","edt_weight_uom","value","ds_header","weight_uom");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item27","edt_dimension_uom","value","ds_header","dimension_uom");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item4","cbo_boxtype","value","ds_header","boxtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item28","edt_lakey","value","ds_header","lakey");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item6","edt_brcdtyp","value","ds_header","brcdtyp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item30","edt_delsttvrftyp","value","ds_header","delsttvrftyp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item32","Static30","text","gds_lang","EMAIL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item35","Static34","text","gds_lang","WEBURL");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item36","edt_weburl","value","ds_header","weburl");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item37","edt_email","value","ds_header","email");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item39","Static41","text","gds_lang","LOGGRPCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item41","Static45","text","gds_lang","UOMTYP");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item45","edt_uomtyp","value","ds_header","uomtyp");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item52","Static47","text","gds_lang","CUSTTYPE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item53","Static48","text","gds_lang","STTCD");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item56","cbo_custtype","value","ds_header","custtype");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item61","edt_sttcd","value","ds_header","sttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item63","edt_addrid","value","ds_header","addrid");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item65","edt_unit","value","ds_header","unit");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item66","edt_stacd","value","ds_header","stacd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item67","edt_stnm","value","ds_header","stnm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item68","edt_zipcd","value","ds_header","zipcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item69","edt_ctynm","value","ds_header","ctynm");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item70","edt_cntrycd","value","ds_header","cntrycd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item71","edt_addrsttcd","value","ds_header","addrsttcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item72","mdt_regno","value","ds_header","regno");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item2","cbo_loggrpcd","value","ds_header","loggrpcd");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item13","Static05","text","gds_lang","ZIPCODE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item23","edt_zipcode","value","ds_header","zipNo");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item47","Static51","text","gds_lang","ADDRESS1");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item73","Static52","text","gds_lang","ADDRESS2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item74","text_address2","value","ds_header","address2");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item75","edt_address1","value","ds_header","lnmAdres");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item9","edt_laname","value","ds_header","lakey_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item15","edt_currtypname","value","ds_header","currtyp_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item16","edt_brcdtypname","value","ds_header","brcdtyp_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item18","edt_sttname","value","ds_header","sttcd_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item76","edt_uomtypname","value","ds_header","uomtyp_desc");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item77","Static07","text","gds_lang","DELETE");
            this.addChild(obj.name, obj);
            obj.bind();
            obj = new BindItem("item78","cbo_alwmxdyn00","value","ds_header","delyn");
            this.addChild(obj.name, obj);
            obj.bind();

            
            // Remove Reference
            obj = null;
        };
        

        
        // User Script
        this.addIncludeScript("old_OwnerPop.xfdl", "lib::Comm.xjs");
        this.registerScript("old_OwnerPop.xfdl", function(exports) {
        /***********************************************************************************
        * FILE NAME   : OwnerPop.xfdl
        * PROGRAMMER  : bgheo
        * DATE        : 2016.06.08
        * DESCRIPTION : 화주 팝업
        *------------------------------------------------------------------
        * MODIFY DATE   PROGRAMMER			DESCRIPTION
        *------------------------------------------------------------------
        *
        *------------------------------------------------------------------
        ***********************************************************************************/

        /***********************************************************************************
        * Common Library
        ***********************************************************************************/
        if (this.executeIncludeScript) { this.executeIncludeScript("lib::Comm.xjs", null, exports); }	//include "lib::Comm.xjs";

        /***********************************************************************************
        * Global/Local Variable
        ***********************************************************************************/
        this.gv_flag = "";
        this.gv_menuId = "";
        this.gv_owkey = "";
        this.gv_dupChk = false;

        this.gv_Pvalue = "";
        this.gv_code = "";

        /***********************************************************************************
        * Form Event
        ***********************************************************************************/
        /* form onload 함수 */
        this.form_onload = function(obj,e)
        {
        	this.gfn_getCode("BOXTYPE", this.ds_boxtype, "");
        	this.gfn_getCode("YESORNO", this.ds_yesno, "");
        	this.gfn_getCode("CUSTTYPE", this.ds_custtype, "");
        	this.gfn_getCode("LOGGRPCD", this.ds_loggrpcd, "");
        	
        	this.cbo_boxtype.set_index(0);
        	
            this.gv_flag     = this.gfn_isNullEmpty(this.parent.argFlag); // 추가/수정 구분
            this.gv_menuId   = this.gfn_isNullEmpty(this.parent.menuId); // 화면 ID
            this.gv_owkey    = this.gfn_isNullEmpty(this.parent.owkey);

        	this.ds_header.applyChange();
        	
            if(this.gv_flag == "U"){
        		this.edt_owkey.set_enable(false);
        		this.btn_check.set_enable(false);
        		this.fn_search();
            }
        }

        /*******************************************************************************
        * TRANSACTION서비스 호출 처리 영역
        ********************************************************************************/
        this.fn_search = function()
        {
        	this.ds_header.clearData();
        	
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "selectOwner");
        	
            var sSvcId   = "select";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_header=OUT_FORM_DATA_MAP ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.gv_owkey
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_CheckCode = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "checkOwnerCount");
        	
            var sSvcId   = "check";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "";
            var sOutData = "ds_param=OUT_PARAM";
            var sParam   =  "owkey="+this.ds_header.getColumn(0, "owkey")
                         ;
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Insert = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "saveOwner");
        		
            var sSvcId   = "insert";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=INSERT";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        this.fn_Update = function()
        {
        	this.gfn_setCommon("BEANID", "ownerController");
        	this.gfn_setCommon("METHODNM", "saveOwner");
        		
            var sSvcId   = "update";
            var sSvcUrl  = application.gv_ams + application.gv_sUrl;
            var sInData  = "IN_FORM_DATA=ds_header";
            var sOutData = "";
            var sParam   = "workType=UPDATE";
            
            this.gfn_transaction(sSvcId, sSvcUrl, sInData, sOutData, sParam, "fn_callBack");
        }

        /*******************************************************************************
        * Transaction Callback 처리부분
        ********************************************************************************/
        this.fn_callBack = function(sSvcId,nErrCd,sErrMsg)
        {
        	var oWorkFrame = application.gv_WorkFrame.frames["M_" + this.gv_menuId];
        	
        	if(nErrCd != 0) {
        		this.gfn_alert("sSvcId : "+sSvcId+"\nnErrCd : "+nErrCd+"\nsErrMsg : "+sErrMsg);
        		return;
        	}
        	
        	if(sSvcId == "select"){
        		var sAddr = this.gfn_isNullEmpty(this.ds_header.getColumn(0, "cntrycd_desc")) + " " + 
        					this.gfn_isNullEmpty(this.ds_header.getColumn(0, "stacd_desc")) + " " + 
        					this.gfn_isNullEmpty(this.ds_header.getColumn(0, "zipNo"))
        				  ;
        		this.txa_addr.set_value(sAddr);

        		this.ds_header.applyChange();
        		
        		if(this.ds_header.rowcount == 0){
        			this.gfn_alert(application.gv_nodataMsg, "", function(msg, flag){
        				this.close();
        			});
        		}
        		
        		var key = "adcd_hdkey";
        		var value = "LAKEY";
        		
        		if(this.gfn_isNotNull(this.edt_lakey.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_lakey.value, "20", application.gv_ams, "", "", this.edt_lakey, this.edt_laname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "CURRTYP";
        		
        		if(this.gfn_isNotNull(this.edt_currtyp.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_currtyp.value, "20", application.gv_ams, "", "", this.edt_currtyp, this.edt_currtypname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "BRCDTYP";
        		
        		if(this.gfn_isNotNull(this.edt_brcdtyp.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_brcdtyp.value, "20", application.gv_ams, "", "", this.edt_brcdtyp, this.edt_brcdtypname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "UOMTYP";
        		
        		if(this.gfn_isNotNull(this.edt_uomtyp.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_uomtyp.value, "20", application.gv_ams, "", "", this.edt_uomtyp, this.edt_uomtypname);
        		}
        		
        		key = "adcd_hdkey";
        		value = "STTCD";
        		
        		if(this.gfn_isNotNull(this.edt_sttcd.value)){
        			this.gv_code = "Y";
        			this.gfn_codeSearch(key, value, this.edt_sttcd.value, "20", application.gv_ams, "", "", this.edt_sttcd, this.edt_sttname);
        		}
        	}else if(sSvcId == "check"){
        		if(this.ds_param.getColumn(0, "isDuplicate")){
        			this.gfn_alert("MSG_USE_NO", "", function(msg, flag){
        				this.edt_owkey.setFocus();
        				this.gv_dupChk = false;
        			});
        		}else{
        			this.gfn_alert("MSG_USE_OK");
        			this.gv_dupChk = true;
        		}
        	}else if(sSvcId == "insert"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}else if(sSvcId == "update"){
        		this.gfn_alert("MSG_ALERT_COMPLETE", "", function(msg, flag){
        			oWorkFrame.form.setFocus();
        			oWorkFrame.form.div_cond.btn_search.click();
        			
        			this.close();
        		});
        	}
        }

        /***********************************************************************************
        * user function
        ***********************************************************************************/

        /***********************************************************************************
        * Component Event
        ***********************************************************************************/
        /* btn_save_onclick 실행 */
        this.btn_save_onclick = function(obj,e)
        {
        	var dsObj = this.ds_header;
        	var dsCol = "owkey|owname|zipNo|address2|lakey|sttcd";
        	var sComp = "edt_owkey|edt_owname|btn_searchAddr|text_address2|btn_searchLakey|btn_searchSttcd";
        	
        	if(!this.gv_dupChk && this.gv_flag == "H"){
        		this.gfn_alert("MSG_ALERT_DUPLICATE", "", function(msg, flag){
        			this.edt_owkey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_laname.value) && !this.gfn_isNull(this.edt_lakey.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_lakey.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_currtypname.value) && !this.gfn_isNull(this.edt_currtyp.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_currtyp.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_brcdtypname.value) && !this.gfn_isNull(this.edt_brcdtyp.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_brcdtyp.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_uomtypname.value) && !this.gfn_isNull(this.edt_uomtyp.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_uomtyp.setFocus();
        		});
        	}else if(this.gfn_isNull(this.edt_sttname.value) && !this.gfn_isNull(this.edt_sttcd.value)){
        		this.gfn_alert("MSG_80300", "", function(msg, flag){
        			this.edt_sttcd.setFocus();
        		});
        	}else{
        		if(this.gfn_isUpdate(this.ds_header)){
        			if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        			console.log();
        				this.gfn_confirm("MSG_ASK_COMPLETE", "", function(msg, flag){
        					if(flag){
        						if(this.gv_flag == "H")	this.fn_Insert();
        						else if(this.gv_flag == "U") this.fn_Update();
        					}
        				});
        			}
        		}else{
        			this.gfn_alert("MSG_CHKDATA_NOTUPDATE");
        		}
        	}
        }

        /* btn_close_onclick 실행 */
        this.btn_close_onclick = function(obj,e)
        {
        	if(this.gfn_isUpdate(this.ds_header)){
        		this.gfn_confirm("MSG_CHKDATA_UPDATE", "", function(msg, flag){
        			if(flag){
        				this.close();
        			}
        		});
        	}else{
        		this.close();
        	}
        }

        /* btn_searchAddr_onclick 실행 */
        this.btn_searchAddr_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { 
        				putDs:this.ds_header,
        				putKey:"zipNo|lnmAdres",
        				oRow:""+0
        			   };
        	this.gfn_popup("ZipCodePop", "comm::ZipCodePop.xfdl", oArg, 580, 768, "");
        }

        /* btn_searchLakey_onclick 실행 */
        this.btn_searchLakey_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:"20"
        				,putObj:this.edt_lakey
        				,putKey:"adcd_hdkey"
        				,putValue:"LAKEY"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchCurrtyp_onclick 실행 */
        this.btn_searchCurrtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_currtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"CURRTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchBrcdtyp_onclick 실행 */
        this.btn_searchBrcdtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_brcdtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"BRCDTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchUomtyp_onclick 실행 */
        this.btn_searchUomtyp_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_uomtyp
        				,putKey:"adcd_hdkey"
        				,putValue:"UOMTYP"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_searchSttcd_onclick 실행 */
        this.btn_searchSttcd_onclick = function(obj,e)
        {
        	this.gv_Pvalue = "";
        	
        	var oArg = { divId:"Single"
        				,searchId:20
        				,putObj:this.edt_sttcd
        				,putKey:"adcd_hdkey"
        				,putValue:"STTCD"
        			   };
        	this.gfn_popup("CodeSearchMultiPop", "comm::CodeSearchMultiPop.xfdl", oArg, 700, 402, "");
        }

        /* btn_check_onclick 실행 */
        this.btn_check_onclick = function(obj,e)
        {
        	if(this.gv_dupChk){
        		return;
        	}
        	
        	var dsObj = this.ds_header;
        	var dsCol = "owkey";
        	var sComp = "edt_owkey";
        	
        	if(this.gfn_isFormDataNullCheck(dsObj, dsCol, sComp)){
        		this.fn_CheckCode();
        	}
        }

        /* ds_header_oncolumnchanged 실행 */
        this.ds_header_oncolumnchanged = function(obj,e)
        {
        	if(e.columnid == "owkey"){
        		this.gv_dupChk = false;
        	}else if(e.columnid == "lakey"){
        		this.edt_laname.set_value(this.gv_Pvalue[1]);
        		this.edt_laname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "currtyp"){
        		this.edt_currtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_currtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "brcdtyp"){
        		this.edt_brcdtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_brcdtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "uomtyp"){
        		this.edt_uomtypname.set_value(this.gv_Pvalue[1]);
        		this.edt_uomtypname.set_tooltiptext(this.gv_Pvalue[1]);
        	}else if(e.columnid == "sttcd"){
        		this.edt_sttname.set_value(this.gv_Pvalue[1]);
        		this.edt_sttname.set_tooltiptext(this.gv_Pvalue[1]);
        	}
        }

        /* edt_lakey_onchanged 실행 */
        this.edt_lakey_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "LAKEY";
        	
        	if(this.gfn_isNotNull(this.edt_lakey.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_lakey.value, "20", application.gv_ams, "", "", this.edt_lakey, this.edt_laname);
        	}
        }

        /* edt_currtyp_onchanged 실행 */
        this.edt_currtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "CURRTYP";
        	
        	if(this.gfn_isNotNull(this.edt_currtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_currtyp.value, "20", application.gv_ams, "", "", this.edt_currtyp, this.edt_currtypname);
        	}
        }

        /* edt_brcdtyp_onchanged 실행 */
        this.edt_brcdtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "BRCDTYP";
        	
        	if(this.gfn_isNotNull(this.edt_brcdtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_brcdtyp.value, "20", application.gv_ams, "", "", this.edt_brcdtyp, this.edt_brcdtypname);
        	}
        }

        /* edt_uomtyp_onchanged 실행 */
        this.edt_uomtyp_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "UOMTYP";
        	
        	if(this.gfn_isNotNull(this.edt_uomtyp.value)){
        		this.gv_code = "";
        		this.gfn_codeSearch(key, value, this.edt_uomtyp.value, "20", application.gv_ams, "", "", this.edt_uomtyp, this.edt_uomtypname);
        	}
        }

        /* edt_sttcd_onchanged 실행 */
        this.edt_sttcd_onchanged = function(obj,e)
        {
        	var key = "adcd_hdkey";
        	var value = "STTCD";
        	
        	if(this.gfn_isNotNull(this.edt_sttcd.value)){
        		this.gv_code = "Y";
        		this.gfn_codeSearch(key, value, this.edt_sttcd.value, "20", application.gv_ams, "", "", this.edt_sttcd, this.edt_sttname);
        	}
        }
        
        });


        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.ds_header.addEventHandler("oncolumnchanged", this.ds_header_oncolumnchanged, this);
            this.addEventHandler("onload", this.form_onload, this);
            this.btn_closeAll.addEventHandler("onclick", this.btn_close_onclick, this);
            this.btn_save.addEventHandler("onclick", this.btn_save_onclick, this);
            this.edt_owname.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_close.addEventHandler("onclick", this.btn_close_onclick, this);
            this.edt_fax.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_tel2.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_ceo.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.btn_check.addEventHandler("onclick", this.btn_check_onclick, this);
            this.edt_currtyp.addEventHandler("onchanged", this.edt_currtyp_onchanged, this);
            this.btn_searchCurrtyp.addEventHandler("onclick", this.btn_searchCurrtyp_onclick, this);
            this.btn_searchAddr.addEventHandler("onclick", this.btn_searchAddr_onclick, this);
            this.edt_weight_uom.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_lakey.addEventHandler("onchanged", this.edt_lakey_onchanged, this);
            this.btn_searchLakey.addEventHandler("onclick", this.btn_searchLakey_onclick, this);
            this.edt_brcdtyp.addEventHandler("onchanged", this.edt_brcdtyp_onchanged, this);
            this.btn_searchBrcdtyp.addEventHandler("onclick", this.btn_searchBrcdtyp_onclick, this);
            this.edt_weburl.addEventHandler("onkeyup", this.edt_regno_onkeyup, this);
            this.edt_uomtyp.addEventHandler("onchanged", this.edt_uomtyp_onchanged, this);
            this.btn_searchUomtyp.addEventHandler("onclick", this.btn_searchUomtyp_onclick, this);
            this.edt_sttcd.addEventHandler("onchanged", this.edt_sttcd_onchanged, this);
            this.btn_searchSttcd.addEventHandler("onclick", this.btn_searchSttcd_onclick, this);
            this.edt_zipcode.addEventHandler("oneditclick", this.edt_zipcode_oneditclick, this);
            this.Static07.addEventHandler("onclick", this.Static07_onclick, this);

        };

        this.loadIncludeScript("old_OwnerPop.xfdl", true);

       
    };
}
)();
